/**
 * @Author        : Balamurugan B
 * @CreatedOn     : June 22 ,2020
 * @Purpose       : Trigger for Narrative on reconsideration Object .
**/

trigger reconsiderationTrigger on Reconsideration__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    if(Trigger.isAfter) {
        if(Trigger.isUpdate) {
            reconsiderationTriggerHandler.afterUpdate( trigger.newMap,trigger.oldMap);
        }
        if(Trigger.isInsert){
            reconsiderationTriggerHandler.afterInsert(trigger.new);
        }
    }
}