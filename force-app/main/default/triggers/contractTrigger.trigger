/**
 * @Author        : Sindhu Venkateswarlu
 * @CreatedOn     : APRIL 23 ,2020
 * @Purpose       : Trigger for Narrative on contract Object .
**/
trigger contractTrigger on Contract__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    if(Trigger.isAfter) {
        if(Trigger.isUpdate) {
            contractTriggerHandler.afterUpdate( trigger.newMap,trigger.oldMap);
        }
        if(Trigger.isInsert){
            contractTriggerHandler.afterInsert(trigger.new);
        }
    }
}