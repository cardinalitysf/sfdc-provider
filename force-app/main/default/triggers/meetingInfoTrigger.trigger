trigger meetingInfoTrigger on MeetingInfo__c (after update) {
    TriggerDispatcher.Run(new MeetingInfoTriggerHandler());
}