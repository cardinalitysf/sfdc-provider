/**
 * @Author        : Sindhu Venkateswarlu
 * @CreatedOn     : MARCH 31 ,2020
 * @Purpose       : Trigger for Narrative.
**/
trigger applicationTrigger on Application__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    if(Trigger.isAfter) {
        if(Trigger.isUpdate) {
            applicationTriggerHandler.afterUpdate( trigger.newMap,trigger.oldMap);
        }
        
    }
}