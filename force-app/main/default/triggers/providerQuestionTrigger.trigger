trigger providerQuestionTrigger on ProviderRecordQuestion__c (after update,after insert) {
   TriggerDispatcher.Run(new ProviderRecordQuestionTriggerHandler());
}