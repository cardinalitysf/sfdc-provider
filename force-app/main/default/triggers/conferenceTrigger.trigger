trigger conferenceTrigger on Conference__c (after insert) {
    TriggerDispatcher.Run(new ConferenceTriggerHandler());
}