trigger taskTrigger on Task (after update,after insert) {
    TriggerDispatcher.Run(new TaskTriggerHandler());
}