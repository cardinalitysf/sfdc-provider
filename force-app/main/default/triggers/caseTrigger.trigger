trigger caseTrigger on Case (after update) {
    
    TriggerDispatcher.Run(new CaseTriggerHandler());
}