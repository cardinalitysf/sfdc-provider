/**
 * @Author        : Sindhu Venkateswarlu
 * @CreatedOn     : APRIL 23 ,2020
 * @Purpose       : Trigger for Narrative on account Object .
**/

trigger accountTrigger on Account (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    if(Trigger.isAfter) {
        if(Trigger.isUpdate) {
            accountTriggerHandler.afterUpdate( trigger.newMap,trigger.oldMap);
        }
        if(Trigger.isInsert){
            accountTriggerHandler.afterInsert(trigger.new);
        }
    }
}