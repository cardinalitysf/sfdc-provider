/**
 * @Author        : Vijayaraj M
 * @CreatedOn     : August 03,2020
 * @Purpose       : incidentProgramInfo Js File
 
 **/
import {
    LightningElement,
    wire,
    track
} from 'lwc';
import getIdAfterInsertSOQL from "@salesforce/apex/IncidentProgramInfo.getIdAfterInsertSOQL";
import getProgramDetails from "@salesforce/apex/IncidentProgramInfo.getProgramDetails";
import getProgramTypeDetails from "@salesforce/apex/IncidentProgramInfo.getProgramTypeDetails";
import getProgramSiteDetails from "@salesforce/apex/IncidentProgramInfo.getProgramSiteDetails";
import getProviderDetails from "@salesforce/apex/IncidentProgramInfo.getProviderDetails";
import getAgencyDetails from "@salesforce/apex/IncidentProgramInfo.getAgencyDetails";
import saveCaseRecord from '@salesforce/apex/IncidentProgramInfo.updateOrInsertSOQL';
import getIncidentDetails from '@salesforce/apex/IncidentProgramInfo.getIncidentDetails';
import getCPAaddressDetails from '@salesforce/apex/IncidentProgramInfo.getCPAaddressDetails';
import {
    utils
} from "c/utils";
import * as sharedData from 'c/sharedData';
import {
    USER_PROFILE_NAME
  } from "c/constants";
import images from "@salesforce/resourceUrl/images";
import userId from '@salesforce/user/Id';


export default class IncidentProgramInfo extends LightningElement {
    //helpIcon = images + "/help.svg";
    @track incidentRecordTypeId = null;
    @track NewReferralNumberForIncident;
    @track programOptions;
    @track program;
    @track ProgramType;
    @track programTypeOptions;
    @track providerName;
    @track providerPhone;
    @track providerEmail;
    @track providerImage;
    @track Agency;
    @track AgencyEmail;
    @track AgencyName;
    @track AgencyPhone;
    @track programSiteOptions;
    @track ProgramTypeValue;
    @track ProgramSiteValue;
    titleimg = images + "/office-building-blue.svg";
    helpIcon = images + "/help.svg";
    agencyInfoIcon = images + "/briefcase.svg";
    noImageIcon = images + "/user.svg";
    @track EmailDetails;
    @track PhoneDetails;
    @track AddressFetchingValue;
    @track ProgramFetchingValue;
    @track ProgramTypeFetchingValue;
    @track ProgramOptionsSite;
    @track Spinner = true;
    @track CPAId;
    @track freezeReviewScreen = false;



    connectedCallback() {

        this.incidentDetails();
        //this.providerDetails();
        this.agencydetails();
        this.incident();
        this.createIncidentRecord();

        
        

    }
    createIncidentRecord() {
        this.NewReferralNumberForIncident = this.caseId;
        if (!this.NewReferralNumberForIncident) {

            getIdAfterInsertSOQL()
                .then((result) => {
                   
                    sharedData.setCaseId(result);
                    this.NewReferralNumberForIncident = result;
                    const onClickId = new CustomEvent("idforcase", {
                        detail: {
                            first: result,
                        }
                    });
                    this.dispatchEvent(onClickId);
                    this.Spinner=false;

                })
                .catch((errors) => {
                    this.Spinner=false;
                    return this.dispatchEvent(utils.handleError(errors));
                });
        }else {
            this.incident();
            const onClickId = new CustomEvent("idforcase", {
                detail: {
                    first: this.NewReferralNumberForIncident,
                }
            });
            this.dispatchEvent(onClickId);
        }
    }

    incident() {
        getIncidentDetails({
            incidentProgramDetails: this.caseId
        })
            .then((result) => {
                this.Spinner = false;
               
                if (result.length > 0) {
                   
                    this.ProgramFetchingValue = result[0].Program__c;
                    this.program = result[0].Program__c;
                    this.programTypehandle();
                    this.ProgramTypeFetchingValue = result[0].ProgramType__c;
                    this.ProgramTypeValue = result[0].ProgramType__c;
                    this.ProgramType = result[0].Application__c;
                    if (this.program != 'CPA') {
                        this.programSiteHandle();
                     }
                    else {
                        this.cpaAddressFetch();
                     }
                    this.AddressFetchingValue = result[0].Address__r.AddressLine1__c;
                    this.ProgramSiteValue = result[0].Address__r.AddressLine1__c;
                    this.ProgramSite = result[0].Address__r.Id;
                    this.EmailDetails = result[0].Address__r.Email__c;
                    this.PhoneDetails = result[0].Address__r.Phone__c
                    this.providerName =result[0] && result[0].Provider__r && result[0].Provider__r.Name||'';
                    this.providerEmail = result[0] && result[0].Provider__r && result[0].Provider__r.Email||'';
                    this.providerPhone = result[0] && result[0].Provider__r && result[0].Provider__r.Phone||'';
                    if(result[0].Provider__r)
                    {
                        this.providerImage = (result[0].Provider__r.IsProfilePhotoActive)?result[0].Provider__r.MediumPhotoUrl:this.noImageIcon;
                        
                    }
                    
                   
                    else
                    {
                        this.providerImage=this.noImageIcon;
                    }
                   
                    //this.providerImage = ( result[0] && result[0].Provider__r && result[0].Provider__r.IsProfilePhotoActive||'')?(result[0] && result[0].Provider__r &&result[0].Provider__r.MediumPhotoUrl||''):this.noImageIcon;


          if (this.userProfileName === USER_PROFILE_NAME.USER_CASE_WRKR || (this.incidentStatus !== undefined && (this.incidentStatus !== "Pending" || this.incidentStatus !== "Draft")))
        {
            this.freezeReviewScreen = true;
  
          } else {
            this.freezeReviewScreen = false;
  
          }


                }
                else{
                    this.providerDetails();
                }


            })
            .catch((errors) => {

                return this.dispatchEvent(utils.handleError(errors));
            });
    }



    incidentDetails() {
        getProgramDetails({
            Programdetails: this.providerId
        }).then((result) => {
            if (result.length > 0) {

                let programOptions = result.map(row => {
                    return {
                        label: row.Program__c,
                        value: row.Program__c
                    };

                });

                const ProgramDuplicateRemove = Object.values(
                    programOptions.reduce((a, c) => {
                        a[c.value] = c;
                        return a
                    }, {}))
               
                this.programOptions = ProgramDuplicateRemove;

            }
            else 
            {
                if(this.userProfileName === USER_PROFILE_NAME.USER_PROVIDER_COMMUNITY)
                {
                    return this.dispatchEvent(utils.toastMessage("There are no approved programs", "warning"));
                }
               
            }
        }).catch((errors) => {
           
            return this.dispatchEvent(utils.handleError(errors));
        });
    }


    
    providerDetails() {
        getProviderDetails({
            ProviderBoxDetails:userId
        }).then((result) => {
           
            if (result.length > 0) {
               
                this.providerName = result[0].Name;
                this.providerEmail = result[0].Email;
                this.providerPhone = result[0].Phone;
                this.providerImage = (result[0].IsProfilePhotoActive)?result[0].MediumPhotoUrl:this.noImageIcon;

            }
        }).catch((errors) => {
            return this.dispatchEvent(utils.handleError(errors));
        });
    }
    


    handleChangeForProgramType(event) {

        this.program = event.target.value;
        this.programTypehandle();
    }

    programTypehandle() {
        getProgramTypeDetails({
            Providerdetails: this.providerId,
            getProgramdetails: this.program
        }).then((result) => {
            

            this.programTypeOptions = result.map(row => {
                
                return {
                    label: row.ProgramType__c,
                    value: row.ProgramType__c,
                    Id: row.Id
                };

            });



        }).catch((errors) => {

            return this.dispatchEvent(utils.handleError(errors));
        });

    }


    handleChangeForProgramSite(event) {
        this.ProgramTypeValue = event.target.value;
        

        event.target.options.map(item => {
            if (item.value === event.target.value) {
               
                sharedData.setApplicationId(item.Id);
                this.ProgramType = item.Id
            }
        })
        if (this.program != 'CPA') {
            this.programSiteHandle();
            
        }
        else {
            this.cpaAddressFetch();
           

        }
        // this.ProgramType = event.target.Id;

        
    }

    cpaAddressFetch() {
       
        getCPAaddressDetails({
            cpaProgramTypeDetails: this.ProgramTypeValue,
            cpaProviderDetails: this.providerId
        }).then((result) => {
            
            this.programSiteOptions = result.map(row => {
                return {
                    label: row.Address__r.AddressLine1__c,
                    value: row.Address__r.AddressLine1__c,
                    Id: row.Address__r.Id,
                    CPAId:row.Id,
                    Email: row.Address__r.Email__c,
                    Phone: row.Address__r.Phone__c
                };

            });


        }).catch((errors) => {

            return this.dispatchEvent(utils.handleError(errors));
        });
        this.agencydetails();

    }

    programSiteHandle() {
        getProgramSiteDetails({
            ProgramSiteDetails: this.ProgramType
        }).then((result) => {
           

            this.programSiteOptions = result.map(row => {
                return {
                    label: row.AddressLine1__c,
                    value: row.AddressLine1__c,
                    Id: row.Id,
                    Email: row.Email__c?row.Email__c:'',
                    Phone: row.Phone__c?row.Phone__c:''
                };

            });


        }).catch((errors) => {

            return this.dispatchEvent(utils.handleError(errors));
        });
        this.agencydetails();

    }

    handleChangeForProgramSiteValue(event) {
        this.ProgramSiteValue = event.target.value;
        //this.ProgramOptionsSite = event.target.options;
        this.Sitehandle();
    }

    Sitehandle() {
        this.programSiteOptions.map(item => {
            if (item.value === this.ProgramSiteValue) {
               
                this.ProgramSite = item.Id;
                this.EmailDetails = item.Email;
                sharedData.setCPAHomeId(item.CPAId);
                this.CPAId=item.CPAId;
                this.PhoneDetails = item.Phone;
            }

        })
    }


    /////agency details fetching Start

    agencydetails() {
        getAgencyDetails({
            AgencyDetails: this.ProgramType
        }).then((result) => {

            if (result.length > 0) {
               
                this.Agency = result[0].Caseworker__r!=undefined ? result[0].Caseworker__r.Unit__c!=undefined ? result[0].Caseworker__r.Unit__c:'':'';
                this.AgencyName = result[0].Caseworker__r!=undefined ? result[0].Caseworker__r.Name!=undefined ? result[0].Caseworker__r.Name:'':'';
                this.AgencyPhone = result[0].Caseworker__r!=undefined ? result[0].Caseworker__r.Phone!=undefined ? result[0].Caseworker__r.Phone:'':'';
                this.AgencyEmail = result[0].Caseworker__r!=undefined ? result[0].Caseworker__r.Email!=undefined ? result[0].Caseworker__r.Email:'':'';
                this.CaseworkerId = result[0].Caseworker__r!=undefined ? result[0].Caseworker__r.Id!=undefined ? result[0].Caseworker__r.Id:'':'';
                

            }
        }).catch((errors) => {
            return this.dispatchEvent(utils.handleError(errors));
        });


    }


    
    

    get providerId() {
        return sharedData.getProviderId();
        //return '0010w00000JUOloAAH';
    }

    get caseId() {
        return sharedData.getCaseId();
        //return '5000w000001z1K1AAI';
    }

    get userProfileName() {
        return sharedData.getUserProfileName();
        
    
      }

      get incidentStatus() {
        return sharedData.getIncidentStatus();
    }

    //Save Functionality
    handleSave() {
        if (!this.program) {
            return this.dispatchEvent(utils.toastMessage("Program is mandatory", "warning"));
        }

        if (!this.ProgramTypeValue)
            return this.dispatchEvent(utils.toastMessage("Program Type is mandatory", "warning"));

        if (!this.ProgramSite)
            return this.dispatchEvent(utils.toastMessage("Program Site is mandatory", "warning"));



        let myobjcase = {
            'sobjectType': 'Case'
        };
        myobjcase.Program__c = this.program;
        myobjcase.ProgramType__c = this.ProgramTypeValue;
        myobjcase.Id = this.caseId;
        myobjcase.Address__c = this.ProgramSite;
        myobjcase.Application__c = this.ProgramType;
        myobjcase.Status = 'Pending';
        myobjcase.Caseworker__c=this.CaseworkerId;
        sharedData.setCaseworkerId(this.CaseworkerId);
        myobjcase.CPAHome__c=this.CPAId;
        myobjcase.AccountId=this.providerId;
        myobjcase.Provider__c=userId;
       
       
        saveCaseRecord({
            objSobjecttoUpdateOrInsert: myobjcase
        })
            .then(result => {
                return this.dispatchEvent(utils.toastMessage("Program details saved successfully", "success"));
            })
            .catch((errors) => {
               
                return this.dispatchEvent(utils.handleError(errors));
            });
    }

   

     


}