import {LightningElement, track, wire} from "lwc";
import {utils} from "c/referralUtils";
import getReferenceValueData from "@salesforce/apex/PublicApplicationBasicInfo.getReferenceValueData";
import getPublicApplicationBasicInfo from "@salesforce/apex/PublicApplicationBasicInfo.getPublicApplicationBasicInfo";
import applicationId from "@salesforce/schema/Application__c.Id";
import PayeeLastName__cFromApplication from "@salesforce/schema/Application__c.PayeeLastName__c";
import PayeeFirstName__cFromApplication from "@salesforce/schema/Application__c.PayeeFirstName__c";
import X1099Indicator__cFromApplication from "@salesforce/schema/Application__c.X1099Indicator__c";
import MedicaidProviderFromApplication from "@salesforce/schema/Application__c.MedicaidProvider__c";
import PlacementStructureFromApplication from "@salesforce/schema/Application__c.PlacementStructure__c";
import {getRecord, updateRecord} from "lightning/uiRecordApi";
import images from "@salesforce/resourceUrl/images";
import * as sharedData from "c/sharedData";
import USER_ID from "@salesforce/user/Id";
import PROFILE_FIELD from "@salesforce/schema/User.Profile.Name";
import {refreshApex} from "@salesforce/apex";

export default class PublicApplicationBasicInfo extends LightningElement {
  applicationId = "";
  @track name;
  @track program;
  @track programType;
  @track ageGroup;
  @track payeeFirstName;
  @track payeeLastName;
  @track referralSource;
  @track indicator;
  @track medicaidProvider;
  @track placementStruct = [];
  @track placementStructData = [];
  @track supervisor = false;
  @track dropdownReferenceValue = [];
  @track userProfileName;
  @track showSelectedDatatable = true;
  refereshPublicApplicationBasicInfo;

  // multi picklist
  @track activeForPType = false;
  @track pillValueForPtype = [];
  @track selectedProgStruct = [];
  @track status;
  @track pillSingleValueCondition = true;
  attachmentIcon = images + "/friend.svg";

  @track Spinner = true;

  get options() {
    return [
      {
        label: "Yes",
        value: "Yes"
      },
      {
        label: "No",
        value: "No"
      }
    ];
  }

  get dropdownValue() {
    return this.dropdownReferenceValue;
  }

  get getApplicationId() {
    this.applicationId = sharedData.getApplicationId();
    return sharedData.getApplicationId();
  }

  get columns() {
    if (this.userProfileName === "Supervisor") {
      this.supervisor = true;
      return [
        {
          label: "Saved Placement Structures",
          fieldName: "value",
          initialWidth: 221,
          cellAttributes: {
            class: "test"
          }
        }
      ];
    } else {
      this.supervisor = false;
      return [
        {
          label: "Saved Placement Structures",
          fieldName: "value",
          initialWidth: 221,
          cellAttributes: {
            class: "test"
          }
        },
        {
          type: "button",
          fieldName: "value",
          typeAttributes: {
            iconName: "utility:delete",
            name: "deleteRecordRow",
            title: "Delete",
            disabled: false,
            class: "test"
          }
        }
      ];
    }
  }

  @wire(getRecord, {
    recordId: USER_ID,
    fields: [PROFILE_FIELD]
  })
  wireuser({ error, data }) {
    if (error) {
      this.error = error;
    } else if (data) {
      this.userProfileName = data.fields.Profile.displayValue;
    }
  }

  statusCheck = () => {
    // let status = sharedData.getApplicationStatus()
    let status = "Approved";
    if (
      (status === "Caseworker Submitted" ||
        status === "Approved" ||
        status === "Rejected",
      status === "Supervisor Rejected")
    ) {
      this.supervisor = true;
    } else {
      this.supervisor = false;
    }
  };

  //   connectedCallback() {
  //       this.statusCheck()
  //   }
  get showHideDivForPlacementCount() {
    return this.pillSingleValueForPtypeCount > 0 ? true : false;
  }

  get getpillvalueForPType() {
    if (this.pillValueForPtype.length >= 1) {
      this.pillSingleValueForPtype = this.pillValueForPtype[0].value;
      this.pillSingleValueCondition = false;
      this.pillSingleValueForPtypeCount = this.pillValueForPtype.length - 1;
    } else {
      this.pillSingleValueCondition = true;
    }
    return [...this.pillValueForPtype];
  }
  // get the PublicApplicationBasicInfo from Apex
  @wire(getPublicApplicationBasicInfo, {
    applicationId: "$getApplicationId"
  })
  getPublicApplicationBasicInfoFromApex({ data, error }) {
    this.refereshPublicApplicationBasicInfo = data;
    try {
      if (!data) {
        this.Spinner = true;
      } else {
        data &&
        data.map((item) => {
          (this.program = item.Program__c),
              (this.programType = item.ProgramType__c),
              // (this.ageGroup = `${item.AgeGroup__c} Yrs`),
              (this.ageGroup = this.ageGroupFormat(item.AgeGroup__c)),
              (this.referralSource = item.ReferralSource__c),
              (this.name = item.Name),
              (this.payeeFirstName = item.PayeeFirstName__c),
              (this.payeeLastName = item.PayeeLastName__c),
              (this.medicaidProvider = item.MedicaidProvider__c),
              (this.pillValueForPtype = item.PlacementStructure__c
                  ? this.placementDataStructure(
                      item.PlacementStructure__c.split(",")
                  )
                  : []),
              (this.indicator = item.X1099Indicator__c);
        });
      }
    } catch (err) {
      return this.dispatchEvent(utils.handleError(err))
    }

    this.pillValueForPtype.forEach((element) => {
      this.selectUnselectCheckboxes(element.value);
    });

    this.statusCheck();
    this.Spinner = false;
  }
  // get the PublicApplicationBasicInfo from Apex  end

  placementDataStructure = (item) => {
    let temp = [];
    item.map((data) => {
      temp.push({
        value: data
      });
    });
    return temp;
  };

  ageGroupFormat = (ageGroup) => {
    let splitAgeGroup = ageGroup.split(";");
    return `${splitAgeGroup[0] ? splitAgeGroup[0] : "-"} ${" "} ${
      splitAgeGroup[1] ? splitAgeGroup[1] : "-"
    } Yrs`;
  };

  //   get getReferenceValueData from apex start
  @wire(getReferenceValueData)
  getReferenceValueDataFromApex({ data, error }) {
    this.dropdownReferenceValue = data &&
        data.map((item) => {
          return Object.assign(
              {
                ...item
              },
              {
                label: item.RefValue__c,
                value: item.RefValue__c
              }
          );
        });

    this.showSelectedDatatable = !(this.dropdownReferenceValue &&
        this.dropdownReferenceValue.length >= 1);
  }

  //   get getReferenceValueData from apex end

  // inputOnChangeHandler functionality start
  inputOnChangeHandler = (event) => {
    let label = event.target.label;
    if (label === "Payee First Name") {
      this.setMaxLengthToInput(event.target.value);
      this.payeeFirstName = event.target.value;
    } else if (label === "Payee Last Name") {
      this.payeeLastName = event.target.value;
    } else if (label === "1099 Indicator") {
      this.indicator = event.target.value;
    } else if (label === "Medicaid Provider") {
      this.medicaidProvider = !!event.target.checked;
    } else {
      return "";
    }
  };
  // inputOnChangeHandler functionality end

  // setMaxLengthToInput functionality start
  setMaxLengthToInput = (name) => {
    return name.length > 50
      ? this.dispatchEvent(
          utils.toastMessage("Not More than 50 Character's", "warning")
        )
      : name;
  };
  // setMaxLengthToInput functionality end

  // mutli pick list start
  handleMouseOutButton(evr) {
    this.activeForPType = false;
  }

  get tabClassForPType() {
    return this.activeForPType
      ? "slds-popover slds-popover_full-width slds-popover_show"
      : "slds-popover slds-popover_full-width slds-popover_hide";
  }

  get toggledivForPtype() {
    return !(this.pillValueForPtype && this.pillValueForPtype.length === 0);
  }

  handlePType() {
    this.activeForPType = !this.activeForPType;
  }

  remove(array, key, value) {
    const index = array.findIndex((obj) => obj[key] === value);
    return index >= 0
      ? [...array.slice(0, index), ...array.slice(index + 1)]
      : array;
  }

  handleclickofPTypeCheckBox(evt) {
    if (evt.target.checked) {
      this.pillValueForPtype.push({
        value: evt.target.value,
        id: evt.target.id
      });
    } else {
      this.pillValueForPtype = this.remove(
        this.pillValueForPtype,
        "value",
        evt.target.value
      );
    }
    // this.selectedMembers = this.pillValueForPtype
    evt.target.checked !== evt.target.checked;
    this.pillValueForPtype = [...this.pillValueForPtype];
  }

  // mutli pick list end

  placementStructure = () => {
    return this.pillValueForPtype.map((item) => {
      return item.value;
    });
  };
  //   handleSave start
  handleSave = () => {
    if (this.pillValueForPtype.length < 1) {
      this.dispatchEvent(
        utils.toastMessage("Placement Structure is Mandatory", "warning")
      );
    } else {
      let fields = {};
      fields[applicationId.fieldApiName] = this.applicationId;
      fields[
        PayeeFirstName__cFromApplication.fieldApiName
      ] = this.payeeFirstName;
      fields[PayeeLastName__cFromApplication.fieldApiName] = this.payeeLastName;
      fields[
        MedicaidProviderFromApplication.fieldApiName
      ] = this.medicaidProvider;
      fields[X1099Indicator__cFromApplication.fieldApiName] = this.indicator;
      fields[
        PlacementStructureFromApplication.fieldApiName
      ] = this.placementStructure().toString();

      this.Spinner = true;
      const recordInput = {
        fields
      };

      updateRecord(recordInput)
        .then((result) => {
          this.Spinner = false;
          this.dispatchEvent(
            utils.toastMessage(
              "Basic Information has been updated successfully",
              "success"
            )
          );
          refreshApex(this.refereshPublicApplicationBasicInfo);
        })
        .catch((err) => {
          this.dispatchEvent(
            utils.toastMessage("Something Went Wrong", "error")
          );
        });
    }
  };

  //   handleSave end

  handleRemove(evt) {
    this.pillValueForPtype = this.remove(
      this.pillValueForPtype,
      "value",
      evt.target.label
    );
    this.selectUnselectCheckboxes(evt.target.label);
    this.activeForPType = true;
  }

  handleRowAction(event) {
    this.pillValueForPtype.map((item, index) => {
      if (item.value === event.detail.row.value) {
        this.pillValueForPtype.splice(index, 1);
      }
    });

    this.selectUnselectCheckboxes(event.detail.row.value);
  }
  selectUnselectCheckboxes(value) {
    let checkboxes = this.template.querySelectorAll("[data-id=checkbox]");
    checkboxes.forEach((element) => {
      if (element.value === value) {
        element.checked = !element.checked;
      }
    });
  }
}
