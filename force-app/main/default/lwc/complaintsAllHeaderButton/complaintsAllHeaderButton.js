/**
 * @Author        : Jayachandran
 * @CreatedOn     : Jun 29,2020
 * @Purpose       : Public / Private HeaderButton show/hide
 * @updatedBy     :
 * @updatedOn     :
 **/
import { LightningElement, track, api } from "lwc";

export default class ComplaintsAllHeaderButton extends LightningElement {
  @api caseNumber;
  @api showAddDeficiencyViolations = false;
  @api showHideAddContact = false;
  @api deficiencyBtnDisable = false;


  redirectToComplaintDashboard() {
    const onClickId = new CustomEvent("redirecttocompliantdashboard", {
      detail: {
        first: true
      }
    });
    this.dispatchEvent(onClickId);
  }

  // method for redirecting a button popup
  RedirectDeficiencyViolations() {
    const onredirectdeficiency = new CustomEvent(
      "showbuttondeficiencyviolation",
      {
        detail: {
          first: true
        }
      }
    );
    this.dispatchEvent(onredirectdeficiency);
  }

  RedirectShowopenContactModel() {
    const onredirectcontact = new CustomEvent("showbuttoncontactmodel", {
      detail: {
        first: true
      }
    });
    this.dispatchEvent(onredirectcontact);
  }
}
