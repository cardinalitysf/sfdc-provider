/**
 * @Author        : Sindhu 
 * @CreatedOn     : August 3,2020
 * @Purpose       : Responsible for fetch or insert Address and CPA Details.
**/
import { LightningElement, wire, api } from 'lwc';
import {
    getPicklistValuesByRecordType,
    getObjectInfo
} from 'lightning/uiObjectInfoApi';
import {
    utils
} from "c/utils";
import {
    CJAMS_CONSTANTS
} from 'c/constants';
import * as sharedData from "c/sharedData";


import CPAHOME_OBJECT from '@salesforce/schema/CPAHome__c';
import ADDRESS_OBJECT from '@salesforce/schema/Address__c';

import updateOrInsertSOQL from "@salesforce/apex/CpaAddressDetails.updateOrInsertSOQL";
import getStateDetails from "@salesforce/apex/CpaAddressDetails.getStateDetails";
import getAPIStreetAddress from "@salesforce/apex/GetStreetAddressFromGoogleAPI.getSuggestions";
import getFullDetails from "@salesforce/apex/GetStreetAddressFromGoogleAPI.getPlaceDetails";

import editAddressCpaDetails from "@salesforce/apex/CpaAddressDetails.editAddressCpaDetails";



export default class CpaAddressDetails extends LightningElement {

    addressDetails = {
        Name: "",
        AddressType: "",
        addressLine1: "",
        addressLine2: "",
        StartDate: "",
        EndDate: "",
        CertificateType: "",
        HomeHealthInspection: "",
        FireInspection: "",
        IsInspectionDocumentRecord__c: "",
        addressId: "",
        cpaId: ""

    };

    InspectionDocumentation;
    CertificateTypeOptions;
    addressTypeOptions;
    stateFetchResult = [];
    //addressDetails = {};
    showStateBox = false;
    showCountyBox = false;
    selectedState;
    strStreetAddresPrediction = [];
    selectedZipCode;
    active = false;
    disabledView = false;
    globalVar;
    isNext = false;
    globalAction;
    globalCpaId;
    @api detailFromPath;
    @api detailCpiId;
    connectedCallback() {
        let a = JSON.parse(this.detailFromPath);
        this.globalVar = a.providertypevalue;
        this.globalAction = a.actionName;
        if (this.globalAction == 'Edit' || this.globalAction == 'View') {
            this.globalCpaId = a.rowActionValue.CPAId;
            this.AddressEditInfo();
            if (a.rowActionValue.CPAStatus == "Suspended" || a.rowActionValue.CPAStatus == "Active")
                this.disabledView = true;
        } else {
            this.globalCpaId = this.detailCpiId;
            this.AddressEditInfo();
        }
    }

    get ProviderId() {
        return sharedData.getProviderId();
    }

    get strStreetAddresPredictionLength() {
        return this.strStreetAddresPrediction.predictions ? true : false;
    }

    get tabClass() {
        return this.active ?
            "slds-dropdown slds-dropdown_length-5 slds-dropdown_fluid slds-show" :
            "slds-dropdown slds-dropdown_length-5 slds-dropdown_fluid slds-hide"
    }


    //Function used to capture on change states in an array
    stateOnChange(event) {
        let stateSearchTxt = event.target.value;

        if (!stateSearchTxt)
            this.showStateBox = false;
        else {
            this.filteredStateArr = this.stateFetchResult.filter(val => val.Value.indexOf(stateSearchTxt) > -1).map(sresult => {
                return sresult;
            });
            this.showStateBox = this.filteredStateArr.length > 0 ? true : false;
        }
    }

    //Function used to set selected state
    setSelectedState(event) {
        this.addressDetails.selectedState = event.currentTarget.dataset.value;
        this.showStateBox = false;
    }


    //Function used to capture on change county in an array
    countyOnChange(event) {
        let countySearchTxt = event.target.value;

        if (!countySearchTxt)
            this.showCountyBox = false;
        else {
            this.filteredCountyArr = this.countyArr.filter(val => val.value.indexOf(countySearchTxt) > -1).map(cresult => {
                return cresult;
            });
            this.showCountyBox = this.filteredCountyArr.length > 0 ? true : false;
        }
    }

    //Function used to set selected city
    setSelectedCounty(event) {
        this.selectedCounty = event.currentTarget.dataset.value;
        this.showCountyBox = false;
    }

    //getStreetAddress from Google API when user manually input
    getStreetAddress(event) {
        this.addressDetails.addressLine1 = event.target.value;

        if (!event.target.value) {
            this.addressDetails.selectedCounty = "";
            this.addressDetails.selectedState = "";
            this.addressDetails.selectedCity = "";
            this.addressDetails.selectedZipCode = "";
        } else {
            getAPIStreetAddress({
                input: this.addressDetails.addressLine1
            })
                .then(result => {
                    this.strStreetAddresPrediction = JSON.parse(result);
                    this.active = this.strStreetAddresPrediction.predictions.length > 0 ? true : false;
                })
                .catch(error => {
                    this.dispatchEvent(utils.toastMessage("Error in Fetching Google API Street Address", "error"));
                })
        }
    }
    selectStreetAddress(event) {
        this.addressDetails.addressLine1 = event.target.dataset.description;

        getFullDetails({
            placeId: event.target.dataset.id
        })
            .then(result => {
                var main = JSON.parse(result);
                try {
                    var cityTextbox = this.template.querySelector(".dummyCity");
                    this.addressDetails.selectedCity = main.result.address_components[2].long_name;

                    var countyComboBox = this.template.querySelector(".dummyCounty");
                    this.addressDetails.selectedCounty = main.result.address_components[4].long_name;

                    var stateComboBox = this.template.querySelector(".dummyState");
                    this.addressDetails.selectedState = main.result.address_components[5].long_name;

                    var ZipCodeTextbox = this.template.querySelector(".dummyZipCode");
                    this.addressDetails.selectedZipCode = main.result.address_components[7].long_name;
                } catch (ex) { }
                this.active = false;
            })
            .error(error => {
                this.dispatchEvent(utils.toastMessage("Error in Selected Address", "error"));
            });
    }
    //All onchange event handler
    addressOnChange(event) {
        this.disabled = false;
        if (event.target.name == 'AddressType') {
            this.addressDetails.AddressType = event.detail.value;
        } else if (event.target.name == 'AddressLine2') {
            this.addressDetails.addressLine2 = event.target.value;
        } else if (event.target.name == 'State') {
            this.addressDetails.selectedState = event.target.value;
        } else if (event.target.name == 'City') {
            this.addressDetails.selectedCity = event.target.value;
        } else if (event.target.name == 'ZipCode') {
            this.addressDetails.selectedZipCode = event.target.value.replace(/(\D+)/g, "");
        } else if (event.target.name == 'County') {
            this.selectedCounty = event.target.value;
        } else if (event.target.name == 'Name') {
            this.addressDetails.Name = event.target.value;
        } else if (event.target.name == 'StartDate') {
            this.addressDetails.StartDate = event.target.value;
        } else if (event.target.name == 'EndDate') {
            this.addressDetails.EndDate = event.target.value;
        } else if (event.target.name == 'CertificateType') {
            this.addressDetails.CertificateType__c = event.target.value;
        } else if (event.target.name == 'HomeHealthInspection') {
            this.addressDetails.HomeHealthInspection = event.target.value;
        } else if (event.target.name == 'FireInspection') {
            this.addressDetails.FireInspection = event.target.value;
        } else if (event.target.name == 'IsInspectionDocumentRecord__c') {
            this.addressDetails.IsInspectionDocumentRecord__c = event.target.value;
        }
    }


    //Function used to fetch all states from Address Object when Screen Loads
    @wire(getStateDetails)
    getStateDetails({
        error,
        data
    }) {
        if (data) {
            if (data.length > 0) {
                let stateData = data.map(row => {
                    return {
                        Id: row.Id,
                        Value: row.RefValue__c
                    };
                });
                this.stateFetchResult = stateData;
                // this.countyInitialLoad();
            } else {
                this.dispatchEvent(utils.toastMessage("No states found", "error"));
            }
        } else if (error) {
            this.dispatchEvent(
                utils.toastMessage("Error in Fetching states", "error")
            );
        }
    }


    //Declare ADDRESS object into one variable
    @wire(getObjectInfo, {
        objectApiName: ADDRESS_OBJECT
    })
    objectInfo;

    //Function used to get all picklist values from CPAHOME object
    @wire(getPicklistValuesByRecordType, {
        objectApiName: ADDRESS_OBJECT,
        recordTypeId: '$objectInfo.data.defaultRecordTypeId'
    })
    getAllPicklistValuesAddressType({
        error,
        data
    }) {
        if (data) {
            this.error = null;

            // addressType Field Picklist values
            let addressType = [];
            data.picklistFieldValues.AddressType__c.values.forEach(key => {
                if (['Main', 'Site', 'Payment'].includes(key.label)) {
                    addressType.push({
                        label: key.label,
                        value: key.value
                    })
                }
            });
            this.addressTypeOptions = addressType;
        } else if (error) {
            this.error = JSON.stringify(error);
        }
    }
    //Declare CPAHOME object into one variable
    @wire(getObjectInfo, {
        objectApiName: CPAHOME_OBJECT
    })
    objectInfo;

    //Function used to get all picklist values from CPAHOME object
    @wire(getPicklistValuesByRecordType, {
        objectApiName: CPAHOME_OBJECT,
        recordTypeId: '$objectInfo.data.defaultRecordTypeId'
    })
    getAllPicklistValues({
        error,
        data
    }) {
        if (data) {
            this.error = null;

            // InspectionDocumentation Field Picklist values
            let inspectionDocumentationOptions = [];
            data.picklistFieldValues.IsInspectionDocumentRecord__c.values.forEach(key => {
                inspectionDocumentationOptions.push({
                    label: key.label,
                    value: key.value
                })
            });
            this.InspectionDocumentation = inspectionDocumentationOptions;

            // Certification Type Field Picklist values
            let CertificateType = [];
            data.picklistFieldValues.CertificateType__c.values.forEach(key => {
                CertificateType.push({
                    label: key.label,
                    value: key.value
                })
            });
            this.CertificateTypeOptions = CertificateType;

        } else if (error) {
            this.error = JSON.stringify(error);
        }
    }
    @api
    AddressEditInfo() {
        editAddressCpaDetails({
            selectedCpaId: this.globalCpaId
        }).then((result) => {
            if (result.length > 0) {
                this.addressDetails = {
                    AddressType: result[0].Address__r != undefined ? result[0].Address__r.AddressType__c : '',
                    addressLine1: result[0].Address__r != undefined ? result[0].Address__r.AddressLine1__c : '',
                    addressLine2: result[0].Address__r != undefined ? result[0].Address__r.AddressLine2__c : '',
                    selectedCounty: result[0].Address__r != undefined ? result[0].Address__r.County__c : '',
                    selectedCity: result[0].Address__r != undefined ? result[0].Address__r.City__c : '',
                    selectedState: result[0].Address__r != undefined ? result[0].Address__r.State__c : '',
                    selectedZipCode: result[0].Address__r != undefined ? result[0].Address__r.ZipCode__c : '',
                    Name: result[0].Name,
                    StartDate: result[0].StartDate__c,
                    EndDate: result[0].EndDate__c,
                    CertificateType__c: result[0].CertificateType__c,
                    HomeHealthInspection: result[0].HomeHealthInspection__c,
                    FireInspection: result[0].FireInspection__c,
                    IsInspectionDocumentRecord__c: result[0].IsInspectionDocumentRecord__c,
                    addressId: result[0].Address__c,
                    cpaId: result[0].Id
                };
            }
            if (this.globalAction == 'View')
                this.disabledView = true;
        }).catch(error => {
        })


    }

    //Save Method
    handleSave() {
        this.disabledView = false;
        const allValid = [
            ...this.template.querySelectorAll("lightning-input"),
            ...this.template.querySelectorAll("lightning-combobox")
        ].reduce((validSoFar, inputFields) => {
            inputFields.reportValidity();
            return validSoFar && inputFields.checkValidity();
        }, true);
        if (allValid) {
            if (
                new Date(this.addressDetails.StartDate).getTime() >
                new Date(this.addressDetails.EndDate).getTime()
            )
                return this.dispatchEvent(
                    utils.toastMessage(
                        "End date should be Greater than Start Date ",
                        "warning"
                    )
                );

            let addressobj = {
                'sobjectType': "Address__c"
            };
            if (this.globalAction == 'Edit')
                addressobj.Id = this.addressDetails.addressId != undefined ? this.addressDetails.addressId : undefined;
            addressobj.AddressType__c = this.addressDetails.AddressType;
            addressobj.AddressLine1__c = this.addressDetails.addressLine1;
            addressobj.AddressLine2__c = this.addressDetails.addressLine2;
            addressobj.State__c = this.addressDetails.selectedState;
            addressobj.City__c = this.addressDetails.selectedCity;
            addressobj.County__c = this.addressDetails.selectedCounty;
            addressobj.ZipCode__c = this.addressDetails.selectedZipCode;
            updateOrInsertSOQL({
                objSobjecttoUpdateOrInsert: addressobj
            }).then(
                result => {
                    let cpaHomesobj = {
                        sobjectType: "CPAHome__c"
                    };
                    if (this.globalAction == 'Edit')
                        cpaHomesobj.Id = this.addressDetails.cpaId != undefined ? this.addressDetails.cpaId : undefined;
                    cpaHomesobj.Name = this.addressDetails.Name;
                    cpaHomesobj.StartDate__c = this.addressDetails.StartDate;
                    cpaHomesobj.EndDate__c = this.addressDetails.EndDate;
                    cpaHomesobj.CertificateType__c = this.addressDetails.CertificateType__c;
                    cpaHomesobj.HomeHealthInspection__c = this.addressDetails.HomeHealthInspection;
                    cpaHomesobj.FireInspection__c = this.addressDetails.FireInspection;
                    cpaHomesobj.IsInspectionDocumentRecord__c = this.addressDetails.IsInspectionDocumentRecord__c;
                    cpaHomesobj.Address__c = result;
                    cpaHomesobj.Provider__c = this.ProviderId;
                    cpaHomesobj.ProgramType__c = this.globalVar;
                    cpaHomesobj.Status__c = 'Pending';
                    cpaHomesobj.ClearanceMedicalStatus__c = 'Pending';

                    updateOrInsertSOQL({
                        objSobjecttoUpdateOrInsert: cpaHomesobj
                    }).then(result => { //result is cpa id
                        if (this.isNext) {
                            const onredirect = new CustomEvent("redirectcpaidtohh", {
                                detail: {
                                    cpaid: result,
                                    first: true
                                }
                            });
                            this.dispatchEvent(onredirect);
                        } else {
                            const onredirect = new CustomEvent("redirecttocpahome", {
                                detail: {
                                    first: true
                                }
                            });
                            this.dispatchEvent(onredirect);
                        }
                        this.dispatchEvent(
                            utils.toastMessage("CPA Home Address Details Saved Successfully", "success")

                        );
                    })
                }
            )

                .catch(errors => {
                    if (errors) {
                        let error = JSON.parse(errors.body.message);
                        const { title, message, errorType } = error;
                        this.dispatchEvent(
                            utils.toastMessageWithTitle(title, message, errorType)
                        );
                    } else {
                        this.dispatchEvent(
                            utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                        );
                    }
                });
        }
    }

    //cancel method
    cancelModal() {
        this.addressDetails = {};
        const onredirect = new CustomEvent("redirecttocpahome", {
            detail: {
                first: true
            }
        });
        this.dispatchEvent(onredirect);
    }

    //Next Method
    handleNext() {
        this.disabledView = true;
        this.isNext = true;
        this.handleSave();
    }
}