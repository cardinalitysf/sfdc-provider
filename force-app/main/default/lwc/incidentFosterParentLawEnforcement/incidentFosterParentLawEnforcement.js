/**
 * @Author        : Saranraj
 * @CreatedOn     : Aug 03 ,2020
 * @Purpose       : Incident Foster parent / law enforcement
 * @updatedBy     :
 * @updatedOn     :
 **/
import { LightningElement} from "lwc";

export default class IncidentFosterParentLawEnforcement extends LightningElement {
    incident = false
    hiddenOtherModal = (event) => {
        this.incident = event.detail.incident
    }

    showOtherModal = (event) => {
        this.incident = event.detail.incident
    }
}
