/**
 * @Author        : Janaswini S
 * @CreatedOn     : March 20 ,2020
 * @Purpose       : General Information of Application
 * @updatedBy     :
 * @updatedOn     :
 **/

import {
  LightningElement,
  track
} from "lwc";
import * as sharedData from "c/sharedData";
import fetchDataForAddOrEdit from "@salesforce/apex/ApplicationGeneralInfo.fetchDataForAddOrEdit";
import fetchDataForAddressSite from "@salesforce/apex/ApplicationGeneralInfo.fetchDataForAddressSite";
import fetchDataForAddressPayment from "@salesforce/apex/ApplicationGeneralInfo.fetchDataForAddressPayment";
import fetchDataForAddressMain from "@salesforce/apex/ApplicationGeneralInfo.fetchDataForAddressMain";

import images from '@salesforce/resourceUrl/images';
import { CJAMS_CONSTANTS } from "c/constants";
import { utils } from "c/utils";


export default class ApplicationGeneralInfo extends LightningElement {

  siteicon = images + '/site-address.svg';
  mainaddress = images + '/main-address.svg';
  paymentaddress = images + '/payment-address.svg';
  providernameicon = images + '/provider-nameicon.svg';
  phone = images + '/phone.svg';
  mobile = images + '/mobile.svg';
  fax = images + '/fax.svg';
  email = images + '/email.svg';
  bedcapacity = images + '/bed-capacity.svg';
  agerange = images + '/age-range.svg';
  iqrange = images + '/iq-range.svg';
  gender = images + '/gender.svg';
  phone2 = images + '/phone2.svg';
  email2 = images + '/email2.svg';

  @track getAddressSite = "site";
  @track getAddressMain = "main";
  @track getAddressPayment = "payment";
  @track Gender;
  @track Capacity;
  @track Program;
  @track ProgramType;
  @track RFP;
  @track TaxId;
  @track SON;
  @track Corporation;
  @track ParentCorporation;
  @track MaxAge;
  @track MaxIQ;
  @track MinAge;
  @track MinIQ;
  @track ProviderEmail;
  @track ProviderCellNumber;
  @track ProviderPhone;
  @track ProviderName;
  @track ProviderFirstName;
  @track ProviderLastName;
  @track ProviderFax;
  @track nameDisplay = '';
  @track resultSiteError = false;
  @track resultSite = [];
  @track resultSitePhoneError = false;
  @track resultSiteEmailError = false;
  @track resultMainProvider = [];
  @track resultMainProviderError = false;

  @track resultPaymentError = false;
  @track resultPayment = [];
  @track resultPaymentPhoneError = false;
  @track resultPaymentEmailError = false;
  @track resultMainError = false;
  @track resultMain = [];
  @track resultMainPhoneError = false;
  @track resultMainEmailError = false;
  @track Spinner = true;
  get getapplicationid() {
    return sharedData.getApplicationId();
  }

  get getProviderid() {
    return sharedData.getProviderId();
  }

  connectedCallback() {
    this.fetchDatasFromApplication();
    this.fetchDatasFromAddressSite();
    this.fetchDatasFromAddressMain();
    this.fetchDatasFromAddressPayment();
  }

  fetchDatasFromApplication() {
    if (this.getapplicationid != undefined) {
      fetchDataForAddOrEdit({
        fetchdataname: this.getapplicationid
      }).then(result => {
        this.Capacity = result[0].Capacity__c ? result[0].Capacity__c : " - ";
        this.Gender = result[0].Gender__c ? result[0].Gender__c : ' - ';
        this.Program = result[0].Program__c ? result[0].Program__c : ' - ';
        this.ProgramType = result[0].ProgramType__c ? result[0].ProgramType__c : " - ";
        this.RFP = result[0].Provider__r.RFP__c ? result[0].Provider__r.RFP__c : ' - ';
        this.TaxId = result[0].Provider__r.TaxId__c ? result[0].Provider__r.TaxId__c : ' - ';
        this.SON = result[0].Provider__r.SON__c ? result[0].Provider__r.SON__c : ' - ';
        this.Corporation = result[0].Provider__r.Corporation__c ? result[0].Provider__r.Corporation__c : ' - ';
        this.ParentCorporation =
          result[0].Provider__r.ParentCorporation__c ? result[0].Provider__r.ParentCorporation__c : ' - ';
        this.MaxAge = result[0].MaxAge__c ? result[0].MaxAge__c : ' 0 ';
        this.MinAge = result[0].MinAge__c ? result[0].MinAge__c : ' 0 ';
        this.MaxIQ = result[0].MaxIQ__c ? result[0].MaxIQ__c : ' 0 ';
        this.MinIQ = result[0].MinIQ__c ? result[0].MinIQ__c : ' 0 ';
        this.ProviderEmail = result[0].Provider__r.Email__c ? result[0].Provider__r.Email__c : ' - ';
        this.ProviderCellNumber =
          result[0].Provider__r.CellNumber__c ? result[0].Provider__r.CellNumber__c : ' - ';
        this.ProviderPhone = result[0].Provider__r.Phone ? result[0].Provider__r.Phone : ' - ';
        this.ProviderName = result[0].Provider__r.Name ? result[0].Provider__r.Name : ' - ';
        const x = this.ProviderName;
        this.nameDisplay = x.substring(0, 1).toUpperCase();
        this.ProviderFirstName =
          result[0].Provider__r.FirstName__c;
        this.ProviderLastName =
          result[0].Provider__r.LastName__c;
        this.ProviderFax = result[0].Provider__r.Fax ? result[0].Provider__r.Fax : ' - ';

      })
        .catch((errors) => {
          if (errors) {
            let error = JSON.parse(errors.body.message);
            const { title, message, errorType } = error;
            this.dispatchEvent(
              utils.toastMessageWithTitle(title, message, errorType)
            );
          } else {
            this.dispatchEvent(
              utils.toastMessage(
                CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
                CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
              )
            );
          }
        });
    }
  }

  fetchDatasFromAddressSite() {
    if (
      this.getapplicationid != undefined &&
      this.getAddressSite
    ) {
      fetchDataForAddressSite({
        fetchdataname: this.getapplicationid,
        fetchdatatype: this.getAddressSite
      })
        .then(result => {
          let currentData = []
          result.forEach(res => {
            let obj = {}
            obj.AddressLine1 = res.AddressLine1__c;
            obj.AddressLine2 = res.AddressLine2__c;
            obj.email = res.Email__c != undefined ? res.Email__c : ' - ';
            obj.phone = res.Phone__c != undefined ? res.Phone__c : ' - ';
            currentData.push(obj)
          })

          let arr = currentData;
          var elements = arr.reduce(function (previous, current) {
            var object = previous.filter(object => object.AddressLine1 === current.AddressLine1);
            if (object.length == 0) {
              previous.push(current);
            }
            return previous;
          }, []);

          this.resultSite = elements;
          if (this.resultSite == undefined || this.resultSite == null || this.resultSite == '' || this.resultSite == []) {
            this.resultSiteError = true;
          }
        })
        .catch((errors) => {
          if (errors) {
            let error = JSON.parse(errors.body.message);
            const { title, message, errorType } = error;
            this.dispatchEvent(
              utils.toastMessageWithTitle(title, message, errorType)
            );
          } else {
            this.dispatchEvent(
              utils.toastMessage(
                CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
                CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
              )
            );
          }
        });
    }
  }

  fetchDatasFromAddressPayment() {
    if (
      this.getProviderid != undefined &&
      this.getAddressPayment
    ) {
      fetchDataForAddressPayment({
        fetchdataname: this.getProviderid,
        fetchdatatype: this.getAddressPayment
      }).then(result => {

        let currentData = []
        result.forEach(res => {
          let obj = {}
          obj.AddressLine1 = res.AddressLine1__c;
          obj.AddressLine2 = res.AddressLine2__c;
          obj.email = res.Email__c != undefined ? res.Email__c : ' - ';
          obj.phone = res.Phone__c != undefined ? res.Phone__c : ' - ';
          currentData.push(obj)
        })

        this.resultPayment = currentData;
        if (this.resultPayment == undefined || this.resultPayment == null || this.resultPayment == '' || this.resultPayment == []) {
          this.resultPaymentError = true;
        }


      })
        .catch((errors) => {
          if (errors) {
            let error = JSON.parse(errors.body.message);
            const { title, message, errorType } = error;
            this.dispatchEvent(
              utils.toastMessageWithTitle(title, message, errorType)
            );
          } else {
            this.dispatchEvent(
              utils.toastMessage(
                CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
                CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
              )
            );
          }
        });
      this.Spinner = false;
    }
  }

  fetchDatasFromAddressMain() {
    if (
      this.getProviderid != undefined &&
      this.getAddressMain
    ) {
      fetchDataForAddressMain({
        fetchdataname: this.getProviderid,
        fetchdatatype: this.getAddressMain
      }).then(result => {
        let currentData = []
        result.forEach(res => {
          let obj = {}
          obj.AddressLine1 = res.AddressLine1__c;
          obj.AddressLine2 = res.AddressLine2__c;
          obj.email = res.Email__c != undefined ? res.Email__c : ' - ';
          obj.phone = res.Phone__c != undefined ? res.Phone__c : ' - ';
          currentData.push(obj)
        })

        this.resultMain = currentData;
        if (this.resultMain == undefined || this.resultMain == null || this.resultMain == '' || this.resultMain == []) {
          this.resultMainError = true;
        }


      })
        .catch((errors) => {
          if (errors) {
            let error = JSON.parse(errors.body.message);
            const { title, message, errorType } = error;
            this.dispatchEvent(
              utils.toastMessageWithTitle(title, message, errorType)
            );
          } else {
            this.dispatchEvent(
              utils.toastMessage(
                CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
                CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
              )
            );
          }
        });
    }

  }


}