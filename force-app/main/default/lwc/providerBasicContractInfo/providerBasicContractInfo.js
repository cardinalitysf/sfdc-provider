/**
 * @Author        : Saranraj
 * @CreatedOn     : Apr 16, 2020
 * @Purpose       : Referral Basic Contact Information.
 * @updatedBy     :
 * @updatedOn     :
 **/

import { LightningElement, track, wire } from "lwc";
import getProviderContractInfoData from "@salesforce/apex/ProviderBasicContractInfo.getProviderContractInfoData";
import getChildCharacteristicsData from "@salesforce/apex/ProviderBasicContractInfo.getChildCharacteristicsData";
import getChildCharacteristicsFromContract from "@salesforce/apex/ProviderBasicContractInfo.getChildCharacteristicsFromContract";
import DeleteChildCharacteristics from "@salesforce/apex/ProviderBasicContractInfo.deleteChildCharacteristics";
import getApplicationCapacity from "@salesforce/apex/ProviderBasicContractInfo.getApplicationCapacity";
import insertOrUpdateChildCharacteristicsData from "@salesforce/apex/ProviderBasicContractInfo.insertOrUpdateChildCharacteristicsData";
import getAddressInfoData from "@salesforce/apex/ProviderBasicContractInfo.getAddressInfoData";
import getIrcBedCapacity from "@salesforce/apex/ProviderBasicContractInfo.getIrcBedCapacity";
import StatusFieldsFromContract from "@salesforce/schema/Contract__c.Status__c";
import ContractLicenseStatusFieldsFromContract from "@salesforce/schema/Contract__c.ContractStatus__c";
import FiscalStatusFieldsFromContract from "@salesforce/schema/Contract__c.FiscalDate__c";
import ContractStartDateFieldsFromContract from "@salesforce/schema/Contract__c.ContractStartDate__c";
import ContractEndDateFieldsFromContract from "@salesforce/schema/Contract__c.ContractEndDate__c";
import TotalContractBedsFieldsFromContract from "@salesforce/schema/Contract__c.TotalContractBed__c";
import ContractIdFieldsFromContract from "@salesforce/schema/Contract__c.Id";
import { refreshApex } from "@salesforce/apex";
import * as sharedData from "c/sharedData";
import { updateRecord } from "lightning/uiRecordApi";
import { utils } from "c/utils";
import images from "@salesforce/resourceUrl/images";

const columns = [
  {
    label: "Selected Characteristics",
    fieldName: "ChildCharacteristics__c",
    cellAttributes: {
      class: "test"
    }
  },
  {
    label: "Action",
    fieldName: "ChildCharacteristics__c",
    type: "button",
    sortable: true,
    typeAttributes: {
      class: "test",
      target: "_self",
      iconName: "utility:delete",
      name: "Delete",
      title: "Delete"
    }
  }
];

export default class ProviderBasicContactInfo extends LightningElement {
  // @track columns = columns;
  @track licenseBeds = "";
  @track licenseStatus = "";
  @track utilizationRate = "";
  @track IRCBedCapacity = "";
  @track contractLicenseStatus = "";
  @track contractedBeds = "";
  _contractedBedsGlobalValue;
  @track licenseNumber = "";
  @track startDate = "";
  @track endDate = "";
  @track fiscalStatus = "";
  @track contractId = "";
  @track siteId;
  @track siteAddress;
  @track emailAddress;
  @track mobileNumber;
  email = images + "/email.svg";
  phone = images + "/phone.svg";
  attachmentIcon = images + "/friend.svg";

  refreshProviderBasicContractInfoData;
  @track openAddChildCharacteristicsModal = false;
  @track childCharacteristicsData = [];
  @track ListSelectedData = [];
  @track deleteModal = false;
  @track editBasicInfo = true;
  @track hiddenEditButton = false;
  @track disabledEdit = true;
  @track userProfile = false;

  deleteRowId;
  applicationId = "";
  @track childCharacteristicsDatatableData = false;
  getProviderContractInfoDataRefresh;
  getChildCharacteristicsFromContractRefresh;
  getChildCharacteristicsDataRefresh;

  get columns() {
    if (this.userProfile) {
      return [
        {
          label: "Selected Characteristics",
          fieldName: "ChildCharacteristics__c",
          cellAttributes: {
            class: "test"
          }
        }
      ];
    } else {
      return [
        {
          label: "Selected Characteristics",
          fieldName: "ChildCharacteristics__c",
          cellAttributes: {
            class: "test"
          }
        },
        {
          label: "Action",
          fieldName: "ChildCharacteristics__c",
          type: "button",
          sortable: true,
          typeAttributes: {
            class: "test",
            target: "_self",
            iconName: "utility:delete",
            name: "Delete",
            title: "Delete"
          }
        }
      ];
    }
  }

  // get contract id from sharedData start
  get getContractId() {
    return sharedData.getContractId();
  }

  // get contract id from sharedData end

  // getLicenseId from sharedData start
  get getLicenseId() {
    return sharedData.getLicenseId();
  }

  get isSuperUserFlow() {
    let userProfile = sharedData.getUserProfileName();
    this.userProfile = userProfile === "Supervisor";
    return userProfile === "Supervisor";
  }

  connectedCallback() {
    this.isSuperUserFlow;
  }

  // getLicenseId from sharedData end

  disabledEditButton(contractLicenseStatus) {
    if (
      contractLicenseStatus === "In process" ||
      contractLicenseStatus === "Returned"
    ) {
      this.disabledEdit = false;
    }
  }

  // get contract  information data
  @wire(getProviderContractInfoData, {
    contractId: "$getContractId"
  })
  ProviderContractInfoData(result) {
    this.getProviderContractInfoDataRefresh = result;
    try {
      if (result.data) {
        result.data.map((item) => {
          this.licenseNumber = item.License__r ? item.License__r.Name : "";
          this.licenseStatus = item.License__r
            ? item.License__r.ProgramType__c
            : "";
          this.contractId = item.Id;
          this.contractLicenseStatus = item.ContractStatus__c;
          this.fiscalStatus = item.FiscalDate__c;
          this.startDate = item.License__r ? item.License__r.StartDate__c : "";
          this.endDate = item.License__r ? item.License__r.EndDate__c : "";
          this.contractedBeds = item.TotalContractBed__c;
          this._contractedBedsGlobalValue = item.TotalContractBed__c;
          this.disabledEditButton(this.contractLicenseStatus);
        });
      }
    } catch (err) {
      return this.dispatchEvent(utils.handleError(err));
    }
  }

  // get contract  information data  end

  // getIrcBedCapacity from apex start
  @wire(getIrcBedCapacity, {
    licenseId: "$getLicenseId"
  })
  getIrcBedCapacityFromApex(result) {
    try {
      if (result.data) {
        result.data.map((item) => {
          this.IRCBedCapacity = item.IRCBedCapacity__c;
        });
      }
    } catch (err) {
      return this.dispatchEvent(utils.handleError(err));
    }
  }
  // getIrcBedCapacity from apex end

  // getApplicationCapacity start

  @wire(getApplicationCapacity, {
    licenseId: "$getLicenseId"
  })
  getApplicationCapacityFromApex(result) {
    try {
      if (result.data) {
        result.data.map((item) => {
          this.licenseBeds = item.Application__r
            ? item.Application__r.Capacity__c
            : "";
          this.getApplicationId(item.Application__c);
        });
      }
    } catch (err) {
      return this.dispatchEvent(utils.handleError(err));
    }
  }

  // getApplicationCapacity end

  // getAddressInfoData  functionality start
  getApplicationId = async (applicationId) => {
    try {
      let response = await getAddressInfoData({ applicationId: applicationId });
      this.siteId = response[0].Name;
      this.siteAddress = response[0].AddressLine1__c;
      this.mobileNumber = response[0].Phone__c;
      this.emailAddress = response[0].Email__c;
    } catch (err) {
      return this.dispatchEvent(utils.handleError(err));
    }

    // getAddressInfoData({
    //         applicationId: applicationId
    //     })
    //     .then(response => {
    //         if (response) {
    //             this.siteId = response[0].Name;
    //             this.siteAddress = response[0].AddressLine1__c;
    //             this.mobileNumber = response[0].Phone__c;
    //             this.emailAddress = response[0].Email__c;
    //         }
    //     })
  };

  // getAddressInfoData functionality end

  // getChildCharacteristics from apex functionality start
  @wire(getChildCharacteristicsFromContract, {
    contractId: "$getContractId"
  })
  ChildCharacteristicsFromContra(result) {
    this.getChildCharacteristicsFromContractRefresh = result;
    let temp = [];
    if (result.data) {
      let childCharacter = result.data.map((item) => {
        return item.ChildCharacteristics__c && item.ChildCharacteristics__c;
      });

      if (childCharacter !== undefined && childCharacter.length >= 1) {
        /** dont simplify **/
        let splittedChildCharacter = childCharacter.map((item) =>
          item ? item.split(",") : ""
        );

        // if (childCharacter !== "") {
        splittedChildCharacter.map((item, index) => {
          return (
            item &&
            item.forEach((data) => {
              temp.push({
                ChildCharacteristics__c: data
              });
            })
          );
        });
        // }
        this.ListSelectedData = temp;
      } else {
        childCharacter &&
          childCharacter.forEach((item) => {
            temp.push({
              ChildCharacteristics__c: item
            });
          });
        this.ListSelectedData = temp;
      }
    }
    this.childCharacteristicsDatatableData = this.ListSelectedData.length === 0;
  }

  // getChildCharacteristics from apex functionality end

  //   get child Characteristics data

  @wire(getChildCharacteristicsData, {
    contractId: "$getContractId"
  })
  ChildCharacteristicsData(result) {
    this.getChildCharacteristicsDataRefresh = result;
    let value = [];

    try {
      if (result.data) {
        result.data.map((item) => {
          return value.push({
            Id: item.Id,
            Activity__c: item.Activity__c,
            RefKey__c: item.RefKey__c
          });
        });
      }
    } catch (err) {
      return this.dispatchEvent(utils.handleError(err));
    }

    this.childCharacteristicsData = JSON.parse(JSON.stringify(value));
  }

  // get child characteristics data function end

  // editFormData functionality start
  editFormData = () => {
    this.editBasicInfo = false;
    this.hiddenEditButton = true;
  };
  // editFormData functionality end

  // onchange handler
  inputChangeHandler = (event) => {
    let evtName = event.target.name;
    let evtValue = event.target.value;
    if (evtValue > this.IRCBedCapacity) {
      this.dispatchEvent(utils.toastMessage("Cannot added", "warning"));
    }
    if (evtName === "TotalContractBed__c") {
      this.contractedBeds = evtValue;
    } else {
      return;
    }
  };

  // onchange handler end

  // CancelHandler handler function start
  cancelHandler = () => {
    this.editBasicInfo = true;
    this.hiddenEditButton = false;
    this.contractedBeds = this._contractedBedsGlobalValue;
    return refreshApex(this.getProviderContractInfoDataRefresh);
  };
  // CancelHandler handler function end

  // addChildCharacteristicsModalHandler function

  addChildCharacteristicsModalHandler = () => {
    this.openAddChildCharacteristicsModal = true;
  };

  handleCloseModal = () => {
    this.openAddChildCharacteristicsModal = false;
  };

  // addChildCharacteristicsModalHandler function end

  // saveHandler functionality start
  saveHandler = () => {
    const fields = {};

    fields[ContractIdFieldsFromContract.fieldApiName] = this.contractId;
    // fields[StatusFieldsFromContract.fieldApiName] = this.licenseStatus;
    // fields[
    //     ContractLicenseStatusFieldsFromContract.fieldApiName
    //     ] = this.contractLicenseStatus;
    // fields[ContractStartDateFieldsFromContract.fieldApiName] = this.startDate;
    // fields[ContractEndDateFieldsFromContract.fieldApiName] = this.endDate;
    // fields[FiscalStatusFieldsFromContract] = this.fiscalStatus;
    // fields[tilizationFieldsFromContract.fieldApiName] = this.utilizationRate;
    fields[
      TotalContractBedsFieldsFromContract.fieldApiName
    ] = this.contractedBeds;

    const updatedContractData = {
      fields
    };

    updateRecord(updatedContractData)
      .then(() => {
        this.dispatchEvent(
          utils.toastMessage("Records Updated Successfully", "success")
        );
        refreshApex(this.getProviderContractInfoDataRefresh);
        // this.cancelHandler();
        this.editBasicInfo = true;
        this.hiddenEditButton = false;
      })
      .catch(() => {
        this.dispatchEvent(
          utils.toastMessage("Something Went Wrong", "warning")
        );
      });
  };

  // saveHandler functionality end

  // handleRowAction functionality start

  handleRowAction = (event) => {
    this.deleteModal = true;
    this.deleteRowId = event.detail.row.ChildCharacteristics__c;
  };
  // handleRowAction functionality end

  // deleteOkHandler functionality start
  deleteOkHandler = () => {
    DeleteChildCharacteristics({
      childCharacter: this.deleteRowId,
      contractId: this.contractId
    }).then((response) => {
      insertOrUpdateChildCharacteristicsData({
        selectedChildCharac: response !== null ? response.toString() : null,
        contractId: this.getContractId
      }).then((res) => {
        this.dispatchEvent(
          utils.toastMessage(
            "Child Characteristics has been deleted successfully",
            "success"
          )
        );
        this.deleteModal = false;
        refreshApex(this.getChildCharacteristicsFromContractRefresh);
      });
    });
  };

  // deleteRecord(this.deleteRowId)
  //   .then(() => {
  //     this.dispatchEvent(
  //       utils.toastMessage("Deleted Successfully", "success")
  //     );
  //     this.deleteModal = false;
  //     refreshApex(this.getChildCharacteristicsFromContractRefresh);
  //   })
  //   .catch((err) => {
  //     this.dispatchEvent(utils.toastMessage(err, "warning"));
  //     this.deleteModal = false;
  //   });
  // this.deleteModal = false;

  // deleteOkHandler functionality end

  // deleteCancel handler functionality start
  deleteCancelHandler = () => {
    this.deleteModal = false;
  };
  // deleteCancel handler functionality end

  closeChildCharacteristics = (event) => {
    this.openAddChildCharacteristicsModal =
      event.detail.openAddChildCharacteristicsModal;
  };

  // selectedData save functionality start
  saveSelectedData = (event) => {
    let selected = [];
    event.detail.selectedData.map((item) => {
      return selected.push(item.Activity__c);
    });

    insertOrUpdateChildCharacteristicsData({
      selectedChildCharac: selected.toString(),
      contractId: this.getContractId
    })
      .then((response) => {
        this.openAddChildCharacteristicsModal = false;
        this.dispatchEvent(
          utils.toastMessage(
            "Child Characteristics has been added successfully",
            "success"
          )
        );
        refreshApex(this.getChildCharacteristicsFromContractRefresh);
      })
      .catch((err) => {
        this.dispatchEvent(utils.toastMessage(err, "error"));
      });
  };
}
