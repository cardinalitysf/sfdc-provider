/**
 * @Author        : BalaMurugan
 * @CreatedOn     : Apr 12,2020
 * @Purpose       : Provider Of Review Status.
 * @UpdatedBy     : Jayachandran S 
 * @Purpose       : Validation for Decision on Apr 13,2020
 * @updatedBy : Sundar K(Whole Functionality is modified)
 * @updatedOn : Apr 15, 2020
 **/

import {
    LightningElement,
    track,
    wire,
    api
} from 'lwc';
import fatchPickListValue from '@salesforce/apex/ProviderDecisionController.getPickListValues';
import getReviewDetails from "@salesforce/apex/ProviderDecisionController.getReviewDetails";
import insertApplicationReport from "@salesforce/apex/ProviderDecisionController.updateapplicationdetails";
import * as sharedData from "c/sharedData";
import {
    utils
} from 'c/utils';
import {
    loadStyle
} from 'lightning/platformResourceLoader';
import myResource from '@salesforce/resourceUrl/styleSheet';

//Modified by Sundar K
import {
    CJAMS_CONSTANTS
} from 'c/constants';

export default class providerDecisionInfo extends LightningElement {
    //Modified By Sundar K
    @track isSaveAsDraftDisable = false;
    @track isStatusDisable = false;
    @track isCommentsDisable = false;
    @track isBtnDisable = false;

    @track ReviewStatus__c;
    @track Comments__c;
    @track statusOptions = [];

    //Mari modified
    _enableSubmitButtonAsPerProgramDetails;

    @api getValueFromParent;
    get getValueFromParent() {
        return this._enableSubmitButtonAsPerProgramDetails;
    }

    set getValueFromParent(value) {
        this._enableSubmitButtonAsPerProgramDetails = value;
    }

    //Get Application id 
    get applicationid() {
        return sharedData.getApplicationId();
    }

    //get Provider Id
    get providerid() {
        return sharedData.getProviderId();
    }

    //Connected Call back method
    connectedCallback() {
        getReviewDetails({
            applicationid: this.applicationid
        }).then(result => {
            if (result.length > 0) {
                this.ReviewStatus__c = result[0].ReviewStatus__c;
                this.Comments__c = result[0].Comments__c;

                if (result[0].Status__c == CJAMS_CONSTANTS.PROVIDER_APP_FINAL_STATUS_REVIEW || result[0].Status__c == CJAMS_CONSTANTS.APPLICATION_FINAL_STATUS_REVIEW || result[0].Status__c == CJAMS_CONSTANTS.PROVIDER_APP_FINAL_STATUS_REJECTED || result[0].Status__c == CJAMS_CONSTANTS.PROVIDER_APP_FINAL_STATUS_APPEAL || result[0].Status__c == CJAMS_CONSTANTS.APPLICATION_FINAL_STATUS_APPROVED || result[0].Status__c == CJAMS_CONSTANTS.APPLICATION_FINAL_STATUS_SUPERVISOR_REJECTED) {
                    this.isSaveAsDraftDisable = true;
                    this.isBtnDisable = true;
                    this.isStatusDisable = true;
                    this.isCommentsDisable = true;
                } else if (result[0].ReviewStatus__c == CJAMS_CONSTANTS.PROVIDER_APP_REVIEW_STATUS_PENDING) {
                    this.isBtnDisable = true;
                } else {
                    this.isSaveAsDraftDisable = false;
                    this.isBtnDisable = false;
                    this.isStatusDisable = false;
                    this.isCommentsDisable = false;
                }
            }
        }).catch(errors => {
            this.Spinner = false;
            let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
            return this.dispatchEvent(utils.handleError(errors, config));
        });
    }
    //Get PickList values for ReviewStatus
    @wire(fatchPickListValue, {
        objInfo: {
            'sobjectType': 'Application__c'
        },
        picklistFieldApi: 'ReviewStatus__c'
    }) wireduserDetails2({
        error,
        data
    }) {
        if (data) {
            if (data.length > 0)
                this.statusOptions = data;
        } else if (error) { }
    }

    //statusOnChange functionality
    statusOnChange(event) {
        if (event.detail.value == CJAMS_CONSTANTS.PROVIDER_APP_REVIEW_STATUS_PENDING) {
            this.isBtnDisable = true;
        } else {
            this.isBtnDisable = false;
        }
        this.ReviewStatus__c = event.detail.value;
    }

    //Comments On Change functionality
    commentsOnChange(event) {
        this.Comments__c = event.detail.value;
    }

    //load stylesheet after component rendered
    renderedCallback() {
        Promise.all([
            loadStyle(this, myResource + '/styleSheet.css')
        ])
    }

    //Save Application functionality
    DraftMethod() {
        if (!this.ReviewStatus__c)
            return this.dispatchEvent(utils.toastMessage("Please Select Status", "Warning"));

        let myObj = {
            'sobjectType': 'Application__c'
        };

        myObj.Status__c = CJAMS_CONSTANTS.PROVIDER_APP_STATUS_PENDING;
        myObj.Provider__c = this.providerid;
        myObj.ReviewStatus__c = this.ReviewStatus__c;
        myObj.Comments__c = this.Comments__c;

        if (this.applicationid)
            myObj.Id = this.applicationid;

        insertApplicationReport({
            objSobjecttoUpdateOrInsert: myObj
        })
            .then(result => {
                this.dispatchEvent(utils.toastMessage("Provider Application has been saved successfully", "Success"));
                setTimeout(() => {
                    window.location.reload();
                }, 1000);
            })
            .catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
    }

    //Submit Application functionality
    submitMethod() {
        if (this._enableSubmitButtonAsPerProgramDetails != true)
            return this.dispatchEvent(utils.toastMessage("Please fill the Program Details First", "Warning"));

        if (!this.ReviewStatus__c)
            return this.dispatchEvent(utils.toastMessage("Please Select Status", "Warning"));

        let myObj = {
            'sobjectType': 'Application__c'
        };

        myObj.Provider__c = this.providerid;
        myObj.ReviewStatus__c = this.ReviewStatus__c;
        myObj.Comments__c = this.Comments__c;

        if (this.ReviewStatus__c == CJAMS_CONSTANTS.PROVIDER_APP_REVIEW_STATUS_PENDING) {
            myObj.Status__c = CJAMS_CONSTANTS.PROVIDER_APP_FINAL_STATUS_PENDING;
        } else if (this.ReviewStatus__c == CJAMS_CONSTANTS.PROVIDER_APP_REVIEW_STATUS_FOR_REVIEW) {
            myObj.Status__c = CJAMS_CONSTANTS.PROVIDER_APP_FINAL_STATUS_REVIEW;
        } else {
            myObj.Status__c = CJAMS_CONSTANTS.PROVIDER_APP_REVIEW_STATUS_APPEAL;
        }

        if (this.applicationid)
            myObj.Id = this.applicationid;

        insertApplicationReport({
            objSobjecttoUpdateOrInsert: myObj
        })
            .then(result => {
                this.dispatchEvent(utils.toastMessage("Provider Application has been submitted successfully", "Success"));
                setTimeout(() => {
                    window.location.reload();
                }, 1000);
            })
            .catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
    }
}