import { LightningElement, track, api, wire } from 'lwc';
import * as sharedData from "c/sharedData";
import { getRecord } from "lightning/uiRecordApi";

import Reconsideration_Id_FIELD from '@salesforce/schema/Reconsideration__c.Name';
import RECONSIDERATIONSTATUS_FIELD from "@salesforce/schema/Reconsideration__c.Status__c";

export default class PublicProvidersAllTabsetCmps extends LightningElement {

    @track providersTabsetView = true;
    @track providersReconsiderationTabsetView = false;
    @track homeVisitModal;
    @track hideHeaderButton = true;

    @track openServiceInfoModal;
    @track openPetInfoModal;
    @track openTrainingModal;
    @api activeTab = 'licenseInfo';

    @api reconsideration = false;
    @api providerName;

    //Name Passed For Recons
    @track reconsiderationName;
    //HHM
    @track isView = false;
    @track searchHolder = false;
    @track showResult = false;
    @track meetingFlag;
    @track showListParent;

    @track showPublicProviderAddmember = false;
    @track showPublicProviderAddmemberSearch = false;
    @track ReconsiderationStatus;

    //Button Handling Functions
    @track activeTabValue = null;
    @track showAddService = false;
    @track showPetDetails = false;
    @track showAddHomeStudy = false;
    @track showTrainingDetails = false;
    @track showAddReconsideration = false;

    //Active Tab Button show/hide
    handleActiveTabButtons(event) {
        //Supervisor flow will return empty when on func called.
        if (this.getUserProfileName != "Supervisor") {
            if (
                [
                    "Submitted",
                    "Approved",
                    "Rejected"
                ].includes(this.ReconsiderationStatus))
                return
        }
        if (this.getUserProfileName == 'Supervisor') {
            return
        }
        //Active tab view show hide according to tabs
        this.activeTabValue = event.target.value;
        if (this.activeTabValue === 'PetInfo') {
            this.showPetDetails = true;
            this.showAddService = false;
            this.showSanctions = false;
            this.showAddHomeStudy = false;
            this.showTrainingDetails = false;
            this.showAddReconsideration = false;
        } else if (this.activeTabValue === 'AddServices') {
            this.showPetDetails = false;
            this.showAddService = true;
            this.showSanctions = false;
            this.showAddHomeStudy = false;
            this.showTrainingDetails = false;
            this.showAddReconsideration = false;
        } else if (this.activeTabValue === 'StudyVisit') {
            this.showPetDetails = false;
            this.showAddService = false;
            this.showSanctions = false;
            this.showAddHomeStudy = true;
            this.showTrainingDetails = false;
            this.showAddReconsideration = false;
        } else if (this.activeTabValue === 'Training') {
            this.showPetDetails = false;
            this.showAddService = false;
            this.showAddHomeStudy = false;
            this.showSanctions = false;
            this.showTrainingDetails = true;
            this.showAddReconsideration = false;
        } else if (this.activeTabValue === 'reconsideration') {
            this.showPetDetails = false;
            this.showAddService = false;
            this.showAddHomeStudy = false;
            this.showSanctions = false;
            this.showTrainingDetails = false;
            this.showAddReconsideration = true;
        } else if (this.activeTabValue === 'sanctions') {
            this.showPetDetails = false;
            this.showAddService = false;
            this.showAddHomeStudy = false;
            this.showTrainingDetails = false;
            this.showAddReconsideration = false;
            this.showSanctions = true
            this.reconsideration = false
        } else {
            this.showPetDetails = false;
            this.showAddService = false;
            this.showAddHomeStudy = false;
            this.showSanctions = false;
            this.showTrainingDetails = false;
            this.showAddReconsideration = false;
        }
    }

    redirectToPubProvDashboard(event) {
        this.providersTabsetView = true;
        this.providersReconsiderationTabsetView = false;
        const onClickId = new CustomEvent('redirecttopubprovdashboard', {
            detail: {
                first: event.detail.first
            }
        });
        this.dispatchEvent(onClickId);
        this.activeTab = "licenseInfo";
    }

    redirectTohomestudy(event) {
        this.homeVisitModal = true;
        this.activeTab = "StudyVisit";
    }
    handleToggleComponentsForHomeVisit(event) {
        this.homeVisitModal = false;
    }

    // redirecttoreconsiderationdetailsComp functionsality start
    redirecttoreconsiderationdetailsComp = (event) => {
        this.reconsideration = true
    }
    // redirecttoreconsiderationdetailsComp functionsality end

    redirectToServiceInfo(event) {
        this.openServiceInfoModal = true;
        this.activeTab = "AddServices";
    }
    handleToggleComponentsForService(event) {
        this.openServiceInfoModal = false;
    }
    redirectToPetInfo(event) {
        this.openPetInfoModal = true;
        this.activeTab = "PetInfo";
    }

    handleTogglePetComponents(event) {
        this.openPetInfoModal = false;
    }

    redirectToTraining(event) {
        this.openTrainingModal = true;
        this.activeTab = "Training";
    }

    handleToggleComponents(event) {
        this.openTrainingModal = false;
    }

    //HHM
    handleAddMemberSearch(event) {
        this.showPublicProviderAddmemberSearch = true;
        this.showPublicProviderAddmember = event.detail.first;
        this.providersTabsetView = event.detail.first;
        this.showResult = false;
        this.hideHeaderButton = false;
    }

    handleAddMember() {
        this.showPublicProviderAddmember = true;
        this.searchHolder = false;
        this.providersTabsetView = false;
        this.hideHeaderButton = false;
    }

    handleAddMemberView(event) {
        this.showPublicProviderAddmember = true;
        this.isView = true;
        this.hideHeaderButton = false;
    }
    handleMeeting(event) {
        this.meetingFlag = event.detail.meetingFlag;
    }

    redirectToHousHoldDashboard(event) {
        this.showPublicProviderAddmember = event.detail.first;
        this.activeTab = 'houseHoldMember';
        this.providersTabsetView = true;
        this.hideHeaderButton = true;
    }
    handleAddMemberSearchFromAddHouse(event) {
        this.showPublicProviderAddmemberSearch = true;
        this.showPublicProviderAddmember = event.detail.first;
        this.providersTabsetView = event.detail.first;
        this.showResult = true;
        this.hideHeaderButton = false;

    }
    handleAddMemberHome(event) {
        this.showPublicProviderAddmemberSearch = event.detail.first;
        this.activeTab = 'houseHoldMember';
        this.providersTabsetView = true;
        this.hideHeaderButton = true;
    }
    redirectHandleAddMember(event) {
        this.showPublicProviderAddmemberSearch = event.detail.first;
        this.showListParent = event.detail.searchLists;
        this.showPublicProviderAddmember = true;
        this.searchHolder = true;
        this.hideHeaderButton = false;
    }
    //Closed HHM

    // reconsiderationModalCloseHandler functionality end

    showReconsiderationTabset(event) {
        this.providersTabsetView = event.detail.first;
        this.providersReconsiderationTabsetView= true;
        this.reconsiderationId = sharedData.getReconsiderationId().split(', ')[0];
    }

    redirectToPubProvTabSet(event) {
        this.providersTabsetView = event.detail.first;
        this.providersReconsiderationTabsetView = false;
        this.activeTab = 'reconsideration';
    }

    // addModalCloseHandler start 
    addModalCloseHandler = (event) => {
        this.reconsideration = event.detail.modalClose
    }
    // addModalCloseHandler end

    //Reconsideration ID Name
    @wire(getRecord, {
        recordId: "$reconsiderationId",
        fields: [Reconsideration_Id_FIELD]
    })
    wirereconsid({ error, data }) {
        try {
            if (data) {
                this.reconsiderationName = data.fields.Name.value;
            }
        } catch (error) {
            throw error;
        }
    }
    get getUserProfileName() {
        return sharedData.getUserProfileName();
    }

    //Supervisor flow Button False Hidden 
    @wire(getRecord, {
        recordId: "$reconsiderationId",
        fields: [RECONSIDERATIONSTATUS_FIELD]
    })
    wireuser({ error, data }) {
        if (error) {
            this.error = error;
        } else if (data) {
            this.ReconsiderationStatus = data.fields.Status__c.value;
            if (data) {
                if (this.getUserProfileName != "Supervisor") {
                    if (
                        [
                            "Submitted",
                            "Approved",
                            "Rejected"
                        ].includes(this.ReconsiderationStatus)
                    ) {
                        this.showPetDetails = false;
                        this.showAddService = false;
                        this.showAddHomeStudy = false;
                        this.showTrainingDetails = false;
                        this.showAddReconsideration = false;
                    }
                }
                if (this.getUserProfileName == "Supervisor") {
                    this.showPetDetails = false;
                    this.showAddService = false;
                    this.showAddHomeStudy = false;
                    this.showTrainingDetails = false;
                    this.showAddReconsideration = false;
                }
            }
        }
    }

    redirectToPublicApplication(){
        const oncaseid = new CustomEvent('redirecteditapplication', {
            detail: {
                first: false
            }
        });
        this.dispatchEvent(oncaseid);
    }

}