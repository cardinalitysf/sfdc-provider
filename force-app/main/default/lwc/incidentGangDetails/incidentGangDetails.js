/**
 * @Author        : G sathishkumar ,
 * @CreatedOn     : Aug 03,2020
 * @Purpose       : IncidentGangDetails.
 **/

import { LightningElement } from 'lwc';
import insertUpdateGangDetails from "@salesforce/apex/IncidentGangDetails.insertUpdateGangDetails";
import getGangDetails from "@salesforce/apex/IncidentGangDetails.getGangDetails";

import images from '@salesforce/resourceUrl/images';
import { utils } from "c/utils";
import * as sharedData from "c/sharedData";
import { CJAMS_CONSTANTS, USER_PROFILE_NAME } from "c/constants";


export default class IncidentGangDetails extends LightningElement {

    gangDetailValue = {};
    showgangDetails = false;
    gangRelated;
    supportingEvidence;
    evidenceDetails;
    gandDetailsID;
    isUpdateFlag = false;
    
    gangFlowBtnDisable = false;
    disableBtnsave = false;


    gangdetailIcon = images + '/gangIcon.svg';


    get caseId() {
        return sharedData.getCaseId();
        //return '5000w000001fWC0AAM';
    }


    get userProfileName() {
        return sharedData.getUserProfileName();
        // return 'Caseworker';
      }

      get incidentStatus() {
        return sharedData.getIncidentStatus();
     }

    get options() {
        return [{
            label: 'Yes',
            value: 'Yes'
        },
        {
            label: 'No',
            value: 'No'
        },
        ];
    }

    handleChange(event) {
        if (event.target.name == 'GangRelated__c') {
            this.gangRelated = event.target.value;
        } else if (event.target.name == 'SupportingEvidence__c') {
            this.supportingEvidence = event.target.value;
            if (this.supportingEvidence == 'No') {
                this.showgangDetails = true;
            } else {
                this.showgangDetails = false;
            }
        } else if (event.target.name == 'EvidenceDetails__c') {
            this.evidenceDetails = event.target.value;
        }
    }

    saveGangDetails() {
        this.gangDetailValue.sobjectType = "Case";
        this.gangDetailValue.Id = this.caseId;
        this.gangDetailValue.GangRelated__c = this.gangRelated;
        this.gangDetailValue.SupportingEvidence__c = this.supportingEvidence;
        this.gangDetailValue.EvidenceDetails__c = this.evidenceDetails;
        //this.gangDetailValue.Id = this.gandDetailsID;

        if (this.gandDetailsID) this.isUpdateFlag = true;
        else this.isUpdateFlag = false;
        insertUpdateGangDetails({ saveObjInsert: this.gangDetailValue }).then((result) => {

            if (this.isUpdateFlag)
                this.dispatchEvent(utils.toastMessage("Gang details updated successfully", "success"));
            else
                this.dispatchEvent(utils.toastMessage("Gang details saved successfully", "success"));

        }).catch(errors => {
            let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
            return this.dispatchEvent(utils.handleError(errors, config));
        });
    }
    connectedCallback() {
        if (this.userProfileName === USER_PROFILE_NAME.USER_CASE_WRKR || (this.incidentStatus !== undefined && (this.incidentStatus !=='Pending'))) {
            this.gangFlowBtnDisable = true;
            this.disableBtnsave = true;
        }
        this.gangDetailsLoad();
    }
    //Function used to show the existing GangDetails
    gangDetailsLoad() {
        getGangDetails({ gangId: this.caseId })
            .then((result) => {
                if (result.length > 0) {
                    this.gangRelated = (result[0].GangRelated__c !=undefined) ? result[0].GangRelated__c:'';
                    this.supportingEvidence = (result[0].SupportingEvidence__c !=undefined) ? result[0].SupportingEvidence__c:'';
                    this.evidenceDetails = (result[0].EvidenceDetails__c !=undefined) ? result[0].EvidenceDetails__c:'';
                    this.gandDetailsID = (result[0].Id !=undefined) ? result[0].Id:'';
                    if (this.supportingEvidence == 'No') {
                        this.showgangDetails = true;
                    } else {
                        this.showgangDetails = false;
                    }
                }
            })
            .catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
    }





}