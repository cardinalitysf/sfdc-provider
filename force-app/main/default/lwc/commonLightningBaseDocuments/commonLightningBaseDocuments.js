/**
    * @Author        : K.Sundar
    * @CreatedOn     : AUGUST 17, 2020
    * @Purpose       : This component contains Common File Upload Document for Multiple Records
**/

import { LightningElement, track, api } from 'lwc';
import { TOAST_HEADER_LABELS } from "c/constants";

export default class CommonLightningBaseDocuments extends LightningElement {
    @api recordId;

    // accepted parameters
    get acceptedFormats() {
        return [".pdf", ".png", ".jpg", ".jpeg"];
    }

    handleUploadFinished() {
        const onredirect = new CustomEvent("redirecttoparent", {
            detail: {
                first: true
            }
        });
        this.dispatchEvent(onredirect);
    }
}