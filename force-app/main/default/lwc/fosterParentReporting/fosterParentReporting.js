/**
 * @Author        : Bala Murugan / Saranraj
 * @CreatedOn     : Aug 03 ,2020
 * @Purpose       : Foster Parent
 * @updatedBy     :
 * @updatedOn     :
 **/

import { LightningElement, wire } from "lwc";
import getFosterParentData from "@salesforce/apex/IncidentFosterParentLawEnforcement.getFosterParentData";
import fosterParentTableData from "@salesforce/apex/IncidentFosterParentLawEnforcement.fosterParentTableData";
import ACTOR_OBJECT from "@salesforce/schema/Actor__c";
import CONTACT_FIELD_FROM_ACTOR from "@salesforce/schema/Actor__c.Contact__c";
import REFERRAL_FIELD_FROM_ACTOR from "@salesforce/schema/Actor__c.Referral__c";
import ROLE_FIELD_FROM_ACTOR from "@salesforce/schema/Actor__c.Role__c";

import * as sharedData from "c/sharedData";
import { utils } from "c/utils";
import { refreshApex } from "@salesforce/apex";
import images from "@salesforce/resourceUrl/images";

// for Api
import { getRecord, createRecord, deleteRecord } from "lightning/uiRecordApi";
import USER_ID from "@salesforce/user/Id";
import PROFILE_FIELD from "@salesforce/schema/User.Profile.Name";

export default class FosterParentReporting extends LightningElement {
  fosterParentModal = false;
  fosterParent;
  fosterParentFirstName;
  fosterParentLastName;
  fosterParentState;
  fosterParentCity;
  fosterParentCounty;
  fosterParentZipCode;
  fosterParentAddressLine1;
  fosterParentAddressLine2;
  fosterParentDropDownData = [];
  fosterParentResult;
  fosterParentContactId;
  fosterParentDataTableData = [];
  lawEnformentDataTableData;
  showAddFosterParentBtn = false;
  showFosterParentSection = false;
  userProfileName;
  disableFosterParent = false;
  disableBtnForCaseworker = false;

  showAddBtn = true;
  showFosterParentPreviewModal = false;

  referralId = "";
  cpahomesId = "";
  attachmentIcon = images + "/foster-parentIcon.svg";

  get fosterParentColumn() {
    if (this.userProfileName !== "Caseworker") {
      return [
        {
          label: "First Name",
          fieldName: "firstName",
          cellAttributes: {
            class: "test"
          }
        },
        {
          label: "Last Name",
          fieldName: "lastName",
          cellAttributes: {
            class: "test"
          }
        },
        {
          label: "Address",
          fieldName: "address",
          cellAttributes: {
            class: "test"
          }
        },
        {
          label: "Action",
          type: "button",
          initialWidth: 100,
          fieldName: "id",
          typeAttributes: {
            iconName: "utility:delete",
            name: "Delete",
            title: "Delete",
            class: "del-red ",
            disabled: false,
            target: "_self",
            initialWidth: 20
          }
        }
      ];
    } else {
      return [
        {
          label: "First Name",
          fieldName: "firstName",
          cellAttributes: {
            class: "test"
          }
        },
        {
          label: "Last Name",
          fieldName: "lastName__c",
          cellAttributes: {
            class: "test"
          }
        },
        {
          label: "Address",
          fieldName: "address",
          cellAttributes: {
            class: "test"
          }
        },
        {
          label: "Action",
          fieldName: "id",
          type: "button",
          initialWidth: 90,
          typeAttributes: {
            iconName: "utility:preview",
            name: "Preview",
            title: "Preview",
            initialWidth: 20,
            disabled: false,
            iconPosition: "left",
            target: "_self"
          }
        }
      ];
    }
  }

  connectedCallback() {
    this.getCaseId();
    this.getCPAHomeId();
    this.getFosterParentData();
    this.fosterParentTableDataFromApex();
  }

  getCaseId() {
    this.referralId = sharedData.getCaseId();
    return sharedData.getCaseId();
  }

  @wire(getRecord, {
    recordId: USER_ID,
    fields: [PROFILE_FIELD]
  })
  wireuser({ error, data }) {
    if (error) {
      this.error = error;
    } else if (data) {
      this.userProfileName = data.fields.Profile.displayValue;
      this.disableBtnForCaseworker =
        data.fields.Profile.displayValue === "Caseworker";
    }
  }

  getCPAHomeId() {
    if (sharedData.getCPAHomeId() !== undefined) {
      this.cpahomesId = sharedData.getCPAHomeId();
      this.showFosterParentSection = true;
      return sharedData.getCPAHomeId();
    }
    this.showFosterParentSection = false;
    return null;
  }

  fosterParentTableDataFromApex = async () => {
    try {
      let response = await fosterParentTableData({
        referralId: this.referralId
      });

      let temp = [];

      response &&
        response.map((item) => {
          temp.push({
            Id: item.Id,
            firstName: item.Contact__r && item.Contact__r.FirstName__c,
            lastName__c: item.Contact__r && item.Contact__r.LastName__c,
            lastName: item.Contact__r && item.Contact__r.LastName,
            addressLine1__c: item && item.Referral__r && item.Referral__r.Address__r.AddressLine1__c,
            addressLine2__c: item && item.Referral__r && item.Referral__r.Address__r.AddressLine2__c || "-", 
            city: item && item.Referral__r && item.Referral__r.Address__r.City__c,
            county: item && item.Referral__r && item.Referral__r.Address__r.County__c,
            state: item && item.Referral__r && item.Referral__r.Address__r.State__c,
            zipCode: item && item.Referral__r && item.Referral__r.Address__r.ZipCode__c,
            address: this.fullAddress(
              item.Referral__r && item.Referral__r.Address__r
            )
          });
        });

      this.fosterParentDataTableData = temp;
      this.showAddFosterParentBtn = this.fosterParentDataTableData.length > 0;

    } catch (err) {
      this.dispatchEvent(utils.handleError(err));
    }
  };

  fullAddress = (address) => {
    return `${address.AddressLine1__c},
     ${address.ZipCode__c}`;
  };

  handleOpenModal = () => {
    this.fosterParentModal = true;
    this.fosterParent = "";
    this.fosterParentAddressLine1 = "";
    this.fosterParentAddressLine2 = "";
    this.fosterParentFirstName = "";
    this.fosterParentLastName = "";
    this.fosterParentState = "";
    this.fosterParentCounty = "";
    this.fosterParentCity = "";
    this.fosterParentZipCode = "";
  };

  handleCloseModal = () => {
    this.fosterParentModal = false;
    this.fosterParentTableDataFromApex();
  };

  getFosterParentData = async () => {
    let response = await getFosterParentData({ cpahomesId: this.cpahomesId });
    this.fosterParentResult = response;
    try {
      this.fosterParentDropDownData = response.map((item) => {
        this.fosterParentContactId = item.Contact__c;
        return {
          value: item.Contact__r ? item.Contact__r.LastName : "",
          label: item.Contact__r ? item.Contact__r.LastName : ""
        };
      });
    } catch (err) {
      this.dispatchEvent(utils.handleError(err));
    }
  };

  handleFosterParentOnchange = (event) => {
    let targetName = event.target.name;
    let targetValue = event.target.value;

    if (targetName === "Foster Parent") {
      this.fosterParent = targetValue;
      this.fosterParentResult.map((item) => {
        if (item.Contact__r.LastName === targetValue) {
          this.fosterParentFirstName = item.Contact__r.FirstName__c;
          this.fosterParentLastName = item.Contact__r.LastName__c;
          this.fosterParentAddressLine1 = item.CPAHomes__r
            ? item.CPAHomes__r.Address__r.AddressLine1__c
            : "";
          this.fosterParentAddressLine2 = item.CPAHomes__r
            ? item.CPAHomes__r.Address__r.AddressLine2__c
            : "";
          this.fosterParentState = item.CPAHomes__r
            ? item.CPAHomes__r.Address__r.State__c
            : "";
          this.fosterParentCounty = item.CPAHomes__r
            ? item.CPAHomes__r.Address__r.County__c
            : "";
          this.fosterParentCity = item.CPAHomes__r
            ? item.CPAHomes__r.Address__r.City__c
            : "";
          this.fosterParentZipCode = item.CPAHomes__r
            ? item.CPAHomes__r.Address__r.ZipCode__c
            : "";
        }
      });
    } else if (targetName === "First Name") {
      this.fosterParentFirstName = targetValue;
    } else if (targetName === "Last Name") {
      this.fosterParentLastName = targetValue;
    } else if (targetName === "State") {
      this.fosterParentState = targetValue;
    } else if (targetName === "City") {
      this.fosterParentCity = targetValue;
    } else if (targetName === "County") {
      this.fosterParentCounty = targetValue;
    } else if (targetName === "Zip Code") {
      this.fosterParentZipCode = targetValue;
    } else if (targetName === "Address Line1") {
      this.fosterParentAddressLine1 = targetValue;
    } else if (targetName === "Address Line2") {
      this.fosterParentAddressLine2 = targetValue;
    } else {
      return "";
    }
  };

  // handleSaveFosterParent function start
  handleSaveFosterParent = () => {
    let fields = {};
    fields[CONTACT_FIELD_FROM_ACTOR.fieldApiName] = this.fosterParentContactId;
    fields[REFERRAL_FIELD_FROM_ACTOR.fieldApiName] = this.referralId;
    fields[ROLE_FIELD_FROM_ACTOR.fieldApiName] = "Foster Parent";

    const recordData = {
      apiName: ACTOR_OBJECT.objectApiName,
      fields
    };

    createRecord(recordData)
      .then((_) => {
        this.showAddBtn = true;
        this.fosterParentTableDataFromApex();
        this.fosterParentModal = false;
        this.dispatchEvent(
          utils.toastMessage("Created successfully", "success")
        );
      })
      .catch((err) => {
        this.dispatchEvent(utils.handleError(err));
      });
  };
  // handleSaveFosterParent function end

  handleFosterParantRowAction = (event) => {
    const actionName = event.detail.action.name;
    const row = event.detail.row;
    switch (actionName) {
      case "Delete":
        this.showFosterParentDeleteModal(row);
        break;
      case "Preview":
        this.showAddBtn = false;
        this.showFosterParentPreviewModal(row);
        break;
      default:
    }
  };

  showFosterParentDeleteModal = (row) => {
    deleteRecord(row.Id)
      .then((_) => {
        this.fosterParentTableDataFromApex();
        this.dispatchEvent(
          utils.toastMessage("Deleted Successfully", "success")
        );
      })
      .catch((err) => {
        this.dispatchEvent(utils.handleError(err));
      });
  };

  showFosterParentPreviewModal = (row) => {
    this.fosterParentModal = true;
    this.disableFosterParent = true;

    this.fosterParent = row.lastName;
    this.fosterParentFirstName = row.firstName;
    this.fosterParentLastName = row.lastName__c;
    this.fosterParentAddressLine1 = row.addressLine1__c;
    this.fosterParentAddressLine2 = row.addressLine2__c;
    this.fosterParentState = row.state;
    this.fosterParentCity = row.city;
    this.fosterParentCounty = row.county;
    this.fosterParentZipCode = row.zipCode;
  };
}
