/**
 * @Author        : S. Jayachandran
 * @CreatedOn     : June 01 ,2020
 * @Purpose       : PublicApplication data table, add, edit, del, list For PetInfo.
 **/
import { LightningElement, track, api, wire } from "lwc";
import { utils } from "c/utils";
import SavePetRecordDetails from "@salesforce/apex/PublicApplicationPetInfo.SavePetRecordDetails";
import getApplicationPetInfo from "@salesforce/apex/PublicApplicationPetInfo.getApplicationPetInfo";
import getRecordType from "@salesforce/apex/PublicApplicationPetInfo.getRecordType";
import editPetDetails from "@salesforce/apex/PublicApplicationPetInfo.editPetDetails";
import { deleteRecord } from "lightning/uiRecordApi";
import images from "@salesforce/resourceUrl/images";
import * as sharedData from 'c/sharedData';
import { getRecord } from "lightning/uiRecordApi";
import APPLICATIONSTATUS_FIELD from "@salesforce/schema/Application__c.Status__c";
import { CJAMS_CONSTANTS } from "c/constants";

const actions = [
  { label: 'Edit', name: 'Edit', iconName: 'utility:edit', target: '_self' },
  { label: 'Delete', name: 'Delete', iconName: 'utility:delete', target: '_self' },
  { label: "View", name: "View", iconName: "utility:preview", target: "_self" }
];

const columns = [
  {
    label: "Pet Type",
    fieldName: "PetType__c",
    type: "text",
    initialWidth: 300
  },
  {
    label: "Pet Breed",
    fieldName: "PetBreed__c",
    type: "text",
    initialWidth: 300
  },
  {
    label: "Pet Name",
    fieldName: "LastName",
    type: "text",
    initialWidth: 300
  },
  {
    label: "Age Of Pet",
    fieldName: "ApproximateAge__c",
    type: "number"
  },
  {
    label: "Rabies Certificate Expiry Date",
    fieldName: "RabiesCertificateExpiryDate__c",
    type: "date",
    typeAttributes: {
      month: "2-digit",
      day: "2-digit",
      year: "numeric"
    }
  }
];
export default class PublicApplicationPetInfo extends LightningElement {
  _openPetModal;
  @track columns = columns;
  @track PetName;
  @track PetBreed;
  @track PetType;
  @track PetAge;
  @track ExpiryDate;
  @track recordTypeId;
  @track norecorddisplay = true;
  @track title;
  @track btnLabel;
  @track IdOfPetRecord;
  @track deleteModel = false;
  petIcon = images + "/pet-icon.svg";
  @track ViewHide = true;
  @track disabledView = false;
  @track disableClick = false;
  @track disableDelClick = false;
  //pagination
  @track currentPagePetData;
  @track totalRecordsCount = 0;
  @track page = 1;
  perpage = 9;
  setPagination = 5;
  @track totalRecords;
  @track AssignbtnDisable = false;
  @track ApplicationStatus;
  @track maxdate;

  //openPetModal from Header Button Child comp Through AllTabs Comp
  @api get openPetModal() {
    return this._openPetModal;
  }

  set openPetModal(value) {
    this._openPetModal = value;
    this.title = "ADD PET DETAILS";
    this.IdOfPetRecord = null;
    this.btnLabel = "SAVE";
    this.disabledView = false;
  }

  get getUserProfileName() {
    return sharedData.getUserProfileName();
  }
  get ApplicationId() {
    return sharedData.getApplicationId();
  }

  @wire(getRecord, {
    recordId: "$ApplicationId",
    fields: [APPLICATIONSTATUS_FIELD]
  })
  wireuser({ error, data }) {
    if (error) {
      this.error = error;
    } else if (data) {
      this.ApplicationStatus = data.fields.Status__c.value;
      if (data) {
        if (this.getUserProfileName != "Supervisor") {
          if (
            [
              "Caseworker Submitted",
              "Approved",
              "Rejected",
              "Supervisor Rejected"
            ].includes(this.ApplicationStatus)
          ) {
            this.AssignbtnDisable = true;
          } else {
            this.AssignbtnDisable = false;
          }
        }
        if (this.getUserProfileName == "Supervisor") {
          if (["Approved", "Supervisor Rejected"].includes(this.ApplicationStatus)) {
            this.AssignbtnDisable = true;
          }
        }
      }
    }
    this.PetInfo();
    this.futuredatedisable();
  }
  //Function used to disable the future date in received date field
  futuredatedisable() {
    var today = new Date();
    var monthDigit = today.getMonth() + 1;
    if (monthDigit <= 9) {
      monthDigit = '0' + monthDigit;
    }
    var dayDigit = today.getDate();
    if (dayDigit <= 9) {
      dayDigit = '0' + dayDigit;
    }

    var date = today.getFullYear() + "-" + monthDigit + "-" + dayDigit;
    var finaldate = date;
    this.maxdate = finaldate;
  }
  //closePetModal from Header Button Child comp Through AllTabs Comp
  closePetModal() {
    this._openPetModal = false;
    this.dispatchCustomEventToHandleAddPetInfoPopUp();
  }
  dispatchCustomEventToHandleAddPetInfoPopUp() {
    const tooglecmps = new CustomEvent("togglecomponents", {
      detail: {
        Component: "ClosePetInfo"
      }
    });
    this.dispatchEvent(tooglecmps);
    this.PetType = "";
    this.PetBreed = "";
    this.PetName = "";
    this.PetAge = "";
    this.ExpiryDate = "";
  }


  //Intial Load Data
  PetInfo() {
    getApplicationPetInfo({
      Appid: this.ApplicationId
    })
      .then((result) => {
        this.currentPagePetData = result.map(i => {
          return {
            ApproximateAge__c: i.ApproximateAge__c,
            Id: i.Id,
            LastName: i.LastName,
            PetBreed__c: i.PetBreed__c,
            PetType__c: i.PetType__c,
            RabiesCertificateExpiryDate__c: i.RabiesCertificateExpiryDate__c,
            RecordTypeId: i.RecordTypeId,
            buttonDisabled: (this.AssignbtnDisable) ? true : false
          }
        })
        result = this.currentPagePetData;
        this.totalRecords = result;
        this.totalRecordsCount = this.totalRecords.length;
        if (this.totalRecords.length == 0) {
          this.norecorddisplay = false;
        } else {
          this.norecorddisplay = true;
        }
        this.pageData();
      })
      .catch(errors => {
        let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
        return this.dispatchEvent(utils.handleError(errors, config));
      });
  }

  //OnChange For ModalPopup
  ModalOnChange(event) {
    if (event.target.name == "PetType") {
      this.PetType = event.detail.value;
    } else if (event.target.name == "PetBreed") {
      this.PetBreed = event.target.value;
    } else if (event.target.name == "PetName") {
      this.PetName = event.target.value;
    } else if (event.target.name == "PetAge") {
      this.PetAge = event.target.value;
    } else if (event.target.name == "ExpiryDate") {
      this.ExpiryDate = event.target.value;
    }
  }

  // record type details
  @wire(getRecordType, { name: "Pet" })
  recordTypeDetails(result) {
    try {
      if (result.data) {
        this.recordTypeId = result && result.data;
      }
    } catch (err) {
      this.dispatchEvent(utils.handleError(err));
    }
  }

  //save and Update Function
  handleSave() {
    if (!this.PetType)
      return this.dispatchEvent(
        utils.toastMessage("Pet Type is mandatory", "warning")
      );

    if (!this.PetBreed)
      return this.dispatchEvent(
        utils.toastMessage("Pet Breed is mandatory", "warning")
      );

    if (!this.PetName)
      return this.dispatchEvent(
        utils.toastMessage("Pet Name is mandatory", "warning")
      );

    if (!this.PetAge)
      return this.dispatchEvent(
        utils.toastMessage("Pet Name is mandatory", "warning")
      );

    if (!this.ExpiryDate)
      return this.dispatchEvent(
        utils.toastMessage("Pet Name is mandatory", "warning")
      );

    if (this.ExpiryDate > this.maxdate)
      return this.dispatchEvent(
        utils.toastMessage("Expiry Date Should not be Greater than Current Date", "warning")
      );
    this.disableClick = true;
    let myobjPet = {
      sobjectType: "Contact"
    };

    myobjPet.PetType__c = this.PetType;
    myobjPet.PetBreed__c = this.PetBreed;
    myobjPet.LastName = this.PetName;
    myobjPet.ApproximateAge__c = this.PetAge;
    myobjPet.RabiesCertificateExpiryDate__c = this.ExpiryDate;
    myobjPet.RecordTypeId = this.recordTypeId;
    myobjPet.Application__c = this.ApplicationId;
    myobjPet.Id = this.IdOfPetRecord;

    SavePetRecordDetails({
      objSobjecttoUpdateOrInsert: myobjPet
    })
      .then((result) => {
        this.dispatchEvent(
          utils.toastMessage("Pet Details are updated successfully", "success")
        );
        this.closePetModal();
        this.disableClick = false;
        this.PetInfo();
      })
      .catch(errors => {
        this.disableClick = false;
        let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
        return this.dispatchEvent(utils.handleError(errors, config));
      });
  }

  //Edit  Functinality
  handleRowAction(event) {
    let petid = event.detail.row.Id;
    if (event.detail.action.name == "Edit") {
      this._openPetModal = true;
      this.title = "EDIT PET DETAILS";
      this.btnLabel = "UPDATE";
      this.ViewHide = true;
      this.disabledView = false;
      editPetDetails({
        selectedEditId: petid
      })
        .then((result) => {
          this.IdOfPetRecord = result[0].Id;
          this.PetType = result[0].PetType__c;
          this.PetBreed = result[0].PetBreed__c;
          this.PetName = result[0].LastName;
          this.PetAge = result[0].ApproximateAge__c;
          this.ExpiryDate = result[0].RabiesCertificateExpiryDate__c;
        })
        .catch(errors => {
          let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
          return this.dispatchEvent(utils.handleError(errors, config));
        });
    } else if (event.detail.action.name == "Delete") {
      this.deleteRecordForPet = event.detail.row.Id;
      this.deleteModel = true;
    } else if (event.detail.action.name == "View") {
      this._openPetModal = true;
      this.title = "VIEW PET DETAILS";
      this.ViewHide = false;
      this.disabledView = true;
      editPetDetails({
        selectedEditId: petid
      })
        .then((result) => {
          this.PetType = result[0].PetType__c;
          this.PetBreed = result[0].PetBreed__c;
          this.PetName = result[0].LastName;
          this.PetAge = result[0].ApproximateAge__c;
          this.ExpiryDate = result[0].RabiesCertificateExpiryDate__c;
        })
        .catch(errors => {
          let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
          return this.dispatchEvent(utils.handleError(errors, config));
        });
    }
  }
  //Delete Pet Record
  handleDelete() {
    this.disableDelClick = true;
    deleteRecord(this.deleteRecordForPet)
      .then(() => {
        this.deleteModel = false;
        this.dispatchEvent(
          utils.toastMessage("Pet Details deleted successfully", "success")
        );
        this.disableDelClick = false;
        this.PetInfo();
      })
      .catch((error) => {
        this.disableDelClick = false;
        this.deleteModel = false;
        this.dispatchEvent(
          utils.toastMessage("Warning in delete Pet details", "Warning")
        );
      });
  }
  closeDeleteModal() {
    this.deleteModel = false;
  }

  //pagination  function part
  pageData = () => {
    let page = this.page;
    let perpage = this.perpage;
    let startIndex = page * perpage - perpage;
    let endIndex = page * perpage;
    this.currentPagePetData = this.totalRecords.slice(
      startIndex,
      endIndex
    );
  };
  hanldeProgressValueChange(event) {
    this.page = event.detail;
    this.pageData();
  }

  /**if decision status = approved show only view otherwise show edit and delete begins */
  constructor() {
    super();
    let ViewIcon = true;
    for (let i = 0; i < this.columns.length; i++) {
      if (this.columns[i].type == 'action') {
        ViewIcon = false;
      }
    }
    if (ViewIcon) {
      this.columns.push(
        {
          type: 'action', typeAttributes: { rowActions: this.getRowActions }, cellAttributes: {
            iconName: 'utility:threedots_vertical',
            iconAlternativeText: 'Actions',
            class: "tripledots"
          }
        }
      )
    }
  }
  getRowActions(row, doneCallback) {
    const actions = [];
    if (row['buttonDisabled'] == true) {
      actions.push({
        'label': 'View',
        'name': 'View',
        'iconName': 'utility:preview',
        'target': '_self'
      });
    } else {
      actions.push({
        'label': 'Edit',
        'name': 'Edit',
        'iconName': 'utility:edit',
        'target': '_self'
      },
        {
          'label': 'Delete',
          'name': 'Delete',
          'iconName': 'utility:delete',
          'target': '_self'
        });
    }
    // simulate a trip to the server
    setTimeout(() => {
      doneCallback(actions);
    }, 200);
  }

}
