/**
 * @Author        : V.S.Marimuthu
 * @CreatedOn     : March 09 ,2020
 * @Purpose       : COntains all the utility functions
 **/
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import { CJAMS_CONSTANTS } from 'c/constants';
const titles = {
  success: "Success!..",
  warning: "Warning!..",
  error: "Error!.."
};
export const utils = {
  // Function used to format phone number in US format like (111) 111-1111
  formattedPhoneNumber(phoneNumber) {
    phoneNumber = phoneNumber
      ? "(" +
      phoneNumber.substr(0, 3) +
      ")" +
      " " +
      phoneNumber.substr(3, 3) +
      "-" +
      phoneNumber.substr(6, 4)
      : null;
    return phoneNumber;
  },

  // Function used to format Tax id  in US format like 11-11111111
  formattedTaxID(taxid) {
    taxid = taxid ? taxid.substr(0, 2) + "-" + taxid.substr(2, 9) : null;
    return taxid;
  },
  // Function used to format SSN in US format like 111-11-1111
  formattedSSN(ssn) {
    ssn = ssn
      ? ssn.substr(0, 3) + "-" + ssn.substr(3, 2) + "-" + ssn.substr(5, 8)
      : null;
    return ssn;
  },
  //Function Used to give Toast Messages On Success, Warning and Error
  toastMessage(message, variant) {
    const toast = new ShowToastEvent({
      title: titles[variant],
      message,
      variant
    });
    return toast;
  },
  //Function Used to give Toast Messages On Success, Warning and Error
  toastMessageWithTitle(title, message, variant) {
    const toast = new ShowToastEvent({
      title,
      message,
      variant
    });
    return toast;
  },

  // time formatting function start
  timeFormat(time) {
    if (time !== undefined && time !== "") {
      let date = new Date(time * 1000);
      let hours = date.getHours();
      let minutes = date.getMinutes();

      let ampm = hours >= 12 ? "pm" : "am";
      hours = hours % 12;
      hours = hours ? hours : 12;
      minutes = minutes < 10 ? "0" + minutes : minutes;
      // let strTime = hours + ':' + minutes + ':'  + ' ' + ampm;

      return `${hours}:${minutes}:${ampm}`;
    } else {
      return "";
    }
  },

  // time formatting function end

  // dateFormat function start
  dateFormat(date) {
    if (date !== undefined) {
      let spiltedData = date.split("-");
      return `${spiltedData[1]}/${spiltedData[2]}/${spiltedData[0]}`;
    }
  },
  // dateFormat function end

  //Function Used to Change the Date Format(MM/DD/YYYY) Start
  formatDate(fullDate) {
    if (fullDate !== null && fullDate !== undefined) {
      let recordDate = new Date(fullDate);
      let date =
        recordDate.getDate() > 9
          ? recordDate.getDate()
          : "0" + recordDate.getDate();
      let month =
        recordDate.getMonth() + 1 > 9
          ? recordDate.getMonth() + 1
          : "0" + (recordDate.getMonth() + 1);
      let year = recordDate.getFullYear();
      fullDate = month + "/" + date + "/" + year;
    }
    return fullDate;
  },
  //Function Used to Change the Date Format(MM/DD/YYYY) End

  /*Adding New Time Format Functionality for 24hr/12hr format by Sundar K*/

  //Function Used to Change the 12 hour Time Format(HH:MM AM/PM)
  formatTime12Hr(duration) {
    var milliseconds = parseInt((duration % 1000) / 100),
      seconds = parseInt((duration / 1000) % 60),
      minutes = parseInt((duration / (1000 * 60)) % 60),
      hours = parseInt((duration / (1000 * 60 * 60)) % 24);

    hours = hours < 10 ? "0" + hours : hours;
    minutes = minutes < 10 ? "0" + minutes : minutes;
    seconds = seconds < 10 ? "0" + seconds : seconds;

    var time = hours + ":" + minutes;
    if (time != null && time != undefined) {
      // Check correct time format and split into components
      time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)?$/) || [time];

      if (time.length > 1) {
        // If time format correct
        time = time.slice(1); // Remove full string match value
        time[5] = +time[0] < 12 ? " AM" : " PM"; // Set AM/PM
        time[0] = +time[0] % 12 || 12; // Adjust hours
      }
      return time.join(""); // return adjusted time or original string
    }
  },

  ////Function Used to Change the 24 hour Time Format(HH:MM AM/PM)
  formatTime24Hr(duration) {
    var milliseconds = parseInt((duration % 1000) / 100),
      seconds = parseInt((duration / 1000) % 60),
      minutes = parseInt((duration / (1000 * 60)) % 60),
      hours = parseInt((duration / (1000 * 60 * 60)) % 24);

    hours = hours < 10 ? "0" + hours : hours;
    minutes = minutes < 10 ? "0" + minutes : minutes;
    seconds = seconds < 10 ? "0" + seconds : seconds;

    var time = hours + ":" + minutes + ":00.000";
    return time;
  },
  /*Ending Function*/

  //Handling of Catch Friendly Message Begins
  handleError(errors, config) {
    if (errors && errors.body && errors.body.message) {
      let error = JSON.parse(errors.body.message);
      const { title, message, errorType } = error;
      return this.toastMessageWithTitle(title, message, errorType)
    }
    else if (config == undefined || config == null) {
      return this.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)

    } else {
      return this.toastMessage(config.friendlyErrorMsg, config.errorType)
    }
  },
  //Handling of Catch Friendly Message End

  //Function Used to Change the Date Format(YYYY/MM/DD) Start
  formatDateYYYYMMDD(fullDate) {
    if (fullDate !== null && fullDate !== undefined) {
      let recordDate = new Date(fullDate);
      let date =
        recordDate.getDate() > 9
          ? recordDate.getDate()
          : "0" + recordDate.getDate();
      let month =
        recordDate.getMonth() + 1 > 9
          ? recordDate.getMonth() + 1
          : "0" + (recordDate.getMonth() + 1);
      let year = recordDate.getFullYear();
      fullDate = year + "-" + month + "-" + date;
    }
    return fullDate;
  },
  // For Public Provider Added Session Time Format
  sessionTableTimeFormat(time) {
    let hours = time.split(":")[0];
    let minutes = time.split(":")[1];

    var time = hours + ":" + minutes;
    if (time != null && time != undefined) {
      // Check correct time format and split into components
      time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)?$/) || [time];

      if (time.length > 1) {
        // If time format correct
        time = time.slice(1); // Remove full string match value
        time[5] = +time[0] < 12 ? " AM" : " PM"; // Set AM/PM
        time[0] = +time[0] % 12 || 12; // Adjust hours
      }
      return time.join(""); // return adjusted time or original string
    }
  },
  //Function Used to Change the Date FormatYYYY/MM/DD) End

  /* Get duration from time */
  getTimeDiff(starttime, endtime) {
    var diff = 0;
    if (starttime && endtime) {
      starttime = ConvertToSeconds(starttime);
      endtime = ConvertToSeconds(endtime);
      diff = Math.abs(endtime - starttime);
      var hours = parseInt(diff / 3600);
      return hours;
    }

    function ConvertToSeconds(time) {
      var splitTime = time.split(":");
      return splitTime[0] * 3600 + splitTime[1] * 60;
    }
  },

  //Function for Age Calculation Start
  getAgeByDOB(dob) {
    let diff_ms = Date.now() - new Date(dob).getTime();
    let age_dt = new Date(diff_ms);

    return Math.abs(age_dt.getUTCFullYear() - 1970);
  },
  //Function for Age Calculation End

  //Time Duration Calculation
  calTimeDifference(startTime, endTime) {
    function getTwentyFourHourTime(amPmString) {
      var d = new Date("1/1/2013 " + amPmString);
      return d.getHours() + ":" + d.getMinutes();
    }
    var str1 =
      "Sun Sep 06 2015 " + getTwentyFourHourTime(startTime) + " GMT+04:30";
    var str2 =
      "Sun Sep 06 2015 " + getTwentyFourHourTime(endTime) + " GMT+04:30";
    var date1 = new Date(str1);
    var date2 = new Date(str2);
    if (date2 < date1) {
      date2.setDate(date2.getDate() + 1);
    }
    var diff = date2 - date1;
    var msec = diff;
    var hh = Math.floor(msec / 1000 / 60 / 60);
    msec -= hh * 1000 * 60 * 60;
    var mm = Math.floor(msec / 1000 / 60);
    msec -= mm * 1000 * 60;

    hh = hh <= 9 ? "0" + hh : hh;
    mm = mm <= 9 ? "0" + mm : mm;
    let duration = hh + ":" + mm;

    return duration;
  },

  //Time format conversion
  handle24hrs(tm) {
    var time = tm;
    var hours = Number(time.match(/^(\d+)/)[1]);
    var minutes = Number(time.match(/:(\d+)/)[1]);
    var AMPM = time.match(/\s(.*)$/)[1];
    if (AMPM == "PM" && hours < 12) hours = hours + 12;
    if (AMPM == "AM" && hours == 12) hours = hours - 12;
    var sHours = hours.toString();
    var sMinutes = minutes.toString();
    if (hours < 10) sHours = "0" + sHours;
    if (minutes < 10) sMinutes = "0" + sMinutes;
    return sHours + ":" + sMinutes;
  }
};



export function reduceErrors(errors) {
  if (!Array.isArray(errors)) {
    errors = [errors];
  }

  return (
    errors
      // Remove null/undefined items
      .filter(error => !!error)
      // Extract an error message
      .map(error => {
        // UI API read errors
        if (Array.isArray(error.body)) {
          return error.body.map(e => e.message);
        }
        // UI API DML, Apex and network errors
        else if (error.body && typeof error.body.message === "string") {
          return error.body.message;
        }
        // JS errors
        else if (typeof error.message === "string") {
          return error.message;
        }
        // Unknown error shape so try HTTP status text
        return error.statusText;
      })
      // Flatten
      .reduce((prev, curr) => prev.concat(curr), [])
      // Remove empty strings
      .filter(message => !!message)
  );
}


