/**
 * @Author        : Pratheeba.V
 * @CreatedOn     : August 3, 2020
 * @Purpose       : Parent Component for CPA Search Household Members and Address Details
 **/
import { LightningElement, api } from 'lwc';
import images from "@salesforce/resourceUrl/images";

export default class CpaAddressHouseHoldMembersPath extends LightningElement {
    itemhousehold = false;
    itemsearchmember;
    addHouseholdMember = false;
    // Box click to change image normal to highlight Declaration
    addressDetailsIcon = images + "/address-details.svg";
    householdInactiveIcon = images + "/add-household-member-inactive.svg";
    householdActiveIcon = images + "/add-household-member-active.svg";
    disablePath = true;
    isView = false;
    itemaddress = false;
    searchbreadcrumb = false;
    addnewcpacrumb = true;
    searchcrumbresult = false;
    searchcrumbhousehold = false;
    directtohousehold = false;
    addresspage = false;
    cpaId;
    title;
    addCpiId;
    searchPage;
    @api searchHolderShow = false;
    @api addressAndPathComponent;

    connectedCallback() {
        this.itemaddress = true;
        this.itemhousehold = false;
        this.addHouseholdMember = false;
        this.itemsearchmember = false;
        let a = JSON.parse(this.addressAndPathComponent);
        this.title = a.providertypevalue;        
    }

    boxbordercolorchange() {
        this.template.querySelector(".iconpadbot1").src = this.householdInactiveIcon;
        this.template.querySelector(".iconpadbot").src = this.addressDetailsIcon;
        this.template
            .querySelector(".box-col1")
            .classList.add("boxbordercolorchange");
        this.template
            .querySelector(".box-col3")
            .classList.remove("boxbordercolorchange");
        this.template
            .querySelector(".box-pathline")
            .classList.remove("blue");
        this.itemaddress = true;
        this.itemhousehold = false;
        this.addHouseholdMember = false;
        this.itemsearchmember = false;
    }

    boxbordercolorchangeForHousehold() {
        this.template.querySelector(
            ".iconpadbot1"
        ).src = this.householdActiveIcon;
        this.template.querySelector(".iconpadbot").src = this.addressDetailsIcon;
        this.template
            .querySelector(".box-col3")
            .classList.add("boxbordercolorchange");
        this.template
            .querySelector(".box-col1")
            .classList.remove("boxbordercolorchange");
        this.template
            .querySelector(".box-pathline")
            .classList.add("blue");
        this.itemhousehold = true;
        this.itemaddress = false;
        this.addHouseholdMember = false;
        this.itemsearchmember = false;
    }
    //redirection 
    searchModal(evt) {
        this.itemhousehold = false;
        this.addHouseholdMember = false;
        this.itemsearchmember = evt.detail.first;
        this.disablePath = false;
    }

    closeSearchModal(evt) {
        this.itemhousehold = true;
        this.addHouseholdMember = false;
        this.itemsearchmember = evt.detail.first;
        this.disablePath = true;
        this.searchcrumbresult = false;
        this.addnewcpacrumb = true;
        this.directtohousehold = false;
        this.searchcrumbhousehold = false;
        setTimeout(() => {
            this.boxbordercolorchangeForHousehold()
        }, 500);
    }

    gotoAddHousehold(evt) {
        this.itemhousehold = false;
        this.itemsearchmember = false;
        this.addHouseholdMember = evt.detail.first;
        this.cpaId = evt.detail.cpaId;
        this.searchPage = evt.detail.searchPage;
        this.disablePath = false;
        this.searchHolderShow = false;
        this.isView = false;
        this.searchcrumbresult = false;
        this.addnewcpacrumb = false;
        this.searchcrumbhousehold = true;
        this.directtohousehold = false;
    }

    handleDirectAddHousehold(evt) {
        this.itemhousehold = false;
        this.itemsearchmember = false;
        this.addHouseholdMember = evt.detail.first;
        this.disablePath = false;
        this.searchHolderShow = false;
        this.isView = false;
        this.searchcrumbresult = false;
        this.addnewcpacrumb = false;
        this.searchcrumbhousehold = false;
        this.directtohousehold = true;
    }

    backToPath(evt) {
        this.disablePath = !evt.detail.first;
        this.itemhousehold = false;
        this.itemsearchmember = false;
        this.addHouseholdMember = false;
        this.searchcrumbresult = false;
        this.addnewcpacrumb = true;
        this.searchcrumbhousehold = false;
        this.directtohousehold = false;
        setTimeout(() => {
            this.boxbordercolorchangeForHousehold()
        }, 500);
    }

    editRedirect(evt) {
        this.itemhousehold = false;
        this.itemsearchmember = false;
        this.addHouseholdMember = !evt.detail.first;
        this.disablePath = false;
        this.searchHolderShow = true;
        this.isView = false;
    }

    viewRedirect(evt) {
        this.itemhousehold = false;
        this.itemsearchmember = false;
        this.addHouseholdMember = !evt.detail.isView;
        this.disablePath = false;
        this.isView = true;
    }

    backtoListPage(evt) {
        const onredirect = new CustomEvent("redirecttocpahome", {
            detail: {
                first: true
            }
        });
        this.dispatchEvent(onredirect);
    }

    handleBackToList(evt){
        if(evt.detail.first){
            this.backtoListPage();
        }
    }

    handleCrumbFlag(evt) {
        this.searchcrumbresult = true;
        this.addnewcpacrumb = false;
        this.searchcrumbhousehold = false;
        this.directtohousehold = false;
    }

    handleRedirectAddress(evt) {
        this.addCpiId = this.cpaId;
        this.addresspage = evt.detail.first;
        this.itemaddress = true;
        this.itemhousehold = false;
        this.itemsearchmember = false;
        this.addHouseholdMember = false;
        setTimeout(() => {
            this.boxbordercolorchange()
        }, 500);
        setTimeout(() => {
            this.template
              .querySelector("c-cpa-address-details")
              .AddressEditInfo();
          }, 0);
    }

    handleCpaId(evt){
        this.itemhousehold = true;
        this.cpaId = evt.detail.cpaid;
        setTimeout(() => {
            this.boxbordercolorchangeForHousehold()
        }, 500);
        setTimeout(() => {
            this.template
              .querySelector("c-cpa-house-hold-members-list")
              .getAllContactDetails();
          }, 2000);
    }
    //redirection 
}