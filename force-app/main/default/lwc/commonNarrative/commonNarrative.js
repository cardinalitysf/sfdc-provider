/*
    Author : Balamurugan B
    Modified On : June 04, 2020
    Modified By: Vijayaraj.M
    Modified On : July 06, 2020
*/

import { LightningElement, track, api, wire } from "lwc";

import { utils } from "c/utils";
import * as sharedData from "c/sharedData";
import updateNarrativeDetails from "@salesforce/apex/CommonNarrative.updateNarrativeDetails";
import getHistoryOfNarrativeDetails from "@salesforce/apex/CommonNarrative.getHistoryOfNarrativeDetails";
import getSelectedHistoryRec from "@salesforce/apex/CommonNarrative.getSelectedHistoryRec";
import images from "@salesforce/resourceUrl/images";
import getApplicationProviderStatus from "@salesforce/apex/CommonNarrative.getApplicationProviderStatus";
//import getUserEmail from "@salesforce/apex/CommonNarrative.getUserEmail";
import getRecordType from "@salesforce/apex/CommonNarrative.getRecordType";

import { refreshApex } from "@salesforce/apex";
import { CJAMS_CONSTANTS } from "c/constants";
import { USER_PROFILE_NAME } from "c/constants";

export default class providernarrativeHistory extends LightningElement {
  @track result;
  @track data = [];
  @track narrativeValue;
  @track visibleHistoryData = false;

  @track richTextDisable = false;
  @track btnDisable = false;
  @track openNarrative = true;
  @track btnspeechDisable = false;
  @track RecordTypeId;
  @track userProfile;
  @track isAddButtonHide = false;

  attachmentIcon = images + "/document-newIcon.svg";
  recordId;
  wireNarrativeHistoryDetails;
  //  applicationForApiCall = this.narrativeId;

  freezeNarrativeScreen = false;
  finalStatus;
  // @track Spinner = true;

  @api narrativeId;

  @api sobjectName;
  @api recordType;

  // @api
  // get ApplicationId() {
  //     return sharedData.getApplicationId();
  // }

  // @api
  // get ProviderId() {
  //     return sharedData.getProviderId();
  // }

  get isSuperUserFlow() {
    let userProfile = sharedData.getUserProfileName();
    this.userProfile = sharedData.getUserProfileName();
    if (userProfile === "Supervisor") {
      return true
    }
    else {
      return false
    }
  }

  connectedCallback() {
    this.isSuperUserFlow;
    if (this.recordType != undefined) {
      getRecordType({
          name: this.recordType
      })
          .then((result) => {
              this.RecordTypeId = result;
          }).catch((errors) => {
              return this.dispatchEvent(utils.handleError(errors));
          });
  }
  }

  openNarrativeModel() {
    if (this.freezeNarrativeScreen)
      return this.dispatchEvent(
        utils.toastMessage(
          `Cannot add narrative for ${this.finalStatus} application`,
          "warning"
        )
      );

    this.openNarrative = true;
    this.btnDisable = false;
    this.richTextDisable = false;
    this.btnspeechDisable = false;
    this.narrativeValue = "";
  }

  // //get User Details
  // @wire(getUserEmail)
  // email({
  //     error,
  //     data
  // }) {
  //     if (data) {
  //         let userData = data[0];
  //         if (userData.Profile.Name === 'Supervisor') {
  //             this.userType = 'Supervisor';
  //         } else {
  //             this.userType = 'System';
  //         }
  //     } else if (error) {
  //         this.error = error;
  //     }
  // }

  // @wire(getNarrativeDetails, {
  //     applicationId: '$narrativeId',
  //     sObjectType: '$sobjectName'
  // })
  // wiredNarratvieDetails({
  //     error,
  //     data
  // }) {
  //     if (data) {
  //         if (data.length > 0) {
  //             this.narrativeValue = data[0].Narrative__c;
  //         }
  //        // this.Spinner = false;
  //     }
  // }

  @wire(getHistoryOfNarrativeDetails, {
    applicationId: "$narrativeId"
  })
  wiredNarrativeHistoryDetails(result) {
    this.wireNarrativeHistoryDetails = result;

    if (result.data) {
      if (result.data.length > 0) {
        this.visibleHistoryData = true;

        this.data = result.data.map((row, index) => {
          return {
            Id: row.Id,
            author: row.LastModifiedBy.Name,
            lastModifiedDate: row.LastModifiedDate,
            dataIndex: index
          };
        });
      } else {
        this.visibleHistoryData = false;
      }
    }
  }

  @wire(getApplicationProviderStatus, {
    applicationId: "$narrativeId",
    sObjectType: "$sobjectName"
  })
  wiredApplicationDetails({ error, data }) {

    if (data) {

      if (
        data.length > 0 &&
        CJAMS_CONSTANTS.NARRATIVE_STATUS.includes(data[0].Status__c)
      ) {
        this.btnDisable = true;
        this.richTextDisable = true;
        this.freezeNarrativeScreen = true;
        this.btnspeechDisable = true;
        this.finalStatus = data[0].Status__c;
        this.narrativeValue = data[0].Narrative__c;
      } else {
        this.btnDisable = false;
        this.richTextDisable = false;
        this.freezeNarrativeScreen = false;
        this.btnspeechDisable = false;
        this.finalStatus = null;
        this.narrativeValue = data[0].Narrative__c;
      }

      if (this.sobjectName == CJAMS_CONSTANTS.CASE_OBJECT_NAME) {
        if (this.userProfile == USER_PROFILE_NAME.USER_SYS_ADMIN || this.userProfile == USER_PROFILE_NAME.USER_INTAKE_WRKR) {
          this.finalStatus = data[0].Status;

          //this.narrativeValue = data[0].Description;
          if (
            data[0].Status == CJAMS_CONSTANTS.PROVIDER_APP_FINAL_STATUS_PENDING || data[0].Status == CJAMS_CONSTANTS.REVIEW_STATUS_DRAFT || data[0].Status == CJAMS_CONSTANTS.PROVIDER_APP_FINAL_STATUS_NEW
          ) {
            this.btnDisable = false;
            this.richTextDisable = false;
            this.freezeNarrativeScreen = false;
            this.btnspeechDisable = false;


          }
          else {

            this.btnDisable = true;
            this.richTextDisable = true;
            this.freezeNarrativeScreen = true;
            this.btnspeechDisable = true;
          }
        } else if (this.userProfile == USER_PROFILE_NAME.USER_CASE_WRKR) {
          this.finalStatus = data[0].Status;
          //this.narrativeValue = data[0].Description;
          if (
            data[0].Status ==
            CJAMS_CONSTANTS.APPLICATION_SUBMITTED_TO_SUPERVISOR ||
            data[0].Status ==
            CJAMS_CONSTANTS.APPLICATION_REVIEW_STATUS_APPROVED ||
            data[0].Status == CJAMS_CONSTANTS.APPLICATION_REVIEW_STATUS_REJECTED ||
            data[0].Status == CJAMS_CONSTANTS.COMPLAINT_STATUS_SUBMITTOPROVIDER ||
            data[0].Status == "Accepted" ||
            data[0].Status == "Disputed"
          ) {
            this.btnDisable = true;
            this.richTextDisable = true;
            this.freezeNarrativeScreen = true;
            this.btnspeechDisable = true;

          }
          else {
            this.btnDisable = false;
            this.richTextDisable = false;
            this.freezeNarrativeScreen = false;
            this.btnspeechDisable = false;

          }
        } else if (this.userProfile == USER_PROFILE_NAME.USER_SUPERVISOR) {
          this.finalStatus = data[0].Status;

          //this.narrativeValue = data[0].Description;
          if (
            data[0].Status ==
            CJAMS_CONSTANTS.APPLICATION_REVIEW_STATUS_APPROVED ||
            data[0].Status == CJAMS_CONSTANTS.APPLICATION_REVIEW_STATUS_REJECTED ||
            data[0].Status == CJAMS_CONSTANTS.COMPLAINT_STATUS_SUBMITTOCW ||
            data[0].Status == CJAMS_CONSTANTS.PROVIDER_APP_FINAL_STATUS_NEW ||
            data[0].Status == CJAMS_CONSTANTS.REVIEW_STATUS_DRAFT ||
            data[0].Status == CJAMS_CONSTANTS.PROVIDER_APP_FINAL_STATUS_PENDING
          ) {
            this.btnDisable = true;
            this.richTextDisable = true;
            this.freezeNarrativeScreen = true;
            this.btnspeechDisable = true;
            this.isAddButtonHide = true;
          }
          else {
            this.btnDisable = false;
            this.richTextDisable = false;
            this.freezeNarrativeScreen = false;
            this.btnspeechDisable = false;
            this.isAddButtonHide = false;
          }
        }

      }
    }
  }

  cancelHandler() {
    this.narrativeValue = "";
  }

  handleSpeechToText(event) {
    this.narrativeValue = event.detail.speechValue;
  }
  //Narrative On Change Functionality
  narrativeOnChange(event) {
    this.narrativeValue = event.target.value;
  }

  //Save Functionality
  saveNarrativeDetails() {
    this.template.querySelector("c-common-speech-to-text").stopSpeech();
    let myObj = {
      sObjectType: this.sobjectName
    };

    if (this.sobjectName == CJAMS_CONSTANTS.CASE_OBJECT_NAME) {
      myObj.Description = this.narrativeValue;
      myObj.RecordTypeId = this.RecordTypeId;
    } else {
      myObj.Narrative__c = this.narrativeValue;
    }

    if (this.narrativeId) myObj.Id = this.narrativeId;

    updateNarrativeDetails({
      objSobjecttoUpdateOrInsert: myObj
    })
      .then((result) => {
        this.narrativeValue = "";
        this.btnDisable = false;
        this.dispatchEvent(
          utils.toastMessage(
            "Application Narrative has been saved successfully",
            "success"
          )
        );

        return refreshApex(this.wireNarrativeHistoryDetails);
      }).catch((errors) => {
        this.btnDisable = false;
        return this.dispatchEvent(utils.handleError(errors));
      });

  }

  //Get respective history record
  narrativeHistoryOnClick(event) {
    this.recordId = event.currentTarget.dataset.id;

    let dataIndex = event.currentTarget.dataset.value;
    let datas = this.template.querySelectorAll(".divHighLight");

    for (let i = 0; i < datas.length; i++) {
      if (i == dataIndex)
        datas[i].className =
          "slds-nav-vertical__item divHighLight slds-is-active";
      else datas[i].className = "slds-nav-vertical__item divHighLight";
    }

    getSelectedHistoryRec({
      recId: this.recordId
    }).then((result) => {
      if (result.length > 0 && result[0].RichNewValue__c) {
        this.narrativeValue = result[0].RichNewValue__c;
        this.btnDisable = true;
        this.richTextDisable = true;
        this.btnspeechDisable = true;
      } else {
        this.narrativeValue = null;
      }
    }).catch((errors) => {
      return this.dispatchEvent(utils.handleError(errors));
    });
  }
}
