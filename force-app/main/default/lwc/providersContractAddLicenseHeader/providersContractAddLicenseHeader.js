/**
 * @Author        : S. Jayachandran
 * @CreatedOn     : April 15, 2020
 * @Purpose       : AddContractLicense in contract for license module.
 * @updatedBy     :
 * @updatedOn     :
 **/
import {
  LightningElement,
  track,
  wire
} from "lwc";
import getProgramNameList from "@salesforce/apex/ProvidersContractAddLicenseInfo.ProgramList";
import getLicensesId from "@salesforce/apex/ProvidersContractAddLicenseInfo.getLicensesId";
import getIdAfterInsertSOQL from "@salesforce/apex/ProvidersContractAddLicenseInfo.getIdAfterInsertSOQL";
import providerContract from "@salesforce/apex/ProvidersContractAddLicenseInfo.providerContract";
import * as shareddata from "c/sharedData";
import images from "@salesforce/resourceUrl/images";
import {
  utils
} from "c/utils";
import { CJAMS_CONSTANTS } from "c/constants";

const columns = [{
  label: "LICENSE NO",
  fieldName: "Name",
  type: "text",
  sortable: true
},
{
  label: "LICENSE TYPE",
  fieldName: "ProgramType",
  type: "text",
  sortable: true
},
{
  label: "SITE ID",
  fieldName: "SiteName",
  type: "Text",
  sortable: true
},
{
  label: "PROVIDER NAME",
  fieldName: "ProviderName",
  type: "text",
  sortable: true
},
{
  label: "ADDRESS TYPE",
  fieldName: "AddressType__c",
  type: "text",
  initialWidth: 350,
  sortable: true
}
];

export default class ProvidersContractAddLicenseHeader extends LightningElement {
  @track columns = columns;
  @track applicationidFromProgram;
  @track optionsLicenseProgram;
  @track currentPageLicenseData;
  @track Select1 = true;
  @track Select2 = false;
  @track Select3 = true;
  @track Select4 = false;
  @track LicenseStartDate = "";
  @track LicenseEndDate = "";
  @track LicenceId = "";
  @track defaultSortDirection = "asc";
  @track sortDirection = "asc";
  @track sortedBy = "Name";
  @track SelectedProgram;
  refreshActivity;
  @track OnLoadLicenseContract;
  @track ParentTochild;
  ProviderforApiCall = this.providerId;
  attachmentIcon = images + "/application-licenseinfo.svg";
  @track SelectedProgramName;
  @track isDisable = false;
  @track isDisableProceed = true;
  @track LicenceIdEdit;
  @track ContractRowID = [];

  get providerId() {
    let data = shareddata.getProviderId();
    return data;
  }
  get contractAction() {
    let data = shareddata.getContractAction();
    return data;
  }

  //License List Loading According to edit and view and filtering the contract
  loadLicenseList() {
    if (
      this.contractAction == "editRecordRow" ||
      this.contractAction == "viewRecordRow"
    ) {
      var Editarray = [];
      this.LicenceIdEdit = shareddata.getLicenseId();
      for (var i = 0; i < this.SelectedProgram.length; i++) {
        if (this.SelectedProgram[i].LicenceId == this.LicenceIdEdit) {
          Editarray.push(this.SelectedProgram[i]);
        }
      }
      this.currentPageLicenseData = Editarray;
      this.SelectedProgramName = Editarray[0].ProgramName;
      this.Select1 = false;
      this.Select2 = true;
      this.Select3 = true;
      this.Select4 = true;
      this.isDisable = true;
      this.isDisableProceed = true;
      this.ContractRowID[0] = this.LicenceIdEdit;
    } else {
      this.LicenceIdEdit = null;
      providerContract({
        contract: this.providerId
      }).then((result) => {
        this.OnLoadLicenseContract = result;
        var exitarrayFilter1 = [];
        var exitarrayFilter2 = [];
        for (var i = 0; i < this.SelectedProgram.length; i++) {
          let x = true;
          for (var j = 0; j < this.OnLoadLicenseContract.length; j++) {
            if (
              this.SelectedProgram[i].LicenceId ==
              this.OnLoadLicenseContract[j].License__c
            ) {
              exitarrayFilter1.push(this.SelectedProgram[i]);
              x = false;
            }
          }
          if (x) {
            exitarrayFilter2.push(this.SelectedProgram[i]);
          }
        }
        this.SelectedProgram = exitarrayFilter2;
      }).catch(errors => {
        let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
        return this.dispatchEvent(utils.handleError(errors, config));
      })
    }
  }

  //Program Options data function
  @wire(getProgramNameList, {
    objInfo: {
      sobjectType: "Application__c"
    },
    ProviderId: "$ProviderforApiCall"
  })
  wiredProgramDetails({
    error,
    data
  }) {
    if (data) {
      this.applicationidFromProgram = [...data];
      let unique = [...new Set(data.map((arr) => arr.Program__c))];
      var optionsLicenseProgram = [];
      unique.forEach((key) => {
        optionsLicenseProgram.push({
          label: key,
          value: key
        });
      });
      this.optionsLicenseProgram = optionsLicenseProgram;
      this.getLicensesFromApplication();
    } else if (error) { }
  }

  //Data from Application According to Program
  getLicensesFromApplication() {

    let applicationStringId = this.applicationidFromProgram.map(e => e.Id).join(",");
    getLicensesId({
      applicationID: applicationStringId
    })
      .then((data) => {
        this.currentPageLicenseData = data.map((item) => {
          return {
            Name: item.Licences__r[0].Name,
            ProgramType: item.Licences__r[0].ProgramType__c,
            SiteName: item.Address__r[0].Name,
            ProviderName: item.Provider__r.Name,
            AddressType__c: item.Address__r[0].AddressType__c,
            ProgramName: item.Licences__r[0].ProgramName__c,
            StartDate__c: item.Licences__r[0].StartDate__c,
            EndDate__c: item.Licences__r[0].EndDate__c,
            LicenceId: item.Licences__r[0].Id,
            Id: item.Licences__r[0].Id
          };
        });
        this.SelectedProgram = [...this.currentPageLicenseData];
        this.loadLicenseList();
      })
      .catch(errors => {
        let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
        return this.dispatchEvent(utils.handleError(errors, config));
      });
  }

  // datatable selectedRows for License
  handleRowSelection = (event) => {
    this.ContractRowID = [];
    if (
      this.contractAction !== "editRecordRow" &&
      this.contractAction !== "viewRecordRow"
    ) {
      this.isDisable = false;
      this.isDisableProceed = false;
    } else {
      this.isDisable = true;
      this.isDisableProceed = true;
    }
    this.LicenseStartDate = event.detail.selectedRows[0].StartDate__c;
    this.LicenseEndDate = event.detail.selectedRows[0].EndDate__c;
    this.LicenceId = event.detail.selectedRows[0].LicenceId;
  };

  //Proceed For License Info page
  handleProceed() {
    this.isDisable = false;
    this.isDisableProceed = true;
    let myobjcase = {
      sobjectType: "Contract__c"
    };
    myobjcase.ContractStatus__c = "New";
    getIdAfterInsertSOQL({
      objIdSobjecttoUpdateOrInsert: myobjcase
    })
      .then((result) => {
        const selectedEvent = new CustomEvent("progressvaluechange", {
          detail: {
            ContractID: result,
            LicenseStartDate: this.LicenseStartDate,
            LicenseEndDate: this.LicenseEndDate,
            LicenseID: this.LicenceId,
            isDisable: false,
          }
        });
        this.dispatchEvent(selectedEvent);
      })
      .catch(errors => {
        let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
        return this.dispatchEvent(utils.handleError(errors, config));
      });
  }

  //combobox program selection
  handleChange(event) {
    this.SelectedProgramName = event.target.value;
    if (this.SelectedProgram != undefined && this.SelectedProgram.length > 0) {
      var exitarray = [];
      for (var i = 0; i < this.SelectedProgram.length; i++) {
        if (this.SelectedProgram[i].ProgramName == event.target.value) {
          exitarray.push(this.SelectedProgram[i]);
        }
        this.currentPageLicenseData = exitarray;
        if (exitarray.length == 0) {
          this.Select1 = true;
          this.Select4 = false;
        } else {
          this.Select1 = false;
          this.Select2 = true;
          this.Select4 = true;
          this.Select3 = true;
        }
      }
    } else {
      this.dispatchEvent(utils.toastMessage("No License Available for the Selected Program", "warning"));
    }
  }

  //Table sort function here

  sortBy(field, reverse, primer) {
    const key = primer ?
      function (x) {
        return primer(x[field]);
      } :
      function (x) {
        return x[field];
      };

    return function (a, b) {
      a = key(a);
      b = key(b);
      if (a === undefined) a = "";
      if (b === undefined) b = "";
      a = typeof a === "number" ? a : a.toLowerCase();
      b = typeof b === "number" ? b : b.toLowerCase();

      return reverse * ((a > b) - (b > a));
    };
  }

  onHandleSort(event) {
    const {
      fieldName: sortedBy,
      sortDirection
    } = event.detail;
    const cloneData = [...this.currentPageLicenseData];
    cloneData.sort(this.sortBy(sortedBy, sortDirection === "asc" ? 1 : -1));
    this.currentPageLicenseData = cloneData;
    this.sortDirection = sortDirection;
    this.sortedBy = sortedBy;
  }
}