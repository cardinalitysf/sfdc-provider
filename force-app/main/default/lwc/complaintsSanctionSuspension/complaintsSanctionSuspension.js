/**
 * @Author        : Sundar Karuppalagu
 * @CreatedOn     : JULY 05 ,2020
 * @Purpose       : Sanctions Suspension Screen
 * @UpdatedBy     : Sundar K -> JULY 23, 2020 -> Complaints License Details method added and Sanction ID stored in Complaint Object. 
 **/

import { LightningElement, track, wire, api } from 'lwc';

import getRecordType from "@salesforce/apex/complaintsSanctionSuspension.getRecordType";
import getComplaintLicenseDetails from "@salesforce/apex/complaintsSanctionSuspension.getComplaintLicenseDetails";
import getSelectedSanctionData from "@salesforce/apex/complaintsSanctionSuspension.getSelectedSanctionData";

import * as sharedData from "c/sharedData";
import { utils } from "c/utils";
import { CJAMS_CONSTANTS, BUTTON_LABELS, TOAST_HEADER_LABELS, DATATABLE_BUTTON_LABELS } from "c/constants";

import { createRecord, updateRecord } from "lightning/uiRecordApi";
import { getObjectInfo, getPicklistValuesByRecordType } from "lightning/uiObjectInfoApi";

//Sanction Object fields declaration
import SANCTIONS_OBJECT from "@salesforce/schema/Sanctions__c";
import ID_FIELD from "@salesforce/schema/Sanctions__c.Id";
import STARTDATE_FIELD from "@salesforce/schema/Sanctions__c.StartDate__c";
import ENDDATE_FIELD from "@salesforce/schema/Sanctions__c.EndDate__c";
import DOCUMENT_FIELD from "@salesforce/schema/Sanctions__c.Document__c";
import DELIVERY_FIELD from "@salesforce/schema/Sanctions__c.Delivery__c";
import SUSPENSION_FIELD from "@salesforce/schema/Sanctions__c.Suspension__c";
import COMPLAINT_FIELD from "@salesforce/schema/Sanctions__c.Complaint__c";
import PROVIDER_FIELD from "@salesforce/schema/Sanctions__c.Provider__c";
import RECORDTYPE_FIELD from "@salesforce/schema/Sanctions__c.RecordTypeId";

//Complaint Object Fields declaration
import SANCTION_FIELD from "@salesforce/schema/Case.Sanctions__c";
import COMPLAINTID_FIELD from "@salesforce/schema/Case.Id";

//Class name declaration in lwc
export default class ComplaintsSanctionSuspension extends LightningElement {
    //Fields declaration
    @track suspensionDetails = {};
    @track minDate = '';
    @track licenseStartDateHelpText = '';
    @track licenseEndDateHelpText = '';
    @track btnLabel = '';
    @track showDelivery = false;
    @track Spinner = true;

    //Disable fields using flags
    @track isStartDateDisable = false;
    @track isEndDateDisable = false;
    @track isDocumentDisable = false;
    @track isDeliveryDisable = false;
    @track isSaveBtnDisable = false;

    //Picklist value Initialization
    @track documentOptions = [];
    @track deliveryOptions = [];

    //Local value declaration
    _recordTypeId;
    _licenseStartDate;
    _licenseEndDate;
    _actionName;
    _actionErrorName;
    error;

    //Global value declaration
    @api selectedSanctionId;
    @api sanctionActionName;
    @api sanctionPageType;

    //get Complaint ID
    get complaintId() {
        return sharedData.getCaseId();
    }

    //get Provider ID
    get providerId() {
        return sharedData.getProviderId();
    }

    //Declare sanction object into one variable - Wire to a property
    @wire(getObjectInfo, { objectApiName: SANCTIONS_OBJECT })
    objectInfo;

    //Function used to get all picklist values from sanction object - Wire to a function
    @wire(getPicklistValuesByRecordType, {
        objectApiName: SANCTIONS_OBJECT,
        recordTypeId: '$objectInfo.data.defaultRecordTypeId'
    })
    getAllPicklistValues({ error, data }) {
        if (data) {
            this.error = null;

            // Document Field Picklist values
            this.documentOptions = data.picklistFieldValues.Document__c.values.map(key => {
                return {
                    label: key.label,
                    value: key.value
                }
            });

            // Delivery Field Picklist values
            this.deliveryOptions = data.picklistFieldValues.Delivery__c.values.map(key => {
                return {
                    label: key.label,
                    value: key.value
                }
            });
        } else if (error) {
            this.error = JSON.stringify(error);
            this.dispatchEvent(utils.toastMessage("Picklist values are failed to load. please check", TOAST_HEADER_LABELS.TOAST_WARNING));
        }
    }

    //Connected Call Back method
    connectedCallback() {
        this.btnLabel = !this.selectedSanctionId ? BUTTON_LABELS.BTN_SAVE : BUTTON_LABELS.BTN_UPDATE;
        this.isEndDateDisable = !this.suspensionDetails.EndDate__c ? true : false;

        this.handleRecordType();
    }

    //Handle Record Type method
    handleRecordType() {
        //Fetch Record Type for Sanction Object
        getRecordType({
            name: this.sanctionPageType
        })
        .then(result => {
            if (!result) {
                this.Spinner = false;
                return this.dispatchEvent(utils.toastMessage("Sanction Record Type failed to load. please check", TOAST_HEADER_LABELS.TOAST_WARNING));
            }

            this._recordTypeId = result;
            this.handleComplaintLicenseDetails();
        })
        .catch(errors => {
            this.Spinner = false;
            return this.dispatchEvent(utils.handleError(errors));
        });
    }

    //Handle complaint license method
    handleComplaintLicenseDetails() {
        //Fetch Complaint License Details
        getComplaintLicenseDetails({
            complaintId: this.complaintId
        })
        .then(result => {
            if (result.length == 0) {
                this.Spinner = false;
                return this.dispatchEvent(utils.toastMessage("Complaint License details failed to load. please check", TOAST_HEADER_LABELS.TOAST_WARNING));
            }

            this._licenseStartDate = result[0].License__r ? result[0].License__r.StartDate__c : null;
            this._licenseEndDate = result[0].License__r ? result[0].License__r.EndDate__c : null;
            this.licenseStartDateHelpText = `License Start Date is ${utils.formatDate(this._licenseStartDate)}`;
            this.licenseEndDateHelpText = `License End Date is ${utils.formatDate(this._licenseEndDate)}`;

            if (!this.selectedSanctionId)
                this.Spinner = false;
            else
                this.handleSuspensionData();
        })
        .catch(errors => {
            this.Spinner = false;
            return this.dispatchEvent(utils.handleError(errors));
        })
    }

    //Handle Suspension Data after ID generated
    handleSuspensionData() {
        if ([DATATABLE_BUTTON_LABELS.BTN_EDIT, DATATABLE_BUTTON_LABELS.BTN_VIEW].includes(this.sanctionActionName)) {
            //Fetch selected sanction id data
            getSelectedSanctionData({
                sanctionId: this.selectedSanctionId
            })
            .then(result => {
                if (result.length == 0) {
                    this.Spinner = false;
                    return this.dispatchEvent(utils.toastMessage("Error in fetching suspension data. please check", TOAST_HEADER_LABELS.TOAST_WARNING));
                }

                try {
                    this.suspensionDetails.StartDate__c = result[0].StartDate__c;
                    this.suspensionDetails.EndDate__c = result[0].EndDate__c;
                    this.suspensionDetails.Document__c = result[0].Document__c;
                    this.suspensionDetails.Delivery__c = result[0].Delivery__c;
    
                    this.showDelivery = ['RCC Moratorium', 'RCC Notice Intent Revoke'].includes(result[0].Document__c) ? true : false;

                    this.handleFieldsDisable();
                } catch (error) {
                    this.error = JSON.stringify(error);
                    this.Spinner = false;
                }
            })
            .catch(errors => {
                this.Spinner = false;
                return this.dispatchEvent(utils.handleError(errors));
            });
        } else
            this.dispatchEvent(utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, TOAST_HEADER_LABELS.TOAST_WARNING));
    }

    //Handle fields disable method
    handleFieldsDisable() {
        try {
            if (this.sanctionActionName == DATATABLE_BUTTON_LABELS.BTN_VIEW) {
                this.isStartDateDisable = true;
                this.isEndDateDisable = true;
                this.isDocumentDisable = true;
                this.isDeliveryDisable = true;
                this.isSaveBtnDisable = true;
            } else {
                this.isStartDateDisable = false;
                this.isEndDateDisable = false;
                this.isDocumentDisable = false;
                this.isDeliveryDisable = false;
                this.isSaveBtnDisable = false;
            }
            this.Spinner = false;
        } catch(error) {
            this.error = JSON.stringify(error);
            this.Spinner = false;
        }
    }

    //Fields On Change method
    suspensionOnChange(event) {
        switch(event.target.name) {
            case 'StartDate': 
                this.isEndDateDisable = false;
                this.suspensionDetails.StartDate__c = event.target.value;
                this.suspensionDetails.EndDate__c = null;
                this.minDate = event.target.value;
                break;
            case 'EndDate' : 
                this.suspensionDetails.EndDate__c = event.target.value;
                break;
            case 'Document' : 
                this.suspensionDetails.Document__c = event.detail.value;
                this.suspensionDetails.Delivery__c = null;
                this.showDelivery = ['RCC Moratorium', 'RCC Notice Intent Revoke'].includes(event.detail.value) ? true : false;
                break;
            case this.showDelivery === true && 'Delivery' : 
                this.suspensionDetails.Delivery__c = event.detail.value;
                break;
        }
    }

    //Handle Common Method for Save/Update sanction
    handleSaveMethod() {
        if (!this._recordTypeId)
            return this.dispatchEvent(utils.toastMessage("Sanction Record Type failed to load. please check", TOAST_HEADER_LABELS.TOAST_WARNING));

        if (!this._licenseStartDate || !this._licenseEndDate)
            return this.dispatchEvent(utils.toastMessage("Complaint License dates are failed to load. please check", TOAST_HEADER_LABELS.TOAST_WARNING));

        if (!this.suspensionDetails.StartDate__c)
            return this.dispatchEvent(utils.toastMessage("Please Select Start Date", TOAST_HEADER_LABELS.TOAST_WARNING));

        if (!this.suspensionDetails.EndDate__c)
            return this.dispatchEvent(utils.toastMessage("Please Select End Date", TOAST_HEADER_LABELS.TOAST_WARNING));

        if (!this.suspensionDetails.Document__c)
            return this.dispatchEvent(utils.toastMessage("Please Select Document", TOAST_HEADER_LABELS.TOAST_WARNING));

        if (this.showDelivery === true && !this.suspensionDetails.Delivery__c)
            return this.dispatchEvent(utils.toastMessage("Please Select Delivery", TOAST_HEADER_LABELS.TOAST_WARNING));

        if (new Date(this.suspensionDetails.StartDate__c).getTime() < new Date(this._licenseStartDate).getTime())
            return this.dispatchEvent(utils.toastMessage("Start date should be greater than License Start Date", TOAST_HEADER_LABELS.TOAST_WARNING));

        if (new Date(this.suspensionDetails.EndDate__c).getTime() > new Date(this._licenseEndDate).getTime())
            return this.dispatchEvent(utils.toastMessage("End date should be lesser than License End Date", TOAST_HEADER_LABELS.TOAST_WARNING));

        if (new Date(this.suspensionDetails.StartDate__c).getTime() > new Date(this.suspensionDetails.EndDate__c).getTime())
            return this.dispatchEvent(utils.toastMessage("End Date should be greater than or equal to Start Date", TOAST_HEADER_LABELS.TOAST_WARNING));

        this.isSaveBtnDisable = true;
        this.Spinner = true;

        const fields = {};

        fields[STARTDATE_FIELD.fieldApiName] = this.suspensionDetails.StartDate__c;
        fields[ENDDATE_FIELD.fieldApiName] = this.suspensionDetails.EndDate__c;
        fields[DOCUMENT_FIELD.fieldApiName] = this.suspensionDetails.Document__c;
        fields[DELIVERY_FIELD.fieldApiName] = this.suspensionDetails.Delivery__c;

        this._actionName = this.btnLabel == BUTTON_LABELS.BTN_SAVE ? 'saved' : 'updated';
        this._actionErrorName = this.btnLabel == BUTTON_LABELS.BTN_SAVE ? 'saving' : 'updating';

        if (!this.selectedSanctionId) {
            fields[COMPLAINT_FIELD.fieldApiName] = this.complaintId;
            fields[PROVIDER_FIELD.fieldApiName] = this.providerId;
            fields[RECORDTYPE_FIELD.fieldApiName] = this._recordTypeId;
            fields[SUSPENSION_FIELD.fieldApiName] = true;

            const recordInput = {
                apiName: SANCTIONS_OBJECT.objectApiName,
                fields
            };

            createRecord(recordInput)
            .then(result => {
                this.selectedSanctionId = result.id;
                this.updateComplaintRecord();
            })
            .catch(error => {
                this.error = JSON.stringify(error);
                this.Spinner = false;
                this.isSaveBtnDisable = false;
                this.dispatchEvent(utils.toastMessage(`Error in ${this._actionErrorName} suspension sanction details. please check`, TOAST_HEADER_LABELS.TOAST_WARNING));
            })
        } else {
            fields[ID_FIELD.fieldApiName] = this.selectedSanctionId;

            const recordInput = {
                fields
            };

            updateRecord(recordInput)
            .then(() => {
                this.dispatchEvent(utils.toastMessage(`Suspension sanction has been ${this._actionName} successfully`, TOAST_HEADER_LABELS.TOAST_SUCCESS));
                this.handleRedirectionMethod();
            })
            .catch(error => {
                this.error = JSON.stringify(error);
                this.Spinner = false;
                this.isSaveBtnDisable = false;
                this.dispatchEvent(utils.toastMessage(`Error in ${this._actionErrorName} suspension sanction details. please check`, TOAST_HEADER_LABELS.TOAST_WARNING));
            })
        }
    }

    //Update Complaint record - sanction ID
    updateComplaintRecord() {
        const fields = {};
        fields[SANCTION_FIELD.fieldApiName] = this.selectedSanctionId;
        fields[COMPLAINTID_FIELD.fieldApiName] = this.complaintId;

        const complaintRecordInput = {
            fields
        };

        updateRecord(complaintRecordInput)
        .then(() => {
            this.dispatchEvent(utils.toastMessage(`Suspension sanction has been ${this._actionName} successfully`, TOAST_HEADER_LABELS.TOAST_SUCCESS));
            this.handleRedirectionMethod();
        })
        .catch(error => {
            this.error = JSON.stringify(error);
            this.Spinner = false;
            this.isSaveBtnDisable = false;
            this.dispatchEvent(utils.toastMessage(`Error in ${this._actionErrorName} suspension sanction details. please check`, TOAST_HEADER_LABELS.TOAST_WARNING));
        })
    }

    //Handle Cancel method
    handleCancelMethod() {
        this.Spinner = true;

        //1 Second Interval
        setTimeout(() => {
            this.handleRedirectionMethod();
        }, 1000);   
    }

    //Handle Sanction Redirection method
    handleRedirectionMethod() {
        this.Spinner = false;
        this.suspensionDetails = {};
        this.isSaveBtnDisable = false;

        const oncomplaintid = new CustomEvent('redirecttocomplaintsanctions', {
            detail: {
                first: true
            }
        });
        this.dispatchEvent(oncomplaintid);
    }
}