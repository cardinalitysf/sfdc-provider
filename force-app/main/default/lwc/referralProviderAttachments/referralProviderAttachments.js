import {
    LightningElement,
    track

} from 'lwc';
import * as referralsharedDatas from "c/sharedData";
import {
    CJAMS_CONSTANTS
} from 'c/constants';


export default class ReferralProviderAttachments extends LightningElement {
    get referralID() {
        return referralsharedDatas.getCaseId();
    }
    get sobjectNameToFetch() {
        return CJAMS_CONSTANTS.CASE_OBJECT_NAME;
    }
}