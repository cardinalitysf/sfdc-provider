/**
 * @Author        : G.sathishkumar
 * @CreatedOn     : April 4, 2020
 * @Purpose       : applicationStaff
 * @updatedBy     :
 * @updatedOn     :
 **/

import { LightningElement, track } from 'lwc';
import getStaffFromProviderID from "@salesforce/apex/applicationStaff.getStaffFromProviderID";
import getStaffFromApplicationId from "@salesforce/apex/applicationStaff.getStaffFromApplicationId";
import * as appsharedData from "c/sharedData";
import images from '@salesforce/resourceUrl/images';
import { CJAMS_CONSTANTS} from "c/constants";

//start stylesheet
//import { loadStyle } from 'lightning/platformResourceLoader';
//import myResource from '@salesforce/resourceUrl/styleSheet';
//end stylesheet

const columns = [

    { label: 'First Name', fieldName: 'FirstName__c', type: 'text' },
    { label: 'Last Name', fieldName: 'LastName__c', type: 'text' },
    { label: 'Affiliation Type', fieldName: 'AffiliationType__c', type: 'text' },
    { label: 'Job Title', fieldName: 'JobTitle__c', type: 'text' },
    { label: 'Employee Type', fieldName: 'EmployeeType__c', type: 'text' },

];


export default class ApplicationStaff extends LightningElement {

    @track columns = columns;
    @track Stafftableshow = [];
    @track staffDataItem;
    @track staffDataItem1;

    //Pagination Tracks
    @track totalRecordsCount = 0;
    @track page = 1;
    perpage = 10;
    setPagination = 5;
    @track totalRecords;
    attachmentIcon = images + '/application-staff.svg';

    //norecorddisplay
    @track norecorddisplay = true;
    @track Spinner = true;

    // start stylesheet
    /*renderedCallback() {
        Promise.all([
            loadStyle(this, myResource + '/styleSheet.css')
        ])
    }*/
    //  end stylesheet


    get ProviderId() {
        return appsharedData.getProviderId();
    }
    get applicationId() {
        return appsharedData.getApplicationId();
    }


    connectedCallback() {
        this.initialStaffMemberLoad();
    }

    initialStaffMemberLoad() {
        getStaffFromProviderID({ StaffproId: this.ProviderId }).then(result => {
            this.staffDataItem = result;
            this.AccountId = this.ProviderId;
            this.staffload1();

        }).catch(errors => {
            let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
            return this.dispatchEvent(utils.handleError(errors, config));
        });
    }
    staffload1() {
        getStaffFromApplicationId({ getcontactfromappId: this.applicationId }).then(result => {
            this.staffDataItem1 = result;
            this.staffload2();
        }).catch(errors => {
            let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
            return this.dispatchEvent(utils.handleError(errors, config));
        });

    }

    staffload2() {
        let assignDataArray = [];

        this.staffDataItem.forEach(assign => {
            this.staffDataItem1.forEach(assign1 => {
                if (assign1.Staff__c == assign.Id) {
                    assignDataArray.push(assign);

                }
            });

        });

        this.totalRecords = assignDataArray;
        this.totalRecordsCount = this.totalRecords.length;

        if (this.totalRecords.length == 0) {
            this.norecorddisplay = false;

        } else {
            this.norecorddisplay = true;
        }
        this.pageData();
        this.Spinner = false;

    }

    pageData = () => {
        let page = this.page;
        let perpage = this.perpage;
        let startIndex = (page * perpage) - perpage;
        let endIndex = (page * perpage);
        this.Stafftableshow = this.totalRecords.slice(startIndex, endIndex);
    }
    //For Pagination Child Bind
    hanldeProgressValueChange(event) {
        this.page = event.detail;
        this.pageData();

    }

}