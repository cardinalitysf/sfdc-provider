/* eslint-disable getter-return */
/* eslint-disable consistent-return */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-return-assign */
/* eslint-disable eqeqeq */
// /**
//  * @Author        : Janaswini S
//  * @CreatedOn     : June 17, 2020
//  * @Purpose       : Public Providers's Home Information
//  **/

import { LightningElement, track, wire, api } from 'lwc';
import getMultiplePicklistValues from "@salesforce/apex/PublicProvidersHomeInformation.getMultiplePicklistValues";
import fetchHomeInformation from "@salesforce/apex/PublicProvidersHomeInformation.fetchHomeInformation";
import images from "@salesforce/resourceUrl/images";
import * as sharedData from 'c/sharedData';
import homeSlider from "@salesforce/resourceUrl/HomeSlider";

import {
  createRecord,
  updateRecord
} from "lightning/uiRecordApi";
import HOME_INFORMATION_OBJECT from '@salesforce/schema/HomeInformation__c';
import PROVIDER_HOME_OBJECT_FIELD from '@salesforce/schema/HomeInformation__c.Provider__c';
import CASE_HOME_OBJECT_FIELD from '@salesforce/schema/HomeInformation__c.Referral__c';
import ID_HOME_INFORMATION_FIELD from '@salesforce/schema/HomeInformation__c.Id';

import ADDRESS_INFORMATION_OBJECT from '@salesforce/schema/Address__c';
import PROVIDER_ADDRESS_OBJECT_FIELD from '@salesforce/schema/Address__c.Provider__c';
import CASE_ADDRESS_OBJECT_FIELD from '@salesforce/schema/Address__c.Application__r.Referral__c';
import ID_ADDRESS_INFORMATION_FIELD from '@salesforce/schema/Address__c.Id';

import fetchAddressInformation from '@salesforce/apex/PublicProvidersHomeInformation.fetchAddressInformation';

import {
  utils
} from 'c/utils';
import {
  CJAMS_CONSTANTS
} from 'c/constants';
import { loadStyle } from 'lightning/platformResourceLoader';
import myResource from '@salesforce/resourceUrl/styleSheet';

import getFullDetails from "@salesforce/apex/GetStreetAddressFromGoogleAPI.getPlaceDetails";
import getAPIStreetAddress from "@salesforce/apex/GetStreetAddressFromGoogleAPI.getSuggestions";

import getAllStates from "@salesforce/apex/PublicProvidersHomeInformation.getStateDetails";
export default class PublicProvidersHomeInformation extends LightningElement {
  @track homeInformation = {
    ChildrenResiding__c: 1,
    Bedrooms__c: 1,
    WaterfrontProperty__c: "No",
    PoolLocated__c: "In Ground",
    SwimmingPool__c: "No",
    LicensedAgency__c: "No",
    LicensedProvider__c: "No",
    ExistingResourceParent__c: "No",
    Homeowner__c: "Rent"
  };
  @track homeInformationId = null;
  @track btnGrp;
  @track LicensedAgency;
  @track LicensedProvider;
  @track LicensedProviderOnchange;
  @track poolLocated;
  @track SwimmingPool;
  @track WaterfrontProperty;
  @track ExistingResourceParent;
  @track Homeowner;
  @track InterestedIn;
  @track ResourceParentState = [];
  @track poolLocatedYes = false;
  @track agencyNameYes = false;
  @track providerDetailsYes = false;
  @track Spinner = true;
  @track saveDisable = true;
  @track openModelwindow = false;
  @track disableStatus = false;
  @track ExistingResourceParentYes = false;
  @track HomeownerYes = false;
  @track interestShow = false;


  active = false;

  @track selectedCounty;
  @track strStreetAddress;
  @track strStreetAddresPrediction = [];
  @track filteredCountyArr = [];
  @track selectedState;
  @track selectedCity;
  @track selectedZipCode;
  @track selectedCounty;
  @track ShippingStreet;

  @track showStateBox = false;
  @track stateFetchResult = [];
  @track stateSearchTxt;
  @track filteredStateArr = [];

  @track showCountyBox = false;
  @track countySearchTxt;
  @track filteredCountyArr = [];
  @track countyArr = [];

  @track Phone;

  @track addressDetails = { AddressType__c: "Home Info" };
  @track addressInformationId = null;

  @track isSuperUserFlow = false;

  homeInformationIcon = images + "/home-information.svg";
  privateIcon = images + "/office-building.svg";
  existingIcon = images + "/clipboard.svg";
  plusIcon = images + "/plus-new-referal.svg";
  publicIcon = images + "/houseIcon.svg";

  _bedRoomSliderValue;
  _childrenSliderValue;
  _bedRoomSlider = homeSlider + "/BedRoom.html";
  _childrenSlider = homeSlider + "/Children.html";

  get bedRoomSlider() {
    return this._bedRoomSlider;
  }
  get childrenSlider() {
    return this._childrenSlider;
  }

  get profileName() {

    let profile = sharedData.getUserProfileName();
    if (profile == "Supervisor") {
      return this.isSuperUserFlow = true;
    }
    return this.isSuperUserFlow = false;
  }

  renderedCallback() {
    Promise.all([
      loadStyle(this, myResource + '/styleSheet.css'),
    ])
  }

  connectedCallback() {
    this.profileName;
    this.statusCheck();
    window.addEventListener("message", event => {
      let iframevalue = event.data.split(":");
      switch (iframevalue[0]) {
        case "BedRoom":
          this._bedRoomSliderValue = iframevalue[1];
          break;
        case "Children":
          this._childrenSliderValue = iframevalue[1];
          break;

        default:
        // code block
      }
    });
  }

  //Status check
  statusCheck() {
    if (this.isSuperUserFlow == false) {
      this.disableStatus = false;
    } else
      this.disableStatus = true;

  }


  get caseId() {
    return sharedData.getCaseId();
  }

  get providerId() {
    return sharedData.getProviderId();
  }

  @wire(getMultiplePicklistValues, {
    objInfo: 'HomeInformation__c',
    picklistFieldApi: 'LicensedAgency__c,LicensedProvider__c,PoolLocated__c,SwimmingPool__c,WaterfrontProperty__c,ExistingResourceParent__c,Homeowner__c,InterestedIn__c,ResourceParentState__c'
  })
  wiredTitle(data) {
    try {
      if (data.data != undefined && data.data !== '') {
        this.LicensedAgency = data.data.LicensedAgency__c;
        this.LicensedProvider = data.data.LicensedProvider__c;
        this.poolLocated = data.data.PoolLocated__c;
        this.SwimmingPool = data.data.SwimmingPool__c;
        this.WaterfrontProperty = data.data.WaterfrontProperty__c;
        this.ExistingResourceParent = data.data.ExistingResourceParent__c;
        this.Homeowner = data.data.Homeowner__c;
        this.InterestedIn = data.data.InterestedIn__c;
        this.ResourceParentState = data.data.ResourceParentState__c;
      }
    } catch (error) {
      throw error;
    }
  }

  @wire(fetchHomeInformation, {
    id: '$providerId'
  })
  fettchDetails(data) {
    if (data.data != undefined && data.data.length > 0) {
      this.homeInformationId = data.data[0].Id;
      this._bedRoomSliderValue = data.data[0].Bedrooms__c;
      this._childrenSliderValue = data.data[0].ChildrenResiding__c;
      this._bedRoomSlider = homeSlider + "/BedRoom.html?Bedroom=" + this._bedRoomSliderValue + "&disableSlider=" + this.disableStatus;
      this._childrenSlider = homeSlider + "/Children.html?Children=" + this._childrenSliderValue + "&disableSlider=" + this.disableStatus;
      this.homeInformation.AgencyName__c = data.data[0].AgencyName__c;
      this.homeInformation.LicensedProviderDetail__c = data.data[0].LicensedProviderDetail__c;
      data.data[0].LicensedAgency__c == 'Yes' ? this.agencyNameYes = true : this.agencyNameYes = false;
      this.homeInformation.LicensedAgency__c = data.data[0].LicensedAgency__c;
      data.data[0].LicensedProvider__c == 'Yes' ? this.providerDetailsYes = true : this.providerDetailsYes = false;
      this.homeInformation.LicensedProvider__c = data.data[0].LicensedProvider__c;
      this.homeInformation.PoolLocated__c = data.data[0].PoolLocated__c;
      data.data[0].SwimmingPool__c == 'Yes' ? this.poolLocatedYes = true : this.poolLocatedYes = false;
      this.homeInformation.SwimmingPool__c = data.data[0].SwimmingPool__c;
      this.homeInformation.WaterfrontProperty__c = data.data[0].WaterfrontProperty__c;
      data.data[0].InterestedIn__c == 'Providing Foster Care' ? this.interestShow = true : this.interestShow = false;
      this.homeInformation.InterestedIn__c = data.data[0].InterestedIn__c;
      this.homeInformation.ExplanatoryText__c = data.data[0].ExplanatoryText__c;
      this.homeInformation.ResourceParentState__c = data.data[0].ResourceParentState__c;
      data.data[0].ExistingResourceParent__c == 'Yes' ? this.ExistingResourceParentYes = true : this.ExistingResourceParentYes = false;
      this.homeInformation.ExistingResourceParent__c = data.data[0].ExistingResourceParent__c;
      data.data[0].Homeowner__c == "Homeowner" ? this.HomeownerYes = true : this.HomeownerYes = false;
      this.homeInformation.Homeowner__c = data.data[0].Homeowner__c;
      this.homeInformation.YearPropertyBuilt__c = data.data[0].YearPropertyBuilt__c;

      this.Spinner = false;
    } else if (data.error) {
      let errors = data.error;
      if (errors) {
        let error = JSON.parse(errors.body.message);
        const { title, message, errorType } = error;
        this.dispatchEvent(
          utils.toastMessageWithTitle(title, message, errorType)
        );
      } else {
        this.dispatchEvent(
          utils.toastMessage(
            CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
            CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
          )
        );
      }
    }
    else {
      this._bedRoomSlider = homeSlider + "/BedRoom.html?disableSlider=" + this.disableStatus;
      this._childrenSlider = homeSlider + "/Children.html?disableSlider=" + this.disableStatus;
    }
    this.Spinner = false;
  }

  @wire(fetchAddressInformation, {
    id: '$providerId'
  })
  fetchDetails(data) {
    if (data.data != undefined && data.data.length > 0) {
      this.addressInformationId = data.data[0].Id;
      this.addressDetails.AddressLine1__c = data.data[0].AddressLine1__c;
      this.addressDetails.AddressLine2__c = data.data[0].AddressLine2__c;
      this.addressDetails.City__c = data.data[0].City__c;
      this.addressDetails.County__c = data.data[0].County__c;
      this.addressDetails.State__c = data.data[0].State__c;
      this.addressDetails.ZipCode__c = data.data[0].ZipCode__c;
      this.addressDetails.Email__c = data.data[0].Email__c;
      this.addressDetails.Phone__c = data.data[0].Phone__c;
    } else if (data.error) {
      let errors = data.error;
      if (errors) {
        let error = JSON.parse(errors.body.message);
        const { title, message, errorType } = error;
        this.dispatchEvent(
          utils.toastMessageWithTitle(title, message, errorType)
        );
      } else {
        this.dispatchEvent(
          utils.toastMessage(
            CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
            CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
          )
        );
      }
    }
  }

  //Google api code starts from here

  get streetAddressLength() {
    return this.strStreetAddresPrediction != "";
  }
  get strStreetAddresPredictionUI() {
    return this.strStreetAddresPrediction;
  }
  get strStreetAddresPredictionLength() {
    if (this.addressDetails.AddressLine1__c)
      return this.strStreetAddresPrediction.predictions ? true : false;
  }

  //Method used to show and hide the drop-down of suggestions
  get tabClass() {
    return this.active ?
      "slds-dropdown slds-dropdown_length-5 slds-dropdown_fluid slds-show" :
      "slds-dropdown slds-dropdown_length-5 slds-dropdown_fluid slds-hide";
  }


  //Function used to fetch all states from Address Object when Screen Loads
  @wire(getAllStates)
  getAllStates({
    error,
    data
  }) {
    if (data) {
      if (data.length > 0) {
        let stateData = data.map(row => {
          return {
            Id: row.Id,
            Value: row.RefValue__c
          };
        });
        this.stateFetchResult = stateData;
        // this.countyInitialLoad();
      } else {
        this.dispatchEvent(utils.toastMessage("No states found", "error"));
      }
    } else if (error) {
      this.dispatchEvent(
        utils.toastMessage("Error in Fetching states", "error")
      );
    }
  }

  //Function used to set selected state
  setSelectedCounty(event) {
    this.addressDetails.County__c = event.currentTarget.dataset.value;
    this.showCountyBox = false;
  }

  //Function used to set selected state
  setSelectedState(event) {
    this.addressDetails.State__c = event.currentTarget.dataset.value;
    this.showStateBox = false;
  }

  //Function used to capture on change county in an array
  countyOnChange(event) {
    let countySearchTxt = event.target.value;

    if (!countySearchTxt) this.showCountyBox = false;
    else {
      this.filteredCountyArr = this.countyArr
        .filter(val => val.value.indexOf(countySearchTxt) > -1)
        .map(cresult => {
          return cresult;
        });
      this.showCountyBox = this.filteredCountyArr.length > 0 ? true : false;
    }
  }

  getStreetAddress(event) {
    this.addressDetails.AddressLine1__c = event.target.value;
    if (!event.target.value) {
      this.addressDetails.County__c = "";
      this.addressDetails.State__c = "";
      this.addressDetails.City__c = "";
      this.addressDetails.ZipCode__c = "";
      //this.strStreetAddresPredictionLength = false;
    } else {
      getAPIStreetAddress({
        input: this.addressDetails.AddressLine1__c
      })
        .then(result => {
          this.strStreetAddresPrediction = JSON.parse(result);
          this.active =
            this.strStreetAddresPrediction.predictions.length > 0 ?
              true :
              false;
        })
        .catch(() => {
          this.dispatchEvent(
            utils.toastMessage(
              "Error in Fetching Google API Street Address",
              "error"
            )
          );
        });
    }
  }

  //Function used to select the address
  selectStreetAddress(event) {
    this.addressDetails.AddressLine1__c = event.target.dataset.description;
    getFullDetails({
      placeId: event.target.dataset.id
    })
      .then(result => {
        var main = JSON.parse(result);
        try {
          var cityTextbox = this.template.querySelector(".dummyCity");
          this.addressDetails.City__c = main.result.address_components[2].long_name;

          var countyComboBox = this.template.querySelector(".dummyCounty");
          this.addressDetails.County__c = main.result.address_components[4].long_name;

          var stateComboBox = this.template.querySelector(".dummyState");
          this.addressDetails.State__c = main.result.address_components[5].long_name;

          var ZipCodeTextbox = this.template.querySelector(".dummyZipCode");
          this.addressDetails.ZipCode__c = main.result.address_components[7].long_name;
        } catch (ex) { }
        this.active = false;
      })
      .error(error => {
        this.dispatchEvent(
          utils.toastMessage("Error in Selected Address", "error")
        );
      });
  }

  //Function used for capturing on change states in an array
  stateOnChange(event) {
    let stateSearchTxt = event.target.value;

    if (!stateSearchTxt) this.showStateBox = false;
    else {
      this.filteredStateArr = this.stateFetchResult
        .filter(val => val.Value.indexOf(stateSearchTxt) > -1)
        .map(sresult => {
          return sresult;
        });
      this.showStateBox = this.filteredStateArr.length > 0 ? true : false;
    }
  }

  //All onchange event handler for address related fields
  addressOnChange(event) {
    if (event.target.name == "ShippingStreet") {
      this.addressDetails.AddressLine2__c = event.target.value;
    } else if (event.target.name == "City") {
      this.addressDetails.City__c = event.target.value;
    } else if (event.target.name == "ZipCode") {
      this.addressDetails.ZipCode__c = event.target.value.replace(/(\D+)/g, "");
    }
  }

  //ends here

  //Validation for Phone Number
  handlePhoneChange(evt) {
    evt.target.value = evt.target.value.replace(/(\D+)/g, "");
    this.addressDetails.Phone__c = utils.formattedPhoneNumber(evt.target.value);
  }

  //Function for Email
  handleEmailChange(event) {
    this.addressDetails.Email__c = event.target.value;
  }

  handleChange(event) {
    this.homeInformation.AgencyName__c = event.target.value;
  }

  handleButtonGroupExplanatoryText(event) {
    this.homeInformation.ExplanatoryText__c = event.target.value;
  }

  handleButtonGroupYearPropertyBuilt(event) {
    this.homeInformation.YearPropertyBuilt__c = event.target.value;
  }

  handlechangeDetails(event) {
    this.homeInformation.LicensedProviderDetail__c = event.target.value;
  }

  handleButtonGroupLicensedAgency(event) {
    this.homeInformation.LicensedAgency__c = event.target.value;
    if (this.homeInformation.LicensedAgency__c == 'Yes') {
      this.agencyNameYes = true;
    } else {
      this.agencyNameYes = false;
    }
  }

  handleButtonGroupWater(event) {
    this.homeInformation.WaterfrontProperty__c = event.target.value;
  }

  handleButtonGroupInterestedIn(event) {
    this.homeInformation.InterestedIn__c = event.target.value;
    if (this.homeInformation.InterestedIn__c == "Providing Foster Care") {
      this.interestShow = true;
    } else {
      this.interestShow = false;
    }
  }

  handleButtonGroupSwimmingPool(event) {
    this.homeInformation.SwimmingPool__c = event.target.value;
    if (this.homeInformation.SwimmingPool__c == 'Yes') {
      this.poolLocatedYes = true;
    } else {
      this.poolLocatedYes = false;
    }
  }

  handleButtonGroupPoolLocated(event) {
    this.homeInformation.PoolLocated__c = event.target.value;
  }

  handleButtonGroupResourceParentState(event) {
    this.homeInformation.ResourceParentState__c = event.target.value;
  }

  LicensedProvideronchange(event) {
    this.homeInformation.LicensedProvider__c = event.target.value;
    if (this.homeInformation.LicensedProvider__c == 'Yes') {
      this.providerDetailsYes = true;
    } else {
      this.providerDetailsYes = false;
    }
  }

  ExistingResourceParentonchange(event) {
    this.homeInformation.ExistingResourceParent__c = event.target.value;
    if (this.homeInformation.ExistingResourceParent__c == 'Yes') {
      this.ExistingResourceParentYes = true;
    } else {
      this.ExistingResourceParentYes = false;
    }
  }

  Homeowneronchange(event) {
    this.homeInformation.Homeowner__c = event.target.value;
    if (this.homeInformation.Homeowner__c == 'Homeowner') {
      this.HomeownerYes = true;
    } else {
      this.HomeownerYes = false;
    }
  }

  //Address save functionality
  handleAddressInfo() {
    let fields = this.addressDetails;

    if (!this.addressDetails.AddressLine1__c) {
      return this.dispatchEvent(utils.toastMessage("Please enter address line1", "warning"));
    }
    if (!this.addressDetails.City__c) {
      return this.dispatchEvent(utils.toastMessage("Please enter city", "warning"));
    }
    if (!this.addressDetails.State__c) {
      return this.dispatchEvent(utils.toastMessage("Please enter state", "warning"));
    }
    if (!this.addressDetails.County__c) {
      return this.dispatchEvent(utils.toastMessage("Please enter county", "warning"));
    }
    if (!this.addressDetails.ZipCode__c) {
      return this.dispatchEvent(utils.toastMessage("Please enter zipcode", "warning"));
    }
    if (!this.addressDetails.Email__c) {
      return this.dispatchEvent(utils.toastMessage("Please enter email", "warning"));
    }
    if (!this.addressDetails.Phone__c) {
      return this.dispatchEvent(utils.toastMessage("Please enter phone number", "warning"));
    }
    const allValid = [
      ...this.template.querySelectorAll('lightning-input')
    ]
      .reduce((validSoFar, inputFields) => {
        inputFields.reportValidity();
        return validSoFar && inputFields.checkValidity();
      }, true);

    if (allValid) {
      let fields = this.addressDetails;

      this.Spinner = true;

      if (this.addressInformationId === undefined || this.addressInformationId === null || this.addressInformationId === '') {
        fields[PROVIDER_ADDRESS_OBJECT_FIELD.fieldApiName] = this.providerId;
        fields[CASE_ADDRESS_OBJECT_FIELD.fieldApiName] = this.caseId;
        const recordInput = {
          apiName: ADDRESS_INFORMATION_OBJECT.objectApiName,
          fields
        };

        createRecord(recordInput)
          .then(result => {
            this.Spinner = false;
            this.dispatchEvent(utils.toastMessage("Home Information has been saved successfully", "success"));
          })
          .catch((errors) => {
            this.disabled = false;

            if (errors) {
              let error = JSON.parse(errors.body.message);
              const { title, message, errorType } = error;
              this.dispatchEvent(
                utils.toastMessageWithTitle(title, message, errorType)
              );
            } else {
              this.dispatchEvent(
                utils.toastMessage(
                  CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
                  CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
                )
              );
            }
          });

      } else {
        fields[ID_ADDRESS_INFORMATION_FIELD.fieldApiName] = this.addressInformationId;
        const recordInput = {
          fields
        };
        updateRecord(recordInput)
          .then(result => {
            try {
              this.Spinner = false;
              this.dispatchEvent(utils.toastMessage("Home Information has been updated successfully", "success"));
            } catch (error) {
              throw error;
            }
          })
          .catch((errors) => {
            this.disabled = false;

            if (errors) {
              let error = JSON.parse(errors.body.message);
              const { title, message, errorType } = error;
              this.dispatchEvent(
                utils.toastMessageWithTitle(title, message, errorType)
              );
            } else {
              this.dispatchEvent(
                utils.toastMessage(
                  CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
                  CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
                )
              );
            }
          });

      }
    } else {
      this.dispatchEvent(utils.toastMessage("Error in saving Home Information. Please check.", "error"));
    }
  }

  handleHomeInfo() {
    let fields = this.homeInformation;
    this.homeInformation.ChildrenResiding__c = this._childrenSliderValue;
    this.homeInformation.Bedrooms__c = this._bedRoomSliderValue;
    if (!this.homeInformation.ChildrenResiding__c) {
      return this.dispatchEvent(utils.toastMessage("Please enter a children residing", "warning"));
    }
    if (this.homeInformation.ChildrenResiding__c > 10) {
      return this.dispatchEvent(
        utils.toastMessage("Exceeded Children Residing Value more than 10", "warning")
      );
    }
    if (!this.homeInformation.Bedrooms__c) {
      return this.dispatchEvent(utils.toastMessage("Please enter the bedrooms", "warning"));
    }
    if (this.homeInformation.Bedrooms__c > 10) {
      return this.dispatchEvent(
        utils.toastMessage("Exceeded Bedrooms Value more than 10", "warning")
      );
    }
    if (this.homeInformationId === undefined || this.homeInformationId === null || this.homeInformationId === '') {
      fields[PROVIDER_HOME_OBJECT_FIELD.fieldApiName] = this.providerId;
      fields[CASE_HOME_OBJECT_FIELD.fieldApiName] = this.caseId;
      const recordInput = {
        apiName: HOME_INFORMATION_OBJECT.objectApiName,
        fields
      };

      createRecord(recordInput)
        .then(result => {
          this.handleAddressInfo();
        })
        .catch((errors) => {
          this.disabled = false;

          if (errors) {
            let error = JSON.parse(errors.body.message);
            const { title, message, errorType } = error;
            this.dispatchEvent(
              utils.toastMessageWithTitle(title, message, errorType)
            );
          } else {
            this.dispatchEvent(
              utils.toastMessage(
                CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
                CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
              )
            );
          }
        });

    } else {
      fields[ID_HOME_INFORMATION_FIELD.fieldApiName] = this.homeInformationId;
      const recordInput = {
        fields
      };
      updateRecord(recordInput)
        .then(result => {
          try {
            this.handleAddressInfo();
          } catch (error) {
            throw error;
          }
        })
        .catch((errors) => {
          this.disabled = false;

          if (errors) {
            let error = JSON.parse(errors.body.message);
            const { title, message, errorType } = error;
            this.dispatchEvent(
              utils.toastMessageWithTitle(title, message, errorType)
            );
          } else {
            this.dispatchEvent(
              utils.toastMessage(
                CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
                CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
              )
            );
          }
        });

    }
  }

  onHandleClear() {
    this.homeInformation = { ChildrenResiding__c: 1 };
    this._childrenSliderValue = 1;
    this._bedRoomSliderValue = 1;
    this.homeInformation.ChildrenResiding__c = 1;
    this.activeSectionMessageResiding = 1;
    this.homeInformation.Bedrooms__c = 1;
    this.activeSectionMessageBedroom = 1;
    this.homeInformation.AgencyName__c = '';
    this.homeInformation.LicensedProviderDetail__c = "";
    this.homeInformation.LicensedAgency__c = "";
    this.homeInformation.LicensedProvider__c = "";
    this.homeInformation.SwimmingPool__c = "";
    this.homeInformation.WaterfrontProperty__c = "";
    this.homeInformation.PoolLocated__c = "";
    this.homeInformation.ResourceParentState__c = "";
    this.homeInformation.YearPropertyBuilt__c = "";
    this.homeInformation.InterestedIn__c = "";
    this.homeInformation.ExistingResourceParent__c = "";
    this.homeInformation.ExplanatoryText__c = "";
    this.homeInformation.Homeowner__c = "";
    this.addressDetails.AddressLine1__c = "";
    this.addressDetails.AddressLine2__c = "";
    this.addressDetails.City__c = "";
    this.addressDetails.County__c = "";
    this.addressDetails.Email__c = "";
    this.addressDetails.Phone__c = "";
    this.addressDetails.State__c = "";
    this.addressDetails.ZipCode__c = "";
    this.addressDetails = {}
  }

  handleSubmit() {
    this.handleHomeInfo();
  }
}