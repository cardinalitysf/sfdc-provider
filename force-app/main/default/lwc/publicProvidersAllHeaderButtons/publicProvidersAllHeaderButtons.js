import { LightningElement, track, wire, api } from 'lwc';

import { getRecord } from "lightning/uiRecordApi";
import Provider_Id_FIELD from '@salesforce/schema/Account.ProviderId__c';
//Utils 
import * as sharedData from "c/sharedData";

export default class PublicProvidersAllHeaderButtons extends LightningElement {

  //Button show/Hide
  @api showReconsiderationTabset = false;
  @api showAddService = false;
  @api showPetDetails = false;
  @api showAddHomeStudy = false;
  @api showTrainingDetails = false;
  @api showAddReconsideration = false;
  @api providerName;
  @api reconsiderationName;

  // method for redirecting a button popup  
  RedirectServiceInfo() {
    const onredirectservice = new CustomEvent(
      "redirecttoserviceinfo",
      {
        detail: {
          first: true
        }
      }
    );
    this.dispatchEvent(onredirectservice);
  }

  RedirectPetInfo() {
    const onredirectpet = new CustomEvent(
      "redirecttopetinfo",
      {
        detail: {
          first: true
        }
      }
    );
    this.dispatchEvent(onredirectpet);
  }

  RedirectTraining() {
    const onredirectpet = new CustomEvent(
      "redirecttotraining",
      {
        detail: {
          first: true
        }
      }
    );
    this.dispatchEvent(onredirectpet);
  }


  redirectToPubProvDashboard() {
    // const onClickId = new CustomEvent('redirecttopubprovdashboard', {
    //   detail: {
    //     first: true
    //   }
    // });
    // this.dispatchEvent(onClickId);
    window.location.reload();
  }

  get providerId() {
    return sharedData.getProviderId();
  }

  /* @wire(getRecord, {
    recordId: "$providerId",
    fields: [Provider_Id_FIELD]
  })
  wireuser({ error, data }) {
    try {
      if (data) {
        this.providerName = data.fields.ProviderId__c.value;
      }
    } catch (error) {
      throw error;
    }
  } */

  openStudyModel() {
    const onredirectstudy = new CustomEvent(
      "redirecttostudyvisit",
      {
        detail: {
          first: true
        }
      }
    );
    this.dispatchEvent(onredirectstudy);
  }


  // redirectReconsidertionDetails functionality start 
  redirectReconsidertionDetails = (event) => {
    const reconsiderationOnClickId = new CustomEvent('redirecttoreconsiderationdetail', {
      detail: {
        first: true
      }
    });
    this.dispatchEvent(reconsiderationOnClickId);
  }
  // redirectReconsidertionDetails functionality end

  redirectToPublicProviderTabSet() {
    const onClickId = new CustomEvent('redirecttoprovtabset', {
      detail: {
        first: true
      }
    });
    this.dispatchEvent(onClickId);
  }
}