/**
 * @Author        : Saranraj v
 * @CreatedOn     : May 16,2020
 * @Purpose       : public provider dashboard datatable
 * @updatedBy     :
 * @updatedOn     :
 **/
import { LightningElement, wire, api, track } from "lwc";
import * as sharedData from "c/sharedData";

export default class PublicProviderDashboardDatatable extends LightningElement {
  @track data;
  @api
  get dashboardData() {
    return this.data;
  }
  set dashboardData(value) {
    this.setAttribute("dashboardData", value);
    this.data = value;
    this.setPages(this.data);
    this.totalRecordsCount = value.length;
  }
  

  get options() {
    return [
      {
        label: "Referral Number",
        fieldName: "ReferralID",
        cellAttributes: {
          class: "fontclrGrey"
        },
        type: "button",
        sortable: true,
        typeAttributes: {
          label: {
            fieldName: "ReferralNumber"
          },
          class: "blue",
          target: "_self"
        }
      },
      {
        label: "Referral Type",
        fieldName: "ReferralType",
        cellAttributes: {
          class: "fontclrGrey"
        }
      },
      {
        label: "Program",
        fieldName: "Program",
        cellAttributes: {
          class: "fontclrGrey"
        }
      },
      {
        label: "Program Type",
        fieldName: "ProgramType",
        cellAttributes: {
          class: "fontclrGrey"
        }
      },
      {
        label: "Applicant",
        fieldName: "Applicant",
        cellAttributes: {
          class: "fontclrGrey"
        }
      },
      {
        label: "Co-Applicant",
        fieldName: "CoApplicant",
        cellAttributes: {
          class: "fontclrGrey"
        }
      },
      {
        label: "Status",
        fieldName: "Status",
        type: "text",
        cellAttributes: {
          class: {
            fieldName: "statusClass"
          }
        }
      }
    ];
  }

  // new pagination

  @track page = 1;
  perPage = 10;
  @track pages = [];
  setSize = 5;
  @track pagePerTime;
  @track pagesLength;

  renderedCallback() {
    this.renderButtons();
  }

  renderButtons = () => {
    this.template.querySelectorAll("button").forEach((but) => {
      but.className =
        this.page === parseInt(but.dataset.id, 10)
          ? "slds-button slds-button_neutral active"
          : "slds-button slds-button_neutral";
    });
  };

  get pagesList() {
    let mid = Math.floor(this.setSize / 2) + 1;
    if (this.page > mid) {
      return this.pages.slice(this.page - mid, this.page + mid - 1);
      // let pagecount = this.pages.slice(0, this.setsize);
      // this.pagePerTime = pagecount[pagecount.length - 1];
      // return this.pages.slice(this.page - mid, this.page + mid - 1);
    }
    // let pagecount = this.pages.slice(0, this.setsize);
    // this.pagePerTime = pagecount[pagecount.length - 1];
    else return this.pages.slice(0, this.setSize);
  }

  setPages = (data) => {
    let numberOfPages = Math.ceil(JSON.stringify(data.length) / this.perPage);
    this.pagesLength = numberOfPages;
    this.pages = [];
    for (let i = 1; i <= numberOfPages; i++) {
      this.pages.push(i);
    }
  };

  get hasPrev() {
    return this.page > 1;
  }

  get hasNext() {
    return this.page < this.pages.length;
  }

  onNext = () => {
    ++this.page;
  };

  onPrev = () => {
    --this.page;
  };

  onPageClick(event) {
    this.page = parseInt(event.target.dataset.id, 10);
  }

  get currentPageData() {
    return this.pageData();
  }

  pageData() {
    let startIndex = this.page * this.perPage - this.perPage;
    let endIndex = this.page * this.perPage;
    return this.data.slice(startIndex, endIndex);
  }
  // new pagination end ==================/

  // handleRowAction functionality start
  handleRowAction(event) {
    sharedData.setCaseId(event.detail.row.ReferralID);
    sharedData.setProviderId(event.detail.row.providerId)
    const onClickOfRow = new CustomEvent("redirecttopublicproviderhomepage", {
      bubbles: true,
      composed: true,
      detail: {
        redirectId: event.detail.row.ReferralID
      }
    });
    this.dispatchEvent(onClickOfRow);
  }
  // handleRowAction functionality end
}
