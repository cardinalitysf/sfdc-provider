//Sundar K modifying this component to freeze provider application

/**
 * @Author        : G.sathishkumar
 * @CreatedOn     : March 18, 2020
 * @Purpose       : assingStaffMember
 * @updatedBy     :
 * @updatedOn     :
 **/


import { LightningElement, track, wire } from 'lwc';
import getStaffFromProviderID from "@salesforce/apex/assingStaffMember.getStaffFromProviderID";
import getStaffFromApplicationID from "@salesforce/apex/assingStaffMember.getStaffFromApplicationID";
import deleteStaffRecord from '@salesforce/apex/assingStaffMember.deleteStaffRecord';
import { utils } from "c/utils";




import * as refsharedData from "c/sharedData";
import InsertUpdateStaffApplication from '@salesforce/apex/ProviderStaffCertificationController.InsertUpdateStaffCertificate';

//start stylesheet
//import { loadStyle } from 'lightning/platformResourceLoader';
//import myResource from '@salesforce/resourceUrl/styleSheet';
//end stylesheet

//Sundar added this lines to freeze assign staff memeber screen
import getProviderApplicationStatus from "@salesforce/apex/assingStaffMember.getProviderApplicationStatus";
import { CJAMS_CONSTANTS } from 'c/constants';
//assignStafftableshow columns
const columns = [

    { label: 'First Name', fieldName: 'FirstName__c', type: 'text' },
    { label: 'Last Name', fieldName: 'LastName__c', type: 'text' },
    { label: 'Affiliated Type', fieldName: 'AffiliationType__c', type: 'text' },
    { label: 'Job Title', fieldName: 'JobTitle__c', type: 'text' },
    { label: 'Employee Type', fieldName: 'EmployeeType__c', type: 'text', initialWidth: 400 },
    {
        label: 'Action', fieldName: 'Id', type: "button", typeAttributes: {
            iconName: 'utility:delete',
            name: 'Delete',
            title: 'Delete',
            variant: 'border-filled',
            class: 'del-red',
            disabled: false,
            iconPosition: 'left',
            target: '_self'
        }
    },


];

//assignStaffpopupshow columns1
const columns1 = [

    { label: 'First Name', fieldName: 'FirstName__c', type: 'text', initialWidth: 140 },
    { label: 'Last Name', fieldName: 'LastName__c', type: 'text', initialWidth: 140 },
    { label: 'Affiliated Type', fieldName: 'AffiliationType__c', type: 'text', initialWidth: 140 },
    { label: 'Job Title', fieldName: 'JobTitle__c', type: 'text', initialWidth: 140 },
    { label: 'Employee Type', fieldName: 'EmployeeType__c', type: 'text' },

];


export default class Assingstaffmembers extends LightningElement {

    @track columns = columns;
    @track columns1 = columns1;
    @track assignDataItem;
    @track assignDataItem1;
    @track openmodel = false;
    @track openmodel2 = false;
    @track assignStafftableshow = [];
    @track assignStaffpopupshow = [];
    @track norecorddisplay = true;
    @track norecorddisplay1 = true;
    selectedAssignees = [];
    showSelectedAssignees = [];
    isAssigneeSelected = false;
    @track selectedUser = false;



    //Pagination Tracks
    @track totalRecordsCount = 0;
    @track page = 1;
    perpage = 10;
    setPagination = 5;
    @track totalRecords;

    @track totalRecordsCount2 = 0;
    @track totalRecords2;
    perpage2 = 8;

    //Sundar K Modified this to Freeze Screen
    @track assignBtnDisable = false;
    @track saveBtnDisable = false;
    @track applicationForApiCall = this.applicationId;
    @track deleteBtnDisable = false;

    openmodal() {
        this.openmodel = true;
        this.page = 1;
        this.pageData();
        this.pageData2();

    }

    openmodal2() {
        this.openmodel2 = true;
    }

    closeModal() {
        this.openmodel = false;
        this.page = 1;
        this.pageData();
        this.pageData2();
    }

    closeModal2() {
        this.openmodel2 = false
    }


    //start stylesheet
    // renderedCallback() {
    //     Promise.all([
    //         loadStyle(this, myResource + '/styleSheet.css')
    //     ])
    // }
    //end stylesheet


    get providerId() {

        return refsharedData.getProviderId();
    }
    get applicationId() {

        return refsharedData.getApplicationId();
    }

    // assignStaffSave to assignStafftableshow
    assignStaffSave() {
        if (this.selectedUser) {
            const selectedValue = [];

            this.selectedAssignees.forEach(item => {
                selectedValue.push({ 'Staff__c': item.Id, 'Application__c': this.applicationId, 'sobjectType': 'Application_Contact__c' });
            });

            selectedValue.forEach(value => {
                InsertUpdateStaffApplication({ objSobjecttoUpdateOrInsert: value }).then(result => {

                    this.dispatchEvent(utils.toastMessage(" Staff Detail Added  Successfully", "Success"));
                    this.selectedAssignees = [];
                    this.initialStaffMemberLoad();
                }).catch(errors => {
                    let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                    return this.dispatchEvent(utils.handleError(errors, config));
                });

            });

        }
        else {
            this.selectedUser = false;

        }

        this.closeModal();
    }

    handleRowSelection = event => {
        this.selectedAssignees = event.detail.selectedRows;
        this.selectedUser = true;
    }

    //Staff Record Delete Functinality
    handleDelete() {

        deleteStaffRecord({ deletestaffrecord: this.currentdeleterecord }).then(result => {
            this.dispatchEvent(utils.toastMessage("Staff Details Removed successfully ", "success"));
            this.openmodel2 = false;
            this.initialStaffMemberLoad();
        }).catch(errors => {
            let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DELETE_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
            return this.dispatchEvent(utils.handleError(errors, config));
        });
    }

    handleRowActionDelete(event) {

        if (event.detail.action.name == "Delete") {

            //Sundar added this condition to arrest delete functionality for Submitted Application
            if (this.deleteBtnDisable)
                return this.dispatchEvent(utils.toastMessage("cannot delete staff details for submitted provider application", "Warning"));

            this.openmodel2 = true;
            this.currentdeleterecord = event.detail.row.applicationRecordDeleteID;
        }

    }

    //Sundar Updated this function for freeze the screen
    //Get Provider Application Status
    @wire(getProviderApplicationStatus, {
        applicationId: '$applicationForApiCall'
    })
    wiredApplicationDetails({ error, data }) {
        if (data) {
            if (data.length > 0) {
                if (data[0].Status__c == CJAMS_CONSTANTS.PROVIDER_APP_FINAL_STATUS_REVIEW || data[0].Status__c == CJAMS_CONSTANTS.PROVIDER_APP_FINAL_STATUS_APPEAL || data[0].Status__c == CJAMS_CONSTANTS.APPLICATION_FINAL_STATUS_REVIEW || data[0].Status__c == CJAMS_CONSTANTS.PROVIDER_APP_FINAL_STATUS_REJECTED || data[0].Status__c == CJAMS_CONSTANTS.APPLICATION_FINAL_STATUS_APPROVED || data[0].Status__c == CJAMS_CONSTANTS.APPLICATION_FINAL_STATUS_SUPERVISOR_REJECTED) {
                    this.assignBtnDisable = true;
                    this.saveBtnDisable = true;
                    this.deleteBtnDisable = true;
                } else {
                    this.assignBtnDisable = false;
                    this.saveBtnDisable = false;
                    this.deleteBtnDisable = false;
                }
            }
        }
    }

    saveDraft() {
        window.location.reload();
    }

    connectedCallback() {
        this.initialStaffMemberLoad();
    }

    initialStaffMemberLoad() {

        getStaffFromProviderID({ getcontactfromprov: this.providerId }).then(result => {
            this.assignDataItem = result;
            this.AccountId = this.providerId;
            this.staffload1();
        }).catch(errors => {
            let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
            return this.dispatchEvent(utils.handleError(errors, config));
        });
    }

    staffload1() {
        getStaffFromApplicationID({ getcontactfromapp: this.applicationId }).then(result => {
            this.assignDataItem1 = result;
            this.staffload2();
        }).catch(errors => {
            let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
            return this.dispatchEvent(utils.handleError(errors, config));
        });
    }

    staffload2() {
        let assignDataArray = [];
        let assignDataArray1 = [];

        this.assignDataItem.forEach(assign => {
            this.assignDataItem1.forEach(assign1 => {
                if (assign1.Staff__c == assign.Id) {

                    const staffDelete = { "applicationRecordDeleteID": assign1.Id };
                    const staffDeleteReturned = Object.assign(staffDelete, assign);
                    assignDataArray.push(staffDeleteReturned);
                    assignDataArray1.push(assign);
                }
            });

        });



        this.totalRecords = assignDataArray;
        this.totalRecordsCount = this.totalRecords.length;

        if (this.totalRecords.length == 0) {
            this.norecorddisplay1 = false;

        } else {
            this.norecorddisplay1 = true;
        }
        this.pageData();

        const staffFilteredArray = this.assignDataItem.filter(function (x) {
            return assignDataArray1.indexOf(x) < 0;
        });


        this.totalRecords2 = staffFilteredArray;
        this.totalRecordsCount2 = this.totalRecords2.length;
        if (this.totalRecords2.length == 0) {
            this.norecorddisplay = false;
        } else {
            this.norecorddisplay = true;
        }
        this.pageData2();

    }


    pageData = () => {
        let page = this.page;
        let perpage = this.perpage;
        let startIndex = (page * perpage) - perpage;
        let endIndex = (page * perpage);
        this.assignStafftableshow = this.totalRecords.slice(startIndex, endIndex);



        if (this.assignStafftableshow.length == 0) {
            if (this.page != 1) {
                this.page = this.page - 1;
                this.pageData();
            }
        }
    }
    pageData2 = () => {
        let page = this.page;
        let perpage = this.perpage2;
        let startIndex = (page * perpage) - perpage;
        let endIndex = (page * perpage);
        this.assignStaffpopupshow = this.totalRecords2.slice(startIndex, endIndex);

    }

    //For Pagination Child Bind
    hanldeProgressValueChange(event) {
        this.page = event.detail;
        this.pageData();

    }
    hanldeProgressValueChange2(event) {
        this.page = event.detail;
        this.pageData2();
    }


}