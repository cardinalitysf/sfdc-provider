/***
 * @Author: Pratheeba V
 * @CreatedOn: April 6, 2020
 * @Purpose: Application Monitoring Add/List/Edit
 * @updatedBy: Pratheeba V
 * @updatedOn: April 13,2020 
 * @UpdatedBy: Sundar K - Fully Updated Component 
 * @UpdatedOn: May 13 2020
 **/

import {
    LightningElement,
    track,
    wire
} from 'lwc';
import getMonitoringDetails from '@salesforce/apex/ApplicationMonitoring.getMonitoringDetails';
import InsertUpdateMonitoring from '@salesforce/apex/ApplicationMonitoring.InsertUpdateMonitoring';
import getAddressDetails from '@salesforce/apex/ApplicationMonitoring.getAddressDetails';
import getProgramDetails from '@salesforce/apex/ApplicationMonitoring.getProgramDetails';
import {
    getObjectInfo
} from 'lightning/uiObjectInfoApi';
import MONITORING_OBJECT from '@salesforce/schema/Monitoring__c';
import {
    getPicklistValuesByRecordType
} from 'lightning/uiObjectInfoApi';
import editMonitoringDetails from '@salesforce/apex/ApplicationMonitoring.editMonitoringDetails';
import viewMonitoringDetails from '@salesforce/apex/ApplicationMonitoring.viewMonitoringDetails';
import getApplicationProviderStatus from "@salesforce/apex/ApplicationMonitoring.getApplicationProviderStatus";
import getMonitoringStatus from "@salesforce/apex/ApplicationMonitoring.getMonitoringStatus";
import * as sharedData from 'c/sharedData';
import {
    utils
} from 'c/utils';
import images from '@salesforce/resourceUrl/images';
import { CJAMS_CONSTANTS } from 'c/constants';
import getApplicationLicenseStatus from '@salesforce/apex/ApplicationMonitoring.getApplicationLicenseStatus';

const actions = [{
    label: 'Edit',
    name: 'Edit',
    iconName: 'utility:edit',
    target: '_self'
},
{
    label: 'View',
    name: 'View',
    iconName: 'utility:preview',
    target: '_self'
},
];

const columns = [{
    label: 'Visit Id',
    fieldName: 'linkName',
    type: 'button',
    initialWidth: 100,
    typeAttributes: {
        label: {
            fieldName: 'Name'
        }
    }
},
{
    label: 'Program Type',
    type: 'button',
    sortable: true,
    typeAttributes: {
        name: 'dontRedirect',
        label: { fieldName: 'ProgramType__c' }
    }, cellAttributes: {
        class: "title"
    }
},
{
    label: 'Visit Start Date',
    fieldName: 'VisitationStartDate__c',
    sortable: true
},
{
    label: 'Site Address',
    type: 'button',
    sortable: true,
    typeAttributes: {
        name: 'dontRedirect',
        label: { fieldName: 'SiteAddress' }
    }, cellAttributes: {
        class: "title"
    }
},
{
    label: 'Frequency',
    fieldName: 'SelectPeriod__c',
    sortable: true
},
{
    label: 'Application/License Id',
    fieldName: 'ApplicationName',
    sortable: true
},
{
    label: 'Status',
    fieldName: 'Status__c',
    cellAttributes: {
        class: {
            fieldName: 'statusClass'
        }
    }
},
{
    label: '',
    type: 'action',
    typeAttributes: {
        rowActions: actions
    },
    cellAttributes: {
        iconName: 'utility:threedots_vertical',
        iconAlternativeText: 'Actions',
        class: "tripledots"
    },
},
];

export default class ApplicationMonitoring extends LightningElement {
    @track siteaddress;
    @track selectperiod;
    @track columns = columns;
    @track visitationstartdate;
    @track announced;
    @track timevisit;
    @track openmodel = false;
    @track viewmodel = false;
    @track currentPageMonitoringData;
    @track norecorddisplay = true;
    @track totalRecords;
    @track totalRecordsCount = 0;
    @track page = 1;
    perpage = 10;
    setPagination = 5;
    @track addEditMonitoring = {};
    @track Program__c;
    @track ProgramType__c;
    @track SiteAddress;
    addressId;
    @track Spinner = true;
    applicationId;
    @track inspectedwithValues;
    @track monitoringID;
    @track ApplicationLicenseId__c;
    @track defaultSortDirection = 'asc';
    @track sortDirection = 'asc';
    @track sortedBy = 'VisitationStartDate__c';
    @track renewalmindate = '1970-01-01';
    @track renewalmaxdate = '1970-01-02';
    @track freezeMonitoringScreen = false;
    applicationforApiCall = this.getapplicationid;
    attachmentIcon = images + '/application-monitoring.svg';

    //Sundar - Checking SuperUser Login
    @track isSuperUserFlow = false;
    @track title;
    @track btnDisable = false;
    @track isLicenseExpired = false;
    @track isDisableSelectPeriod = false;
    @track isDisableEndDate = false;
    @track btnLabel = false;
    finalStatus;
    @track isUpdateFlag = false;

    //Function used to get application id from shared data
    get getapplicationid() {
        return sharedData.getApplicationId();
    }

    //SuperUser Login Check
    get profileName() {
        return sharedData.getUserProfileName();
    }

    //Get Site Address Value
    get siteAddressValues() {
        return [{
            label: this.SiteAddress,
            value: this.SiteAddress
        },];
    }

    //Connected Call Back method
    connectedCallback() {
        if (this.profileName === 'Supervisor')
            this.isSuperUserFlow = true;
        else
            this.isSuperUserFlow = false;

        this.monitoringDetailsLoad();

        getAddressDetails({
            applicationid: this.getapplicationid
        })
            .then(result => {
                if (result.length == 0) {
                    this.addressId = null;
                    this.SiteAddress = null;
                } else {
                    this.addressId = result[0].Id;
                    this.SiteAddress = result[0].AddressLine1__c + ',' + result[0].City__c + ',' + result[0].County__c;
                }
            }).catch(errors => {
                return this.dispatchEvent(utils.handleError(errors));
            });

        getProgramDetails({
            applicationid: this.getapplicationid
        })
            .then(result => {
                if (result.length == 0) {
                    this.Program__c = null;
                    this.ProgramType__c = null;
                    this.ApplicationLicenseId__c = null;
                } else {
                    this.Program__c = result[0].Program__c;
                    this.ProgramType__c = result[0].ProgramType__c;
                    this.ApplicationLicenseId__c = result[0].Name;
                }
            }).catch(errors => {
                return this.dispatchEvent(utils.handleError(errors));
            });
    }

    //Get Monitoring Details
    monitoringDetailsLoad() {
        getMonitoringDetails({
            applicationid: this.getapplicationid
        })
            .then(result => {
                this.totalRecordsCount = result.length;

                if (this.totalRecordsCount == 0) {
                    this.norecorddisplay = true;
                } else {
                    this.norecorddisplay = false;
                }

                this.totalRecords = result.map(row => {
                    return {
                        Id: row.Id,
                        Name: row.Name,
                        ProgramType__c: row.ProgramType__c,
                        SiteAddress: row.SiteAddress__r ? row.SiteAddress__r.AddressLine1__c + ',' + row.SiteAddress__r.City__c : null,
                        ApplicationName: row.ApplicationLicenseId__r.Name,
                        SelectPeriod__c: row.SelectPeriod__c,
                        VisitationStartDate__c: utils.formatDate(row.VisitationStartDate__c),
                        linkName: '/' + row.Id,
                        Status__c: row.Status__c,
                        SiteAddressId: row.SiteAddress__c,
                        statusClass: row.Status__c
                    }
                });

                this.pageData();
            }).catch(errors => {
                return this.dispatchEvent(utils.handleError(errors));
            });
    }

    //Function used to show the monitoring based on the pages
    pageData = () => {
        let page = this.page;
        let perpage = this.perpage;
        let startIndex = (page * perpage) - perpage;
        let endIndex = (page * perpage);
        this.currentPageMonitoringData = this.totalRecords.slice(startIndex, endIndex);
        if (this.currentPageMonitoringData.length == 0) {
            if (this.page != 1) {
                this.page = this.page - 1;
                this.pageData();
            }
        }
        this.Spinner = false;
    }

    // Used to sort the 'Age' column
    sortBy(field, reverse, primer) {
        const key = primer ?
            function (x) {
                return primer(x[field]);
            } :
            function (x) {
                return x[field];
            };

        return function (a, b) {
            a = key(a);
            b = key(b);
            return reverse * ((a > b) - (b > a));
        };
    }

    //Sorting Method
    onHandleSort(event) {
        const {
            fieldName: sortedBy,
            sortDirection
        } = event.detail;
        const cloneData = [...this.currentPageMonitoringData];
        cloneData.sort(this.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1));
        this.currentPageMonitoringData = cloneData;
        this.sortDirection = sortDirection;
        this.sortedBy = sortedBy;
    }

    //Function used to get the page number from child pagination component
    hanldeProgressValueChange(event) {
        this.page = event.detail;
        this.pageData();
    }

    //get Application Status
    @wire(getApplicationProviderStatus, {
        applicationId: '$applicationforApiCall'
    })
    applicationStatus({
        error,
        data
    }) {
        if (data) {
            if (data.length > 0 && (data[0].Status__c == "Caseworker Submitted" || data[0].Status__c == "Approved" || data[0].Status__c == "Supervisor Rejected" || data[0].Status__c == "Rejected" || data[0].Status__c == "Appeal")) {
                this.freezeMonitoringScreen = true;
                this.finalStatus = data[0].Status__c;
            } else {
                this.freezeMonitoringScreen = false;
            }
        }
    }

    //get Application License Status
    @wire(getApplicationLicenseStatus, {
        applicationId: '$applicationforApiCall'
    })
    licenseStatus({
        error,
        data
    }) {
        if (data) {
            if (data.length > 0 && data[0].Status__c == "License Expired") {
                this.isLicenseExpired = true;
            } else {
                this.isLicenseExpired = false;
            }
        }
    }

    //Declare Monitoring object into one variable
    @wire(getObjectInfo, {
        objectApiName: MONITORING_OBJECT
    })
    objectInfo;

    //Function used to get all picklist values from Monitoring object
    @wire(getPicklistValuesByRecordType, {
        objectApiName: MONITORING_OBJECT,
        recordTypeId: '$objectInfo.data.defaultRecordTypeId'
    })
    getAllPicklistValues({
        error,
        data
    }) {
        if (data) {
            // inspectedwith
            let inspectedwithOptions = [];
            data.picklistFieldValues.JointlyInspectedWith__c.values.forEach(key => {
                inspectedwithOptions.push({
                    label: key.label,
                    value: key.value
                })
            });
            this.inspectedwithValues = inspectedwithOptions;

            //timevisitValues
            let timevisitOptions = [];
            data.picklistFieldValues.TimetoVisit__c.values.forEach(key => {
                timevisitOptions.push({
                    label: key.label,
                    value: key.value
                })
            });
            this.timevisitValues = timevisitOptions;

            //selectPeriodValues
            let selectPeriodOptions = [];
            if (this.isLicenseExpired) {
                data.picklistFieldValues.SelectPeriod__c.values.forEach(key => {
                    selectPeriodOptions.push({
                        label: key.label,
                        value: key.value
                    })
                });
            } else {
                data.picklistFieldValues.SelectPeriod__c.values.forEach(key => {
                    if (key.label != 'Re-Licensure') {
                        selectPeriodOptions.push({
                            label: key.label,
                            value: key.value
                        })
                    }
                });
            }
            this.selectPeriodValues = selectPeriodOptions;

            //selectAnnouncedValues
            let selectAnnouncedOptions = [];
            data.picklistFieldValues.AnnouncedUnannouced__c.values.forEach(key => {
                selectAnnouncedOptions.push({
                    label: key.label,
                    value: key.value
                })
            });
            this.selectAnnouncedValues = selectAnnouncedOptions;
        } else if (error) {
            this.dispatchEvent(utils.toastMessage("Error in fetching record", "error"));
            this.error = JSON.stringify(error);
        }
    }

    //Sundar Modifying this functionality
    openmodal() {
        if (this.freezeMonitoringScreen)
            return this.dispatchEvent(utils.toastMessage(`Cannot add monitoring for ${this.finalStatus} application`, "warning"));

        this.title = 'ADD MONITORING';
        this.btnLabel = 'SAVE';
        this.addEditMonitoring = {
            SelectPeriod__c: this.totalRecordsCount == 0 ? 'Initial' : (this.isLicenseExpired === true ? 'Re-Licensure' : '')
        };
        this.isDisableSelectPeriod = this.totalRecordsCount == 0 ? true : false;
        this.isDisableEndDate = true;
        this.monitoringID = null;
        this.btnDisable = true;
        this.openmodel = true;
        this.viewmodel = false;
    }

    //Closing the Modal
    closeModal() {
        this.openmodel = false;
        this.viewmodel = false;
    }

    //Function used to edit the existing monitoring 
    handleRowAction(event) {
        let monitoringrowid = event.detail.row.Id;

        if (event.detail.action.name == undefined) {
            sharedData.setMonitoringId(event.detail.row.Id);
            sharedData.setMonitoringAddressId(event.detail.row.SiteAddressId);
            sharedData.setMonitoringStatus(event.detail.row.Status__c);
            const oncaseid = new CustomEvent('redirecteditmonitering', {
                detail: {
                    first: false
                }
            });
            this.dispatchEvent(oncaseid);
        } else if (event.detail.action.name == "Edit") {
            getMonitoringStatus({
                monitoringid: monitoringrowid
            }).then(result => {
                if (result[0].Status__c == 'Completed')
                    return this.dispatchEvent(utils.toastMessage(`Completed monitoring cant be edited.`, "warning"));

                if (this.freezeMonitoringScreen)
                    return this.dispatchEvent(utils.toastMessage(`Cannot edit monitoring for ${this.finalStatus} application`, "warning"));

                this.title = 'EDIT MONITORING';
                this.btnLabel = 'UPDATE';
                this.isDisableEndDate = false;
                this.isDisableSelectPeriod = false;
                this.openmodel = true;
                this.btnDisable = false;
                this.viewmodel = false;
                editMonitoringDetails({
                    editMonitoringDetails: monitoringrowid
                }).then(result => {
                    this.Program__c = result[0].Program__c;
                    this.ProgramType__c = result[0].ProgramType__c;
                    this.addEditMonitoring.ApplicationLicenseId__c = result[0].ApplicationLicenseId__c;
                    this.SiteAddress__c = result[0].SiteAddress__c;
                    this.addEditMonitoring.SelectPeriod__c = result[0].SelectPeriod__c;
                    this.addEditMonitoring.VisitationStartDate__c = result[0].VisitationStartDate__c;
                    this.addEditMonitoring.VisitationEndDate__c = result[0].VisitationEndDate__c;
                    this.addEditMonitoring.AnnouncedUnannouced__c = result[0].AnnouncedUnannouced__c;
                    this.addEditMonitoring.TimetoVisit__c = result[0].TimetoVisit__c;
                    this.addEditMonitoring.JointlyInspectedWith__c = result[0].JointlyInspectedWith__c;
                    this.monitoringID = result[0].Id;
                    this.renewalmindate = result[0].VisitationStartDate__c;
                    this.renewalmaxdate = result[0].VisitationStartDate__c + 20;
                }).catch(errors => {
                    return this.dispatchEvent(utils.handleError(errors));
                });
            }).catch(errors => {
                return this.dispatchEvent(utils.handleError(errors));
            });
        } else if (event.detail.action.name == "View") {
            this.title = 'VIEW MONITORING';
            this.openmodel = false;
            this.viewmodel = true;
            viewMonitoringDetails({
                viewMonitoringDetails: monitoringrowid
            }).then(result => {
                this.Program__c = result[0].Program__c;
                this.ProgramType__c = result[0].ProgramType__c;
                this.ApplicationLicenseId__c = this.ApplicationLicenseId__c;
                this.SiteAddress__c = result[0].SiteAddress__c;
                this.addEditMonitoring.SelectPeriod__c = result[0].SelectPeriod__c;
                this.addEditMonitoring.VisitationStartDate__c = result[0].VisitationStartDate__c;
                this.addEditMonitoring.VisitationEndDate__c = result[0].VisitationEndDate__c;
                this.addEditMonitoring.AnnouncedUnannouced__c = result[0].AnnouncedUnannouced__c;
                this.addEditMonitoring.TimetoVisit__c = result[0].TimetoVisit__c;
                this.addEditMonitoring.JointlyInspectedWith__c = result[0].JointlyInspectedWith__c;
                this.monitoringID = result[0].Id;
            }).catch(errors => {
                return this.dispatchEvent(utils.handleError(errors));
            });
        }
    }

    //On Change functionality
    handleChange(event) {
        this.btnDisable = false;

        if (event.target.name == 'Program__c') {
            this.Program__c = event.target.value;
        } else if (event.target.name == 'ProgramType__c') {
            this.ProgramType__c = event.target.value;
        } else if (event.target.name == 'SiteAddress__c') {
            this.SiteAddress__c = event.target.value;
        } else if (event.target.name == 'SelectPeriod__c') {
            this.addEditMonitoring.SelectPeriod__c = event.target.value;
        } else if (event.target.name == 'VisitationStartDate__c') {
            this.addEditMonitoring.VisitationStartDate__c = event.target.value;
            this.renewalmindate = event.target.value;
            this.renewalmaxdate = event.target.value + 20;
            this.isDisableEndDate = false;
        } else if (event.target.name == 'VisitationEndDate__c') {
            this.addEditMonitoring.VisitationEndDate__c = event.target.value;
        } else if (event.target.name == 'AnnouncedUnannouced__c') {
            this.addEditMonitoring.AnnouncedUnannouced__c = event.target.value;
        } else if (event.target.name == 'TimetoVisit__c') {
            this.addEditMonitoring.TimetoVisit__c = event.target.value;
        } else if (event.target.name == 'JointlyInspectedWith__c') {
            this.addEditMonitoring.JointlyInspectedWith__c = event.target.value;
        }
    }

    //Sundar modifying this functionality validation
    handleSave() {
        if (!this.Program__c)
            return this.dispatchEvent(utils.toastMessage("Program is mandatory", "warning"));

        if (!this.ProgramType__c)
            return this.dispatchEvent(utils.toastMessage("Program Type is mandatory", "warning"));

        if (!this.SiteAddress)
            return this.dispatchEvent(utils.toastMessage("Site Address is mandatory", "warning"));

        if (!this.ApplicationLicenseId__c)
            return this.dispatchEvent(utils.toastMessage("Application License is mandatory", "warning"));

        if (this.addEditMonitoring && !this.addEditMonitoring.SelectPeriod__c)
            return this.dispatchEvent(utils.toastMessage("Select Frequency is mandatory", "warning"));

        if (this.addEditMonitoring && !this.addEditMonitoring.VisitationStartDate__c)
            return this.dispatchEvent(utils.toastMessage("Visitation Start Date is mandatory", "warning"));

        if (this.addEditMonitoring && this.addEditMonitoring.VisitationEndDate__c && (new Date(this.addEditMonitoring.VisitationStartDate__c).getTime() > new Date(this.addEditMonitoring.VisitationEndDate__c).getTime()))
            return this.dispatchEvent(utils.toastMessage("Visitation Start Date should be greater than Visitation End Date", "warning"));

        this.btnDisable = true;

        this.addEditMonitoring.sobjectType = 'Monitoring__c';
        this.addEditMonitoring.SiteAddress__c = this.addressId;
        this.addEditMonitoring.ApplicationLicenseId__c = this.getapplicationid;

        if (this.monitoringID) {
            this.isUpdateFlag = true;
            this.addEditMonitoring.Id = this.monitoringID;
        } else {
            this.isUpdateFlag = false;
            this.addEditMonitoring.Status__c = 'Draft';
        }

        InsertUpdateMonitoring({
            objSobjecttoUpdateOrInsert: this.addEditMonitoring
        })
            .then(result => {
                if (this.isUpdateFlag)
                    this.dispatchEvent(utils.toastMessage("Application Monitoring has been updated successfully", "success"));
                else
                    this.dispatchEvent(utils.toastMessage("Application Monitoring has been added successfully", "success"));

                this.addEditMonitoring = {};
                this.btnDisable = false;
                this.openmodel = false;
                this.monitoringDetailsLoad();
            }).catch(errors => {
                this.btnDisable = false;
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.APPLICATION_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
    }
}