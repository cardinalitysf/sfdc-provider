/**
 * @Author        : Naveen S
 * @CreatedOn     : July 02 ,2020
 * @Purpose       : Complaints Monitering Records for Staff / Plant and Office Inspection
 * @updatedBy     : Naveen S
 * @updatedOn     : July 02 ,2020
 **/
import {
    LightningElement,
    track,
    wire,
    api
} from 'lwc';

// Utils
import {
    utils
} from 'c/utils';
import * as sharedData from "c/sharedData";
import {
    loadStyle
} from 'lightning/platformResourceLoader';
import myResource from '@salesforce/resourceUrl/styleSheet';
import {
    constPopupVariables
} from 'c/constants';

//For Create, Refresh Records in Table utils
import {
    refreshApex
} from "@salesforce/apex";
import {
    createRecord,
    updateRecord
} from "lightning/uiRecordApi";
import {
    deleteRecord
} from "lightning/uiRecordApi";
import {
    CurrentPageReference
} from 'lightning/navigation';
import {
    fireEvent
} from 'c/pubsub';
import { CJAMS_CONSTANTS } from 'c/constants';

import REFVALUE_OBJECT from '@salesforce/schema/ReferenceValue__c';
import REGULATIONS_FIELD from '@salesforce/schema/ReferenceValue__c.Regulations__c';
import QUESTION_FIELD from '@salesforce/schema/ReferenceValue__c.Question__c';
import TYPE_FIELD from '@salesforce/schema/ReferenceValue__c.Type__c';
import RECORDTYPEID_FIELD from '@salesforce/schema/ReferenceValue__c.RecordTypeId';

//Record Fields
import PROVIDERRECANS_OBJECT from '@salesforce/schema/ProviderRecordAnswer__c';
import ID_FIELD from '@salesforce/schema/ProviderRecordAnswer__c.Id';
import COMAR_FIELD from '@salesforce/schema/ProviderRecordAnswer__c.Comar__c';
import COMPLIANCE_FIELD from '@salesforce/schema/ProviderRecordAnswer__c.Compliance__c';
import DATE_FIELD from '@salesforce/schema/ProviderRecordAnswer__c.Date__c';
import FINDINGS_FIELD from '@salesforce/schema/ProviderRecordAnswer__c.Findings__c';
import FREQUENCYOFDEF_FIELD from '@salesforce/schema/ProviderRecordAnswer__c.FrequencyofDeficiency__c';
import IMPACTOFDEF_FIELD from '@salesforce/schema/ProviderRecordAnswer__c.ImpactofDeficiency__c';
import SCOPEOFDEF_FIELD from '@salesforce/schema/ProviderRecordAnswer__c.Scope_of_Deficiency__c';
import PROVIDERRECQUES_FIELD from '@salesforce/schema/ProviderRecordAnswer__c.ProviderRecordQuestion__c';
import PROVIDERRECQUES_OBJECT from '@salesforce/schema/ProviderRecordQuestion__c';
import MONITERIG_FIELD from '@salesforce/schema/ProviderRecordQuestion__c.Monitoring__c';
import STAFFQUES_FIELD from '@salesforce/schema/ProviderRecordQuestion__c.Name__c';
import STAFFQUESACTOR_FIELD from '@salesforce/schema/ProviderRecordQuestion__c.Actor__c';
import STATUS_FIELD from '@salesforce/schema/ProviderRecordQuestion__c.Status__c';
import SITE_FIELD from '@salesforce/schema/ProviderRecordQuestion__c.Site__c';
import RECTYPEQUES_FIELD from '@salesforce/schema/ProviderRecordQuestion__c.RecordTypeId';
import DATEQUES_FIELD from '@salesforce/schema/ProviderRecordQuestion__c.Date__c';
import NCQUES_FIELD from '@salesforce/schema/ProviderRecordQuestion__c.ANYNC__c';
import PENDGQUES_FIELD from '@salesforce/schema/ProviderRecordQuestion__c.ANYPending__c';
import IDQUES_FIELD from '@salesforce/schema/ProviderRecordQuestion__c.Id';

//Apex Classes
import getUserEmail from '@salesforce/apex/providerApplication.getUserEmail';
import getRecordDetails from '@salesforce/apex/ComplaintsMoniteringComarRecords.getRecordDetails';
import getQuestionsByType from '@salesforce/apex/ComplaintsMoniteringComarRecords.getQuestionsByType';
import getStaffFromApplicationId from "@salesforce/apex/ComplaintsMoniteringComarRecords.getStaffFromApplicationId";
import bulkAddRecordAns from "@salesforce/apex/ComplaintsMoniteringComarRecords.bulkAddRecordAns";
import getSiteId from "@salesforce/apex/ComplaintsMoniteringComarRecords.getSiteId";
import editRecordDet from "@salesforce/apex/ComplaintsMoniteringComarRecords.editRecordDet";
import getRecordType from "@salesforce/apex/ComplaintsMoniteringComarRecords.getRecordType";
import bulkUpdateRecordAns from "@salesforce/apex/ComplaintsMoniteringComarRecords.bulkUpdateRecordAns";
import images from '@salesforce/resourceUrl/images';
import getProviderType from "@salesforce/apex/ComplaintsMoniteringComarRecords.getProviderType";
import getHouseholdMembers from "@salesforce/apex/ComplaintsMoniteringComarRecords.getHouseholdMembers";

export default class ComplaintsMoniteringComarRecords extends LightningElement {

    //Get Active Tab Record
    @api getActiveTab = null;
    @track hasStaffEnable = true;
    @track hasStaffCreate = false;
    @track staffLists = [];
    @track staffId = null;
    @track disableFields = false;
    @track Spinner = true;
    //DataTable Data
    @track dataTableShow = true;
    @track columns = [];
    @track tabContent = '';
    @track wiredRecordDetails = [];
    wiredEditReg;
    @track totalRecords = [];
    @track datas = [];
    @api siteId = null;
    @track programSiteName = null;
    @api recordTypeId = null;
    @track deleteLoader = false;

    //Label for New Record Creation
    @track newRecordCreate = null;
    @track openModel = false;
    @track createComar = {};
    @track createNewRecords = [];
    @api type = null;
    @track recordNewModel;
    @track questions = [];
    @track editedQuestions = [];
    questionsRegulations;
    @api updateMode = false;
    @track deleteRowId = null;
    @track openmodelDel = false;
    @api editRecordId ;
    @track btnEnable = true;
    @track hideAddButton = false;
    attachmentIcon = images + '/application-licenseinfo.svg';

    //Pagination Tracks    
    @track page = 1;
    perpage = 10;
    setPagination = 5;
    @track totalRecordsCount = 0;
    @track providerType;

    //DataTable RowAction Method Start
    constructor() {
        super();
        this.columns = [{
            label: 'Any NC',
            fieldName: 'ANYNC__c',
            sortable: true
        },
        {
            label: 'Any Pending',
            fieldName: 'ANYPending__c',
        },
        {
            label: 'Update Date',
            fieldName: 'Date__c'
        },
        {
            label: 'Updated By',
            fieldName: 'LastModifiedBy',
            initialWidth: 220
        },
        {
            label: 'Status',
            fieldName: 'Status__c',
            type: 'text',
            cellAttributes: {
                class: {
                    fieldName: 'statusClass'
                }
            }
        },
        {
            label: 'Action',
            type: 'button',
            initialWidth: 100,
            fieldName: 'id',
            typeAttributes: {
                iconName: 'utility:preview',
                name: 'viewRecord',
                title: 'view',
                disabled: false,
                initialWidth: 20,
                target: '_self'
            }
        }, {
            type: 'button',
            initialWidth: 40,
            fieldName: 'id',
            typeAttributes: {
                iconName: 'utility:edit',
                name: 'editRecord',
                title: 'Edit',
                disabled: false,
                initialWidth: 20,
                class: "action-position"
            }
        }, {
            type: 'button',
            fieldName: 'id',
            initialWidth: 40,
            typeAttributes: {
                iconName: 'utility:delete',
                name: 'deleteRecordRow',
                title: 'Delete',
                disabled: false,
                initialWidth: 20,
                class: "del-red action-positiontrd",
            }
        }
        ]
    }
    @wire(CurrentPageReference) pageRef;
    // Sorting the Data Table Column Function End
    sortBy(field, reverse, primer) {
        const key = primer ?
            function (x) {
                return primer(x[field]);
            } :
            function (x) {
                return x[field];
            };

        return function (a, b) {
            a = key(a);
            b = key(b);
            if (a === undefined) a = '';
            if (b === undefined) b = '';
            a = typeof (a) === 'number' ? a : a.toLowerCase();
            b = typeof (b) === 'number' ? b : b.toLowerCase();

            return reverse * ((a > b) - (b > a));
        };
    }

    onHandleSort(event) {


        const {
            fieldName: sortedBy,
            sortDirection: sortDirection
        } = event.detail;
        const cloneData = [...this.datas];

        cloneData.sort(this.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1));
        this.datas = cloneData;
        this.sortDirection = sortDirection;
        this.sortedBy = sortedBy;

    }

    // Sorting the Data Table Column Function End
    get moniteringId() {
        return sharedData.getMonitoringId();
        //return 'a0K0p000001LzSUEA0'
    }
    get applicationId() {
        return sharedData.getApplicationId();
    }

    get monitoringStatus() {
        return sharedData.getMonitoringStatus();
        //return 'Draft';
    }

    get getProviderId() {
        return sharedData.getProviderId();
        //return '0010p00000ixw1pAAA';

    }
    //Depending the Logged In User Getting the Monitering Id of the User Function End

    connectedCallback() {



        if (this.userProfileName == 'Supervisor') {
            this.hideAddButton = true;

        } else {
            this.hideAddButton = false;
        }

        if (this.getActiveTab == 'officeTab') {
            this.newRecordCreate = 'Add Office Record';
            this.type = 'Office';
            this.popUpForSuccess = constPopupVariables[this.getActiveTab];
            this.popUpForUpdate = constPopupVariables[this.getActiveTab + 'Update'];
            this.hasStaffEnable = false;
            this.columns.unshift({
                label: 'Site',
                type: 'button',
                sortable: true,
                typeAttributes: {
                    name: 'dontRedirect',
                    label: {
                        fieldName: 'siteAddress'
                    }
                },
                cellAttributes: {
                    class: "title"
                }
            })

        } else if (this.getActiveTab == 'plantTab') {
            this.newRecordCreate = 'Add Site Record';
            this.type = 'Physical Plant';
            this.popUpForSuccess = constPopupVariables[this.getActiveTab];
            this.popUpForUpdate = constPopupVariables[this.getActiveTab + 'Update'];
            this.hasStaffEnable = false;
            this.columns.unshift({
                label: 'Site',
                type: 'button',
                sortable: true,
                typeAttributes: {
                    name: 'dontRedirect',
                    label: {
                        fieldName: 'siteAddress'
                    }
                },
                cellAttributes: {
                    class: "title"
                }
            })
        } else {
            this.getProviderTypeDetails();
            this.newRecordCreate = 'Add New Staff Record';
            this.type = 'Staff Record';
            this.popUpForSuccess = constPopupVariables.staffTab;
            this.popUpForUpdate = constPopupVariables.staffTabUpdate;
            this.hasStaffEnable = true;
            this.columns.unshift({
                label: 'Name',
                fieldName: 'FirstName__c',
                sortable: true
            })
        }
        if (this.monitoringStatus !== 'Draft')
            this.disableFields = true
    }

    /* Style Sheet Loading */
    renderedCallback() {
        Promise.all([
            loadStyle(this, myResource + '/styleSheet.css')
        ])
    }

    pageData() {
        let page = this.page;
        let perpage = this.perpage;
        let startIndex = page * perpage - perpage;
        let endIndex = page * perpage;
        this.datas = [...this.totalRecords.slice(startIndex, endIndex)];
    }

    //Get Record Detais of Staff / Physical Plant / Office Function Start
    get siteId() {
        //  return 'a080p00000N4kvPAAR';       
        return sharedData.getMonitoringAddressId();

    }

    @wire(getSiteId, {
        siteId: '$siteId'
    })
    siteDetails(data) {
        try {
            if (data.data != undefined && data.data.length > 0) {
                this.programSiteName = data.data[0].AddressLine1__c;
            }

        }
        catch (error) {
            if (error) {
                let error = JSON.parse(error.body.message);
                const { title, message, errorType } = error;
                this.dispatchEvent(
                    utils.toastMessageWithTitle(title, message, errorType)
                )
            }
            else {
                this.dispatchEvent(
                    utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                );
            }
        }
    }

    @wire(getRecordType, {
        name: '$type'
    })
    recordTypeDetails(data) {

        if (data.data != undefined && data.data.length > 0) {

            this.recordTypeId = data && data.data;
        }

    }

    @wire(getRecordDetails, {
        moniteringId: '$moniteringId',
        siteId: '$siteId',
        recordType: '$recordTypeId'
    })
    recordDetails(data) {

        try {
            this.wiredRecordDetails = data;

            if (data.data != null && data.data != '') {

                this.dataTableShow = true;
                let currentData = [];
                //this.programSiteName = data.data[0].Site__r.AddressLine1__c;
                data.data.forEach((row) => {
                    let rowData = {};
                    rowData.id = row.Id;
                    rowData.Monitoring__c = row.Monitoring__c;
                    rowData.siteAddress = row.Site__r.AddressLine1__c;
                    //  rowData.siteAddress = null;
                    rowData.ANYNC__c = row.ANYNC__c;
                    rowData.ANYPending__c = row.ANYPending__c;
                    rowData.Status__c = row.Status__c;
                    rowData.LastModifiedBy = row.LastModifiedBy.Name;

                    rowData.FirstName__c = row.Name__r == undefined ?
                        (row.Actor__r !== undefined ? row.Actor__r.Contact__r.FirstName__c : "") : row.Name__r.FirstName__c;

                    rowData.Name__c = row.Name__c == undefined ? " " : row.Name__c;
                    rowData.Date__c = utils.formatDate(row.Date__c);
                    rowData.statusClass = row.Status__c === 'Completed' ? 'Approved' : row.Status__c;
                    currentData.push(rowData);
                });
                this.totalRecords = currentData;
                this.totalRecordsCount = this.totalRecords.length;
                this.Spinner = false;
                this.pageData();
            } else {
                this.dataTableShow = false;
            }

        }
        catch (error) {
            if (error) {
                let error = JSON.parse(error.body.message);
                const { title, message, errorType } = error;
                this.dispatchEvent(
                    utils.toastMessageWithTitle(title, message, errorType)
                )
            }
            else {
                this.dispatchEvent(
                    utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                );
            }
        }

    }
    //Get Record Detais of Staff / Physical Plant / Office Function End

    //New Record Create for Staff / Physical Plant / Office Function Start

    /* New Record Creation */
    createNewRecordModelPop(event) {

        if (this.getActiveTab == 'officeTab' || this.getActiveTab == 'plantTab' || this.hasStaffCreate == true) {
            if (event.target.name == 'staff')
                this.staffId = event.target.value;
            if (this.getActiveTab == 'officeTab')
                this.type = 'Office';
            else if (this.getActiveTab == 'plantTab')
                this.type = 'Physical Plant';
            else this.type = 'Staff Record';
            if (this.type == 'Staff Record') {
                if (this.staffId == null)
                    return;
            }
            this.recordNewModel = true;
            this.hasStaffCreate = false;
            this.btnEnable = true;
            refreshApex(this.questionsRegulations);

        } else {
            //this.staffListLoad();
            this.hasStaffCreate = true;

        }
        this.Spinner = false;
    }

    closeNewRecord() {
        this.recordNewModel = false;
        this.btnEnable = true;
        this.updateMode = false;
        this.staffId = null;
        this.type = '';
    }


    @wire(getQuestionsByType, {
        type: '$type'
    })
    wiredquestions(data) {

        // try {
        this.questionsRegulations = data;
        if (data.data != null && data.data != '') {
            let currentData = [];
            let id = 1;
            this.questions = [];
            data.data.forEach((row) => {
                let rowData = {};
                rowData.rowId = id;
                rowData.RegulationId = row.Id;
                rowData.Regulations__c = row.Regulations__c;
                rowData.Question__c = row.Question__c;
                rowData.Compliance__c = 'Pending';
                rowData.Date__c = utils.formatDateYYYYMMDD(new Date());
                rowData.Findings__c = null;
                rowData.FrequencyofDeficiency__c = null;
                rowData.ImpactofDeficiency__c = null;
                rowData.Scope_of_Deficiency__c = null;
                rowData.ProviderRecordQuestion__c = null;
                currentData.push(rowData);
                id++;
            });
            this.questions = currentData;
            this.Spinner = false;
        }
    }
    // 

    //New Record Create for Staff / Physical Plant / Office Function End    

    /* On-Click Button Change Functionlity on New Record Creation Start */
    openNewRow(event) {
        if (event.target != undefined && event.target.name != undefined && event.target.name != null) {
            let allD = this.template.querySelectorAll('tbody');
            allD[event.target.name - 1].rows[1].className = 'displayContent';
            allD[event.target.name - 1].rows[0].cells[1].childNodes.forEach((but) => {
                but.className = but.className.includes('backgroComboColor') == true ? but.className.replace(' backgroComboColor', '') : but.className;
                but.className = 2 === parseInt(but.dataset.id, 10) ? but.className + ' backgroComboColor' : but.className;
            });
            this.questions[event.target.name - 1].Compliance__c = 'Non-Compliance';
        } else {

            let index = parseInt(event);
            let allD = this.template.querySelectorAll('tbody');
            allD[index].rows[1].className = 'displayContent';
            allD[index].rows[0].cells[1].childNodes.forEach((but) => {
                but.className = but.className.includes('backgroComboColor') == true ? but.className.replace(' backgroComboColor', '') : but.className;
                but.className = 2 === parseInt(but.dataset.id, 10) ? but.className + ' backgroComboColor' : but.className;
            });
        }
    }

    selectedCompliance(event) {
        if (event.target != undefined && event.target.name != undefined && event.target.name != null) {
            let allD = this.template.querySelectorAll('tbody');
            allD[event.target.name - 1].rows[1].className = 'displayNone';
            allD[event.target.name - 1].rows[0].cells[1].childNodes.forEach((but) => {
                but.className = but.className.includes('backgroComboColor') == true ? but.className.replace(' backgroComboColor', '') : but.className;
                but.className = 1 === parseInt(but.dataset.id, 10) ? but.className + ' backgroComboColor' : but.className;
            });
            this.questions[event.target.name - 1].Compliance__c = 'Compliance';
        } else {

            let index = parseInt(event);
            let allD = this.template.querySelectorAll('tbody');
            allD[index].rows[1].className = 'displayNone';
            allD[index].rows[0].cells[1].childNodes.forEach((but) => {
                but.className = but.className.includes('backgroComboColor') == true ? but.className.replace(' backgroComboColor', '') : but.className;
                but.className = 1 === parseInt(but.dataset.id, 10) ? but.className + ' backgroComboColor' : but.className;
            });
        }
    }

    selectedNonApplicable(event) {
        if (event.target != undefined && event.target.name != undefined && event.target.name != null) {
            let allD = this.template.querySelectorAll('tbody');
            allD[event.target.name - 1].rows[1].className = 'displayNone';
            allD[event.target.name - 1].rows[0].cells[1].childNodes.forEach((but) => {
                but.className = but.className.includes('backgroComboColor') == true ? but.className.replace(' backgroComboColor', '') : but.className;
                but.className = 3 === parseInt(but.dataset.id, 10) ? but.className + ' backgroComboColor' : but.className;
            });
            this.questions[event.target.name - 1].Compliance__c = 'Not Applicable';
        } else {
            let index = parseInt(event);
            let allD = this.template.querySelectorAll('tbody');
            allD[index].rows[1].className = 'displayNone';
            allD[index].rows[0].cells[1].childNodes.forEach((but) => {
                but.className = but.className.includes('backgroComboColor') == true ? but.className.replace(' backgroComboColor', '') : but.className;
                but.className = 3 === parseInt(but.dataset.id, 10) ? but.className + ' backgroComboColor' : but.className;
            });
        }
    }

    selectedPending(event) {
        if (event.target != undefined && event.target.name != undefined && event.target.name != null) {
            let allD = this.template.querySelectorAll('tbody');
            allD[event.target.name - 1].rows[1].className = 'displayNone';
            allD[event.target.name - 1].rows[0].cells[1].childNodes.forEach((but) => {
                but.className = but.className.includes('backgroComboColor') == true ? but.className.replace(' backgroComboColor', '') : but.className;
                but.className = 4 === parseInt(but.dataset.id, 10) ? but.className + ' backgroComboColor' : but.className;
            });
            this.questions[event.target.name - 1].Compliance__c = 'Pending';
        } else {
            let index = parseInt(event);
            let allD = this.template.querySelectorAll('tbody');
            allD[index].rows[1].className = 'displayNone';
            allD[index].rows[0].cells[1].childNodes.forEach((but) => {
                but.className = but.className.includes('backgroComboColor') == true ? but.className.replace(' backgroComboColor', '') : but.className;
                but.className = 4 === parseInt(but.dataset.id, 10) ? but.className + ' backgroComboColor' : but.className;
            });
        }
    }
    /* On-Click Button Change Functionlity on New Record Creation End */

    /* Non - Comliance Row Functions Start */

    get frequencyDefis() {
        return [{
            label: 'Initial',
            value: 'Initial'
        },
        {
            label: 'Repeat',
            value: 'Repeat'
        },
        {
            label: 'Chronic',
            value: 'Chronic'
        },
        ];
    }

    get impactDefis() {
        return [{
            label: 'Minor',
            value: 'Minor'
        },
        {
            label: 'Major',
            value: 'Major'
        },
        {
            label: 'Serious',
            value: 'Serious'
        }
        ]
    }

    get scopeDefis() {
        return [{
            label: 'Isolated',
            value: 'Isolated'
        },
        {
            label: 'Narrow',
            value: 'Narrow'
        },
        {
            label: 'Moderate',
            value: 'Moderate'
        },
        {
            label: 'Broad',
            value: 'Broad'
        }
        ]
    }

    /* Non - Comliance Row Functions End */

    /* New Comar Add POP-UP Function Start */

    addRegulationPopup() {
        this.openModel = true;
    }

    closeModal() {
        this.openModel = false;
        this.createComar = {};
        this.openmodelDel = false;
    }

    comarOnChange(event) {
        if (event.target.name == 'regulations') {
            this.createComar.regulations = event.detail.value;
        } else if (event.target.name == 'question') {
            this.createComar.question = event.detail.value;
        }
    }

    createNewComar() {
        const allValid = [
            ...this.template.querySelectorAll('section lightning-input')
        ]
            .reduce((validSoFar, inputFields) => {
                inputFields.reportValidity();
                return validSoFar && inputFields.checkValidity();
            }, true);
        this.createComar.type = this.type;
        if (allValid) {
            let fields = {};

            fields[REGULATIONS_FIELD.fieldApiName] = this.createComar.regulations;
            fields[QUESTION_FIELD.fieldApiName] = this.createComar.question;
            fields[TYPE_FIELD.fieldApiName] = this.createComar.type;
            //fields[RECORDTYPEID_FIELD.fieldApiName] = this.recordTypeId;
            const recordInput = {
                apiName: REFVALUE_OBJECT.objectApiName,
                fields
            };

            createRecord(recordInput)
                .then(result => {
                    this.openModel = false;

                    this.createComar = {};

                    fireEvent(this.pageRef, "RefreshActivityPage", true);
                    this.dispatchEvent(utils.toastMessage(constPopupVariables.comarSave, "success"));
                    return refreshApex(this.questionsRegulations);
                    //this.fetchQuestions();
                })
                .catch(error => {
                    this.dispatchEvent(utils.toastMessage(constPopupVariables.savingErrorRec, "error"));
                })
        }
    }
    /* New Comar Add POP-UP Function End */

    //Edit Record Details Function Datatable Start
    @wire(editRecordDet, {
        questionId: '$editRecordId'
    })

    wiredEditRecords(result) {
        try {

            this.wiredEditReg = result;
            if (result.data != undefined && result.data.length > 0) {
                this.recordNewModel = true;
                let currentData = [];
                //let i = 1;
                //result.forEach(i => {
                for (let i = 0; i < result.data.length; i++) {
                    currentData.push({
                        rowId: i + 1,
                        Id: result.data[i].Id,
                        RegulationId: result.data[i].Comar__c,
                        Regulations__c: result.data[i].Comar__r != undefined ? result.data[i].Comar__r.Regulations__c : '',
                        Question__c: result.data[i].Comar__r != undefined ? result.data[i].Comar__r.Question__c : '',
                        Compliance__c: result.data[i].Compliance__c,
                        Date__c: result.data[i].Date__c,
                        Findings__c: result.data[i].Findings__c,
                        FrequencyofDeficiency__c: result.data[i].FrequencyofDeficiency__c,
                        ImpactofDeficiency__c: result.data[i].ImpactofDeficiency__c,
                        Scope_of_Deficiency__c: result.data[i].Scope_of_Deficiency__c,
                        ProviderRecordQuestion__c: result.data[i].ProviderRecordQuestion__c
                    });
                } //);

                this.questions = currentData;
                this.editedQuestions = [...this.questions];
                setTimeout(() => {
                    this.rowBtnChangeFn();
                }, 0);
                //this.rowBtnChangeFn();

            }
        }
        catch (error) {
            if (error) {
                let error = JSON.parse(error.body.message);
                const { title, message, errorType } = error;
                this.dispatchEvent(
                    utils.toastMessageWithTitle(title, message, errorType)
                )
            }
            else {
                this.dispatchEvent(
                    utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                );
            }
        }
    }

    handleRowAction(event) {
        if (event.detail.action.name === 'dontRedirect') return;
        const action = event.detail.action;
        const row = event.detail.row;
        switch (action.name) {
            case 'editRecord':
                if (this.disableFields == true)
                    return this.dispatchEvent(utils.toastMessage(constPopupVariables.UNEDIT, "warning"));
                let doAny = 'do';
                if (this.editRecordId != null && row.id == this.editRecordId)
                    doAny = 'dont';
                this.editRecordId = row.id;
                let data = this.datas.filter(q => q.id == row.id);

                this.btnEnable = data[0].Status__c == 'Completed' ? false : true;
                this.updateMode = true;
                this.recordNewModel = true;
                if (doAny == 'dont') {
                    this.questions = this.editedQuestions;
                    setTimeout(() => {
                        this.rowBtnChangeFn();
                    }, 0);
                    break;
                } else {
                    refreshApex(this.wiredEditReg);
                    break;
                }
            case 'deleteRecordRow':
                if (this.disableFields == true)
                    return this.dispatchEvent(utils.toastMessage(constPopupVariables.UNDELETE, "warning"));
                this.deleteRowId = row.id;
                this.openmodelDel = true;
                break;
            case 'viewRecord':
                this.disableFields = true;
                let doAnyA = 'do';
                if (this.editRecordId != null && row.id == this.editRecordId)
                    doAnyA = 'dont';
                this.editRecordId = row.id;
                this.btnEnable = false;
                this.updateMode = true;
                this.recordNewModel = true;
                if (doAnyA == 'dont') {
                    this.questions = this.editedQuestions;
                    setTimeout(() => {
                        this.rowBtnChangeFn();
                    }, 0);
                    break;
                } else {
                    refreshApex(this.wiredEditReg);
                    break;
                }
        }
    }

    btnChangeFn(id, compliance) {
        switch (compliance) {
            case 'Compliance':
                this.selectedCompliance(id);
                break;
            case 'Non-Compliance':
                this.openNewRow(id);
                break;
            case 'Not Applicable':
                this.selectedNonApplicable(id);
                break;
            case 'Pending':
                this.selectedPending(id);
                break;
        }
    }

    rowBtnChangeFn() {
        for (let ques in this.questions) {
            this.btnChangeFn(ques, this.questions[ques].Compliance__c);
        }
    }

    deleteFrSure() {
        this.deleteLoader = true;
        deleteRecord(this.deleteRowId)
            .then(() => {
                this.closeModal();
                this.dispatchEvent(utils.toastMessage(constPopupVariables.DELETESUCCESS, "success"));
                this.deleteLoader = false;
                return refreshApex(this.wiredRecordDetails);

            })

            .catch(errors => {
                if (errors) {
                    this.closeModal();
                    refreshApex(this.wiredRecordDetails);
                    let error = JSON.parse(errors.body.message);
                    const { title, message, errorType } = error;
                    this.dispatchEvent(
                        utils.toastMessageWithTitle(title, message, errorType)
                    );
                } else {
                    this.dispatchEvent(
                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                    );
                }
            });
    }

    //Edit Record Details Function Datatable End

    //For Pagination Child Bind
    hanldeProgressValueChange(event) {
        this.page = event.detail;

        this.pageData();
    }

    /* Staff List Load In Staff Record Page Function Start */
    @wire(getStaffFromApplicationId, {
        contactfromapp: '$applicationId'
    })
    wiredStaffDetails(result) {
        try {
            if (result.data) {
                let tmperrarray = [];
                result.data.forEach((staff) => {
                    let obj = {};
                    if (staff.Staff__r !== undefined && staff.Staff__r.FirstName__c != undefined) {
                        obj.label = staff.Staff__r.FirstName__c;
                        obj.value = staff.Staff__r.Id;
                        tmperrarray.push(obj);
                    }
                });
                this.staffLists = tmperrarray;
            }
        }
        catch (error) {
            if (error) {
                let error = JSON.parse(error.body.message);
                const { title, message, errorType } = error;
                this.dispatchEvent(
                    utils.toastMessageWithTitle(title, message, errorType)
                )
            }
            else {
                this.dispatchEvent(
                    utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                );
            }
        }
    }

    /*  Data Fetch from HouseHold Members Details Public Start */
    getPublicStaffDetails() {
        getHouseholdMembers({
            providerid: this.getProviderId
        }).then((data) => {

            if (data.length > 0) {
                var selectHouseholdOptions = [];
                data.forEach((row) => {
                    selectHouseholdOptions.push({
                        label: row.Contact__r.FirstName__c + ' ' + row.Contact__r.LastName__c,
                        value: row.Id
                    });
                });
                this.staffLists = selectHouseholdOptions;
            }
        })
            .catch(errors => {
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const { title, message, errorType } = error;
                    this.dispatchEvent(
                        utils.toastMessageWithTitle(title, message, errorType)
                    );
                } else {
                    this.dispatchEvent(
                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                    );
                }
            });
    }


    getProviderTypeDetails() {
        getProviderType({
            providerid: this.getProviderId
        }).then((data) => {

            if (data.length > 0) {
                this.providerType = data[0].ProviderType__c;

            }
            if (this.providerType == 'Public') {
                this.getPublicStaffDetails();
            }
        })
            .catch(errors => {
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const { title, message, errorType } = error;
                    this.dispatchEvent(
                        utils.toastMessageWithTitle(title, message, errorType)
                    );
                } else {
                    this.dispatchEvent(
                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                    );
                }
            });
    }



    /*  Data Fetch from HouseHold Members Details End */
    /* Staff List Load In Staff Record Page Function End */

    /* Create New Record Function Start */
    handleDate(event) {
        this.questions[event.target.name - 1].Date__c = event.detail.value;
    }
    handleFreq(event) {
        this.questions[event.target.name - 1].FrequencyofDeficiency__c = event.detail.value;
    }
    handleImpact(event) {
        this.questions[event.target.name - 1].ImpactofDeficiency__c = event.detail.value;
    }
    handleScope(event) {
        this.questions[event.target.name - 1].Scope_of_Deficiency__c = event.detail.value;
    }
    handlefind(event) {
        this.questions[event.target.name - 1].Findings__c = event.detail.value;
    }

    createNewRecord(event) {
        this.Spinner = true;
        this.disableFields = true;
        let nc = this.questions.filter(q => q.Compliance__c == 'Non-Compliance');
        let pending = this.questions.filter(q => q.Compliance__c == 'Pending');
        let status = event.target.name == 'draft' ? 'Draft' : 'Completed';

        if (this.updateMode == false) {
            let fields = {};

            if (this.type == 'Staff Record') {
                if (this.providerType == 'Public') {
                    fields[STAFFQUESACTOR_FIELD.fieldApiName] = this.staffId;
                }
                else {
                    fields[STAFFQUES_FIELD.fieldApiName] = this.staffId;
                }

            }

            fields[MONITERIG_FIELD.fieldApiName] = this.moniteringId;
            fields[STATUS_FIELD.fieldApiName] = status;
            fields[SITE_FIELD.fieldApiName] = this.siteId;
            fields[RECTYPEQUES_FIELD.fieldApiName] = this.recordTypeId;
            fields[DATEQUES_FIELD.fieldApiName] = new Date();
            fields[NCQUES_FIELD.fieldApiName] = nc.length > 0 ? 'Yes' : 'No';
            fields[PENDGQUES_FIELD.fieldApiName] = pending.length > 0 ? 'Yes' : 'No';

            const recordInput = {
                apiName: PROVIDERRECQUES_OBJECT.objectApiName,
                fields
            };
            createRecord(recordInput)
                .then(result => {
                    if (result.id) {
                        let datasTo = [];
                        this.questions.forEach(data => {
                            let fieldst = {};

                            fieldst[DATE_FIELD.fieldApiName] = data.Date__c,
                                fieldst[COMPLIANCE_FIELD.fieldApiName] = data.Compliance__c,
                                fieldst[FREQUENCYOFDEF_FIELD.fieldApiName] = data.FrequencyofDeficiency__c,
                                fieldst[IMPACTOFDEF_FIELD.fieldApiName] = data.ImpactofDeficiency__c,
                                fieldst[COMAR_FIELD.fieldApiName] = data.RegulationId,
                                fieldst[FINDINGS_FIELD.fieldApiName] = data.Findings__c,
                                fieldst[SCOPEOFDEF_FIELD.fieldApiName] = data.Scope_of_Deficiency__c,
                                fieldst[PROVIDERRECQUES_FIELD.fieldApiName] = result.id
                            datasTo.push(fieldst);
                        });

                        let datasString = JSON.stringify(datasTo);
                        bulkAddRecordAns({
                            datas: datasString
                        })
                            .then(result => {
                                this.Spinner = false;
                                fireEvent(this.pageRef, "RefreshActivityPage", true);
                                this.dispatchEvent(utils.toastMessage(this.popUpForSuccess, "success"));
                                this.closeNewRecord();
                                this.disableFields = false;
                                return refreshApex(this.wiredRecordDetails);
                            });
                    }
                })
                .catch(errors => {
                    if (errors) {
                        let error = JSON.parse(errors.body.message);
                        const { title, message, errorType } = error;
                        this.dispatchEvent(
                            utils.toastMessageWithTitle(title, message, errorType)
                        );
                    } else {
                        this.dispatchEvent(
                            utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                        );
                    }
                });

        } else {
            let fields = {};
            fields[IDQUES_FIELD.fieldApiName] = this.editRecordId;
            fields[STATUS_FIELD.fieldApiName] = status;
            fields[DATEQUES_FIELD.fieldApiName] = new Date();
            fields[NCQUES_FIELD.fieldApiName] = nc.length > 0 ? 'Yes' : 'No';
            fields[PENDGQUES_FIELD.fieldApiName] = pending.length > 0 ? 'Yes' : 'No';

            const recordInput = {
                fields
            };
            updateRecord(recordInput)
                .then(result => {
                    let datasTo = [];
                    this.questions.forEach(dat => {
                        let fieldst = {};

                        fieldst[ID_FIELD.fieldApiName] = dat.Id,
                            fieldst[DATE_FIELD.fieldApiName] = dat.Date__c,
                            fieldst[COMPLIANCE_FIELD.fieldApiName] = dat.Compliance__c,
                            fieldst[FREQUENCYOFDEF_FIELD.fieldApiName] = dat.FrequencyofDeficiency__c,
                            fieldst[IMPACTOFDEF_FIELD.fieldApiName] = dat.ImpactofDeficiency__c,
                            fieldst[COMAR_FIELD.fieldApiName] = dat.RegulationId,
                            fieldst[FINDINGS_FIELD.fieldApiName] = dat.Findings__c,
                            fieldst[SCOPEOFDEF_FIELD.fieldApiName] = dat.Scope_of_Deficiency__c,
                            fieldst[PROVIDERRECQUES_FIELD.fieldApiName] = dat.ProviderRecordQuestion__c
                        datasTo.push(fieldst);
                    });
                    let datasString = JSON.stringify(datasTo);
                    bulkUpdateRecordAns({
                        datas: datasString
                    })
                        .then(result => {
                            fireEvent(this.pageRef, "RefreshActivityPage", true);
                            this.dispatchEvent(utils.toastMessage("Records Has Been Updated Successfully!..", "success"));
                            this.closeNewRecord();
                            this.closeModal();
                            this.questions = [];
                            this.disableFields = false;
                            return refreshApex(this.wiredRecordDetails);
                        });
                })
                .catch(errors => {
                    if (errors) {
                        let error = JSON.parse(errors.body.message);
                        const { title, message, errorType } = error;
                        this.dispatchEvent(
                            utils.toastMessageWithTitle(title, message, errorType)
                        );
                    } else {
                        this.dispatchEvent(
                            utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                        );
                    }
                });

        }
        /* Create New Record Function End */
    }

    /*User profile based button hide */
    get userProfileName() {
        return sharedData.getUserProfileName();
    }
}