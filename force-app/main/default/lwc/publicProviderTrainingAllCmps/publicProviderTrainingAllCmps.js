/**
    * @UpdatedBy     : Sundar K -> AUGUST 9, 2020 -> Training Module Add/Edit/View flag validation handled
**/

import {
    LightningElement,
    track,
    api
} from 'lwc';

import myResource from '@salesforce/resourceUrl/styleSheet';
import {
    loadStyle
} from 'lightning/platformResourceLoader';

export default class PublicProviderTrainingAllCmps extends LightningElement {
    @track showTrainingDashBoard = true;
    @track showTrainingViewSession = false;
    @track showAddNewTraining = false;
    @track showAddInstructor = false;
    //@track addTrainingDecideToClose = false;

    _refreshApexForDashBoard;
    editOrViewTrainingModalActionType;

    @api get refreshTrainingDashboard() {
        return this._refreshApexForDashBoard
    }

    renderedCallback() {
        Promise.all([
            loadStyle(this, myResource + '/styleSheet.css')
        ])
    }

    handleToggleComponents(event) {
        if (event.detail.Component == 'ViewTraining') {
            this.showTrainingDashBoard = false;
            this.showTrainingViewSession = true;
            this.showAddNewTraining = false;
            this.showAddInstructor = false;
        } else if (['AddTraining', 'Edit', 'View'].includes(event.detail.Component)) {
            this.editOrViewTrainingModalActionType = event.detail;
            this.showAddNewTraining = true;
            this.showTrainingViewSession = false;
            this.showAddInstructor = false;
        } else if (event.detail.Component == 'AddInstructor') {
            this.showTrainingDashBoard = false;
            this.showTrainingViewSession = false;
            this.showAddNewTraining = false;
            this.showAddInstructor = true;
        } else if (event.detail.Component == 'CloseTraining') {
            //this.addTrainingDecideToClose = false;
            this._refreshApexForDashBoard = true;
        }
    }

    redirectToTrainingDashboard(event) {
        this.showTrainingDashBoard = true;
        this.showTrainingViewSession = false;
    }
}