// Author: Saranraj
// for dashborad

import {
    LightningElement,
    track,
    api,
    wire
} from "lwc";
import getDashboardData from "@salesforce/apex/Board.getDashboardData";
import {
    refreshApex
} from "@salesforce/apex";

import {
    getRecord
} from 'lightning/uiRecordApi';
import USER_ID from '@salesforce/user/Id';
import PROFILE_FIELD from '@salesforce/schema/User.Profile.Name';
import * as sharedData from "c/sharedData";

export default class Boards extends LightningElement {
    @track providerId = this.ProviderId;
    // for card click- data
    @track showApprovedData;
    @track showTotalData;
    @track showPendingData;
    @track showRejectedData;
    @track showReturnedData;
    @track PageSpinner = true;

    @track activeClass = "border: 2px solid black";

    @track tableDataVisible = true;
    _refreshDashBoardData;
    @api hidePagination;


    // for initail data
    pendingData = [];
    approvedData = [];
    rejectedData = [];
    totalData = [];
    returnedData = [];

    // for Status
    @track pending = "Pending";
    @track approved = "Approved";
    @track rejected = "Rejected";
    @track total = "Total Applications";
    @track returned = "Returned";
    @track pendingStatus;
    @track returnedStatus;
    @track approvedStatus;
    @track rejectedStatus;
    @track totalStatus;
    @api searchKey = "";

    //return end
    @track userProfileName;

    //   Pending
    @api get getPendingData() {
        return this.pendingData;
    }

    @api get getPendingStatus() {
        return this.pendingStatus;
    }

    // returned start
    @api get getReturnedData() {
        return this.returnedData;
    }

    @api get getReturnedStatus() {
        return this.returnedStatus;
    }

    // Aproved
    @api get getApprovedData() {
        return this.approvedData;
    }

    @api get getApprovedStatus() {
        return this.approvedStatus;
    }

    // /Rejected
    @api get getRejectedData() {
        return this.rejectedData;
    }

    @api get getRejectedStatus() {
        return this.rejectedStatus;
    }

    //   Total
    @api get getTotalData() {
        return this.totalData;
    }

    @api get getTotalStatus() {
        return this.totalStatus;
    }

    @wire(getDashboardData, {
        searchKey: '$searchKey'
    })
    wiredDashboardData({
        error,
        data
    }) {
        if (data) {
            try {
                this._refreshDashBoardData = data;
                let response = data;
                //Pending data
                let _data;
                if (this.userProfileName === "Caseworker" || this.userProfileName === "System Administrator") {
                    let forReviewData = this.filterAndAssignArray(response, "For Review");
                    let forReviewFinalData = this.mapAndAssignArray(forReviewData, "Pending")
                    let caseworkerData = this.filterAndAssignArray(response, "Caseworker Submitted");
                    let forCaseworkerFinalData = this.mapAndAssignArray(caseworkerData, "Submitted");


                    _data = [...forReviewFinalData, ...forCaseworkerFinalData];

                } else if (this.userProfileName === "Supervisor") {
                    let forSupervisorCaseworkerData = this.filterAndAssignArray(response, "Caseworker Submitted");
                    _data = this.mapAndAssignArray(forSupervisorCaseworkerData, "Pending")

                }

                this.pendingData = _data;

                this.pendingStatus = _data ? _data.length : 0;

                //Pending data


                //return data
                let returnedData;
                if (this.userProfileName === "Caseworker" || this.userProfileName === "System Administrator") {
                    returnedData = this.filterAndAssignArray(response, "Returned");

                } else if (this.userProfileName === "Supervisor") {
                    returnedData = this.filterAndAssignArray(response, "Returned");
                }

                let returnedDataConbinedArray = [...returnedData];
                this.returnedData = this.mapAndAssignArray(returnedDataConbinedArray, "Returned");

                this.returnedStatus = returnedDataConbinedArray.length;

                //return data


                //Approved data

                let caseWorkerData = this.filterAndAssignArray(response, "Approved");

                let approvedData = caseWorkerData;
                this.approvedData = this.mapAndAssignArray(approvedData, "Approved");
                this.approvedStatus = approvedData.length;
                //Approved data

                //Rejected data
                let _rejectedData;
                if (this.userProfileName === "Caseworker" || this.userProfileName === "System Administrator") {
                    let forRejectedData = this.filterAndAssignArray(response, "Rejected");
                    let forReviewFinalData = this.mapAndAssignArray(forRejectedData, "Rejected");
                    let forSupervisorRejectedData = this.filterAndAssignArray(response, "Supervisor Rejected");
                    let forSupervisorFinalData = this.mapAndAssignArray(forSupervisorRejectedData, "Rejected");
                    _rejectedData = [...forReviewFinalData, ...forSupervisorFinalData];
                } else if (this.userProfileName === "Supervisor") {
                    let forSupervisorRejectedData = this.filterAndAssignArray(response, "Supervisor Rejected");
                    _rejectedData = this.mapAndAssignArray(forSupervisorRejectedData, "Rejected")
                }
                this.rejectedData = _rejectedData;
                this.rejectedStatus = _rejectedData.length;
                //Rejected data

                //Total data
                let allDashboardData = [...this.rejectedData, ...this.approvedData, ...this.pendingData, ...this.returnedData]
                this.totalData = this.mapAndAssignArray(allDashboardData);
                this.totalStatus = this.totalData.length;
                this.PageSpinner = false;
            } catch (err) {
                this.PageSpinner = false;
                if (err) {
                    let error = JSON.parse(err.body.message);
                    const { title, message, errorType } = error;
                    this.dispatchEvent(
                      utils.toastMessageWithTitle(title, message, errorType)
                    );
                  } else {
                    this.dispatchEvent(
                      utils.toastMessage(
                        CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
                        CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
                      )
                    );
                  }

            }
            //Total data
        } else if (error) {}
    }

    filterAndAssignArray(response, filterValue) {
        if (this.userProfileName === "Caseworker" || this.userProfileName === "System Administrator") {
            return (
                response && response.filter((item) => item.Status__c === filterValue)
            );
        } else if (this.userProfileName === "Supervisor") {
            return (
                response && response.filter((item) => item.Status__c === filterValue && item.Supervisor__c === USER_ID)
            );
        }

    }

    mapAndAssignArray(dataArray, status) {
        return dataArray.map((item) => {
            if (item.Provider__r !== undefined) {
                return Object.assign({
                    ...item
                }, {
                    Status: status !== undefined ? status : item.Status,
                    ProviderId__c: item.Provider__r.ProviderId__c,
                    statusClass: status !== undefined ? status : item.Status
                });
            }
        });
    }

    @wire(getRecord, {
        recordId: USER_ID,
        fields: [PROFILE_FIELD]
    }) wireuser({
        error,
        data
    }) {
        if (error) {
            this.error = error;
        } else if (data) {
            this.userProfileName = data.fields.Profile.displayValue;
            sharedData.setUserProfileName(data.fields.Profile.displayValue);

        }
    }

    handleStatusCard(event) {
        if (event.target.dataset.id === "Pending") {
            this.showPendingData = true;
            this.showApprovedData = false;
            this.showTotalData = false;
            this.showRejectedData = false;
            this.tableDataVisible = false;
            this.showReturnedData = false;
            this.activeClass = "";
            this.template.querySelector(".pending").classList.add("pending-cls");
            this.template.querySelector(".returned").classList.remove("returned-cls");

            this.template.querySelector(".approved").classList.remove("approved-cls");
            this.template.querySelector(".rejected").classList.remove("rejected-cls");
            this.template.querySelector(".total").classList.remove("total-cls");
        } else if (event.target.dataset.id === "Approved") {
            this.showApprovedData = true;
            this.showPendingData = false;
            this.showTotalData = false;
            this.showRejectedData = false;
            this.tableDataVisible = false;
            this.showReturnedData = false;
            this.template.querySelector(".approved").classList.add("approved-cls");
            this.template.querySelector(".returned").classList.remove("returned-cls");

            this.template.querySelector(".pending").classList.remove("pending-cls");
            this.template.querySelector(".rejected").classList.remove("rejected-cls");
            this.template.querySelector(".total").classList.remove("total-cls");
        } else if (event.target.dataset.id === "Rejected") {
            this.showRejectedData = true;
            this.showPendingData = false;
            this.showApprovedData = false;
            this.showTotalData = false;
            this.tableDataVisible = false;
            this.showReturnedData = false;
            this.template.querySelector(".rejected").classList.add("rejected-cls");
            this.template.querySelector(".returned").classList.remove("returned-cls");

            this.template.querySelector(".pending").classList.remove("pending-cls");
            this.template.querySelector(".approved").classList.remove("approved-cls");
            this.template.querySelector(".total").classList.remove("total-cls");
        } else if (event.target.dataset.id === "Total Applications") {
            this.showTotalData = true;
            this.tableDataVisible = false;
            this.showRejectedData = false;
            this.showPendingData = false;
            this.showApprovedData = false;
            this.showReturnedData = false;
            this.template.querySelector(".total").classList.add("total-cls");
            this.template.querySelector(".returned").classList.remove("returned-cls");
            this.template.querySelector(".pending").classList.remove("pending-cls");
            this.template.querySelector(".rejected").classList.remove("rejected-cls");
            this.template.querySelector(".approved").classList.remove("approved-cls");
        } else if (event.target.dataset.id === "Returned") {
            this.showRejectedData = false;
            this.showReturnedData = true;
            this.showPendingData = false;
            this.showApprovedData = false;
            this.showTotalData = false;
            this.tableDataVisible = false;
            this.template.querySelector(".rejected").classList.remove("rejected-cls");
            this.template.querySelector(".returned").classList.add("returned-cls");
            this.template.querySelector(".pending").classList.remove("pending-cls");
            this.template.querySelector(".approved").classList.remove("approved-cls");
            this.template.querySelector(".total").classList.remove("total-cls");
        } else {
            this.tableDataVisible = true;
        }
    }

    handleChange(event) {
        this.searchKey = event.target.value;
        event.target.value.length > 0 ? this.hidePagination = true : this.hidePagination = false;
        return refreshApex(this._refreshDashBoardData);

    }


    handleredirectApplicationTabs() {
        const oncaseid = new CustomEvent("redirecteditapplication", {
            detail: {
                first: false
            }
        });
        this.dispatchEvent(oncaseid);
    }
}