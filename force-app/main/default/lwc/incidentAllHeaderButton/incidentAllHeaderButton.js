import { LightningElement, api, wire } from 'lwc';
import pubsub from 'c/pubsub';
import { CurrentPageReference } from 'lightning/navigation';
import * as sharedData from "c/sharedData";

export default class IncidentAllHeaderButton extends LightningElement {
    @wire(CurrentPageReference) pageRef;
    @api caseNumber;
    //Staff Details
    @api showAddStaffBtn = false;
    @api showStaffDetailsBtn = false;
    @api enableStaffDetails = false;

    //person
    @api showPersonButton = false;

    //witness
    @api showWitnessButton = false;

    redirecttoInfoPersonchild() {
        sharedData.setPersonKidIncidentId(undefined);
        sharedData.setContactId(undefined);
        sharedData.setAddressId(undefined);
        sharedData.setActionPersonKid(undefined);
        const onredirect = new CustomEvent("redirecttoaddnew", {
            detail: {
                first: true
            }
        });
        this.dispatchEvent(onredirect);
    }

    //Show Hide Btn Functions Start
    redirectToStaffMem() {
        let detail = {
            'first': 'staffMember'
        }
        pubsub.fireEvent(this.pageRef, 'registerBtnAction', detail);
    }

    redirectToStaffDetails() {
        let detail = {
            'first': 'staffDetails'
        }
        pubsub.fireEvent(this.pageRef, 'registerBtnAction', detail);
    }

    redirectToIncidentDashboard() {
        const onClickId = new CustomEvent("redirecttoincidentdashboard", {
            detail: {
                first: false,
            }
        });
        this.dispatchEvent(onClickId);
    }

    redirecttoWitnessButton() {
        const onClickId = new CustomEvent("clickbuttonwitness", {
            detail: {
                first: true,
            }
        });
        this.dispatchEvent(onClickId);
    }

}