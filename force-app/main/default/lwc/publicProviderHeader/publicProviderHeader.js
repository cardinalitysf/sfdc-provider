/**
 * @Author        : M.Vijayaraj
 * @CreatedOn     : May 22, 2020
 * @Purpose       : This component for Public provider Header
 **/

import { LightningElement, track, wire, api } from "lwc";
import fatchPickListValue from "@salesforce/apex/PublicProviderHeader.getPickListValues";
import fatchPickListValueJurisdiction from "@salesforce/apex/PublicProviderHeader.getPickListValuesJurisdiction";
import getCaseDetails from "@salesforce/apex/PublicProviderHeader.getCaseDetails";
import getIdAfterInsertSOQL from "@salesforce/apex/PublicProviderHeader.getIdAfterInsertSOQL";

import { utils } from "c/utils";
import * as sharedData from "c/sharedData";

export default class PublicProviderHeader extends LightningElement {
  @track CreatedDate;
  @track StartDate;
  @track Origin;
  @track CaseNumber;
  @track id;
  @track PublicProviderHeaderCommunication;
  @track PublicProviderHeaderJurisdiction;
  @track data = [];
  @track maxdate;
  @track today;
  @track NewReferralNumber;
  @track statusWisePageFreeze = false;
  @track Jurisdiction;

  connectedCallback() {
    let today = new Date();
    let dd = String(today.getDate()).padStart(2, "0");
    let mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
    let yyyy = today.getFullYear();

    this.today = yyyy + "-" + mm + "-" + dd;
    this.maxdate = this.today;
    sharedData.setReceivedDateValue(this.today);
    //Function used to create the new CASE

    this.NewReferralNumber = this.CaseId;

    //this.blnDecideLabel = this.NewReferralNumber ? true : false;
    if (!this.NewReferralNumber) {
      let myobjcase = { sobjectType: "Case" };
      myobjcase.Status = "New";
      getIdAfterInsertSOQL({ objIdSobjecttoUpdateOrInsert: myobjcase })
        .then((result) => {
          sharedData.setCaseId(result);
          this.NewReferralNumber = result;
          this.providerHeader();
        })
        .catch((errors) => {
          return this.dispatchEvent(utils.handleError(errors));
        });
    } else {
      this.providerHeader();
    }
  }
  providerHeader() {
    getCaseDetails({ casedetails: this.CaseId }).then((result) => {
      if (result.length > 0) {
        this.CreatedDate = result[0].CreatedDate;
        this.StartDate = result[0].StartDate__c;
        this.Origin =
          result[0].Origin !== undefined
            ? result[0].Origin
            : '';
        this.CaseNumber = result[0].CaseNumber;
        this.Id = result[0].Id;
        this.Jurisdiction =
          result[0].Account !== undefined
            ? result[0].Account.Jurisdiction__c
            : '';
        this.Status = result[0].Status;
        this.today =
          result[0].ReceivedDate__c !== undefined
            ? result[0].ReceivedDate__c
            : this.today;
        sharedData.setCommunicationValue(this.Origin);
        sharedData.setJurisdiction(this.Jurisdiction);
        sharedData.setReceivedDateValue(this.today);
        if (
          (result[0].Status !== undefined && result[0].Status == "Approved") ||
          result[0].Status == "Rejected"
        ) {
          this.statusWisePageFreeze = true;
        }
      }
    }).catch((errors) => {
      return this.dispatchEvent(utils.handleError(errors));
    });
  }

  //picklist values fetch from Case Object

  @wire(fatchPickListValue, {
    objInfo: { sobjectType: "Case" },
    picklistFieldApi: "Origin"
  })
  wireduserDetails1({ error, data }) {
    /*eslint-disable*/

    if (data) {
      this.PublicProviderHeaderCommunication = data;
    } else if (error) {
    }
  }

  //picklist Jurisdiction values fetch from Case Object
  @wire(fatchPickListValueJurisdiction, {
    objInfoJuris: { sobjectType: "Account" },
    picklistFieldApiJuris: "jurisdiction__c"
  })
  wireJurisdiction({ error, data }) {
    /*eslint-disable*/

    if (data) {
      this.PublicProviderHeaderJurisdiction = data;
    } else if (error) {
    }
  }

  //Function used to get the data from Communication field
  communicationchange(event) {
    sharedData.setCommunicationValue(event.detail.value);
  }

  jurisdictionchange(event) {
    sharedData.setJurisdiction(event.detail.value);
  }

  ReceivedDatechange(event) {
    sharedData.setReceivedDateValue(event.detail.value);
  }

  get CaseId() {
    return sharedData.getCaseId();
  }

  PageRedirecttoDashBoard() {
    const onClickNewDirection = new CustomEvent('redirecttoreferralhome', {
      detail: {
        first: false
      }
    });
    this.dispatchEvent(onClickNewDirection);
  }
}
