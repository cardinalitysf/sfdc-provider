/**
 * @Author        : Preethi Bojarajan
 * @CreatedOn     : March 16,2020
 * @Purpose       : Provider's Applications List with Edit 
 * @updatedBy     : Preethi Bojarajan
 * @updatedOn     : May 15 ,2020
 **/
import { LightningElement, track, wire, api } from 'lwc';
import getDashboardDatas from '@salesforce/apex/providerApplication.getDashboardDatas';
import getApplicationDet from '@salesforce/apex/providerApplication.getApplicationDet';
import getUserEmail from '@salesforce/apex/providerApplication.getUserEmail';

// Utils
import { utils } from 'c/utils';
import * as sharedData from "c/sharedData";
import { loadStyle } from 'lightning/platformResourceLoader';
import myResource from '@salesforce/resourceUrl/styleSheet';

//For Delete Record and Table Refresh
import {
    refreshApex
} from "@salesforce/apex";

const columns = [{
    label: 'Application Id',
    fieldName: 'id',
    sortable: true,
    type: 'button',
    typeAttributes: {
        label: {
            fieldName: "name"
        },
        class: "blue",
        target: "_self",
        color: "blue"
    }
},
{
    label: 'Program Name',
    fieldName: 'program',
    cellAttributes: {
        class: "fontclrGrey"
    }
},
{
    label: 'Program Type',
    type: 'button',
    sortable: true,
    typeAttributes: {
        name: 'dontRedirect',
        label: {
            fieldName: 'programType'
        }
    },
    cellAttributes: {
        class: "title"
    }
},
{
    label: 'Contract Start Date',
    fieldName: 'startDate',
    cellAttributes: {
        alignment: 'left'
    }
},
{
    label: 'Contract End Date',
    fieldName: 'endDate',
    cellAttributes: {
        alignment: 'left'
    }
},
{
    label: 'Capacity',
    fieldName: 'capacity',
    cellAttributes: {
        alignment: 'left'
    }
},
{
    label: 'Status',
    fieldName: 'status',
    type: 'text',
    cellAttributes: {
        class: {
            fieldName: 'statusClass'
        }
    }
}

];

export default class ProviderApplicationsDashboard extends LightningElement {

    // Dashboard Details
    @track dataTableShow = true;
    @track capacityTotalCount = 0;
    @track staffTotalCount;
    @api providerId = null;
    @track emailId = '';
    @track placements = 0;
    @track Spinner = true;
    //DataTable Data
    @track columns = columns;
    @track totalRecords = [];
    @track applicationsOfProv = [];
    wiredApplications = [];

    // Sort Function Tracks
    @track defaultSortDirection = 'asc';
    @track sortDirection = 'asc';
    @track sortedBy;

    //Pagination Tracks
    @track totalRecordsCount = 0;
    @track page = 1;
    perpage = 10;
    setPagination = 5;
    @track pages = [];
    @track paginationRange = [];
    @track first = 1;
    @track last = 1;

    //Depending the Logged In User Getting the Provider Id of the User Function Start
    @wire(getUserEmail)
    email({
        error,
        data
    }) {
        try {
            if (data.length > 0) {
                this.providerId = data[0].Id;
                sharedData.setProviderId(data[0].Id);
            } else if (error) {
                this.error = error;
            }
        } catch (e) {

        }
    }
    //Depending the Logged In User Getting the Provider Id of the User Function End

    // Dashboard Card Count Details Fetching Function Start
    @wire(getDashboardDatas, {
        providerId: '$providerId',
        type: 'capacityCount'
    })
    capacityCount({
        error,
        data
    }) {
        if (data) {
            try {
                if (data.length > 0) {
                    if (Object.keys(data[0]).length == 0)
                        this.capacityTotalCount = 0;
                    else
                        this.capacityTotalCount = data[0].expr0;
                } else
                    this.capacityTotalCount = 0;
            } catch (e) {

            }
        } else if (error) {
            this.error = error;
        }
    }

    @wire(getDashboardDatas, {
        providerId: '$providerId',
        type: 'staffCount'
    })
    staffCount({
        error,
        data
    }) {
        if (data) {
            try {
                if (data.length > 0) {
                    this.staffTotalCount = data.length;
                } else {
                    this.staffTotalCount = 0;
                }
            } catch (e) { }
        } else if (error) {
            this.error = error;
        }
    }
    // Dashboard Card Count Details Fetching Function End

    // Fatching and Formating Datas for the Data Table Function End
    @wire(getApplicationDet, {
        providerId: '$providerId'
    })
    appOfProvider(data) {

        this.wiredApplications = data;
        if (data.data != null && data.data[0].applications != '' && data.data[0].applications.length > 0) {
            this.dataTableShow = true;
            let contracts = data.data[0].contracts;
            let currentData = [];
            this.placements = 0;
            data.data[0].applications.forEach((row) => {
                let rowData = {};
                rowData.name = row.Name;
                rowData.program = row.Program__c;
                rowData.programType = row.ProgramType__c;
                if (row.Status__c == "Caseworker Submitted" || row.Status__c == "Submit for Review") {
                    rowData.status = "For Review";
                } else if (row.Status__c == "Supervisor Rejected") {
                    rowData.status = "Rejected";
                } else {
                    rowData.status = row.Status__c;
                }
                rowData.statusClass = row.Status__c == 'For Review' ? 'Review' : row.Status__c;
                rowData.statusClass = row.Status__c == 'Caseworker Submitted' ? 'Review' : row.Status__c;
                rowData.id = row.Id;
                let index = contracts.findIndex(cont => cont.License__r.Application__c === row.Id);
                if (index >= 0) {
                    rowData.startDate = contracts[index].ContractStartDate__c !== undefined ? utils.formatDate(contracts[index].ContractStartDate__c) : ' - ';
                    rowData.endDate = contracts[index].ContractEndDate__c !== undefined ? utils.formatDate(contracts[index].ContractEndDate__c) : ' - ';
                    rowData.capacity = contracts[index].TotalContractBed__c.toString();
                    this.placements += rowData.capacity;
                } else {
                    rowData.startDate = ' - ';
                    rowData.endDate = ' - ';
                    rowData.capacity = ' - ';
                }
                currentData.push(rowData);
            });
            this.totalRecords = currentData;
            this.totalRecordsCount = this.totalRecords.length;
            refreshApex(this.wiredApplications);
            this.pageData();
            this.capacityTotalCount = this.capacityTotalCount - this.placements;
        } else {
            this.Spinner = false;
            this.dataTableShow = false;
        }
    }
    // Fatching and Formating Datas for the Data Table Function End

    // Sorting the Data Table Column Function End
    sortBy(field, reverse, primer) {
        const key = primer ?
            function (x) {
                return primer(x[field]);
            } :
            function (x) {
                return x[field];
            };

        return function (a, b) {
            a = key(a);
            b = key(b);
            if (a === undefined) a = '';
            if (b === undefined) b = '';
            a = typeof (a) === 'number' ? a : a.toLowerCase();
            b = typeof (b) === 'number' ? b : b.toLowerCase();

            return reverse * ((a > b) - (b > a));
        };
    }
    onHandleSort(event) {
        const {
            fieldName: sortedBy,
            sortDirection: sortDirection
        } = event.detail;
        const cloneData = [...this.applicationsOfProv];

        cloneData.sort(this.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1));
        this.applicationsOfProv = cloneData;
        this.sortDirection = sortDirection;
        this.sortedBy = sortedBy;
    }
    // Sorting the Data Table Column Function End

    //Set the Application Id for Redirection Start
    handleRowAction(event) {
        if (event.detail.action.name != undefined) return;
        sharedData.setApplicationId(event.detail.row.id);
        const oncaseid = new CustomEvent('redirecteditapplication', {
            detail: {
                first: false
            }
        });
        this.dispatchEvent(oncaseid);
    }
    //Set the Application Id for Redirection End

    //Pagination Functions Start
    pageData = () => {
        let page = this.page;
        let perpage = this.perpage;
        let startIndex = (page * perpage) - perpage;
        let endIndex = (page * perpage);
        this.applicationsOfProv = this.totalRecords.slice(startIndex, endIndex);
        this.Spinner = false;
    }
    //Pagination Functions End

    /* Style Sheet Loading */
    renderedCallback() {
        Promise.all([
            loadStyle(this, myResource + '/styleSheet.css')
        ])
    }

    //For Pagination Child Bind
    hanldeProgressValueChange(event) {
        this.page = event.detail;
        this.pageData();
    }
}