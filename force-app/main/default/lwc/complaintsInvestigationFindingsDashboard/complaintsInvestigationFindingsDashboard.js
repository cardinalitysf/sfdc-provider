/**
 * @Author        : Tamilarasan G
 * @CreatedOn     : July 3, 2020
 * @Purpose       : Complaints Investigation / Findings Dashboard
 **/
import {
    LightningElement,
    track
} from 'lwc';
import getInvestigationDetails from '@salesforce/apex/ComplaintsInvestigationFindings.getInvestigationDetails';
import {
    CJAMS_CONSTANTS
} from 'c/constants';
import images from "@salesforce/resourceUrl/images";
import * as shareddata from 'c/sharedData';
import {
    utils
} from "c/utils";

const columns = [{
    label: 'Cap Number',
    fieldName: 'Id',
    sortable: true,
    type: 'button',
    initialWidth: 130,
    typeAttributes: {
        label: {
            fieldName: 'CapNumber'
        },
        class: "blue",
        target: "_self",
        color: "blue"
    }
},
{
    label: 'Complaint Number',
    fieldName: 'ComplaintNumber',
    type: 'text',
    wrapText: true,
    initialWidth: 170
},
{
    label: 'Deficiency/Violation',
    fieldName: 'Deficiency',
    type: 'text',
    wrapText: true,
    initialWidth: 180
},
{
    label: 'Frequency of Deficiency',
    fieldName: 'FrequencyofDeficiency',
    type: 'text',
    wrapText: true,
    initialWidth: 200
},
{
    label: 'Impact of Deficiency',
    fieldName: 'ImpactofDeficiency',
    type: 'text',
    wrapText: true,
    initialWidth: 200
},
{
    label: 'Scope of Deficiency',
    fieldName: 'ScopeofDeficiency',
    type: 'text',
    wrapText: true,
    initialWidth: 210
},
{
    label: 'Status',
    fieldName: 'Status',
    type: 'text',
    wrapText: true,
    cellAttributes: {
        class: {
            fieldName: "statusClass"
        }
    }
},
{
    label: '',
    fieldName: 'Id',
    type: "button",
    initialWidth: 100,
    typeAttributes: {
        iconName: 'utility:preview',
        name: 'View',
        title: 'View',
        initialWidth: 100,
        disabled: false,
        iconPosition: 'left',
        target: '_self',
    }
},

];

export default class ComplaintsInvestigationFindingsDashboard extends LightningElement {

    @track columns = columns;
    @track norecorddisplay = true;
    @track currentPageInvestigationData;

    //Pagination Tracks    
    @track page = 1;
    perpage = 10;
    setPagination = 5;
    @track totalRecordsCount = 0;
    @track totalRecords;
    InvestigationFindingsIcon = images + "/deficiencyIcon.svg";


    get investigationdetails() {
        return shareddata.getProviderId();
        // return '0010w00000IpqqRAAR';
    }

    connectedCallback() {
        this.investigationDetailsLoad();
    }

    investigationDetailsLoad() {
        getInvestigationDetails({
            investigationdetails: this.investigationdetails
        }).then(result => {
            if (result.length == 0) {
                this.norecorddisplay = false;
            } else {
                this.norecorddisplay = true;
            }
            if (result.length > 0) {
                result.forEach((item) => {
                    item.DeficiencyId = item.Id;
                    item.ComplaintNumber = item.Complaint__r != undefined ? item.Complaint__r.CaseNumber : '';
                    item.statusClass = item.Status__c;
                    item.CapNumber = item.Name != undefined ? item.Name : '';
                    item.Deficiency = item.Deficiency__c != undefined ? item.Deficiency__c : '';
                    item.Citation = item.Citation__c != undefined ? item.Citation__c : '';
                    item.Findings = item.Findings__c != undefined ? item.Findings__c : '';
                    item.Comments = item.Comments__c != undefined ? item.Comments__c : '';
                    item.CoordinatorName = item.Complaint__r != undefined ? item.Complaint__r.Caseworker__r != undefined ? item.Complaint__r.Caseworker__r.Name : '' : '';
                    item.CoordinatorEmail = item.Complaint__r != undefined ? item.Complaint__r.Caseworker__r != undefined ? item.Complaint__r.Caseworker__r.Email : '' : '';
                    item.CoordinatorPhone = item.Complaint__r != undefined ? item.Complaint__r.Caseworker__r != undefined ? item.Complaint__r.Caseworker__r.Phone : '' : '';
                    item.ProgramName = item.Provider__r != undefined ? item.Provider__r.Name : '';
                    item.ProgramAdmin = item.Provider__r != undefined ? item.Provider__r.FirstName__c + ' ' + item.Provider__r.LastName__c : '';
                    item.ProgramAddress = item.Complaint__r != undefined ? item.Complaint__r.Address__r != undefined ? item.Complaint__r.Address__r.AddressLine1__c : '' : '';
                    item.ProgramEmail = item.Complaint__r != undefined ? item.Complaint__r.Address__r != undefined ? item.Complaint__r.Address__r.Email__c : '' : '';
                    item.ProgramPhone = item.Complaint__r != undefined ? item.Complaint__r.Address__r != undefined ? item.Complaint__r.Address__r.Phone__c : '' : '';
                    item.ComplaintDate = item.Complaint__r != undefined ? item.Complaint__r.CreatedDate : '';
                    item.LicenseNumber = item.Complaint__r != undefined ? item.Complaint__r.License__r != undefined ? item.Complaint__r.License__r.Name : '' : '';

                    item.ComplaintId = item.Complaint__c != undefined ? item.Complaint__c : '';
                    item.NextApproverId = item.Complaint__r != undefined ? item.Complaint__r.Supervisor__c != undefined ? item.Complaint__r.Supervisor__c : '' : '';
                    item.FrequencyofDeficiency = item.FrequencyofDeficiency__c != undefined ? item.FrequencyofDeficiency__c : '';
                    item.ImpactofDeficiency = item.ImpactofDeficiency__c != undefined ? item.ImpactofDeficiency__c : '';
                    item.ScopeofDeficiency = item.ScopeofDeficiency__c != undefined ? item.ScopeofDeficiency__c : '';
                    item.ProviderDecision = item.ProviderDecision__c != undefined ? item.ProviderDecision__c : '';
                    item.ProviderComments = item.ProviderComments__c != undefined ? item.ProviderComments__c : '';
                    item.Status = item.Status__c != undefined ? item.Status__c : '';
                    item.actionName = 'undefined';
                });

                this.totalRecords = result;

                this.totalRecordsCount = this.totalRecords.length;
                this.pageData();
            }

        }).catch(errors => {
            let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
            return this.dispatchEvent(utils.handleError(errors, config));
        });
    }
    pageData() {
        let page = this.page;
        let perpage = this.perpage;
        let startIndex = page * perpage - perpage;
        let endIndex = page * perpage;
        this.currentPageInvestigationData = this.totalRecords.slice(startIndex, endIndex);
    }
    //For Pagination Child Bind
    hanldeProgressValueChange(event) {
        this.page = event.detail;
        this.pageData();
    }

    handleRowAction(event) {
        if (event.detail.action.name == "View" || event.detail.action.name == undefined) {
            event.detail.row.actionName = event.detail.action.name;
            const redirectiontoinvestigationdetailpage = new CustomEvent("redirectiontoinvestigationdetailpage", {
                detail: {
                    isView: false,
                    capDetails: event.detail.row
                }
            });
            this.dispatchEvent(redirectiontoinvestigationdetailpage);
        }
    }
}