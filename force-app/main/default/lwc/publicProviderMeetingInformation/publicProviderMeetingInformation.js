/**
 * @Author        : Pratheeba V
 * @CreatedOn     : May 13, 2020
 * @Purpose       : Public Provider Meeting Information tab 
 **/
import {
    LightningElement,
    track,
    api,
    wire
} from 'lwc';
import getHouseholdMembers from '@salesforce/apex/PublicProviderMeetingInformation.getHouseholdMembers';
import getTrainingDetails from '@salesforce/apex/PublicProviderMeetingInformation.getTrainingDetails';
import getMeetingDetails from '@salesforce/apex/PublicProviderMeetingInformation.getMeetingDetails';
import InsertUpdateMeetingInfo from '@salesforce/apex/PublicProviderMeetingInformation.InsertUpdateMeetingInfo';
import viewMeetingDetails from '@salesforce/apex/PublicProviderMeetingInformation.viewMeetingDetails';
import getMeetingAttendedHours from '@salesforce/apex/PublicProviderMeetingInformation.getMeetingAttendedHours';
import getPublicDecisionStatus from '@salesforce/apex/PublicProviderMeetingInformation.getPublicDecisionStatus';
import {
    updateRecord,
    deleteRecord
} from "lightning/uiRecordApi";
import ID_FIELD from '@salesforce/schema/SessionAttendee__c.Id';
import INTENT_FIELD from '@salesforce/schema/SessionAttendee__c.IntenttoAttend__c';
import MEETINGSTATUS_FIELD from '@salesforce/schema/SessionAttendee__c.SessionStatus__c';
import { CJAMS_CONSTANTS } from 'c/constants';
import {
    utils
} from "c/utils";
import * as sharedData from 'c/sharedData';
import {
    refreshApex
} from "@salesforce/apex";
import images from '@salesforce/resourceUrl/images';

const columns = [{
    label: 'TRAINING NUMBER',
    fieldName: 'TrainingName__c',
    type: 'text'
},
{
    label: 'SESSION TYPE',
    fieldName: 'SessionType__c',
    type: 'text'
},
{
    label: 'DATE OF MEETING',
    fieldName: 'Date__c',
    type: 'date',
    typeAttributes: { month: '2-digit', day: '2-digit', year: 'numeric' }
},
{
    label: 'START TIME',
    fieldName: 'StartTime__c',
    type: 'text'
},
{
    label: 'END TIME',
    fieldName: 'EndTime__c',
    type: 'text'
},
{
    label: 'TOPIC DESCRIPTION',
    fieldName: 'Description__c',
    type: 'button',
    typeAttributes: {
        label: {
            fieldName: 'Description__c'
        },
        name: 'Descpopup',
        title: {
            fieldName: 'Description__c'
        }
    },
    cellAttributes: {
        iconName: 'utility:description',
        iconAlternativeText: 'Description',
        class: {
            fieldName: 'descriptionClass'
        }
    }
}
];

const actions = [{
    label: 'View',
    name: 'View',
    iconName: 'utility:preview',
    target: '_self'
},
{
    label: 'Delete',
    name: 'Delete',
    iconName: 'utility:delete',
    target: '_self'
},
];

const meetingInfoColumns = [{
    label: 'TRAINING NUMBER',
    fieldName: 'Name',
    type: 'text'
},
{
    label: 'DATE ',
    fieldName: 'Date__c',
    type: 'date',
    typeAttributes: { month: '2-digit', day: '2-digit', year: 'numeric' }
},
{
    label: 'SESSION TYPE',
    fieldName: 'SessionNumber__c',
    type: 'text'
},
{
    label: 'TRAINEE',
    fieldName: 'Trainee',
    type: 'text'
},
{
    label: 'ROLE',
    fieldName: 'Role__c',
    type: 'text'
},
{
    label: 'JURISDICTION',
    fieldName: 'Jurisdiction__c',
    type: 'text'
},
{
    label: 'INTENT TO ATTEND',
    fieldName: 'Id',
    type: 'toogle',
    typeAttributes: {
        isChecked: {
            fieldName: 'IntenttoAttend__c'
        },
        isDisabled: {
            fieldName: 'toggleDisabled'
        }
    }
},
{
    label: 'TRAINING STATUS',
    fieldName: 'SessionStatus__c',
    type: 'text',
    cellAttributes: {
        class: {
            fieldName: 'statusClass'
        }
    }
}
];

export default class PublicProviderMeetingInformation extends LightningElement {
    @track openmodel = false;
    @track activeForPType = false;
    @track pillValueForPtype = [];
    @track pillSingleValueForPtype;
    @track pillSingleValueForPtypeCount;
    @track pillSingleValueCondition = true;
    @track householdmember = [];
    columns = columns;
    @track norecorddisplay = true;
    @track nomeetingdisplay = true;
    @track totalRecords;
    @track totalRecordsCount = 0;
    @track totalMeetingCount = 0;
    @track page = 1;
    perpage = 6;
    @track currentPageTrainingData;
    @track currentPageMeetingData;
    @track viewmodel = false;
    @track deleteModel = false;
    @track trainingName;
    @track trainingDate;
    @track trainingStartTime;
    @track trainingEndTime;
    @track trainingMeetingStatus;
    @track trainingDescription;
    @track trainingDuration;
    attachmentIcon = images + '/people-icon.svg';
    clockIcon = images + '/clockwise-rotation.svg';
    descriptionIcon = images + '/description-icon.svg';
    meetingInfoColumns = meetingInfoColumns;
    @track totalHours;
    @track applicantName;
    @api providerMeetingInfoRecord;
    @track descModel = false;
    @track topicDescriptionPopup;
    @track isApplicant = false;
    @track AssignbtnDisable = false;

    get caseid() {
        return sharedData.getCaseId();
    }

    get providerId() {
        return sharedData.getProviderId();
    }

    @wire(getPublicDecisionStatus, {
        refid: "$caseid"
    })
    StatusData(result) {
        if (result.data != undefined) {
            if (result.data[0].Status == "Approved" || result.data[0].Status == "Rejected") {
                this.AssignbtnDisable = true;
            } else {
                this.AssignbtnDisable = false;
            }
        }
    }

    connectedCallback() {
        getMeetingAttendedHours({
            caseid: this.caseid
        })
            .then((result) => {
                if (result.length > 0) {
                    this.totalHrsByActor = result[0].totalhours;
                    var hours = Math.floor(result[0].totalhours / 60);
                    var cminutes = result[0].totalhours % 60;
                    this.isApplicant = true;
                    this.applicantName = result[0].Name;
                    this.totalHours = hours + '.' + cminutes + ' Hrs';
                } else {
                    this.totalHours = '00 Hr';
                    this.applicantName = '';
                }
            }).catch(errors => {
                return this.dispatchEvent(utils.handleError(errors));
            });
        if (!this.providerId)
            return;
        getHouseholdMembers({
            providerid: this.providerId
        }).then(result => {
            result.forEach((row) => {
                this.householdmember.push(
                    {
                        Id: row.Id,
                        memberNameRole: row.Contact__r.Name + ((row.Role__c) ? ' - ' + row.Role__c : ''),
                        label: row.Contact__r.Name,
                        role: row.Role__c
                    });
            });
        }).catch(errors => {
            return this.dispatchEvent(utils.handleError(errors));
        });
        this.meetingOnLoad();
    }

    trainingLoad() {
        getTrainingDetails({
            param: 'Information Meeting'
        }).then(result => {
            this.totalRecordsCount = result.length;
            if (this.totalRecordsCount == 0) {
                this.norecorddisplay = true;
            } else {
                this.norecorddisplay = false;
                let assignDataArray = [];
                if (this.currentPageMeetingData) {
                    result.forEach(assign => {
                        this.currentPageMeetingData.forEach(assign1 => {
                            if (assign1.Training__c == assign.Id) {
                                assignDataArray.push(assign);
                            }
                        });
                    });
                    result = result.filter(function (val) {
                        return assignDataArray.indexOf(val) == -1;
                    });
                }


                this.totalRecords = result.map(row => {
                    return {
                        Id: row.Id,
                        TrainingName__c: row.Name,
                        Date__c: row.Date__c,
                        Description__c: (row.Description__c) ? (row.Description__c.length > 13) ? row.Description__c.substring(0, 9) + '...' : row.Description__c : '',
                        StartTime__c: utils.formatTime12Hr(row.StartTime__c),
                        EndTime__c: utils.formatTime12Hr(row.EndTime__c),
                        Duration__c: row.Duration__c,
                        SessionType__c: (row.SessionType__c) ? row.SessionType__c : '-',
                        descriptionClass: (row.Description__c) ? 'btnbdrRemove blue' : 'svgRemoveicon',
                        popupdescription: row.Description__c
                    }
                });
                this.pageData();
            }
        }).catch(errors => {
            return this.dispatchEvent(utils.handleError(errors));
        });
    }

    handleToggleChange(event) {
        event.stopPropagation();
        const fields = {};
        fields[ID_FIELD.fieldApiName] = event.detail.data.recordId;
        fields[INTENT_FIELD.fieldApiName] = (event.detail.data.isChecked) ? 'Yes' : 'No';
        fields[MEETINGSTATUS_FIELD.fieldApiName] = (event.detail.data.isChecked) ? 'Scheduled' : 'Cancelled';
        const recordInput = {
            fields
        };
        updateRecord(recordInput)
            .then(() => {
                this.dispatchEvent(utils.toastMessage("Intent to attend updated successfully", "success"));
                return refreshApex(this.meetingOnLoad());
            })
            .catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.APPLICATION_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            })
    }
    pageData = () => {
        let page = this.page;
        let perpage = this.perpage;
        let startIndex = (page * perpage) - perpage;
        let endIndex = (page * perpage);
        this.currentPageTrainingData = this.totalRecords.slice(startIndex, endIndex);
        if (this.currentPageTrainingData.length == 0) {
            if (this.page != 1) {
                this.page = this.page - 1;
                this.pageData();
            }
        }
    }

    hanldeProgressValueChange(event) {
        this.page = event.detail;
        this.pageData();
    }
    @api intentFlag = false;
    meetingOnLoad() {
        getMeetingDetails({
            caseid: this.caseid
        }).then(res => {
            refreshApex(this.trainingLoad());
            this.totalMeetingCount = res.length;
            if (this.totalMeetingCount == 0) {
                this.providerMeetingInfoRecord = false;
                this.nomeetingdisplay = true;
            } else {
                this.providerMeetingInfoRecord = true;
                this.nomeetingdisplay = false;
                this.currentPageMeetingData = res.map(row => {
                    return {
                        Id: row.Id,
                        Training__c: row.Training__c,
                        Name: row.Training__r.Name,
                        Date__c: row.Training__r.Date__c,
                        SessionNumber__c: row.Training__r.SessionType__c,
                        Trainee: row.Actor__r.Contact__r.Name,
                        Role__c: row.Actor__r.Role__c,
                        Jurisdiction__c: (row.Training__r.Jurisdiction__c) ? row.Training__r.Jurisdiction__c : '-',
                        SessionStatus__c: row.SessionStatus__c,
                        IntenttoAttend__c: row.IntenttoAttend__c == 'Yes' ? true : false,
                        statusClass: (row.SessionStatus__c == 'Completed') ? 'Approved' : (row.SessionStatus__c == 'Cancelled') ? 'Incomplete' : row.SessionStatus__c,
                        toggleDisabled: (row.SessionStatus__c == 'Completed' || this.intentFlag == true) ? true : false,
                        buttonDisabled: (this.AssignbtnDisable) ? true : false
                    }
                });
            }
        }).catch(errors => {
            return this.dispatchEvent(utils.handleError(errors));
        });
    }

    @api meetingFlag = false;
    openmodal() {
        this.pillValueForPtype = [];
        if (this.meetingFlag || this.householdmember.length != 0) {
            this.openmodel = true;
            this.viewmodel = false;
            this.deleteModel = false;
            this.descModel = false;
        } else {
            return this.dispatchEvent(utils.toastMessage("Household members not available", "Warning"));
        }
    }
    closeModal() {
        this.openmodel = false;
        this.viewmodel = false;
        this.deleteModel = false;
        this.descModel = false;
    }

    //Code start multi select in household members pill
    get pillvalueForPType() {
        if (this.pillValueForPtype.length > 1) {
            this.pillSingleValueForPtype = this.pillValueForPtype[0].value;
            this.pillSingleValueCondition = false;
            this.pillSingleValueForPtypeCount = this.pillValueForPtype.length - 1;
            return this.pillSingleValueForPtype;
        } else {
            this.pillSingleValueCondition = true;
            return this.pillValueForPtype;
        }
    }

    handleMouseOutButton(evr) {
        this.activeForPType = false;
    }

    get toggledivForPtype() {
        return this.pillValueForPtype.length == 0 ? false : true;
    }

    handlePType(evt) {
        this.activeForPType = this.activeForPType ? false : true;
    }

    handleRemove(evt) {
        this.pillValueForPtype = this.remove(this.pillValueForPtype, "value", evt.target.label);
        let checkboxes = this.template.querySelectorAll("[data-id=checkbox]");
        checkboxes.forEach(element => {
            if (element.value == evt.target.label) {
                if (element.checked) {
                    element.checked = false;
                }
            }
        });
        this.activeForPType = true;
    }

    get tabClassForPType() {
        return this.activeForPType ? "slds-popover slds-popover_full-width slds-popover_show" :
            "slds-popover slds-popover_full-width slds-popover_hide";
    }

    handleclickofPTypeCheckBox(evt) {
        if (evt.target.checked) {
            this.pillValueForPtype.push({
                value: evt.target.value,
                id: evt.target.id
            });
        } else {
            this.pillValueForPtype = this.remove(
                this.pillValueForPtype,
                "value",
                evt.target.value
            );
        }
        this.selectedMembers = this.pillValueForPtype;
        evt.target.checked != evt.target.checked;
    }

    remove(array, key, value) {
        const index = array.findIndex(obj => obj[key] === value);
        return index >= 0 ? [...array.slice(0, index), ...array.slice(index + 1)] :
            array;
    }
    //Code end multi select in household members pill

    handleRowSelection = event => {
        this.selectedTraining = event.detail.selectedRows;
    }

    handleDelete() {
        refreshApex(this.trainingLoad());
        deleteRecord(this.deleteId)
            .then(() => {
                this.deleteModel = false;
                this.dispatchEvent(utils.toastMessage('Training Details deleted successfully', "success"));
                refreshApex(this.trainingLoad());
                return refreshApex(this.meetingOnLoad());
            })
            .catch(errors => {
                this.deleteModel = false;
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.APPLICATION_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                this.dispatchEvent(utils.handleError(errors, config));
                return refreshApex(this.meetingOnLoad());
            });
    }

    closeDeleteModal() {
        this.deleteModel = false;
    }

    closeDescModal() {
        this.descModel = false;
    }

    handleMeetingRowAction(event) {
        this.descModel = true;
        if (event.detail.action.name == "Descpopup") {
            this.topicDescriptionPopup = event.detail.row.popupdescription;
        }
    }

    handleRowAction(event) {
        let meetingrowid = event.detail.row.Id;
        if (event.detail.action.name == "Delete") {
            this.deleteModel = true;
            this.deleteId = event.detail.row.Id;
        } else if (event.detail.action.name == "View") {
            this.title = 'VIEW TRAINING';
            this.openmodel = false;
            this.viewmodel = true;
            this.deleteModel = false;
            this.descModel = false;
            viewMeetingDetails({
                meetingid: meetingrowid
            }).then(result => {
                if (result.length > 0) {
                    this.trainingName = result[0].Training__r.Name;
                    this.trainingDate = utils.formatDate(result[0].Training__r.Date__c);
                    this.trainingStartTime = utils.formatTime12Hr(result[0].Training__r.StartTime__c);
                    this.trainingEndTime = utils.formatTime12Hr(result[0].Training__r.EndTime__c);
                    this.trainingMeetingStatus = result[0].SessionStatus__c;
                    this.trainingDescription = result[0].Training__r.Description__c;
                    this.trainingDuration = result[0].Training__r.Duration__c;
                } else {
                    this.trainingName = '';
                    this.trainingDate = '';
                    this.trainingStartTime = '';
                    this.trainingEndTime = '';
                    this.trainingMeetingStatus = '';
                    this.trainingDescription = '';
                    this.trainingDuration = '';
                }
            }).catch(errors => {
                return this.dispatchEvent(utils.handleError(errors));
            });
        }
    }

    handle24hrs(tm) {
        var time = tm;
        var hours = Number(time.match(/^(\d+)/)[1]);
        var minutes = Number(time.match(/:(\d+)/)[1]);
        var AMPM = time.match(/\s(.*)$/)[1];
        if (AMPM == "PM" && hours < 12) hours = hours + 12;
        if (AMPM == "AM" && hours == 12) hours = hours - 12;
        var sHours = hours.toString();
        var sMinutes = minutes.toString();
        if (hours < 10) sHours = "0" + sHours;
        if (minutes < 10) sMinutes = "0" + sMinutes;
        return sHours + ":" + sMinutes;
    }

    handleSave() {
        this.sobjectType = 'SessionAttendee__c';
        let objToInsert = [];
        if (!this.selectedMembers && !this.selectedTraining) {
            this.dispatchEvent(utils.toastMessage("Please choose members and training", "Warning"));
        } else if (!this.selectedMembers) {
            this.dispatchEvent(utils.toastMessage("Please choose members", "Warning"));
        } else if (!this.selectedTraining) {
            this.dispatchEvent(utils.toastMessage("Please choose training", "Warning"));
        } else {
            this.selectedMembers.forEach(element => {
                var memberId = element.id.split("-");
                this.selectedTraining.forEach(trainingElem => {
                    let trainingFields = {
                        Actor__c: '',
                        Training__c: '',
                        //Date__c: '',
                        //SessionNumber__c: '',
                        //StartTime__c: '',
                        //EndTime__c: '',
                        SessionStatus__c: '',
                        Referral__c: '',
                        TotalHoursAttended__c: ''
                    };
                    let hoursDotMinutes = trainingElem.Duration__c;
                    let fieldArray = hoursDotMinutes.split(":");
                    let minutes = (parseInt(fieldArray[0]) * 60) + parseInt(fieldArray[1]);
                    trainingFields.Actor__c = memberId[0];
                    trainingFields.Training__c = trainingElem.Id;
                    //trainingFields.Date__c = utils.formatDateYYYYMMDD(trainingElem.Date__c);
                    //trainingFields.SessionNumber__c = trainingElem.SessionType__c;
                    //trainingFields.StartTime__c = this.handle24hrs(trainingElem.StartTime__c);
                    //trainingFields.EndTime__c = this.handle24hrs(trainingElem.EndTime__c);
                    trainingFields.SessionStatus__c = 'Scheduled';
                    trainingFields.Referral__c = this.caseid;
                    trainingFields.TotalHoursAttended__c = minutes;
                    //trainingFields.TotalHoursAttended__c = utils.getTimeDiff(trainingFields.StartTime__c, trainingFields.EndTime__c);
                    objToInsert.push(trainingFields);
                })
            });
            InsertUpdateMeetingInfo({
                strTrainingDetails: JSON.stringify(objToInsert)
            }).then(result => {
                this.openmodel = false;
                refreshApex(this.trainingLoad());
                this.dispatchEvent(utils.toastMessage("Record Saved Successfully", "Success"));
                return refreshApex(this.meetingOnLoad());
            }).catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.APPLICATION_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
        }
    }

    /**if decision status = approved show only view otherwise show view and delete begins */
    constructor() {
        super();
        let deleteIcon = true;
        for (let i = 0; i < this.meetingInfoColumns.length; i++) {
            if (this.meetingInfoColumns[i].type == 'action') {
                deleteIcon = false;
            }
        }
        if (deleteIcon) {
            this.meetingInfoColumns.push(
                {
                    type: 'action', typeAttributes: { rowActions: this.getRowActions }, cellAttributes: {
                        iconName: 'utility:threedots_vertical',
                        iconAlternativeText: 'Actions',
                        class: "tripledots"
                    }
                }
            )
        }
    }

    getRowActions(row, doneCallback) {
        const actions = [];
        if (row['buttonDisabled'] == true) {
            actions.push({
                'label': 'View',
                'name': 'View',
                'iconName': 'utility:preview',
                'target': '_self'
            });
        } else {
            actions.push({
                'label': 'View',
                'name': 'View',
                'iconName': 'utility:preview',
                'target': '_self'
            },
                {
                    'label': 'Delete',
                    'name': 'Delete',
                    'iconName': 'utility:delete',
                    'target': '_self'
                });
        }
        // simulate a trip to the server
        setTimeout(() => {
            doneCallback(actions);
        }, 200);
    }
    /**if decision status = approved show only view otherwise show view and delete ends */
}