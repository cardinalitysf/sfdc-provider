/**
 * @Author        : Preethi Bojarajan
 * @CreatedOn     : Aug 04, 2020
 * @Purpose       : New Staff Details Capturing for The Incident
 * @updatedBy     : 
 * @updatedOn     : 
 **/
import { LightningElement, wire } from 'lwc';

//Utils
import pubsub from 'c/pubsub';
import { CurrentPageReference } from 'lightning/navigation';
import * as sharedData from 'c/sharedData';
import { utils } from "c/utils";
import { constPopupVariables, CJAMS_OBJECT_QUERIES, CJAMS_CONSTANTS, DASHBORD_LABELS, USER_PROFILE_NAME } from 'c/constants';

//Apex Classes
import getStaffMembers from '@salesforce/apex/IncidentStaffDetails.getStaffMembers';
import getMultiplePicklistValues from '@salesforce/apex/IncidentStaffDetails.getMultiplePicklistValues';
import getActorDetails from '@salesforce/apex/IncidentStaffDetails.getActorDetails';

//Wired Api
import { createRecord, updateRecord, deleteRecord } from "lightning/uiRecordApi";
import { refreshApex } from "@salesforce/apex";
import ACTOR_OBJECT from '@salesforce/schema/Actor__c';

const columns = [{
    label: 'First Name', fieldName: 'FirstName__c', type: 'text'
}, {
    label: 'Last Name', fieldName: 'LastName__c', type: 'text'
}, {
    label: 'Affiliation Type', fieldName: 'AffiliationType__c', type: 'text'
}, {
    label: 'Job Title', fieldName: 'JobTitle__c', type: 'button',
    typeAttributes: {
        name: 'dontRedirect',
        label: {
            fieldName: 'JobTitle__c'
        }
    },
    cellAttributes: {
        class: "title"
    }
}, {
    label: 'Employee TYPE', fieldName: 'EmployeeType__c', type: 'button',
    typeAttributes: {
        name: 'dontRedirect',
        label: {
            fieldName: 'EmployeeType__c'
        }
    },
    cellAttributes: {
        class: "title"
    }
}];

export default class IncidentStaffDetails extends LightningElement {
    @wire(CurrentPageReference) pageRef;
    Spinner = true;
    noIconShow = true;
    showStaffMemberPop = false;
    showStaffDetailsPop = false;
    //Staff Member Details
    staffWholeDet;
    staffMemberListColumn;
    staffMemberDetList;
    staffMemberAddList;
    showTable = true;
    disableStaffName = false;
    isDisabled = false
    //Pop Variable Defines
    staffMemberList = [];
    wholeStaffMemberList = [];
    staffMemberListFrmActor = [];
    staffMemberColumns = columns;
    enableAdd = true;
    preSelectedRows = [];
    availableStaffsvalue;
    //Staff Member Details
    afflicationTypeValues;
    jobTitleValues;
    employeeTypeValues;
    injurySeverityLevelValues;
    primaryStaffMemValues;
    staffAssaultedValues;
    injurySustainedValues;
    contact = {};
    actor = {};
    AffiliationType__c = ' - ';
    addOrEditLable = 'Add';
    saveOrUpdateLable = 'Save';
    //DeleteRecord
    openmodel = false;
    deleteRecordId;

    constructor() {
        super();
        this.staffMemberListColumn = [{
            label: 'First Name', fieldName: 'FirstName__c', type: 'text'
        }, {
            label: 'Last Name', fieldName: 'LastName__c', type: 'text'
        }, {
            label: 'Affiliation Type', fieldName: 'AffiliationType__c', type: 'text'
        }, {
            label: 'Job Title', fieldName: 'JobTitle__c', type: 'button',
            typeAttributes: {
                name: 'dontRedirect', label: {
                    fieldName: 'JobTitle__c'
                }
            },
            cellAttributes: {
                class: "title"
            }
        }, {
            label: 'Staff Assaulted', fieldName: 'StaffAssaulted__c', type: 'text'
        }, {
            label: 'Injury Sustained', fieldName: 'InjurySustained__c', type: 'text'
        }, {
            label: 'Injury Severity Level', fieldName: 'InjurySeverityLevel__c', type: 'text'
        }, {
            label: 'Primary Staff Member', fieldName: 'PrimaryStaffMember__c', type: 'text'
        }, {
            type: 'action', fieldName: 'id', typeAttributes: { rowActions: this.getRowActions },
            cellAttributes: {
                iconName: 'utility:threedots_vertical',
                iconAlternativeText: 'Actions',
                class: { fieldName: "statusClass" }
            }
        }];
    }


    getRowActions(row, doneCallback) {
        const actions = [];
        let userProfileName = sharedData.getUserProfileName();
        let incidentStatus = sharedData.getIncidentStatus();
        incidentStatus = incidentStatus !== undefined ? incidentStatus : "Draft";
        if (userProfileName === USER_PROFILE_NAME.USER_SUPERVISOR || userProfileName === USER_PROFILE_NAME.USER_CASE_WRKR || (incidentStatus !== "Draft" && incidentStatus !== "Pending"))
            actions.push({
                'label': 'View',
                'iconName': 'action:preview',
                'name': 'viewRecord'
            });
        else
            actions.push({
                'label': 'Edit',
                'iconName': 'action:edit',
                'name': 'editRecord'
            }, {
                'label': 'Delete',
                'iconName': 'action:delete',
                'name': 'deleteRecord'
            });
        // simulate a trip to the server
        setTimeout(() => {
            doneCallback(actions);
        }, 200);
    }

    connectedCallback() {
        this.registerBtnAction();
    }

    registerBtnAction() {
        pubsub.registerListener('registerBtnAction', this.handleEvent, this);
    }

    get applicationId() {
        return sharedData.getApplicationId();
    }

    get caseId() {
        return sharedData.getCaseId();
    }

    get userProfileName() {
        return sharedData.getUserProfileName();
    }

    get incidentStatus() {
        return sharedData.getIncidentStatus();
    }

    get allPickListFields() {
        return CJAMS_OBJECT_QUERIES.FETCH_FIELDS_FOR_STAFF_PICKLIST;
    }

    get allActorPicklistFields() {
        return CJAMS_OBJECT_QUERIES.FETCH_FIELDS_FOR_ACTOR_PICKLIST;
    }

    handleEvent(event) {
        this.enableAdd = true;
        if (event.first === 'staffMember') {
            this.showStaffMemberPop = true;
            this.staffMemberAddAlign();
        } else if (event.first === 'staffDetails') {
            this.showStaffDetailsPop = true;
            this.disableStaffName = false;
            this.contact = {};
            this.actor = {};
            this.AffiliationType__c = '';
            this.addOrEditLable = 'Add';
            this.saveOrUpdateLable = 'Save';
        }
    }

    closeModel() {
        this.showStaffMemberPop = false;
        this.showStaffDetailsPop = false;
        this.openmodel = false;
    } 

    formatData(data) {
        let currentData = [];
        data.forEach((row) => {
            let statusClass = "tripledots tripleDot_sec_del";
            let incidentStatus = this.incidentStatus !== undefined ? this.incidentStatus : "Draft";
            if (this.userProfileName === USER_PROFILE_NAME.USER_SUPERVISOR || this.userProfileName === USER_PROFILE_NAME.USER_CASE_WRKR || (incidentStatus !== "Draft" && incidentStatus !== "Pending"))
                statusClass = "tripledots";
            if (row.Staff__r !== undefined || row.Contact__r !== undefined)
                currentData.push({
                    FirstName__c: row.Staff__r !== undefined ? row.Staff__r.FirstName__c : row.Contact__r.FirstName__c,
                    LastName__c: row.Staff__r !== undefined ? row.Staff__r.LastName__c : row.Contact__r.LastName__c,
                    LastName: row.Staff__r !== undefined ? row.Staff__r.LastName : row.Contact__r.LastName,
                    AffiliationType__c: row.Staff__r !== undefined ? row.Staff__r.AffiliationType__c : row.Contact__r.AffiliationType__c,
                    JobTitle__c: row.Staff__r !== undefined ? row.Staff__r.JobTitle__c : row.Contact__r.JobTitle__c,
                    EmployeeType__c: row.Staff__r !== undefined ? row.Staff__r.EmployeeType__c : row.Contact__r.EmployeeType__c,
                    Contact__c: row.Staff__c !== undefined ? row.Staff__c : row.Contact__c,
                    Id: row.Id,
                    StaffAssaulted__c: row.StaffAssaulted__c !== undefined ? row.StaffAssaulted__c : ' - ',
                    InjurySustained__c: row.InjurySustained__c !== undefined ? row.InjurySustained__c : ' - ',
                    InjurySeverityLevel__c: row.InjurySeverityLevel__c !== undefined ? row.InjurySeverityLevel__c : ' - ',
                    PrimaryStaffMember__c: row.PrimaryStaffMember__c !== undefined ? row.PrimaryStaffMember__c : ' - ',
                    statusClass,
                });
        });
        return currentData;
    }

    @wire(getStaffMembers, {
        applicationId: '$applicationId'
    })
    wiredStaffDet(result) {
        try {
            if (result.data != undefined && result.data.length > 0) {
                let currentData = [];
                currentData = this.formatData(result.data);
                this.wholeStaffMemberList = currentData;
                this.staffMemberList = [...this.wholeStaffMemberList];
            } else if (result.error) {
                let errors = result.error;
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const { title, message, errorType } = error;
                    this.dispatchEvent(utils.toastMessageWithTitle(title, message, errorType));
                } else {
                    this.dispatchEvent(utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE));
                }
            }
        } catch (error) {
            throw error;
        }
    }

    @wire(getActorDetails, {
        caseId: '$caseId'
    })
    actorListDet(result) {
        this.staffWholeDet = result;
        try {
            if (result.data !== undefined)
                if (result.data.length === 0) {
                    this.Spinner = false;
                    this.staffMemberListFrmActor = [];
                    this.noIconShow = true;
                    this.functionForDet(true);
                } else if (result.data.length > 0) {
                    let currentData = [];
                    currentData = this.formatData(result.data);
                    this.staffMemberListFrmActor = currentData;
                    this.noIconShow = false;
                    this.functionForDet(false);
                } else if (result.error) {
                    let errors = result.error;
                    if (errors) {
                        let error = JSON.parse(errors.body.message);
                        const { title, message, errorType } = error;
                        this.dispatchEvent(utils.toastMessageWithTitle(title, message, errorType));
                    } else {
                        //this.dispatchEvent(utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE));
                    }
                }
        } catch (error) {
            throw error;
        }
    }

    functionForDet(btnFnDet) {
        const onAddEnable = new CustomEvent('enablestaffdetailbtn', {
            detail: {
                first: btnFnDet
            }
        });
        this.dispatchEvent(onAddEnable);

        if (btnFnDet === false) {
            let currentData = [];
            this.staffMemberListFrmActor.forEach(staff => {
                currentData.push({
                    label: staff.LastName,
                    value: staff.Contact__c
                });
            });
            this.availableStaffsvalue = currentData;

            const onStaffAvai = new CustomEvent('flagfrstaffdetailcount', {
                detail: {
                    count: true
                }
            });
            this.dispatchEvent(onStaffAvai);
        } else
            this.availableStaffsvalue = [];
        this.staffMemberAddAlign();
    }

    staffMemberAddAlign() {
        this.Spinner = false;
        if (this.staffMemberListFrmActor.length > 0 && this.wholeStaffMemberList.length > 0) {
            let fileredData = this.wholeStaffMemberList.filter(obj1 => this.staffMemberListFrmActor.every(obj2 => obj1.Contact__c !== obj2.Contact__c));
            this.staffMemberList = [...fileredData];
        } else if (this.staffMemberListFrmActor.length === 0 && this.wholeStaffMemberList.length > 0)
            this.staffMemberList = this.wholeStaffMemberList;
        this.showTable = this.staffMemberList.length > 0 ? true : false;
    }

    @wire(getMultiplePicklistValues, {
        objInfo: 'Actor__c',
        picklistFieldApi: '$allActorPicklistFields'
    })
    wiredActorPicks(data) {
        try {
            if (data.data != undefined && data.data !== '') {
                this.injurySeverityLevelValues = data.data.InjurySeverityLevel__c;
                this.primaryStaffMemValues = data.data.PrimaryStaffMember__c;
                this.staffAssaultedValues = data.data.StaffAssaulted__c;
                this.injurySustainedValues = data.data.InjurySustained__c;
            } else if (data.error) {
                let errors = data.error;
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const { title, message, errorType } = error;
                    this.dispatchEvent(utils.toastMessageWithTitle(title, message, errorType));
                } else {
                    this.dispatchEvent(utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE));
                }
            }
        } catch (error) {
            throw error;
        }
    }

    handleRowSelection = event => {
        if (event.detail.action === undefined && event.detail.selectedRows !== undefined && event.detail.selectedRows.length > 0) {
            this.staffMemberDetList = event.detail.selectedRows;
            this.enableAdd = this.staffMemberDetList.length > 0 ? false : true;
        }
        else return;
    }

    onStaffDetChange(staffDet) {
        this.AffiliationType__c = staffDet.AffiliationType__c;
        this.contact.JobTitle__c = staffDet.JobTitle__c;
        this.contact.EmployeeType__c = staffDet.EmployeeType__c;
        this.contact.AffiliationType__c = staffDet.AffiliationType__c;
        this.contact.staffId = staffDet.Contact__c;
        this.actor.InjurySeverityLevel__c = staffDet.InjurySeverityLevel__c !== undefined ? staffDet.InjurySeverityLevel__c : '';
        this.actor.PrimaryStaffMember__c = staffDet.PrimaryStaffMember__c !== undefined ? staffDet.PrimaryStaffMember__c : '';
        this.actor.StaffAssaulted__c = staffDet.StaffAssaulted__c !== undefined ? staffDet.StaffAssaulted__c : '';
        this.actor.InjurySustained__c = staffDet.InjurySustained__c !== undefined ? staffDet.InjurySustained__c : '';

    }

    onChangeHandlers(event) {
        switch (event.target.name) {
            case 'staffList':
                let staffDet = this.staffMemberListFrmActor.filter(staff => staff.Contact__c === event.target.value)[0];
                this.actor.Contact__c = event.target.value;
                this.actor.Id = staffDet.Id;
                this.onStaffDetChange(staffDet);
                break;
            case 'injSevLevel':
                this.actor.InjurySeverityLevel__c = event.target.value;
                this.enableAdd = false;
                break;
            case 'priStaffMember':
                this.actor.PrimaryStaffMember__c = event.target.value;
                this.enableAdd = false;
                break;
            case 'staffAssaulted':
                this.actor.StaffAssaulted__c = event.target.value;
                this.enableAdd = false;
                break;
            case 'injSustained':
                this.actor.InjurySustained__c = event.target.value;
                this.enableAdd = false;
                break;
        }
    }

    handleRowAction(event) {
        if (event.detail.action.name != undefined && event.detail.action.name === 'dontRedirect') return;
        const action = event.detail.action;
        const row = event.detail.row;
        switch (action.name) {
            case 'editRecord':
                this.showStaffDetailsPop = true;
                this.actor.Id = row.Id;
                let staffDet = this.staffMemberListFrmActor.filter(staff => staff.Id === row.Id)[0];
                this.onStaffDetChange(staffDet);
                this.disableStaffName = true;
                this.addOrEditLable = 'Edit';
                this.saveOrUpdateLable = 'Update'
                break;
            case 'deleteRecord':
                this.deleteRecordId = row.Id;
                this.openmodel = true;
                break;
            case 'viewRecord':
                this.showStaffDetailsPop = true;
                this.actor.Id = row.Id;
                let staffDetTo = this.staffMemberListFrmActor.filter(staff => staff.Id === row.Id)[0];
                this.onStaffDetChange(staffDetTo);
                this.disableStaffName = true;
                this.isDisabled = true;
                break;
        }
    }

    addStaffsToActor() {
        this.Spinner = true;
        this.showStaffMemberPop = false;
        let i = 0;
        this.staffMemberDetList.forEach(d => {
            if (d.Contact__c === undefined || d.Contact__c === null || d.Contact__c === '') return;
            let fields = {};
            fields.Contact__c = d.Contact__c;
            fields.Referral__c = this.caseId;
            fields.Role__c = 'Staff';
            const recordInput = {
                apiName: ACTOR_OBJECT.objectApiName,
                fields
            }
            createRecord(recordInput).then(result => {
                i = i + 1;
                if (i === this.staffMemberDetList.length) {
                    this.Spinner = false;
                    return refreshApex(this.staffWholeDet);
                }
            }
            ).catch((errors) => {
                return this.dispatchEvent(utils.handleError(errors));
            });
        });
    }

    updateActorDetails() {
        this.Spinner = true;
        const fields = this.actor;
        const recordInput = {
            fields
        }
        updateRecord(recordInput)
            .then(actorResult => {
                refreshApex(this.staffWholeDet);
                this.Spinner = false;
                this.closeModel();
                this.dispatchEvent(utils.toastMessage(constPopupVariables.STAFF_DETAIL_UPDATE_SUCESS, "success"));
            }).catch((errors) => {
                return this.dispatchEvent(utils.handleError(errors));
            });
    }

    deleteFrSure() {
        this.openmodel = false;
        this.Spinner = true;
        deleteRecord(this.deleteRecordId)
            .then(() => {
                this.Spinner = false;
                this.dispatchEvent(utils.toastMessage(constPopupVariables.DELETESUCCESS, "success"));
                return refreshApex(this.staffWholeDet);
            }).catch(error => {
                this.Spinner = false;
                this.dispatchEvent(utils.toastMessage(constPopupVariables.UNAUTHOREDEL, "error"));
            })
    }
}