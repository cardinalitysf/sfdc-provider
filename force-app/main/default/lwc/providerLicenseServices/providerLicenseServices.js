/**
 * @Author        : Sindhu Venkateswarlu
 * @CreatedOn     : APRIL 17, 2020
 * @Purpose       : This component contains License Services Details For Provider.
 **/
import { LightningElement, track, api, wire } from "lwc"
import getLicenseDetails from "@salesforce/apex/providerLicenseServices.getLicenseDetails";
import editLicenseDetails from "@salesforce/apex/providerLicenseServices.editLicenseDetails";
import getContractLicenseStatus from "@salesforce/apex/providerLicenseServices.getContractLicenseStatus";

import {
  getPicklistValuesByRecordType,
  getObjectInfo
} from "lightning/uiObjectInfoApi";
import {
  createRecord,
  updateRecord,
  deleteRecord
} from "lightning/uiRecordApi";
import { refreshApex } from "@salesforce/apex";

import { utils } from "c/utils";
import * as sharedData from "c/sharedData";

import images from "@salesforce/resourceUrl/images";

import LICENSESERIVCES_OBJECT from "@salesforce/schema/LicenseServices__c";
import ID_FIELD from "@salesforce/schema/LicenseServices__c.Id";
import CATEGORY_FIELD from "@salesforce/schema/LicenseServices__c.Category__c";
import CLASSIFICATION_FIELD from "@salesforce/schema/LicenseServices__c.Classification__c";
import COST_FIELD from "@salesforce/schema/LicenseServices__c.Cost__c";
import ENDDATE_FIELD from "@salesforce/schema/LicenseServices__c.EndDate__c";
import STARTDATE_FIELD from "@salesforce/schema/LicenseServices__c.StartDate__c";
import PAIDBY_FIELD from "@salesforce/schema/LicenseServices__c.PaidBy__c";
import SERVICEDESCRIPTION_FIELD from "@salesforce/schema/LicenseServices__c.ServiceDescription__c";
import TASK_FIELD from "@salesforce/schema/LicenseServices__c.Task__c";
import UNIT_FIELD from "@salesforce/schema/LicenseServices__c.Unit__c";
import CONTRACT_FIELD from "@salesforce/schema/LicenseServices__c.Contract__c";

const actions = [
  { label: "Edit", name: "Edit", iconName: "utility:edit", target: "_self" },
  { label: "View", name: "View", iconName: "utility:preview", target: "_self" },
  {
    label: "Delete",
    name: "Delete",
    iconName: "utility:delete",
    target: "_self"
  }
];

const columns = [
  {
    label: "License Services",
    fieldName: "Name",
    type: "text",
    sortable: true
  },
  { label: "Category", fieldName: "Category__c", type: "text" },
  { label: "Available Tasks", fieldName: "Task__c", type: "text" },
  { label: "Start Date", fieldName: "StartDate__c", type: "date" },
  { label: "End Date", fieldName: "EndDate__c", type: "date" },
  { label: "Paid By", fieldName: "PaidBy__c", type: "text" },
  { label: "Classification", fieldName: "Classification__c", type: "text" },
  { label: "Unit", fieldName: "Unit__c", type: "text" },
  { label: "$ Cost", fieldName: "Cost__c", type: "text" },
  {
    label: "Service Description",
    fieldName: "ServiceDescription__c",
    type: "text"
  },
  {
    type: "action",
    typeAttributes: { rowActions: actions },
    cellAttributes: {
      iconName: "utility:threedots_vertical",
      iconAlternativeText: "Actions",
      class: "tripledots"
    }
  }
];

export default class ProviderLicenseServices extends LightningElement {
  attachmentIcon = images + "/application-licenseinfo.svg";

  @track columns = columns;
  @track openModel = false;
  @track noRecordsFound = false;
  @track UnitOptions;
  @track PaidByOptions;
  @track ClassificationOptions;
  @track CategoryOptions;
  @track AvailableTasksOptions;
  @track Licensedetails = {
    Category__c: "",
    Task__c: "",
    StartDate__c: "",
    EndDate__c: "",
    Cost__c: "",
    Unit__c: "",
    PaidBy__c: "",
    Classification__c: "",
    ServiceDescription__c: ""
  };

  wiredLicenseResult;
  licenseserviceId;
  contractforApiCall = this.ContractId;

  //Pagination
  @track totalLicenseCount = 0;
  @track totalLicense = [];
  @track page = 1;
  @track currentPageLicenseData;
  setPagination = 5;
  perpage = 10;

  //Sort Function
  @track defaultSortDirection = "asc";
  @track sortDirection = "asc";
  @track sortedBy = "Name";

  @track disabled = false;
  @track deleteModel = false;
  @track viewModel = false;
  @track deleteId;
  @track freezeLicenseScreen = false;

  @track mindate;
  @track maxdate;

  @track btnLabel;
  @track title;
  @track isDisableEndDate = false;

  @api
  get ContractId() {
    return sharedData.getContractId();
  }

  //Sundar Adding this
  get isSuperUserFlow() {
    let userProfile = sharedData.getUserProfileName();

    return userProfile === "Supervisor";
  }

  connectedCallback() {
    this.isSuperUserFlow;
  }

  //Get List Table Values
  @wire(getLicenseDetails, {
    fetchdataname: "$contractforApiCall"
  })
  allLicensetData(result) {
    this.wiredLicenseResult = result;
    if (result.data) {
      this.totalLicenseCount = result.data.length;

      this.noRecordsFound = this.totalLicenseCount === 0;

      this.totalLicense = result.data.map((row) => {
        return {
          Id: row.Id,
          Name: row.Name,
          Category__c: row.Category__c,
          Task__c: row.Task__c,
          StartDate__c: utils.formatDate(row.StartDate__c),
          EndDate__c: utils.formatDate(row.EndDate__c),
          Cost__c: row.Cost__c && row.Cost__c.toString(),
          Unit__c: row.Unit__c,
          PaidBy__c: row.PaidBy__c,
          Classification__c: row.Classification__c,
          ServiceDescription__c: row.ServiceDescription__c
        };
      });

      this.pageData();
      refreshApex(this.wiredLicenseResult);
    }
  }

  @wire(getContractLicenseStatus, {
    contractId: "$contractforApiCall"
  })
  wiredApplicationDetails(result) {
    this.refreshContractStatusData = result;

    try {
      if (result.data) {
        result.data.map((item) => {
          if (
            item.ContractStatus__c === "Active" ||
            item.ContractStatus__c === "Inactive" ||
            item.ContractStatus__c === "Expired" ||
            item.ContractStatus__c === "Submitted"
          ) {
            this.freezeLicenseScreen = true;
          } else {
            this.freezeLicenseScreen = false;
            this.mindate = item.ContractStartDate__c;
            this.maxdate = item.ContractEndDate__c;
          }
        });
      }
    } catch (err) {
      return this.dispatchEvent(utils.handleError(err));
    }
    // if (data) {
    //     if (data.length > 0 && (data[0].ContractStatus__c == 'Active' || data[0].ContractStatus__c == 'Inactive' || data[0].ContractStatus__c == 'Expired' || data[0].ContractStatus__c == 'Submitted')) {
    //         this.freezeLicenseScreen = true;
    //     } else {
    //         this.freezeLicenseScreen = false;
    //         this.mindate=data[0].ContractStartDate__c;
    //         this.maxdate=data[0].ContractEndDate__c;
    //     }
    // }
  }

  //Pagination
  pageData() {
    let page = this.page;
    let perpage = this.perpage;
    let startIndex = page * perpage - perpage;
    let endIndex = page * perpage;
    this.currentPageLicenseData = this.totalLicense.slice(startIndex, endIndex);

    if (this.currentPageLicenseData.length === 0) {
      if (this.page !== 1) {
        this.page = this.page - 1;
        this.pageData();
      }
    }
  }

  //Page Change Action in Pagination Bar
  hanldeProgressValueChange(event) {
    this.page = event.detail;
    this.pageData();
  }

  // Sorting the Data Table Column Function End
  sortBy(field, reverse, primer) {
    const key = primer
      ? function (x) {
        return primer(x[field]);
      }
      : function (x) {
        return x[field];
      };

    return function (a, b) {
      a = key(a);
      b = key(b);
      if (a === undefined) a = "";
      if (b === undefined) b = "";
      a = typeof a === "number" ? a : a.toLowerCase();
      b = typeof b === "number" ? b : b.toLowerCase();

      return reverse * ((a > b) - (b > a));
    };
  }

  //Data Table Sorting Function
  onHandleSort(event) {
    const { fieldName: sortedBy, sortDirection: sortDirection } = event.detail;
    const cloneData = [...this.currentPageLicenseData];

    cloneData.sort(this.sortBy(sortedBy, sortDirection === "asc" ? 1 : -1));

    this.currentPageLicenseData = cloneData;
    this.sortDirection = sortDirection;
    this.sortedBy = sortedBy;
  }

  //Edit/Delete Model Action
  handleRowAction(event) {
    if (
      event.detail.action.name === "Edit" ||
      event.detail.action.name === "View"
    ) {
      if (this.freezeLicenseScreen && event.detail.action.name === "Edit")
        return this.dispatchEvent(
          utils.toastMessage("Cannot edit License Services ", "warning")
        );

      let selectedLicenseId = event.detail.row.Id;

      this.openModel = event.detail.action.name == "Edit" ? true : false;
      this.viewModel = event.detail.action.name == "View" ? true : false;
      this.btnLabel =
        event.detail.action.name == "Edit" ? "UPDATE" : this.btnLabel;
      this.title =
        event.detail.action.name == "Edit"
          ? "EDIT LICENSE SERVICES"
          : this.title;
      this.isDisableEndDate = false;

      this.disabled = false;

      editLicenseDetails({
        selectedLicenseId
      })
        .then((result) => {
          if (result.length > 0) {
            this.Licensedetails = {
              Category__c: result[0].Category__c,
              Task__c: result[0].Task__c,
              StartDate__c: result[0].StartDate__c,
              EndDate__c: result[0].EndDate__c,
              Cost__c: result[0].Cost__c,
              Unit__c: result[0].Unit__c,
              PaidBy__c: result[0].PaidBy__c,
              Classification__c: result[0].Classification__c,
              ServiceDescription__c: result[0].ServiceDescription__c
            };
            this.licenseserviceId = result[0].Id;
          } else {
            this.dispatchEvent(
              utils.toastMessage(
                "Error in getting License Services details",
                "error"
              )
            );
          }
        })
        .catch((error) => {
          this.dispatchEvent(
            utils.toastMessage(
              "Error in getting License Services details",
              "error"
            )
          );
        });
    } else if (event.detail.action.name == "Delete") {
      if (this.freezeLicenseScreen)
        return this.dispatchEvent(
          utils.toastMessage("Can not delete License Services", "warning")
        );

      this.deleteModel = true;
      this.deleteId = event.detail.row.Id;
    }
  }

  handleDelete() {
    deleteRecord(this.deleteId)
      .then(() => {
        this.deleteModel = false;
        this.dispatchEvent(
          utils.toastMessage(
            "License Services Details deleted successfully",
            "success"
          )
        );

        return refreshApex(this.wiredLicenseResult);
      })
      .catch((error) => {
        this.deleteModel = false;
        this.dispatchEvent(
          utils.toastMessage(
            "Error in delete License Services details",
            "error"
          )
        );
        return refreshApex(this.wiredLicenseResult);
      });
  }

  closeDeleteModal() {
    this.deleteModel = false;
  }

  //Fields On Change Event Handler
  LicenseOnChange(event) {
    this.disabled = false;

    if (event.target.name == "Category") {
      this.Licensedetails.Category__c = event.detail.value;
    } else if (event.target.name == "AvailableTasks") {
      this.Licensedetails.Task__c = event.detail.value;
    } else if (event.target.name == "StartDate") {
      this.Licensedetails.StartDate__c = event.target.value;
      this.isDisableEndDate = false;
    } else if (event.target.name == "EndDate") {
      this.Licensedetails.EndDate__c = event.target.value;
    } else if (event.target.name == "Cost") {
      this.Licensedetails.Cost__c = event.target.value;
    } else if (event.target.name == "Unit") {
      this.Licensedetails.Unit__c = event.detail.value;
    } else if (event.target.name == "PaidBy") {
      this.Licensedetails.PaidBy__c = event.detail.value;
    } else if (event.target.name == "Classification") {
      this.Licensedetails.Classification__c = event.detail.value;
    } else if (event.target.name == "ServiceDescription") {
      this.Licensedetails.ServiceDescription__c = event.target.value;
    }
  }

  //open License dialog box
  openLicenseModel() {
    if (this.freezeLicenseScreen)
      return this.dispatchEvent(
        utils.toastMessage("Cannot add Lincese Services", "warning")
      );

    this.openModel = true;
    this.btnLabel = "CREATE";
    this.title = "ADD LICENSE SERVICES";
    this.disabled = true;
    this.Licensedetails = {};
    this.isDisableEndDate = true;
  }
  closeDeleteLicense() {
    this.openModel = false;
  }

  //Close License dialog box
  closeLicenseModel() {
    this.openModel = false;
    this.viewModel = false;
  }
  //Cancel License dialog box
  cancelLicenseDetails() {
    this.openModel = false;
    this.viewModel = false;
  }

  //Create License
  CreateLicenseDetails() {
    if (
      (this.Licensedetails.StartDate__c) <
      (this.mindate) || (this.Licensedetails.StartDate__c) >
      (this.maxdate)
    )
      return this.dispatchEvent(
        utils.toastMessage(
          "Please select Dates between Contract Start Date and End Date",
          "warning"
        )
      );
    if (
      (this.Licensedetails.EndDate__c) <
      (this.mindate) || (this.Licensedetails.EndDate__c) >
      (this.maxdate)
    )
      return this.dispatchEvent(
        utils.toastMessage(
          "Please select Dates between Contract Start Date and End Date",
          "warning"
        )
      );
    if (!this.Licensedetails.Category__c)
      return this.dispatchEvent(utils.toastMessage("Please Select Category", "warning"));
    if (!this.Licensedetails.Task__c)
      return this.dispatchEvent(utils.toastMessage("Please Select Task", "warning"));
    if (!this.Licensedetails.StartDate__c)
      return this.dispatchEvent(utils.toastMessage("Please Select StartDate", "warning"));
    if (!this.Licensedetails.EndDate__c)
      return this.dispatchEvent(utils.toastMessage("Please Select EndDate", "warning"));
    if (!this.Licensedetails.Cost__c)
      return this.dispatchEvent(utils.toastMessage("Please Enter Cost", "warning"));
    if (!this.Licensedetails.Unit__c)
      return this.dispatchEvent(utils.toastMessage("Please Select Unit", "warning"));
    if (!this.Licensedetails.PaidBy__c)
      return this.dispatchEvent(utils.toastMessage("Please Select PaidBy", "warning"));
    if (!this.Licensedetails.Classification__c)
      return this.dispatchEvent(utils.toastMessage("Please Select Classification", "warning"));

    const allValid = [
      ...this.template.querySelectorAll("lightning-input"),
      ...this.template.querySelectorAll("lightning-combobox")
    ].reduce((validSoFar, inputFields) => {
      inputFields.reportValidity();
      return validSoFar && inputFields.checkValidity();
    }, true);
    if (allValid) {

      if (
        new Date(this.Licensedetails.StartDate__c).getTime() >
        new Date(this.Licensedetails.EndDate__c).getTime()
      )
        return this.dispatchEvent(
          utils.toastMessage(
            "End date should be Greater than Start Date ",
            "warning"
          )
        );

      this.disabled = true;

      const fields = {};

      fields[CATEGORY_FIELD.fieldApiName] = this.Licensedetails.Category__c;
      fields[
        CLASSIFICATION_FIELD.fieldApiName
      ] = this.Licensedetails.Classification__c;
      fields[COST_FIELD.fieldApiName] = this.Licensedetails.Cost__c;
      fields[ENDDATE_FIELD.fieldApiName] = this.Licensedetails.EndDate__c;
      fields[STARTDATE_FIELD.fieldApiName] = this.Licensedetails.StartDate__c;
      fields[PAIDBY_FIELD.fieldApiName] = this.Licensedetails.PaidBy__c;
      fields[
        SERVICEDESCRIPTION_FIELD.fieldApiName
      ] = this.Licensedetails.ServiceDescription__c;
      fields[TASK_FIELD.fieldApiName] = this.Licensedetails.Task__c;
      fields[UNIT_FIELD.fieldApiName] = this.Licensedetails.Unit__c;
      fields[CONTRACT_FIELD.fieldApiName] = this.ContractId;

      if (!this.licenseserviceId) {
        const recordInput = {
          apiName: LICENSESERIVCES_OBJECT.objectApiName,
          fields
        };


        createRecord(recordInput)
          .then((result) => {
            this.openModel = false;
            this.Licensedetails = {};
            this.licenseserviceId = null;

            this.dispatchEvent(
              utils.toastMessage(
                "License Service Details has been created successfully",
                "success"
              )
            );
            this.disabled = false;

            return refreshApex(this.wiredLicenseResult);
          })
          .catch((error) => {
            this.disabled = false;
            this.dispatchEvent(
              utils.toastMessage(
                "Error in saving License Service Details. Please check.",
                "error"
              )
            );
          });
      } else {
        fields[ID_FIELD.fieldApiName] = this.licenseserviceId;

        const recordInput = { fields };

        updateRecord(recordInput)
          .then(() => {
            this.openModel = false;
            this.Licensedetails = {};
            this.licenseserviceId = null;
            this.disabled = false;

            this.dispatchEvent(
              utils.toastMessage(
                "License Service Details has been updated successfully",
                "success"
              )
            );

            return refreshApex(this.wiredLicenseResult);
          })
          .catch((error) => {
            this.disabled = false;
            this.dispatchEvent(
              utils.toastMessage(
                "Error in saving License  Service Details. Please check.",
                "error"
              )
            );
          });
      }
    } else {
      this.disabled = false;
      this.dispatchEvent(
        utils.toastMessage(
          "Error in saving Contact Details. Please check.",
          "error"
        )
      );
    }
  }

  //Declare License object into one variable
  @wire(getObjectInfo, { objectApiName: LICENSESERIVCES_OBJECT })
  objectInfo;

  //Function used to get all picklist values from License object
  @wire(getPicklistValuesByRecordType, {
    objectApiName: LICENSESERIVCES_OBJECT,
    recordTypeId: "$objectInfo.data.defaultRecordTypeId"
  })
  getAllPicklistValues({ error, data }) {
    if (data) {
      this.error = null;

      // Category Type Field Picklist values
      let CategoryTypeOptions = [];
      data.picklistFieldValues.Category__c.values.forEach((key) => {
        CategoryTypeOptions.push({
          label: key.label,
          value: key.value
        });
      });
      this.CategoryOptions = CategoryTypeOptions;

      // Task Type Field Picklist values
      let TaskTypeOptions = [];
      data.picklistFieldValues.Task__c.values.forEach((key) => {
        TaskTypeOptions.push({
          label: key.label,
          value: key.value
        });
      });
      this.AvailableTasksOptions = data.picklistFieldValues.Task__c.values;

      // Classification Type Field Picklist values
      let ClassificationTypeOptions = [];
      data.picklistFieldValues.Classification__c.values.forEach((key) => {
        ClassificationTypeOptions.push({
          label: key.label,
          value: key.value
        });
      });
      this.ClassificationOptions =
        data.picklistFieldValues.Classification__c.values;

      // Unit Type Field Picklist values
      let UnitTypeOptions = [];
      data.picklistFieldValues.Unit__c.values.forEach((key) => {
        UnitTypeOptions.push({
          label: key.label,
          value: key.value
        });
      });
      this.UnitOptions = data.picklistFieldValues.Unit__c.values;
      // PaidBy Type Field Picklist values
      let PaidByTypeOptions = [];
      data.picklistFieldValues.PaidBy__c.values.forEach((key) => {
        PaidByTypeOptions.push({
          label: key.label,
          value: key.value
        });
      });
      this.PaidByOptions = data.picklistFieldValues.PaidBy__c.values;
    } else if (error) {
      this.error = JSON.stringify(error);
    }
  }
}
