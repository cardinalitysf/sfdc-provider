/**
 * @Author        : Tamilarasan G
 * @CreatedOn     : July 3, 2020
 * @Purpose       : Complaints Investigation / Findings Detail Page
**/
import { LightningElement, api, track, wire } from 'lwc';
import images from "@salesforce/resourceUrl/images";
import getUploadedDocuments from "@salesforce/apex/ComplaintsInvestigationFindings.getUploadedDocuments";
import InsertUpdateProviderDecision from '@salesforce/apex/ComplaintsInvestigationFindings.InsertUpdateProviderDecision';
import saveSign from '@salesforce/apex/ComplaintsInvestigationFindings.saveSign';
import getWorkItemId from '@salesforce/apex/ComplaintsInvestigationFindings.getWorkItemId';
import updateProviderApprovalProcess from '@salesforce/apex/ComplaintsInvestigationFindings.updateProviderApprovalProcess';
import getSignatureDetails from '@salesforce/apex/ComplaintsInvestigationFindings.getLoginUserSignature';
import getSupervisorResponse from '@salesforce/apex/ComplaintsInvestigationFindings.getSupervisorResponse';
import { CJAMS_CONSTANTS } from "c/constants";

import { NavigationMixin } from 'lightning/navigation';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import DEFICIENCY_OBJECT from '@salesforce/schema/Deficiency__c';
import STATUS_FIELD from '@salesforce/schema/Deficiency__c.ProviderDecision__c';
import * as shareddata from 'c/sharedData';
import { utils } from "c/utils";

// declaration of variables
// for calculations
let isDownFlag,
    isDotFlag = false,
    prevX = 0,
    currX = 0,
    prevY = 0,
    currY = 0;

let x = "#0000A0"; //blue color
let y = 1.5; //weight of line width and dot.       

let canvasElement, ctx; //storing canvas context
let attachment; //holds attachment information after saving the sigture on canvas
let dataURL, convertedDataURI; //holds image data

export default class ComplaintsInvestigationFindingsDetailPage extends NavigationMixin(LightningElement) {

    @api capDetails;
    @track complaintId;
    @track deficiencyId;
    @track complaintDocumentShowHide = false;
    @track dataStr = [];
    @track noRecordDisplay = true;
    @track providerStatus;
    @track providerComments;
    @track deficiencyStatus;
    @track SuperVisorResponse = '';
    @track openmodel = false;
    @track SignatureShowHide = false;
    @track SignatureModalShow = false;
    @track DisableStatusAndComments = true;    //Disable inputs based on the submit application
    @track ProviderDecisionStatusOptions;
    @track SignatureValidationView;
    @track WorkItemID;
    @track NextApproverId;
    @track ApprovalStatus;
    @track ApprovalComments;
    @track addsign;
    helpIcon = images + "/help.svg";
    privateIcon = images + "/office-building-box.svg";
    descriptionIcon = images + '/description-icon.svg';
    refreshIcon = images + '/refresh-icon.svg';
    InvestigationFindingsIcon = images + "/deficiencyIcon.svg";


    connectedCallback() {
        this.complaintId = this.capDetails.ComplaintId;
        this.deficiencyId = this.capDetails.DeficiencyId;
        this.NextApproverId = this.capDetails.NextApproverId;
        this.providerStatus = this.capDetails.ProviderDecision;
        this.providerComments = this.capDetails.ProviderComments;
        this.deficiencyStatus = this.capDetails.Status;
        let actionName = this.capDetails.actionName != undefined ? true : false;

        if (this.deficiencyStatus != 'Pending' || actionName)
            this.DisableStatusAndComments = true;
        else
            this.DisableStatusAndComments = false;

        this.getProviderSignature();
        this.getProviderWorkItem();
        getSupervisorResponse({ complaintId: this.complaintId }).then(result => {
            if (result.length > 0) {
                this.SuperVisorResponse = result[0].RecordType.Name;
            }
        }).catch(errors => {
            let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
            return this.dispatchEvent(utils.handleError(errors, config));
        });
    }
    RedirecttoInvestigationDashboard() {
        const redirecttoinvestigationdashboard = new CustomEvent('redirecttoinvestigationdashboard', {
            detail: {
                isView: true
            }
        });
        this.dispatchEvent(redirecttoinvestigationdashboard);
    }

    handleComplaintDocuments() {
        this.complaintDocumentShowHide = !this.complaintDocumentShowHide;
        if (this.complaintDocumentShowHide) {
            this.template.querySelector('.chevron').iconName = 'utility:up';
        }
        else {
            this.template.querySelector('.chevron').iconName = 'utility:down';
        }
    }

    handleChange(event) {
        if (event.target.name == 'ProviderDecisionStatus')
            this.providerStatus = event.target.value;
        if (event.target.name == 'ProviderDecisionComments')
            this.providerComments = event.target.value;
    }
    handleSaveAsDraft() {
        let myobj = { sobjectType: 'Deficiency__c' }
        myobj.Id = this.deficiencyId;
        myobj.ProviderDecision__c = this.providerStatus;
        myobj.ProviderComments__c = this.providerComments;
        InsertUpdateProviderDecision({ objSobjecttoUpdateOrInsert: myobj }).then(result => {
            this.dispatchEvent(utils.toastMessage("Record Saved Successfully", "success"));
            this.RedirecttoInvestigationDashboard();
        }).catch(errors => {
            let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
            return this.dispatchEvent(utils.handleError(errors, config));
        });

    }
    handleSubmit() {
        if (!this.providerStatus)
            return this.dispatchEvent(utils.toastMessage("Please choose the Status", "error"));

        this.ApprovalStatus = 'Approve'; //this.providerStatus =='Accepted' ? 'Approve' : 'Reject';
        this.ApprovalComments = this.providerComments != undefined ? this.providerComments : '';

        let myobj = { sobjectType: 'Deficiency__c' }
        myobj.Id = this.deficiencyId;
        myobj.ProviderDecision__c = this.providerStatus;
        myobj.ProviderComments__c = this.providerComments;
        myobj.Status__c = 'Submitted';


        let myobjcase = { sobjectType: 'Case' }
        myobjcase.Id = this.complaintId;
        myobjcase.Status = this.providerStatus;


        updateProviderApprovalProcess({ nextApproverId: this.NextApproverId, workItemId: this.WorkItemID, status: this.ApprovalStatus, comments: this.ApprovalComments }).then(result => {
            this.dispatchEvent(utils.toastMessage("Record Saved Successfully", "success"));
            InsertUpdateProviderDecision({ objSobjecttoUpdateOrInsert: myobj }).then(result => {
                // this.dispatchEvent(utils.toastMessage("Record Saved Successfully", "success"));
            }).catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
            InsertUpdateProviderDecision({ objSobjecttoUpdateOrInsert: myobjcase }).then(result => {
                // this.dispatchEvent(utils.toastMessage("Record Saved Successfully", "success"));
            }).catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });

            this.RedirecttoInvestigationDashboard();
        }).catch(errors => {
            let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
            return this.dispatchEvent(utils.handleError(errors, config));
        });

    }
    // Wire Service for fetching files for specified target record
    @wire(getUploadedDocuments, {
        strDocumentID: '$complaintId'
    })
    DocumentData(value) {
        this.wiredAttachment = value;
        const {
            data,
            error
        } = value;
        if (data) {
            setTimeout(() => {
                this.dataStr = data;
                if (data.length > 0)
                    this.noRecordDisplay = false;
            }, 300);
            // update the start position with end postion


        } else if (error) {
            this.dataStringNotEmpty = false;
        }
    }

    viewFile(event) {
        this[NavigationMixin.Navigate]({
            type: 'standard__namedPage',
            attributes: {
                pageName: 'filePreview'
            },
            state: {
                // assigning ContentDocumentId to show the preview of file
                selectedRecordId: event.currentTarget.dataset.id
            }
        });

    }
    downloadFile(event) {
        this[NavigationMixin.Navigate]({
            type: 'standard__webPage',
            attributes: {
                url: window.location.origin + '/sfc/servlet.shepherd/document/download/' + event.currentTarget.dataset.id
            }
        },
            false // Replaces the current page in your browser history with the URL
        );
    }

    @wire(getObjectInfo, { objectApiName: DEFICIENCY_OBJECT })
    objectInfo;

    @wire(getPicklistValues, { recordTypeId: '$objectInfo.data.defaultRecordTypeId', fieldApiName: STATUS_FIELD })
    getAllPicklistValues({ error, data }) {
        if (data) {
            // Deficiency Field Picklist values
            let deficiencyOptions = [];
            data.values.forEach((key) => {
                deficiencyOptions.push({
                    label: key.label,
                    value: key.value
                });
            });
            this.ProviderDecisionStatusOptions = deficiencyOptions;
        }
        else if (error) {
        }
    }

    getProviderWorkItem() {
        getWorkItemId({ complaintId: this.complaintId }).then(result => {
            if (result.length > 0) {
                this.WorkItemID = result[0].Workitems != undefined ? result[0].Workitems[0].Id : '';
            }
        }).catch(errors => {
            let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
            return this.dispatchEvent(utils.handleError(errors, config));
        });
    }
    //Signature Code Start

    getProviderSignature() {
        getSignatureDetails({ strDocumentID: this.complaintId }).then(result => {
            if (result.length > 0) {
                this.SignatureShowHide = true;
                this.addsign = true;
                this.SignatureValidationView = '/sfc/servlet.shepherd/version/download/' + result[0].Id;
            }
            else {
                this.SignatureShowHide = false;
            }

        }).catch(errors => {
            let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
            return this.dispatchEvent(utils.handleError(errors, config));
        });
    }

    signatureCloseModal() {
        this.SignatureModalShow = false;
    }
    signatureOpenModal() {
        // to open modal window set 'bShowsignatureModal' track value as true
        this.SignatureModalShow = true;

    }

    @api recordId;

    //event listeners added for drawing the signature within shadow boundary
    constructor() {
        super();
        this.template.addEventListener('mousemove', this.handleMouseMove.bind(this));
        this.template.addEventListener('mousedown', this.handleMouseDown.bind(this));
        this.template.addEventListener('mouseup', this.handleMouseUp.bind(this));
        this.template.addEventListener('mouseout', this.handleMouseOut.bind(this));
    }

    //retrieve canvase and context
    renderedCallback() {
        canvasElement = this.template.querySelector('canvas');
        setTimeout(function () {
            ctx = canvasElement.getContext("2d");
        }, 500);
    }

    //handler for mouse move operation
    handleMouseMove(event) {
        this.searchCoordinatesForEvent('move', event);
    }

    //handler for mouse down operation
    handleMouseDown(event) {
        this.searchCoordinatesForEvent('down', event);
    }

    //handler for mouse up operation
    handleMouseUp(event) {
        this.searchCoordinatesForEvent('up', event);
    }

    //handler for mouse out operation
    handleMouseOut(event) {
        this.searchCoordinatesForEvent('out', event);
    }

    /*
        handler to perform save operation.
        save signature as attachment.
        after saving shows success or failure message as toast
    */
    handleSaveClick() {
        //set to draw behind current content
        ctx.globalCompositeOperation = "destination-over";
        ctx.fillStyle = "#FFF"; //white
        ctx.fillRect(0, 0, canvasElement.width, canvasElement.height);

        //convert to png image as dataURL
        dataURL = canvasElement.toDataURL("image/png");
        //convert that as base64 encoding
        convertedDataURI = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");

        //call Apex method imperatively and use promise for handling sucess & failure
        saveSign({
            strSignElement: convertedDataURI,
            recId: this.complaintId
        })
            .then(result => {
                this.signatureCloseModal();
                this.dispatchEvent(utils.toastMessage("Signature saved Successfully", "success"));
            }).catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });

    }

    //clear the signature from canvas
    handleClearClick() {
        this.SignatureShowHide = false;
        ctx.clearRect(0, 0, canvasElement.width, canvasElement.height);
    }

    searchCoordinatesForEvent(requestedEvent, event) {
        // event.preventDefault();
        if (requestedEvent === 'down') {
            this.setupCoordinate(event);
            isDownFlag = true;
            isDotFlag = true;
            if (isDotFlag) {
                this.drawDot();
                isDotFlag = false;
            }
        }
        if (requestedEvent === 'up' || requestedEvent === "out") {
            isDownFlag = false;
        }
        if (requestedEvent === 'move') {
            if (isDownFlag) {
                this.setupCoordinate(event);
                this.redraw();
            }
        }
    }

    //This method is primary called from mouse down & move to setup cordinates.
    setupCoordinate(eventParam) {
        //get size of an element and its position relative to the viewport 
        //using getBoundingClientRect which returns left, top, right, bottom, x, y, width, height.
        const clientRect = canvasElement.getBoundingClientRect();
        prevX = currX;
        prevY = currY;
        currX = eventParam.clientX - clientRect.left;
        currY = eventParam.clientY - clientRect.top;
    }

    //For every mouse move based on the coordinates line to redrawn
    redraw() {
        ctx.beginPath();
        ctx.moveTo(prevX, prevY);
        ctx.lineTo(currX, currY);
        ctx.strokeStyle = x; //sets the color, gradient and pattern of stroke
        ctx.lineWidth = y;
        ctx.closePath(); //create a path from current point to starting point
        ctx.stroke(); //draws the path
    }

    //this draws the dot
    drawDot() {
        ctx.beginPath();
        ctx.fillStyle = x; //blue color
        ctx.fillRect(currX, currY, y, y); //fill rectrangle with coordinates
        ctx.closePath();
    }
    handleFilesChange(event) {
        let strFileNames = '';
        // Get the list of uploaded files
        const uploadedFiles = event.detail.files;

        for (let i = 0; i < uploadedFiles.length; i++) {
            strFileNames += uploadedFiles[i].name + ', ';

        }
        this.dispatchEvent(utils.toastMessage(strFileNames + " Signature uploaded Successfully", "success"));
    }
    //Signature Code End
}