/**
 * @Author        : Saranraj v
 * @CreatedOn     : May 16,2020
 * @Purpose       : public provider dashboard
 * @updatedBy     :
 * @updatedOn     :
 **/
import {
  LightningElement,
  wire,
  track,
  api
} from "lwc";
import getpublicProviderDashboard from "@salesforce/apex/publicProviderDashboard.getpublicProviderDashboard";
import {
  loadStyle
} from "lightning/platformResourceLoader";
import myResource from '@salesforce/resourceUrl/styleSheet';
import * as sharedData from "c/sharedData";
import images from "@salesforce/resourceUrl/images";



export default class PublicProviderDashboard extends LightningElement {
  @api searchKey = "";
  // for initail data
  pendingData = [];
  approvedData = [];
  rejectedData = [];
  totalData = [];
  returnedData = [];

  // for Status
  @track pending = "Pending";
  @track approved = "Approved";
  @track rejected = "Rejected";
  @track total = "Total Applications";
  @track returned = "Returned";
  @track pendingStatus;
  @track returnedStatus;
  @track approvedStatus;
  @track rejectedStatus;
  @track totalStatus;

  @track showApprovedData;
  @track showTotalData;
  @track showPendingData;
  @track showRejectedData;
  @track showReturnedData;
  @track activeClass = "border: 2px solid black";
  @track openModelwindow = false;


  @track tableDataVisible = true;
  @track showPublicPrivateBtn = true;
  @track publicReferralDataShow = true;
  @track privateReferralDataShow = false;
  @track referralName = true;
  @api redirection = "redirect";
  @track searchString = '';
  privateIcon = images + "/office-building.svg";
  existingIcon = images + "/clipboard.svg";
  plusIcon = images + "/plus-new-referal.svg";
  publicIcon = images + "/houseIcon.svg";

  renderedCallback() {
    Promise.all([loadStyle(this, myResource + "/styleSheet.css")]);
  }

  // for pending
  @api get getPendingData() {
    return this.pendingData;
  }

  @api get getPendingStatus() {
    return this.pendingStatus;
  }
  // for pending end

  // for Approved
  @api get getApprovedData() {
    return this.approvedData;
  }

  @api get getApprovedStatus() {
    return this.approvedStatus;
  }
  // for Approved end

  // Rejected
  @api get getRejectedData() {
    return this.rejectedData;
  }

  @api get getRejectedStatus() {
    return this.rejectedStatus;
  }
  // rejected end

  //   for Total
  @api get getTotalData() {
    return this.totalData;
  }

  @api get getTotalStatus() {
    return this.totalStatus;
  }

  // for Total end

  openModel() {
    sharedData.setCaseId(undefined)
    this.openModelwindow = true;
  }

  closeModel() {
    this.openModelwindow = false;
  }

  // getpublicProviderDashboard functionality start

  @wire(getpublicProviderDashboard, {
    searchString: '$searchString'
  })
  publicReferralDataFromApex({
    data,
    error
  }) {
    
    this.publicProviderData = data;

    if (data) {
      let pendingData = this.filterAndAssignArray(data, "Draft");
      let finalPendingData = this.mapAndAssignArray(pendingData, "Draft");
      let pData = this.filterAndAssignArray(data, "Pending");
      let finalPData = this.mapAndAssignArray(pData, "Pending");
      let assigningTraining = this.filterAndAssignArray(
        data,
        "Assigned for Training"
      );
      let finalAssigningTraining = this.mapAndAssignArray(
        assigningTraining,
        "Assigned for Training"
      );
      let trainingUnattended = this.filterAndAssignArray(
        data,
        "Training Unattended"
      );
      let finalTrainingUnattended = this.mapAndAssignArray(
        trainingUnattended,
        "Training Unattended"
      );
      let trainingCompleted = this.filterAndAssignArray(
        data,
        "Training Completed"
      );
      let finalTrainingCompleted = this.mapAndAssignArray(
        trainingCompleted,
        "Training Completed"
      );
      this.pendingData = [
        ...finalPendingData,
        ...finalAssigningTraining,
        ...finalTrainingUnattended,
        ...finalTrainingCompleted,
        ...finalPData,
      ];
      this.pendingStatus = this.pendingData.length;


      let approvedData = this.filterAndAssignArray(data, "Approved");
      this.approvedData = this.mapAndAssignArray(approvedData, "Approved");
      this.approvedStatus = this.approvedData.length;

      let rejectedData = this.filterAndAssignArray(data, "Rejected");
      this.rejectedData = this.mapAndAssignArray(rejectedData, "Rejected");
      this.rejectedStatus = this.rejectedData.length;

      this.totalData = [
        ...this.pendingData,
        ...this.rejectedData,
        ...this.approvedData
      ];
      this.totalStatus = this.totalData.length;
    }
  }

  // getpublicProviderDashboard functionality end


  // filterAndAssignArray start

  filterAndAssignArray = (data, filterValue) =>
    data && data.filter((item) => item.Status === filterValue);

  mapAndAssignArray(dataArray, status) {
    return dataArray.map((item) => {
      return Object.assign({
        ...item
      }, {
        Status: status !== undefined ? status : item.Status,
        ReferralNumber: item.ReferralNumber,
        ReferralType: item.ReferralType,
        Program: item.Program ? item.Program : '-',
        ProgramType: item.ProgramType ? item.ProgramType : '-',
        Applicant: item.Applicant,
        CoApplicant: item.CoApplicant,
        statusClass: this.statusHandler(item && item.Status),
      });
    });
  }

  // filterAndAssignArray end

  // statusHandler functionality start

  statusHandler = (status) => {
    if (status === "Assigned for Training") {
      return "Pending"
    } else if (status === "Training Unattended") {
      return "Rejected"
    } else if (status === "Training Completed") {
      return "Completed"
    } else if (status === "Draft") {
      return ""
    } else if (status === "Approved") {
      return "Approved"
    } else if (status === "Rejected") {
      return "Rejected"
    } else {
      return ""
    }
  }

  // statusHandler functionality end


  handleStatusCard(event) {
    if (event.target.dataset.id === "Pending") {
      this.showPendingData = true;
      this.showApprovedData = false;
      this.showTotalData = false;
      this.showRejectedData = false;
      this.tableDataVisible = false;
      this.showReturnedData = false;
      this.activeClass = "";
      this.template.querySelector(".pending").classList.add("pending-cls");
      this.template.querySelector(".approved").classList.remove("approved-cls");
      this.template.querySelector(".rejected").classList.remove("rejected-cls");
      this.template.querySelector(".total").classList.remove("total-cls");
    } else if (event.target.dataset.id === "Approved") {
      this.showApprovedData = true;
      this.showPendingData = false;
      this.showTotalData = false;
      this.showRejectedData = false;
      this.tableDataVisible = false;
      this.showReturnedData = false;
      this.template.querySelector(".approved").classList.add("approved-cls");
      this.template.querySelector(".pending").classList.remove("pending-cls");
      this.template.querySelector(".rejected").classList.remove("rejected-cls");
      this.template.querySelector(".total").classList.remove("total-cls");
    } else if (event.target.dataset.id === "Rejected") {
      this.showRejectedData = true;
      this.showPendingData = false;
      this.showApprovedData = false;
      this.showTotalData = false;
      this.tableDataVisible = false;
      this.showReturnedData = false;
      this.template.querySelector(".rejected").classList.add("rejected-cls");
      this.template.querySelector(".pending").classList.remove("pending-cls");
      this.template.querySelector(".approved").classList.remove("approved-cls");
      this.template.querySelector(".total").classList.remove("total-cls");
    } else if (event.target.dataset.id === "Total Applications") {
      this.showTotalData = true;
      this.tableDataVisible = false;
      this.showRejectedData = false;
      this.showPendingData = false;
      this.showApprovedData = false;
      this.showReturnedData = false;
      this.template.querySelector(".total").classList.add("total-cls");
      this.template.querySelector(".pending").classList.remove("pending-cls");
      this.template.querySelector(".rejected").classList.remove("rejected-cls");
      this.template.querySelector(".approved").classList.remove("approved-cls");
    } else if (event.target.dataset.id === "Returned") {
      this.showRejectedData = false;
      this.showReturnedData = true;
      this.showPendingData = false;
      this.showApprovedData = false;
      this.showTotalData = false;
      this.tableDataVisible = false;
      this.template.querySelector(".rejected").classList.remove("rejected-cls");
      this.template.querySelector(".pending").classList.remove("pending-cls");
      this.template.querySelector(".approved").classList.remove("approved-cls");
      this.template.querySelector(".total").classList.remove("total-cls");
    } else {
      this.tableDataVisible = true;
    }
  }

  // handleClick functionality start
  handleClick = (event) => {
    const whichReferral = new CustomEvent("referralclick", {
      detail: {
        name: event.target.name
      }
    });
    this.dispatchEvent(whichReferral);
    if (event.target.name === "Public") {
      this.publicReferralDataShow = true;
      this.privateReferralDataShow = false;
      this.showPublicPrivateBtn = false;
    } else {
      this.privateReferralDataShow = true;
      this.publicReferralDataShow = false;
      this.showPublicPrivateBtn = false;
      this.referralName = false;
    }
  };
  // handleClick functionality end

  intimateParentToCloseModal() {
    this.openModelwindow = false;
  }

  // handleRedirection functionality start
  handleRedirection() {
    const redirect = new CustomEvent("redirect", {
      detail: true
    });
    this.dispatchEvent(redirect);
  }
  // handleRedirection functionality end

  // handleChange functionality start
  handleChange = (event) => {
    this.searchString = event.target.value
  }
  // handleChange functionality end


  handleSetValues = (event) => {
    this.publicProviderData.map(item => {
      if (event.detail.redirectId === item.ReferralID) {
        sharedData.setPublicReferralData(item.ReferralNumber, item.Status)
      }
    })
  }
}