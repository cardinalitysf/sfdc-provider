/**
 * @Author        : Sundar Karuppalagu
 * @CreatedOn     : JULY 05 ,2020
 * @Purpose       : Sanctions Limitation Screen
 * @UpdatedBy     : Sundar K -> JULY 24, 2020 -> Complaints License Details method added and Sanction ID stored in Complaint Object.
 **/

import { LightningElement, track, wire, api } from 'lwc';

import getRecordType from "@salesforce/apex/ComplaintsSanctionLimitation.getRecordType";
import getComplaintLicenseDetails from "@salesforce/apex/ComplaintsSanctionLimitation.getComplaintLicenseDetails";
import getSelectedSanctionData from "@salesforce/apex/ComplaintsSanctionLimitation.getSelectedSanctionData";

import * as sharedData from "c/sharedData";
import { utils } from "c/utils";
import { CJAMS_CONSTANTS, BUTTON_LABELS, TOAST_HEADER_LABELS, DATATABLE_BUTTON_LABELS } from "c/constants";

import { createRecord, updateRecord } from "lightning/uiRecordApi";
import { getObjectInfo, getPicklistValuesByRecordType } from "lightning/uiObjectInfoApi";

//Sanction Object fields declaration
import SANCTIONS_OBJECT from "@salesforce/schema/Sanctions__c";
import ID_FIELD from "@salesforce/schema/Sanctions__c.Id";
import YOUTHSERVED_FIELD from "@salesforce/schema/Sanctions__c.Numberofyouthserved__c";
import YOUTHEFFECTIVEDATE_FIELD from "@salesforce/schema/Sanctions__c.YouthEffectiveDate__c";
import YOUTHENDDATE_FIELD from "@salesforce/schema/Sanctions__c.YouthEndDate__c";
import YOUTHLIMITATIONREASON_FIELD from "@salesforce/schema/Sanctions__c.YouthLimitationReason__c";

import MINAGE_FIELD from "@salesforce/schema/Sanctions__c.MinAge__c";
import MAXAGE_FIELD from "@salesforce/schema/Sanctions__c.MaxAge__c";
import GENDER_FIELD from "@salesforce/schema/Sanctions__c.Gender__c";
import POPULATIONLIMITATIONREASON_FIELD from "@salesforce/schema/Sanctions__c.PopulationLimitationReason__c";
import POPULATIONEFFECTIVEDATE_FIELD from "@salesforce/schema/Sanctions__c.PopulationEffectiveDate__c";
import POPULATIONENDDATE_FIELD from "@salesforce/schema/Sanctions__c.PopulationEndDate__c";

import PROVIDERASSIGNEDSERVICE_FIELD from "@salesforce/schema/Sanctions__c.ProviderAssignedService__c";
import PROGRAMEFFECTIVEDATE_FIELD from "@salesforce/schema/Sanctions__c.ProgramEffectiveDate__c";
import PROGRAMENDDATE_FIELD from "@salesforce/schema/Sanctions__c.ProgramEndDate__c";
import PROGRAMLIMITATIONREASON_FIELD from "@salesforce/schema/Sanctions__c.ProgramLimitationReason__c";
import DOCUMENT_FIELD from "@salesforce/schema/Sanctions__c.Document__c";
import DELIVERY_FIELD from "@salesforce/schema/Sanctions__c.Delivery__c";

import LIMITATION_FIELD from "@salesforce/schema/Sanctions__c.Limitation__c";
import COMPLAINT_FIELD from "@salesforce/schema/Sanctions__c.Complaint__c";
import PROVIDER_FIELD from "@salesforce/schema/Sanctions__c.Provider__c";
import RECORDTYPE_FIELD from "@salesforce/schema/Sanctions__c.RecordTypeId";

//Complaint Object Fields declaration
import SANCTION_FIELD from "@salesforce/schema/Case.Sanctions__c";
import COMPLAINTID_FIELD from "@salesforce/schema/Case.Id";

//Class name declaration in lwc
export default class ComplaintsSanctionLimitation extends LightningElement {
    //Fields declaration
    @track limitationDetails = {};
    @track licenseStartDateHelpText = '';
    @track licenseEndDateHelpText = '';
    @track minYouthEndDate = '';
    @track minPopulationEndDate = '';
    @track minProgramEndDate = '';
    @track minAge = 0;
    @track btnLabel = '';
    @track showDelivery = false;
    @track Spinner = true;

    //Disable fields using flags
    @track isNumberOfYouthServedDisable = false;
    @track isYouthEffectiveDateDisable = false;
    @track isYouthEndDateDisable = false;
    @track isYouthLimitationReasonDisable = false;

    @track isMinAgeDisable = false;
    @track isMaxAgeDisable = false;
    @track isGenderDisable = false;
    @track isPopulationLimitationReasonDisable = false;
    @track isPopulationEffectiveDateDisable = false;
    @track isPopulationEndDateDisable = false;

    @track isAssignedServiceDisable = false;
    @track isProgramEffectiveDateDisable = false;
    @track isProgramEndDateDisable = false;
    @track isProgramLimitationReasonDisable = false;
    @track isDocumentDisable = false;
    @track isDeliveryDisable = false;
    @track isSaveBtnDisable = false;

    //Picklist value Initialization
    @track documentOptions = [];
    @track deliveryOptions = [];

    //Local value declaration
    _recordTypeId;
    _licenseStartDate;
    _licenseEndDate;
    _actionName;
    _actionErrorName;
    error;

    //Global value declaration
    @api selectedSanctionId;
    @api sanctionActionName;
    @api sanctionPageType;

    //get Complaint ID
    get complaintId() {
        return sharedData.getCaseId();
    }

    //get Provider ID
    get providerId() {
        return sharedData.getProviderId();
    }

    //Declare sanction object into one variable
    @wire(getObjectInfo, { objectApiName: SANCTIONS_OBJECT })
    objectInfo;

    //Function used to get all picklist values from sanction object
    @wire(getPicklistValuesByRecordType, {
        objectApiName: SANCTIONS_OBJECT,
        recordTypeId: '$objectInfo.data.defaultRecordTypeId'
    })
    getAllPicklistValues({ error, data }) {
        if (data) {
            this.error = null;

            // Document Field Picklist values
            this.documentOptions = data.picklistFieldValues.Document__c.values.map(key => {
                return {
                    label: key.label,
                    value: key.value
                }
            });

            // Delivery Field Picklist values
            this.deliveryOptions = data.picklistFieldValues.Delivery__c.values.map(key => {
                return {
                    label: key.label,
                    value: key.value
                }
            });

            // Gender Picklist values
            this.genderOptions = data.picklistFieldValues.Gender__c.values.map(key => {
                return {
                    label: key.label,
                    value: key.value
                }
            });
        } else if (error) {
            this.error = JSON.stringify(error);
            this.dispatchEvent(utils.toastMessage("Picklist values are failed to load. please check", TOAST_HEADER_LABELS.TOAST_WARNING));
        }
    }

    //Connected Call Back method
    connectedCallback() {
        this.btnLabel = !this.selectedSanctionId ? BUTTON_LABELS.BTN_SAVE : BUTTON_LABELS.BTN_UPDATE;
        this.isYouthEndDateDisable = !this.limitationDetails.YouthEndDate__c ? true : false;
        this.isPopulationEndDateDisable = !this.limitationDetails.PopulationEndDate__c ? true : false;
        this.isProgramEndDateDisable = !this.limitationDetails.ProgramEndDate__c ? true : false;
        this.isMaxAgeDisable = !this.limitationDetails.MaxAge__c ? true : false;

        this.handleRecordType();
    }

    //Handle Record Type method
    handleRecordType() {
        //Fetch Record Type for Sanction Object
        getRecordType({
            name: this.sanctionPageType
        })
        .then(result => {
            if (!result) {
                this.Spinner = false;
                return this.dispatchEvent(utils.toastMessage("Sanction Record Type failed to load. please check", TOAST_HEADER_LABELS.TOAST_WARNING));
            }

            this._recordTypeId = result;
            this.handleComplaintLicenseDetails();
        })
        .catch(errors => {
            this.Spinner = false;
            return this.dispatchEvent(utils.handleError(errors));
        });
    }

    //Handle complaint license method
    handleComplaintLicenseDetails() {
        //Fetch Complaint License Details
        getComplaintLicenseDetails({
            complaintId: this.complaintId
        })
        .then(result => {
            if (result.length == 0) {
                this.Spinner = false;
                return this.dispatchEvent(utils.toastMessage("Complaint License details failed to load. please check", TOAST_HEADER_LABELS.TOAST_WARNING));
            }

            this._licenseStartDate = result[0].License__r ? result[0].License__r.StartDate__c : null;
            this._licenseEndDate = result[0].License__r ? result[0].License__r.EndDate__c : null;
            this.licenseStartDateHelpText = `License Start Date is ${utils.formatDate(this._licenseStartDate)}`;
            this.licenseEndDateHelpText = `License End Date is ${utils.formatDate(this._licenseEndDate)}`;

            if (!this.selectedSanctionId)
                this.Spinner = false;
            else
                this.handleLimitationData();
        })
        .catch(errors => {
            this.Spinner = false;
            return this.dispatchEvent(utils.handleError(errors));
        })
    }

    //Handle Limitation Data after ID generated
    handleLimitationData() {
        if ([DATATABLE_BUTTON_LABELS.BTN_EDIT, DATATABLE_BUTTON_LABELS.BTN_VIEW].includes(this.sanctionActionName)) {
            //Fetch selected sanction id data
            getSelectedSanctionData({
                sanctionId: this.selectedSanctionId
            })
            .then(result => {
                if (result.length == 0) {
                    this.Spinner = false;
                    return this.dispatchEvent(utils.toastMessage("Error in fetching suspension data. please check", TOAST_HEADER_LABELS.TOAST_WARNING));
                }

                try {
                    this.limitationDetails.Numberofyouthserved__c = result[0].Numberofyouthserved__c;
                    this.limitationDetails.YouthEffectiveDate__c = result[0].YouthEffectiveDate__c;
                    this.limitationDetails.YouthEndDate__c = result[0].YouthEndDate__c;
                    this.limitationDetails.YouthLimitationReason__c = result[0].YouthLimitationReason__c;
                    this.limitationDetails.MinAge__c = result[0].MinAge__c;
                    this.limitationDetails.MaxAge__c = result[0].MaxAge__c;
                    this.limitationDetails.Gender__c = result[0].Gender__c;
                    this.limitationDetails.PopulationLimitationReason__c = result[0].PopulationLimitationReason__c;
                    this.limitationDetails.PopulationEffectiveDate__c = result[0].PopulationEffectiveDate__c;
                    this.limitationDetails.PopulationEndDate__c = result[0].PopulationEndDate__c;
                    this.limitationDetails.ProviderAssignedService__c = result[0].ProviderAssignedService__c;
                    this.limitationDetails.ProgramEffectiveDate__c = result[0].ProgramEffectiveDate__c;
                    this.limitationDetails.ProgramEndDate__c = result[0].ProgramEndDate__c;
                    this.limitationDetails.ProgramLimitationReason__c = result[0].ProgramLimitationReason__c;
                    this.limitationDetails.Document__c = result[0].Document__c;
                    this.limitationDetails.Delivery__c = result[0].Delivery__c;

                    this.showDelivery = ['RCC Moratorium', 'RCC Notice Intent Revoke'].includes(result[0].Document__c) ? true : false;

                    this.handleFieldsDisable();
                } catch (error) {
                    this.error = JSON.stringify(error);
                    this.Spinner = false;
                }
            })
            .catch(errors => {
                this.Spinner = false;
                return this.dispatchEvent(utils.handleError(errors));
            });
        } else
            this.dispatchEvent(utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, TOAST_HEADER_LABELS.TOAST_WARNING));
    }

    //Handle fields disable method
    handleFieldsDisable() {
        try {
            if (this.sanctionActionName == DATATABLE_BUTTON_LABELS.BTN_VIEW) {
                this.isNumberOfYouthServedDisable = true;
                this.isYouthEffectiveDateDisable = true;
                this.isYouthEndDateDisable = true;
                this.isYouthLimitationReasonDisable = true;
                this.isMinAgeDisable = true;
                this.isMaxAgeDisable = true;
                this.isGenderDisable = true;
                this.isPopulationLimitationReasonDisable = true;
                this.isPopulationEffectiveDateDisable = true;
                this.isPopulationEndDateDisable = true;
                this.isAssignedServiceDisable = true;
                this.isProgramEffectiveDateDisable = true;
                this.isProgramEndDateDisable = true;
                this.isProgramLimitationReasonDisable = true;
                this.isDocumentDisable = true;
                this.isDeliveryDisable = true;
                this.isSaveBtnDisable = true;
            } else {
                this.isNumberOfYouthServedDisable = false;
                this.isYouthEffectiveDateDisable = false;
                this.isYouthEndDateDisable = !result[0].YouthEndDate__c ? true : false;
                this.isYouthLimitationReasonDisable = false;
                this.isMinAgeDisable = false;
                this.isMaxAgeDisable = !result[0].MaxAge__c ? true : false;
                this.isGenderDisable = false;
                this.isPopulationLimitationReasonDisable = false;
                this.isPopulationEffectiveDateDisable = false;
                this.isPopulationEndDateDisable = !result[0].PopulationEndDate__c ? true : false;
                this.isAssignedServiceDisable = false;
                this.isProgramEffectiveDateDisable = false;
                this.isProgramEndDateDisable = !result[0].ProgramEndDate__c ? true : false;
                this.isProgramLimitationReasonDisable = false;
                this.isDocumentDisable = false;
                this.isDeliveryDisable = false;
                this.isSaveBtnDisable = false;
            }
            this.Spinner = false;
        } catch(error) {
            this.error = JSON.stringify(error);
            this.Spinner = false;
        }
    }
    
    //Fields On Change method
    limitationOnChange(event) {
        switch(event.target.name) {
            case 'ServedYouth': 
                this.limitationDetails.Numberofyouthserved__c = event.target.value;
                break;
            case 'YouthEffectiveDate': 
                this.limitationDetails.YouthEffectiveDate__c = event.target.value;
                this.limitationDetails.YouthEndDate__c = null;
                this.minYouthEndDate = event.target.value;
                this.isYouthEndDateDisable = false;
                break;
            case 'YouthEndDate': 
                this.limitationDetails.YouthEndDate__c = event.target.value;
                break;
            case 'YouthLimitationReasons': 
                this.limitationDetails.YouthLimitationReason__c = event.target.value;
                break;
            case 'MinAge': 
                this.limitationDetails.MinAge__c = event.target.value;
                this.limitationDetails.MaxAge__c = null;
                this.minAge = event.target.value;
                this.isMaxAgeDisable = false;
                break;
            case 'MaxAge': 
                this.limitationDetails.MaxAge__c = event.target.value;
                break;
            case 'Gender': 
                this.limitationDetails.Gender__c = event.detail.value;
                break;
            case 'PopulationLimitationReasons': 
                this.limitationDetails.PopulationLimitationReason__c = event.target.value;
                break;
            case 'PopulationEffectiveDate': 
                this.limitationDetails.PopulationEffectiveDate__c = event.target.value;
                this.limitationDetails.PopulationEndDate__c = null;
                this.minPopulationEndDate = event.target.value;
                this.isPopulationEndDateDisable = false;
                break;
            case 'PopulationEndDate': 
                this.limitationDetails.PopulationEndDate__c = event.target.value;
                break;
            case 'AssignedServices': 
                this.limitationDetails.ProviderAssignedService__c = event.target.value;
                break;
            case 'ProgramEffectiveDate': 
                this.limitationDetails.ProgramEffectiveDate__c = event.target.value;
                this.limitationDetails.ProgramEndDate__c = null;
                this.minProgramEndDate = event.target.value;
                this.isProgramEndDateDisable = false;
                break;
            case 'ProgramEndDate': 
                this.limitationDetails.ProgramEndDate__c = event.target.value;
                break;
            case 'ProgramLimitationReasons': 
                this.limitationDetails.ProgramLimitationReason__c = event.target.value;
                break;
            case 'Document' : 
                this.limitationDetails.Document__c = event.detail.value;
                this.limitationDetails.Delivery__c = null;
                this.showDelivery = ['RCC Moratorium', 'RCC Notice Intent Revoke'].includes(event.detail.value) ? true : false;
                break;
            case this.showDelivery === true && 'Delivery' : 
                this.limitationDetails.Delivery__c = event.detail.value;
                break;
        }
    }

    //Handle Common Method for Save/Update sanction
    handleSaveMethod() {
        if (!this._recordTypeId)
            return this.dispatchEvent(utils.toastMessage("Sanction Record Type failed to load. please check", TOAST_HEADER_LABELS.TOAST_WARNING));

        if (!this._licenseStartDate || !this._licenseEndDate)
            return this.dispatchEvent(utils.toastMessage("Complaint License dates are failed to load. please check", TOAST_HEADER_LABELS.TOAST_WARNING));

        if (this.limitationDetails.Numberofyouthserved__c || this.limitationDetails.YouthEffectiveDate__c || this.limitationDetails.YouthEndDate__c || this.limitationDetails.YouthLimitationReason__c) {
            if (!(this.limitationDetails.Numberofyouthserved__c && this.limitationDetails.YouthEffectiveDate__c && this.limitationDetails.YouthEndDate__c && this.limitationDetails.YouthLimitationReason__c))
                return this.dispatchEvent(utils.toastMessage("Youth Limitation fields are mandatory", "warning"));

            if (this.limitationDetails.YouthEffectiveDate__c && (new Date(this.limitationDetails.YouthEffectiveDate__c).getTime() < new Date(this._licenseStartDate).getTime()))
                return this.dispatchEvent(utils.toastMessage("Youth effective date should be greater than License Start Date", TOAST_HEADER_LABELS.TOAST_WARNING));
    
            if (this.limitationDetails.YouthEndDate__c && new Date(this.limitationDetails.YouthEndDate__c).getTime() > new Date(this._licenseEndDate).getTime())
                return this.dispatchEvent(utils.toastMessage("Youth end date should be lesser than License End Date", TOAST_HEADER_LABELS.TOAST_WARNING));
    
            if (this.limitationDetails.YouthEffectiveDate__c && this.limitationDetails.YouthEndDate__c && (new Date(this.limitationDetails.YouthEffectiveDate__c).getTime() > new Date(this.limitationDetails.YouthEndDate__c).getTime()))
                return this.dispatchEvent(utils.toastMessage("Youth end date should be greater than or equal to Youth effective date", TOAST_HEADER_LABELS.TOAST_WARNING));
        }

        if (this.limitationDetails.MinAge__c || this.limitationDetails.MaxAge__c || this.limitationDetails.Gender__c || this.limitationDetails.PopulationLimitationReason__c || this.limitationDetails.PopulationEffectiveDate__c || this.limitationDetails.PopulationEndDate__c) {
            if (!(this.limitationDetails.MinAge__c && this.limitationDetails.MaxAge__c && this.limitationDetails.Gender__c && this.limitationDetails.PopulationLimitationReason__c && this.limitationDetails.PopulationEffectiveDate__c && this.limitationDetails.PopulationEndDate__c))
                return this.dispatchEvent(utils.toastMessage("Population Limitation fields are mandatory", TOAST_HEADER_LABELS.TOAST_WARNING));

            if (this.limitationDetails.MinAge__c > this.limitationDetails.MaxAge__c)
                return this.dispatchEvent(utils.toastMessage("Max Age should be greater than Min Age", "warning"));

            if (this.limitationDetails.PopulationEffectiveDate__c && (new Date(this.limitationDetails.PopulationEffectiveDate__c).getTime() < new Date(this._licenseStartDate).getTime()))
                return this.dispatchEvent(utils.toastMessage("Population effective date should be greater than License Start Date", TOAST_HEADER_LABELS.TOAST_WARNING));
    
            if (this.limitationDetails.PopulationEndDate__c && new Date(this.limitationDetails.PopulationEndDate__c).getTime() > new Date(this._licenseEndDate).getTime())
                return this.dispatchEvent(utils.toastMessage("Population end date should be lesser than License End Date", TOAST_HEADER_LABELS.TOAST_WARNING));
    
            if (this.limitationDetails.PopulationEffectiveDate__c && this.limitationDetails.PopulationEndDate__c && (new Date(this.limitationDetails.PopulationEffectiveDate__c).getTime() > new Date(this.limitationDetails.PopulationEndDate__c).getTime()))
                return this.dispatchEvent(utils.toastMessage("Population end date should be greater than or equal to Population effective date", TOAST_HEADER_LABELS.TOAST_WARNING));
        }

        if (this.limitationDetails.ProviderAssignedService__c || this.limitationDetails.ProgramEffectiveDate__c || this.limitationDetails.ProgramEndDate__c || this.limitationDetails.ProgramLimitationReason__c || this.limitationDetails.Document__c || this.limitationDetails.Delivery__c) {
            if (!(this.limitationDetails.ProviderAssignedService__c && this.limitationDetails.ProgramEffectiveDate__c && this.limitationDetails.ProgramEndDate__c && this.limitationDetails.ProgramLimitationReason__c && this.limitationDetails.Document__c && this.limitationDetails.Delivery__c))
                return this.dispatchEvent(utils.toastMessage("Program Services fields are mandatory", "warning"));

            if (this.limitationDetails.ProgramEffectiveDate__c && (new Date(this.limitationDetails.ProgramEffectiveDate__c).getTime() < new Date(this._licenseStartDate).getTime()))
                return this.dispatchEvent(utils.toastMessage("Program effective date should be greater than License Start Date", TOAST_HEADER_LABELS.TOAST_WARNING));
    
            if (this.limitationDetails.ProgramEndDate__c && new Date(this.limitationDetails.ProgramEndDate__c).getTime() > new Date(this._licenseEndDate).getTime())
                return this.dispatchEvent(utils.toastMessage("Program end date should be lesser than License End Date", TOAST_HEADER_LABELS.TOAST_WARNING));
    
            if (this.limitationDetails.ProgramEffectiveDate__c && this.limitationDetails.ProgramEndDate__c && (new Date(this.limitationDetails.ProgramEffectiveDate__c).getTime() > new Date(this.limitationDetails.ProgramEndDate__c).getTime()))
                return this.dispatchEvent(utils.toastMessage("Program end date should be greater than or equal to Program effective date", TOAST_HEADER_LABELS.TOAST_WARNING));
        }

        this.isSaveBtnDisable = true;
        this.Spinner = true;

        const fields = {};

        fields[YOUTHSERVED_FIELD.fieldApiName] = this.limitationDetails.Numberofyouthserved__c;
        fields[YOUTHEFFECTIVEDATE_FIELD.fieldApiName] = this.limitationDetails.YouthEffectiveDate__c;
        fields[YOUTHENDDATE_FIELD.fieldApiName] = this.limitationDetails.YouthEndDate__c;
        fields[YOUTHLIMITATIONREASON_FIELD.fieldApiName] = this.limitationDetails.YouthLimitationReason__c;
        fields[MINAGE_FIELD.fieldApiName] = this.limitationDetails.MinAge__c;
        fields[MAXAGE_FIELD.fieldApiName] = this.limitationDetails.MaxAge__c;
        fields[GENDER_FIELD.fieldApiName] = this.limitationDetails.Gender__c;
        fields[POPULATIONLIMITATIONREASON_FIELD.fieldApiName] = this.limitationDetails.PopulationLimitationReason__c;
        fields[POPULATIONEFFECTIVEDATE_FIELD.fieldApiName] = this.limitationDetails.PopulationEffectiveDate__c;
        fields[POPULATIONENDDATE_FIELD.fieldApiName] = this.limitationDetails.PopulationEndDate__c;
        fields[PROVIDERASSIGNEDSERVICE_FIELD.fieldApiName] = this.limitationDetails.ProviderAssignedService__c;
        fields[PROGRAMEFFECTIVEDATE_FIELD.fieldApiName] = this.limitationDetails.ProgramEffectiveDate__c;
        fields[PROGRAMENDDATE_FIELD.fieldApiName] = this.limitationDetails.ProgramEndDate__c;
        fields[PROGRAMLIMITATIONREASON_FIELD.fieldApiName] = this.limitationDetails.ProgramLimitationReason__c;
        fields[DOCUMENT_FIELD.fieldApiName] = this.limitationDetails.Document__c;
        fields[DELIVERY_FIELD.fieldApiName] = this.limitationDetails.Delivery__c;

        this._actionName = this.btnLabel == BUTTON_LABELS.BTN_SAVE ? 'saved' : 'updated';
        this._actionErrorName = this.btnLabel == BUTTON_LABELS.BTN_SAVE ? 'saving' : 'updating';

        if (!this.selectedSanctionId) {
            fields[COMPLAINT_FIELD.fieldApiName] = this.complaintId;
            fields[PROVIDER_FIELD.fieldApiName] = this.providerId;
            fields[RECORDTYPE_FIELD.fieldApiName] = this._recordTypeId;
            fields[LIMITATION_FIELD.fieldApiName] = true;

            const recordInput = {
                apiName: SANCTIONS_OBJECT.objectApiName,
                fields
            };

            createRecord(recordInput)
            .then(result => {
                this.selectedSanctionId = result.id;
                this.updateComplaintRecord();
            })
            .catch(error => {
                this.error = JSON.stringify(error);
                this.Spinner = false;
                this.isSaveBtnDisable = false;
                this.dispatchEvent(utils.toastMessage(`Error in ${this._actionErrorName} limitation sanction details. please check`, TOAST_HEADER_LABELS.TOAST_WARNING));
            })
        } else {
            fields[ID_FIELD.fieldApiName] = this.selectedSanctionId;

            const recordInput = {
                fields
            };

            updateRecord(recordInput)
            .then(() => {
                this.isSaveBtnDisable = false;
                this.limitationDetails = {};
                this.btnLabel = "";
                this.dispatchEvent(utils.toastMessage(`Limitation sanction has been ${this._actionName} successfully`, TOAST_HEADER_LABELS.TOAST_SUCCESS));
                this.handleRedirectionMethod();
            })
            .catch(error => {
                this.error = JSON.stringify(error);
                this.Spinner = false;
                this.isSaveBtnDisable = false;
                this.dispatchEvent(utils.toastMessage(`Error in ${this._actionErrorName} limitation sanction details. please check`, TOAST_HEADER_LABELS.TOAST_WARNING));
            })
        }
    }

    //Update Complaint record - sanction ID
    updateComplaintRecord() {
        const fields = {};
        fields[SANCTION_FIELD.fieldApiName] = this.selectedSanctionId;
        fields[COMPLAINTID_FIELD.fieldApiName] = this.complaintId;

        const complaintRecordInput = {
            fields
        };

        updateRecord(complaintRecordInput)
        .then(() => {
            this.dispatchEvent(utils.toastMessage(`Limitation sanction has been ${this._actionName} successfully`, TOAST_HEADER_LABELS.TOAST_SUCCESS));
            this.handleRedirectionMethod();
        })
        .catch(error => {
            this.error = JSON.stringify(error);
            this.Spinner = false;
            this.isSaveBtnDisable = false;
            this.dispatchEvent(utils.toastMessage(`Error in ${this._actionErrorName} suspension sanction details. please check`, TOAST_HEADER_LABELS.TOAST_WARNING));
        })
    }

    //Handle Cancel method
    handleCancelMethod() {
        this.Spinner = true;

        //1 Second Interval
        setTimeout(() => {
            this.handleRedirectionMethod();
        }, 1000);   
    }

    //Handle Sanction Redirection method
    handleRedirectionMethod() {
        this.Spinner = false;
        this.suspensionDetails = {};
        this.isSaveBtnDisable = false;

        const oncomplaintid = new CustomEvent('redirecttocomplaintsanctions', {
            detail: {
                first: true
            }
        });
        this.dispatchEvent(oncomplaintid);
    }
}