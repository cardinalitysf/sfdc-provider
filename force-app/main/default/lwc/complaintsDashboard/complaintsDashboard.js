/**
 * @Author        : Jayachandran
 * @CreatedOn     : Jun 29,2020
 * @Purpose       : Public / Private Complaint Dashboard With Card Clickable
 * @updatedBy     :
 * @updatedOn     :
 **/
import { LightningElement, track, wire } from "lwc";
//Import Apex
import getComplaintsCount from "@salesforce/apex/ComplaintsDashboard.getComplaintsCount";
import getComplaintsPrivateList from "@salesforce/apex/ComplaintsDashboard.getComplaintsPrivateList";
import getPublicComplaintList from "@salesforce/apex/ComplaintsDashboard.getPublicComplaintList";
//Utils
import * as sharedData from "c/sharedData";
import { DASHBORD_LABELS, USER_PROFILE_NAME } from "c/constants";
import { getRecord } from "lightning/uiRecordApi";
import USER_ID from "@salesforce/user/Id";
import PROFILE_FIELD from "@salesforce/schema/User.Profile.Name";
import { CJAMS_CONSTANTS } from "c/constants";
import { utils } from "c/utils";

const privateCardDatas = [
  {
    Id: "1",
    title: "Pending",
    key: "pending",
    count: 0
  },
  {
    Id: "2",
    title: "Approved",
    key: "approved",
    count: 0
  },
  {
    Id: "3",
    title: "Rejected",
    key: "rejected",
    count: 0
  },
  {
    Id: "4",
    title: "Total Complaints",
    key: "total",
    count: 0
  }
];

const publicCardDatas = [
  {
    Id: "1",
    title: "Pending",
    key: "pending",
    count: 0
  },
  {
    Id: "2",
    title: "Approved",
    key: "approved",
    count: 0
  },
  {
    Id: "3",
    title: "Rejected",
    key: "rejected",
    count: 0
  },
  {
    Id: "4",
    title: "Total Complaints",
    key: "total",
    count: 0
  }
];

const publicColumns = [
  {
    label: "Complaint Number",
    sortable: true,
    type: "button",
    typeAttributes: {
      label: { fieldName: "CaseNumber" },
      class: "blue",
      target: "_self",
      color: "blue",
      name: "Complaint"
    }
  },
  {
    label: "Provider Id(No of Complaints)",
    sortable: true,
    type: "button",
    typeAttributes: {
      label: { fieldName: "ProviderId__c" },
      class: "blue",
      target: "_self",
      color: "blue",
      name: "Provider"
    }
  },
  {
    label: "Applicant Name",
    fieldName: "ApplicantName",
    sortable: true,
    cellAttributes: { class: "fontclrGrey" }
  },
  {
    label: "SSN Number",
    fieldName: "SSN__c",
    sortable: true,
    cellAttributes: { class: "fontclrGrey" }
  },
  {
    label: "Received Date",
    fieldName: "ReceivedDate__c",
    cellAttributes: { class: "fontclrGrey" },
    type: "date",
    typeAttributes: {
      year: "numeric",
      month: "numeric",
      day: "numeric"
    }
  },
  {
    label: "Status",
    fieldName: "Status",
    type: "text",
    cellAttributes: { class: { fieldName: "statusClass" } }
  }
];

const privateColumns = [
  {
    label: "Complaint Number",
    sortable: true,
    type: "button",
    typeAttributes: {
      label: { fieldName: "CaseNumber" },
      class: "blue",
      target: "_self",
      color: "blue",
      name: "Complaint"
    }
  },
  {
    label: "Provider Id(No of Complaints)",
    sortable: true,
    type: "button",
    typeAttributes: {
      label: { fieldName: "ProviderId__c" },
      class: "blue",
      target: "_self",
      color: "blue",
      name: "Provider"
    }
  },
  {
    label: "Provider Name",
    fieldName: "Name",
    sortable: true,
    cellAttributes: { class: "fontclrGrey" }
  },
  {
    label: "Site Address",
    fieldName: "AddressType__c",
    sortable: true,
    cellAttributes: { class: "fontclrGrey" }
  },
  {
    label: "Received Date",
    fieldName: "ReceivedDate__c",
    cellAttributes: { class: "fontclrGrey" },
    type: "date",
    typeAttributes: {
      year: "numeric",
      month: "numeric",
      day: "numeric"
    }
  },
  {
    label: "Status",
    fieldName: "Status",
    type: "text",
    cellAttributes: { class: { fieldName: "statusClass" } }
  }
];

export default class ComplaintsDashboard extends LightningElement {
  @track publicCardDatas = publicCardDatas;
  @track privateCardDatas = privateCardDatas;
  @track publicColumns = publicColumns;
  @track privateColumns = privateColumns;
  @track totalRecords = [];
  @track activePage = 0;
  @track providerType = DASHBORD_LABELS.BTN_PUBLIC;
  @track searchKey = "";
  @track showPublicData = true;
  @track showPrivateData = false;
  @track PublicComplaintsData;
  @track PrivateComplaintsData;
  @track supervisorCaseHide = true;
  @track wholePubData;

  //User Details
  userProfileName;

  get cardClasses() {
    return [
      {
        title: "pending",
        card: "pending-cls",
        init: true
      },
      {
        title: "approved",
        card: "approved-cls",
        init: false
      },
      {
        title: "rejected",
        card: "rejected-cls",
        init: false
      },
      {
        title: "total",
        card: "total-cls",
        init: false
      }
    ];
  }

  //user to Fetch UserProfile details.
  @wire(getRecord, {
    recordId: USER_ID,
    fields: [PROFILE_FIELD]
  })
  wireuser({ error, data }) {
    if (error) {
      this.error = error;
    } else if (data) {
      this.userProfileName = data.fields.Profile.displayValue;
      sharedData.setUserProfileName(data.fields.Profile.displayValue);
      if (
        this.userProfileName === USER_PROFILE_NAME.USER_SUPERVISOR ||
        this.userProfileName === USER_PROFILE_NAME.USER_CASE_WRKR
      ) {
        this.supervisorCaseHide = false;
      }
    }
  }

  //Common Data Formating Function For Public and Private.
  formatData(data) {
    let currentData = [];
    if (this.providerType === DASHBORD_LABELS.BTN_PRIVATE)
      data.forEach((row) => {
        let rowData = {};
        rowData.CaseNumber = row.CaseNumber;
        rowData.statusClass = this.statusHandler(row.Status);
        rowData.Status = this.statusDisplayValues(row.Status);
        rowData.DataBaseStatus = row.Status;// db status to set for shareddata
        try {
          rowData.License =row.License__c;
        } catch (error) {
          
        }
        rowData.Id = row.Id;
        if (row.Account !== undefined) {
          let AccountCount = this.PrivateComplaintsData.filter(
            (i) => i.Account.Id == row.Account.Id
          );
          rowData.ProviderId__c =
            (row.Account.ProviderId__c !== undefined
              ? row.Account.ProviderId__c
              : "") +
            "  " +
            "(" +
            AccountCount.length +
            ")";
          rowData.providerId = row.Account.Id;
          rowData.Name = row.Account.Name !== undefined ? row.Account.Name : "";
          rowData.OwnerId =
            row.Account.OwnerId !== undefined ? row.Account.OwnerId : "";
        }
        rowData.ReceivedDate__c =
          row.ComplaintReceivedDate__c !== undefined ? row.ComplaintReceivedDate__c : "";
        rowData.AddressType__c =
          row.Address__r !== undefined
            ? (row.Address__r.AddressLine1__c !== undefined
              ? row.Address__r.AddressLine1__c
              : "") +
            " " +
            (row.Address__r.City__c !== undefined
              ? row.Address__r.City__c
              : "")
            : "";
        rowData.ApplicationId =
          row.License__r !== undefined ? row.License__r.Application__r.Id : "";
        currentData.push(rowData);
      });
    else
      data.forEach((row) => {
        let rowData = {};
        rowData.CaseNumber = row.CaseNumber;
        try {
          rowData.License =row.License__c;
        } catch (error) {
          
        }
        if (row.AccountId !== undefined && row.Account !== undefined) {
          let AccountCount = this.PublicComplaintsData.filter((i) => i.AccountId == row.AccountId);
          rowData.ProviderId__c = (row.Account.ProviderId__c !== undefined ? row.Account.ProviderId__c : "") + "  " + "(" + AccountCount.length + ")";
          rowData.OwnerId = row.Account.OwnerId !== undefined ? row.Account.OwnerId : "";
        }
        if (this.wholePubData.Actor !== undefined && this.wholePubData.Actor.length > 0) {
          let applicant = this.wholePubData.Actor.filter(data => data.Provider__c === row.AccountId)
          rowData.ApplicantName = applicant.length > 0 ? applicant[0].Contact__r.LastName : ' - ';
          rowData.SSN__c = applicant.length > 0 ? applicant[0].Contact__r.SSN__c : ' - ';
        } else {
          rowData.ApplicantName = ' - ';
          rowData.SSN__c = ' - ';
        }
        rowData.statusClass = this.statusHandler(row.Status);
        rowData.Status = this.statusDisplayValues(row.Status);
        rowData.DataBaseStatus = row.Status;// db status to set for shareddata
        rowData.Id = row.Id;
        rowData.ReceivedDate__c =
          row.ComplaintReceivedDate__c !== undefined ? row.ComplaintReceivedDate__c : "";
        rowData.providerId = row.AccountId !== undefined ? row.AccountId : "";
        rowData.ApplicationId =
          row.License__r !== undefined ? row.License__r.Application__r.Id : "";
        currentData.push(rowData);
      });
    return currentData;
  }
  //Status Values To be shown in FrontEnd Point.
  statusDisplayValues(status) {
    if (
      this.userProfileName === "Intake" ||
      this.userProfileName === "System Administrator"
    ) {
      switch (status) {
        case "Draft":
          return "New";
          break;
        case "Pending":
          return "New";
          break;
        case "Submitted to Caseworker":
          return "Submitted for Review";
          break;
        case "Submitted to Supervisor":
          return "Submitted for Review";
          break;
        case "Awaiting Confirmation":
          return "Submitted to Provider";
          break;
        case "Accepted":
          return "Accepted";
          break;
        case "Disputed":
          return "Disputed";
          break;
        case "Approved":
          return "Approved";
          break;
        case "Rejected":
          return "Rejected";
          break;
        default:
          return status;
      }
    } else if (this.userProfileName === "Caseworker") {
      switch (status) {
        case "Draft":
          return "New";
          break;
        case "Pending":
          return "New";
          break;
        case "Submitted to Caseworker":
          return "Pending";
          break;
        case "Submitted to Supervisor":
          return "Submitted for Review";
          break;
        case "Awaiting Confirmation":
          return "Submitted to Provider";
          break;
        case "Accepted":
          return "Accepted";
          break;
        case "Disputed":
          return "Disputed";
          break;
        case "Approved":
          return "Approved";
          break;
        case "Rejected":
          return "Rejected";
          break;
        default:
          return status;
      }
    } else if (this.userProfileName === "Supervisor") {
      switch (status) {
        case "Draft":
          return "New";
          break;
        case "Pending":
          return "New";
          break;
        case "Submitted to Caseworker":
          return "In Progress";
          break;
        case "Submitted to Supervisor":
          return "Awaiting Approval";
          break;
        case "Awaiting Confirmation":
          return "Submitted to Provider";
          break;
        case "Accepted":
          return "Accepted";
          break;
        case "Disputed":
          return "Disputed";
          break;
        case "Approved":
          return "Approved";
          break;
        case "Rejected":
          return "Rejected";
          break;
        default:
          return status;
      }
    }
  }
  //Class Change According to Colors From StyleSheet.css(Dashboard Data Table Status).
  statusHandler = (status) => {
    if (
      this.userProfileName === "Intake" ||
      this.userProfileName === "System Administrator"
    ) {
      switch (status) {
        case "Awaiting Confirmation":
        case "Draft":
        case "Pending":
          return "Pending";
          break;
        case "Accepted":
        case "Approved":
          return "Approved";
          break;
        case "Rejected":
        case "Disputed":
          return "Rejected";
          break;
        case "Submitted to Supervisor":
        case "Submitted to Caseworker":
          return "Scheduled";
          break;
        default:
          return status;
      }
    } else if (this.userProfileName === "Caseworker") {
      switch (status) {
        case "Awaiting Confirmation":
        case "Draft":
        case "Pending":
        case "Submitted to Caseworker":
          return "Pending";
          break;
        case "Accepted":
        case "Approved":
          return "Approved";
          break;
        case "Rejected":
        case "Disputed":
          return "Rejected";
          break;
        case "Submitted to Supervisor":
          return "Scheduled";
          break;
        default:
          return status;
      }
    } else if (this.userProfileName === "Supervisor") {
      switch (status) {
        case "Draft":
        case "Pending":
        case "Submitted to Caseworker":
          return "Pending";
          break;
        case "Accepted":
        case "Approved":
          return "Approved";
          break;
        case "Rejected":
        case "Disputed":
          return "Rejected";
          break;
        case "Submitted to Supervisor":
        case "Awaiting Confirmation":
          return "Scheduled";
          break;
        default:
          return status;
      }
    }
  };
  // Common Data Formating Function End.

  // Database Get For Public and Private.
  @wire(getComplaintsCount, {
    searchKey: "$searchKey",
    type: "$providerType",
    user: "$userProfileName"
  })
  wiredComplaintResult(result) {
    try {
      this.wiredComplaintCount = result;
      if (result.data) {
        let init = 0;
        let search = this.searchKey;
        let datas = result.data[0];
        if (this.providerType === DASHBORD_LABELS.BTN_PRIVATE)
          for (let key of this.privateCardDatas) {
            key.count = datas[key.key];
          }
        else
          for (let key of this.publicCardDatas) {
            key.count = datas[key.key];
          }
        this.getDataTableDatas(init, search);
      }
    } catch (error) {
      throw error;
    }
  }

  getDataTableDatas(id, search) {
    this.activePage = id;
    if (this.providerType === DASHBORD_LABELS.BTN_PRIVATE)
      getComplaintsPrivateList({
        searchKey: search,
        type: this.privateCardDatas[id].key,
        user: this.userProfileName
      })
        .then((data) => {
          this.PrivateComplaintsData = [...data];
       

          try {
            let currentData = [];
            if (data.length > 0) currentData = this.formatData(data);
            this.totalRecords = currentData;
            this.pageLoader = true;
          } catch (error) {
            throw console.error(error);
          }
        })
        .catch(errors => {
          let config = { friendlyErrorMsg: CJAMS_CONSTANTS.COMPLAINTS_PRIVATE_DATAEXCEPTION, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
          return this.dispatchEvent(utils.handleError(errors, config));
        });
    else
      getPublicComplaintList({
        searchKey: search,
        type: this.publicCardDatas[id].key,
        user: this.userProfileName
      })
        .then((result) => {
          this.wholePubData = JSON.parse(result);
          let data = this.wholePubData.Complaints;
          this.PublicComplaintsData = [...data];
          try {
            let currentData = [];
            if (data.length > 0) currentData = this.formatData(data);           
            this.totalRecords = currentData;
            this.pageLoader = true;
          } catch (error) {
            throw console.error(error);
          }
        })
        .catch(errors => {
          let config = { friendlyErrorMsg: CJAMS_CONSTANTS.COMPLAINTS_PUBLIC_DATAEXCEPTION, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
          return this.dispatchEvent(utils.handleError(errors, config));
        });
    this.template.querySelector("c-common-dashboard").searchFn();
  }

  handleDataTableDatas(event) {
    let search = this.searchKey;
    if (this.providerType == DASHBORD_LABELS.BTN_PUBLIC) {
      this.getDataTableDatas(event.detail, search);
    }
    this.getDataTableDatas(event.detail, search);
  }

  handleChange(event) {
    this.searchKey = event.target.value;
    this.getDataTableDatas(this.activePage, this.searchKey);
    this.template.querySelector("c-common-dashboard").searchFn();
  }

  // Redirection Function Complaint Number And Provider Id.
  handleRedirection(event) {
    sharedData.setCaseId(event.detail.redirectId);
    sharedData.setProviderId(event.detail.providerId);
    sharedData.setComplaintsStatus(event.detail.status);
    sharedData.setLicenseId(event.detail.LicenseId);
    sharedData.setPrivateOrPublicType(this.providerType);
    if (event.detail.first !== "Provider") {
      let CaseData = this.totalRecords.filter(
        (i) => i.Id == event.detail.redirectId
      )[0];
      let CaseId = CaseData.CaseNumber;
      let applicationId = CaseData.ApplicationId;
      let ownerId = CaseData.OwnerId;
      sharedData.setOwnerIdCommunityUser(ownerId);
      sharedData.setApplicationId(applicationId);
      const onClickId = new CustomEvent("redirecttocomplaintstabset", {
        detail: {
          first: false,
          CaseId: CaseId
        }
      });
      this.dispatchEvent(onClickId);
    } else {
      if (this.providerType === DASHBORD_LABELS.BTN_PUBLIC) {
        let CaseData = this.totalRecords.filter(
          (i) => i.Id == event.detail.redirectId
        )[0];
        let ProviderName = CaseData.ProviderId__c.split(" ")[0];
        const onClickId = new CustomEvent("redirecttoproviderpublictabs", {
          detail: {
            first: false,
            providerName: ProviderName
          }
        });
        this.dispatchEvent(onClickId);
      } else if (this.providerType === DASHBORD_LABELS.BTN_PRIVATE) {
        this.redirectionComplaintForProvidersPublicorPrivate(
          "redirecttoproviderprivatetabs"
        );
      }
    }
  }

  redirectionComplaintForProvidersPublicorPrivate(tabToRedirect) {
    const onClickId = new CustomEvent(tabToRedirect, {
      detail: {
        first: false
      }
    });
    this.dispatchEvent(onClickId);
  }
  //On Public / Private Toggle

  handleDashboardDatas(event) {
    this.providerType = event.detail;
    if (this.providerType === DASHBORD_LABELS.BTN_PUBLIC) {
      this.showPublicData = true;
      this.showPrivateData = false;
      let init = 0;
      let search = this.searchKey;
      this.getDataTableDatas(init, search);
    } else if (this.providerType === DASHBORD_LABELS.BTN_PRIVATE) {
      this.showPublicData = false;
      this.showPrivateData = true;
      let init = 0;
      let search = this.searchKey;
      this.getDataTableDatas(init, search);
    }
    this.template.querySelector("c-common-dashboard").searchFn();
  }

  /* New Complaint Redirection Start */
  redirectionNewComplaint() {
    sharedData.setCaseId(undefined);
    sharedData.setProviderId(undefined);
    sharedData.setPrivateOrPublicType(undefined);
    this.redirectionComplaintForProvidersPublicorPrivate(
      "redirecttocomplaintstabset"
    );
  }

  /*  Complaint Redirection End */
}
