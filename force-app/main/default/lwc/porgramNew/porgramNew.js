// Author : saranraj
// for program details

import { LightningElement, track, api, wire } from "lwc";
import { utils } from "c/referralUtils";
import setProgramAdress from "@salesforce/apex/ProgramAddress.setProgramAdress";
import getProgramAdress from "@salesforce/apex/ProgramAddress.getProgramAdress";
import { refreshApex } from "@salesforce/apex";
import getSearchAddress from "@salesforce/apex/ProgramAddress.getSearchAddress";
import getStateDetails from "@salesforce/apex/ProgramAddress.getStateDetails";
import * as SharedData from "c/sharedData";

// Record api
import { updateRecord } from "lightning/uiRecordApi";
import ADDRESS_LINE_1 from "@salesforce/schema/Address__c.AddressLine1__c";
import ADDRESS_LINE_2 from "@salesforce/schema/Address__c.AddressLine2__c";
import ID from "@salesforce/schema/Address__c.Id";
import ZIP_CODE from "@salesforce/schema/Address__c.ZipCode__c";
import ADDRESS_TYPE from "@salesforce/schema/Address__c.AddressType__c";
import STATE from "@salesforce/schema/Address__c.State__c";
import COUNTY from "@salesforce/schema/Address__c.County__c";
import CITY from "@salesforce/schema/Address__c.City__c";
import PHONE_NUMBER from "@salesforce/schema/Address__c.Phone__c";
import EMAIL from "@salesforce/schema/Address__c.Email__c";

//Sundar Modified Button Freeze
import getProviderApplicationStatus from "@salesforce/apex/ProgramAddress.getProviderApplicationStatus";
import { CJAMS_CONSTANTS } from "c/constants";
import { deleteRecord } from "lightning/uiRecordApi";
// for google api start
import getAPIStreetAddress from "@salesforce/apex/GetStreetAddressFromGoogleAPI.getSuggestions";
import getFullDetails from "@salesforce/apex/GetStreetAddressFromGoogleAPI.getPlaceDetails";

// for google api end

/*const actions = [

  { label: "edit", name: "edit" },
  { label: "delete", name: "delete" }
];*/

export default class PorgramNew extends LightningElement {
  @track queryTerm;
  @track responseData = [];
  @track searchAddress;
  @track editable = false;
  // individualProgramAdddressRecord;
  @track AddressLine1 = "";
  @track responseDataLength;
  @track AddressLine2 = "";
  @track AddressType = "";
  @track Email = "";
  @track PhoneNumber = "";
  @track searchString = "";
  @track State = "";
  @track Zipcode = "";
  @track County = "";
  @track City = "";
  @api Address__c;
  @track stateData;
  @track searchedProgramAddressData;
  @track showDropDownData = false;
  @track dataFromGoogleAPI = [];
  rowId;
  // waitUntillGetDataFormApexForModalOpen = false;
  @track deleteModalPopup = false;
  @track appid = this.ApplicationId;
  @track providerId = this.ProviderId;
  @track googleDropdown = false;
  @track deleteLoader = false;
  currentDeleteData;
  wiredAddressResult;
  @track Spinner = false;
  //Sundar Modified this
  @track freezeProgramAddressTable = false;
  @track searchDisable = false;
  editDataId;

  get ApplicationId() {
    return SharedData.getApplicationId();
  }

  get ProviderId() {
    return SharedData.getProviderId();
  }

  get options() {
    return [
      {
        label: "Address",
        fieldName: "AddressLine1__c"
      },
      {
        label: "Type",
        fieldName: "AddressType__c"
      },
      {
        label: "Email",
        fieldName: "Email__c"
      },
      {
        label: "Action",
        fieldName: "Id",
        type: "button",
        initialWidth: 120,
        typeAttributes: {
          iconName: "utility:edit",
          name: "Edit",
          title: "Edit",
          initialWidth: 20,
          disabled: false,
          iconPosition: "left",
          target: "_self"
        }
      },
      {
        type: "button",
        typeAttributes: {
          iconName: "utility:delete",
          name: "Delete",
          title: "Delete",
          initialWidth: 20,
          class: "del-red del-position",
          disabled: false,
          iconPosition: "left",
          target: "_self"
        }
      }
    ];
  }

  // Static county Values
  get county() {
    return [
      {
        label: "Adams",
        value: "Adams"
      },
      {
        label: "Allen",
        value: "Allen"
      },
      {
        label: "Bartholomew",
        value: "Bartholomew"
      },
      {
        label: "Benton",
        value: "Benton"
      },
      {
        label: "Blackford",
        value: "Blackford"
      },
      {
        label: "Boone",
        value: "Boone"
      },
      {
        label: "Brown",
        value: "Brown"
      },
      {
        label: "Carroll",
        value: "Carroll"
      },
      {
        label: "Cass",
        value: "Cass"
      },
      {
        label: "Clark",
        value: "Clark"
      },
      {
        label: "Clay",
        value: "Clay"
      },
      {
        label: "Clinton",
        value: "Clinton"
      },
      {
        label: "Crawford",
        value: "Crawford"
      },
      {
        label: "Daviess",
        value: "Daviess"
      },
      {
        label: "Dearborn",
        value: "Dearborn"
      },
      {
        label: "Decatur",
        value: "Decatur"
      },
      {
        label: "DeKalb",
        value: "DeKalb"
      },
      {
        label: "Delaware",
        value: "Delaware"
      },
      {
        label: "Dubois",
        value: "Dubois"
      },
      {
        label: "Elkhart",
        value: "Elkhart"
      },
      {
        label: "Fayette",
        value: "Fayette"
      },
      {
        label: "Floyd",
        value: "Floyd"
      },
      {
        label: "Fountain",
        value: "Fountain"
      },
      {
        label: "Franklin",
        value: "Franklin"
      },
      {
        label: "Fulton",
        value: "Fulton"
      },
      {
        label: "Gibson",
        value: "Gibson"
      },
      {
        label: "Grant",
        value: "Grant"
      },
      {
        label: "Greene",
        value: "Greene"
      },
      {
        label: "Hamilton",
        value: "Hamilton"
      },
      {
        label: "Hancock",
        value: "Hancock"
      },
      {
        label: "Harrison",
        value: "Harrison"
      },
      {
        label: "Hendricks",
        value: "Hendricks"
      },
      {
        label: "Henry",
        value: "Henry"
      },
      {
        label: "Howard",
        value: "Howard"
      },
      {
        label: "Huntington",
        value: "Huntington"
      },
      {
        label: "Jackson",
        value: "Jackson"
      },
      {
        label: "Jasper",
        value: "Jasper"
      },
      {
        label: "Jay",
        value: "Jay"
      },
      {
        label: "Jefferson",
        value: "Jefferson"
      },
      {
        label: "Jennings",
        value: "Jennings"
      },
      {
        label: "Johnson",
        value: "Johnson"
      },
      {
        label: "Knox",
        value: "Knox"
      },
      {
        label: "Kosciusko",
        value: "Kosciusko"
      },
      {
        label: "LaGrange",
        value: "LaGrange"
      },
      {
        label: "Lake",
        value: "Lake"
      },
      {
        label: "LaPorte",
        value: "LaPorte"
      },
      {
        label: "Lawrence",
        value: "Lawrence"
      },
      {
        label: "Madison",
        value: "Madison"
      },
      {
        label: "Marion",
        value: "Marion"
      },
      {
        label: "Marshall",
        value: "Marshall"
      },
      {
        label: "Martin",
        value: "Martin"
      },
      {
        label: "Miami",
        value: "Miami"
      },
      {
        label: "Monroe",
        value: "Monroe"
      },
      {
        label: "Montgomery",
        value: "Montgomery"
      },
      {
        label: "Morgan",
        value: "Morgan"
      },
      {
        label: "Newton",
        value: "Newton"
      },
      {
        label: "Noble",
        value: "Noble"
      },
      {
        label: "Ohio",
        value: "Ohio"
      },
      {
        label: "Orange",
        value: "Orange"
      },
      {
        label: "Owen",
        value: "Owen"
      },
      {
        label: "Parke",
        value: "Parke"
      },
      {
        label: "Perry",
        value: "Perry"
      },
      {
        label: "Pike",
        value: "Pike"
      },
      {
        label: "Porter",
        value: "Porter"
      },
      {
        label: "Posey",
        value: "Posey"
      },
      {
        label: "Pulaski",
        value: "Pulaski"
      },
      {
        label: "Putnam",
        value: "Putnam"
      },
      {
        label: "Randolph",
        value: "Randolph"
      },
      {
        label: "Ripley",
        value: "Ripley"
      },
      {
        label: "Rush",
        value: "Rush"
      },
      {
        label: "St. Joseph",
        value: "St. Joseph"
      },
      {
        label: "Scott",
        value: "Scott"
      },
      {
        label: "Shelby",
        value: "Shelby"
      },
      {
        label: "Spencer",
        value: "Spencer"
      },
      {
        label: "Starke",
        value: "Starke"
      },
      {
        label: "Steuben",
        value: "Steuben"
      },
      {
        label: "Sullivan",
        value: "Sullivan"
      },
      {
        label: "Switzerland",
        value: "Switzerland"
      },
      {
        label: "Tippecanoe County",
        value: "Tippecanoe County"
      },
      {
        label: "Tipton",
        value: "Tipton"
      },
      {
        label: "Union",
        value: "Union"
      },
      {
        label: "Vanderburgh",
        value: "Vanderburgh"
      },
      {
        label: "Vermillion",
        value: "Vermillion"
      },
      {
        label: "Lafayette",
        value: "Lafayette"
      },
      {
        label: "Vigo",
        value: "Vigo"
      },
      {
        label: "Wabash",
        value: "Wabash"
      },
      {
        label: "Warren",
        value: "Warren"
      },
      {
        label: "Warrick",
        value: "Warrick"
      },
      {
        label: "Washington",
        value: "Washington"
      },
      {
        label: "Wayne",
        value: "Wayne"
      },
      {
        label: "Wells",
        value: "Wells"
      },
      {
        label: "White",
        value: "White"
      },
      {
        label: "Whitley",
        value: "Whitley"
      }
    ];
  }

  get addressType() {
    return [
      {
        label: "None",
        value: "None"
      },
      {
        label: "Main",
        value: "Main"
      },
      {
        label: "Payment",
        value: "Payment"
      },
      {
        label: "Site",
        value: "Site"
      }
    ];
  }

  @wire(getProgramAdress, {
    appID: "$appid" //hardcoded as of now
  })
  wiredAddress(result) {
    this.wiredAddressResult = result;
    if (result.data) {
      this.responseData = result.data;
      this.responseDataLength = !!result.data.length;
      this.showDropDownData = false;
      this.searchString = "";

      //this line of code modified by Sundar K
      this.dispatchEvent(
        new CustomEvent("addresscount", {
          detail: {
            addressLength: this.responseDataLength
          }
        })
      );
    }
    if (result.error) {
    }
  }

  modalInputHandler(event) {
    if (event.target.name === "AddressType") {
      this.AddressType = event.detail.value;
    } else if (event.target.name === "AddressLine1") {
      let addressLine1 = event.target.value;
      getAPIStreetAddress({
        input: addressLine1
      })
        .then((data) => {
          this.dataFromGoogleAPI = JSON.parse(data);
          this.googleDropdown = this.dataFromGoogleAPI.predictions.length > 0;
        })
        .catch((err) => {});
    } else if (event.target.name === "AddressLine2") {
      this.AddressLine2 = event.target.value;
    } else if (event.target.name === "Email") {
      this.Email = event.target.value;
    } else if (event.target.name === "PhoneNumber") {
      event.target.value = event.target.value.replace(/(\D+)/g, "");
      this.PhoneNumber = utils.formattedPhoneNumber(event.target.value);
    } else if (event.target.name === "State") {
      this.State = event.target.value;
    } else if (event.target.name === "Zipcode") {
      if (event.target.value.length > 5) {
        this.dispatchEvent(utils.toastMessage("Invalid Zipcode", "warning"));
      } else {
        this.Zipcode = event.target.value;
      }
    } else if (event.target.name === "County") {
      this.County = event.target.value;
    } else if (event.target.name === "City") {
      this.City = event.target.value;
    }
  }

  handleSelectedAddress = (event) => {
    this.addressLine1 = event.target.dataset.description;

    getFullDetails({
      placeId: event.target.dataset.id
    }).then((result) => {
      const main = JSON.parse(result);
      this.AddressLine1 =
        main.result.address_components[0].long_name +
        " " +
        main.result.address_components[1].long_name;
      this.City = main.result.address_components[2].long_name;
      this.County = main.result.address_components[4].long_name;
      this.State = main.result.address_components[5].long_name;
      this.Zipcode = main.result.address_components[7].long_name;
      this.googleDropdown = false;
    });
  };

  emailValidation = (email) => {
    let emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (email === "") {
      return true;
    } else if (!emailRegex.test(String(email).toLowerCase())) {
      this.editable = true;
      this.dispatchEvent(utils.toastMessage("Invalid Email!", "warning"));
      return false;
    } else {
      this.Email = email;
      return true;
    }
  };

  connectedCallback() {
    this.getStateDetailsHandler();
    this.getApplicationStatus();
  }

  getStateDetailsHandler() {
    getStateDetails().then((stateResponse) => {
      let stateValue = stateResponse.map((item) => {
        return Object.assign(
          {},
          {
            label: item.RefValue__c,
            value: item.RefValue__c
          }
        );
      });
      this.stateData = stateValue;
    });
  }

  inputHandler(evt) {
    this.queryTerm = evt.target.value;
    this.searchString = evt.target.value;
    getSearchAddress({
      searchQuery: this.queryTerm,
      providerId: this.providerId,
      applicationId: this.ApplicationId
    })
      .then((data) => {
        this.searchedProgramAddressData = data;
        this.showDropDownData = true;
      })
      .catch((err) => {
        if (err) {
          let error = JSON.parse(err.body.message);
          const { title, message, errorType } = error;
          this.dispatchEvent(
            utils.toastMessageWithTitle(title, message, errorType)
          );
        } else {
          this.dispatchEvent(
            utils.toastMessage(
              CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
              CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
            )
          );
        }
      });
  }

  handleSearchProgramAddress = (event) => {
    this.Spinner = true;
    this.showDropDownData = false;
    let recordId = event.currentTarget.dataset.id;
    let objAddress = {
      sobjectType: "Address__c"
    };
    objAddress.Application__c = this.ApplicationId;
    objAddress.Id = recordId;
    setProgramAdress({
      objSobjecttoUpdateOrInsert: objAddress
    })
      .then((response) => {
        this.Spinner = false;
        return refreshApex(this.wiredAddressResult);
      })
      .catch((error) => {
        this.dispatchEvent(utils.toastMessage(error, "warning"));
      });
  };

  handleCloseModal() {
    this.editable = false;
  }

  closeDeleteModalHandler() {
    this.deleteModalPopup = false;
  }

  openDeleteModalHandler = () => {
    this.deleteModalPopup = true;
    this.deleteRow();
  };

  getApplicationStatus() {
    getProviderApplicationStatus({
      applicationId: this.ApplicationId
    }).then((result) => {
      if (result.length > 0) {
        if (
          result[0].Status__c ==
            CJAMS_CONSTANTS.PROVIDER_APP_FINAL_STATUS_REVIEW ||
          result[0].Status__c ==
            CJAMS_CONSTANTS.PROVIDER_APP_FINAL_STATUS_APPEAL ||
          result[0].Status__c ==
            CJAMS_CONSTANTS.APPLICATION_FINAL_STATUS_REVIEW ||
          result[0].Status__c ==
            CJAMS_CONSTANTS.PROVIDER_APP_FINAL_STATUS_REJECTED ||
          result[0].Status__c ==
            CJAMS_CONSTANTS.APPLICATION_FINAL_STATUS_APPROVED ||
          result[0].Status__c ==
            CJAMS_CONSTANTS.APPLICATION_FINAL_STATUS_SUPERVISOR_REJECTED
        ) {
          this.freezeProgramAddressTable = true;
          this.searchDisable = true;
        } else {
          this.freezeProgramAddressTable = false;
          this.searchDisable = false;
        }
      }
    });
  }

  handleRowAction(event) {
    const actionName = event.detail.action.name;
    const row = event.detail.row;

    if (this.freezeProgramAddressTable && actionName === "Edit")
      return this.dispatchEvent(
        utils.toastMessage(
          "Cannot edit program address for submitted provider application",
          "warning"
        )
      );

    if (this.freezeProgramAddressTable && actionName === "Delete")
      return this.dispatchEvent(
        utils.toastMessage(
          "Cannot delete program address for submitted provider application",
          "warning"
        )
      );

    switch (actionName) {
      case "Delete":
        this.deleteRow(row);
        break;
      case "Edit":
        this.showRowDetails(row);
        this.rowId = row["Id"];
        break;
      default:
    }
  }

  deleteRow(row) {
    this.deleteModalPopup = true;
    this.currentDeleteData = row["Id"];
  }

  deleteRowHandler = (event) => {
    this.deleteLoader = true;
    let recordId = this.currentDeleteData;
    deleteRecord(recordId)
      .then((item) => {
        this.deleteLoader = false;
        this.dispatchEvent(utils.toastMessage(item, "success"));
        this.deleteModalPopup = false;
        return refreshApex(this.wiredAddressResult);
      })
      .catch((error) => {
        this.deleteLoader = false;
        this.dispatchEvent(utils.toastMessage(error, "error"));
      });
  };

  showRowDetails = (row) => {
    this.editable = true;

    // this.individualProgramAdddressRecord = item[0];
    this.AddressLine1 = row.AddressLine1__c;
    this.editDataId = row.Id;
    this.Zipcode = row.ZipCode__c;
    this.AddressType = row.AddressType__c;
    this.State = row.State__c;
    this.County = row.County__c;
    this.City = row.City__c;
    this.PhoneNumber = row.Phone__c ? row.Phone__c : this.PhoneNumber;
    this.Email = row.Email__c ? row.Email__c : "";
  };

  saveModalFormData = () => {
    if (
      this.AddressLine1 === "" ||
      this.AddressType === "" ||
      this.State === "" ||
      this.City === "" ||
      this.Zipcode === "" ||
      this.County === ""
    ) {
      this.dispatchEvent(
        utils.toastMessage("Please Fill All Mandatory Fields", "warning")
      );
      this.editable = true;
    } else if (!this.emailValidation(this.Email)) {
      return ""
    } else {
      const fields = {};
      fields[ID.fieldApiName] = this.editDataId;
      fields[ADDRESS_LINE_1.fieldApiName] = this.AddressLine1;
      fields[ADDRESS_LINE_2.fieldApiName] = this.AddressLine2;
      fields[ADDRESS_TYPE.fieldApiName] = this.AddressType;
      fields[ZIP_CODE.fieldApiName] = this.Zipcode;
      fields[CITY.fieldApiName] = this.City;
      fields[STATE.fieldApiName] = this.State;
      fields[COUNTY.fieldApiName] = this.County;
      fields[EMAIL.fieldApiName] = this.Email;
      fields[PHONE_NUMBER.fieldApiName] = this.PhoneNumber && this.PhoneNumber;

      const recordInput = { fields };
      updateRecord(recordInput).then(() => {
        this.dispatchEvent(
          utils.toastMessage("Successfully Updated", "success")
        );
        return refreshApex(this.wiredAddressResult);
      });
      this.editable = false;
    }
  };

  // showRowDetails(row) {
  //     setTimeout(() => {
  //         this.editable = true;
  //     }, 2000);
  //     getProgramAdressRowId({
  //         Id: row["Id"]
  //     }).then(item => {
  //         this.individualProgramAdddressRecord = item[0];
  //         this.AddressLine1 = item[0].AddressLine1__c;
  //         this.Zipcode = item[0].ZipCode__c;
  //         this.AddressType = item[0].AddressType__c;
  //         this.State = item[0].State__c;
  //         this.County = item[0].County__c;
  //         this.City = item[0].City__c;
  //         this.PhoneNumber = item[0].Phone__c ? item[0].Phone__c : this.PhoneNumber;
  //         this.Email = item[0].Email__c ? item[0].Email__c : "";
  //     });
  // }

  //   saveModalFormData() {
  //     let addrFromJS = {
  //       sobjectType: "Address__c"
  //     };

  //     if (
  //       this.AddressLine1 === "" ||
  //       this.AddressType === "" ||
  //       this.State === "" ||
  //       this.City === "" ||
  //       this.Zipcode === "" ||
  //       this.County === ""
  //     ) {
  //       this.dispatchEvent(
  //         utils.toastMessage("Please Fill All Mandatory Fields", "warning")
  //       );
  //       this.editable = true;
  //     } else if (!this.emailValidation(this.Email)) {
  //       // this.editable = true;
  //       return;
  //     } else {
  //       addrFromJS.AddressLine1__c = this.AddressLine1;
  //       addrFromJS.AddressLine2__c = this.AddressLine2;
  //       addrFromJS.AddressType__c = this.AddressType;
  //       addrFromJS.City__c = this.City;
  //       addrFromJS.State__c = this.State;
  //       addrFromJS.County__c = this.County;
  //       addrFromJS.Phone__c =
  //         this.PhoneNumber === undefined ? "" : this.PhoneNumber;
  //       addrFromJS.ZipCode__c = this.Zipcode;
  //       addrFromJS.Email__c = this.Email;
  //       addrFromJS.Id = this.rowId;

  //       UpdatingModalProgramAddressData({
  //         modalUpdatedData: addrFromJS
  //       }).then((data) => {
  //         this.dispatchEvent(utils.toastMessage(data, "success"));
  //         return refreshApex(this.wiredAddressResult);
  //       });
  //       this.editable = false;
  //     }
  //   }
}
