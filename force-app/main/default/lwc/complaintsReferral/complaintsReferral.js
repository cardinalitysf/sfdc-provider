/**
 * @Author        : Preethi Bojarajan
 * @CreatedOn     : July 06, 2020
 * @Purpose       : New Complaints Registering Model Functions
 * @updatedBy     : 
 * @updatedOn     : 
 **/
import {
    LightningElement,
    track,
    wire
} from 'lwc';

//Utils
import * as sharedData from 'c/sharedData';
import {
    utils
} from "c/utils";
import {
    constPopupVariables,
    CJAMS_OBJECT_QUERIES,
    CJAMS_CONSTANTS,
    DASHBORD_LABELS,
    USER_PROFILE_NAME
} from 'c/constants';

//Wired uiApi
import {
    createRecord,
    updateRecord,
    deleteRecord
} from "lightning/uiRecordApi";
import {
    refreshApex
} from "@salesforce/apex";
import ACTOR_OBJECT from '@salesforce/schema/Actor__c';
import ACTORDET_OBJECT from '@salesforce/schema/ActorDetails__c';
import ADDRESS_OBJECT from '@salesforce/schema/Address__c';
import CONTACT_OBJECT from '@salesforce/schema/Contact';

//Import Apex
import getProvidersList from "@salesforce/apex/ComplaintsReferral.getProvidersList";
import getIdAfterInsertSOQL from "@salesforce/apex/ComplaintsReferral.getIdAfterInsertSOQL";
import getCaseHeaderDetails from "@salesforce/apex/ComplaintsReferral.getCaseHeaderDetails";
import getMultiplePicklistValues from '@salesforce/apex/ComplaintsReferral.getMultiplePicklistValues';
import getRecordType from '@salesforce/apex/ComplaintsReferral.getRecordType';
import fetchUserId from '@salesforce/apex/ComplaintsReferral.fetchUserId'

//Google Api Apex
import getFullDetails from "@salesforce/apex/GetStreetAddressFromGoogleAPI.getPlaceDetails";
import getAPIStreetAddress from "@salesforce/apex/GetStreetAddressFromGoogleAPI.getSuggestions";
import getAllStates from "@salesforce/apex/ComplaintsReferral.getStateDetails";

const columns = [{
    label: 'SITE ADDRESS',
    fieldName: 'Address',
    type: 'button',
    typeAttributes: {
        name: 'dontRedirect',
        label: {
            fieldName: 'Address'
        }
    },
    cellAttributes: {
        class: "title"
    }
}, {
    label: 'PROGRAM NAME',
    fieldName: 'ProgramName',
    type: 'text'
}, {
    label: 'PROGRAM TYPE',
    fieldName: 'ProgrameType',
    type: 'button',
    typeAttributes: {
        name: 'dontRedirect',
        label: {
            fieldName: 'ProgrameType'
        }
    },
    cellAttributes: {
        class: "title"
    }
}];

const publicColumns = [{
    label: 'PROVIDER ID',
    fieldName: 'ProviderId__c',
    type: 'text'
}, {
    label: 'APPLICANT NAME',
    fieldName: 'applicant',
    type: 'text'
}, {
    label: 'CO-APPLICANT NAME',
    fieldName: 'coApplicant',
    type: 'text'
}, {
    label: 'SSN NUMBER',
    fieldName: 'SSN__c',
    type: 'text'
}, {
    label: 'STATUS',
    fieldName: 'Status__c',
    type: 'text',
    cellAttributes: {
        class: {
            fieldName: 'statusClass'
        }
    }
}];

export default class ComplaintsReferral extends LightningElement {
    @track noIconShow = true;
    @track showSearchProvider = false;
    @track searchModel = {
        providerType: null,
        providerId: '',
        providerName: ''
    };
    @track providersListsData = [];
    @track Spinner = true;
    @track showTable = false;
    @track providersWholeData;
    @track privateProviderDatas;
    @track showPrivateData = true;
    @track privateProviderColumns = columns;
    @track preSelectedRows = [];
    @track selectedProvider = [];
    @track providerId;
    @track providerSiteId;
    @track publicColumns = publicColumns;
    //Provider Receiver Datas
    @track isDisableActionBtn = true;
    @track isDisabled = false;
    @track compRefPrivTable = [];
    @track compRefPubTable = [];
    @track providerRecord = [];
    @track complaints = {};
    @track newCaseId;
    @track recordTypeId = null;
    @track reporterRecordTypeId = null;
    @track showReportDetails = true;
    //Piclist Tracks
    @track communicationValues;
    @track sourceOfInfValues;
    @track compSourceValues;
    currentDate = utils.formatDateYYYYMMDD(new Date());

    //recSave Tracks
    @track reporterDet = {};
    @track addressOfReporter = {};
    @track saveOrUpdate = 'Save';
    @track caseHeaderData;

    //googleApi Tracks
    active = false;
    @track selectedCounty;
    @track strStreetAddress;
    @track strStreetAddresPrediction = [];
    @track filteredCountyArr = [];
    @track selectedState;
    @track selectedCity;
    @track selectedZipCode;
    @track selectedCounty;
    @track ShippingStreet;
    @track showStateBox = false;
    @track stateFetchResult = [];
    @track stateSearchTxt;
    @track filteredStateArr = [];
    @track showCountyBox = false;
    @track countySearchTxt;
    @track filteredCountyArr = [];
    @track countyArr = [];
    @track addressInformationId = null;
    @track editMode = false;

    /* Provider Record Tracks and Tables  */

    getRowActions(row, doneCallback) {
        const actions = [];
        actions.push({
            'label': 'Edit',
            'iconName': 'action:edit',
            'name': 'editRecord'
        });
        // simulate a trip to the server
        setTimeout(() => {
            doneCallback(actions);
        }, 200);
    }

    constructor() {
        super();
        this.compRefPrivTable = [{
            label: 'PROVIDER ID',
            fieldName: 'ProviderId__c',
            type: 'text'
        }, {
            label: 'PROVIDER NAME',
            fieldName: 'Name',
            type: 'text'
        }, {
            label: 'PROVIDER TYPE',
            fieldName: 'ProviderType__c',
            type: 'text'
        }, {
            label: 'PROGRAM',
            fieldName: 'ProgramName__c',
            type: 'text'
        }, {
            label: 'PROGRAM TYPE',
            fieldName: 'ProgramType__c',
            type: 'button',
            typeAttributes: {
                name: 'dontRedirect',
                label: {
                    fieldName: 'ProgramType__c'
                }
            },
            cellAttributes: {
                class: "title"
            }
        }, {
            label: 'SITE ADDRESS',
            fieldName: 'SiteAddress',
            type: 'button',
            typeAttributes: {
                name: 'dontRedirect',
                label: {
                    fieldName: 'SiteAddress'
                }
            },
            cellAttributes: {
                class: "title"
            }
        }, {
            type: 'action',
            fieldName: 'id',
            typeAttributes: {
                rowActions: this.getRowActions
            },
            cellAttributes: {
                iconName: 'utility:threedots_vertical',
                iconAlternativeText: 'Actions',
                class: "tripledots blueIcons"
            }
        }];

        this.compRefPubTable = [{
            label: 'PROVIDER ID',
            fieldName: 'ProviderId__c',
            type: 'text'
        }, {
            label: 'PROGRAM NAME',
            fieldName: 'ProgramName__c',
            type: 'text'
        }, {
            label: 'APPLICANT NAME',
            fieldName: 'applicant',
            type: 'text'
        }, {
            label: 'CO-APPLICANT NAME',
            fieldName: 'coApplicant',
            type: 'text'
        }, {
            label: 'SSN NUMBER',
            fieldName: 'SSN__c',
            type: 'text'
        }, {
            label: 'STATUS',
            fieldName: 'Status__c',
            type: 'text',
            cellAttributes: {
                class: {
                    fieldName: 'statusClass'
                }
            }
        }, {
            type: 'action',
            fieldName: 'id',
            typeAttributes: {
                rowActions: this.getRowActions
            },
            cellAttributes: {
                iconName: 'utility:threedots_vertical',
                iconAlternativeText: 'Actions',
                class: "tripledots blueIcons"
            }
        }];
        if (this.userProfileName === USER_PROFILE_NAME.USER_SUPERVISOR) {
            this.compRefPrivTable.pop();
            this.compRefPubTable.pop();
        }
        let statusValues = ["Submitted to supervisor",
            "Accepted",
            "Disputed",
            "Approved",
            "Rejected",
            "Awaiting Confirmation"
        ];
        if (this.userProfileName !== USER_PROFILE_NAME.USER_SUPERVISOR)
            this.isDisabled = statusValues.includes(this.complaintsStatus) ? true : false;
        else
            this.isDisabled = this.userProfileName === USER_PROFILE_NAME.USER_SUPERVISOR ? true : false;
    }

    get caseId() {
        return sharedData.getCaseId();
    }

    get userProfileName() {
        return sharedData.getUserProfileName();
    }

    get complaintsStatus() {
        return sharedData.getComplaintsStatus();
    }
    fetchRefCompDetails() {
        let model = 'Case';
        model = this.editMode === true ? 'Address__c' : model;
        getCaseHeaderDetails({
            newreferral: this.newCaseId,
            model
        }).then(result => {
            this.caseHeaderData = [...result];
            if (result[0].Status !== 'New' && result[0].ComplaintSource__c !== 'Unknown') {
                let data = Object.assign({}, result[0].Actor__r.Referral__r);
                delete data.Account;
                this.complaints = data;
            } else {
                this.complaints = Object.assign({}, result[0]);
                delete this.complaints.Account;
            }
            const onClickId = new CustomEvent('casenumbersetinheader', {
                detail: {
                    CaseNumber: this.complaints.CaseNumber
                }
            });
            this.dispatchEvent(onClickId);
            this.Spinner = false;
            if (this.caseId !== undefined && this.editMode === true) {
                this.noIconShow = false;
                this.showReportDetails = this.complaints.ComplaintSource__c === 'Unknown' ? false : true;
                let Account = {};
                if (this.showReportDetails === true) {
                    Account = Object.assign({}, result[0].Actor__r.Referral__r.Account);
                    this.showPrivateData = Account.ProviderType__c === DASHBORD_LABELS.BTN_PUBLIC ? false : true;
                    this.reporterDet = Object.assign({}, result[0].Actor__r.Contact__r);
                    let addressOfReprtr = Object.assign({}, result[0]);
                    delete addressOfReprtr.Actor__r;
                    this.addressOfReporter = addressOfReprtr;
                } else {
                    Account = Object.assign({}, result[0].Account);
                    this.showPrivateData = Account.ProviderType__c === DASHBORD_LABELS.BTN_PUBLIC ? false : true;
                }
                if (this.showPrivateData === false) {
                    this.searchModel.providerType = DASHBORD_LABELS.BTN_PUBLIC;
                    this.searchModel.providerId = Account.ProviderId__c;
                    this.searchModel.providerName = '';
                    this.providerId = Account.Id;
                } else {
                    this.searchModel.providerType = DASHBORD_LABELS.BTN_PRIVATE;
                    this.searchModel.providerId = Account.ProviderId__c;
                    this.searchModel.providerName = '';
                    this.providerId = Account.Id;
                    this.providerSiteId = this.complaints.Address__c;
                }
                this.fetchProviderDet();
            }
        }).catch((errors) => {
            if (errors) {
                let error = JSON.parse(errors.body.message);
                const {
                    title,
                    message,
                    errorType
                } = error;
                this.dispatchEvent(
                    utils.toastMessageWithTitle(title, message, errorType)
                );
            } else {
                this.dispatchEvent(
                    utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                );
            }
        });
    }

    @wire(getRecordType, {
        name: 'Reporter',
        model: 'Contact'
    })
    reporterRecordTypeDetails(result) {
        try {
            if (result.data) {
                this.reporterRecordTypeId = result.data;
            } else if (result.error) {
                let errors = result.error;
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const {
                        title,
                        message,
                        errorType
                    } = error;
                    this.dispatchEvent(utils.toastMessageWithTitle(title, message, errorType));
                } else {
                    this.dispatchEvent(utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE));
                }
            }
        } catch (error) {
            throw error;
        }
    }

    @wire(getRecordType, {
        name: 'Complaints',
        model: 'Case'
    })
    recordTypeDetails(data) {
        try {
            if (data.data) {
                this.recordTypeId = data.data;
                if (!this.caseId) {
                    let myobjcase = {
                        'sobjectType': 'Case'
                    };
                    myobjcase.Status = 'New';
                    myobjcase.RecordTypeId = this.recordTypeId;
                    getIdAfterInsertSOQL({
                        objIdSobjecttoUpdateOrInsert: myobjcase
                    })
                        .then(result => {
                            sharedData.setCaseId(result);
                            this.newCaseId = result;
                            this.fetchRefCompDetails();
                        }).catch((errors) => {
                            if (errors) {
                                let error = JSON.parse(errors.body.message);
                                const {
                                    title,
                                    message,
                                    errorType
                                } = error;
                                this.dispatchEvent(utils.toastMessageWithTitle(title, message, errorType));
                            } else {
                                this.dispatchEvent(utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE));
                            }
                        });
                    this.editMode = false;
                } else {
                    this.newCaseId = this.caseId;
                    this.editMode = true;
                    this.saveOrUpdate = 'Update';
                    this.fetchRefCompDetails();
                }
            }
        } catch (error) {
            throw error;
        }
    }

    /* ----- Pop-Up Function And Field Function Start ----- */
    openSearchProvider() {
        this.showSearchProvider = true;
    }

    closeSearchProvider() {
        this.showSearchProvider = false;
        if (this.providerRecord.length === 0) {
            this.searchModel = {
                providerType: '',
                providerId: '',
                providerName: ''
            };
            this.providersListsData = [];
            this.showTable = false;
        }
    }

    get providerTypeValues() {
        return [{
            label: DASHBORD_LABELS.BTN_PUBLIC,
            value: DASHBORD_LABELS.BTN_PUBLIC
        }, {
            label: DASHBORD_LABELS.BTN_PRIVATE,
            value: DASHBORD_LABELS.BTN_PRIVATE
        }]
    }

    onChangeHandlersFrSrch(event) {
        switch (event.target.name) {
            case 'providerType':
                this.searchModel.providerType = event.target.value;
                break;
            case 'providerId':
                this.searchModel.providerId = event.target.value;
                break;
            case 'providerName':
                this.searchModel.providerName = event.target.value;
                break;
        }
    }

    fetchProviderDet() {
        this.Spinner = true;
        getProvidersList({
            providerType: this.searchModel.providerType,
            providerId: this.searchModel.providerId,
            name: this.searchModel.providerName
        }).then((data) => {
            try {
                if (data.length > 0) {
                    this.providersWholeData = [...data];
                    this.showTable = true;
                    let currentData = [];
                    if (this.searchModel.providerType === DASHBORD_LABELS.BTN_PRIVATE) {
                        data.forEach(row => {
                            let rowData = {};
                            rowData.rowId = row.Id;
                            rowData.ProviderId__c = row.ProviderId__c;
                            rowData.Name = row.Name;
                            rowData.Email = row.Email__c !== undefined ? row.Email__c : '';
                            rowData.ProviderType__c = row.ProviderType__c !== undefined ? row.ProviderType__c : DASHBORD_LABELS.BTN_PRIVATE;
                            rowData.Status__c = row.Status__c;
                            rowData.statusClass = row.Status__c;
                            rowData.applicant = row.ProviderType__c !== undefined ? row.ProviderType__c : DASHBORD_LABELS.BTN_PRIVATE;
                            rowData.coApplicant = row.TaxId__c !== undefined ? row.TaxId__c : '';
                            currentData.push(rowData);
                        });
                        this.providersListsData = currentData;
                        this.showPrivateData = true;
                    } else {
                        data.forEach(row => {
                            let rowData = {};
                            rowData.rowId = row.Id;
                            rowData.ProviderId__c = row.ProviderId__c;
                            rowData.Name = row.Name;
                            rowData.Email = row.Email__c !== undefined ? row.Email__c : '';
                            rowData.ProviderType__c = row.ProviderType__c !== undefined ? row.ProviderType__c : DASHBORD_LABELS.BTN_PUBLIC;
                            rowData.Status__c = row.Status__c;
                            rowData.statusClass = row.Status__c;
                            if (row.Actors__r !== undefined && row.Actors__r.length > 0) {
                                let applicant = row.Actors__r.filter(actor => actor.Role__c === 'Applicant');
                                let coApplicant = row.Actors__r.filter(actor => actor.Role__c === 'Co-Applicant');
                                rowData.applicant = applicant.length > 0 ? applicant[0].Contact__r.LastName : '';
                                rowData.coApplicant = coApplicant.length > 0 ? coApplicant[0].Contact__r.LastName : '';
                                rowData.SSN__c = (applicant.length > 0 && applicant[0].Contact__r.SSN__c !== undefined) ? applicant[0].Contact__r.SSN__c : '';
                            } else {
                                rowData.applicant = '';
                                rowData.coApplicant = '';
                                rowData.SSN__c = '';
                            }
                            currentData.push(rowData);
                        });
                        this.providersListsData = currentData;
                        this.showPrivateData = false;
                    }
                    if (this.editMode === true) {
                        this.providerSiteId = this.complaints.Address__c;
                        this.addProviderToComp();
                    }
                    this.Spinner = false;
                } else {
                    this.Spinner = false;
                    this.showTable = false;
                    this.dispatchEvent(utils.toastMessage(constPopupVariables.PROVIDER_DET_INCORRECT, "warning"));
                }
                this.isDisableActionBtn = true;
            } catch (error) {
                throw error;
            }
        }).catch((errors) => {
            if (errors) {
                let error = JSON.parse(errors.body.message);
                const {
                    title,
                    message,
                    errorType
                } = error;
                this.dispatchEvent(utils.toastMessageWithTitle(title, message, errorType));
            } else {
                this.dispatchEvent(utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE));
            }
        });
    }

    toggleRowContinuation(event) {
        if (this.providerRecord.length === 0) {
            this.preSelectedRows = [];
            this.providerId = null;
            this.providerSiteId = null;
        }
        let conId = event.target !== undefined ? event.target.dataset.id : event;
        let conList = this.template.querySelectorAll('.conli');
        let btnList = this.template.querySelectorAll('.check');

        btnList.forEach((item) => {
            item.className = (item.dataset.id == conId && item.className !== 'btn check showLess') ? "btn check showLess" : "btn check dropdown";
        });
        conList.forEach((item) => {
            item.className = (item.dataset.id == conId && item.className !== 'conli slds-showd') ? 'conli slds-showd' : 'conli slds-hide';
        });

        let providerDatas = this.providersWholeData.filter(data => data.Id === conId)[0];
        this.privateProviderDatas = [];
        if (providerDatas.License__r !== undefined && providerDatas.License__r.length > 0) {
            let currentData = [];
            let Id = providerDatas.Id;
            providerDatas.License__r.forEach(row => {
                let rowData = {};
                rowData.Id = Id;
                rowData.addressId = row.Address__c;
                rowData.Address = row.Address__r.AddressLine1__c;
                rowData.ProgramName = row.ProgramName__c;
                rowData.ProgrameType = row.ProgramType__c;
                currentData.push(rowData);
            });
            this.privateProviderDatas = currentData;
            if (providerDatas.License__r.length === 1) {
                this.isDisableActionBtn = false;
                this.providerId = this.privateProviderDatas[0].Id;
                this.providerSiteId = this.privateProviderDatas[0].addressId;
                this.preSelectedRows.push(this.providerSiteId);
            } else {
                this.isDisableActionBtn = true;
                if (this.providerRecord.length === 0) {
                    this.providerId = null;
                    this.providerSiteId = null;
                    this.preSelectedRows = [];
                }
            }
        }
    }


    handleRowSelection = event => {
        this.isDisableActionBtn = false;
        if (event.detail.selectedRows !== undefined && event.detail.selectedRows.length > 0) {
            this.providerId = event.detail.selectedRows[0].Id;
            this.providerSiteId = event.detail.selectedRows[0].addressId;
        } else {
            if (event.target.dataset !== undefined && event.target.dataset.id !== undefined)
                this.toggleRowContinuation(event);
        }
    }

    handleRowSelectionPub = event => {
        this.providerId = event.detail.selectedRows[0].rowId;
        this.isDisableActionBtn = false;
    }

    handleRowAction(event) {
        if (event.detail.action.name != undefined && event.detail.action.name === 'dontRedirect') return;
        const action = event.detail.action;
        const row = event.detail.row;
        switch (action.name) {
            case 'editRecord':
                this.isDisabled = false;
                this.showSearchProvider = true;
                setTimeout(() =>
                    this.toggleRowContinuation(row.Id), 0);
                break;
        }
    }
    /* ----- Pop-Up Function And Field Function End ----- */

    /* ----Provider Add Compliant Start */

    addProviderToComp() {
        if (this.providerId == null) return;
        this.providerRecord = [];
        let providerDet = this.providersWholeData.filter(data => data.Id === this.providerId)[0];
        if (this.showPrivateData === true) {
            let rowData = {};
            rowData.Id = providerDet.Id;
            rowData.ProviderId__c = providerDet.ProviderId__c;
            rowData.Name = providerDet.Name;
            rowData.ProviderType__c = providerDet.ProviderType__c;
            if (this.providerSiteId !== null && this.providerSiteId !== undefined) {
                let licenseDet = providerDet.License__r.filter(data => data.Address__c === this.providerSiteId)[0];
                rowData.ProgramName__c = licenseDet.ProgramName__c;
                rowData.ProgramType__c = licenseDet.ProgramType__c;
                rowData.SiteAddress = licenseDet.Address__r.AddressLine1__c;
                this.preSelectedRows.push(this.providerSiteId);
            }
            this.providerRecord.push(rowData);
        } else {
            let rowData = {};
            rowData.Id = providerDet.Id;
            rowData.ProviderId__c = providerDet.ProviderId__c;
            let row = providerDet;
            if (row.Actors__r !== undefined && row.Actors__r.length > 0) {
                let applicant = row.Actors__r.filter(actor => actor.Role__c === 'Applicant');
                let coApplicant = row.Actors__r.filter(actor => actor.Role__c === 'Co-Applicant');
                rowData.applicant = applicant.length > 0 ? applicant[0].Contact__r.LastName : ' - ';
                rowData.coApplicant = coApplicant.length > 0 ? coApplicant[0].Contact__r.LastName : ' - ';
                rowData.SSN__c = applicant[0].Contact__r.SSN__c !== undefined ? applicant[0].Contact__r.SSN__c : ' - ';
                rowData.ProgramName__c = (row.Actors__r[0].Referral__r !== undefined && row.Actors__r[0].Referral__r.Program__c !== undefined) ? row.Actors__r[0].Referral__r.Program__c : ' - ';
            }
            rowData.Status__c = row.Status__c;
            rowData.statusClass = row.Status__c;
            this.providerRecord.push(rowData);
            this.selectedProvider.push(this.providerId);
        }
        this.showSearchProvider = this.providerRecord.length < 1 ? true : false;
        this.noIconShow = this.providerRecord.length === 1 ? false : true;
    }

    get allPickListFields() {
        return CJAMS_OBJECT_QUERIES.FETCH_FIELDS_FOR_COMPLAINTS_REFERRAL_PICKLIST;
    }

    @wire(getMultiplePicklistValues, {
        objInfo: 'Case',
        picklistFieldApi: '$allPickListFields'
    })
    wiredTitle(data) {
        try {
            if (data.data != undefined && data.data !== '') {
                this.communicationValues = data.data.Origin;
                this.sourceOfInfValues = data.data.SourceofInformation__c;
                this.compSourceValues = data.data.ComplaintSource__c;
            } else if (data.error) {
                let errors = data.error;
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const {
                        title,
                        message,
                        errorType
                    } = error;
                    this.dispatchEvent(utils.toastMessageWithTitle(title, message, errorType));
                } else {
                    this.dispatchEvent(utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE));
                }
            }
        } catch (error) {
            throw error;
        }
    }

    onChangeHandlers(event) {
        switch (event.target.name) {
            case 'commincationValues':
                this.complaints.Origin = event.target.value;
                break;
            case 'sourceOfInf':
                this.complaints.SourceofInformation__c = event.target.value;
                break;
            case 'DOC':
                this.complaints.ComplaintReceivedDate__c = event.target.value;
                break;
            case 'complaintSource':
                this.complaints.ComplaintSource__c = event.target.value;
                if (this.complaints.ComplaintSource__c === 'Unknown')
                    this.showReportDetails = false;
                else this.showReportDetails = true;
                break;
            case 'address1':
                this.getStreetAddress(event);
                break;
            case 'address2':
                this.addressOfReporter.AddressLine2__c = event.target.value;
                break;
            case 'city':
                this.addressOfReporter.City__c = event.target.value;
                break;
            case 'state':
                this.addressOfReporter.State__c = event.target.value;
                this.stateOnChange(event);
                break;
            case 'county':
                this.addressOfReporter.County__c = event.target.value;
                this.countyOnChange(event);
                break;
            case 'zipcode':
                this.addressOfReporter.ZipCode__c = event.target.value;
                break;
            case 'firstName':
                this.reporterDet.FirstName__c = event.target.value;
                break;
            case 'lastName':
                this.reporterDet.LastName__c = event.target.value;
                break;
            case 'mobileNumber':
                event.target.value = event.target.value.replace(/(\D+)/g, "");
                this.addressOfReporter.Phone__c = utils.formattedPhoneNumber(event.target.value);
                break;
            case 'email':
                this.addressOfReporter.Email__c = event.target.value;
                break;
            case 'incidentDescription':
                this.reporterDet.Description = event.target.value;
                break;
        }
    }

    /* Save or Update Function Start */
    getUserId() {
        let providerDet = this.providersWholeData.filter(data => data.Id === this.providerId)[0];
        fetchUserId({
            emailId: providerDet.Email__c
        })
            .then(data => {
                if (data !== undefined && data.length > 0)
                    return data[0].Id;
                else if (this.showPrivateData === false) return '';
                else {
                    this.Spinner = false;
                    return this.dispatchEvent(utils.toastMessage(constPopupVariables.PROVIDER_DET_INCORRECT, "warning"));
                }
            }).catch((errors) => {
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const {
                        title,
                        message,
                        errorType
                    } = error;
                    this.dispatchEvent(utils.toastMessageWithTitle(title, message, errorType));
                } else {
                    this.dispatchEvent(utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE));
                }
            });
    }

    validationBfSaveOrUpdate() {
        const email = [...this.template.querySelectorAll(".email")].reduce((validSoFar, inputCmp) => {
            inputCmp.reportValidity();
            return validSoFar && inputCmp.checkValidity();
        }, true);
        if (!email)
            return this.dispatchEvent(utils.toastMessage('Please Enter Valid Email Id..!', "warning"));

        const allValid = [
            ...this.template.querySelectorAll("lightning-input"),
            ...this.template.querySelectorAll("lightning-combobox"),
            ...this.template.querySelectorAll("lightning-textarea")
        ].reduce((validSoFar, inputCmp) => {
            inputCmp.reportValidity();
            return validSoFar && inputCmp.checkValidity();
        }, true);
        if (!allValid)
            return this.dispatchEvent(utils.toastMessage(constPopupVariables.VALIDATION_WARN, "warning"));
        this.saveOrUpdateCompReferral();
    }

    saveOrUpdateCompReferral() {
        this.Spinner = true;
        this.complaints.AccountId = this.providerId;
        sharedData.setProviderId(this.providerId);
        let type = this.showPrivateData === true ? DASHBORD_LABELS.BTN_PRIVATE : DASHBORD_LABELS.BTN_PUBLIC;
        sharedData.setPrivateOrPublicType(type);
        let fields = Object.assign({}, this.complaints);
        fields.Id = this.newCaseId;
        fields.Status = 'Pending';
        fields.Provider__c = this.getUserId();
        if (this.providerSiteId !== null && this.providerSiteId !== undefined) {
            let providerDet = this.providersWholeData.filter(data => data.Id === this.providerId)[0];
            let licenseDet = providerDet.License__r.filter(data => data.Address__r.Id === this.providerSiteId)[0];
            fields.License__c = licenseDet.Id;
            fields.Address__c = licenseDet.Address__r !== undefined ? licenseDet.Address__r.Id : '';
            fields.Program__c = licenseDet.ProgramName__c;
            fields.ProgramType__c = licenseDet.ProgramType__c;
        } else {
            let providerDet = this.providersWholeData.filter(data => data.Id === this.providerId)[0];
            if (providerDet.License__r !== undefined && providerDet.License__r.length > 0 && providerDet.License__r[0].Address__r !== undefined) {
                fields.License__c = providerDet.License__r[0].Id;
                fields.Address__c = providerDet.License__r[0].Address__r.Id;
                fields.Program__c = providerDet.License__r[0].ProgramName__c;
                fields.ProgramType__c = providerDet.License__r[0].ProgramType__c;
            } else {
                this.Spinner = false;
                return this.dispatchEvent(utils.toastMessage('Provider Address Is Missing, Please Check It..!', "warning"));
            }
        }
        delete fields.CaseNumber;
        const recordInput = {
            fields
        };
        updateRecord(recordInput)
            .then(result => {
                try {
                    if (result.id && this.complaints.ComplaintSource__c !== 'Unknown') {
                        if (this.reporterDet.Id === undefined && this.addressOfReporter.Id === undefined)
                            this.reporterDetailsCreate();
                        else this.reporterDetailsUpdate();
                    } else {
                        this.dispatchEvent(utils.toastMessage(constPopupVariables.COMPLAINT_REF_SUCCESS, "success"));
                        this.Spinner = false;
                    }
                } catch (error) {
                    throw error;
                }
            });
    }

    reporterDetailsCreate() {
        let fields = this.reporterDet;
        fields.LastName = this.reporterDet.FirstName__c + ' ' + this.reporterDet.LastName__c;
        fields.RecordTypeId = this.reporterRecordTypeId;
        const recordInput = {
            apiName: CONTACT_OBJECT.objectApiName,
            fields
        };
        createRecord(recordInput)
            .then(result => {
                try {
                    if (result.id) {
                        let fields = {};
                        fields.Referral__c = this.newCaseId;
                        fields.Contact__c = result.id;
                        const recordInput = {
                            apiName: ACTOR_OBJECT.objectApiName,
                            fields
                        };
                        createRecord(recordInput)
                            .then(result => {
                                try {
                                    if (result.id) {
                                        let fields = this.addressOfReporter;
                                        fields.Actor__c = result.id;
                                        fields.AddressType__c = 'Complaints'
                                        const recordInput = {
                                            apiName: ADDRESS_OBJECT.objectApiName,
                                            fields
                                        };
                                        createRecord(recordInput)
                                            .then(result => {
                                                this.isDisableActionBtn = false;
                                                if (this.editMode === false)
                                                    this.dispatchEvent(utils.toastMessage(constPopupVariables.COMPLAINT_REF_SUCCESS, "success"));
                                                else
                                                    this.dispatchEvent(utils.toastMessage(constPopupVariables.COMPLAINT_REF_UPDATE, "success"));
                                                this.Spinner = false;
                                            })
                                    }
                                } catch (error) {
                                    throw error;
                                }
                            });
                    }
                } catch (error) {
                    throw error;
                }
            });
    }

    reporterDetailsUpdate() {
        let fields = this.reporterDet;
        const recordInput = {
            fields
        };
        updateRecord(recordInput)
            .then(result => {
                try {
                    if (result.id) {
                        let fields = this.addressOfReporter;
                        const recordInput = {
                            fields
                        };
                        updateRecord(recordInput)
                            .then(result => {
                                this.isDisableActionBtn = false;
                                this.dispatchEvent(utils.toastMessage(constPopupVariables.COMPLAINT_REF_UPDATE, "success"));
                                this.Spinner = false;
                            })
                    }
                } catch (error) {
                    throw error;
                }
            });
    }
    /* Save or Update Function End */

    //Google api code starts from here

    get streetAddressLength() {
        return this.strStreetAddresPrediction != "";
    }
    get strStreetAddresPredictionUI() {
        return this.strStreetAddresPrediction;
    }
    get strStreetAddresPredictionLength() {
        if (this.addressOfReporter.AddressLine1__c)
            return this.strStreetAddresPrediction.predictions ? true : false;
    }

    //Method used to show and hide the drop-down of suggestions
    get tabClass() {
        return this.active ?
            "slds-dropdown slds-dropdown_length-5 slds-dropdown_fluid slds-show" :
            "slds-dropdown slds-dropdown_length-5 slds-dropdown_fluid slds-hide";
    }


    //Function used to fetch all states from Address Object when Screen Loads
    @wire(getAllStates)
    getAllStates({
        error,
        data
    }) {
        if (data) {
            if (data.length > 0) {
                let stateData = data.map(row => {
                    return {
                        Id: row.Id,
                        Value: row.RefValue__c
                    };
                });
                this.stateFetchResult = stateData;
                // this.countyInitialLoad();
            } else {
                this.dispatchEvent(utils.toastMessage("No states found", "error"));
            }
        } else if (error) {
            this.dispatchEvent(
                utils.toastMessage("Error in Fetching states", "error")
            );
        }
    }

    //Function used to set selected state
    setSelectedCounty(event) {
        this.addressOfReporter.County__c = event.currentTarget.dataset.value;
        this.showCountyBox = false;
    }

    //Function used to set selected state
    setSelectedState(event) {
        this.addressOfReporter.State__c = event.currentTarget.dataset.value;
        this.showStateBox = false;
    }

    //Function used to capture on change county in an array
    countyOnChange(event) {
        let countySearchTxt = event.target.value;

        if (!countySearchTxt) this.showCountyBox = false;
        else {
            this.filteredCountyArr = this.countyArr
                .filter(val => val.value.indexOf(countySearchTxt) > -1)
                .map(cresult => {
                    return cresult;
                });
            this.showCountyBox = this.filteredCountyArr.length > 0 ? true : false;
        }
    }

    getStreetAddress(event) {
        this.addressOfReporter.AddressLine1__c = event.target.value;
        if (!event.target.value) {
            this.addressOfReporter.County__c = "";
            this.addressOfReporter.State__c = "";
            this.addressOfReporter.City__c = "";
            this.addressOfReporter.ZipCode__c = "";
        } else {
            getAPIStreetAddress({
                input: this.addressOfReporter.AddressLine1__c
            })
                .then(result => {
                    this.strStreetAddresPrediction = JSON.parse(result);
                    this.active =
                        this.strStreetAddresPrediction.predictions.length > 0 ?
                            true :
                            false;
                })
                .catch(error => {
                    this.dispatchEvent(
                        utils.toastMessage(
                            "Error in Fetching Google API Street Address",
                            "error"
                        )
                    );
                });
        }
    }

    //Function used to select the address
    selectStreetAddress(event) {
        this.addressOfReporter.AddressLine1__c = event.target.dataset.description;

        getFullDetails({
            placeId: event.target.dataset.id
        })
            .then(result => {
                var main = JSON.parse(result);
                try {
                    this.addressOfReporter.City__c = main.result.address_components[2].long_name;
                    this.addressOfReporter.County__c = main.result.address_components[4].long_name;
                    this.addressOfReporter.State__c = main.result.address_components[5].long_name;
                    this.addressOfReporter.ZipCode__c = main.result.address_components[7].long_name;
                } catch (ex) { }
                this.active = false;
            })
            .error(error => {
                this.dispatchEvent(
                    utils.toastMessage("Error in Selected Address", "error")
                );
            });
    }

    //Function used for capturing on change states in an array
    stateOnChange(event) {
        let stateSearchTxt = event.target.value;

        if (!stateSearchTxt) this.showStateBox = false;
        else {
            this.filteredStateArr = this.stateFetchResult
                .filter(val => val.Value.indexOf(stateSearchTxt) > -1)
                .map(sresult => {
                    return sresult;
                });
            this.showStateBox = this.filteredStateArr.length > 0 ? true : false;
        }
    }

    //ends here

    //Redirect Back To Dashboard Start
    redirectToComplaintsRefDashboard() {
        const onClickId = new CustomEvent('redirecttocomplaintsrefdashboard', {
            detail: {
                first: false
            }
        });
        this.dispatchEvent(onClickId);
    }
    //Redirect Back To Dashboard End
}