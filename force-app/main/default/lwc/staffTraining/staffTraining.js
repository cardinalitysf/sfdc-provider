import {
    LightningElement,
    track,
    wire
} from 'lwc';
import fatchPickListValue from '@salesforce/apex/referralProviderHeaderController.getPickListValues';
import saveCaseRecord from '@salesforce/apex/StaffTraining.updateOrInsertSOQL';
import gettrainingDetails from '@salesforce/apex/StaffTraining.gettrainingDetails';
import deletetrainingDetails from '@salesforce/apex/StaffTraining.deletetrainingDetails';
import edittrainingDetails from '@salesforce/apex/StaffTraining.edittrainingDetails';
import {
    utils
} from "c/utils";
import * as sharedData from 'c/sharedData';
import getRecordType from "@salesforce/apex/StaffTraining.getRecordType";
import { CJAMS_CONSTANTS } from "c/constants";


const columns = [{
    label: 'Training Name',
    fieldName: 'TrainingName__c',
    type: 'text',
    initialWidth: 300
},
{
    label: 'No of Hours',
    fieldName: 'TrainingHours__c',
    type: 'number'
},
{
    label: 'Date',
    fieldName: 'CompletionDate__c',
    type: 'date',
    editable: false,
    typeAttributes: {
        month: '2-digit',
        day: '2-digit',
        year: 'numeric'
    }
},

{
    label: '',
    fieldName: 'Id',
    initialWidth: 70,
    type: "button",
    typeAttributes: {
        iconName: 'utility:edit',
        name: 'Edit',
        title: 'Edit',
        initialWidth: 20,
        disabled: false,
        iconPosition: 'left',
        target: '_self'
    }
},
{
    initialWidth: 5,
    type: "button",
    typeAttributes: {
        iconName: 'utility:delete',
        name: 'Delete',
        title: 'Delete',
        initialWidth: 20,
        class: "del-red del-position",
        disabled: false,
        iconPosition: 'left',
        target: '_self'
    }
}
];


export default class StaffTraining extends LightningElement {
    @track data = [];
    @track columns = columns;
    @track currentPagetrainingData;
    @track openmodel = false;
    @track openmodel1 = false;
    @track value;
    @track norecorddisplay = true;
    @track StaffTrainingNameValues;
    @track Training;
    @track Training1;
    @track Training2;
    @track stid;
    @track totalRecordsCount = 0;
    @track page = 1;
    perpage = 4;
    setPagination = 5;
    @track totalRecords;
    @track RecordTypeId;

    //Sundar Adding this 
    @track title;
    @track btnLabel;
    @track isUpdateFlag = false;
    @track isBtnDisabled = false;

    openmodal() {
        this.openmodel = true
        this.title = "ADD TRAINING DETAILS";
        this.btnLabel = "SAVE";
        this.isBtnDisabled = true;
        this.Training = "";
        this.Training1 = "";
        this.Training2 = "";
        this.stid = null;
    }

    openmodal1() {
        this.openmodel = true
    }
    closeModal() {
        this.openmodel = false
    }
    closeModal1() {
        this.openmodel1 = false
    }


    // get Html values to js using name 

    handleChange(event) {
        this.isBtnDisabled = false;

        if (event.target.name == 'TrainingName') {
            this.Training2 = event.detail.value;
        } else if (event.target.name == 'TrainingHours') {
            this.Training = event.target.value;
        } else if (event.target.name == 'CompletionDate') {
            this.Training1 = event.target.value;
        }
    }

    //Get data fetching using ConnectedCallback

    connectedCallback() {
        this.training();
    }

    training() {
        gettrainingDetails({
            trainingdetails: this.ContactId
        }).then(result => {
            //this.currentPagetrainingData = result;
            this.totalRecords = result;
            this.totalRecordsCount = this.totalRecords.length;

            if (this.totalRecords.length == 0) {
                this.norecorddisplay = false;
            } else {
                this.norecorddisplay = true;
            }
            this.pageData();

        }).catch((errors) => {
            return this.dispatchEvent(utils.handleError(errors));
        });
    }


    //picklist values fetch from Object
    @wire(fatchPickListValue, {
        objInfo: {
            'sobjectType': 'Training__c'
        },
        picklistFieldApi: 'TrainingName__c'
    })
    wireduserDetails1({
        error,
        data
    }) {
        /*eslint-disable*/

        if (data) {
            this.StaffTrainingNameValues = data;
        } else if (error) {

        }
    }


    //Save Functionality
    handleSave() {
        if (!this.Training2)
            return this.dispatchEvent(utils.toastMessage("Training Name is mandatory", "warning"));

        if (!this.Training1)
            return this.dispatchEvent(utils.toastMessage("Completion Date is mandatory", "warning"));

        this.isBtnDisabled = true;

        let myobjcase = {
            'sobjectType': 'Training__c'
        };
        myobjcase.TrainingHours__c = this.Training;
        myobjcase.TrainingName__c = this.Training2;
        myobjcase.CompletionDate__c = this.Training1;
        myobjcase.Staff__c = this.ContactId;
        myobjcase.Id = this.stid;
        myobjcase.RecordTypeId = this.RecordTypeId;
        if (this.stid)
            this.isUpdateFlag = true;
        else
            this.isUpdateFlag = false;

        saveCaseRecord({
            objSobjecttoUpdateOrInsert: myobjcase
        })
            .then(result => {

                if (this.isUpdateFlag)
                    this.dispatchEvent(utils.toastMessage("Training Details are updated successfully", "success"));
                else
                    this.dispatchEvent(utils.toastMessage("Training Details are added successfully", "success"));

                this.openmodel = false;
                this.isBtnDisabled = false;
                this.training();
            })
            .catch((errors) => {
                return this.dispatchEvent(utils.handleError(errors));
            });
    }

    //Edit  Functinality

    handleRowAction(event) {
        if (event.detail.action.name == "Edit") {
            let trainingid = event.detail.row.Id;
            this.openmodel = true;
            this.title = "EDIT TRAINING DETAILS";
            this.btnLabel = "UPDATE";
            this.isBtnDisabled = false;

            edittrainingDetails({
                edittrainingdetails: trainingid
            }).then(result => {
                this.Training = result[0].TrainingHours__c;
                this.Training2 = result[0].TrainingName__c;
                this.Training1 = result[0].CompletionDate__c;
                this.stid = result[0].Id;
            }).catch((errors) => {
                return this.dispatchEvent(utils.handleError(errors));
            });
        } else if (event.detail.action.name == "Delete") {
            this.openmodel1 = true;
            this.currentdeleterecord = event.detail.row.Id;

        }


    }

    //Delete Functinality

    handleDelete() {
        deletetrainingDetails({
            deletetrainingdetails: this.currentdeleterecord
        }).then(result => {
            this.dispatchEvent(utils.toastMessage("Training Details are deleted successfully", "success"));
            this.openmodel1 = false;
            this.stid = null;
            this.training();
        }).catch((errors) => {
            return this.dispatchEvent(utils.handleError(errors));
        });

    }



    
    // record type details
    @wire(getRecordType, {
        name: CJAMS_CONSTANTS.PRIVATE_RECORD_TYPE_NAME
    })
    recordTypeDetails(data) {
        
        this.RecordTypeId = data.data;

       
    }

    // Get Dynamic Contact id 

    get ContactId() {
        return sharedData.getStaffId();
    }


    //pagination  function part

    pageData = () => {
        let page = this.page;
        let perpage = this.perpage;
        let startIndex = (page * perpage) - perpage;
        let endIndex = (page * perpage);
        this.currentPagetrainingData = this.totalRecords.slice(startIndex, endIndex);
    }
    hanldeProgressValueChange(event) {
        this.page = event.detail;
        this.pageData();
    }

}