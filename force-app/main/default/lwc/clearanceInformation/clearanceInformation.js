/**
 * @Author        : Pratheeba.V
 * @CreatedOn     : Mar 23 ,2020
 * @Purpose       : Staff Clearance Information Tab
 * @UploadedBy    : Sundar K
 * @UploadedOn    : MAY 15, 2020
 **/

import {
    LightningElement,
    track,
    api
} from 'lwc';
import saveClearanceInformation from '@salesforce/apex/ClearanceInformationController.getIdAfterInsertSOQL';
import getContactList from '@salesforce/apex/ClearanceInformationController.getContactList';
import * as referralShareddata from 'c/sharedData';
import { CJAMS_CONSTANTS } from 'c/constants';
import {
    utils
} from "c/utils";

export default class ClearanceInformation extends LightningElement {
    @track data = [];
    @track corporateinfo = {
        creqdate: '',
        cresdate: '',
        value: '',
        cpavalue: ''
    };
    @track state = {
        sreqdate: '',
        sresdate: '',
        rccstatevalue: '',
        cpastatevalue: ''
    };
    @track federal = {
        freqdate: '',
        fresdate: '',
        rccfederalvalue: '',
        cpafederalvalue: ''
    };
    @track recordfound = false;

    //Sundar Adding this code
    @track isSaveBtnDisabled = false;
    @track minCorporateRequestDate = '1970-01-01';
    @track minCorporateResponseDate = '1970-01-01';
    @track isDisableCorporateDate = false;

    @track minStateRequestDate = '1970-01-01';
    @track minStateResponseDate = '1970-01-01';
    @track isDisableStateDate = false;

    @track minFederalRequestDate = '1970-01-01';
    @track minFederalResponseDate = '1970-01-01';
    @track isDisableFederalDate = false;


    handleCreqdate(event) {
        this.isSaveBtnDisabled = false;
        this.corporateinfo.creqdate = event.target.value;
        this.minCorporateResponseDate = event.target.value;
        this.isDisableCorporateDate = false;
    }

    handleCresdate(event) {
        this.isSaveBtnDisabled = false;
        this.corporateinfo.cresdate = event.target.value;
    }

    handleSreqdate(event) {
        this.isSaveBtnDisabled = false;
        this.state.sreqdate = event.target.value;
        this.minStateResponseDate = event.target.value;
        this.isDisableStateDate = false;
    }

    handleSresdate(event) {
        this.isSaveBtnDisabled = false;
        this.state.sresdate = event.target.value;
    }

    handleFreqdate(event) {
        this.isSaveBtnDisabled = false;
        this.federal.freqdate = event.target.value;
        this.minFederalResponseDate = event.target.value;
        this.isDisableFederalDate = false;
    }

    handleFresdate(event) {
        this.isSaveBtnDisabled = false;
        this.federal.fresdate = event.target.value;
    }

    get rcc() {
        return [{
            label: 'Yes',
            value: 'Yes'
        },
        {
            label: 'No',
            value: 'No'
        },
        {
            label: 'N/A',
            value: 'N/A'
        },
        ];
    }

    get staffid() {
        return referralShareddata.getStaffId();
    }

    rccselectval(evt) {
        this.corporateinfo.value = evt.target.value;
    }

    get cpa() {
        return [{
            label: 'Yes',
            value: 'Yes'
        },
        {
            label: 'No',
            value: 'No'
        },
        {
            label: 'N/A',
            value: 'N/A'
        },
        ];
    }

    cpaselectval(evt) {
        this.corporateinfo.cpavalue = evt.target.value;
    }

    get rccstate() {
        return [{
            label: 'Yes',
            value: 'Yes'
        },
        {
            label: 'No',
            value: 'No'
        },
        {
            label: 'N/A',
            value: 'N/A'
        },
        ];
    }

    rccstateselectval(evt) {
        this.state.rccstatevalue = evt.target.value;
    }

    get cpastate() {
        return [{
            label: 'Yes',
            value: 'Yes'
        },
        {
            label: 'No',
            value: 'No'
        },
        {
            label: 'N/A',
            value: 'N/A'
        },
        ];
    }
    cpastateselectval(evt) {
        this.state.cpastatevalue = evt.target.value;
    }

    get rccfederal() {
        return [{
            label: 'Yes',
            value: 'Yes'
        },
        {
            label: 'No',
            value: 'No'
        },
        {
            label: 'N/A',
            value: 'N/A'
        },
        ];
    }

    rssfederalselectval(evt) {
        this.federal.rccfederalvalue = evt.target.value;
    }

    get cpafederal() {
        return [{
            label: 'Yes',
            value: 'Yes'
        },
        {
            label: 'No',
            value: 'No'
        },
        {
            label: 'N/A',
            value: 'N/A'
        },
        ];
    }
    cpafederalselectval(evt) {
        this.federal.cpafederalvalue = evt.target.value;
    }


    initial() {
        getContactList({
            accountId: this.staffid
        })
            .then(result => {
                if (result.length == 0) {
                    this.recordfound = false;
                    this.data = {
                        CIRequestDate__c: '',
                        CIResponseDate__c: '',
                        CIRCC__c: '',
                        CICPA__c: '',
                        SCRequestDate__c: '',
                        SCResponseDate__c: '',
                        SCRCC__c: '',
                        SCCPA__c: '',
                        FCRequestDate__c: '',
                        FCResponseDate__c: '',
                        FCRCC__c: '',
                        FCCPA__c: ''
                    };
                } else {
                    this.recordfound = true;
                    this.data = result[0];
                    this.data.CIRequestDate__c = (this.data.CIRequestDate__c) ? this.data.CIRequestDate__c : '';
                    this.data.CIResponseDate__c = (this.data.CIResponseDate__c) ? this.data.CIResponseDate__c : '';
                    this.data.CIRCC__c = (this.data.CIRCC__c) ? this.capitalizeFirstLetter(this.data.CIRCC__c) : '';
                    this.data.CICPA__c = (this.data.CICPA__c) ? this.data.CICPA__c : '';
                    this.data.SCRequestDate__c = (this.data.SCRequestDate__c) ? this.data.SCRequestDate__c : '';
                    this.data.SCResponseDate__c = (this.data.SCResponseDate__c) ? this.data.SCResponseDate__c : '';
                    this.data.SCRCC__c = (this.data.SCRCC__c) ? this.data.SCRCC__c : '';
                    this.data.SCCPA__c = (this.data.SCCPA__c) ? this.data.SCCPA__c : '';
                    this.data.FCRequestDate__c = (this.data.FCRequestDate__c) ? this.data.FCRequestDate__c : '';
                    this.data.FCResponseDate__c = (this.data.FCResponseDate__c) ? this.data.FCResponseDate__c : '';
                    this.data.FCRCC__c = (this.data.FCRCC__c) ? this.data.FCRCC__c : '';
                    this.data.FCCPA__c = (this.data.FCCPA__c) ? this.data.FCCPA__c : '';
                }

                this.getListRecord = result[0];
                this.corporateinfo.creqdate = this.getListRecord.CIRequestDate__c;
                this.corporateinfo.cresdate = this.getListRecord.CIResponseDate__c;
                this.corporateinfo.value = this.getListRecord.CIRCC__c ? this.capitalizeFirstLetter(this.getListRecord.CIRCC__c) : 'No';
                this.corporateinfo.cpavalue = this.getListRecord.CICPA__c ? this.getListRecord.CICPA__c : 'No';
                this.state.sreqdate = this.getListRecord.SCRequestDate__c;
                this.state.sresdate = this.getListRecord.SCResponseDate__c;
                this.state.rccstatevalue = this.getListRecord.SCRCC__c ? this.getListRecord.SCRCC__c : 'No';
                this.state.cpastatevalue = this.getListRecord.SCRCC__c ? this.getListRecord.SCCPA__c : 'No';
                this.federal.freqdate = this.getListRecord.FCRequestDate__c;
                this.federal.fresdate = this.getListRecord.FCResponseDate__c;
                this.federal.rccfederalvalue = this.getListRecord.FCRCC__c ? this.getListRecord.FCRCC__c : 'No';
                this.federal.cpafederalvalue = this.getListRecord.FCCPA__c ? this.getListRecord.FCCPA__c : 'No';

                this.isDisableCorporateDate = !this.getListRecord.CIResponseDate__c ? true : false;
                this.isDisableStateDate = !this.getListRecord.SCResponseDate__c ? true : false;
                this.isDisableFederalDate = !this.getListRecord.FCResponseDate__c ? true : false;
                this.isSaveBtnDisabled = (!this.getListRecord.CIRequestDate__c && !this.getListRecord.CIResponseDate__c) ? true : false;
                this.minCorporateResponseDate = this.getListRecord.CIRequestDate__c;
                this.minFederalResponseDate = this.getListRecord.SCRequestDate__c;
                this.minFederalResponseDate = this.getListRecord.FCRequestDate__c;
            })
            .catch(errors => {
                return this.dispatchEvent(utils.handleError(errors));
            });
    }


    connectedCallback() {
        this.initial();
    }

    capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    handleCorporationSave() {
        if (!this.corporateinfo.creqdate)
            return this.dispatchEvent(utils.toastMessage("Please enter corporate request date", "warning"));

        if (!this.corporateinfo.cresdate)
            return this.dispatchEvent(utils.toastMessage("Please enter corporate response date", "warning"));

        if (new Date(this.corporateinfo.cresdate).getTime() < new Date(this.corporateinfo.creqdate))
            return this.dispatchEvent(utils.toastMessage("Response date should be greater than Request date", "warning"));

        this.isSaveBtnDisabled = true;

        let myobjcase = {
            'sobjectType': 'Contact'
        };
        myobjcase.CIRequestDate__c = this.corporateinfo.creqdate;
        myobjcase.Id = this.staffid;
        myobjcase.CIResponseDate__c = this.corporateinfo.cresdate;
        myobjcase.CIRCC__c = this.corporateinfo.value;
        myobjcase.CICPA__c = this.corporateinfo.cpavalue;
        myobjcase.ProgramType__c = 'corporate';
        saveClearanceInformation({
            objIdSobjecttoUpdateOrInsert: myobjcase
        })
            .then(result => {
                this.dispatchEvent(utils.toastMessage("Corporate details has been added successfully", "success"));
                this.isSaveBtnDisabled = false;
                this.initial();
            }).catch(errors => {
                this.isSaveBtnDisabled = false;
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
    }

    handleStateClick() {
        if (!this.state.sreqdate)
            return this.dispatchEvent(utils.toastMessage("Please enter state request date", "warning"));

        if (!this.state.sresdate)
            return this.dispatchEvent(utils.toastMessage("Please enter state response date", "warning"));

        if (new Date(this.state.sresdate).getTime() < new Date(this.state.sreqdate))
            return this.dispatchEvent(utils.toastMessage("Response date should be greater than Request date", "warning"));

        this.isSaveBtnDisabled = true;

        let myobjcase = {
            'sobjectType': 'Contact'
        };
        myobjcase.SCRequestDate__c = this.state.sreqdate;
        myobjcase.Id = this.staffid;
        myobjcase.SCResponseDate__c = this.state.sresdate;
        myobjcase.SCRCC__c = this.state.rccstatevalue;
        myobjcase.SCCPA__c = this.state.cpastatevalue;
        myobjcase.ProgramType__c = 'state';
        saveClearanceInformation({
            objIdSobjecttoUpdateOrInsert: myobjcase
        })
            .then(result => {
                this.dispatchEvent(utils.toastMessage("State details has been saved successfully", "success"));
                this.isSaveBtnDisabled = false;
                this.initial();
            }).catch(errors => {
                this.isSaveBtnDisabled = false;
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
    }

    handleFederalClick() {
        if (!this.federal.freqdate)
            return this.dispatchEvent(utils.toastMessage("Please enter federal request date", "warning"));

        if (!this.federal.fresdate)
            return this.dispatchEvent(utils.toastMessage("Please enter federal response date", "warning"));

        if (new Date(this.federal.fresdate).getTime() < new Date(this.federal.freqdate))
            return this.dispatchEvent(utils.toastMessage("Response date should be greater than Request date", "warning"));

        this.isSaveBtnDisabled = true;

        let myobjcase = {
            'sobjectType': 'Contact'
        };
        myobjcase.FCRequestDate__c = this.federal.freqdate;
        myobjcase.Id = this.staffid;
        myobjcase.FCResponseDate__c = this.federal.fresdate;
        myobjcase.FCRCC__c = this.federal.rccfederalvalue;
        myobjcase.FCCPA__c = this.federal.cpafederalvalue;
        myobjcase.ProgramType__c = 'federal';
        saveClearanceInformation({
            objIdSobjecttoUpdateOrInsert: myobjcase
        })
            .then(result => {
                this.dispatchEvent(utils.toastMessage("Federal details has been saved successfully", "success"));
                this.isSaveBtnDisabled = false;
                this.initial();
            }).catch(errors => {
                this.isSaveBtnDisabled = false;
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
    }

    @api
    handleValidation() {
        let blnValidate = false;
        if (!this.data.CIRequestDate__c || !this.data.CIResponseDate__c || !this.data.SCRequestDate__c || !this.data.SCResponseDate__c || !this.data.FCRequestDate__c || !this.data.FCResponseDate__c) {
            blnValidate = true;
        } else {
            blnValidate = false;
        }
        this.dispatchEvent(new CustomEvent("checkvalidationinfinalpage", {
            detail: {
                validate: blnValidate
            }
        }));
    }

}