/**
 * @Author        : Jayachandran
 * @CreatedOn     : Jun 29,2020
 * @Purpose       : Public / Private Complaint Dashboard Linking tabset component with Active tab
 * @updatedBy     :
 * @updatedOn     :
 **/

import {
  LightningElement,
  track,
  api
} from "lwc";
import * as sharedData from "c/sharedData";

export default class ComplaintsAllTabsCmps extends LightningElement {
  @track activeTab = "complaintreferral";
  @track activeTabValue = null;
  @track showAddDeficiencyViolations = false;
  @track showHideAddContact = false;
  @track ComplaintsStatus;

  @api showComplaintsTabsetCmps = false;
  @api showDashboard = false;
  @track caseNumber;
  @track monitoringTabs;
  @track showButtonForDeficiency;
  @track showComplaintsForCaseSupervisor = false;
  @track showButtonForContacts;
  @track SanctionTabShowHide = false;
  @track deficiencyFlag;

  @track sanctionDetail;
  @track showSanctionType = false;

  //providers show aand hide
  @track showProvidersDashoard = false;
  @track showPublicApplications = false;
  @track showPrivateProvidersTabSet = false;
  @track showPublicPrivatetabsforProvider = false;
  @track showProviderInfo = false;
  @track providerName;

  @track selectedSanctionId;
  @track sanctionActionName;
  @track sanctionName;
  @track deficiencyAddBtndisable;

  @track sanctionFlag = false;
  @track sendMonitoringFlag;

  get getUserProfileName() {
    return sharedData.getUserProfileName();
    //return "Caseworker";
  }

  handleActiveTabButtons(event) {
    //Active tab view show hide according to tabs
    this.activeTabValue = event.target.value;
    if (this.activeTabValue === "contacts") {
      this.showHideAddContact = true;
      this.showAddDeficiencyViolations = false;
    } else if (this.activeTabValue === "deficiencyviolation") {
      this.showAddDeficiencyViolations = true;
      this.showHideAddContact = false;
    } else {
      this.showAddDeficiencyViolations = false;
      this.showHideAddContact = false;
    }
    this.supervisor();
  }

  redirectComplaintsTabset(event) {
    if (
      this.getUserProfileName == "Caseworker" ||
      this.getUserProfileName == "Supervisor"
    ) {
      this.showComplaintsTabsCaseSupervisor = true;
    }
    if (this.getUserProfileName == "Supervisor") {
      let status = sharedData.getComplaintsStatus();
      let user = sharedData.getPrivateOrPublicType();
      switch (user) {
        case 'Private': {
          if (status == 'Accepted' || status == 'Disputed' || status == 'Approved') {
            this.SanctionTabShowHide = true;
          }
          break;
        }
        case 'Public': {
          if (status == 'Submitted to Supervisor' || status == 'Approved') {
            this.SanctionTabShowHide = true;
          }
          break;
        }
      }
    }
    this.showDashboard = !event.detail.first;
    this.showComplaintsTabsetCmps = true;
    this.caseNumber = event.detail.CaseId;
    this.monitoringTabs = false;
    this.ComplaintsStatus = sharedData.getComplaintsStatus();
  }

  supervisor() {
    if (this.getUserProfileName != "Supervisor") {
      if (
        [
          "Submitted to supervisor",
          "Accepted",
          "Disputed",
          "Approved",
          "Rejected"
        ].includes(this.ComplaintsStatus)
      ) {
        this.showAddDeficiencyViolations = false;
        this.showHideAddContact = false;
      }
    }
    if (this.getUserProfileName == "Supervisor") {
      this.showAddDeficiencyViolations = false;
      this.showHideAddContact = false;
    }
  }

  redirectToComplaintDashboard(event) {
    this.showDashboard = !event.detail.first;
    this.showComplaintsTabsetCmps = false;
    this.monitoringTabs = false;
  }

  redirecttoMonitoringtabs(event) {
    this.monitoringTabs = event.detail.first;
    this.showComplaintsTabsetCmps = false;
  }
  backtomonitoringAdd(event) {
    this.monitoringTabs = event.detail.first;
    this.showComplaintsTabsetCmps = true;
    this.activeTab = "monitor";
  }

  buttondeficiencyviolations(event) {
    this.showButtonForDeficiency = event.detail.first;
  }

  handleToggleComponentsForDeficiency(event) {
    this.showButtonForDeficiency = false;
  }

  handleRedirectionSanction(event) {
    this.showSanctionType = true;
    this.showComplaintsTabsetCmps = false;
    this.sanctionDetail = event.detail.pageType;
    this.selectedSanctionId = event.detail.selectedSanctionId;
    this.sanctionActionName = event.detail.actionName;
    this.sanctionName = event.detail.sanctionName;
  }

  handleComplaintSanction(event) {
    this.showSanctionType = false;
    this.showComplaintsTabsetCmps = true;
    this.SanctionTabShowHide = event.detail.first;
    this.activeTab = "complaintsanction";
  }

  handleComplaintDashboardTab(event) {
    this.showDashboard = !event.detail.first;
    this.showComplaintsTabsetCmps = false;
    this.monitoringTabs = false;
    this.showSanctionType = false;
  }

  caseNumberSet(event) {
    if (event.detail.CaseNumber !== undefined) {
      this.caseNumber = event.detail.CaseNumber;
    }
  }

  buttonContactButton(event) {
    this.showButtonForContacts = event.detail.first;
  }

  handleToggleComponentsForContacts() {
    this.showButtonForContacts = false;
  }

  //provider redirect
  redirecttoProviderPublictabs(event) {
    this.showDashboard = true;
    this.showPublicApplications = true;
    this.showPrivateProvidersTabSet = false;
    this.showProvidersDashoard = true;
    this.providerName = event.detail.providerName;
    this.showPublicPrivatetabsforProvider = true;
  }

  redirecttoProviderPrivatetabs(event) {
    this.showDashboard = true;
    this.showPublicPrivatetabsforProvider = true;
    setTimeout(() => {
      this.template
        .querySelector("c-public-provider-all-tab-cmbs")
        .handleToShowPrivateProvTab();
    }, 0);
  }

  handledeficiencyDispatchData(event) {
    this.deficiencyFlag = event.detail.deficiencyFlag;
  }

  redirctToComRefDashboard(event) {
    this.showDashboard = false;
    this.showComplaintsTabsetCmps = false;
  }

  handleDisable(event) {
    this.deficiencyAddBtndisable = event.detail.deficiencybtndis;
  }

  handleSanctionAddedFlag(event) {
    this.sanctionFlag = event.detail.sanctionflag;
  }

  handleMonitoringAddedFlag(event) {
    this.sendMonitoringFlag = event.detail.decisionflag;

  }
}