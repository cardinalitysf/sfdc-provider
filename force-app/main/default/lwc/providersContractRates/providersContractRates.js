import {
  LightningElement,
  track,
  wire
} from "lwc";
import getLicenseRatesDetails from "@salesforce/apex/providersContractRates.getLicenseRatesDetails";
import {
  utils
} from "c/utils";
import * as sharedData from 'c/sharedData';

export default class ProvidersContractRates extends LightningElement {

  //@track LicenseId = "a080p000001SGuAAAW";
  @track AvailableRateCodes;
  @track StartDate;
  @track EndDate;
  @track AnnualRates;
  @track MonthlyRates;
  @track PerDiemRate;


  //Get data fetching using ConnectedCallback

  connectedCallback() {

    this.rates();
  }
  rates() {

    getLicenseRatesDetails({
      getLicenseRatesDetails: this.LicenseId
    })
      .then((result) => {
        this.AvailableRateCodes = result[0].RateCode__c;
        this.StartDate = result[0].StartDate__c;
        this.EndDate = result[0].EndDate__c;
        this.PerDiemRate = result[0].PerDiemRate__c;
        this.MonthlyRates = result[0].Monthly__c;
        this.AnnualRates = result[0].Annual__c;
        this.id = result[0].id;
      })
      .catch((error) => {
        return this.dispatchEvent(utils.handleError(errors));
      });
  }

  // Get Dynamic LicenseId

  get LicenseId() {
    return sharedData.getLicenseId();
  }
}