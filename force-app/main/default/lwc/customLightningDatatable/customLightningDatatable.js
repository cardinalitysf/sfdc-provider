import {
    LightningElement
} from 'lwc';

import LightningDatatable from 'lightning/datatable';
import pocFileUpload from './inCustomToogle.html';


export default class CustomLightningDatatable extends LightningDatatable {

    static customTypes = {
        toogle: {
            template: pocFileUpload,
            typeAttributes: ['isChecked', 'isDisabled']
        }
    };
}