/**
 * @Author        : Preethi Bojarajan
 * @CreatedOn     : March 18,2020
 * @Purpose       : Provider's Staff List With Edit, History and Delete Options 
 * @updatedBy     : Preethi Bojarajan
 * @updatedOn     : Apr 25,2020
 **/

import { LightningElement, track, wire, api } from 'lwc';
import getStaffsOfProvider from '@salesforce/apex/providerStaff.getStaffsOfProvider';
import getStaffCertDetails from '@salesforce/apex/providerStaff.getStaffCertDetails';

// Utils
import * as sharedData from "c/sharedData";
import { utils } from 'c/utils';
import { constPopupVariables } from 'c/constants';
import { loadStyle } from 'lightning/platformResourceLoader';
import myResource from '@salesforce/resourceUrl/styleSheet';

//For Delete Record and Table Refresh
import { deleteRecord } from "lightning/uiRecordApi";
import { refreshApex } from "@salesforce/apex";

const columnsCert = [
    { label: 'Certificate Type', fieldName: 'CertificationType__c', type: 'text' },
    { label: 'Due Date', fieldName: 'DueDate__c', type: 'text' },
    { label: 'Application Mailed Date', fieldName: 'ApplicationMailedDate__c', type: 'text' },
    { label: 'Tested Date', fieldName: 'TestedDate__c', type: 'text' },
    { label: 'Effective Date', fieldName: 'CertificationEffectiveDate__c', type: 'text' },
    { label: 'Certificate Number', fieldName: 'CertificateNumber__c', type: 'text' },
    { label: 'Renewal Date', fieldName: 'RenewalDate__c', type: 'text' }
]

export default class ProviderStaffDetails extends LightningElement {
    //DataTable Data
    @track dataTableShow = false;
    @track columns = [];
    @track totalRecords = [];
    @track providerStaffs = [];
    wiredStaffRecords = [];

    // Sort Function Tracks
    @track defaultSortDirection = 'asc';
    @track sortDirection = 'asc';
    @track sortedBy = "FirstName__c";

    //Pagination Tracks    
    @track page = 1;
    perpage = 10;
    setPagination = 5;
    @track totalRecordsCount = 0;

    //Model PopUp Track
    @track openmodel = false;
    @track openmodelHistory = false;
    @api staffId = null;
    @track deleteRowId = null;
    @track staffCertificates;
    @track columnsCert = columnsCert;
    @track showAddStaffMember = false;
    @track certificateShow = true;

    openmodal() {
        return this.openmodel = true
    }
    closeModal() {
        this.openmodel = false;
        this.openmodelHistory = false;
    }
    openmodalHistory() {
        return this.openmodelHistory = true
    }

    //Get the Provider Id Start
    get providerId() {
        return sharedData.getProviderId();
    }
    //Get the Provider Id End

    //DataTable RowAction Method Start
    constructor() {
        super();
        this.columns = [
            { label: 'First Name', fieldName: 'FirstName__c', sortable: true },
            { label: 'Last Name', fieldName: 'LastName__c', sortable: true },
            { label: 'Affiliation Type', fieldName: 'AffiliationType__c' },
            { label: 'Job Title', fieldName: 'JobTitle__c' },
            { label: 'Employee Type', fieldName: 'EmployeeType__c' },
            { label: 'Start Date', fieldName: 'StartDate__c' },
            { label: 'End Date', fieldName: 'EndDate__c' },
            //Removed ? on 21st apr
            { label: 'Is BIT', fieldName: 'isBit' },
            { label: 'Reason For Leaving', fieldName: 'ReasonforLeaving__c' },
            { label: 'Comments', fieldName: 'Comments__c' },
            { type: 'action', fieldName: 'id', typeAttributes: { rowActions: this.getRowActions } },
        ]
    }
    getRowActions(row, doneCallback) {
        const actions = [];
        actions.push({
            'label': 'Edit',
            'iconName': 'action:edit',
            'name': 'editStaff'
        });
        actions.push({
            'label': 'History',
            'iconName': 'standard:channel_program_history',
            'name': 'records'
        });
        actions.push({
            'label': 'Delete',
            'iconName': 'action:delete',
            'name': 'deleteStaff'
        });
        // simulate a trip to the server
        setTimeout(() => {
            doneCallback(actions);
        }, 200);
    }
    //DataTable RowAction Method Start

    //Get Staffs for the Provider Function Start
    @wire(getStaffsOfProvider, { providerId: '$providerId' })
    staffDetails(data) {
        this.wiredStaffRecords = data;
        if (data.data != null && data.data[0].contacts != '' && data.data[0].contacts.length > 0) {
            this.dataTableShow = true;
            let currentData = [];
            let certi = data.data[0].certificates;
            data.data[0].contacts.forEach((row) => {
                let rowData = {};
                rowData.id = row.Id;
                rowData.FirstName__c = row.FirstName__c;
                rowData.LastName__c = row.LastName__c;
                rowData.AffiliationType__c = row.AffiliationType__c;
                rowData.JobTitle__c = row.JobTitle__c;
                rowData.EmployeeType__c = row.EmployeeType__c;
                rowData.StartDate__c = utils.formatDate(row.StartDate__c);
                rowData.EndDate__c = utils.formatDate(row.EndDate__c);
                rowData.ReasonforLeaving__c = row.ReasonforLeaving__c;
                rowData.Comments__c = row.Comments__c,
                    rowData.isBit = certi.findIndex(cer => cer.Staff__c === row.Id) >= 0 ? 'Yes' : 'No',
                    currentData.push(rowData);
            });
            this.totalRecords = currentData;
            this.totalRecordsCount = this.totalRecords.length;
            this.pageData();
        } else {
            this.dataTableShow = false;
        }
    }
    //Get Staffs for the Provider Function End

    // Sorting the Data Table Column Function End
    sortBy(field, reverse, primer) {
        const key = primer
            ? function (x) {
                return primer(x[field]);
            }
            : function (x) {
                return x[field];
            };

        return function (a, b) {
            a = key(a);
            b = key(b);
            if (a === undefined) a = '';
            if (b === undefined) b = '';
            a = typeof (a) === 'number' ? a : a.toLowerCase();
            b = typeof (b) === 'number' ? b : b.toLowerCase();

            return reverse * ((a > b) - (b > a));
        };
    }
    onHandleSort(event) {
        const { fieldName: sortedBy, sortDirection: sortDirection } = event.detail;
        const cloneData = [...this.providerStaffs];

        cloneData.sort(this.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1));
        this.applicationsOfProv = cloneData;
        this.sortDirection = sortDirection;
        this.sortedBy = sortedBy;
    }
    // Sorting the Data Table Column Function End

    //Pagination Functions Start
    pageData = () => {
        let page = this.page;
        let perpage = this.perpage;
        let startIndex = (page * perpage) - perpage;
        let endIndex = (page * perpage);
        this.providerStaffs = this.totalRecords.slice(startIndex, endIndex);
    }
    //Pagination Functions End

    //Redirect to Edit the Staff Function Start
    editStaff() {
        this.template.querySelector('c-staff-progress-indicator').openmodal();
    }
    //Redirect to Edit the Staff Function End

    /* Style Sheet Loading */
    renderedCallback() {
        Promise.all([
            loadStyle(this, myResource + '/styleSheet.css')
        ])
    }

    //Action Button Functions(Edit / Delete and Certification) Start
    deleteStaff(id) {
        this.openmodal();
        return id;
    }

    handleRowAction(event) {
        const action = event.detail.action;
        const row = event.detail.row;
        switch (action.name) {
            case 'editStaff':
                sharedData.setStaffId(row.id);
                this.editStaff();
                break;
            case 'deleteStaff':
                this.deleteRowId = row;
                this.staffId = this.deleteStaff(row.id);
                break;
            case 'records':
                this.staffId = row.id;
                this.openmodalHistory();
                this.fetchStaffCertificates();
                break;
        }
    }

    deleteFrSure() {
        deleteRecord(this.staffId)
            .then(() => {
                this.closeModal();
                this.dispatchEvent(utils.toastMessage(constPopupVariables.DELETESUCCESS, "success"));
                return refreshApex(this.wiredStaffRecords);
            }).catch(error => {
                this.closeModal();
                if (error.body.statusCode === 403)
                    this.dispatchEvent(utils.toastMessage(constPopupVariables.UNAUTHOREDEL, "error"));
                else
                    this.dispatchEvent(utils.toastMessage(constPopupVariables.STAFF_HAV_REF_DEL, "error"));
            })
    }

    fetchStaffCertificates() {
        getStaffCertDetails({ staffId: this.staffId }).then(data => {
            if (data.length == 0) {
                this.certificateShow = false;
            } else {
                this.certificateShow = true;
                let currentData = [];
                data.forEach((row) => {
                    let rowData = {};
                    rowData.CertificationType__c = row.CertificationType__c;
                    rowData.DueDate__c = utils.formatDate(row.DueDate__c);
                    rowData.ApplicationMailedDate__c = utils.formatDate(row.ApplicationMailedDate__c);
                    rowData.TestedDate__c = utils.formatDate(row.TestedDate__c);
                    rowData.CertificationEffectiveDate__c = utils.formatDate(row.CertificationEffectiveDate__c);
                    rowData.RenewalDate__c = utils.formatDate(row.RenewalDate__c);
                    rowData.CertificateNumber__c = row.CertificateNumber__c;
                    currentData.push(rowData);
                });
                this.staffCertificates = currentData;
            }
        }).catch(error => {
            this.dispatchEvent(utils.toastMessage("Error in fetching record", "error"));
        });
    }
    //Action Button Functions(Edit / Delete and Certification) End

    //For Refresh After Creating New Staff
    hanldeProgressPopClose(event) {
        this.progressValue = event.detail;
        if (this.progressValue == true) {
            return refreshApex(this.wiredStaffRecords);
        }
    }

    //For Pagination Child Bind
    hanldeProgressValueChange(event) {
        this.page = event.detail;
        this.pageData();
    }

}