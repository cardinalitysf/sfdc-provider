/**
 * @Author        : BalaMurugan
 * @CreatedOn     : Apr 12,2020
 * @Purpose       : Application Of Monitoring in Staff Interview  Ques And Ans
 **/

import { LightningElement, api, track } from "lwc";
import * as sharedData from "c/sharedData";

export default class MonitoringBoardInterviewQuestion extends LightningElement {
  @track _monitoringData;
  @track _monitoringFieldtype;
  @track _monitoringId;
  @track radioButton;
  @track textBox;
  @track subHeading;
  @track _monitoringQuestions;
  @track _commentsAnswer;
  @track _profitAnswer;
  @track _subHeading;
  @track disableFields = false;

  answer = {
    comar: null,
    answer: null
  };

  @api
  get monitoringQuestions() {
    return this._monitoringQuestions;
  }

  set monitoringQuestions(value) {
    this.setAttribute("monitoringQuestions", value);
    let tempQuestionArr = [];
    tempQuestionArr.push(value);
    this._monitoringQuestions = tempQuestionArr;
  }
  // end monitoring question

  //   getting monitoring questions
  @api
  get monitoringData() {
    return this._monitoringData;
  }
  set monitoringData(value) {
    this._monitoringData = value;
  }

  //geting monitring id

  @api
  get monitoringId() {
    return this._monitoringId;
  }

  set monitoringId(value) {
    this.setAttribute("monitoringId", value);
    this._monitoringId = value;
  }

  //   getting monitoring fieldType
  @api
  get monitoringFieldtype() {
    return this._monitoringFieldtype;
  }
  set monitoringFieldtype(value) {
    this.setAttribute("monitoringFieldtype", value);
    this._monitoringFieldtype = value;
    if (value === "Text Box") {
      this.textBox = true;
    } else if (value === "Radio Button") {
      this.radioButton = true;
    } else if (value === "Sub Heading") {
      this.subHeading = true;
    } else {
      return;
    }
  }

  // gettting answer

  @api
  get question() {
    return this._answer;
  }
  set question(value) {
    if (sharedData.getMonitoringStatus() !== 'Draft')
      this.disableFields = true;
    this.setAttribute("question", value);

    if (value.FieldType__c === "Text Box") {
      this._commentsAnswer = value.answer;
    } else if (value.FieldType__c === "Radio Button") {
      this._profitAnswer = value.answer;
    } else if (value.FieldType__c === "Sub Heading") {
      this._subHeading = value.answer;
    } else {
      return;
    }
  }

  //   radio button value
  get options() {
    if (sharedData.getMonitoringStatus() !== 'Draft')
      this.disableFields = true;
    return [
      {
        label: "Yes",
        value: "Yes"
      },
      {
        label: "No",
        value: "No" // change  NO to No
      }
    ];
  }

  // radio button handler
  onChangeHandler(event) {
    // this.value = "Yes";

    let value = event.target.value;
    let quesId = event.target.dataset.id;
    let fieldType = event.target.dataset.type;

    this.answer.comar = quesId;
    this.answer.answer = value;
    this.answer.fieldType = fieldType;

    const selectedEvent = new CustomEvent("changequestionanswer", {
      detail: this.answer
    });
    this.dispatchEvent(selectedEvent);
  }


}