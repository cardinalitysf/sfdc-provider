import {
    LightningElement,
    track,
    wire
} from 'lwc';
import {
    loadStyle
} from 'lightning/platformResourceLoader';
import myResource from '@salesforce/resourceUrl/styleSheet';

/* eslint-disable no-console */
/* eslint-disable no-alert */
import getchildwelfareCases from '@salesforce/apex/ReferralHomeDashboard.getreferralhomeCases';

export default class ReferralHomeDashboard extends LightningElement {
    @track approvedcountval;
    @track pendingcasescountval;
    @track Rejectedcasescountval;
    @track totalcountval;
    @track defaultDesignClass = 'slds-card__header slds-grid shadow';

    @wire(getchildwelfareCases)
    childwelfarecaseresult(result) {
        if (result.data) {
            var wraperresulet = result.data;
            for (var key in wraperresulet) {
                this.approvedcountval = wraperresulet[key].approvedcasescount;
                this.pendingcasescountval = wraperresulet[key].pendingcasescount;
                this.Rejectedcasescountval = wraperresulet[key].Rejectedcasescount;
                this.totalcountval = wraperresulet[key].totalcount;
            }
        }
    }


    renderedCallback() {
        Promise.all([
            loadStyle(this, myResource + '/styleSheet.css'),
            loadStyle(this, myResource + '/responsive-style.css')
        ])
    }

    pendingRecords() {
        const selectedEvent = new CustomEvent("progressvaluechange", {
            detail: 'pending'
        });
        this.dispatchEvent(selectedEvent);
        let datas = this.template.querySelectorAll('.shadow');
        for (let i = 0; i < datas.length; i++) {
            if (i == 0)
                datas[i].className = this.defaultDesignClass + ' shadowPending';
            else
                datas[i].className = this.defaultDesignClass;
        }
    }
    approvedRecords() {
        const selectedEvent = new CustomEvent("progressvaluechange", {
            detail: 'approved'
        });
        this.dispatchEvent(selectedEvent);
        let datas = this.template.querySelectorAll('.shadow');
        for (let i = 0; i < datas.length; i++) {
            if (i == 1)
                datas[i].className = this.defaultDesignClass + ' shadowApproved';
            else
                datas[i].className = this.defaultDesignClass;
        }
    }
    rejectedRecords() {
        const selectedEvent = new CustomEvent("progressvaluechange", {
            detail: 'rejected'
        });
        this.dispatchEvent(selectedEvent);
        let datas = this.template.querySelectorAll('.shadow');
        for (let i = 0; i < datas.length; i++) {
            if (i == 2)
                datas[i].className = this.defaultDesignClass + ' shadowRejected';
            else
                datas[i].className = this.defaultDesignClass;
        }
    }

    totalRecords() {
        const selectedEvent = new CustomEvent("progressvaluechange", {
            detail: 'total'
        });
        this.dispatchEvent(selectedEvent);
        let datas = this.template.querySelectorAll('.shadow');
        for (let i = 0; i < datas.length; i++) {
            if (i == 3)
                datas[i].className = this.defaultDesignClass + ' shadowTotal';
            else
                datas[i].className = this.defaultDesignClass;
        }
    }

}