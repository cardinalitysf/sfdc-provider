/**
 * @Author        : BalaMurugan
 * @CreatedOn     : Apr 12,2020
 * @Purpose       : Attachments based on Provider Application
 **/


    import {
        LightningElement,
        track
    
    } from 'lwc';
    import * as referralsharedDatas from "c/sharedData";
    import {
        CJAMS_CONSTANTS
    } from 'c/constants';
    
    
    export default class ProviderApplicationDocuments extends LightningElement {
        get applicationID() {
            return referralsharedDatas.getApplicationId();
        }
        get sobjectNameToFetch() {
            return CJAMS_CONSTANTS.APPLICATION_OBJECT_NAME;
        }
    
        get getUserProfileName() {
            return referralsharedDatas.getUserProfileName();
        }
    
    }