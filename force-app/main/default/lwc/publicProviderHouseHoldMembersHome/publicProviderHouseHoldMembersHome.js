/**
 * @Author        : S. Jayachandran
 * @CreatedOn     : May 14 ,2020
 * @Purpose       : function show publicProviderHouseHoldMembersHome(Contact Sobj).
 **/
import { LightningElement, track, wire } from "lwc";
import getContactHouseHoldMembersDetails from "@salesforce/apex/PublicProviderHouseHoldMembersHome.getContactHouseHoldMembersDetails";
import getPublicDecisionStatusDetails from "@salesforce/apex/PublicProviderHouseHoldMembersHome.getPublicDecisionStatusDetails";
import images from "@salesforce/resourceUrl/images";
import getContactImgAsBas64Image from "@salesforce/apex/PublicProviderHouseHoldMembersHome.getContactImgAsBas64Image";
import { utils } from "c/utils";
import { deleteRecord } from "lightning/uiRecordApi";
import * as sharedData from "c/sharedData";
import { getRecord } from "lightning/uiRecordApi";
import APPLICATIONSTATUS_FIELD from "@salesforce/schema/Application__c.Status__c";
import { CJAMS_CONSTANTS } from "c/constants";

export default class PublicProviderHouseHoldMembersHome extends LightningElement {
  @track ContactDetailsOfCard;
  @track norecorddisplay = true;
  @track defaultDesignClass = "mcard houseHoldMemberList";
  attachmentIcon = images + "/humanpictosIcon.svg";
  @track ContactID;
  @track signUrl;
  @track Spinner = true;
  @track ImgOfHHMembers;
  @track deleteModel = false;
  @track recIdDelete;
  @track editdeleteShowHide = true;
  @track addSearchbtnShowHide = true;
  @track getapplicationDetails;
  @track noimages = images + "/user.svg";
  @track disabled = false;
  @track AssignbtnDisable;

  //color change Based on Gender
  renderedCallback() {
    let datas = this.template.querySelectorAll(".houseHoldMemberList");
    for (let i in this.ContactDetailsOfCard) {
      if (this.ContactDetailsOfCard[i].Gender__c == "Male") {
        datas[i].className = this.defaultDesignClass + " male";
      } else {
        datas[i].className = this.defaultDesignClass + " female";
      }
    }
  }
  connectedCallback() {
    setTimeout(() => {
      this.getAllContactDetails();
    }, 1000);

    window.addEventListener('click', evt => {
      this.ShowOfEditDelView();
    }, true);

  }

  get refid() {
    return sharedData.getCaseId();
  }

  get ApplicationId() {
    return sharedData.getApplicationId();
  }

  get providerId() {
    return sharedData.getProviderId();
  }

  get getUserProfileName() {
    return sharedData.getUserProfileName();
  }

  @wire(getPublicDecisionStatusDetails, {
    refid: "$refid"
  })
  StatusData(result) {
    if (result.data != undefined) {
      if (
        result.data[0].Status == "Approved" ||
        result.data[0].Status == "Rejected"
      ) {
        this.editdeleteShowHide = false;
        this.addSearchbtnShowHide = false;
      } else {
        this.editdeleteShowHide = true;
        this.addSearchbtnShowHide = true;
      }
    }
  }

  @wire(getRecord, {
    recordId: "$ApplicationId",
    fields: [APPLICATIONSTATUS_FIELD]
  })
  wireuser({ error, data }) {
    if (error) {
      this.error = error;
    } else if (data) {
      this.ApplicationStatus = data.fields.Status__c.value;

      if (data) {
        if (this.getUserProfileName != "Supervisor") {
          if (
            [
              "Caseworker Submitted",
              "Approved",
              "Rejected",
              "Supervisor Rejected"
            ].includes(this.ApplicationStatus)
          ) {
            this.editdeleteShowHide = false;
            this.addSearchbtnShowHide = false;
          } else {
            this.editdeleteShowHide = true;
            this.addSearchbtnShowHide = true;
          }
        }
        if (this.getUserProfileName == "Supervisor") {
          this.editdeleteShowHide = false;
          this.addSearchbtnShowHide = false;
        } else {
          this.editdeleteShowHide = true;
          this.addSearchbtnShowHide = true;
        }
      }
    }
  }

  handleView(event) {
    this.recViewId = event.target.dataset.id;
    sharedData.setHouseHoldContactId(this.recViewId);
    const viewEvent = new CustomEvent("viewofhouseholdmembers", {
      detail: {
        isView: false
      }
    });
    this.dispatchEvent(viewEvent);
  }

  handleEdit(event) {
    this.recordEditId = event.target.dataset.id;
    sharedData.setHouseHoldContactId(this.recordEditId);
    const onaddid = new CustomEvent("redirecttoaddhouseholdmembers", {
      detail: {
        first: false
      }
    });
    this.dispatchEvent(onaddid);
  }

  addHouseMemberRedirect() {
    sharedData.setHouseHoldContactId(undefined);
    const onaddid = new CustomEvent("redirecttoaddhouseholdmembers", {
      detail: {
        first: false
      }
    });
    this.dispatchEvent(onaddid);
  }

  addSearchHouseMemberRedirect() {
    sharedData.setHouseHoldContactId(undefined);
    const onsearchredirectid = new CustomEvent(
      "redirecttoaddhouseholdmembersearch",
      {
        detail: {
          first: false
        }
      }
    );
    this.dispatchEvent(onsearchredirectid);
  }

  DeleteModal(event) {
    this.deleteModel = true;
    this.recIdDelete = event.target.dataset.id;
  }

  handleDelete() {
    this.disabled = true;
    deleteRecord(this.recIdDelete)
      .then(() => {
        this.deleteModel = false;
        this.disabled = false;
        this.dispatchEvent(
          utils.toastMessage("Household Member deleted successfully", "success")
        );
        this.getAllContactDetails();
      })
      .catch((error) => {
        this.deleteModel = false;
        this.disabled = false;
        this.dispatchEvent(
          utils.toastMessage("Error in delete contact details", "error")
        );
      });
  }

  closeDeleteModal() {
    this.deleteModel = false;
  }

  ShowOfEditDelView() {
    let threedotClass = this.template.querySelectorAll(".EditDelDots");
    threedotClass.forEach((i) => {
      let indvRow = i;
      indvRow.classList.add("slds-hide");
      indvRow.classList.remove("slds-showd");
    });
  }

  editDeLDetails(event) {
    window.removeEventListener('click', evt => {
    }, true);
    this.recId = event.target.dataset.id;
    this.toggleEditDelView();
  }
  //edit del ThreeDots class hide func
  toggleEditDelView() {
    let threedotClass = this.template.querySelectorAll(".EditDelDots");

    threedotClass.forEach((i) => {
      let indvRow = i;
      let indvRowId = i.getAttribute("id");

      if (indvRowId.includes(this.recId)) {
        if (indvRow.classList.contains("slds-showd")) {
          indvRow.classList.add("slds-hide");
          indvRow.classList.remove("slds-showd");
        } else {
          indvRow.classList.remove("slds-hide");
          indvRow.classList.add("slds-showd");
        }
      } else {
        indvRow.classList.add("slds-hide");
        indvRow.classList.remove("slds-showd");
      }
    });
  }

  //fetch data from contact obj and show in home.
  getAllContactDetails() {
    if (!this.providerId) {
      this.norecorddisplay = false;
      this.Spinner = false;
      return
    }
    getContactHouseHoldMembersDetails({
      providerId: this.providerId
    }).then((data) => {
      this.Spinner = false;
      if (data) {
        let contactID = [];
        if (data.length == 0) {
          this.norecorddisplay = false;
        } else {
          const selectedEvent = new CustomEvent("datacardhhmembertrue", {
            detail: {
              meetingFlag: true
            }
          });
          this.dispatchEvent(selectedEvent);
          this.norecorddisplay = true;
          this.ContactDetailsOfCard = data.map((row) => {
            contactID.push(row.Contact__r.Id);
            return {
              ActorId: row.Id,
              Id: row.Contact__r.Id,
              Role__c: row.Role__c,
              SSN__c: row.Contact__r.SSN__c,
              Gender__c: row.Contact__r.Gender__c,
              ApproximateAge__c: row.Contact__r.ApproximateAge__c,
              DOB__c: utils.formatDate(row.Contact__r.DOB__c),
              ContactNumber__c: row.Contact__r.ContactNumber__c,
              FirstName: row.Contact__r.FirstName__c,
              LastName:
                row.Contact__r.LastName__c != undefined
                  ? row.Contact__r.LastName__c
                  : " ",
              Title__c: row.Contact__r.Title__c,
              ProfilePercentage__c: row.Contact__r.ProfilePercentage__c,
              BackgroungCheckPercentage__c: row.BackgroungCheckPercentage__c,
              TrainingPercentage__c: row.TrainingPercentage__c
            };
          });

          this.ContactID = contactID.toString();
          setTimeout(() => {
            this.ImagesOfHHM();
          }, 1000);
        }
      }
    }).catch(errors => {
      let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
      return this.dispatchEvent(utils.handleError(errors, config));
    });
  }

  ImagesOfHHM() {
    getContactImgAsBas64Image({
      ContactID: this.ContactID
    }).then((result) => {
      var DataOfImages = result;
      //Images For House listed For Card func
      this.ContactDetailsOfCard.forEach((element) => {
        DataOfImages.forEach((Key) => {
          if (element.Id == Key.LinkedEntityId) {
            element.signUrl =
              "/sfc/servlet.shepherd/version/download/" +
              Key.ContentDocument.LatestPublishedVersionId;
          }
        });
        if (!element.signUrl) {
          element.signUrl = this.noimages;
        }
      });
    }).catch(errors => {
      let config = { friendlyErrorMsg: CJAMS_CONSTANTS.ERROR_IN_FETCHING_IMAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
      return this.dispatchEvent(utils.handleError(errors, config));
    });
  }
}
