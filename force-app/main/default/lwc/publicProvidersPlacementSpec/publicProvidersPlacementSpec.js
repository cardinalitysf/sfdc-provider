/**
 * @Author        : Pratheeba V
 * @CreatedOn     : June 19, 2020
 * @Purpose       : Public Provider Placement tab 
 **/
import { LightningElement, track, wire, api } from 'lwc';
import homeSlider from "@salesforce/resourceUrl/HomeSlider";
import images from "@salesforce/resourceUrl/images";
import getProgramDetails from '@salesforce/apex/PublicProvidersPlacementSpec.getProgramDetails';
import getPickListValues from '@salesforce/apex/PublicProvidersPlacementSpec.getPickListValues';
import InsertUpdateAppPlacement from '@salesforce/apex/PublicProvidersPlacementSpec.InsertUpdateAppPlacement';
import InsertUpdateAppPlacementAddr from '@salesforce/apex/PublicProvidersPlacementSpec.InsertUpdateAppPlacementAddr';
import fetchPlacementInformation from "@salesforce/apex/PublicProvidersPlacementSpec.fetchPlacementInformation";
import fetchAddressInformation from "@salesforce/apex/PublicProvidersPlacementSpec.fetchAddressInformation";
import RECONSIDERATIONSTATUS_FIELD from '@salesforce/schema/Reconsideration__c.Status__c';
import getApplicationByProvider from '@salesforce/apex/PublicProvidersPlacementSpec.getApplicationByProvider';
import getDataByReconsiderationId from '@salesforce/apex/PublicProvidersPlacementSpec.getDataByReconsiderationId';
import {
  updateRecord
} from "lightning/uiRecordApi";

import ID_FIELD from "@salesforce/schema/Reconsideration__c.Id";
import CAPACITY_FIELD from "@salesforce/schema/Reconsideration__c.Capacity__c";
import MINAGE_FIELD from "@salesforce/schema/Reconsideration__c.MinAge__c";
import MAXAGE_FIELD from "@salesforce/schema/Reconsideration__c.MaxAge__c";
import GENDER_FIELD from "@salesforce/schema/Reconsideration__c.Gender__c";
import RACE_FIELD from "@salesforce/schema/Reconsideration__c.Race__c";
import RESPITESERVICE_FIELD from "@salesforce/schema/Reconsideration__c.RespiteService__c";
import STARTDATE_FIELD from "@salesforce/schema/Reconsideration__c.StartDate__c";
import ENDDATE_FIELD from "@salesforce/schema/Reconsideration__c.EndDate__c";
import PROGRAM_FIELD from "@salesforce/schema/Reconsideration__c.Program__c";
import PROGRAMTYPE_FIELD from "@salesforce/schema/Reconsideration__c.ProgramType__c";
import {
  utils
} from "c/utils";
import * as sharedData from "c/sharedData";
import {
  getRecord
} from "lightning/uiRecordApi";
import getFullDetails from "@salesforce/apex/GetStreetAddressFromGoogleAPI.getPlaceDetails";
import getAPIStreetAddress from "@salesforce/apex/GetStreetAddressFromGoogleAPI.getSuggestions";
import getAllStates from "@salesforce/apex/ReferralProviderAddOrEdit.getStateDetails";
import rangeSlider from "@salesforce/resourceUrl/RangeSlider";
import { CJAMS_CONSTANTS } from 'c/constants';
export default class PublicProvidersPlacementSpec extends LightningElement {
  @track Gender;
  @track GenderOptions;
  @track Race;
  @track selectedCounty;
  @track strStreetAddress;
  @track strStreetAddresPrediction = [];
  @track filteredCountyArr = [];
  @track selectedState;
  @track selectedCity;
  @track selectedZipCode;
  @track selectedCounty;
  @track ShippingStreet;
  @track placementInformation = {};
  @track reconsiderationObj = {};
  @track addressDetails = {};
  @track isDisabled = false;
  @track commonVar;
  @track Status;
  @track applicationid;
  rangeSliderAgeURL = rangeSlider + "/Age.html";

  homeInformationIcon = images + "/home-information.svg";
  privateIcon = images + "/office-building.svg";
  existingIcon = images + "/clipboard.svg";
  plusIcon = images + "/plus-new-referal.svg";
  publicIcon = images + "/houseIcon.svg";

  _vacancySliderValue;
  _vacancySlider = homeSlider + "/Vacancy.html";

  get vacancySlider() {
    return this._vacancySlider;
  }

  get providerId() {
    return sharedData.getProviderId();
  }

  get reconsiderationId() {
    return sharedData.getReconsiderationId();
  }

  //Get Slider Age URL
  get sliderAgeURL() {
    return this.rangeSliderAgeURL;
  }

  get getUserProfileName() {
    return sharedData.getUserProfileName();
  }


  connectedCallback() {
    getApplicationByProvider({
      providerid: this.providerId
    })
      .then(result => {
        this.applicationid = result[0].Id;
        this.getProgram();
      }).catch(errors => {
        return this.dispatchEvent(utils.handleError(errors));
      });

    window.addEventListener("message", event => {
      let iframevalue = event.data.split(":");
      switch (iframevalue[0]) {
        case "Vacancy":
          this._vacancySliderValue = iframevalue[1];
          break;
        case "MinAge":
          this.placementInformation.MinAge__c = iframevalue[1];
          break;
        case "MaxAge":
          this.placementInformation.MaxAge__c = iframevalue[1];
          break;
        default:
        // code block
      }
    });

  }

  @wire(getRecord, {
    recordId: "$reconsiderationid",
    fields: [RECONSIDERATIONSTATUS_FIELD]
  })
  wireuser({
    error,
    data
  }) {
    if (error) {
      this.error = error;
    } else if (data) {
      this.Status = data.fields.Status__c.value;
      if (this.Status) {
        if (this.getUserProfileName != 'Supervisor') {
          if (['Submitted', 'Approved', 'Rejected'].includes(this.Status)) {
            this.isDisabled = true;
          }
          else {
            this.isDisabled = false;
          }
        }
        if (this.getUserProfileName == 'Supervisor') {
          this.isDisabled = true;
        }
      }
    }
  }

  //store provider id and application id in address table against application
  @wire(getPickListValues, {
    objInfo: {
      'sobjectType': 'Application__c'
    },
    picklistFieldApi: 'Gender__c'
  })
  wireCheckPicklistGender({
    error,
    data
  }) {
    if (data) {
      this.GenderOptions = data;
    }
  }

  @wire(getPickListValues, {
    objInfo: {
      'sobjectType': 'Application__c'
    },
    picklistFieldApi: 'Race__c'
  })
  wireCheckPicklistRace({
    error,
    data
  }) {
    if (data) {
      this.RaceOptions = data;
    }
  }

  get streetAddressLength() {
    return this.strStreetAddresPrediction != "";
  }
  get strStreetAddresPredictionUI() {
    return this.strStreetAddresPrediction;
  }
  get strStreetAddresPredictionLength() {
    if (this.strStreetAddress)
      return this.strStreetAddresPrediction.predictions ? true : false;
  }

  get radioOptions() {
    return [
      {
        label: "Yes",
        value: "Yes"
      },
      {
        label: "No",
        value: "No"
      }
    ];
  }

  getProgram() {
    getDataByReconsiderationId({
      reconsiderationId: this.reconsiderationId
    })
      .then(result => {
        if (!result[0].Gender__c) {
          getProgramDetails({
            applicationid: this.applicationid
          })
            .then(result => {
              if (result.length == 0) {
                this.placementInformation.Program__c = null;
                this.placementInformation.ProgramType__c = null;
              } else {
                this.placementInformation.Program__c = (result[0].Program__c) ? result[0].Program__c : null;
                this.placementInformation.ProgramType__c = (result[0].ProgramType__c) ? result[0].ProgramType__c : null;
                this.reconsiderationObj.Program__c = (result[0].Program__c) ? result[0].Program__c : null;
                this.reconsiderationObj.ProgramType__c = (result[0].ProgramType__c) ? result[0].ProgramType__c : null;
                this.reconsiderationObj.Capacity__c = (result[0].Capacity__c) ? result[0].Capacity__c : null;
                this.reconsiderationObj.MinAge__c = (result[0].MinAge__c) ? result[0].MinAge__c : null;
                this.reconsiderationObj.MaxAge__c = (result[0].MaxAge__c) ? result[0].MaxAge__c : null;
                this.reconsiderationObj.Gender__c = (result[0].Gender__c) ? result[0].Gender__c : null;
                this.reconsiderationObj.Race__c = (result[0].Race__c) ? result[0].Race__c : null;
                this.reconsiderationObj.StartDate__c = (result[0].LicenseStartdate__c) ? result[0].LicenseStartdate__c : null;
                this.reconsiderationObj.EndDate__c = (result[0].LicenseEnddate__c) ? result[0].LicenseEnddate__c : null;
                this.reconsiderationObj.RespiteService__c = (result[0].RespiteServices__c) ? result[0].RespiteServices__c : null;
                if (Object.keys(this.reconsiderationObj).length > 0) {
                  const fields = {};
                  fields[CAPACITY_FIELD.fieldApiName] = this.reconsiderationObj.Capacity__c;
                  fields[MINAGE_FIELD.fieldApiName] = this.reconsiderationObj.MinAge__c;
                  fields[MAXAGE_FIELD.fieldApiName] = this.reconsiderationObj.MaxAge__c;
                  fields[GENDER_FIELD.fieldApiName] = this.reconsiderationObj.Gender__c;
                  fields[RACE_FIELD.fieldApiName] = this.reconsiderationObj.Race__c;
                  fields[RESPITESERVICE_FIELD.fieldApiName] = this.reconsiderationObj.RespiteService__c;
                  fields[STARTDATE_FIELD.fieldApiName] = this.reconsiderationObj.StartDate__c;
                  fields[ENDDATE_FIELD.fieldApiName] = this.reconsiderationObj.EndDate__c;
                  fields[PROGRAM_FIELD.fieldApiName] = this.reconsiderationObj.Program__c;
                  fields[PROGRAMTYPE_FIELD.fieldApiName] = this.reconsiderationObj.ProgramType__c;
                  fields[ID_FIELD.fieldApiName] = this.reconsiderationId;
                  const recordInput = { fields };
                  updateRecord(recordInput)
                    .then(result => {
                      this.fetchDataOnLoad();
                    })
                    .catch((errors) => {
                      let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                      return this.dispatchEvent(utils.handleError(errors, config));
                    });
                  if (!result[0].Address__r)
                    return;
                  let myobj = {}
                  myobj.sobjectType = 'Address__c';
                  myobj.Reconsideration__c = this.reconsiderationId;
                  myobj.Id = (result[0].Address__r[0].Id) ? result[0].Address__r[0].Id : null;
                  InsertUpdateAppPlacementAddr({
                    objSobjecttoUpdateOrInsert: myobj
                  })
                    .then(result => {
                      this.fetchDataOnLoad();
                    }).catch(errors => {
                      let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                      return this.dispatchEvent(utils.handleError(errors, config));
                    });
                }
              }
            }).catch(errors => {
              return this.dispatchEvent(utils.handleError(errors));
            });
        } else {
          this.fetchDataOnLoad();
        }
      }).catch(errors => {
        return this.dispatchEvent(utils.handleError(errors));
      });
  }
  /** Google api code begins here*/
  get streetAddressLength() {
    return this.strStreetAddresPrediction != "";
  }
  get strStreetAddresPredictionUI() {
    return this.strStreetAddresPrediction;
  }
  get strStreetAddresPredictionLength() {
    if (this.addressDetails.AddressLine1__c)
      return this.strStreetAddresPrediction.predictions ? true : false;
  }

  //Method used to show and hide the drop-down of suggestions
  get tabClass() {
    return this.active ?
      "slds-dropdown slds-dropdown_length-5 slds-dropdown_fluid slds-show" :
      "slds-dropdown slds-dropdown_length-5 slds-dropdown_fluid slds-hide";
  }

  //Function used to fetch all states from Address Object when Screen Loads
  @wire(getAllStates)
  getAllStates({
    error,
    data
  }) {
    if (data) {
      if (data.length > 0) {
        let stateData = data.map(row => {
          return {
            Id: row.Id,
            Value: row.RefValue__c
          };
        });
        this.stateFetchResult = stateData;
      } else {
        this.dispatchEvent(utils.toastMessage("No states found", "Warning"));
      }
    } else if (error) {
      this.dispatchEvent(
        utils.toastMessage("Error in Fetching states", "Warning")
      );
    }
  }

  //Function used to set selected state
  setSelectedCounty(event) {
    this.addressDetails.County__c = event.currentTarget.dataset.value;
    this.showCountyBox = false;
  }

  //Function used to set selected state
  setSelectedState(event) {
    this.addressDetails.State__c = event.currentTarget.dataset.value;
    this.showStateBox = false;
  }

  //Function used to capture on change county in an array
  countyOnChange(event) {
    let countySearchTxt = event.target.value;

    if (!countySearchTxt) this.showCountyBox = false;
    else {
      this.filteredCountyArr = this.countyArr
        .filter(val => val.value.indexOf(countySearchTxt) > -1)
        .map(cresult => {
          return cresult;
        });
      this.showCountyBox = this.filteredCountyArr.length > 0 ? true : false;
    }
  }

  getStreetAddress(event) {
    this.addressDetails.AddressLine1__c = event.target.value;
    if (!event.target.value) {
      this.addressDetails.County__c = "";
      this.addressDetails.State__c = "";
      this.addressDetails.City__c = "";
      this.addressDetails.ZipCode__c = "";
    } else {
      getAPIStreetAddress({
        input: this.addressDetails.AddressLine1__c
      })
        .then(result => {
          this.strStreetAddresPrediction = JSON.parse(result);
          this.active =
            this.strStreetAddresPrediction.predictions.length > 0 ?
              true :
              false;
        })
        .catch(errors => {
          let config = { friendlyErrorMsg: CJAMS_CONSTANTS.ERROR_GOOGLE_API_STREET, errorType: CJAMS_CONSTANTS.TOAST_WARNING }
          return this.dispatchEvent(utils.handleError(errors, config));
        });
    }
  }

  //Function used to select the address
  selectStreetAddress(event) {
    this.addressDetails.AddressLine1__c = event.target.dataset.description;

    getFullDetails({
      placeId: event.target.dataset.id
    })
      .then(result => {
        var main = JSON.parse(result);
        try {
          var cityTextbox = this.template.querySelector(".dummyCity");
          this.addressDetails.City__c = main.result.address_components[2].long_name;

          var countyComboBox = this.template.querySelector(".dummyCounty");
          this.addressDetails.County__c = main.result.address_components[4].long_name;

          var stateComboBox = this.template.querySelector(".dummyState");
          this.addressDetails.State__c = main.result.address_components[5].long_name;

          var ZipCodeTextbox = this.template.querySelector(".dummyZipCode");
          this.addressDetails.ZipCode__c = main.result.address_components[7].long_name;
        } catch (ex) { }
        this.active = false;
      })
      .catch(errors => {
        let config = { friendlyErrorMsg: CJAMS_CONSTANTS.ERROR_GOOGLE_API_STREET, errorType: CJAMS_CONSTANTS.TOAST_WARNING }
        return this.dispatchEvent(utils.handleError(errors, config));
      });
  }

  //Function used for capturing on change states in an array
  stateOnChange(event) {
    let stateSearchTxt = event.target.value;

    if (!stateSearchTxt) this.showStateBox = false;
    else {
      this.filteredStateArr = this.stateFetchResult
        .filter(val => val.Value.indexOf(stateSearchTxt) > -1)
        .map(sresult => {
          return sresult;
        });
      this.showStateBox = this.filteredStateArr.length > 0 ? true : false;
    }
  }

  //All onchange event handler for address related fields
  addressOnChange(event) {
    if (event.target.name == "ShippingStreet") {
      this.addressDetails.AddressLine2__c = event.target.value;
    } else if (event.target.name == "City") {
      this.addressDetails.City__c = event.target.value;
    } else if (event.target.name == "ZipCode") {
      this.addressDetails.ZipCode__c = event.target.value.replace(/(\D+)/g, "");
    }
  }
  //ends here

  /** google address ends */

  handlePhoneChange(evt) {
    evt.target.value = evt.target.value.replace(/(\D+)/g, "");
    this.addressDetails.Phone__c = utils.formattedPhoneNumber(evt.target.value);
  }
  handleChange(event) {
    if (event.target.name == 'Gender__c') {
      this.placementInformation.Gender__c = event.target.value;
    } else if (event.target.name == 'Race__c') {
      this.placementInformation.Race__c = event.target.value;
    } else if (event.target.name == 'LicenseStartdate__c') {
      this.placementInformation.LicenseStartdate__c = event.target.value;
    } else if (event.target.name == 'LicenseEnddate__c') {
      this.placementInformation.LicenseEnddate__c = event.target.value;
    } else if (event.target.name == 'Email__c') {
      this.addressDetails.Email__c = event.target.value;
    } else if (event.target.name == 'RespiteService__c') {
      this.placementInformation.RespiteService__c = event.target.value;
    }
  }

  handleSave(event) {
    this.disableToastMessage = true;
    this.placementInformation.Capacity__c = this._vacancySliderValue;
    if (!this.placementInformation.Gender__c)
      return this.dispatchEvent(utils.toastMessage("Please select gender", "warning"));
    if (!this.placementInformation.Race__c)
      return this.dispatchEvent(utils.toastMessage("Please select race", "warning"));
    if (!this.placementInformation.LicenseStartdate__c)
      return this.dispatchEvent(utils.toastMessage("Please choose Start Date", "warning"));
    if (!this.placementInformation.LicenseEnddate__c)
      return this.dispatchEvent(utils.toastMessage("Please choose End Date", "warning"));
    if (!this.placementInformation.RespiteService__c)
      return this.dispatchEvent(utils.toastMessage("Please choose respite services", "warning"));
    if (!this.addressDetails.AddressLine1__c)
      return this.dispatchEvent(utils.toastMessage("Please enter address line1", "warning"));
    if (!this.addressDetails.City__c)
      return this.dispatchEvent(utils.toastMessage("Please enter city", "warning"));
    if (!this.addressDetails.State__c)
      return this.dispatchEvent(utils.toastMessage("Please enter state", "warning"));
    if (!this.addressDetails.County__c)
      return this.dispatchEvent(utils.toastMessage("Please enter county", "warning"));
    if (!this.addressDetails.ZipCode__c)
      return this.dispatchEvent(utils.toastMessage("Please enter zipcode", "warning"));
    if (!this.addressDetails.Email__c)
      return this.dispatchEvent(utils.toastMessage("Please enter email", "warning"));
    if (!this.addressDetails.Phone__c)
      return this.dispatchEvent(utils.toastMessage("Please enter phone number", "warning"));

    this.placementInformation.sobjectType = 'Reconsideration__c';
    if (!this.placementInformation.MinAge__c) {
      this.placementInformation.MinAge__c = '1';
    }
    this.placementInformation.StartDate__c = this.placementInformation.LicenseStartdate__c;
    this.placementInformation.EndDate__c = this.placementInformation.LicenseEnddate__c;
    this.placementInformation.Id = this.reconsiderationId;
    this.Spinner = true;
    if (Object.keys(this.placementInformation).length > 0 && Object.keys(this.addressDetails).length > 0) {
      InsertUpdateAppPlacement({
        objSobjecttoUpdateOrInsert: this.placementInformation
      })
        .then(result => {
          this.Spinner = false;
          this.handleSuccessMessage(result);
        }).catch(error => {
        });

      this.addressDetails.sobjectType = 'Address__c';
      this.addressDetails.Reconsideration__c = this.reconsiderationId;
      this.addressDetails.Provider__c = this.providerId;
      this.addressDetails.AddressType__c = 'Payment';
      this.addressDetails.Id = this.addressInformationId != undefined ? this.addressInformationId : undefined;
      InsertUpdateAppPlacementAddr({
        objSobjecttoUpdateOrInsert: this.addressDetails
      })
        .then(result => {
          this.Spinner = false;
          this.handleSuccessMessage(result);
        }).catch(errors => {
          let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.TOAST_WARNING }
          return this.dispatchEvent(utils.handleError(errors, config));
        });
    } else {
      this.Spinner = false;
      return this.dispatchEvent(utils.toastMessage("Please fill mandatory details", "warning"));
    }
  }

  handleSuccessMessage(result) {
    if (this.disableToastMessage) {
      this.dispatchEvent(utils.toastMessage("Placement Specification Saved Successfully", "success"));
    } else {
      this.disableToastMessage = false;
      this.dispatchEvent(utils.toastMessage("Please check all details", "warning"));
    }
  }

  fetchDataOnLoad() {
    fetchPlacementInformation({ id: this.reconsiderationId })
      .then(result => {
        if (result.length > 0) {
          this._vacancySliderValue = (result[0].Capacity__c) ? result[0].Capacity__c : '1';
          this.placementInformationId = result[0].Id;
          this._vacancySlider = (this._vacancySliderValue) ? homeSlider + "/Vacancy.html?Vacancy=" + this._vacancySliderValue + "&disableSlider=" + this.isDisabled : homeSlider + "/Vacancy.html?Vacancy=";
          this.placementInformation.MinAge__c = result[0].MinAge__c;
          this.placementInformation.MaxAge__c = result[0].MaxAge__c;
          this.rangeSliderAgeURL = rangeSlider + "/Age.html?MinIQvalue=" + this.placementInformation.MinAge__c + "&MaxIQvalue=" + this.placementInformation.MaxAge__c + "&disableSlider=" + this.isDisabled;
          this.placementInformation.Gender__c = result[0].Gender__c;
          this.placementInformation.Race__c = result[0].Race__c;
          this.placementInformation.LicenseStartdate__c = result[0].StartDate__c;
          this.placementInformation.LicenseEnddate__c = (result[0].EndDate__c) ? result[0].EndDate__c : null;
          this.placementInformation.RespiteService__c = result[0].RespiteService__c;
          this.placementInformation.Program__c = result[0].Program__c;
          this.placementInformation.ProgramType__c = result[0].ProgramType__c;
          this.Spinner = false;
        }
        this.Spinner = false;
      }).catch(errors => {
        return this.dispatchEvent(utils.handleError(errors));
      });

    fetchAddressInformation({ appid: this.reconsiderationId })
      .then(result => {
        if (result.length > 0) {
          this.addressInformationId = result[0].Id;
          this.addressDetails.AddressLine1__c = result[0].AddressLine1__c;
          this.addressDetails.AddressLine2__c = result[0].AddressLine2__c;
          this.addressDetails.City__c = result[0].City__c;
          this.addressDetails.County__c = result[0].County__c;
          this.addressDetails.State__c = result[0].State__c;
          this.addressDetails.ZipCode__c = result[0].ZipCode__c;
          this.addressDetails.Email__c = result[0].Email__c;
          this.addressDetails.Phone__c = result[0].Phone__c;
        }
      }).catch(errors => {
        return this.dispatchEvent(utils.handleError(errors));
      });
  }
}