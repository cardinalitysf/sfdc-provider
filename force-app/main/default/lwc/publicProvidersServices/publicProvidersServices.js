/**
 * @Author        : Jayachandran S ,
 * @CreatedOn     : June 19 ,2020
 * @Purpose       : PublicProvidersServices data table, add, edit, del, list For Services.
 **/

import { LightningElement, track, api, wire } from "lwc";
import getCategoryOptions from "@salesforce/apex/PublicProvidersServices.getCategoryOptions";
import getPotentialOptions from "@salesforce/apex/PublicProvidersServices.getPotentialOptions";
import SaveServiceRecordDetails from "@salesforce/apex/PublicProvidersServices.SaveServiceRecordDetails";
import getServicesdetails from "@salesforce/apex/PublicProvidersServices.getServicesdetails";
import editSerivceDetails from "@salesforce/apex/PublicProvidersServices.editSerivceDetails";
import { deleteRecord } from "lightning/uiRecordApi";
import images from "@salesforce/resourceUrl/images";
import { utils } from "c/utils";
import * as sharedData from 'c/sharedData';
import { getRecord } from "lightning/uiRecordApi";
import RECONSIDERATIONSTATUS_FIELD from "@salesforce/schema/Reconsideration__c.Status__c";
import { CJAMS_CONSTANTS } from "c/constants";

const actions = [
  { label: "Edit", name: "Edit", iconName: "utility:edit", target: "_self" },
  { label: "Delete", name: "Delete", iconName: "utility:delete", target: "_self" },
  { label: "View", name: "View", iconName: "utility:preview", target: "_self" }
];

const columns = [
  { label: "Service ID", fieldName: "Name", type: "text" },
  {
    label: "Potential Services",
    fieldName: "PotentialServices__c",
    type: "text"
  },
  { label: "Category", fieldName: "Category__c", type: "text" },
  {
    label: 'Description',
    fieldName: 'Description__c',
    type: 'button',
    typeAttributes: {
      label: {
        fieldName: 'Description__c'
      },
      name: 'Descpopup',
      title: {
        fieldName: 'Description__c'
      }
    },
    cellAttributes: {
      iconName: 'utility:description',
      iconAlternativeText: 'Description',
      class: {
        fieldName: 'descriptionClass'
      }
    }
  }
];

export default class PublicProvidersServices extends LightningElement {
  _newServiceModal;
  @track columns = columns;
  @track noRecordsFound = false;
  @track serviceCategoryOptions;
  @track Category;
  @track Potential;
  @track potentialValues;
  @track potentialOptions;
  @track description;
  @track isDisablePotential = true;
  @track Name;
  @track ViewRecordForService;
  @track deleteRecordForService;
  @track deleteModel = false;
  @track disabledForView = false;
  @track ViewHide = true;
  @track title;
  @track btnLabel;
  @track descModel = false;

  //pagination
  @track currentPageServiceData;
  @track totalRecordsCount = 0;
  @track page = 1;
  perpage = 10;
  setPagination = 5;
  @track totalRecords;
  @track norecorddisplay = true;
  @track AssignbtnDisable = false;
  @track ReconsiderationStatus;

  serviceIcon = images + "/services-icon.svg";
  descriptionIcon = images + '/description-icon.svg';

  //Sundar Adding this
  @track Spinner = true;
  @track isDisableBtn = false;
  @track disableDelClick = false;


  get ReconsiderationId() {
    return sharedData.getReconsiderationId();
  }

  get getUserProfileName() {
    return sharedData.getUserProfileName();
  }

  @wire(getRecord, {
    recordId: "$ReconsiderationId",
    fields: [RECONSIDERATIONSTATUS_FIELD]
  })
  wireuser({ error, data }) {
    if (error) {
      this.error = error;
    } else if (data) {
      this.ReconsiderationStatus = data.fields.Status__c.value;

      if (data) {
        if (this.getUserProfileName != "Supervisor") {
          if (
            [
              "Submitted",
              "Approved",
              "Rejected"
            ].includes(this.ReconsiderationStatus)
          ) {
            this.AssignbtnDisable = true;
          } else {
            this.AssignbtnDisable = false;
          }
        }
        if (this.getUserProfileName == "Supervisor") {
          if (["Approved", "Rejected"].includes(this.ReconsiderationStatus)) {
            this.AssignbtnDisable = true;
          }
        }
      }
    }
    this.fetchCategoryval();
    this.getServicedata();
  }

  //Initial Data Load For Services
  getServicedata() {
    getServicesdetails({
      Reconsid: this.ReconsiderationId
    })
      .then((result) => {
        this.currentPageServiceData = result.map(i => {
          return {
            Category__c: i.Category__c,
            Description__c: i.Description__c,
            Id: i.Id,
            Name: i.Name,
            PotentialServices__c: i.PotentialServices__c,
            buttonDisabled: (this.AssignbtnDisable) ? true : false,
            descriptionClass: (i.Description__c) ? 'btnbdrIpad blue' : 'svgRemoveicon',
          }
        });
        result = this.currentPageServiceData
        this.totalRecords = result;
        this.totalRecordsCount = this.totalRecords.length;
        if (this.totalRecords.length == 0) {
          this.norecorddisplay = false;
        } else {
          this.norecorddisplay = true;
        }
        this.pageData();
        this.Spinner = false;
      })
      .catch(errors => {
        this.Spinner = false;
        let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
        return this.dispatchEvent(utils.handleError(errors, config));
      });
  }

  //Category PickList Value
  fetchCategoryval() {
    getCategoryOptions({})
      .then((result) => {
        if (result.length > 0) {
          this.serviceCategoryOptions = result.map((item) => {
            return {
              label: item.RefValue__c,
              value: item.RefValue__c
            };
          });
        }
      })
      .catch(errors => {
        let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
        return this.dispatchEvent(utils.handleError(errors, config));
      });
  }

  categoryOnChange(evt) {
    this.Category = evt.target.value;
    this.isDisablePotential = false;
    if (this.Category) {
      this.fetchPotentialval();
    }
  }

  potentialOnChange(evt) {
    this.Potential = evt.target.value;
  }

  descOnChange(evt) {
    this.description = evt.target.value;
  }

  //save and Update func 
  handleSave() {
    if (!this.Category)
      return this.dispatchEvent(
        utils.toastMessage("Category is mandatory", "warning")
      );

    if (!this.Potential)
      return this.dispatchEvent(
        utils.toastMessage("Potential Services is mandatory", "warning")
      );

    this.isDisableBtn = true;

    let myobjcase = {
      sobjectType: "Services__c"
    };
    myobjcase.Category__c = this.Category;
    myobjcase.PotentialServices__c = this.Potential;
    myobjcase.Description__c = this.description;
    myobjcase.Reconsideration__c = this.ReconsiderationId;
    myobjcase.Id = this.serviceRecId;
    SaveServiceRecordDetails({
      objSobjecttoUpdateOrInsert: myobjcase
    })
      .then((result) => {
        this.dispatchEvent(
          utils.toastMessage(
            "Service Details has been updated successfully",
            "success"
          )
        );
        this.closeServiceModal();
        this.isDisableBtn = false;
        this.getServicedata();
      })
      .catch(errors => {
        this.isDisableBtn = false;
        let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
        return this.dispatchEvent(utils.handleError(errors, config));
      });
  }

  //Potential Services PickList Values
  fetchPotentialval() {
    getPotentialOptions({})
      .then((result) => {
        if (result.length > 0) {
          this.potentialValues = result;
          if (this.Category) {
            this.potentialOptions = this.potentialValues
              .filter((row) => row.RefValue__c == this.Category)
              .map((row1) => {
                return {
                  label: row1.RefKey__c,
                  value: row1.RefKey__c
                };
              });
          }
        }
      })
      .catch(errors => {
        let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
        return this.dispatchEvent(utils.handleError(errors, config));
      });
  }

  //openPetModal from Header Button Child comp Through AllTabs Comp
  @api get openServiceModal() {
    return this._newServiceModal;
  }

  set openServiceModal(value) {
    this._newServiceModal = value;
    this.serviceRecId = null;
    this.title = "ADD SERVICES";
    this.btnLabel = "ADD";
    this.disabledForView = false;
    this.descModel = false;
  }

  closeDescModal() {
    this.descModel = false;
  }

  //closemdal dispatch function
  closeServiceModal() {
    this._newServiceModal = false;
    this.dispatchCustomEventToHandleAddServiceInfoPopUp();
  }
  dispatchCustomEventToHandleAddServiceInfoPopUp() {
    const tooglecmps = new CustomEvent("togglecomponents", {
      detail: {
        Component: "CloseServiceInfo"
      }
    });
    this.dispatchEvent(tooglecmps);
    this.Category = "";
    this.Potential = "";
    this.description = "";
    this.isDisablePotential = true;
    this.descModel = false;
  }

  //pagination  function part
  pageData = () => {
    let page = this.page;
    let perpage = this.perpage;
    let startIndex = page * perpage - perpage;
    let endIndex = page * perpage;
    this.currentPageServiceData = this.totalRecords.slice(startIndex, endIndex);
  };
  hanldeProgressValueChange(event) {
    this.page = event.detail;
    this.pageData();
  }

  handleRowAction(event) {
    let serviceid = event.detail.row.Id;
    if (event.detail.action.name == "Edit") {
      this.title = "EDIT SERVICES";
      this.btnLabel = "UPDATE";
      this._newServiceModal = true;
      this.isDisablePotential = false;
      this.disabledForView = false;
      this.ViewHide = true;
      this.fetchPotentialval();
      editSerivceDetails({
        selectedEditId: serviceid
      })
        .then((result) => {
          this.serviceRecId = result[0].Id;
          this.Name = result[0].Name;
          this.Category = result[0].Category__c;
          this.Potential = result[0].PotentialServices__c;
          this.description = result[0].Description__c;
        })
        .catch(errors => {
          let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
          return this.dispatchEvent(utils.handleError(errors, config));
        });
    } else if (event.detail.action.name == "View") {
      this.title = "VIEW SERVICES";
      this.disabledForView = true;
      this.isDisablePotential = true;
      this.ViewHide = false;
      this._newServiceModal = true;
      this.fetchPotentialval();
      editSerivceDetails({
        selectedEditId: serviceid
      })
        .then((result) => {
          this.Name = result[0].Name;
          this.Category = result[0].Category__c;
          this.Potential = result[0].PotentialServices__c;
          this.description = result[0].Description__c;
        })
        .catch(errors => {
          let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
          return this.dispatchEvent(utils.handleError(errors, config));
        });
    } else if (event.detail.action.name == "Delete") {
      this.deleteRecordForService = event.detail.row.Id;
      this.deleteModel = true;
    } else if (event.detail.action.name == "Descpopup") {
      this.descModel = true
      this.description = event.detail.row.Description__c;
    }
  }

  handleDelete() {
    this.disableDelClick = true;
    deleteRecord(this.deleteRecordForService)
      .then(() => {
        this.deleteModel = false;
        this.dispatchEvent(
          utils.toastMessage("Service Details deleted successfully", "success")
        );
        this.getServicedata();
      })
      .catch((error) => {
        this.disableDelClick = false;
        this.deleteModel = false;
        this.dispatchEvent(
          utils.toastMessage("Warning in delete Services details", "Warning")
        );
      });
  }
  closeDeleteModal() {
    this.deleteModel = false;
  }

  /**if decision status = approved show only view otherwise show edit and delete begins */
  constructor() {
    super();
    let ViewIcon = true;
    for (let i = 0; i < this.columns.length; i++) {
      if (this.columns[i].type == 'action') {
        ViewIcon = false;
      }
    }
    if (ViewIcon) {
      this.columns.push(
        {
          type: 'action', typeAttributes: { rowActions: this.getRowActions }, cellAttributes: {
            iconName: 'utility:threedots_vertical',
            iconAlternativeText: 'Actions',
            class: "tripledots"
          }
        }
      )
    }
  }
  getRowActions(row, doneCallback) {
    const actions = [];
    if (row['buttonDisabled'] == true) {
      actions.push({
        'label': 'View',
        'name': 'View',
        'iconName': 'utility:preview',
        'target': '_self'
      });
    } else {
      actions.push({
        'label': 'Edit',
        'name': 'Edit',
        'iconName': 'utility:edit',
        'target': '_self'
      },
        {
          'label': 'Delete',
          'name': 'Delete',
          'iconName': 'utility:delete',
          'target': '_self'
        });
    }
    // simulate a trip to the server
    setTimeout(() => {
      doneCallback(actions);
    }, 200);
  }


}
