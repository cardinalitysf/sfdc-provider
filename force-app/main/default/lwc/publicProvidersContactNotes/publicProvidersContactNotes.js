/**
 * @Author        : Vijayaraj M
 * @CreatedOn     : June 18 , 2020
 * @Purpose       : This component for ContactNotes reusability Js file **/

 import { LightningElement } from 'lwc';
import * as referralsharedDatas from "c/sharedData";
import { CJAMS_CONSTANTS } from "c/constants";

export default class PublicProvidersContactNotes extends LightningElement {
    get reconsiderationId() {
        return referralsharedDatas.getReconsiderationId();
        //return "a0V0w0000006KtLEAU";
      }
    
      get sobjectName() {
        return CJAMS_CONSTANTS.RECONSIDERATION_OBJECT_NAME;
        //return 'Reconsideration__c';
      }
}