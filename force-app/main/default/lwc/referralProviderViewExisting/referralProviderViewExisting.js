import {
  LightningElement,
  wire,
  track
} from "lwc";
import getAccounts from "@salesforce/apex/referralProviderSearchController.getAccounts";
import getProviderStatusOptions from "@salesforce/apex/referralProviderSearchController.getProviderStatusOptions";
import getProviderJuridictionOptions from "@salesforce/apex/referralProviderSearchController.getProviderJuridictionOptions";
import {
  NavigationMixin
} from "lightning/navigation";

import * as refShareProviderId from "c/sharedData";

const actions = [{
  label: "Select",
  name: "show_details"
}];
const columns = [{
  label: "Provider Id",
  fieldName: "ProviderId",
  type: "text"
},
{
  label: "Provider Name",
  fieldName: "ProviderName",
  type: "text"
},
{
  label: "Email",
  fieldName: "Email",
  type: "text"
},
{
  label: "Phone",
  fieldName: "Phone",
  type: "text"
},
{
  label: "Program",
  fieldName: "Program",
  type: "text"
},
{
  label: "Program Type",
  fieldName: "ProgramType",
  type: "text"
},
{
  label: "Tax Id",
  fieldName: "TaxId",
  type: "text"
},
{
  label: "SON?",
  fieldName: "SON",
  type: "text",
  wrapText: true
},
{
  label: "Action",
  fieldName: "AccountId",
  sortable: true,
  type: "button",
  typeAttributes: {
    label: "Select",
    class: "blue",
    target: "_self",
    color: "blue",
    sortable: "false"
  }
  // label: 'Action',
  // type:"button",
  // typeAttributes: {
  //     label: 'View Detail',
  //     name: 'orderNumber',
  //     title: 'orderNumber',
  //     disabled: false,
  //     value: 'orderNumber',
  //     iconPosition: 'left',
  //     sortable: "false"
  // }
}
];

export default class referralProviderSearchController extends LightningElement {
  @track data = [];
  @track columns = columns;
  @track record = {};
  @track showSpinner = false;

  @track sf_ProviderId;
  @track sf_ProviderTaxId;
  @track sf_SelectStatus;
  @track sf_SelectJurisdiction;

  //@track options_SelectStatus;
  // @track options_SelectJurisdiction;

  @track page = 1;
  perpage = 5;
  @track pages = [];
  set_size = 5;

  pageData = () => {
    let page = this.page;
    let perpage = this.perpage;
    let startIndex = page * perpage - perpage;
    let endIndex = page * perpage;
    return this.data.slice(startIndex, endIndex);
  };
  setPages = data => {
    let numberOfPages = Math.ceil(data.length / this.perpage);
    for (let index = 1; index <= numberOfPages; index++) {
      this.pages.push(index);
    }
  };
  get hasPrev() {
    return this.page > 1;
  }
  get hasNext() {
    return this.page < this.pages.length;
  }
  onNext = () => {
    ++this.page;
  };
  onPrev = () => {
    --this.page;
  };
  onPageClick = e => {
    this.page = parseInt(e.target.dataset.id, 10);
  };
  get currentPageData() {
    return this.pageData();
  }

  /*async connectedCallback() { 
        this.setDefaultValues();
        this.getAccountInformation();
    }*/

  renderedCallback() {
    this.renderButtons();
  }
  renderButtons = () => {
    this.template.querySelectorAll("button").forEach(but => {
      but.style.backgroundColor =
        this.page === parseInt(but.dataset.id, 10) ? "dodgerblue" : "white";
      but.style.color =
        this.page === parseInt(but.dataset.id, 10) ? "white" : "black";
    });
  };
  get pagesList() {
    let mid = Math.floor(this.set_size / 2) + 1;
    if (this.page > mid) {
      return this.pages.slice(this.page - mid, this.page + mid - 1);
    }
    return this.pages.slice(0, this.set_size);
  }
  async connectedCallback() {
    //get select status
    getProviderStatusOptions({})
      .then(result => {
        if (result == null) {
          //do nothing
        } else {
          this.options_SelectStatus = JSON.parse(result);
        }
      })
      .catch(error => {
        this.error = error;
      });

    //get juridictions

    getProviderJuridictionOptions({})
      .then(result => {
        if (result == null) {
          //do nothing
        } else {
          var Juridiction = JSON.parse(result);
          var JuridictionOptions = [];
          if (Juridiction.length > 0) {
            for (var i = 0; i < Juridiction.length; i++) {
              var cell = {
                value: Juridiction[i].Id,
                label: Juridiction[i].RefValue__c
              };
              JuridictionOptions.push(cell);
            }
          }
          this.options_SelectJurisdiction = JuridictionOptions;
        }
      })
      .catch(error => {
        this.error = error;
      });

    this.setDefaultValues();
    this.getAccountInformation();
  }

  setDefaultValues() {
    this.sf_ProviderId = null;
    this.sf_ProviderTaxId = null;
    this.sf_SelectStatus = "All";
    this.sf_SelectJurisdiction = null;
    this.page = 1;
    this.perpage = 5;
    this.pages = [];
    this.set_size = 5;
  }
  getAccountInformation() {
    this.showSpinner = true;
    getAccounts({
      providerId: this.sf_ProviderId,
      providerTaxId: this.sf_ProviderTaxId,
      status: this.sf_SelectStatus,
      jurisdiction: this.sf_SelectJurisdiction
    })
      .then(result => {
        this.showSpinner = false;
        if (result == null) {
          this.data = [];
          this.setPages(this.data);
        } else {
          this.data = result;
          this.setPages(this.data);
        }
      })
      .catch(error => {
        this.error = error;
        this.contacts = undefined;
      });
  }

  handleRowAction(event) {
    refShareProviderId.setProviderId(event.detail.row.AccountId);
    const row = event.detail.row;
    const onproviderid = new CustomEvent("providerid", {
      detail: {
        first: false,
        second: this.providerTypeViewOrExisting === "option1" ? true : false,
        providerid: event.detail.row.AccountId,
        pagetype: "provider"
      }
    });
    this.dispatchEvent(onproviderid);
  }

  handleSearchClick() {
    this.page = 1;
    this.perpage = 5;
    this.pages = [];
    this.set_size = 5;

    this.getAccountInformation();
  }

  showRowDetails(row) {
    this.record = row;
  }
  handleSelectStatus(evt) {
    this.sf_SelectStatus = evt.detail.value;
  }
  handleSelectJurisdiction(evt) {
    this.sf_SelectJurisdiction = evt.detail.value;
  }
  handleProviderId(evt) {
    this.sf_ProviderId = evt.detail.value;
  }
  handleProviderTaxId(evt) {
    this.sf_ProviderTaxId = evt.detail.value;
  }
  handleResetClick(evt) {
    this.setDefaultValues();
    this.getAccountInformation();
  }
}