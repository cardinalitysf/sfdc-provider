/**
 * @Author        : Pratheeba V
 * @CreatedOn     : June 5, 2020
 * @Purpose       : Public Application Placement tab 
 **/
import { LightningElement, track, wire, api } from 'lwc';
import homeSlider from "@salesforce/resourceUrl/HomeSlider";
import images from "@salesforce/resourceUrl/images";
import getProgramDetails from '@salesforce/apex/PublicApplicationPlacement.getProgramDetails';
import getPickListValues from '@salesforce/apex/PublicApplicationPlacement.getPickListValues';
import InsertUpdateAppPlacement from '@salesforce/apex/PublicApplicationPlacement.InsertUpdateAppPlacement';
import InsertUpdateAppPlacementAddr from '@salesforce/apex/PublicApplicationPlacement.InsertUpdateAppPlacementAddr';
import fetchPlacementInformation from "@salesforce/apex/PublicApplicationPlacement.fetchPlacementInformation";
import fetchAddressInformation from "@salesforce/apex/PublicApplicationPlacement.fetchAddressInformation";
import APPLICATIONSTATUS_FIELD from '@salesforce/schema/Application__c.Status__c';
import {
  utils
} from "c/utils";
import * as sharedData from "c/sharedData";
import {
  getRecord
} from "lightning/uiRecordApi";
import getFullDetails from "@salesforce/apex/GetStreetAddressFromGoogleAPI.getPlaceDetails";
import getAPIStreetAddress from "@salesforce/apex/GetStreetAddressFromGoogleAPI.getSuggestions";
import getAllStates from "@salesforce/apex/ReferralProviderAddOrEdit.getStateDetails";
import rangeSlider from "@salesforce/resourceUrl/RangeSlider";
import { CJAMS_CONSTANTS } from 'c/constants';
export default class PublicApplicationPlacement extends LightningElement {
  @track Program__c;
  @track ProgramType__c;
  @track Gender;
  @track GenderOptions;
  @track Race;
  @track selectedCounty;
  @track strStreetAddress;
  @track strStreetAddresPrediction = [];
  @track filteredCountyArr = [];
  @track selectedState;
  @track selectedCity;
  @track selectedZipCode;
  @track selectedCounty;
  @track ShippingStreet;
  @track placementInformation = {};
  @track addressDetails = {};
  @track isDisabled = false;
  @track btnLabel;
  @track Status;
  rangeSliderAgeURL = rangeSlider + "/Age.html";

  homeInformationIcon = images + "/home-information.svg";
  privateIcon = images + "/office-building.svg";
  existingIcon = images + "/clipboard.svg";
  plusIcon = images + "/plus-new-referal.svg";
  publicIcon = images + "/houseIcon.svg";

  _vacancySliderValue;
  _vacancySlider = homeSlider + "/Vacancy.html";

  get vacancySlider() {
    return this._vacancySlider;
  }

  get applicationid() {
    return sharedData.getApplicationId();
  }

  get providerId() {
    return sharedData.getProviderId();
  }

  //Get Slider Age URL
  get sliderAgeURL() {
    return this.rangeSliderAgeURL;
  }

  get getUserProfileName() {
    return sharedData.getUserProfileName();
  }

  @wire(getRecord, {
    recordId: "$applicationid",
    fields: [APPLICATIONSTATUS_FIELD]
  })
  wireuser({
    error,
    data
  }) {
    if (error) {
      this.error = error;
    } else if (data) {
      this.Status = data.fields.Status__c.value;
      if (this.Status) {
        if (this.getUserProfileName != 'Supervisor') {
          if (['Caseworker Submitted', 'Approved', 'Rejected', 'Supervisor Rejected'].includes(this.Status)) {
            this.isDisabled = true;
          }
          else {
            this.isDisabled = false;
          }
        }
        if (this.getUserProfileName == 'Supervisor') {
          this.isDisabled = true;
        }
      }
    }
  }

  //store provider id and application id in address table against application
  @wire(getPickListValues, {
    objInfo: {
      'sobjectType': 'Application__c'
    },
    picklistFieldApi: 'Gender__c'
  })
  wireCheckPicklistGender({
    error,
    data
  }) {
    if (data) {
      this.GenderOptions = data;
    }
  }

  @wire(getPickListValues, {
    objInfo: {
      'sobjectType': 'Application__c'
    },
    picklistFieldApi: 'Race__c'
  })
  wireCheckPicklistRace({
    error,
    data
  }) {
    if (data) {
      this.RaceOptions = data;
    }
  }

  get streetAddressLength() {
    return this.strStreetAddresPrediction != "";
  }
  get strStreetAddresPredictionUI() {
    return this.strStreetAddresPrediction;
  }
  get strStreetAddresPredictionLength() {
    if (this.strStreetAddress)
      return this.strStreetAddresPrediction.predictions ? true : false;
  }

  get radioOptions() {
    return [
      {
        label: "Yes",
        value: "Yes"
      },
      {
        label: "No",
        value: "No"
      }
    ];
  }

  connectedCallback() {
    window.addEventListener("message", event => {
      let iframevalue = event.data.split(":");
      switch (iframevalue[0]) {
        case "Vacancy":
          this._vacancySliderValue = iframevalue[1];
          break;
        case "MinAge":
          this.placementInformation.MinAge__c = iframevalue[1];
          break;
        case "MaxAge":
          this.placementInformation.MaxAge__c = iframevalue[1];
          break;
        default:
        // code block
      }
    });
    getProgramDetails({
      applicationid: this.applicationid
    })
      .then(result => {
        if (result.length == 0) {
          this.Program__c = null;
          this.ProgramType__c = null;
        } else {
          this.Program__c = result[0].Program__c;
          this.ProgramType__c = result[0].ProgramType__c;
        }
      }).catch(error => {
        return this.dispatchEvent(utils.handleError(errors));
      });
  }
  /** Google api code begins here*/
  get streetAddressLength() {
    return this.strStreetAddresPrediction != "";
  }
  get strStreetAddresPredictionUI() {
    return this.strStreetAddresPrediction;
  }
  get strStreetAddresPredictionLength() {
    if (this.addressDetails.AddressLine1__c)
      return this.strStreetAddresPrediction.predictions ? true : false;
  }

  //Method used to show and hide the drop-down of suggestions
  get tabClass() {
    return this.active ?
      "slds-dropdown slds-dropdown_length-5 slds-dropdown_fluid slds-show" :
      "slds-dropdown slds-dropdown_length-5 slds-dropdown_fluid slds-hide";
  }


  //Function used to fetch all states from Address Object when Screen Loads
  @wire(getAllStates)
  getAllStates({
    error,
    data
  }) {
    if (data) {
      if (data.length > 0) {
        let stateData = data.map(row => {
          return {
            Id: row.Id,
            Value: row.RefValue__c
          };
        });
        this.stateFetchResult = stateData;
      } else {
        this.dispatchEvent(utils.toastMessage("No states found", "error"));
      }
    } else if (error) {
      this.dispatchEvent(
        utils.toastMessage("Error in Fetching states", "error")
      );
    }
  }

  //Function used to set selected state
  setSelectedCounty(event) {
    this.addressDetails.County__c = event.currentTarget.dataset.value;
    this.showCountyBox = false;
  }

  //Function used to set selected state
  setSelectedState(event) {
    this.addressDetails.State__c = event.currentTarget.dataset.value;
    this.showStateBox = false;
  }

  //Function used to capture on change county in an array
  countyOnChange(event) {
    let countySearchTxt = event.target.value;

    if (!countySearchTxt) this.showCountyBox = false;
    else {
      this.filteredCountyArr = this.countyArr
        .filter(val => val.value.indexOf(countySearchTxt) > -1)
        .map(cresult => {
          return cresult;
        });
      this.showCountyBox = this.filteredCountyArr.length > 0 ? true : false;
    }
  }

  getStreetAddress(event) {
    this.addressDetails.AddressLine1__c = event.target.value;
    if (!event.target.value) {
      this.addressDetails.County__c = "";
      this.addressDetails.State__c = "";
      this.addressDetails.City__c = "";
      this.addressDetails.ZipCode__c = "";
    } else {
      getAPIStreetAddress({
        input: this.addressDetails.AddressLine1__c
      })
        .then(result => {
          this.strStreetAddresPrediction = JSON.parse(result);
          this.active =
            this.strStreetAddresPrediction.predictions.length > 0 ?
              true :
              false;
        })
        .catch(errors => {
          let config = { friendlyErrorMsg: CJAMS_CONSTANTS.ERROR_GOOGLE_API_SELECT_ADDR, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
          return this.dispatchEvent(utils.handleError(errors, config));
        });
    }
  }

  //Function used to select the address
  selectStreetAddress(event) {
    this.addressDetails.AddressLine1__c = event.target.dataset.description;

    getFullDetails({
      placeId: event.target.dataset.id
    })
      .then(result => {
        var main = JSON.parse(result);
        try {
          var cityTextbox = this.template.querySelector(".dummyCity");
          this.addressDetails.City__c = main.result.address_components[2].long_name;

          var countyComboBox = this.template.querySelector(".dummyCounty");
          this.addressDetails.County__c = main.result.address_components[4].long_name;

          var stateComboBox = this.template.querySelector(".dummyState");
          this.addressDetails.State__c = main.result.address_components[5].long_name;

          var ZipCodeTextbox = this.template.querySelector(".dummyZipCode");
          this.addressDetails.ZipCode__c = main.result.address_components[7].long_name;
        } catch (ex) { }
        this.active = false;
      })
      .error(error => {
        this.dispatchEvent(
          utils.toastMessage("Error in Selected Address", "error")
        );
      });
  }

  //Function used for capturing on change states in an array
  stateOnChange(event) {
    let stateSearchTxt = event.target.value;

    if (!stateSearchTxt) this.showStateBox = false;
    else {
      this.filteredStateArr = this.stateFetchResult
        .filter(val => val.Value.indexOf(stateSearchTxt) > -1)
        .map(sresult => {
          return sresult;
        });
      this.showStateBox = this.filteredStateArr.length > 0 ? true : false;
    }
  }

  //All onchange event handler for address related fields
  addressOnChange(event) {
    if (event.target.name == "ShippingStreet") {
      this.addressDetails.AddressLine2__c = event.target.value;
    } else if (event.target.name == "City") {
      this.addressDetails.City__c = event.target.value;
    } else if (event.target.name == "ZipCode") {
      this.addressDetails.ZipCode__c = event.target.value.replace(/(\D+)/g, "");
    }
  }
  //ends here

  /** google address ends */

  handlePhoneChange(evt) {
    evt.target.value = evt.target.value.replace(/(\D+)/g, "");
    this.addressDetails.Phone__c = utils.formattedPhoneNumber(evt.target.value);
  }
  handleChange(event) {
    /*if (event.target.name == 'MinAge__c') {
      this.placementInformation.MinAge__c = event.target.value;
    } else if (event.target.name == 'MaxAge__c') {
      this.placementInformation.MaxAge__c = event.target.value;
    } else*/ if (event.target.name == 'Gender__c') {
      this.placementInformation.Gender__c = event.target.value;
    } else if (event.target.name == 'Race__c') {
      this.placementInformation.Race__c = event.target.value;
    } else if (event.target.name == 'LicenseStartdate__c') {
      this.placementInformation.LicenseStartdate__c = event.target.value;
    } else if (event.target.name == 'LicenseEnddate__c') {
      this.placementInformation.LicenseEnddate__c = event.target.value;
    } else if (event.target.name == 'Email__c') {
      this.addressDetails.Email__c = event.target.value;
    } else if (event.target.name == 'RespiteServices__c') {
      this.placementInformation.RespiteServices__c = event.target.value;
    }
  }

  handleSave(event) {
    this.disableToastMessage = true;
    this.placementInformation.Capacity__c = this._vacancySliderValue;
    //if (!this.placementInformation.MinAge__c)
    //return this.dispatchEvent(utils.toastMessage("Please select min age", "warning"));
    //if (!this.placementInformation.MaxAge__c)
    //return this.dispatchEvent(utils.toastMessage("Please select max age", "warning"));
    if (!this.placementInformation.Gender__c)
      return this.dispatchEvent(utils.toastMessage("Please select gender", "warning"));
    if (!this.placementInformation.Race__c)
      return this.dispatchEvent(utils.toastMessage("Please select race", "warning"));
    if (!this.placementInformation.LicenseStartdate__c)
      return this.dispatchEvent(utils.toastMessage("Please choose Start Date", "warning"));
    if (!this.placementInformation.LicenseEnddate__c)
      return this.dispatchEvent(utils.toastMessage("Please choose End Date", "warning"));
    if (!this.placementInformation.RespiteServices__c)
      return this.dispatchEvent(utils.toastMessage("Please choose respite services", "warning"));
    if (!this.addressDetails.AddressLine1__c)
      return this.dispatchEvent(utils.toastMessage("Please enter address line1", "warning"));
    if (!this.addressDetails.City__c)
      return this.dispatchEvent(utils.toastMessage("Please enter city", "warning"));
    if (!this.addressDetails.State__c)
      return this.dispatchEvent(utils.toastMessage("Please enter state", "warning"));
    if (!this.addressDetails.County__c)
      return this.dispatchEvent(utils.toastMessage("Please enter county", "warning"));
    if (!this.addressDetails.ZipCode__c)
      return this.dispatchEvent(utils.toastMessage("Please enter zipcode", "warning"));
    if (!this.addressDetails.Email__c)
      return this.dispatchEvent(utils.toastMessage("Please enter email", "warning"));
    if (!this.addressDetails.Phone__c)
      return this.dispatchEvent(utils.toastMessage("Please enter phone number", "warning"));

    this.placementInformation.sobjectType = 'Application__c';
    if (!this.placementInformation.MinAge__c) {
      this.placementInformation.MinAge__c = '1';
    }
    this.placementInformation.Id = this.placementInformationId;

    this.Spinner = true;
    if (Object.keys(this.placementInformation).length > 0 && Object.keys(this.addressDetails).length > 0) {
      InsertUpdateAppPlacement({
        objSobjecttoUpdateOrInsert: this.placementInformation
      })
        .then(result => {
          this.Spinner = false;
          this.handleSuccessMessage(result);
        }).catch(errors => {
          let config = { friendlyErrorMsg: CJAMS_CONSTANTS.APPLICATION_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
          return this.dispatchEvent(utils.handleError(errors, config));
        });

      this.addressDetails.sobjectType = 'Address__c';
      this.addressDetails.Application__c = this.applicationid;
      this.addressDetails.Provider__c = this.providerId;
      this.addressDetails.AddressType__c = 'Payment';
      this.addressDetails.Id = this.addressInformationId != undefined ? this.addressInformationId : undefined;
      InsertUpdateAppPlacementAddr({
        objSobjecttoUpdateOrInsert: this.addressDetails
      })
        .then(result => {
          this.Spinner = false;
          this.handleSuccessMessage(result);
        }).catch(errors => {
          let config = { friendlyErrorMsg: CJAMS_CONSTANTS.APPLICATION_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
          return this.dispatchEvent(utils.handleError(errors, config));
        });
    } else {
      this.Spinner = false;
      return this.dispatchEvent(utils.toastMessage("Please fill mandatory details", "warning"));
    }
  }

  handleSuccessMessage(result) {
    if (this.disableToastMessage) {
      this.dispatchEvent(utils.toastMessage("Placement Specification Saved Successfully", "success"));
    } else {
      this.disableToastMessage = false;
      this.dispatchEvent(utils.toastMessage("Please check all details", "warning"));
    }
  }

  @wire(fetchPlacementInformation, {
    id: '$applicationid'
  })
  fettchDetails(data) {
    if (data.data != undefined && data.data.length > 0) {
      this._vacancySliderValue = (data.data[0].Capacity__c) ? data.data[0].Capacity__c : '1';
      this.placementInformationId = data.data[0].Id;
      this._vacancySlider = (this._vacancySliderValue) ? homeSlider + "/Vacancy.html?Vacancy=" + this._vacancySliderValue + "&disableSlider=" + this.isDisabled : homeSlider + "/Vacancy.html?Vacancy=";
      this.placementInformation.MinAge__c = data.data[0].MinAge__c;
      this.placementInformation.MaxAge__c = data.data[0].MaxAge__c;
      this.rangeSliderAgeURL = rangeSlider + "/Age.html?MinIQvalue=" + this.placementInformation.MinAge__c + "&MaxIQvalue=" + this.placementInformation.MaxAge__c + "&disableSlider=" + this.isDisabled;
      this.placementInformation.Gender__c = data.data[0].Gender__c;
      this.placementInformation.Race__c = data.data[0].Race__c;
      this.placementInformation.LicenseStartdate__c = data.data[0].LicenseStartdate__c;
      this.placementInformation.LicenseEnddate__c = data.data[0].LicenseEnddate__c;
      this.placementInformation.RespiteServices__c = data.data[0].RespiteServices__c;
      this.Spinner = false;
    }
    this.btnLabel = (this.placementInformation.Gender__c) ? 'Update' : 'Save';
    this.Spinner = false;
  }

  @wire(fetchAddressInformation, {
    id: '$providerId',
    appid: '$applicationid'
  })
  fetchDetails(data) {
    if (data.data != undefined && data.data.length > 0) {
      this.addressInformationId = data.data[0].Id;
      this.addressDetails.AddressLine1__c = data.data[0].AddressLine1__c;
      this.addressDetails.AddressLine2__c = data.data[0].AddressLine2__c;
      this.addressDetails.City__c = data.data[0].City__c;
      this.addressDetails.County__c = data.data[0].County__c;
      this.addressDetails.State__c = data.data[0].State__c;
      this.addressDetails.ZipCode__c = data.data[0].ZipCode__c;
      this.addressDetails.Email__c = data.data[0].Email__c;
      this.addressDetails.Phone__c = data.data[0].Phone__c;
    }
  }
}