/**
    * @Author        : K.Sundar
    * @CreatedOn     : AUGUST 04, 2020
    * @Purpose       : This component contains Clearance/Medical for CPA Household Members
**/

import { LightningElement, api, wire, track } from "lwc";

import getCPAHomeHouseHoldMembersDetails from "@salesforce/apex/CpaClearanceMedicalInfo.getCPAHomeHouseHoldMembersDetails";
import getCPARecordDetails from "@salesforce/apex/CpaClearanceMedicalInfo.getCPARecordDetails";
import bulkUpdateRecordAns from "@salesforce/apex/CpaClearanceMedicalInfo.bulkUpdateRecordAns";

import { deleteRecord } from "lightning/uiRecordApi";
import { CJAMS_CONSTANTS, TOAST_HEADER_LABELS } from "c/constants";
import { utils } from "c/utils";
import { refreshApex } from '@salesforce/apex';
import { NavigationMixin } from 'lightning/navigation';

export default class CpaClearanceMedicalInfo extends NavigationMixin(LightningElement) {
    cpaHouseHoldMemberDetails = {};
    Spinner = true;
    toggledivForHouseholdMember = false;
    dropdownSelectedValue = "";
    dropdownSelectedValueStatus = "";
    dropdownSelectedValueStatusColor = "";
    selectHouseholdMemberDetails;
    selectHouseholdMemberOptions = [];
    selectedId;
    actorId;
    allHouseholdMembersChecklistAnswers;
    _cpaHomeId;
    error;
    questions = [];
    cpaAnswerCheckList = [];

    @track wiredActorDetails;
    @track cpaHomeIDforApiCall='';

    @track selectedDocumentId;

    @api
    get cpaHomeId() {
        return this._cpaHomeId;
    }

    set cpaHomeId(value) {
        if (value) {
            this._cpaHomeId = value;
            this.cpaHomeIDforApiCall = value;
        }
    }

    //Check Box options
    get options() {
        return [
            { label: "Yes", value: "Yes" },
            { label: "No", value: "No" }
        ];
    }

    //Wire service method to fetch All House hold members against CPA Home ID
    @wire(getCPAHomeHouseHoldMembersDetails, {
        cpaHomeId: '$cpaHomeIDforApiCall'
    })
    allActorDetails(result) {
        this.wiredActorDetails = result;

        if (result.data) {
            if (result.data.length == 0) {
                this.Spinner = false;
                this.handleRedirectionToCPAHomeDashboard();
                return this.dispatchEvent(utils.toastMessage("Please Add House hold members to proceed further", TOAST_HEADER_LABELS.TOAST_WARNING));
            }

            let selectHouseHoldMembers = [];
            this.allHouseholdMembersChecklistAnswers = result.data;
    
            result.data.forEach(row => {
                selectHouseHoldMembers.push({
                    id: row.Id,
                    label: row.Contact__r.FirstName__c + ' ' + row.Contact__r.LastName__c,
                    value: row.Contact__r.FirstName__c + ' ' + row.Contact__r.LastName__c,
                    status: !row.ClearanceStatus__c ? 'Pending' : row.ClearanceStatus__c,
                    statusColor: row.ClearanceStatus__c == 'Completed' ? 'house-hold-status-greencolor' : 'house-hold-status-orangecolor',
                    ProgramType__c: row.CPAHomes__r.ProgramType__c,
                    Role__c: row.Role__c,
                    ApproximateAge__c: row.Contact__r.ApproximateAge__c,
                    Gender__c: row.Contact__r.Gender__c
                });
            });

            this.selectHouseholdMemberOptions = selectHouseHoldMembers;
            this.selectedId = this.selectHouseholdMemberOptions[0].id;
            this.cpaHouseHoldMemberDetails.ProgramType__c = this.selectHouseholdMemberOptions[0].ProgramType__c;
            this.cpaHouseHoldMemberDetails.Role__c = this.selectHouseholdMemberOptions[0].Role__c;
            this.cpaHouseHoldMemberDetails.ApproximateAge__c = this.selectHouseholdMemberOptions[0].ApproximateAge__c;
            this.cpaHouseHoldMemberDetails.Gender__c = this.selectHouseholdMemberOptions[0].Gender__c;
            this.dropdownSelectedValue = this.selectHouseholdMemberOptions[0].value;
            this.dropdownSelectedValueStatus = this.selectHouseholdMemberOptions[0].status;
            this.dropdownSelectedValueStatusColor = this.selectHouseholdMemberOptions[0].statusColor;
            this.actorId = result.data[0].Id;

            this.handleCPARecordDetails();

            refreshApex(this.wiredActorDetails);
        }
    }

    //This method used to fetch Questions and Answers against Selected Actor ID
    handleCPARecordDetails() {
        getCPARecordDetails({
            cpaHomeId: this.cpaHomeIDforApiCall,
            stractorId: this.actorId
        })
        .then(data => {
            if (data.length > 0) {
                let recordQuestionAnswers = data;

                if (recordQuestionAnswers.length > 0) {
                    this.questions = [];
                    let id = 1;
    
                    try {
                        recordQuestionAnswers.forEach(row => {
                            this.questions.push({
                                rowId: id,
                                chk1Id: 'c1' + id,
                                chk2Id: 'c2' + id,
                                providerAnswerId: row.providerAnswerId,
                                actorId: row.actorId,
                                referenceValueObjId: row.referenceValueObjId,
                                providerRecordQuestionId: row.providerRecordQuestionId,
                                questionOrder: row.questionOrder,
                                questionHeading: row.questionHeading,
                                questionType: row.questionType,
                                requestDate: !row.requestDate ? utils.formatDateYYYYMMDD(new Date()) : row.requestDate,
                                resultDate: row.questionHeading == 'Clearance' ? row.resultDate : null,
                                subQuestion1: row.subQuestion1,
                                subQuestion1Answer: row.subQuestion1Answer ? row.subQuestion1Answer : null,
                                subQuestion2: row.questionHeading == 'Clearance' ? row.subQuestion2 : null,
                                subQuestion2Answer: row.questionHeading == 'Clearance' ? row.subQuestion2Answer : null,
                                resultDateFlag: row.questionHeading == 'Clearance' ? true : false,
                                subQuestion2Flag: row.questionHeading == 'Clearance' ? true : false,
                                disableRequestDate: false,
                                disableResultDate: false,
                                minDate: row.questionHeading == 'Clearance' ? (!row.requestDate ? utils.formatDateYYYYMMDD(new Date()) : row.requestDate) : null,
                                uploadedDocs: row.uploadedDocs
                            });
                            id++;
                        });
                        this.Spinner = false;
                    } catch (error) {
                        this.Spinner = false;
                        this.error = error;
                    }
                } else {
                    this.Spinner = false;
                    this.handleRedirectionToCPAHomeDashboard();
                    return this.dispatchEvent(utils.toastMessage("Please Add House hold members to proceed further", TOAST_HEADER_LABELS.TOAST_WARNING));
                }
            } else {
                this.Spinner = false;
                this.handleRedirectionToCPAHomeDashboard();
                return this.dispatchEvent(utils.toastMessage("Please Add House hold members to proceed further", TOAST_HEADER_LABELS.TOAST_WARNING));
            }
        })
        .catch(error => {
            this.error = error;
            this.Spinner = false;
        })
    }

    //This method used to refresh the screen after the Document has been uploaded
    handleRefresh(event) {
        if (event.detail.first === true) {
            this.Spinner = true;
            setTimeout(() => {
                this.handleCPARecordDetails();
            }, 1000);
        }
    }

    //This method used to show the House hold members dropdown list
    handleHouseholdMember() {
      this.toggledivForHouseholdMember = true;
    }

    //This method used to hide the House hold members dropdown list
    handleMouseOutButtonHouseholdMember() {
        this.toggledivForHouseholdMember = false;
    }

    //This method used to display onchange Selected House hold members details
    selectHouseholdMember(event) {
        this.selectedId = event.target != undefined ? event.target.id : this.selectedId;

        this.selectHouseholdMemberOptions.forEach((item) => {
            if (this.selectedId.includes(item.id)) {
                this.actorId = item.id;
                this.dropdownSelectedValue = item.value;
                this.dropdownSelectedValueStatus = item.status;
                this.dropdownSelectedValueStatusColor = item.status == 'Completed' ? 'house-hold-status-greencolor' : 'house-hold-status-orangecolor';
                this.cpaHouseHoldMemberDetails.ProgramType__c = item.ProgramType__c;
                this.cpaHouseHoldMemberDetails.Role__c = item.Role__c;
                this.cpaHouseHoldMemberDetails.ApproximateAge__c = item.ApproximateAge__c;
                this.cpaHouseHoldMemberDetails.Gender__c = item.Gender__c;
                this.dropDownValueColorChange();
            }
        });

        this.toggledivForHouseholdMember = false;
        this.Spinner = true;
        this.handleCPARecordDetails();
    }

    //This method used to display label colors while onchanging household member details
    dropDownValueColorChange() {
        if (this.allHouseholdMembersChecklistAnswers != undefined) {
            const answerArray = Object.values(
                this.allHouseholdMembersChecklistAnswers.reduce((a, c) => {
                    a[c.id] = c;
                    return a
                }, {}));

            this.selectHouseholdMemberOptions.forEach((item) => {
                let flag = false;
                answerArray.forEach((key) => {
                    if (key.Id == item.id) {
                        flag = true;
                    }
                });

                if (flag) {
                    item.statusColor = 'house-hold-status-greencolor';
                    item.status = 'Completed';
                } else {
                    item.statusColor = 'house-hold-status-orangecolor';
                    item.status = 'Pending';
                }
            });
        } else {
            this.selectHouseholdMemberOptions.forEach((item) => {
                this.dropdownSelectedValueStatus = 'Pending';
                this.dropdownSelectedValueStatusColor = 'house-hold-status-orangecolor';
                item.statusColor = 'house-hold-status-orangecolor';
                item.status = 'Pending';
            });
        }
    }

    //Request Date onchange method
    requestDateOnChange(event) {
        if (!event.target.value) {
            this.questions.find(v => v.rowId == event.target.name).requestDate = null;
            this.questions.find(v => v.rowId == event.target.name).disableResultDate = true;
            this.questions.find(v => v.rowId == event.target.name).resultDate = null;
            this.questions.find(v => v.rowId == event.target.name).minDate = null;
        } else {
            this.questions.find(v => v.rowId == event.target.name).requestDate = event.target.value;
            this.questions.find(v => v.rowId == event.target.name).minDate = event.target.value;
            this.questions.find(v => v.rowId == event.target.name).resultDate = null;
            this.questions.find(v => v.rowId == event.target.name).disableResultDate = false;
        }
    }

    //Result Date onchange method
    resultDateOnChange(event) {
        if (!event.target.value)
            this.questions.find(v => v.rowId == event.target.name).resultDate = null;
        else
            this.questions.find(v => v.rowId == event.target.name).resultDate = event.target.value;
    }

    //SubQuestion1 Checkbox onchange method
    subQuestion1OnChange(event) {
        this.questions.find(v => v.chk1Id == event.target.name).subQuestion1Answer = event.target.value;
    }

    //SubQuestion2 Checkbox onchange method
    subQuestion2OnChange(event) {
        this.questions.find(v => v.chk2Id == event.target.name).subQuestion2Answer = event.target.value;
    }

    //This method used to delete selected Record Id
    deleteFileupload(event) {
        this.Spinner = true;

        deleteRecord(event.target.dataset.id)
        .then(result => {
            this.Spinner = false;
            this.dispatchEvent(utils.toastMessage("Record has been deleted successfully", TOAST_HEADER_LABELS.TOAST_SUCCESS));
            this.handleCPARecordDetails();
        })
        .catch(errors => {
            this.Spinner = false;

            if (errors) {
                let error = JSON.parse(errors.body.message);
                const { title, message, errorType } = error;
                this.dispatchEvent(utils.toastMessageWithTitle(title, message, errorType));
            } else {
                this.dispatchEvent(utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, TOAST_HEADER_LABELS.TOAST_WARNING));
            }
        });
    }

    //This method used to go back to CPA Home Dashboard screen
    handleCancel() {
        this.Spinner = true;

        setTimeout(() => {
            this.cpaHouseHoldMemberDetails = {};
            this.dropdownSelectedValue = null;
            this.dropdownSelectedValueStatus = null;
            this.selectHouseholdMemberOptions = [];
            this.questions = [];
            this.handleRedirectionToCPAHomeDashboard();
        }, 1000)
    }

    //This method used to update QA details into CPA Home actors
    handleSave() {
        if (!this.selectedId)
            return this.dispatchEvent(utils.toastMessage('Please select Household Member', TOAST_HEADER_LABELS.TOAST_WARNING));

        let dateNotFound = false;
        let dateMismatchFlag = false;

        for (var i = 0; i < this.questions.length; i++) {
            if (this.questions[i].requestDate == null) {
                dateNotFound = true;
                break;
            }

            if (this.questions[i].resultDateFlag === true && this.questions[i].resultDate == null) {
                dateNotFound = true;
                break;
            }

            if (this.questions[i].resultDateFlag === true && (new Date(this.questions[i].requestDate).setHours(0,0,0,0) > new Date(this.questions[i].resultDate).setHours(0,0,0,0))) {
                dateMismatchFlag = true;
                break;
            }
        }

        if (dateNotFound === true)
            return this.dispatchEvent(utils.toastMessage('Date field is mandatory', TOAST_HEADER_LABELS.TOAST_WARNING));

        if (dateMismatchFlag === true)
            return this.dispatchEvent(utils.toastMessage('Request date should be greater than Result date', TOAST_HEADER_LABELS.TOAST_WARNING));

        this.Spinner = true;
        let answerArray = [];

        this.questions.forEach((item) => {
            answerArray.push({
                'Id': item.providerAnswerId,
                'Comar__c': item.referenceValueObjId,
                'Actor__c': item.actorId,
                'ProviderRecordQuestion__c': item.providerRecordQuestionId,
                'RequestDate__c': item.requestDate,
                'ResultDate__c': item.resultDate,
                'AsPerStateRegulations__c': item.subQuestion1Answer,
                'IsResultsinRecord__c': item.subQuestion2Answer
            });
        });

        bulkUpdateRecordAns({
            datas: JSON.stringify(answerArray),
            actorId: this.actorId
        })
        .then(result => {
            this.dispatchEvent(utils.toastMessage("CPA House hold member Clearance/Medical details are saved successfully", TOAST_HEADER_LABELS.TOAST_SUCCESS));

            refreshApex(this.wiredActorDetails);
            this.handleCPARecordDetails();
        }).catch(errors => {
            this.Spinner = false;
            let config = {
                friendlyErrorMsg: 'Error in saving CPA Clearance/Medical details. please check',
                errorType: TOAST_HEADER_LABELS.TOAST_WARNING
            }
            this.dispatchEvent(utils.handleError(errors, config));
        });
    }

    //This method used to redirect to CPA Home Dashboard screen
    handleRedirectionToCPAHomeDashboard() {
        this.Spinner = false;

        const onredirect = new CustomEvent("redirecttocpahomedashboard", {
            detail: {
                first: true
            }
        });
        this.dispatchEvent(onredirect);
    }

    //This method used to preview the Selected File Record ID
    viewFile(event) {
        this[NavigationMixin.Navigate]({
            type: 'standard__namedPage',
            attributes: {
                pageName: 'filePreview'
            },
            state: {
                // assigning ContentDocumentId to show the preview of file
                selectedRecordId: event.target.dataset.id
            }
        });
    }
}