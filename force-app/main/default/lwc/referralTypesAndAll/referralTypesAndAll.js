import { LightningElement, api } from "lwc";

//Utils
import * as sharedData from "c/sharedData";

export default class ReferralTypesAndAll extends LightningElement {

  @api caseid;
  @api pageType = false;
  @api pageTypeFrom;
  _hideShowNewOrExistingProviderComponent;
  @api
  get hideShowNewOrExistingProviderComponent() {
    return this._hideShowNewOrExistingProviderComponent;
  }

  set hideShowNewOrExistingProviderComponent(value) {
    this._hideShowNewOrExistingProviderComponent = value;

  }
  @api
  get providerid() {
    return this._providerid;
  }

  set providerid(value) {
    this._providerid = value;
  }
  @api
  get hideshowrovideromponent() {
    return this._hideshowrovideromponent;
  }

  set hideshowrovideromponent(value) {
    if (!value) {
      this.hideShowSubcomponentsBetwwenExistingProviders = false;
    }
    this._hideshowrovideromponent = value;
  }


  hideShowSubcomponentsBetwwenExistingProviders = true;

  get ShowhideSubcomponentsBetwwenExistingProviders() {
    return this.hideShowSubcomponentsBetwwenExistingProviders;
  }
  connectedCallback() {
    if (this._hideShowNewOrExistingProviderComponent) {
      this.hideShowSubcomponentsBetwwenExistingProviders = true;
    } else {
      this.hideShowSubcomponentsBetwwenExistingProviders = false;
    }
  }
  handletoggling(event) {
    this.hideShowSubcomponentsBetwwenExistingProviders = event.detail.second;
  }
  handleReferralId(event) {
    this.caseid = event.detail;
  }

  handleProviderId(event) {
    this.hideShowSubcomponentsBetwwenExistingProviders = event.detail.second;
    sharedData.setProviderId(event.detail.providerid);
    this.providerid = event.detail.providerid;
    this.pageType = event.detail.first;
    this.pageTypeFrom = event.detail.pagetype;
  }

  tabSelected(event) {
    setTimeout(() => {
      this.template
        .querySelector("c-referral-provider-decision")
        .handletabClick();
    }, 100);
  }

  redirectToReferralHome(event) {
    const onClickNewDirection = new CustomEvent('redirecttoreferralhome', {
      detail: {
        first: event.detail.first
      }
    });
    this.dispatchEvent(onClickNewDirection);
  }
}