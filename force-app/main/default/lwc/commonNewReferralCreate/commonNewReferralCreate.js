import {
    LightningElement,
    track,
    wire
} from 'lwc';
import images from "@salesforce/resourceUrl/images";

export default class CommonNewReferralCreate extends LightningElement {

    @track privateIcon = images + "/office-building.svg";
    @track privateIconblue = images + "/office-building-blue.svg";
    @track existingIcon = images + "/clipboard.svg";
    @track plusIcon = images + "/plus-new-referal.svg";
    @track existingIconblue = images + "/clipboard-blue.svg";
    @track plusIconblue = images + "/plus-new-referal-blue.svg";
    @track publicIcon = images + "/houseIcon.svg";
    @track publicIconblue = images + "/houseIcon-blue.svg";
    @track complaintIcon = images + "/complaint-greyIcon.svg";
    @track complaintIconblue = images + "/complaint-blueIcon.svg";
    @track openModelwindow = true;
    @track nextButtonEnableAndDisable = true;
    _pageType;

    handleNewComlaintsRedirect() {
        const onredirect = new CustomEvent("redirecttonewcomplaintstabs", {
            detail: {
                first: true
            }
        });
        this.dispatchEvent(onredirect);
    }

    handlePivateProviderRedirect(event) {
        this._pageType = event.currentTarget.name;
        this.nextButtonEnableAndDisable = false;

        if (event.currentTarget.name == "Private New") {
            this.template.querySelector(".box-col1").classList.add("bluebdr");
            this.template.querySelector(".box-col2").classList.remove("bluebdr");
            this.template.querySelector(".box-col3").classList.remove("bluebdr");
            this.template.querySelector(".box-col4").classList.remove("bluebdr");
            this.template.querySelector(".iconprivate").src = this.privateIconblue;
            this.template.querySelector(".iconexisting").src = this.existingIcon;
            this.template.querySelector(".iconexistingplus").src = this.plusIcon;
            this.template.querySelector(".iconexistingprivate").src = this.privateIcon;
            this.template.querySelector(".iconpublic").src = this.publicIcon;
            this.template.querySelector(".iconcomplaint").src = this.complaintIcon;
        }
        if (event.currentTarget.name == "Private Existing") {
            this.template.querySelector(".box-col2").classList.add("bluebdr");
            this.template.querySelector(".box-col1").classList.remove("bluebdr");
            this.template.querySelector(".box-col3").classList.remove("bluebdr");
            this.template.querySelector(".box-col4").classList.remove("bluebdr");
            this.template.querySelector(".iconprivate").src = this.privateIcon;
            this.template.querySelector(".iconexisting").src = this.existingIconblue;
            this.template.querySelector(".iconexistingplus").src = this.plusIconblue;
            this.template.querySelector(".iconexistingprivate").src = this.privateIconblue;
            this.template.querySelector(".iconpublic").src = this.publicIcon;
            this.template.querySelector(".iconcomplaint").src = this.complaintIcon;
        }
        if (event.currentTarget.name == "Public New") {
            this.template.querySelector(".box-col3").classList.add("bluebdr");
            this.template.querySelector(".box-col1").classList.remove("bluebdr");
            this.template.querySelector(".box-col2").classList.remove("bluebdr");
            this.template.querySelector(".box-col4").classList.remove("bluebdr");
            this.template.querySelector(".iconprivate").src = this.privateIcon;
            this.template.querySelector(".iconexisting").src = this.existingIcon;
            this.template.querySelector(".iconexistingplus").src = this.plusIcon;
            this.template.querySelector(".iconexistingprivate").src = this.privateIcon;
            this.template.querySelector(".iconpublic").src = this.publicIconblue;
            this.template.querySelector(".iconcomplaint").src = this.complaintIcon;
        }
        if (event.currentTarget.name == "Complaints") {
            this.template.querySelector(".box-col4").classList.add("bluebdr");
            this.template.querySelector(".box-col1").classList.remove("bluebdr");
            this.template.querySelector(".box-col2").classList.remove("bluebdr");
            this.template.querySelector(".box-col3").classList.remove("bluebdr");
            this.template.querySelector(".iconprivate").src = this.privateIcon;
            this.template.querySelector(".iconexisting").src = this.existingIcon;
            this.template.querySelector(".iconexistingplus").src = this.plusIcon;
            this.template.querySelector(".iconexistingprivate").src = this.privateIcon;
            this.template.querySelector(".iconpublic").src = this.publicIcon;
            this.template.querySelector(".iconcomplaint").src = this.complaintIconblue;
        }
    }
    closeModel(event) {
        event.preventDefault();
        this.openModelwindow = false;
        this.intimateParentToCloseModal();
    }
    intimateParentToCloseModal() {
        const intimateParentToCloseModal = new CustomEvent("intimateparenttoclosemodal", {
            detail: {
                closeModal: true
            }
        });
        this.dispatchEvent(intimateParentToCloseModal);
    }
    handleRedirection(event) {
        if (this._pageType !== "Complaints") {
            const onProviderClick = new CustomEvent("redirecttopublicorprivateproviderhomepage", {
                bubbles: true,
                composed: true,
                detail: {
                    pageType: this._pageType
                }
            });

            this.dispatchEvent(onProviderClick);
        } else {
            this.handleNewComlaintsRedirect();
        }


        this.intimateParentToCloseModal();

        this.openModelwindow = false;
    }


}