/**
 * @Author        : Pratheeba V
 * @CreatedOn     : April 16, 2020
 * @Purpose       : Provider Staff View Tab
 **/
import { LightningElement, track, wire, api } from 'lwc';
import getStaffInfo from '@salesforce/apex/ProvidersStaff.getStaffInfo';
import getStaffTrainingInfo from '@salesforce/apex/ProvidersStaff.getStaffTrainingInfo';
import { utils } from 'c/utils';

export default class ProvidersViewStaffInfo extends LightningElement {
    @track staffInformation = {};
    @track staffTraining = {};
    @track staffexists = false;

    //This function used to get the backend data
    connectedCallback() {
        this.providerStaffDetails();
    }

    @api getProviderStaffId;

    //This function used to get the backend data and show tables
    providerStaffDetails() {
        getStaffInfo({ staffid: this.getProviderStaffId }).then(result => {
            this.staffInformation = {
                firstName: result.length > 0 ? result[0].Staff__r ? result[0].Staff__r.FirstName__c ? result[0].Staff__r.FirstName__c : null : null : null,
                lastName: result.length > 0 ? result[0].Staff__r ? result[0].Staff__r.LastName__c ? result[0].Staff__r.LastName__c : null : null : null,
                jobTitle: result.length > 0 ? result[0].Staff__r ? result[0].Staff__r.JobTitle__c ? result[0].Staff__r.JobTitle__c : null : null : null,
                employeeType: result.length > 0 ? result[0].Staff__r ? result[0].Staff__r.EmployeeType__c ? result[0].Staff__r.EmployeeType__c : null : null : null,
                comments: result.length > 0 ? result[0].Staff__r ? result[0].Staff__r.Comments__c ? result[0].Staff__r.Comments__c : null : null : null,
                contactNo: result.length > 0 ? result[0].Staff__r ? result[0].Staff__r.Phone ? result[0].Staff__r.Phone : null : null : null,
                email: result.length > 0 ? result[0].Staff__r ? result[0].Staff__r.Email ? result[0].Staff__r.Email : null : null : null,
                certificationType: result.length > 0 ? result[0].CertificationType__c : null,
                dueDate: result.length > 0 ? utils.formatDate(result[0].DueDate__c) : null,
                appMailedDate: result.length > 0 ? utils.formatDate(result[0].ApplicationMailedDate__c) : null,
                testedDate: result.length > 0 ? utils.formatDate(result[0].TestedDate__c) : null,
                CertificateNumber__c: result.length > 0 ? result[0].CertificateNumber__c : null,
                bitCompleted: result.length > 0 ? result[0].BehavioralInterventions__c : null,
                certificationEffectiveDate: result.length > 0 ? utils.formatDate(result[0].CertificationEffectiveDate__c) : null,
                renewalDate: result.length > 0 ? utils.formatDate(result[0].RenewalDate__c) : null,
                ciReqDate: result.length > 0 ? result[0].Staff__r ? result[0].Staff__r.CIRequestDate__c ? utils.formatDate(result[0].Staff__r.CIRequestDate__c) : null : null : null,
                ciResDate: result.length > 0 ? result[0].Staff__r ? result[0].Staff__r.CIResponseDate__c ? utils.formatDate(result[0].Staff__r.CIResponseDate__c) : null : null : null,
                ciRcc: result.length > 0 ? result[0].Staff__r ? result[0].Staff__r.CIRCC__c ? result[0].Staff__r.CIRCC__c : null : null : null,
                ciCpa: result.length > 0 ? result[0].Staff__r ? result[0].Staff__r.CICPA__c ? result[0].Staff__r.CICPA__c : null : null : null,
                scReqDate: result.length > 0 ? result[0].Staff__r ? result[0].Staff__r.SCRequestDate__c ? utils.formatDate(result[0].Staff__r.SCRequestDate__c) : null : null : null,
                scResDate: result.length > 0 ? result[0].Staff__r ? result[0].Staff__r.SCResponseDate__c ? utils.formatDate(result[0].Staff__r.SCResponseDate__c) : null : null : null,
                scRcc: result.length > 0 ? result[0].Staff__r ? result[0].Staff__r.SCRCC__c ? result[0].Staff__r.SCRCC__c : null : null : null,
                scCpa: result.length > 0 ? result[0].Staff__r ? result[0].Staff__r.SCCPA__c ? result[0].Staff__r.SCCPA__c : null : null : null,
                fcReqDate: result.length > 0 ? result[0].Staff__r ? result[0].Staff__r.FCRequestDate__c ? utils.formatDate(result[0].Staff__r.FCRequestDate__c) : null : null : null,
                fcResDate: result.length > 0 ? result[0].Staff__r ? result[0].Staff__r.FCResponseDate__c ? utils.formatDate(result[0].Staff__r.FCResponseDate__c) : null : null : null,
                fcRcc: result.length > 0 ? result[0].Staff__r ? result[0].Staff__r.FCRCC__c ? result[0].Staff__r.FCRCC__c : null : null : null,
                fcCpa: result.length > 0 ? result[0].Staff__r ? result[0].Staff__r.FCCPA__c ? result[0].Staff__r.FCCPA__c : null : null : null,

            }
            this.staffexists = true;
        }).catch(errors => {
            return this.dispatchEvent(utils.handleError(errors));
        });

        getStaffTrainingInfo({ staffid: this.getProviderStaffId }).then(res => {
            this.staffexists = true;
            this.staffTraining = {
                staffTrainingName: res.length > 0 ? res[0].TrainingName__c ? res[0].TrainingName__c : null : null,
                staffTrainingHours: res.length > 0 ? res[0].TrainingHours__c ? res[0].TrainingHours__c + ' Hrs' : null : null,
                staffTrainingcompletionDate: res.length > 0 ? res[0].CompletionDate__c ? utils.formatDate(res[0].CompletionDate__c) : null : null
            }
        }).catch(errors => {
            return this.dispatchEvent(utils.handleError(errors));
        });
    }

    redirectToStaff() {
        const onbacktoStaff = new CustomEvent('redirecttostaffinfo', { detail: { first: true } });
        this.dispatchEvent(onbacktoStaff);
    }
}