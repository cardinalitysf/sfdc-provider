/**
 * @Author        : Preethi Bojarajan
 * @CreatedOn     : Apr 22,2020
 * @Purpose       : Providers Monitering Reviw for Caseworker
 * @updatedBy     : 
 * @updatedOn     : 
 **/
import {
    LightningElement,
    wire,
    track,
    api
} from 'lwc';

import getPickListValues from '@salesforce/apex/providersMonitoringReview.getPickListValues';
import saveSign from '@salesforce/apex/providersMonitoringReview.saveSign';
import updateapplicationdetailsDraft from '@salesforce/apex/providersMonitoringReview.updateapplicationdetailsDraft';
import updateapplicationdetails from '@salesforce/apex/providersMonitoringReview.updateapplicationdetails';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';
import getReviewDetails from "@salesforce/apex/providersMonitoringReview.getReviewDetails";
import getAllUsersList from "@salesforce/apex/providersMonitoringReview.getAllUsersList";
import updateCaseworkerdetails from "@salesforce/apex/providersMonitoringReview.updateCaseworkerdetails";
import getDatatableDetails from "@salesforce/apex/providersMonitoringReview.getDatatableDetails";

import images from '@salesforce/resourceUrl/images';
import userId from '@salesforce/user/Id';


import * as sharedData from "c/sharedData";
import {
    utils
} from 'c/utils';


const dataTableColumns = [{
    label: 'Date',
    fieldName: 'SubmittedDate__c'
},
{
    label: 'Author',
    fieldName: 'authorName'
},
{
    label: 'Designation',
    fieldName: 'authorDesignation'
},
{
    label: 'Status',
    fieldName: 'status'
},
{
    label: 'Assigned To',
    fieldName: 'assignedToName'
},
{
    label: 'Designation',
    fieldName: 'supervisorDesignation'
},
{
    label: 'Action',
    fieldName: 'id',
    type: "button",
    typeAttributes: {
        iconName: 'utility:preview',
        name: 'View',
        title: 'Click to Preview',
        variant: 'border-filled',
        class: 'view-red',
        disabled: false,
        iconPosition: 'left',
        target: '_self'
    }
},

]

const supervisorColumns = [{
    label: 'Name',
    fieldName: 'Name',
    type: 'Name'
},
{
    label: 'CaseLoad',
    fieldName: 'CaseLoad__c',
    type: 'number'
},
{
    label: 'Availability',
    fieldName: 'Availability__c',
    type: 'text'
},
{
    label: 'Zip Code',
    fieldName: '',
    type: 'text'
},
{
    label: 'Type Of Worker',
    fieldName: 'ProfileName',
    type: 'text'
},
{
    label: 'Unit',
    fieldName: 'Unit__c',
    type: 'text'
},
];


// declaration of variables
// for calculations
let isDownFlag,
    isDotFlag = false,
    prevX = 0,
    currX = 0,
    prevY = 0,
    currY = 0;

let x = "#0000A0"; //blue color
let y = 1.5; //weight of line width and dot.       

let canvasElement, ctx; //storing canvas context
let attachment; //holds attachment information after saving the sigture on canvas
let dataURL, convertedDataURI; //holds image data

export default class ProvidersMonitoringReview extends LightningElement {
    @track handleVisibility = false;
    @track comments;
    @track reviewstatuscomments = {};
    @track statusOptions;
    @track supervisorColumns = supervisorColumns;
    @track dataTableColumns = dataTableColumns;
    @track isOpenModal = false;
    @track actionShowModal = false;
    @track openmodel = false;
    @track assignTierpopupshow = [];
    @track bShowsignatureModal = false;
    @track getContext;
    @track supervisor;
    @track caseworker;
    @track currenttablepage = [];
    @track ApprovalCommentsvalue;
    @track ReviewStatus__c;
    @track assignDisable = false;
    @track filedisable = true;
    @track HideDatatable = false;
    @track MonitoringStatusValidation;
    @track SignatureValidation;
    @track SignatureValidationView;
    @track SignatureShowHide = false;
    @track monitoringStatus = '';

    //Page Freeze Initialization
    @track isDisable = false;
    @track isDisableTierApproval = true;
    @track isDisablesignature = false;
    @track isSaveAsDraftDisable = false;
    @track isSubmitBtnDisable = false;

    refreshIcon = images + '/refresh-icon.svg';
    @track isMonitoringNotCompleted = false;
    @track preSelectedRows = [];
    @track workItemId = '';
    @track hideReviewBar = false;

    // Monitoring id 
    get monitoringId() {
        this.monitoringStatus = sharedData.getMonitoringStatus();
        return sharedData.getMonitoringId();
    }

    get supervisorFlow() {
        if (sharedData.getUserProfileName() === 'Supervisor')
            return true;
        else return false;
    }

    connectedCallback() {
        getReviewDetails({
            monitoringId: this.monitoringId
        })
            .then(result => {
                if (result.length > 0) {
                    if (this.supervisorFlow === false) {
                        this.ReviewStatus__c = result[0].ReviewStatus__c;
                        this.ApprovalCommentsvalue = result[0].ApprovalComments__c;
                        this.SignatureValue = result[0].signatureUrl;

                        if (['Submitted', 'Rejected', 'Approved'].includes(this.monitoringStatus) && ['Approved', 'Rejected'].includes(result[0].ReviewStatus__c)) {
                            this.isDisabled = true;
                            this.isDisableTierApproval = true;
                            this.isDisablesignature = true;
                            this.isSaveAsDraftDisable = true;
                            this.isSubmitBtnDisable = true;
                        } else if (!result[0].ReviewStatus__c) {
                            this.isDisableTierApproval = true;
                            this.isDisablesignature = true;
                            this.isSaveAsDraftDisable = true;
                            this.isSubmitBtnDisable = true;
                        } else if (result[0].ReviewStatus__c == 'None') {
                            this.isDisableTierApproval = true;
                            this.isDisablesignature = false;
                            this.isSaveAsDraftDisable = false;
                            this.isSubmitBtnDisable = true;
                        } else {
                            this.isDisabled = false;
                            this.isDisableTierApproval = false;
                            this.isDisablesignature = false;
                            this.isSaveAsDraftDisable = false;
                            this.isSubmitBtnDisable = false;
                        }
                    } else {
                        if (['Rejected', 'Approved', 'Returned'].includes(this.monitoringStatus)) {
                            this.isSubmitBtnDisable = true;
                        } else {
                            this.isSubmitBtnDisable = false;
                        }
                    }
                    this.getDatatablevalues();
                }
            }).catch((errors) => {
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const {
                        title,
                        message,
                        errorType
                    } = error;
                    this.dispatchEvent(
                        utils.toastMessageWithTitle(title, message, errorType)
                    );
                } else {
                    this.dispatchEvent(
                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                    );
                }
            });
    }

    getDatatablevalues() {
        getDatatableDetails({
            monitoringId: this.monitoringId
        })
            .then(result => {
                let resultArray = [];
                result = JSON.parse(result);
                this.SignatureValidation = result[0].signatureUrl;
                this.preSelectedRows = [];
                if (this.supervisorFlow === false && result[0].monitorRecord.Caseworker__r !== undefined) {
                    resultArray.push({
                        SubmittedDate__c: utils.formatDate(result[0].monitorRecord.SubmittedDate__c),
                        authorName: result[0].monitorRecord.Caseworker__r.Name,
                        authorDesignation: 'OLM',
                        status: result[0].monitorRecord.ReviewStatus__c == 'Approved' ? 'Submit For Review' : result[0].monitorRecord.ReviewStatus__c,
                        assignedToName: result[0].monitorRecord.Supervisor__r.Name,
                        ApprovalCommentsvalue: result[0].monitorRecord.ApprovalComments__c,
                        supervisorDesignation: 'Supervisor',
                        SignatureValue: result[0].signatureUrl
                    });
                    this.currenttablepage = resultArray;
                    let array = [];
                    array.push(result[0].monitorRecord.Supervisor__c);
                    this.preSelectedRows = array;
                    this.HideDatatable = true;
                    if (!this.SignatureValidation) {
                        this.SignatureShowHide = false;
                    } else {
                        this.SignatureShowHide = true;
                        this.SignatureValidationView = '/sfc/servlet.shepherd/version/download/' + result[0].signatureUrl;
                    }
                } else if (result[0].monitorRecord.Steps !== undefined) {
                    if (result[0].monitorRecord.Steps.totalSize === 1) {
                        let signUrlIndex = result[0].signatureUrl.findIndex(sign => sign.OwnerId === result[0].monitorRecord.Steps.records[0].ActorId);
                        let signUrl = signUrlIndex >= 0 ? result[0].signatureUrl[signUrlIndex].Id : '';
                        resultArray.push({
                            authorName: result[0].monitorRecord.Steps.records[0].Actor.Name,
                            status: result[0].monitorRecord.Steps.records[0].StepStatus == 'Started' ? 'Submit For Review' : (result[0].monitorRecord.StepStatus == 'Removed' ? 'Returned' : result[0].monitorRecord.StepStatus),
                            assignedToName: result[0].monitorRecord.Workitems.records[0].Actor.Name,
                            ApprovalCommentsvalue: result[0].monitorRecord.Steps.records[0].Comments,
                            authorDesignation: result[0].monitorRecord.Steps.records[0].Actor.Profile.Name == 'Supervisor' ? 'Supervisor' : 'OLM',
                            supervisorDesignation: result[0].monitorRecord.Workitems.records[0].Actor.Profile.Name,
                            SubmittedDate__c: utils.formatDate(result[0].monitorRecord.Steps.records[0].CreatedDate),
                            SignatureValue: signUrl
                        });
                    } else if (result[0].monitorRecord.Steps.totalSize > 1) {
                        let datas = result[0].monitorRecord.Steps.records;
                        for (let data in datas) {
                            let signUrlIndex = result[0].signatureUrl.findIndex(sign => sign.OwnerId === datas[data].ActorId);
                            let signUrl = signUrlIndex >= 0 ? result[0].signatureUrl[signUrlIndex].Id : '';
                            resultArray.push({
                                authorName: datas[data].Actor.Name,
                                status: datas[data].StepStatus == 'Started' ? 'Submit For Review' : datas[data].StepStatus == 'Removed' ? 'Returned' : datas[data].StepStatus,
                                assignedToName: datas[parseInt(data) - 1] !== undefined ? datas[parseInt(data) - 1].Actor.Name : ' - ',
                                ApprovalCommentsvalue: datas[data].Comments,
                                authorDesignation: datas[data].Actor.Profile.Name == 'Supervisor' ? 'Supervisor' : 'OLM',
                                supervisorDesignation: datas[parseInt(data) - 1] !== undefined ? datas[data - 1].Actor.Profile.Name : ' - ',
                                SubmittedDate__c: utils.formatDate(datas[data].CreatedDate),
                                SignatureValue: signUrl
                            });
                        }
                    }
                    this.currenttablepage = resultArray;
                    if (result[0].monitorRecord.Steps.totalSize === 1)
                        this.workItemId = result[0].monitorRecord.Workitems.records[0].Id;
                    this.HideDatatable = true;
                    if (result[0].monitorRecord.Steps.records[0].StepStatus == 'Rejected' || result[0].monitorRecord.Steps.records[0].StepStatus == 'Approved' || (this.supervisorFlow === true && result[0].monitorRecord.Steps.records[0].StepStatus == 'Removed'))
                        this.hideReviewBar = true;
                }
            }).catch((errors) => {
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const {
                        title,
                        message,
                        errorType
                    } = error;
                    this.dispatchEvent(utils.toastMessageWithTitle(title, message, errorType));
                } else {
                    this.dispatchEvent(utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE));
                }
            });
    }

    openmodal() {
        this.openmodel = true;
    }
    DoneModal() {
        this.openmodel = false;
    }
    closeModal() {
        this.openmodel = false;
    }
    closeModalAction() {
        this.openmodel = false;
    }
    handleCloseModal() {
        this.isOpenModal = false;
    }

    actionCloseModal() {
        this.actionShowModal = false;
    }

    signaturemodal() {
        // to open modal window set 'bShowsignatureModal' track value as true      
        this.bShowsignatureModal = true;
    }

    closeModal() {
        this.SignatureShowHide = true;
        this.bShowsignatureModal = false; // to close modal window set 'bShowsignatureModal' tarck value as false
    }

    //PickList for Status
    @wire(getPickListValues, {
        objInfo: {
            'sobjectType': 'Monitoring__c'
        },
        picklistFieldApi: 'ReviewStatus__c'
    })
    wireduserDetails2(data) {
        try {
            if (data.data != undefined && data.data.length > 0) {
                if (this.supervisorFlow === false)
                    this.statusOptions = data.data;
                else
                    this.statusOptions = this.supervisorStatusOptions;

            } else if (data.error) {
                let errors = data.error;
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const {
                        title,
                        message,
                        errorType
                    } = error;
                    this.dispatchEvent(utils.toastMessageWithTitle(title, message, errorType));
                } else {
                    this.dispatchEvent(utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE));
                }
            }
        } catch (error) {
            throw error;
        }
    }

    get supervisorStatusOptions() {
        return [{
            label: 'Approve',
            value: 'Approve'
        },
        {
            label: 'Reject',
            value: 'Reject'
        },
        {
            label: 'Return To Case Worker',
            value: 'Reassign'
        },
        ];
    }

    changeStatusHandler(event) {
        if (event.detail.value == 'None') {
            this.isSubmitBtnDisable = true;
            this.isDisableTierApproval = true;
            this.isSaveAsDraftDisable = false;
            this.isDisablesignature = false;
        } else if (event.detail.value == 'Rejected') {
            this.isDisableTierApproval = true;
            this.isSubmitBtnDisable = false;
            this.isDisablesignature = false;
            this.isSaveAsDraftDisable = false;
        } else {
            this.isSubmitBtnDisable = false;
            this.isSaveAsDraftDisable = false;
            this.isDisablesignature = false;
            this.isDisableTierApproval = false;
        }
        this.ReviewStatus__c = event.detail.value;

        //Sundar Commenting this lines
        /*if (this.ReviewStatus__c === 'Approved' && this.monitoringStatus === 'Draft') {
            this.isDisableTierApproval = false;
            this.isDisablesignature = false;
        } else {
            this.isDisableTierApproval = true;
            this.isDisablesignature = true;
        }

        if (this.ReviewStatus__c == 'None')
            this.isSubmitBtnDisable = true;
        else {
            this.isSubmitBtnDisable = false;
            this.isSaveAsDraftDisable = false;
        }
        if (this.supervisorFlow === true)
            this.isDisablesignature = false;*/
    }

    commentschangeHandler(event) {
        this.ApprovalCommentsvalue = event.detail.value;
    }

    @wire(getAllUsersList)
    wireduserDetails1(data) {
        try {
            if (data.data != undefined && data.data.length > 0) {
                this.assignTierpopupshow = data;
            } else if (data.error) {
                let errors = data.error;
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const {
                        title,
                        message,
                        errorType
                    } = error;
                    this.dispatchEvent(utils.toastMessageWithTitle(title, message, errorType));
                } else {
                    this.dispatchEvent(utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE));
                }
            }
        } catch (error) {
            throw error;
        }
    }

    handleRowSelection = event => {
        this.supervisor = event.detail.selectedRows[0].Id
        this.caseworker = userId;
        this.assignDisable = false;
    }

    dataAction(event) {
        let actiondt = event.detail.row;
        this.AuthorValue = event.detail.row.authorName;
        this.DateValue = event.detail.row.SubmittedDate__c;
        this.StatusValue = event.detail.row.status;
        this.commentsValue = event.detail.row.ApprovalCommentsvalue;
        this.SignatureValue = '/sfc/servlet.shepherd/version/download/' + event.detail.row.SignatureValue;
        this.openmodel = true;
    }

    toggleVisibility() {
        this.handleVisibility = false;
    }

    handleFilesChange(event) {
        let strFileNames = '';
        // Get the list of uploaded files
        const uploadedFiles = event.detail.files;

        for (let i = 0; i < uploadedFiles.length; i++) {
            strFileNames += uploadedFiles[i].name + ', ';

        }
        this.dispatchEvent(
            new ShowToastEvent({
                title: 'Success!!',
                message: strFileNames + ' Files uploaded Successfully!!!',
                variant: 'success',
            }),
        );
    }

    handleOpenModal() {
        this.isOpenModal = true;
        let localvar = [...this.assignTierpopupshow]
        for (var i = 0; i < localvar.length; i++) {
            var x = {
                ProfileName: localvar[i].Profile.Name
            };
            var y = Object.assign(x, localvar[i]);
            localvar[i] = y;
        }
        this.assignTierpopupshow = localvar;
    }

    handleAssignModal() {
        let myobjapp = {
            'sobjectType': 'Monitoring__c'
        };
        myobjapp.ReviewStatus__c = this.ReviewStatus__c;
        myobjapp.Caseworker__c = this.caseworker;
        myobjapp.Supervisor__c = this.supervisor;
        myobjapp.SubmittedDate__c = utils.formatDateYYYYMMDD(new Date());

        if (this.monitoringId)
            myobjapp.Id = this.monitoringId;

        updateCaseworkerdetails({
            objSobjecttoUpdateOrInsert: myobjapp
        })
            .then(result => {
                this.isOpenModal = false;
                this.getDatatablevalues();
            }).catch((errors) => {
                this.isOpenModal = false;
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const {
                        title,
                        message,
                        errorType
                    } = error;
                    this.dispatchEvent(utils.toastMessageWithTitle(title, message, errorType));
                } else {
                    this.dispatchEvent(utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE));
                }
            });
    }

    /**********Signature******/

    @api recordId;

    //event listeners added for drawing the signature within shadow boundary
    constructor() {
        super();
        this.template.addEventListener('mousemove', this.handleMouseMove.bind(this));
        this.template.addEventListener('mousedown', this.handleMouseDown.bind(this));
        this.template.addEventListener('mouseup', this.handleMouseUp.bind(this));
        this.template.addEventListener('mouseout', this.handleMouseOut.bind(this));
    }

    //retrieve canvase and context
    renderedCallback() {
        setTimeout(() => {
            canvasElement = this.template.querySelector('canvas');
            ctx = canvasElement.getContext("2d");
        }, 1000);
    }

    //handler for mouse move operation
    handleMouseMove(event) {
        this.searchCoordinatesForEvent('move', event);
    }

    //handler for mouse down operation
    handleMouseDown(event) {
        this.searchCoordinatesForEvent('down', event);
    }

    //handler for mouse up operation
    handleMouseUp(event) {
        this.searchCoordinatesForEvent('up', event);
    }

    //handler for mouse out operation
    handleMouseOut(event) {
        this.searchCoordinatesForEvent('out', event);
    }

    /*
        handler to perform save operation.
        save signature as attachment.
        after saving shows success or failure message as toast
    */
    handleSaveClick() {
        //set to draw behind current content
        ctx.globalCompositeOperation = "destination-over";
        ctx.fillStyle = "#FFF"; //white
        ctx.fillRect(0, 0, canvasElement.width, canvasElement.height);

        //convert to png image as dataURL
        dataURL = canvasElement.toDataURL("image/png");
        //convert that as base64 encoding
        convertedDataURI = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");

        //call Apex method imperatively and use promise for handling sucess & failure
        saveSign({
            strSignElement: convertedDataURI,
            recId: this.monitoringId
        })
            .then(result => {
                this.getDatatablevalues();
                // this.isDisableTierApproval = false;
                this.bShowsignatureModal = false;
                this.isDisableTierApproval = this.ReviewStatus__c == "Rejected" ||
                    this.ReviewStatus__c == "None" ?
                    true :
                    false;

                //this.ContentDocumentLink = result;              
                //show success message
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: ' Signature saved Successfully',
                        variant: 'success',
                    }),
                );
            }).catch(error => {
                //show error message
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error  in creating Signature',
                        message: error.body.message,
                        variant: 'error',
                    }),
                );
            });

    }

    //clear the signature from canvas
    handleClearClick() {
        this.SignatureShowHide = false;
        ctx.clearRect(0, 0, canvasElement.width, canvasElement.height);
    }

    searchCoordinatesForEvent(requestedEvent, event) {
        // event.preventDefault();
        if (requestedEvent === 'down') {
            this.setupCoordinate(event);
            isDownFlag = true;
            isDotFlag = true;
            if (isDotFlag) {
                this.drawDot();
                isDotFlag = false;
            }
        }
        if (requestedEvent === 'up' || requestedEvent === "out") {
            isDownFlag = false;
        }
        if (requestedEvent === 'move') {
            if (isDownFlag) {
                this.setupCoordinate(event);
                this.redraw();
            }
        }
    }

    //This method is primary called from mouse down & move to setup cordinates.
    setupCoordinate(eventParam) {
        //get size of an element and its position relative to the viewport 
        //using getBoundingClientRect which returns left, top, right, bottom, x, y, width, height.
        const clientRect = canvasElement.getBoundingClientRect();
        prevX = currX;
        prevY = currY;
        currX = eventParam.clientX - clientRect.left;
        currY = eventParam.clientY - clientRect.top;
    }

    //For every mouse move based on the coordinates line to redrawn
    redraw() {
        ctx.beginPath();
        ctx.moveTo(prevX, prevY);
        ctx.lineTo(currX, currY);
        ctx.strokeStyle = x; //sets the color, gradient and pattern of stroke
        ctx.lineWidth = y;
        ctx.closePath(); //create a path from current point to starting point
        ctx.stroke(); //draws the path
    }

    //this draws the dot
    drawDot() {
        ctx.beginPath();
        ctx.fillStyle = x; //blue color
        ctx.fillRect(currX, currY, y, y); //fill rectrangle with coordinates
        ctx.closePath();
    }

    // Save As Draft 
    saveAsDraftMethod() {
        if (!this.ReviewStatus__c)
            return this.dispatchEvent(utils.toastMessage("Please Select Status", "Warning"));

        //Sundar commenting this because on Save, Signature validation is not needed.
        /*if (!this.SignatureValidation)
            return this.dispatchEvent(utils.toastMessage("Signature Is Mandatory", "Warning"));*/

        let myObj = {
            'sobjectType': 'Monitoring__c'
        };

        myObj.ReviewStatus__c = this.ReviewStatus__c;
        myObj.ApprovalComments__c = this.ApprovalCommentsvalue;
        myObj.SignatureValue = this.SignatureValue;
        myObj.MonitoringStatus__c = 'Draft';
        myObj.SubmittedDate__c = utils.formatDateYYYYMMDD(new Date());

        if (this.monitoringId)
            myObj.Id = this.monitoringId;

        updateapplicationdetailsDraft({
            objSobjecttoUpdateOrInsert: myObj
        })
            .then(result => {
                this.actionShowModal = true;
                this.dispatchEvent(utils.toastMessage("Monitoring Review has been saved successfully !", "success"));
                const oncaseid = new CustomEvent('redirecteditmonitering', {
                    detail: {
                        first: true
                    }
                });
                this.dispatchEvent(oncaseid);
            }).catch((errors) => {
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const {
                        title,
                        message,
                        errorType
                    } = error;
                    this.dispatchEvent(
                        utils.toastMessageWithTitle(title, message, errorType)
                    );
                } else {
                    this.dispatchEvent(
                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                    );
                }
            });
    }

    //Submit
    submitMethod() {
        if (!this.ReviewStatus__c)
            return this.dispatchEvent(utils.toastMessage("Please Select Status", "warning"));

        if (!this.SignatureValidation)
            return this.dispatchEvent(utils.toastMessage("Signature Is Mandatory", "warning"));

        if (this.currenttablepage.length == 0 && this.supervisorFlow === false && this.ReviewStatus__c == 'Approved')
            return this.dispatchEvent(utils.toastMessage("Tier of Approval is mandatory", "warning"));

        let myObj = {
            'sobjectType': 'Monitoring__c'
        };

        myObj.ReviewStatus__c = this.ReviewStatus__c;
        myObj.Caseworker__c = this.caseworker;
        myObj.ApprovalComments__c = this.ApprovalCommentsvalue;
        myObj.SubmittedDate__c = utils.formatDateYYYYMMDD(new Date());
        myObj.MonitoringStatus__c = this.ReviewStatus__c == 'Approved' ? 'Submitted' : this.ReviewStatus__c;

        if (this.monitoringId)
            myObj.Id = this.monitoringId;
        let preSelectedRows = this.preSelectedRows[0] == undefined ? '' : this.preSelectedRows[0];
        let workItemId = this.workItemId === '' ? '' : this.workItemId;

        if (this.ReviewStatus__c == 'Rejected') {
            updateapplicationdetailsDraft({
                objSobjecttoUpdateOrInsert: myObj
            })
                .then(result => {
                    this.dispatchEvent(utils.toastMessage("Review has been rejected successfully", "Success"));
                    const oncaseid = new CustomEvent('redirecteditmonitering', {
                        detail: {
                            first: true
                        }
                    });
                    this.dispatchEvent(oncaseid);
                })
                .catch(error => {
                    this.error = error.message;
                    this.dispatchEvent(utils.toastMessage(error.body.pageErrors[0].StatusCode, "warning"));
                });
        } else {
            updateapplicationdetails({
                objSobjecttoUpdateOrInsert: myObj,
                supervisorId: preSelectedRows,
                workItemId: workItemId
            })
                .then(result => {
                    this.dispatchEvent(utils.toastMessage("Review has been submitted successfully", "Success"));
                    const oncaseid = new CustomEvent('redirecteditmonitering', {
                        detail: {
                            first: true
                        }
                    });
                    this.dispatchEvent(oncaseid);
                }).catch((errors) => {
                    if (errors) {
                        let error = JSON.parse(errors.body.message);
                        const {
                            title,
                            message,
                            errorType
                        } = error;
                        this.dispatchEvent(
                            utils.toastMessageWithTitle(title, message, errorType)
                        );
                    } else {
                        this.dispatchEvent(
                            utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                        );
                    }
                });
        }
    }
}