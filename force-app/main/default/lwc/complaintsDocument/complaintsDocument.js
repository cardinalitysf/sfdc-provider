/**
 * @Author        : Murugan
 * @CreatedOn     : July 3,2020
 * @Purpose       : Attachments based on Referral Documents
 **/

import {
    LightningElement,
    track

} from 'lwc';
import * as referralsharedDatas from "c/sharedData";
import {
    CJAMS_CONSTANTS
} from 'c/constants';

export default class ComplaintsDocument extends LightningElement {
    get referralID() {
       return referralsharedDatas.getCaseId();
    //    return '5009D000002oKg0QAE';
    }
    get sobjectNameToFetch() {
        return CJAMS_CONSTANTS.CASE_OBJECT_NAME;
    }
}