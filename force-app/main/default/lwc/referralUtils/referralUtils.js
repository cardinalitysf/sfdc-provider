/**
 * @Author        : V.S.Marimuthu
 * @CreatedOn     : March 09 ,2020
 * @Purpose       : COntains all the utility functions
 **/
import { ShowToastEvent } from "lightning/platformShowToastEvent";
const titles = { success: 'Success!..', warning: 'Warning!..', error: 'Error!..'};
export const utils = {

    // Function used to format phone number in US format like (111) 111-1111
    formattedPhoneNumber(phoneNumber){
        phoneNumber =  phoneNumber ? '(' +phoneNumber.substr(0, 3) + ')' + ' ' + phoneNumber.substr(3, 3) + '-' + phoneNumber.substr(6, 4) : null;
        return phoneNumber;
    },

    // Function used to format Tax id  in US format like 11-11111111
    formattedTaxID(taxid){
        taxid = taxid ? taxid.substr(0, 2) + '-' + taxid.substr(2, 9) : null;
        return taxid;
    },
    //Function Used to give Toast Messages On Success, Warning and Error
    toastMessage(message, variant) {
      
        const toast = new ShowToastEvent({
            title: titles[variant],
            message,
            variant
        });
        return toast;
    }
}