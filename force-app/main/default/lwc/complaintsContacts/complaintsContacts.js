/***
 * @Author: Janaswini S
 * @CreatedOn: July 02, 2020
 * @Purpose: Complaints Contact Details Add/View/Delete
 **/

import { LightningElement, track, api, wire } from "lwc";

import myResource from "@salesforce/resourceUrl/styleSheet";
import * as sharedData from "c/sharedData";

import { loadStyle } from "lightning/platformResourceLoader";
import { refreshApex } from "@salesforce/apex";
import { utils } from "c/utils";

import { constPopupVariables, CJAMS_CONSTANTS } from "c/constants";

import {
  createRecord,
  updateRecord,
  deleteRecord
} from "lightning/uiRecordApi";

import fetchContactDetails from "@salesforce/apex/ComplaintsContacts.fetchContactDetails";
import editContactDetails from "@salesforce/apex/ComplaintsContacts.editContactDetails";
import getMultiplePicklistValues from "@salesforce/apex/ComplaintsContacts.getMultiplePicklistValues";

import CONTACTNOTES_OBJECT from "@salesforce/schema/ContactNotes__c";
import ID_FIELD from "@salesforce/schema/ContactNotes__c.Id";
import CASE_OBJECT_FIELD from "@salesforce/schema/ContactNotes__c.Case__c";

import images from "@salesforce/resourceUrl/images";

import getApplicationProviderStatus from "@salesforce/apex/ComplaintsContacts.getApplicationProviderStatus";

const columns = [
  {
    label: "CONTACT NAME / AGENCY ",
    fieldName: "ContactName__c",
    type: "text",
    sortable: true,
    initialWidth: 200
  },
  {
    label: "SOURCE",
    type: "button",
    sortable: true,
    typeAttributes: {
      name: "dontRedirect",
      label: { fieldName: "Source__c" }
    },
    cellAttributes: {
      class: "title"
    }
  },
  {
    label: "MODE OF CONTACT",
    type: "button",
    sortable: true,
    typeAttributes: {
      name: "dontRedirect",
      label: { fieldName: "ContactType__c" }
    },
    cellAttributes: {
      class: "title"
    }
  },
  {
    label: "CONTACT DATE",
    fieldName: "Date",
    type: "text",
    initialWidth: 170
  },
  { label: "PHONE", fieldName: "Phone__c", type: "text" },
  {
    label: "EMAIL",
    type: "button",
    typeAttributes: {
      name: "dontRedirect",
      label: { fieldName: "Email__c" }
    },
    cellAttributes: {
      class: "title"
    }
  },
  {
    label: "Notes",
    fieldName: "Notes",
    type: "button",
    typeAttributes: {
      label: {
        fieldName: "Notes"
      },
      name: "Descpopup",
      title: {
        fieldName: "Notes"
      }
    },
    cellAttributes: {
      iconName: "utility:description",
      iconAlternativeText: "Notes",
      class: {
        fieldName: "descriptionClass"
      }
    }
  }
];

export default class ComplaintsContacts extends LightningElement {
  //picklist values
  @track modeOfContact;
  @track sourceContact;
  @track Notesdescription;

  //show and hide
  @track showUpdates = false;

  @api sobjectName;
  @track columns = columns;
  @track contactDetails = {};
  @track noRecordsFound = false;

  //Sort Function
  @track defaultSortDirection = "asc";
  @track sortDirection = "asc";
  @track sortedBy = "ContactType__c";

  //Pagination
  @track totalContactsCount = 0;
  @track totalContacts = [];
  @track page = 1;
  @track currentPageContactData;
  setPagination = 5;
  perpage = 10;

  @track disabled = false;
  @track freezeContactScreen = false;

  @api recordId;
  @track buttonDisable = false;

  _newContactModal;
  wiredContactDetails;
  contactId;
  attachmentIcon = images + "/contact-newIcon.svg";

  @track deleteModel = false;
  @track viewModel = false;
  @track deleteId;

  @track title;
  @track finalStatus;

  @track isSuperUserFlow = false;
  @track disableStatus = false;
  @track Spinner = true;

  @track descModel = false;

  @track btnLabel;

  renderedCallback() {
    Promise.all([loadStyle(this, myResource + "/styleSheet.css")]);
  }

  constructor() {
    super();
    let deleteIcon = true;
    for (let i = 0; i < this.columns.length; i++) {
      if (this.columns[i].type == "action") {
        deleteIcon = false;
      }
    }
    if (deleteIcon) {
      this.columns.push({
        type: "action",
        typeAttributes: { rowActions: this.getRowActions },
        cellAttributes: {
          iconName: "utility:threedots_vertical",
          iconAlternativeText: "Actions",
          class: "tripledots"
        }
      });
    }
  }

  //openModal from Header Button Child comp Through AllTabs Comp

  @api get showButtonContacts() {
    return this._newContactModal;
  }
  set showButtonContacts(value) {
    this._newContactModal = value;
    if (this.freezeContactScreen)
      return this.dispatchEvent(
        utils.toastMessage(
          `Cannot add contact notes for ${this.finalStatus} application`,
          "warning"
        )
      );

    if (!this.contactId) {
      this.title = "ADD CONTACT DETAILS";
      this.btnLabel = "SAVE";
      this.showUpdates = false;
      this.descModel = false;
    }
  }

  //closemdal dispatch function
  closeModal() {
    this._newContactModal = false;
    this.dispatchCustomEventToHandleAddContactsInfoPopUp();
  }
  dispatchCustomEventToHandleAddContactsInfoPopUp() {
    const tooglecmps = new CustomEvent("contactstogglecomponents", {
      detail: {
        Component: "CloseContact"
      }
    });
    this.dispatchEvent(tooglecmps);
    this.contactDetails = {};
    this.viewModel = false;
    this.contactId = null;
    this.descModel = false;
  }

  _getContactNotesData;
  @api get getContactNotesData() {
    return this._getContactNotesData;
  }
  set getContactNotesData(value) {
    this._getContactNotesData = value;
  }
  _selectQueryToGetData;

  //case

  get caseId() {
    return sharedData.getCaseId();
  }

  //Get PickList Values from Object
  @wire(getMultiplePicklistValues, {
    objInfo: "ContactNotes__c",
    picklistFieldApi: "ContactType__c,Source__c"
  })
  wiredTitle(data) {
    try {
      if (data.data != undefined && data.data !== "") {
        this.modeOfContact = data.data.ContactType__c;
        this.sourceContact = data.data.Source__c;
      }
    } catch (error) {
      throw error;
    }
  }

  //Status Check
  @wire(getApplicationProviderStatus, {
    applicationId: "$caseId"
  })
  applicationStatus({ error, data }) {
    if (data) {
      if (
        data.length > 0 &&
        (data[0].Status == "Caseworker Submitted" ||
          data[0].Status == "Approved" ||
          data[0].Status == "Supervisor Rejected" ||
          data[0].Status == "Rejected" ||
          data[0].Status == "Appeal" ||
          data[0].Status == "Submitted to supervisor" ||
          data[0].Status == "Accepted" ||
          data[0].Status == "Disputed")
      ) {
        this.freezeContactScreen = true;
        this.finalStatus = data[0].Status;
      } else {
        this.freezeContactScreen = false;
      }
    } else if (error) {
      let errors = error;
      if (errors) {
        let error = JSON.parse(errors.body.message);
        const { title, message, errorType } = error;
        this.dispatchEvent(
          utils.toastMessageWithTitle(title, message, errorType)
        );
      } else {
        this.dispatchEvent(
          utils.toastMessage(
            CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
            CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
          )
        );
      }
    }
  }

  // Check Supervisor Login
  get profileName() {
    return sharedData.getUserProfileName();
  }

  connectedCallback() {
    this.profileName;
  }

  //Get List Table Values
  @wire(fetchContactDetails, {
    applicationId: "$caseId"
  })
  allContactData(result) {
    this.wiredContactDetails = result;

    if (result.data) {
      this.totalContactsCount = result.data.length;

      if (this.totalContactsCount == 0) this.noRecordsFound = true;
      else this.noRecordsFound = false;

      this.totalContacts = result.data.map(row => {
        return {
          Id: row.Id,
          ContactType__c: row.ContactType__c,
          Location__c: row.Location__c,
          Source__c: row.Source__c,
          Date: utils.formatDate(row.Date__c),
          ContactName__c: row.ContactName__c,
          ContactRole__c: row.ContactRole__c,
          Phone__c: row.Phone__c,
          Email__c: row.Email__c,
          Notes: row.Narrative__c,
          descriptionClass: row.Narrative__c
            ? "btnbdrIpad blue"
            : "svgRemoveicon",
          buttonDisabled: this.profileName == "Supervisor" ? false : true
        };
      });

      this.pageData();
      refreshApex(this.wiredContactDetails);
      this.Spinner = false;
    } else if (result.error) {
      let errors = result.error;
      if (errors) {
        let error = JSON.parse(errors.body.message);
        const { title, message, errorType } = error;
        this.dispatchEvent(
          utils.toastMessageWithTitle(title, message, errorType)
        );
      } else {
        this.dispatchEvent(
          utils.toastMessage(
            CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
            CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
          )
        );
      }
    }
  }

  //Pagination
  pageData() {
    let page = this.page;
    let perpage = this.perpage;
    let startIndex = page * perpage - perpage;
    let endIndex = page * perpage;
    this.currentPageContactData = this.totalContacts.slice(
      startIndex,
      endIndex
    );

    if (this.currentPageContactData.length == 0) {
      if (this.page != 1) {
        this.page = this.page - 1;
        this.pageData();
      }
    }
  }

  // Sorting the Data Table Column Function End
  sortBy(field, reverse, primer) {
    const key = primer
      ? function (x) {
        return primer(x[field]);
      }
      : function (x) {
        return x[field];
      };

    return function (a, b) {
      a = key(a);
      b = key(b);
      if (a === undefined) a = "";
      if (b === undefined) b = "";
      a = typeof a === "number" ? a : a.toLowerCase();
      b = typeof b === "number" ? b : b.toLowerCase();

      return reverse * ((a > b) - (b > a));
    };
  }

  //Data Table Sorting Function
  onHandleSort(event) {
    const { fieldName: sortedBy, sortDirection: sortDirection } = event.detail;
    const cloneData = [...this.currentPageContactData];

    cloneData.sort(this.sortBy(sortedBy, sortDirection === "asc" ? 1 : -1));

    this.currentPageContactData = cloneData;
    this.sortDirection = sortDirection;
    this.sortedBy = sortedBy;
  }

  //Page Change Action in Pagination Bar
  hanldeProgressValueChange(event) {
    this.page = event.detail;
    this.pageData();
  }

  //Edit/Delete Model Action
  handleRowAction(event) {
    this.showUpdates = true;
    if (
      event.detail.action.name == "Edit" ||
      event.detail.action.name == "View"
    ) {
      if (this.freezeContactScreen && event.detail.action.name == "Edit")
        return this.dispatchEvent(
          utils.toastMessage(
            `Cannot edit contact notes for ${this.finalStatus} application`,
            "warning"
          )
        );

      let selectedContactId = event.detail.row.Id;

      this._newContactModal = event.detail.action.name == "Edit" ? true : false;
      this.viewModel = event.detail.action.name == "View" ? true : false;
      this.title =
        event.detail.action.name == "Edit"
          ? "EDIT CONTACT DETAILS"
          : this.title;
      this.btnLabel =
        event.detail.action.name == "Edit" ? "UPDATE" : this.btnLabel;

      this.disabled = false;
      this.showUpdates = true;

      editContactDetails({
        selectedContactId
      })
        .then(result => {
          if (result.length > 0) {
            this.contactDetails = {
              ContactType__c: result[0].ContactType__c,
              Source__c: result[0].Source__c,
              Date__c: result[0].Date__c,
              ContactName__c: result[0].ContactName__c,
              ContactRole__c: result[0].ContactRole__c,
              Email__c: result[0].Email__c,
              Phone__c: result[0].Phone__c,
              Narrative__c: result[0].Narrative__c,
              LastModifiedBy: result[0].LastModifiedBy.Name,
              LastModifiedDate: utils.formatDate(result[0].LastModifiedDate)
            };
            this.contactId = result[0].Id;
          } else {
            this.dispatchEvent(
              utils.toastMessage("Error in getting contact details", "error")
            );
          }
        })
        .catch(errors => {
          if (errors) {
            let error = JSON.parse(errors.body.message);
            const { title, message, errorType } = error;
            this.dispatchEvent(
              utils.toastMessageWithTitle(title, message, errorType)
            );
          } else {
            this.dispatchEvent(
              utils.toastMessage(
                CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
                CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
              )
            );
          }
        });
    } else if (event.detail.action.name == "Delete") {
      if (this.freezeContactScreen)
        return this.dispatchEvent(
          utils.toastMessage(
            `Cannot delete contact notes for ${this.finalStatus} application`,
            "warning"
          )
        );

      this.deleteModel = true;
      this.deleteId = event.detail.row.Id;
    } else if (event.detail.action.name == "Descpopup") {
      this.descModel = true;
      this.Notesdescription = event.detail.row.Notes;
    }
  }

  handleDelete() {
    deleteRecord(this.deleteId)
      .then(() => {
        this.deleteModel = false;
        this.dispatchEvent(
          utils.toastMessage("Contact Details deleted successfully", "Success")
        );

        return refreshApex(this.wiredContactDetails);
      })
      .catch(error => {
        this.deleteModel = false;
        this.dispatchEvent(
          utils.toastMessage("Error in delete contact details", "error")
        );

        return refreshApex(this.wiredContactDetails);
      });
  }

  closeDeleteModal() {
    this.deleteModel = false;
  }

  //Fields On Change Event Handler
  contactOnChange(event) {
    if (event.target.name == "ContactType") {
      this.contactDetails.ContactType__c = event.target.value;
    } else if (event.target.name == "Purpose") {
      this.contactDetails.Source__c = event.target.value;
    } else if (event.target.name == "Date") {
      this.contactDetails.Date__c = event.target.value;
    } else if (event.target.name == "ContactName") {
      this.contactDetails.ContactName__c = event.target.value;
    } else if (event.target.name == "Phone") {
      event.target.value = event.target.value.replace(/(\D+)/g, "");
      this.contactDetails.Phone__c = utils.formattedPhoneNumber(
        event.target.value
      );
    } else if (event.target.name == "Email") {
      this.contactDetails.Email__c = event.target.value;
    } else if (event.target.name == "Narrative") {
      this.contactDetails.Narrative__c = event.target.value;
    } else if (event.target.name == "lastModifiedBy") {
      this.contactDetails.LastModifiedBy = event.target.value;
    } else if (event.target.name == "lastModifiedDate") {
      this.contactDetails.LastModifiedDate = event.target.value;
    }
  }

  saveContactDetails() {
    this.disabled = true;
    const allValid = [
      ...this.template.querySelectorAll("lightning-input"),
      ...this.template.querySelectorAll("lightning-combobox")
    ].reduce((validSoFar, inputFields) => {
      inputFields.reportValidity();
      return validSoFar && inputFields.checkValidity();
    }, true);

    if (allValid) {
      if (
        this.contactDetails &&
        this.contactDetails.Phone__c &&
        this.contactDetails.Phone__c.length != 14
      ) {
        this.disabled = false;
        return this.dispatchEvent(
          utils.toastMessage("Invalid Phone", "warning")
        );
      }

      const fields = this.contactDetails;
      if (!this.contactId) {
        fields[CASE_OBJECT_FIELD.fieldApiName] = this.caseId;
        const recordInput = {
          apiName: CONTACTNOTES_OBJECT.objectApiName,
          fields
        };
        this.showUpdates = false;
        createRecord(recordInput)
          .then(result => {
            this.showUpdates = false;
            this.closeModal();
            this.dispatchEvent(
              utils.toastMessage(
                "Contact Notes has been saved successfully",
                "Success"
              )
            );
            this.disabled = false;

            return refreshApex(this.wiredContactDetails);
          })
          .catch(errors => {
            this.disabled = false;

            if (errors) {
              let error = JSON.parse(errors.body.message);
              const { title, message, errorType } = error;
              this.dispatchEvent(
                utils.toastMessageWithTitle(title, message, errorType)
              );
            } else {
              this.dispatchEvent(
                utils.toastMessage(
                  CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
                  CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
                )
              );
            }
          });
      } else {
        this.showUpdates = true;
        fields[ID_FIELD.fieldApiName] = this.contactId;
        delete fields.LastModifiedBy;
        delete fields.LastModifiedDate;
        const recordInput = { fields };

        updateRecord(recordInput)
          .then(result => {
            this._newContactModal = false;
            this.showUpdates = true;
            this.contactDetails = {};
            this.disabled = false;
            this.dispatchCustomEventToHandleAddContactsInfoPopUp();
            this.dispatchEvent(
              utils.toastMessage(
                "Contact Notes has been saved successfully",
                "Success"
              )
            );
            return refreshApex(this.wiredContactDetails);
          })
          .catch(errors => {
            this.disabled = false;
            if (errors) {
              let error = JSON.parse(errors.body.message);
              const { title, message, errorType } = error;
              this.dispatchEvent(
                utils.toastMessageWithTitle(title, message, errorType)
              );
            } else {
              this.dispatchEvent(
                utils.toastMessage(
                  CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
                  CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
                )
              );
            }
          });
      }
    } else {
      this.disabled = false;
      this.dispatchEvent(
        utils.toastMessage(constPopupVariables.VALIDATION_WARN, "Warning")
      );
    }
  }

  closeDescModal() {
    this.descModel = false;
  }

  getRowActions(row, doneCallback) {
    const actions = [];
    if (row["buttonDisabled"] == true) {
      actions.push(
        {
          label: "View",
          name: "View",
          iconName: "utility:preview",
          target: "_self"
        },
        {
          label: "Edit",
          name: "Edit",
          iconName: "utility:edit",
          target: "_self"
        },
        {
          label: "Delete",
          name: "Delete",
          iconName: "utility:delete",
          target: "_self"
        }
      );
    } else {
      actions.push({
        label: "View",
        name: "View",
        iconName: "utility:preview",
        target: "_self"
      });
    }
    // simulate a trip to the server
    setTimeout(() => {
      doneCallback(actions);
    }, 200);
  }
}
