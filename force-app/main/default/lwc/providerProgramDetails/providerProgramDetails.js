import {
    LightningElement,
    track,
    wire,
    api
} from 'lwc';
import getPickListValues from '@salesforce/apex/ApplicationProviderProgramDetails.getPickListValues';
// import GENDER from '@salesforce/schema/Application__c.Gender__c';
// import CAPACITY from '@salesforce/schema/Application__c.Capacity__c';
// import PROGRAM from '@salesforce/schema/Application__c.Program__c';
// import PROGRAMTYPE from '@salesforce/schema/Application__c.ProgramType__c';
// import MinAge from '@salesforce/schema/Application__c.MinAge__c';
// import MaxAge from '@salesforce/schema/Application__c.MaxAge__c';
// import MinIQ from '@salesforce/schema/Application__c.MinIQ__c';
// import MaxIQ from '@salesforce/schema/Application__c.MaxIQ__c';
// import Profit from '@salesforce/schema/Account.Profit__c';
// import Accrediation from '@salesforce/schema/Account.Accrediation__c';
// import Name from '@salesforce/schema/Account.Name';
// import TaxId from '@salesforce/schema/Account.TaxId__c';
//import Programsave from '@salesforce/apex/ApplicationProviderProgramDetails.Programsave';
import updateapplicationdetails from '@salesforce/apex/ApplicationProviderProgramDetails.updateOrInsertSOQL';
import updateOrInsertSOQLnew from '@salesforce/apex/ApplicationProviderProgramDetails.updateOrInsertSOQLnew';
import fetchDataForProgramDetails from '@salesforce/apex/ApplicationProviderProgramDetails.fetchDataForProgramDetails';
//import getAddress from '@salesforce/apex/ApplicationProviderProgramDetails.getAddress';
//import getProgramAdressRowId from '@salesforce/apex/ApplicationProviderProgramDetails.getProgramAdressRowId';
import {
    utils
} from "c/utils";
import * as sharedData from "c/sharedData";


// const actions = [
//     { label: 'edit', name: 'edit' },
//     { label: 'delete', name: 'delete' },
// ];


export default class ApplicationproviderprogramDetails extends LightningElement {
    @track Accreditation;
    @track Gender;
    @track GenderOptions;
    @track ProfitOptions;
    @track certificationStatus;
    @track Gender__c = GENDER;
    @track Capacity__c = CAPACITY;
    @track Program__c = PROGRAM;
    @track ProgramType__c = PROGRAMTYPE;
    @track Profit__c = Profit;
    @track Accrediation__c = Accrediation;
    @track TaxId__c = TaxId;
    @track slide1 = 1;
    @track slide2 = 20;
    @track MinAge__c = MinAge;
    @track MaxAge__c = MaxAge;
    @track MaxIQ__c = MaxIQ;
    @track MinIQ__c = MinIQ;
    @track Name = Name;
    @track Profitvalue;

    @api
    get ApplicationId() {
        return sharedData.getApplicationId();
    }
    @api
    get ProviderId() {
        return sharedData.getProviderId();
    }

    handleevent(ent) {
        this.dispatchEvent(new CustomEvent('checkvalidationinfinalpage', {
            detail: {
                valid: true
            }
        }));
    }

    connectedCallback() {

        fetchDataForProgramDetails
            ({
                fetchdataname: this.ApplicationId

            }).then(result => {
                this.Program = result[0].Program__c;
                this.ProgramType = result[0].ProgramType__c;
                this.ProgramName = result[0].Provider__r.Name;
                this.TaxId = result[0].Provider__r.TaxId__c;
                this.Accrediation = result[0].Provider__r.Accrediation__c;
                this.Gender = result[0].Gender__c;
                this.Capacity = result[0].Capacity__c;
                this.MinAgevalue = result[0].MinAge__c;
                this.MaxAgeValue = result[0].MaxAge__c;
                this.MinIQvalue = result[0].MinIQ__c;
                this.MaxIQvalue = result[0].MaxIQ__c;
                this.Profitvalue = result[0].Provider__r.Profit__c;

            })
            .catch(error => {

            });
    }




    handleChangeTax(event) {
        event.target.value = event.target.value.replace(/(\D+)/g, '');
        this.TaxId = utils.formattedTaxID(event.target.value);
        event.target.value = this.TaxId;

    }
    handleChangeProgramType(event) {
        this.ProgramType = event.detail.value;
    }

    handleChangeProgram(event) {
        this.Program = event.detail.value;
    }


    handleCapacityChange(event) {
        this.Capacity = event.detail.value;
    }

    handleChangeProgramName(event) {
        this.ProgramName = event.detail.value;
    }

    handleChangeAccreditation(event) {
        this.Accreditation = event.detail.value;
    }

    handlechangeminAge(event) {

        this.MinAgevalue = event.detail.value;

    }

    handlechangemaxAge(event) {
        this.MaxAgeValue = event.detail.value;
        //return inp2.value
    }

    handlechangeminIQ(event) {

        this.MinIQvalue = event.detail.value;
    }

    handlechangemaxIQ(event) {
        this.MaxIQvalue = event.detail.value;
        //return inp2.value
    }




    @wire(getPickListValues, {
        objInfo: {
            'sobjectType': 'Application__c'
        },
        picklistFieldApi: 'Gender__c'
    })
    wireCheckPicklistGender({
        error,
        data
    }) {
        /*eslint-disable*/

        if (data) {
            this.GenderOptions = data;
        } else if (error) {

        }
    }
    handleChangegender(event) {
        this.Gender = event.detail.value;
    }




    @wire(getPickListValues, {
        objInfo: {
            'sobjectType': 'Account'
        },
        picklistFieldApi: 'Profit__c'
    })
    wireCheckPicklistProfitvalue({
        error,
        data
    }) {
        /*eslint-disable*/

        if (data) {
            this.ProfitOptions = data;

        } else if (error) {


        }
    }

    handleRadioButton(event) {
        this.Profitvalue = event.detail.value;
    }


    get Accreditionoptions() {
        return [{
                label: "Commission on Accreditation of Rehabilitation Facilities",
                value: "Commission on Accreditation of Rehabilitation Facilities"
            },
            {
                label: "Council on Accreditation",
                value: "Council on Accreditation"
            },
            {
                label: "Joint Commission on Accreditation of Healthcare Organizations",
                value: "Joint Commission on Accreditation of Healthcare Organizations"
            }
        ];

    }





    handleClicksave() {

        let myobjcase = {
            'sobjectType': 'Application__c'
        };
        myobjcase.Gender__c = this.Gender;
        myobjcase.Capacity__c = this.Capacity;
        myobjcase.Id = this.ApplicationId;
        myobjcase.MinAge__c = this.MinAgevalue;
        myobjcase.MaxAge__c = this.MaxAgeValue;
        myobjcase.MinIQ__c = this.MinIQvalue;
        myobjcase.MaxIQ__c = this.MaxIQvalue;
        myobjcase.ProgramType = this.ProgramType;
        myobjcase.Program = this.Program;
        updateapplicationdetails({
                objSobjecttoUpdateOrInsert: myobjcase
            })
            .then(result => {
                this.MinAgevalue = {};
                this.MaxAgeValue = {};
                this.dispatchEvent(utils.toastMessage("Application Saved Successfully", "success"));
            })
        let myobjcasenew = {
            'sobjectType': 'Account'
        };
        myobjcasenew.Accrediation__c = this.Accreditation;
        myobjcasenew.Profit__c = this.Profitvalue;
        // myobjcasenew.Name = this.Name;
        myobjcasenew.TaxId__c = this.TaxId;
        myobjcasenew.Id = this.ProviderId;
        updateOrInsertSOQLnew({
                objSobjecttoUpdateOrInsert: myobjcasenew
            })
            .then(result => {
                this.Profitvalue = {};
                this.Accreditation = {};
                this.Name = {};
                this.TaxId = {};


                this.dispatchEvent(utils.toastMessage("Application Saved Successfully", "success"));
            })
        // .catch(error => {
        //     this.dispatchEvent(utils.toastMessage("Error in creating record", "error"));

        // });
    }
}