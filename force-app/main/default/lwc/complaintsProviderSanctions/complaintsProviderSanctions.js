/**
 * @Author        : Saranraj
 * @CreatedOn     : July 6 ,2020
 * @Purpose       : Proivider Sanctions complaints
 **/
import { LightningElement, track, api, wire } from "lwc";
import getSanctionsData from "@salesforce/apex/ComplaintsProviderSanctions.getSanctionsData";
import { CJAMS_CONSTANTS } from "c/constants";
import { utils } from "c/utils";
import images from "@salesforce/resourceUrl/images";
import * as sharedData from "c/sharedData";
const actions = [
  {
    label: "View",
    name: "View",
    iconName: "utility:preview",
    target: "_self"
  }
];
export default class ComplaintsProviderSanctions extends LightningElement {
  @track program;
  @track complaintsTabelData = [];
  @track viewModalOpen = false;
  @track sanctionsViewStartDate;
  @track sanctionsViewEndDate;
  @track sanctionsViewDocument;
  @track sanctionsViewDelivery;
  @track suspensionModal;
  @track limitationViewRevocationDate;
  @track limitationViewDocumentData;
  @track limitationViewDeliveryData;
  @track rovocationModal;
  @track limitionModal;
  @track numberOfYouthServed;
  @track programEndDate;
  @track youthEndDate;
  @track populationEffectiveDate;
  @track populationEndDate;
  @track youthLimitationReason;
  @track providerAssignedService;
  @track populationLimitationReason;
  @track programEffectiveDate;
  @track programLimitationReason;
  @track gender;
  @track maxAge;
  @track minAge;
  @track delivery;
  @track deliveryLength = false;
  @track startDate;
  @track endDate;
  @track showNoDataImage;
  @track noRecordsFound = false;
  attachmentIcon = images + "/deficiencyIcon.svg";
  get columns() {
    return [
      {
        label: "Complaint Number",
        fieldName: "complaintNumber__c"
      },
      {
        label: "Cap Number",
        fieldName: "capNumber__c"
      },
      {
        label: "program Type",
        fieldName: "programType__c"
      },
      {
        label: "Type Of Sanction",
        fieldName: "typeOfSanction__c"
      },
      {
        label: "",
        type: "action",
        typeAttributes: {
          rowActions: actions
        },
        cellAttributes: {
          iconName: "utility:threedots_vertical",
          iconAlternativeText: "Actions",
          class: "tripledots blueIcons"
        }
      }
    ];
  }
  get getProviderId() {
    return sharedData.getProviderId();
  }
  @wire(getSanctionsData, {
    providerId: "$getProviderId"
  })
  ggetSanctionsDataFromApex(result) {
    this.refreshcomplaintsData = result;
    let temp = [];
    try {
      result.data &&
        result.data.map((item) => {
          temp.push({
            Id: item.Id,
            complaintNumber__c: item.Complaint__r
              ? item.Complaint__r.CaseNumber
              : "",
            programType__c: item.Complaint__r
              ? item.Complaint__r.License__r.ProgramType__c
              : "",
            capNumber__c: item.Complaint__r.CAPNumber__r
              ? item.Complaint__r.CAPNumber__r.Name
              : "",
            typeOfSanction__c: item.RecordType.Name,
            Delivery__c: item.Delivery__c,
            Document__c: item.Document__c,
            EndDate__c: utils.dateFormat(item.EndDate__c),
            StartDate__c: utils.dateFormat(item.StartDate__c),
            Numberofyouthserved__c: item.Numberofyouthserved__c,
            MinAge__c: item.MinAge__c,
            MaxAge__c: item.MaxAge__c,
            PopulationEffectiveDate__c: utils.dateFormat(
              item.PopulationEffectiveDate__c
            ),
            Gender__c: item.Gender__c,
            PopulationEndDate__c: utils.dateFormat(item.PopulationEndDate__c),
            PopulationLimitationReason__c: item.PopulationLimitationReason__c,
            ProgramEffectiveDate__c: utils.dateFormat(
              item.ProgramEffectiveDate__c
            ),
            ProgramEndDate__c: utils.dateFormat(item.ProgramEndDate__c),
            ProgramLimitationReason__c: item.ProgramLimitationReason__c,
            ProviderAssignedService__c: item.ProviderAssignedService__c,
            YouthEffectiveDate__c: utils.dateFormat(item.YouthEffectiveDate__c),
            YouthEndDate__c: utils.dateFormat(item.YouthEndDate__c),
            YouthLimitationReason__c: item.YouthLimitationReason__c
          });
        });
    } catch (err) {
      this.dispatchEvent(utils.handleError(err));
    }
    this.complaintsTabelData = temp;
    this.showNoDataImage = this.complaintsTabelData.length === 0;
  }
  handleRowAction = (event) => {
    
    if (event.detail.row.typeOfSanction__c === "Suspension") {
      this.viewModalOpen = true;
      this.suspensionModal = true;
      this.rovocationModal = false;
      this.limitionModal = false;
      this.sanctionsViewStartDate = event.detail.row.StartDate__c;
      this.sanctionsViewEndDate = event.detail.row.EndDate__c;
      this.sanctionsViewDocument = event.detail.row.Document__c;
      this.sanctionsViewDelivery = event.detail.row.Delivery__c;
      this.deliveryLength =
        event.detail.row.Delivery__c === undefined ? true : false;
    } else if (event.detail.row.typeOfSanction__c === "Revocation") {
      this.viewModalOpen = true;
      this.limitionModal = false;
      this.rovocationModal = true;
      this.suspensionModal = false;
      this.limitationViewRevocationDate = event.detail.row.StartDate__c;
      this.limitationViewDocumentData = event.detail.row.Document__c;
      this.limitationViewDeliveryData = event.detail.row.Delivery__c;
    } else if (event.detail.row.typeOfSanction__c === "Limitation") {
      this.viewModalOpen = true;
      this.limitionModal = true;
      this.suspensionModal = false;
      this.rovocationModal = false;
      this.numberOfYouthServed = event.detail.row.Numberofyouthserved__c;
      this.programEndDate = event.detail.row.ProgramEndDate__c;
      this.youthEndDate = event.detail.row.YouthEndDate__c;
      this.populationEffectiveDate =
        event.detail.row.PopulationEffectiveDate__c;
      this.populationEndDate = event.detail.row.PopulationEndDate__c;
      this.youthLimitationReason = event.detail.row.YouthLimitationReason__c;
      this.programEffectiveDate = event.detail.row.ProgramEffectiveDate__c;
      this.populationLimitationReason =
        event.detail.row.PopulationLimitationReason__c;
      this.providerAssignedService =
        event.detail.row.ProviderAssignedService__c;
      this.gender = event.detail.row.Gender__c;
      this.maxAge = event.detail.row.MaxAge__c;
      this.minAge = event.detail.row.MinAge__c;
      this.delivery = event.detail.row.Delivery__c;
      this.startDate = event.detail.row.StartDate__c;
      this.endDate = event.detail.row.EndDate__c;
      this.programLimitationReason =
        event.detail.row.ProgramLimitationReason__c;
    }
  };
  handleCloseModal = () => {
    this.viewModalOpen = false;
  };
}
