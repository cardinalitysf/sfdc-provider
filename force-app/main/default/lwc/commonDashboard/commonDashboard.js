/**
 * @Author        : Preethi Bojarajan
 * @CreatedOn     : Jun 01,2020
 * @Purpose       : Reusable Dashboard With Card Clickable For Public And Private Btn View
 * @updatedBy     :
 * @updatedOn     :
 **/

import { LightningElement, track, api, wire } from "lwc";
import { loadStyle } from "lightning/platformResourceLoader";
import myResource from "@salesforce/resourceUrl/styleSheet";
import * as sharedData from "c/sharedData";
import {
    CJAMS_CONSTANTS
} from "c/constants";

//Card Values For Translation
import PUBLIC_LABEL from '@salesforce/label/c.Public';
import PRIVATE_LABEL from '@salesforce/label/c.Private';
import { utils } from "c/utils";
import { USER_PROFILE_NAME } from "c/constants";


export default class CommonDashboard extends LightningElement {

    @track public = PUBLIC_LABEL;
    @track private = PRIVATE_LABEL;

    //Card Tracks
    @api byTypes = [];
    @api byClasses = [];
    @track initLoad = false;
    @api redirectToPublicProvider;
    //Table Tracks
    @api searchKey = "";
    @api tableRecords = [];
    @api columns;
    @track currentPageRecords = [];
    @track tableTopAlign = false;
    @track noRecordsFound = false;
    //Pagination Tracks
    @api totalRecordsCount = 0;
    perpage = 10;
    setPagination = 5;
    @track page = 1;
    @track Spinner = true;
    // Sort Function Tracks
    @track defaultSortDirection = "asc";
    @track sortDirection = "asc";
    @track noRecordsFoundMessage = CJAMS_CONSTANTS.NO_RECORDS_FOUND;

    //Private Public Btns
    @track showPublicPrivateBtn = true;
    @track showPrivateBtn = false;
    @track showPublicBtn = true;
    @api activePage;
    @api hidePrivatePublic = false;
    @api caseworkFlow = false;

    //Depending Dashboard Header
    @api providerShow = false;
    @api applicationShow = false;
    @api supervisorFlow = false;

    /* Style Sheet Loading */
    renderedCallback() {
        if (this.initLoad) return;
        this.initLoad = true;

        Promise.all([loadStyle(this, myResource + "/styleSheet.css")]);
        /* For Card Color Depending Status */
        let allCards = this.template.querySelectorAll(".slds-card");
        if (this.byClasses.length === allCards.length) {
            for (let i = 0; i < allCards.length; i++) {
                allCards[i].classList.add(this.byClasses[i].title);
                if (this.byClasses[i].init == true)
                    allCards[i].classList.add(this.byClasses[i].card);
            }
        }
        setTimeout(() => {
            this.pageData();
        }, 1500);
        if (this.activePage === undefined || this.activePage === PUBLIC_LABEL) {
            this.showPublicBtn = true;
            this.showPrivateBtn = false;
        } else if (this.activePage === PRIVATE_LABEL) {
            this.showPublicBtn = false;
            this.showPrivateBtn = true;
            this.template.querySelector('.btn1').className = 'btn1';
            this.template.querySelector('.btn2').className = 'btn2 active';
        }
    }

    handleByStatus(event) {
        this.Spinner = true;
        let index = 0;
        if (event.target.dataset.id !== undefined)
            index = this.byTypes.findIndex((b) => b.Id === event.target.dataset.id);
        else {
            let statusName = null;
            if (event.target.outerText.includes("\n"))
                statusName = event.target.outerText.split("\n")[0];
            else statusName = event.target.outerText;
            index =
                this.byTypes.findIndex((b) => b.title == statusName) >= 0 ?
                    this.byTypes.findIndex((b) => b.title == statusName) :
                    this.byTypes.findIndex((b) => b.count == statusName);
        }

        const selectedEvent = new CustomEvent("cardvaluechange", {
            detail: index
        });
        this.dispatchEvent(selectedEvent);

        let datas = this.template.querySelectorAll(".slds-card");
        for (let i = 0; i < datas.length; i++) {
            if (index === i) datas[i].classList.add(this.byClasses[i].card);
            else datas[i].classList.remove(this.byClasses[i].card);
        }
        this.page = 1;
        setTimeout(() => {
            this.pageData();
        }, 1000);
    }

    /* Pagination Function Start */
    pageData = () => {
        let page = this.page;
        let perpage = this.perpage;
        let startIndex = page * perpage - perpage;
        let endIndex = page * perpage;
        this.currentPageRecords = this.tableRecords.slice(startIndex, endIndex);
        this.noRecordsFound = this.currentPageRecords.length > 0 ? false : true;
        this.Spinner = false;
    };

    sortedPageData = (datas) => {
        let page = this.page;
        let perpage = this.perpage;
        let startIndex = page * perpage - perpage;
        let endIndex = page * perpage;
        this.currentPageRecords = datas.slice(startIndex, endIndex);
    };

    hanldeProgressValueChange(event) {
        this.page = event.detail;
        this.pageData();
    }

    /* Pagination Function End */

    // Sorting the Data Table Column Function End
    sortBy(field, reverse, primer) {
        const key = primer ?
            function (x) {
                return primer(x[field]);
            } :
            function (x) {
                return x[field];
            };

        return function (a, b) {
            a = key(a);
            b = key(b);
            if (a === undefined) a = "";
            if (b === undefined) b = "";
            a = typeof a === "number" ? a : a.toLowerCase();
            b = typeof b === "number" ? b : b.toLowerCase();

            return reverse * ((a > b) - (b > a));
        };
    }
    onHandleSort(event) {
        const {
            fieldName: sortedBy,
            sortDirection: sortDirection
        } = event.detail;
        const cloneData = [...this.tableRecords];

        cloneData.sort(this.sortBy(sortedBy, sortDirection === "asc" ? 1 : -1));
        this.sortedPageData(cloneData);
        this.sortDirection = sortDirection;
        this.sortedBy = sortedBy;
    }
    // Sorting the Data Table Column Function End

    //Search Function Start
    @api
    searchFn() {
        setTimeout(() => {
            this.pageData();
        }, 1000);
    }
    //Search Funtion End

    //Redirection Function Start
    handleRowAction(event) {
        if (this.supervisorFlow === false) {
            if (event.detail.action.name === 'Complaint') {
                const onClickId = new CustomEvent("redirecttonextpage", {
                    detail: {
                        first: false,
                        redirectId: event.detail.row.Id,
                        providerId: event.detail.row.providerId,
                        status: event.detail.row.DataBaseStatus,
                        providerName: event.detail.row.ProviderId,
                        LicenseId:event.detail.row.License
                    }
                });
                this.dispatchEvent(onClickId);
            } else if (event.detail.action.name === 'Provider') {
                const onClickId = new CustomEvent("redirecttonextpage", {
                    detail: {
                        first: 'Provider',
                        redirectId: event.detail.row.Id,
                        providerId: event.detail.row.providerId,
                        status: event.detail.row.DataBaseStatus,
                        providerName: event.detail.row.ProviderId
                    }
                });
                this.dispatchEvent(onClickId);

            } else {
                const onClickId = new CustomEvent("redirecttonextpage", {
                    detail: {
                        first: false,
                        redirectId: event.detail.row.Id,
                        providerId: event.detail.row.providerId,
                        status: event.detail.row.Status,
                        providerName: event.detail.row.ProviderId
                    }
                });
                this.dispatchEvent(onClickId);
            }
        } else {
            if (event.detail.action.name === undefined) {
                const onClickId = new CustomEvent("redirecttonextpage", {
                    detail: {
                        first: false,
                        redirectId: event.detail.row.Id,
                        providerId: event.detail.row.providerId,
                        status: event.detail.row.Status
                    }
                });
                this.dispatchEvent(onClickId);
            } else if (event.detail.action.name === 'contract' || event.detail.action.name === 'monitoring') {
                const onClickId = new CustomEvent("redirecttonextpage", {
                    detail: {
                        first: false,
                        name: event.detail.action.name,
                        redirectId: event.detail.row.Id,
                        providerId: event.detail.row.providerId,
                        status: event.detail.row.Status
                    }
                });
                this.dispatchEvent(onClickId);
            } else if (event.detail.action.name === 'Complaint') {
                const onClickId = new CustomEvent("redirecttonextpage", {
                    detail: {
                        first: false,
                        redirectId: event.detail.row.Id,
                        providerId: event.detail.row.providerId,
                        status: event.detail.row.DataBaseStatus,
                        providerName: event.detail.row.ProviderId
                    }
                });
                this.dispatchEvent(onClickId);

            } else if (event.detail.action.name === 'Provider') {
                const onClickId = new CustomEvent("redirecttonextpage", {
                    detail: {
                        first: 'Provider',
                        redirectId: event.detail.row.Id,
                        providerId: event.detail.row.providerId,
                        status: event.detail.row.DataBaseStatus,
                        providerName: event.detail.row.ProviderId
                    }
                });
                this.dispatchEvent(onClickId);

            } else {
                const onClickId = new CustomEvent("redirecttonextpage", {
                    detail: {
                        first: false,
                        redirectId: event.detail.row.Id,
                        providerName: event.detail.row.ProviderId,
                        status: event.detail.row.Status
                    }
                });
                this.dispatchEvent(onClickId);
            }
        }
    }
    //Redirection Function End

/*     get supervisorFlow() {
        let userProfile = sharedData.getUserProfileName();
        if (userProfile === USER_PROFILE_NAME.USER_SUPERVISOR || USER_PROFILE_NAME.USER_CASE_WRKR) return true;
        else return false;
    } */

    //   handleRedirect functionality start
    handleRedirect = (event) => {
        const redirect = new CustomEvent("redirect", {
            detail: event.detail
        });
        this.dispatchEvent(redirect);
    };
    //   handleRedirect functionality end

    handleClick(event) {
        this.activePage = event.target.name;

        let allCards = this.template.querySelectorAll(".slds-card");
        if (this.byClasses.length === allCards.length) {
            for (let i = 0; i < allCards.length; i++) {
                if (i === 0) allCards[i].classList.add(this.byClasses[i].card);
                else allCards[i].classList.remove(this.byClasses[i].card);
            }
        }

        const publicPrivateToggle = new CustomEvent("publicprivate", {
            detail: event.target.name
        });
        this.dispatchEvent(publicPrivateToggle);

        if (this.activePage === undefined || this.activePage === PUBLIC_LABEL) {
            this.showPublicBtn = true;
            this.showPrivateBtn = false;
            this.template.querySelector('.btn1').className = 'btn1 active';
            this.template.querySelector('.btn2').className = 'btn2';
        } else if (this.activePage === PRIVATE_LABEL) {
            this.showPublicBtn = false;
            this.showPrivateBtn = true;
            this.template.querySelector('.btn1').className = 'btn1';
            this.template.querySelector('.btn2').className = 'btn2 active';
        }
        setTimeout(() => {
            this.pageData();
        }, 1500);
    }
}