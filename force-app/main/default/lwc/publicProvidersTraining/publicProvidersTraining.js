/**
 * @Author        : Pratheeba V
 * @CreatedOn     : June 19, 2020
 * @Purpose       : Public Providers Training tab 
 **/
import {
    LightningElement,
    track,
    api,
    wire
} from 'lwc';
import getHouseholdMembers from '@salesforce/apex/PublicProvidersTraining.getHouseholdMembers';
import getTrainingDetails from '@salesforce/apex/PublicProvidersTraining.getTrainingDetails';
import getSessionTraining from '@salesforce/apex/PublicProvidersTraining.getSessionTraining';
import InsertUpdateSessionInfo from '@salesforce/apex/PublicProvidersTraining.InsertUpdateSessionInfo';
import viewSessionDetails from '@salesforce/apex/PublicProvidersTraining.viewSessionDetails';
import getSessionAttendedHours from '@salesforce/apex/PublicProvidersTraining.getSessionAttendedHours';
import selectActor from '@salesforce/apex/PublicProvidersTraining.selectActor';

import {
    getRecord,
    updateRecord,
    deleteRecord
} from "lightning/uiRecordApi";
import ID_FIELD from '@salesforce/schema/SessionAttendee__c.Id';
import INTENT_FIELD from '@salesforce/schema/SessionAttendee__c.IntenttoAttend__c';
import SESSIONSTATUS_FIELD from '@salesforce/schema/SessionAttendee__c.SessionStatus__c';
import ACTORID_FIELD from '@salesforce/schema/Actor__c.Id';
import TRAININGCOMPLETED_FIELD from '@salesforce/schema/Actor__c.TrainingCompleted__c';
import APPLICATIONSTATUS_FIELD from '@salesforce/schema/Application__c.Status__c';
import {
    utils
} from "c/utils";
import * as sharedData from 'c/sharedData';
import images from '@salesforce/resourceUrl/images';
import { CJAMS_CONSTANTS } from 'c/constants';
const columns = [{
    label: 'TRAINING NUMBER',
    fieldName: 'TrainingName__c',
    type: 'text'
},
{
    label: 'TRAINING DATE',
    fieldName: 'Date__c',
    type: 'date',
    typeAttributes: { month: '2-digit', day: '2-digit', year: 'numeric' }
},
{
    label: 'START TIME',
    fieldName: 'StartTime__c',
    type: 'text'
},
{
    label: 'END TIME',
    fieldName: 'EndTime__c',
    type: 'text'
},
{
    label: 'TOPIC DESCRIPTION',
    fieldName: 'Description__c',
    type: 'button',
    typeAttributes: {
        label: {
            fieldName: 'Description__c'
        },
        name: 'Descpopup',
        title: {
            fieldName: 'Description__c'
        }
    },
    cellAttributes: {
        iconName: 'utility:description',
        iconAlternativeText: 'Description',
        class: {
            fieldName: 'descriptionClass'
        }
    }
}
];

const sessionInfoColumns = [{
    label: 'TRAINING NUMBER',
    fieldName: 'Training__c',
    type: 'text'
},
{
    label: 'DATE',
    fieldName: 'Date__c',
    type: 'date',
    typeAttributes: { month: '2-digit', day: '2-digit', year: 'numeric' }
},
{
    label: 'TRAINING TYPE',
    fieldName: 'SessionType__c',
    type: 'text'
},
{
    label: 'DURATION',
    fieldName: 'Duration__c',
    type: 'text'
},
{
    label: 'TRAINEE',
    fieldName: 'Trainee',
    type: 'text'
},
{
    label: 'ROLE',
    fieldName: 'Role__c',
    type: 'text'
},
{
    label: 'JURISDICTION',
    fieldName: 'Jurisdiction__c',
    type: 'text'
},
{
    label: 'INTENT TO ATTEND',
    fieldName: 'Id',
    type: 'toogle',
    typeAttributes: {
        isChecked: {
            fieldName: 'IntenttoAttend__c'
        },
        isDisabled: {
            fieldName: 'toggleDisabled'
        }
    }
},
{
    label: 'TRAINING STATUS',
    fieldName: 'SessionStatus__c',
    type: 'text',
    cellAttributes: {
        class: {
            fieldName: 'statusClass'
        }
    }
}
];

export default class PublicProvidersTraining extends LightningElement {
    @track activeForPType = false;
    @track pillValueForPtype = [];
    @track pillSingleValueForPtype;
    @track pillSingleValueForPtypeCount;
    @track pillSingleValueCondition = true;
    @track householdmember = [];
    columns = columns;
    sessionInfoColumns = sessionInfoColumns;
    @track norecorddisplay = true;
    @track nosessiondisplay = true;
    @track totalRecords;
    @track totalRecordsCount = 0;
    @track totalSessionCount = 0;
    @track page = 1;
    perpage = 6;
    @track currentPageTrainingData;
    @track currentPageSessionData;
    @track viewmodel = false;
    @track trainingName;
    @track trainingDate;
    @track trainingStartTime;
    @track trainingEndTime;
    @track trainingSessionStatus;
    @track trainingDescription;
    @track trainingDuration;
    attachmentIcon = images + '/people-icon.svg';
    clockIcon = images + '/clockwise-rotation.svg';
    descriptionIcon = images + '/description-icon.svg';
    sessionInfoColumns = sessionInfoColumns;
    @track totalHours;
    @track applicantName;
    @track descModel = false;
    @track topicDescriptionPopup;
    @track isApplicant = false;
    @track AssignbtnDisable = false;
    _openSessionModal;
    @track deleteModel = false;
    @track isDisableDelButton = false;
    @track totalHrsByActor;

    get applicationid() {
        return sharedData.getApplicationId();
    }

    get providerId() {
        return sharedData.getProviderId();
    }

    get reconsiderationId() {
        return sharedData.getReconsiderationId();
    }

    @api get openTrainingModal() {
        return this._openSessionModal;
    }
    set openTrainingModal(value) {
        this._openSessionModal = value;
        this.pillValueForPtype = [];
    }
    closeSessionModal() {
        this._openSessionModal = false;
        this.dispatchCustomEventToHandleAddSessionInfoPopUp();
    }
    dispatchCustomEventToHandleAddSessionInfoPopUp() {
        const tooglecmps = new CustomEvent("togglecomponents", {
            detail: {
                Component: "CloseSessionInfo"
            }
        });
        this.dispatchEvent(tooglecmps);
        this.viewmodel = false;
        this.descModel = false;
    }

    get getUserProfileName() {
        return sharedData.getUserProfileName();
    }


    @wire(getRecord, {
        recordId: "$applicationid",
        fields: [APPLICATIONSTATUS_FIELD]
    })
    wireuser({
        error,
        data
    }) {
        if (error) {
            this.error = error;
        } else if (data) {
            this.Status = data.fields.Status__c.value;
            if (this.Status) {
                if (this.getUserProfileName != 'Supervisor') {
                    if (['Caseworker Submitted', 'Approved', 'Rejected', 'Supervisor Rejected'].includes(this.Status)) {
                        this.isDisableDelButton = true;
                    }
                    else {
                        this.isDisableDelButton = false;
                    }
                }
                if (this.getUserProfileName == 'Supervisor') {
                    this.isDisableDelButton = true;
                }
            }
        }
    }

    updateActorHours() {
        selectActor({ applicationid: this.applicationid, providerid: this.providerId })
            .then((res) => {
                const fields = {};
                fields[ACTORID_FIELD.fieldApiName] = res[0].Id;
                fields[TRAININGCOMPLETED_FIELD.fieldApiName] = this.totalHrsByActor;
                const recordInput = {
                    fields
                };
                updateRecord(recordInput)
                    .then(() => {
                    })
                    .catch(errors => {
                        let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                        return this.dispatchEvent(utils.handleError(errors, config));
                    })
            }).catch(errors => {
                return this.dispatchEvent(utils.handleError(errors));
            })

    }

    attendedHoursOnLoad() {
        getSessionAttendedHours({
            reconsiderationid: this.reconsiderationId
        })
            .then((result) => {
                if (result.length > 0) {
                    this.totalHrsByActor = result[0].totalhours;
                    var hours = Math.floor(result[0].totalhours / 60);
                    var cminutes = result[0].totalhours % 60;
                    this.isApplicant = true;
                    this.totalHours = hours + '.' + cminutes + ' Hrs';
                    if (hours >= 27) {
                        const selectedEvent = new CustomEvent("sessionhoursflag", {
                            detail: {
                                sessionFlag: true,
                            }
                        });
                        this.dispatchEvent(selectedEvent);
                    }
                    this.applicantName = result[0].Name;
                } else {
                    this.totalHours = '00 Hr';
                    this.applicantName = '';
                }
            }).catch(errors => {
                return this.dispatchEvent(utils.handleError(errors));
            });
    }

    connectedCallback() {
        this.sessionOnLoad();
        this.attendedHoursOnLoad();
        getHouseholdMembers({
            providerid: this.providerId
        }).then(result => {
            result.forEach((row) => {
                this.householdmember.push(
                    {
                        Id: row.Id,
                        memberNameRole: row.Contact__r.Name + ((row.Role__c) ? ' - ' + row.Role__c : ''),
                        label: row.Contact__r.Name,
                        role: row.Role__c
                    });
            });
        }).catch(errors => {
            return this.dispatchEvent(utils.handleError(errors));
        });
    }
    trainingLoad() {
        getTrainingDetails({
            param: 'In-Service Training'
        }).then(result => {
            this.totalRecordsCount = result.length;
            if (this.totalRecordsCount == 0) {
                this.norecorddisplay = true;
            } else {
                this.norecorddisplay = false;
                let assignDataArray = [];
                if (this.currentPageSessionData) {
                    result.forEach(assign => {
                        this.currentPageSessionData.forEach(assign1 => {
                            if (assign1.TrainingId == assign.Id) {
                                assignDataArray.push(assign);
                            }
                        });
                    });
                    result = result.filter(function (val) {
                        return assignDataArray.indexOf(val) == -1;
                    });
                }


                this.totalRecords = result.map(row => {
                    return {
                        Id: row.Id,
                        TrainingName__c: row.Name,
                        Date__c: row.Date__c,
                        StartTime__c: utils.formatTime12Hr(row.StartTime__c),
                        EndTime__c: utils.formatTime12Hr(row.EndTime__c),
                        Duration__c: row.Duration__c,
                        Description__c: (row.Description__c) ? (row.Description__c.length > 13) ? row.Description__c.substring(0, 9) + '...' : row.Description__c : '',
                        descriptionClass: (row.Description__c) ? 'btnbdrRemove blue' : 'svgRemoveicon',
                        popupdescription: row.Description__c
                    }
                });
                this.pageData();
            }
        }).catch(errors => {
            return this.dispatchEvent(utils.handleError(errors));
        });
    }

    @api intentFlag = false;
    sessionOnLoad() {
        getSessionTraining({
            reconsiderationid: this.reconsiderationId,
            providerid: this.providerId
        }).then(res => {
            this.trainingLoad();
            this.totalSessionCount = res.length;
            if (this.totalSessionCount == 0) {
                this.nosessiondisplay = true;
            } else {
                this.nosessiondisplay = false;
                this.currentPageSessionData = res.map(row => {
                    return {
                        Id: row.Id,
                        Training__c: row.Training__r.Name,
                        TrainingId: row.Training__c,
                        Date__c: row.Training__r.Date__c,
                        SessionType__c: row.Training__r.SessionType__c,
                        Duration__c: row.Training__r.Duration__c + ' Hrs',
                        Trainee: row.Actor__r.Contact__r.Name,
                        Role__c: row.Actor__r.Role__c,
                        Jurisdiction__c: (row.Training__r.Jurisdiction__c) ? row.Training__r.Jurisdiction__c : '-',
                        IntenttoAttend__c: row.IntenttoAttend__c == 'Yes' ? true : false,
                        SessionStatus__c: row.SessionStatus__c,
                        statusClass: (row.SessionStatus__c == 'Completed') ? 'Approved' : (row.SessionStatus__c == 'Cancelled') ? 'Incomplete' : row.SessionStatus__c,
                        toggleDisabled: (row.SessionStatus__c == 'Completed' || this.intentFlag == true) ? true : false,
                        buttonDisabled: (this.isDisableDelButton) ? true : false
                    }
                });
            }
        }).catch(errors => {
            return this.dispatchEvent(utils.handleError(errors));
        });
        this.updateActorHours();
    }

    handleToggleChange(event) {
        event.stopPropagation();
        const fields = {};
        fields[ID_FIELD.fieldApiName] = event.detail.data.recordId;
        fields[INTENT_FIELD.fieldApiName] = (event.detail.data.isChecked) ? 'Yes' : 'No';
        fields[SESSIONSTATUS_FIELD.fieldApiName] = (event.detail.data.isChecked) ? 'Scheduled' : 'Cancelled';
        const recordInput = {
            fields
        };
        updateRecord(recordInput)
            .then(() => {
                this.dispatchEvent(utils.toastMessage("Intent to attend updated successfully", "success"));
                return this.sessionOnLoad();
            })
            .catch(error => {
                this.dispatchEvent(utils.toastMessage("Error in intent to attend", "Warning"));
            })
    }
    pageData = () => {
        let page = this.page;
        let perpage = this.perpage;
        let startIndex = (page * perpage) - perpage;
        let endIndex = (page * perpage);
        this.currentPageTrainingData = this.totalRecords.slice(startIndex, endIndex);
        if (this.currentPageTrainingData.length == 0) {
            if (this.page != 1) {
                this.page = this.page - 1;
                this.pageData();
            }
        }
    }

    hanldeProgressValueChange(event) {
        this.page = event.detail;
        this.pageData();
    }

    //Code start multi select in household members pill
    get pillvalueForPType() {
        if (this.pillValueForPtype.length > 1) {
            this.pillSingleValueForPtype = this.pillValueForPtype[0].value;
            this.pillSingleValueCondition = false;
            this.pillSingleValueForPtypeCount = this.pillValueForPtype.length - 1;
            return this.pillSingleValueForPtype;
        } else {
            this.pillSingleValueCondition = true;
            return this.pillValueForPtype;
        }
    }

    handleMouseOutButton(evr) {
        this.activeForPType = false;
    }

    get toggledivForPtype() {
        return this.pillValueForPtype.length == 0 ? false : true;
    }

    handlePType(evt) {
        this.activeForPType = this.activeForPType ? false : true;
    }

    handleRemove(evt) {
        this.pillValueForPtype = this.remove(this.pillValueForPtype, "value", evt.target.label);
        let checkboxes = this.template.querySelectorAll("[data-id=checkbox]");
        checkboxes.forEach(element => {
            if (element.value == evt.target.label) {
                if (element.checked) {
                    element.checked = false;
                }
            }
        });
        this.activeForPType = true;
    }

    get tabClassForPType() {
        return this.activeForPType ? "slds-popover slds-popover_full-width slds-popover_show" :
            "slds-popover slds-popover_full-width slds-popover_hide";
    }

    handleclickofPTypeCheckBox(evt) {
        if (evt.target.checked) {
            this.pillValueForPtype.push({
                value: evt.target.value,
                id: evt.target.id
            });
        } else {
            this.pillValueForPtype = this.remove(
                this.pillValueForPtype,
                "value",
                evt.target.value
            );
        }
        this.selectedMembers = this.pillValueForPtype;
        evt.target.checked != evt.target.checked;
    }

    remove(array, key, value) {
        const index = array.findIndex(obj => obj[key] === value);
        return index >= 0 ? [...array.slice(0, index), ...array.slice(index + 1)] :
            array;
    }
    //Code end multi select in household members pill

    handleRowSelection = event => {
        this.selectedTraining = event.detail.selectedRows;
    }

    handleDelete() {
        this.trainingLoad();
        deleteRecord(this.deleteId)
            .then(() => {
                this.deleteModel = false;
                this.attendedHoursOnLoad();
                this.trainingLoad();
                this.dispatchEvent(utils.toastMessage('Training Details deleted successfully', "success"));
                return this.sessionOnLoad();
            })
            .catch(error => {
                this.deleteModel = false;
                this.dispatchEvent(utils.toastMessage("Error in delete training details", "Warning"));
                return this.sessionOnLoad();
            });
    }

    closeDeleteModal() {
        this.deleteModel = false;
    }

    closeDescModal() {
        this.descModel = false;
    }

    handleTrainingRowAction(event) {
        this.descModel = true;
        if (event.detail.action.name == "Descpopup") {
            this.topicDescriptionPopup = event.detail.row.popupdescription;
        }
    }

    handleRowAction(event) {
        let sessionrowid = event.detail.row.Id;
        if (event.detail.action.name == "Delete") {
            this.deleteModel = true;
            this.deleteId = event.detail.row.Id;
        } else if (event.detail.action.name == "View") {
            this.title = 'VIEW TRAINING';
            this.closeSessionModal();
            this.viewmodel = true;
            this.descModel = false;
            viewSessionDetails({
                sessionid: sessionrowid
            }).then(result => {
                if (result.length > 0) {
                    this.trainingName = result[0].Training__r.Name;
                    this.trainingDate = utils.formatDate(result[0].Training__r.Date__c);
                    this.trainingStartTime = utils.formatTime12Hr(result[0].Training__r.StartTime__c);
                    this.trainingEndTime = utils.formatTime12Hr(result[0].Training__r.EndTime__c);
                    this.trainingSessionStatus = result[0].SessionStatus__c;
                    this.trainingDuration = result[0].Training__r.Duration__c + ' Hrs';
                    this.trainingDescription = result[0].Training__r.Description__c;
                } else {
                    this.trainingName = '';
                    this.trainingDate = '';
                    this.trainingStartTime = '';
                    this.trainingEndTime = '';
                    this.trainingSessionStatus = '';
                    this.trainingDuration = '';
                    this.trainingDescription = '';
                }
            }).catch(errors => {
                return this.dispatchEvent(utils.handleError(errors));
            });
        }
    }

    handleSave() {
        this.sobjectType = 'SessionAttendee__c';
        let objToInsert = [];
        if (!this.selectedMembers && !this.selectedTraining) {
            this.dispatchEvent(utils.toastMessage("Please select members and training", "Warning"));
        } else if (!this.selectedMembers) {
            this.dispatchEvent(utils.toastMessage("Please select members", "Warning"));
        } else if (!this.selectedTraining) {
            this.dispatchEvent(utils.toastMessage("Please select training", "Warning"));
        } else {

            this.selectedMembers.forEach(element => {
                var memberId = element.id.split("-");
                this.selectedTraining.forEach(trainingElem => {
                    let trainingFields = {
                        Actor__c: '',
                        Training__c: '',
                        SessionStatus__c: '',
                        Referral__c: '',
                        TotalHoursAttended__c: '',
                        Reconsideration__c: ''
                    };
                    let hoursDotMinutes = trainingElem.Duration__c;
                    let fieldArray = hoursDotMinutes.split(":");
                    let minutes = (parseInt(fieldArray[0]) * 60) + parseInt(fieldArray[1]);
                    trainingFields.Actor__c = memberId[0];
                    trainingFields.Training__c = trainingElem.Id;
                    trainingFields.SessionStatus__c = 'Scheduled';
                    trainingFields.Referral__c = this.providerId;
                    trainingFields.TotalHoursAttended__c = minutes;
                    trainingFields.Reconsideration__c = this.reconsiderationId;
                    objToInsert.push(trainingFields);
                })
            });
            if (objToInsert.length > 0) {
                InsertUpdateSessionInfo({
                    strTrainingDetails: JSON.stringify(objToInsert)
                }).then(result => {
                    this.closeSessionModal();
                    this.attendedHoursOnLoad();
                    this.trainingLoad();
                    this.dispatchEvent(utils.toastMessage("PRIDE Training Session details has been assigned successfully", "success"));
                    return this.sessionOnLoad();
                }).catch(errors => {
                    let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                    return this.dispatchEvent(utils.handleError(errors, config));
                });
            }
        }
    }

    //Sundar Adding this
    closeModal() {
        this.viewmodel = false;
    }

    /**if decision status = approved show only view otherwise show view and delete begins */
    constructor() {
        super();
        let deleteIcon = true;
        for (let i = 0; i < this.sessionInfoColumns.length; i++) {
            if (this.sessionInfoColumns[i].type == 'action') {
                deleteIcon = false;
            }
        }
        if (deleteIcon) {
            this.sessionInfoColumns.push(
                {
                    type: 'action', typeAttributes: { rowActions: this.getRowActions }, cellAttributes: {
                        iconName: 'utility:threedots_vertical',
                        iconAlternativeText: 'Actions',
                        class: "tripledots"
                    }
                }
            )
        }
    }

    getRowActions(row, doneCallback) {
        const actions = [];
        if (row['buttonDisabled'] == true) {
            actions.push({
                'label': 'View',
                'name': 'View',
                'iconName': 'utility:preview',
                'target': '_self'
            });
        } else {
            actions.push({
                'label': 'View',
                'name': 'View',
                'iconName': 'utility:preview',
                'target': '_self'
            },
                {
                    'label': 'Delete',
                    'name': 'Delete',
                    'iconName': 'utility:delete',
                    'target': '_self'
                });
        }
        // simulate a trip to the server
        setTimeout(() => {
            doneCallback(actions);
        }, 200);
    }
    /**if decision status = approved show only view otherwise show view and delete ends */
}