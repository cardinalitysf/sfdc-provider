/* eslint-disable default-case */
/**
 * @Author        : Janaswini S
 * @CreatedOn     : March 1, 2020
 * @Purpose       : This component contains Refferal Provider Add or Edit Screen
 * @Updated       : MMM (UI screen changes 09-06-2020)
 **/

import { LightningElement, track, wire, api } from "lwc";
import fetchPickListValue from "@salesforce/apex/ReferralProviderAddOrEdit.getPickListValues";
import updateproviderandReferral from "@salesforce/apex/ReferralProviderAddOrEdit.updateOrInsertSOQL";
import updateproviderandReferralReturnId from "@salesforce/apex/ReferralProviderAddOrEdit.updateOrInsertSOQLReturnId";
import getFullDetails from "@salesforce/apex/GetStreetAddressFromGoogleAPI.getPlaceDetails";
import * as sharedData from "c/sharedData";
import { utils } from "c/utils";
import getDataByCaseOrProvider from "@salesforce/apex/ReferralProviderAddOrEdit.getDataByCaseOrProvider";
//Modified by Sundar K
import getAllStates from "@salesforce/apex/ReferralProviderAddOrEdit.getStateDetails";
import getAPIStreetAddress from "@salesforce/apex/GetStreetAddressFromGoogleAPI.getSuggestions";
import getAccreditationValues from "@salesforce/apex/ReferralProviderAddOrEdit.getAccreditationValues";

import getProgramOptions from "@salesforce/apex/ReferralProviderAddOrEdit.getProgramOptions";
import getProgramTypeOptions from "@salesforce/apex/ReferralProviderAddOrEdit.getProgramTypeOptions";

import getReferralProviderStatus from "@salesforce/apex/ReferralProviderAddOrEdit.getReferralProviderStatus";
import images from "@salesforce/resourceUrl/images";
import { CJAMS_CONSTANTS } from "c/constants";

export default class ReferralProviderAddOrEdit extends LightningElement {
  active = false;
  @track valueProfit = "";
  selectedVals = [];
  @track result;
  @track RFP;
  @track FEIN;
  @track Profit;
  @track SON;
  @track getFetchDatas = [];
  @track strStreetAddress;
  @track strStreetAddresPrediction = [];
  @track value = "";
  @track Name;
  @track Phone;
  @track MobileNumber;
  @track TaxId;
  @track Fax;
  @track ParentCorporation;
  @track Program;
  @track Corporation;
  @track ProgramType;
  @track SONValue;
  @track FEINValue;
  @track proposal;
  @track FirstName;
  @track LastName;
  @track ShippingStreet;
  @track BillingStreet;

  @track Accreditation;
  @track AccountId;
  @track Priority;
  @track Origin;
  @track Status;
  buttonactive = true;
  @track Spinner = true;
  selectddl = false;
  @api receiveddateinaddoredit;
  provideridforcaseinsert = "";

  //State and County Added By Sundar K
  @track selectedState;
  @track selectedCity;
  @track selectedZipCode;
  @track selectedCounty;

  @track showStateBox = false;
  @track stateFetchResult = [];
  @track stateSearchTxt;
  @track filteredStateArr = [];

  @track showCountyBox = false;
  @track countySearchTxt;
  @track filteredCountyArr = [];
  @track countyArr = [];

  @track accreditationOptions = [];

  @api pageTypeFrom;

  @track programTypeResultArray;
  @track programOptions;
  @track isDisableProgramType = false;

  //Sundar Modified to freeze application
  @track freezeReferralScreen = false;
  @track fetchCaseforApiCall = this.caseid;

  //Sundar Modified this
  @track isSaveBtnDisabled = false;

  iconSON = images + "/clipboardIcon.svg";
  iconRFP = images + "/receiptIcon.svg";
  iconTAX = images + "/faxIcon.svg";

  get receiveddate() {
    return this.receiveddateinaddoredit;
  }
  get streetAddressLength() {
    return this.strStreetAddresPrediction != "";
  }
  get strStreetAddresPredictionUI() {
    return this.strStreetAddresPrediction;
  }
  get strStreetAddresPredictionLength() {
    if (this.strStreetAddress)
      return this.strStreetAddresPrediction.predictions ? true : false;
  }
  get tabClass() {
    return this.active
      ? "slds-dropdown slds-dropdown_length-5 slds-dropdown_fluid slds-show"
      : "slds-dropdown slds-dropdown_length-5 slds-dropdown_fluid slds-hide";
  }
  @track activeForPType = false;
  @track pillValueForPtype = [];
  get tabClassForPType() {
    return this.activeForPType
      ? "slds-popover slds-popover_full-width slds-popover_show"
      : "slds-popover slds-popover_full-width slds-popover_hide";
  }
  handlePType(evt) {
    this.activeForPType = this.activeForPType ? false : true;
  }
  get toggledivForPtype() {
    return this.pillValueForPtype.length == 0 ? false : true;
  }
  handleMouseOutButton(evr) {
    this.activeForPType = false;
  }
  handleclickofPTypeCheckBox(evt) {
    if (evt.target.checked) {
      this.pillValueForPtype.push({
        value: evt.target.value
      });
    } else {
      this.pillValueForPtype = this.remove(
        this.pillValueForPtype,
        "value",
        evt.target.value
      );
    }
    evt.target.checked != evt.target.checked;
  }
  remove(array, key, value) {
    const index = array.findIndex(obj => obj[key] === value);
    return index >= 0
      ? [...array.slice(0, index), ...array.slice(index + 1)]
      : array;
  }
  handleRemove(evt) {
    this.pillValueForPtype = this.remove(
      this.pillValueForPtype,
      "value",
      evt.target.label
    );
    let i;
    let checkboxes = this.template.querySelectorAll("[data-id=checkbox]");
    checkboxes.forEach(element => {
      if (element.value == evt.target.label) {
        if (element.checked) {
          element.checked = false;
        }
      }
    });
    this.activeForPType = true;
  }

  get pillvalueForPType() {
    return this.pillValueForPtype;
  }

  get caseid() {
    return sharedData.getCaseId();
  }

  get communication() {
    return sharedData.getCommunicationValue();
  }

  get receivedDate() {
    return sharedData.getReceivedDateValue();
  }

  @api
  get providerid() {
    return sharedData.getProviderId();
  }

  get provideridForUpdate() {
    return sharedData.getProviderId();
  }

  get tabClass() {
    return this.active
      ? "slds-dropdown slds-dropdown_length-5 slds-dropdown_fluid slds-show"
      : "slds-dropdown slds-dropdown_length-5 slds-dropdown_fluid slds-hide";
  }

  connectedCallback() {
    //Fetching values if only CaseId is Avaiable
    let caseOrProviderID = "";
    if (this.pageTypeFrom) {
      caseOrProviderID =
        this.pageTypeFrom === "case" ? this.caseid : this.providerid;
      getDataByCaseOrProvider({
        caseorProviderID: caseOrProviderID,
        prgtype: this.pageTypeFrom
      })
        .then(result => {
          let currentData = [];
          result.forEach(res => {
            let obj = {};
            this.AccountId = res.AccountId;
            if (res.AccountId) sharedData.setProviderId(res.AccountId);
            this.Origin = res.Origin;
            this.Status = res.Status;
            this.Priority = res.Priority;
            this.Program = res.Program__c;
            this.ProgramType = res.ProgramType__c;

            if (res.Account) {
              this.Name = res.Account.Name;
              this.ProviderType = res.Account.ProviderType__c;
              this.ParentCorporation = res.Account.ParentCorporation__c;
              this.Corporation = res.Account.Corporation__c;
              this.FEINValue = res.Account.FEINTaxID__c
                ? res.Account.FEINTaxID__c
                : "";
              this.SONValue = res.Account.SON__c ? res.Account.SON__c : "";
              this.proposal = res.Account.RFP__c ? res.Account.RFP__c : "";
              this.FirstName = res.Account.FirstName__c;
              this.LastName = res.Account.LastName__c;
              this.TaxId = res.Account.TaxId__c;
              this.Phone = res.Account.Phone;
              this.MobileNumber = res.Account.CellNumber__c;
              this.ShippingStreet = res.Account.ShippingStreet;
              this.strStreetAddress = res.Account.BillingStreet;
              this.Email = res.Account.Email__c;
              this.selectedState = res.Account.BillingCountry;
              this.selectedCity = res.Account.BillingCity;
              this.selectedCounty = res.Account.BillingState;
              this.selectedZipCode = res.Account.BillingPostalCode;
              this.Accreditation = res.Account.Accrediation__c;
              this.Id = res.Account.Id;
              this.valueProfit = res.Account.Profit__c;
              this.Fax = res.Account.Fax;
            }
            currentData.push(obj);
          });
          this.getFetchDatas = currentData[0];
          if (this.Program) {
            this.initialLoadProgramTypeValues();
          }
          this.Spinner = false;
        })
        .catch(errors => {
          this.disabled = false;
          if (errors) {
            let error = JSON.parse(errors.body.message);
            const { title, message, errorType } = error;
            this.dispatchEvent(
              utils.toastMessageWithTitle(title, message, errorType)
            );
          } else {
            this.dispatchEvent(
              utils.toastMessage(
                CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
                CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
              )
            );
          }
        });
    } else {
      this.Spinner = false;
    }
  }
  programTypeCheckBoxSelectUnselectDuringPageLoad() {
    let localObject = [];
    if (this.ProgramType) {
      this.ProgramType.split(/\s*,\s*/).forEach(function (myString) {
        localObject.push({
          value: myString
        });
      });

      var inp = [...this.template.querySelectorAll("[data-id=checkbox]")];
      inp.forEach(element => {
        localObject.forEach(function (item) {
          if (item.value == element.value) element.checked = true;
        });
      });
      this.pillValueForPtype = localObject;
    }
  }

  //Sundar modified to freeze referral screen
  @wire(getReferralProviderStatus, {
    caseId: "$fetchCaseforApiCall"
  })
  providerStatus({ error, data }) {
    if (data) {
      if (
        data.length > 0 &&
        (data[0].Status == "Approved" || data[0].Status == "Rejected")
      ) {
        this.freezeReferralScreen = true;
        this.isDisableProgramType = true;
        this.isSaveBtnDisabled = true;
      } else {
        this.freezeReferralScreen = false;
        this.isDisableProgramType = false;
        this.isSaveBtnDisabled = false;
      }
    }
  }

  //PickList for FEINTaxId
  @wire(fetchPickListValue, {
    objInfo: {
      sobjectType: "Account"
    },
    picklistFieldApi: "FEINTaxID__c"
  })
  wireCheckPicklistFEIN({ error, data }) {
    if (data) {
      this.FEIN = data;
    } else if (error) {
    }
  }

  //PickList for Profit Radio button
  @wire(fetchPickListValue, {
    objInfo: {
      sobjectType: "Account"
    },
    picklistFieldApi: "Profit__c"
  })
  wireCheckPicklistProfit({ error, data }) {
    if (data) {
      this.Profit = data;
    } else if (error) {
    }
  }

  //PickList for RFP
  @wire(fetchPickListValue, {
    objInfo: {
      sobjectType: "Account"
    },
    picklistFieldApi: "RFP__c"
  })
  wireCheckPicklistRFP({ error, data }) {
    if (data) {
      this.RFP = data;
    } else if (error) {
    }
  }
  get programTypeOptionsForDisplay() {
    return this.programTypeOptions;
  }
  //PickList for SON
  @wire(fetchPickListValue, {
    objInfo: {
      sobjectType: "Account"
    },
    picklistFieldApi: "SON__c"
  })
  wireCheckPicklistSON({ error, data }) {
    if (data) {
      this.SON = data;
    } else if (error) {
    }
  }

  /**
		@Author        : K.Sundar
		@ModifiedOn    : March 26, 2020
		@Purpose       : Used to get State and County Address
	**/

  //Function used to fetch all states from Address Object when Screen Loads
  @wire(getAllStates)
  getAllStates({ error, data }) {
    if (data) {
      if (data.length > 0) {
        let stateData = data.map(row => {
          return {
            Id: row.Id,
            Value: row.RefValue__c
          };
        });
        this.stateFetchResult = stateData;
        this.countyInitialLoad();
      } else {
        this.dispatchEvent(utils.toastMessage("No states found", "error"));
      }
    } else if (error) {
      this.dispatchEvent(
        utils.toastMessage("Error in Fetching states", "error")
      );
    }
  }

  //Function used to capture on change states in an array
  stateOnChange(event) {
    let stateSearchTxt = event.target.value;

    if (!stateSearchTxt) this.showStateBox = false;
    else {
      this.filteredStateArr = this.stateFetchResult
        .filter(val => val.Value.indexOf(stateSearchTxt) > -1)
        .map(sresult => {
          return sresult;
        });
      this.showStateBox = this.filteredStateArr.length > 0 ? true : false;
    }
  }

  //Function used to set selected state
  setSelectedState(event) {
    this.selectedState = event.currentTarget.dataset.value;
    this.showStateBox = false;
  }

  ////Function used to set hardcoded states when Modal Opens
  countyInitialLoad() {
    this.countyArr = [
      {
        label: "Adams",
        value: "Adams"
      },
      {
        label: "Allen",
        value: "Allen"
      },
      {
        label: "Bartholomew",
        value: "Bartholomew"
      },
      {
        label: "Benton",
        value: "Benton"
      },
      {
        label: "Blackford",
        value: "Blackford"
      },
      {
        label: "Boone",
        value: "Boone"
      },
      {
        label: "Brown",
        value: "Brown"
      },
      {
        label: "Carroll",
        value: "Carroll"
      },
      {
        label: "Cass",
        value: "Cass"
      },
      {
        label: "Clark",
        value: "Clark"
      },
      {
        label: "Clay",
        value: "Clay"
      },
      {
        label: "Clinton",
        value: "Clinton"
      },
      {
        label: "Crawford",
        value: "Crawford"
      },
      {
        label: "Daviess",
        value: "Daviess"
      },
      {
        label: "Dearborn",
        value: "Dearborn"
      },
      {
        label: "Decatur",
        value: "Decatur"
      },
      {
        label: "DeKalb",
        value: "DeKalb"
      },
      {
        label: "Delaware",
        value: "Delaware"
      },
      {
        label: "Dubois",
        value: "Dubois"
      },
      {
        label: "Elkhart",
        value: "Elkhart"
      },
      {
        label: "Fayette",
        value: "Fayette"
      },
      {
        label: "Floyd",
        value: "Floyd"
      },
      {
        label: "Fountain",
        value: "Fountain"
      },
      {
        label: "Franklin",
        value: "Franklin"
      },
      {
        label: "Fulton",
        value: "Fulton"
      },
      {
        label: "Gibson",
        value: "Gibson"
      },
      {
        label: "Grant",
        value: "Grant"
      },
      {
        label: "Greene",
        value: "Greene"
      },
      {
        label: "Hamilton",
        value: "Hamilton"
      },
      {
        label: "Hancock",
        value: "Hancock"
      },
      {
        label: "Harrison",
        value: "Harrison"
      },
      {
        label: "Hendricks",
        value: "Hendricks"
      },
      {
        label: "Henry",
        value: "Henry"
      },
      {
        label: "Howard",
        value: "Howard"
      },
      {
        label: "Huntington",
        value: "Huntington"
      },
      {
        label: "Jackson",
        value: "Jackson"
      },
      {
        label: "Jasper",
        value: "Jasper"
      },
      {
        label: "Jay",
        value: "Jay"
      },
      {
        label: "Jefferson",
        value: "Jefferson"
      },
      {
        label: "Jennings",
        value: "Jennings"
      },
      {
        label: "Johnson",
        value: "Johnson"
      },
      {
        label: "Knox",
        value: "Knox"
      },
      {
        label: "Kosciusko",
        value: "Kosciusko"
      },
      {
        label: "LaGrange",
        value: "LaGrange"
      },
      {
        label: "Lake",
        value: "Lake"
      },
      {
        label: "LaPorte",
        value: "LaPorte"
      },
      {
        label: "Lawrence",
        value: "Lawrence"
      },
      {
        label: "Madison",
        value: "Madison"
      },
      {
        label: "Marion",
        value: "Marion"
      },
      {
        label: "Marshall",
        value: "Marshall"
      },
      {
        label: "Martin",
        value: "Martin"
      },
      {
        label: "Miami",
        value: "Miami"
      },
      {
        label: "Monroe",
        value: "Monroe"
      },
      {
        label: "Montgomery",
        value: "Montgomery"
      },
      {
        label: "Morgan",
        value: "Morgan"
      },
      {
        label: "Newton",
        value: "Newton"
      },
      {
        label: "Noble",
        value: "Noble"
      },
      {
        label: "Ohio",
        value: "Ohio"
      },
      {
        label: "Orange",
        value: "Orange"
      },
      {
        label: "Owen",
        value: "Owen"
      },
      {
        label: "Parke",
        value: "Parke"
      },
      {
        label: "Perry",
        value: "Perry"
      },
      {
        label: "Pike",
        value: "Pike"
      },
      {
        label: "Porter",
        value: "Porter"
      },
      {
        label: "Posey",
        value: "Posey"
      },
      {
        label: "Pulaski",
        value: "Pulaski"
      },
      {
        label: "Putnam",
        value: "Putnam"
      },
      {
        label: "Randolph",
        value: "Randolph"
      },
      {
        label: "Ripley",
        value: "Ripley"
      },
      {
        label: "Rush",
        value: "Rush"
      },
      {
        label: "St. Joseph",
        value: "St. Joseph"
      },
      {
        label: "Scott",
        value: "Scott"
      },
      {
        label: "Shelby",
        value: "Shelby"
      },
      {
        label: "Spencer",
        value: "Spencer"
      },
      {
        label: "Starke",
        value: "Starke"
      },
      {
        label: "Steuben",
        value: "Steuben"
      },
      {
        label: "Sullivan",
        value: "Sullivan"
      },
      {
        label: "Switzerland",
        value: "Switzerland"
      },
      {
        label: "Tippecanoe County",
        value: "Tippecanoe County"
      },
      {
        label: "Tipton",
        value: "Tipton"
      },
      {
        label: "Union",
        value: "Union"
      },
      {
        label: "Vanderburgh",
        value: "Vanderburgh"
      },
      {
        label: "Vermillion",
        value: "Vermillion"
      },
      {
        label: "Vigo",
        value: "Vigo"
      },
      {
        label: "Wabash",
        value: "Wabash"
      },
      {
        label: "Warren",
        value: "Warren"
      },
      {
        label: "Warrick",
        value: "Warrick"
      },
      {
        label: "Washington",
        value: "Washington"
      },
      {
        label: "Wayne",
        value: "Wayne"
      },
      {
        label: "Wells",
        value: "Wells"
      },
      {
        label: "White",
        value: "White"
      },
      {
        label: "Whitley",
        value: "Whitley"
      }
    ];
    return this.countyArr;
  }

  //Function used to capture on change county in an array
  countyOnChange(event) {
    let countySearchTxt = event.target.value;

    if (!countySearchTxt) this.showCountyBox = false;
    else {
      this.filteredCountyArr = this.countyArr
        .filter(val => val.value.indexOf(countySearchTxt) > -1)
        .map(cresult => {
          return cresult;
        });
      this.showCountyBox = this.filteredCountyArr.length > 0 ? true : false;
    }
  }

  //Function used to set selected city
  setSelectedCounty(event) {
    this.selectedCounty = event.currentTarget.dataset.value;
    this.showCountyBox = false;
  }

  //getStreetAddress from Google API when user manually input
  getStreetAddress(event) {
    this.strStreetAddress = event.target.value;

    if (!event.target.value) {
      this.selectedCounty = "";
      this.selectedState = "";
      this.selectedCity = "";
      this.selectedZipCode = "";
    } else {
      getAPIStreetAddress({
        input: this.strStreetAddress
      })
        .then(result => {
          this.strStreetAddresPrediction = JSON.parse(result);
          this.active =
            this.strStreetAddresPrediction.predictions.length > 0
              ? true
              : false;
        })
        .catch(error => {
          this.dispatchEvent(
            utils.toastMessage(
              "Error in Fetching Google API Street Address",
              "error"
            )
          );
        });
    }
  }

  selectStreetAddress(event) {
    this.strStreetAddress = event.target.dataset.description;

    getFullDetails({
      placeId: event.target.dataset.id
    })
      .then(result => {
        var main = JSON.parse(result);
        try {
          var cityTextbox = this.template.querySelector(".dummyCity");
          this.selectedCity = main.result.address_components[2].long_name;

          var countyComboBox = this.template.querySelector(".dummyCounty");
          this.selectedCounty = main.result.address_components[4].long_name;

          var stateComboBox = this.template.querySelector(".dummyState");
          this.selectedState = main.result.address_components[5].long_name;

          var ZipCodeTextbox = this.template.querySelector(".dummyZipCode");
          this.selectedZipCode = main.result.address_components[7].long_name;
        } catch (ex) { }
        this.active = false;
      })
      .error(error => {
        this.dispatchEvent(
          utils.toastMessage("Error in Selected Address", "error")
        );
      });
  }

  //Address onchange event handler
  addressOnChange(event) {
    if (event.target.name == "ShippingStreet") {
      this.ShippingStreet = event.target.value;
    } else if (event.target.name == "City") {
      this.selectedCity = event.target.value;
    } else if (event.target.name == "ZipCode") {
      this.selectedZipCode = event.target.value.replace(/(\D+)/g, "");
    }
  }

  @wire(getAccreditationValues)
  getAccreditationValues({ error, data }) {
    if (data) {
      if (data.length > 0) {
        this.accreditationOptions = data.map(row => {
          return {
            label: row.RefValue__c,
            value: row.RefValue__c
          };
        });
      } else {
        this.dispatchEvent(
          utils.toastMessage("No Accreditation found", "error")
        );
      }
    } else if (error) {
      this.dispatchEvent(
        utils.toastMessage("Error in Fetching Accreditation", "error")
      );
    }
  }

  accreditationOnChange(evt) {
    this.Accreditation = event.target.value;
  }

  @wire(getProgramOptions)
  wireGetProgramOptions({ error, data }) {
    if (data) {
      if (data.length > 0) {
        this.programOptions = data.map(row => {
          return {
            label: row.RefValue__c,
            value: row.RefKey__c
          };
        });

        this.isDisableProgramType = !this.Program ? true : false;
        this.initialLoadProgramTypeValues();
      } else {
        this.dispatchEvent(
          utils.toastMessage("Program Values are not found", "error")
        );
      }
    } else {
      this.dispatchEvent(
        utils.toastMessage("Error in Fetching Program Values", "error")
      );
    }
  }

  initialLoadProgramTypeValues() {
    getProgramTypeOptions()
      .then(result => {
        let resultArray = JSON.stringify(result);

        if (result.length > 0) {
          this.programTypeResultArray = result;

          if (this.Program) {
            this.programTypeOptions = this.programTypeResultArray
              .filter(row => row.RefKey__c == this.Program)
              .map(row1 => {
                return {
                  label: row1.RefValue__c,
                  value: row1.RefValue__c
                };
              });
          }
          this.programTypeCheckBoxSelectUnselectDuringPageLoad();
        } else {
          this.dispatchEvent(
            utils.toastMessage("No Program Types found", "error")
          );
        }
      })
      .catch(error => {
        this.dispatchEvent(
          utils.toastMessage("Error in fetching Program Types", "error")
        );
      });
  }

  programOnChange(evt) {
    let selectedValue = evt.target.value;

    if (this.Program != selectedValue) {
      this.pillValueForPtype = [];
    }

    this.Program = evt.target.value;
    this.isDisableProgramType = false;

    this.programTypeOptions = this.programTypeResultArray
      .filter(row => row.RefKey__c == this.Program)
      .map(row1 => {
        return {
          label: row1.RefValue__c,
          value: row1.RefValue__c
        };
      });
  }

  get optionsProviderType() {
    return [
      {
        label: "Private",
        value: "Private"
      },
      {
        label: "Public",
        value: "Public"
      },
      {
        label: "Vendor",
        value: "Vendor"
      }
    ];
  }

  handleChangeRadioButton(event) {
    const selectedOption = event.detail.value;
  }

  //Validation for Phone Number
  handlePhoneChange(evt) {
    evt.target.value = evt.target.value.replace(/(\D+)/g, "");
    this.Phone = utils.formattedPhoneNumber(evt.target.value);
  }

  //Validation for Mobile Number
  handleMobileChange(evt) {
    evt.target.value = evt.target.value.replace(/(\D+)/g, "");
    this.MobileNumber = utils.formattedPhoneNumber(evt.target.value);
  }

  //Validation for Fax
  handleFaxChange(evt) {
    evt.target.value = evt.target.value.replace(/(\D+)/g, "");
    this.Fax = utils.formattedPhoneNumber(evt.target.value);
  }

  //Validation for TaxId
  handleTaxChange(evt) {
    evt.target.value = evt.target.value.replace(/(\D+)/g, "");
    this.TaxId = utils.formattedTaxID(evt.target.value);
  }

  accreditationOnChange(evt) {
    this.Accreditation = event.target.value;
  }


  //Saving Provider object values
  handleClickProvider(event) {
    var inp = [
      ...this.template.querySelectorAll("lightning-input"),
      ...this.template.querySelectorAll("lightning-combobox"),
      ...this.template.querySelectorAll("lightning-radio-group")
    ];

    let objProvider = {
      sobjectType: "Account"
    };
    objProvider.ProviderType__c = "Private";
    objProvider.ProgramType__c = this.getProgramTypeValues();
    if (sharedData.getProviderId()) {
      objProvider.Id = sharedData.getProviderId();
    }
    inp.forEach(element => {
      switch (element.name) {
        case "Program":
          objProvider.Program__c = element.value;
          break;
        case "ParentCorporation":
          objProvider.ParentCorporation__c = element.value;
          break;
        case "Corporation":
          objProvider.Corporation__c = element.value;
          break;
        case "TaxId":
          objProvider.TaxId__c = element.value;
          break;
        case "proposal":
          objProvider.RFP__c =
            typeof element.value == "object" ? "" : element.value;
          break;
        case "FEIN":
          objProvider.FEINTaxID__c =
            typeof element.value == "object" ? "" : element.value;
          break;
        case "Name":
          objProvider.Name = element.value;
          break;
        case "FirstName":
          objProvider.FirstName__c = element.value;
          break;
        case "SON":
          objProvider.SON__c =
            typeof element.value == "object" ? "" : element.value;
          break;
        case "LastName":
          objProvider.LastName__c = element.value;
          break;
        case "strStreetAddress":
          objProvider.BillingStreet = element.value;
          break;
        case "Email":
          objProvider.Email__c = element.value;
          break;
        case "ShippingStreet":
          objProvider.ShippingStreet = element.value;
          break;
        case "PhoneNumber":
          objProvider.Phone = element.value;
          break;
        case "State":
          objProvider.BillingState = element.value;
          break;
        case "MobileNumber":
          objProvider.CellNumber__c = element.value;
          break;
        case "City":
          objProvider.BillingCity = element.value;
          break;
        case "Fax":
          objProvider.Fax = element.value;
          break;
        case "County":
          objProvider.BillingCountry = element.value;
          break;
        case "Accreditation":
          objProvider.Accrediation__c = element.value;
          break;
        case "ZipCode":
          objProvider.BillingPostalCode = element.value;
          break;
        case "valueProfit":
          objProvider.Profit__c = element.value;
          break;
      }
    }); // End for each

    if (!objProvider.Name) {
      this.dispatchEvent(
        utils.toastMessage("Provider Name is mandatory", "warning")
      );
    } else if (!objProvider.Email__c) {
      this.dispatchEvent(utils.toastMessage("Email is mandatory", "warning"));
    } else if (!objProvider.BillingStreet) {
      this.dispatchEvent(
        utils.toastMessage("Address Line1 is mandatory", "warning")
      );
    } else if (!objProvider.BillingState) {
      this.dispatchEvent(utils.toastMessage("State is mandatory", "warning"));
    }
    else if (!objProvider.FirstName__c) {
      this.dispatchEvent(utils.toastMessage("FirstName is mandatory", "warning"));
    }
      else if (!objProvider.LastName__c) {
        this.dispatchEvent(utils.toastMessage("LastName is mandatory", "warning"));
    } else if (!objProvider.BillingCity) {
      this.dispatchEvent(utils.toastMessage("City is mandatory", "warning"));
    } else if (!objProvider.BillingPostalCode) {
      this.dispatchEvent(utils.toastMessage("ZipCode is mandatory", "warning"));
    } else if (!objProvider.BillingCountry) {
      this.dispatchEvent(utils.toastMessage("County is mandatory", "warning"));
    } else if (
      objProvider.BillingPostalCode &&
      /^\d{1,5}$/.test(objProvider.BillingPostalCode) === false
    ) {
      this.dispatchEvent(utils.toastMessage("Invalid ZipCode", "warning"));
    } else if (objProvider.Phone && objProvider.Phone.length != 14) {
      this.dispatchEvent(utils.toastMessage("Invalid Phone", "warning"));
    } else if (!objProvider.TaxId__c) {
      this.dispatchEvent(utils.toastMessage("Tax Id is mandatory", "warning"));
    } else {
      updateproviderandReferralReturnId({
        objSobjecttoUpdateOrInsert: objProvider
      })
        .then(result => {
          this.provideridforcaseinsert = result;
          sharedData.setProviderId(result);
          this.handleClickRefferal();
          this.dispatchEvent(
            new CustomEvent("sendproviderid", {
              detail: {
                providerid: result
              }
            })
          );
          this.dispatchEvent(
            utils.toastMessage("Referral Saved Successfully", "success")
          );
        })
        .catch(error => {
          this.dispatchEvent(
            utils.toastMessage("Provider already exists", "error")
          );
        });
    }
  }
  getProgramTypeValues() {
    var sArray = [];
    this.pillValueForPtype.forEach(function (item) {
      sArray.push(item.value);
    });
    return sArray.join(",");
  }

  //Saving Referral object values
  handleClickRefferal() {
    var inp = [
      ...this.template.querySelectorAll("lightning-input"),
      ...this.template.querySelectorAll("lightning-combobox"),
      ...this.template.querySelectorAll("lightning-radio-group")
    ];

    let objReferral = {
      sobjectType: "Case"
    };
    objReferral.Status = "Draft";
    objReferral.Priority = "Medium";
    objReferral.AccountId = this.provideridforcaseinsert;
    objReferral.Id = this.caseid;
    objReferral.Origin = this.communication;
    objReferral.ReceivedDate__c = this.receivedDate;
    objReferral.ProgramType__c = this.getProgramTypeValues();

    inp.forEach(element => {
      if (element.name == "Program") {
        objReferral.Program__c = element.value;
      }
    });

    if (!objReferral.Program__c) {
      this.dispatchEvent(utils.toastMessage("Program is mandatory", "warning"));
    } else if (objReferral.ProgramType__c.length == 0) {
      this.dispatchEvent(
        utils.toastMessage("Program Type is mandatory", "warning")
      );
    }

    updateproviderandReferral({
      objSobjecttoUpdateOrInsert: objReferral
    })
      .then(result => { })
      .catch(error => { });
  }

  //Final Saving the value for both Provider and Referral Objects
  saveForm() {
    if (!this.communication) {
      this.dispatchEvent(
        utils.toastMessage("Please fill the Communication", "warning")
      );
    } else if (!this.receivedDate) {
      this.dispatchEvent(
        utils.toastMessage("Please fill the Received Date", "warning")
      );
    } else {
      const allValid = [
        ...this.template.querySelectorAll("lightning-input"),
        ...this.template.querySelectorAll("lightning-combobox")
      ].reduce((validSoFar, inputCmp) => {
        inputCmp.reportValidity();
        return validSoFar && inputCmp.checkValidity();
      }, true);
      let button = this.template.querySelector("lightning-button");

      this.handleClickProvider();
    }
  }

  handleChangePickList(event) {
    this.selectedVals.push(event.detail.value);
  }
}
