/***
 * @Author: Pratheeba V
 * @CreatedOn: April 6, 2020
 * @Purpose: Application Monitoring Add/List/Edit
 * @updatedBy: Pratheeba V
 * @updatedOn: April 13,2020 
 **/
import {
  LightningElement,
  track,
  wire
} from 'lwc';
import getMonitoringDetails from '@salesforce/apex/ProvidersMonitoring.getMonitoringDetails';
import InsertUpdateMonitoring from '@salesforce/apex/ProvidersMonitoring.InsertUpdateMonitoring';
import getAddressDetails from '@salesforce/apex/ProvidersMonitoring.getAddressDetails';
import getProgramDetails from '@salesforce/apex/ProvidersMonitoring.getProgramDetails';
import {
  getObjectInfo
} from 'lightning/uiObjectInfoApi';
import Monitoring__c_OBJECT from '@salesforce/schema/Monitoring__c';
import {
  getPicklistValuesByRecordType
} from 'lightning/uiObjectInfoApi';
import editMonitoringDetails from '@salesforce/apex/ProvidersMonitoring.editMonitoringDetails';
import viewMonitoringDetails from '@salesforce/apex/ProvidersMonitoring.viewMonitoringDetails';
import getApplicationProviderStatus from "@salesforce/apex/ProvidersMonitoring.getApplicationProviderStatus";
import getMonitoringStatus from "@salesforce/apex/ProvidersMonitoring.getMonitoringStatus";
import * as sharedData from 'c/sharedData';
import {
  utils
} from 'c/utils';
import images from '@salesforce/resourceUrl/images';

const actions = [
  // { label: 'Edit', name: 'Edit', iconName: 'utility:edit', target: '_self' },
  {
    label: 'View',
    name: 'View',
    iconName: 'utility:preview',
    target: '_self'
  },
];
const columns = [{
    label: 'VISIT ID',
    fieldName: 'linkName',
    type: 'button',
    typeAttributes: {
      label: {
        fieldName: 'Name'
      }
    }
  },
  {
    label: 'PROGRAM TYPE',
    fieldName: 'ProgramType__c',
    sortable: true
  },
  {
    label: 'VISIT DATE',
    fieldName: 'VisitationStartDate__c',
    sortable: true
  },
  {
    label: 'SITE',
    fieldName: 'SiteAddress',
    sortable: true
  },
  {
    label: 'PERIODIC',
    fieldName: 'SelectPeriod__c',
    sortable: true
  },
  {
    label: 'APPLICATION',
    fieldName: 'ApplicationName',
    sortable: true
  },
  {
    label: 'LICENSE NUMBER',
    fieldName: 'ApplicationName',
    sortable: true
  },
  {
    label: 'EXPIRY',
    fieldName: 'Status__c',
    cellAttributes: {
      class: {
        fieldName: 'CSSClass'
      }
    }
  },

  {
    label: 'Action',
    fieldName: 'id',
    type: "button",
    typeAttributes: {
      iconName: 'utility:preview',
      name: 'View',
      title: 'Click to Preview',
      variant: 'border-filled',
      class: 'view-red',
      disabled: false,
      iconPosition: 'left',
      target: '_self'
    }
  },
  // {
  //   label: 'ACTION',
  //   type: 'action',
  //   typeAttributes: { rowActions: actions },
  //   cellAttributes: { iconName: 'utility:threedots_vertical', iconAlternativeText: 'Actions', class: "tripledots" },
  // },
];

export default class ProviderMonitoring extends LightningElement {
  @track siteaddress;
  @track selectperiod;
  @track columns = columns;
  @track visitationstartdate;
  @track announced;
  @track timevisit;
  @track openmodel = false;
  @track viewmodel = false;
  @track currentPageMonitoringData;
  @track norecorddisplay = true;
  @track totalRecords;
  @track totalRecordsCount = 0;
  @track page = 1;
  perpage = 10;
  setPagination = 5;
  @track addEditMonitoring = {
    SelectPeriod__c: '',
    VisitationStartDate__c: '',
    VisitationEndDate__c: '',
    AnnouncedUnannouced__c: '',
    TimetoVisit__c: '',
    JointlyInspectedWith__c: ''
  };
  @track Program__c;
  @track SiteAddress;
  @track ProgramType__c;
  addressId;
  applicationId;
  @track inspectedwithValues;
  @track monitoringID;
  @track ApplicationLicenseId__c;
  @track defaultSortDirection = 'asc';
  @track sortDirection = 'asc';
  @track sortedBy = 'VisitationStartDate__c';
  @track renewalmindate = '1970-01-01';
  @track renewalmaxdate = '1970-01-02';
  @track freezeMonitoringScreen = false;

  @track getapplicationid = 'a090p000001GQPsAAO';

  applicationforApiCall = this.getapplicationid;
  attachmentIcon = images + '/application-monitoring.svg';

  //get Application Status
  @wire(getApplicationProviderStatus, {
    applicationId: '$applicationforApiCall'
  })
  applicationStatus({
    error,
    data
  }) {
    if (data) {
      if (data.length > 0 && (data[0].Status__c == "Caseworker Submitted" || data[0].Status__c == "Approved")) {
        this.freezeMonitoringScreen = true;
      } else {
        this.freezeMonitoringScreen = false;
      }
    }
  }

  //Declare Monitoring object into one variable
  @wire(getObjectInfo, {
    objectApiName: Monitoring__c_OBJECT
  })
  objectInfo;

  //Function used to get all picklist values from Monitoring object
  @wire(getPicklistValuesByRecordType, {
    objectApiName: Monitoring__c_OBJECT,
    recordTypeId: '$objectInfo.data.defaultRecordTypeId'
  })
  getAllPicklistValues({
    error,
    data
  }) {
    if (data) {
      // inspectedwith
      let inspectedwithOptions = [];
      data.picklistFieldValues.JointlyInspectedWith__c.values.forEach(key => {
        inspectedwithOptions.push({
          label: key.label,
          value: key.value
        })
      });
      this.inspectedwithValues = inspectedwithOptions;

      //timevisitValues
      let timevisitOptions = [];
      data.picklistFieldValues.TimetoVisit__c.values.forEach(key => {
        timevisitOptions.push({
          label: key.label,
          value: key.value
        })
      });
      this.timevisitValues = timevisitOptions;

      //selectPeriodValues
      let selectPeriodOptions = [];
      data.picklistFieldValues.SelectPeriod__c.values.forEach(key => {
        selectPeriodOptions.push({
          label: key.label,
          value: key.value
        })
      });
      this.selectPeriodValues = selectPeriodOptions;

      //selectAnnouncedValues
      let selectAnnouncedOptions = [];
      data.picklistFieldValues.AnnouncedUnannouced__c.values.forEach(key => {
        selectAnnouncedOptions.push({
          label: key.label,
          value: key.value
        })
      });
      this.selectAnnouncedValues = selectAnnouncedOptions;
    } else if (error) {
      this.error = JSON.stringify(error);
    }
  }

  // Used to sort the 'Age' column
  sortBy(field, reverse, primer) {
    const key = primer ?
      function (x) {
        return primer(x[field]);
      } :
      function (x) {
        return x[field];
      };

    return function (a, b) {
      a = key(a);
      b = key(b);
      return reverse * ((a > b) - (b > a));
    };
  }

  onHandleSort(event) {
    const {
      fieldName: sortedBy,
      sortDirection
    } = event.detail;
    const cloneData = [...this.currentPageMonitoringData];
    cloneData.sort(this.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1));
    this.currentPageMonitoringData = cloneData;
    this.sortDirection = sortDirection;
    this.sortedBy = sortedBy;
  }

  connectedCallback() {
    this.monitoringDetailsLoad();
    getAddressDetails({
      applicationid: this.getapplicationid
    }).then(result => {
      if (result.length == 0) {
        this.addressId = null;
        this.SiteAddress = null;
      } else {
        this.addressId = result[0].Id;
        this.SiteAddress = result[0].AddressLine1__c + ',' + result[0].City__c + ',' + result[0].County__c;
      }
    }).catch(error => {

    });
    getProgramDetails({
      applicationid: this.getapplicationid
    }).then(result => {
      if (result.length == 0) {
        this.Program__c = null;
        this.ProgramType__c = null;
        this.ApplicationLicenseId__c = null;
      } else {
        this.Program__c = result[0].Program__c;
        this.ProgramType__c = result[0].ProgramType__c;
        this.ApplicationLicenseId__c = result[0].Name;
      }
    }).catch(error => {

    });
  }

  get siteAddressValues() {
    return [{
      label: this.SiteAddress,
      value: this.SiteAddress
    }, ];
  }

  monitoringDetailsLoad() {
    getMonitoringDetails({
      applicationid: this.getapplicationid
    }).then(result => {
      this.totalRecordsCount = result.length;
      if (this.totalRecordsCount == 0) {
        this.norecorddisplay = true;
      } else {
        this.norecorddisplay = false;
      }
      this.totalRecords = result.map(row => {
        return {
          Id: row.Id,
          Name: row.Name,
          ProgramType__c: row.ProgramType__c,
          SiteAddress: row.SiteAddress__r ? row.SiteAddress__r.AddressLine1__c + ',' + row.SiteAddress__r.City__c : null,
          ApplicationName: row.ApplicationLicenseId__r.Name,
          SelectPeriod__c: row.SelectPeriod__c,
          VisitationStartDate__c: utils.formatDate(row.VisitationStartDate__c),
          linkName: '/' + row.Id,
          Status__c: row.Status__c,
          SiteAddressId: row.SiteAddress__c,
          CSSClass: (row.Status__c == 'Completed') ? 'slds-text-color_success' : ''
        }
      });
      this.pageData();
    }).catch(error => {

    });
  }

  //Function used to show the monitoring based on the pages
  pageData = () => {
    let page = this.page;
    let perpage = this.perpage;
    let startIndex = (page * perpage) - perpage;
    let endIndex = (page * perpage);
    this.currentPageMonitoringData = this.totalRecords.slice(startIndex, endIndex);
    if (this.currentPageMonitoringData.length == 0) {
      if (this.page != 1) {
        this.page = this.page - 1;
        this.pageData();
      }
    }
  }

  //Function used to get the page number from child pagination component
  hanldeProgressValueChange(event) {
    this.page = event.detail;
    this.pageData();
  }

  //Function used to edit the existing monitoring 
  handleRowAction(event) {
    let monitoringrowid = event.detail.row.Id;
    if (event.detail.action.name == undefined) {
      sharedData.setMonitoringId(event.detail.row.Id);
      sharedData.setMonitoringAddressId(event.detail.row.SiteAddressId);
      sharedData.setMonitoringStatus(event.detail.row.Status__c);
      const oncaseid = new CustomEvent('redirecteditmonitering', {
        detail: {
          first: false
        }
      });
      this.dispatchEvent(oncaseid);
    } else if (event.detail.action.name == "View") {
      this.openmodel = false;
      this.viewmodel = true;
      viewMonitoringDetails({
        viewMonitoringDetails: monitoringrowid
      }).then(result => {
        this.Program__c = result[0].Program__c;
        this.ProgramType__c = result[0].ProgramType__c;
        this.ApplicationLicenseId__c = this.ApplicationLicenseId__c;
        this.SiteAddress__c = result[0].SiteAddress__c;
        this.addEditMonitoring.SelectPeriod__c = result[0].SelectPeriod__c;
        this.addEditMonitoring.VisitationStartDate__c = result[0].VisitationStartDate__c;
        this.addEditMonitoring.VisitationEndDate__c = result[0].VisitationEndDate__c;
        this.addEditMonitoring.AnnouncedUnannouced__c = result[0].AnnouncedUnannouced__c;
        this.addEditMonitoring.TimetoVisit__c = result[0].TimetoVisit__c;
        this.addEditMonitoring.JointlyInspectedWith__c = result[0].JointlyInspectedWith__c;
        this.monitoringID = result[0].Id;
      }).catch(error => {
        this.dispatchEvent(utils.toastMessage("Error in fetching record", "error"));
      });
    }
  }

  openmodal() {
    this.addEditMonitoring = {};
    this.monitoringID = undefined;
    this.viewmodel = false;
    if (this.freezeMonitoringScreen)
      return this.dispatchEvent(utils.toastMessage("Cannot add monitoring for submitted application", "warning"));
    else
      this.openmodel = true;
  }
  closeModal() {
    this.openmodel = false;
    this.viewmodel = false;
  }

  handleChange(event) {
    if (event.target.name == 'Program__c') {
      this.Program__c = event.target.value;
    } else if (event.target.name == 'ProgramType__c') {
      this.ProgramType__c = event.target.value;
    } else if (event.target.name == 'SiteAddress__c') {
      this.SiteAddress__c = event.target.value;
    } else if (event.target.name == 'SelectPeriod__c') {
      this.addEditMonitoring.SelectPeriod__c = event.target.value;
    } else if (event.target.name == 'VisitationStartDate__c') {
      this.addEditMonitoring.VisitationStartDate__c = event.target.value;
      this.renewalmindate = event.target.value;
      this.renewalmaxdate = event.target.value + 20;
    } else if (event.target.name == 'VisitationEndDate__c') {
      this.addEditMonitoring.VisitationEndDate__c = event.target.value;
    } else if (event.target.name == 'AnnouncedUnannouced__c') {
      this.addEditMonitoring.AnnouncedUnannouced__c = event.target.value;
    } else if (event.target.name == 'TimetoVisit__c') {
      this.addEditMonitoring.TimetoVisit__c = event.target.value;
    } else if (event.target.name == 'JointlyInspectedWith__c') {
      this.addEditMonitoring.JointlyInspectedWith__c = event.target.value;
    }
  }

  handleSave() {
    if (!this.addEditMonitoring.SelectPeriod__c) {
      this.dispatchEvent(utils.toastMessage("Please enter select period", "warning"));
    } else if (!this.addEditMonitoring.VisitationStartDate__c) {
      this.dispatchEvent(utils.toastMessage("Please enter visitation startdate", "warning"));
    } else {
      this.addEditMonitoring.sobjectType = 'Monitoring__c';
      this.addEditMonitoring.SiteAddress__c = this.addressId;
      this.addEditMonitoring.ApplicationLicenseId__c = this.getapplicationid;
      if (this.monitoringID)
        this.addEditMonitoring.Id = this.monitoringID;
      else
        this.addEditMonitoring.Status__c = 'Draft';
      InsertUpdateMonitoring({
          objSobjecttoUpdateOrInsert: this.addEditMonitoring
        })
        .then(result => {
          this.dispatchEvent(utils.toastMessage("Record Saved Successfully", "Success"));
          this.addEditMonitoring = {};
          this.closeModal();
          this.currentPageMonitoringData = [];
          this.monitoringDetailsLoad();
        }).catch(error => {
          this.dispatchEvent(utils.toastMessage("Record not Saved", "error"));
        });
    }
  }
}