/**
 * @Author        : B Balamurugan.
 * @CreatedOn     : Aug 03,2020
 * @Purpose       : Incident Witness Fetching -Staff and Person/Kid.
 **/

import { LightningElement, wire, api } from "lwc";
import { utils } from "c/utils";
import { CJAMS_CONSTANTS } from "c/constants";
import getPkcDetails from "@salesforce/apex/IncidentWitness.getPkcDetails";
import getSelectedPersonKidChildDropdownValueData from "@salesforce/apex/IncidentWitness.getSelectedPersonKidChildDropdownValueData";
import getStaffDetails from "@salesforce/apex/IncidentWitness.getStaffDetails";
import getSelectedStaffDropdownValueData from "@salesforce/apex/IncidentWitness.getSelectedStaffDropdownValueData";
import getRecordType from "@salesforce/apex/IncidentWitness.getRecordType";
import InsertUpdateContactActor from "@salesforce/apex/IncidentWitness.InsertUpdateContactActor";
import getWitnessDetails from "@salesforce/apex/IncidentWitness.getWitnessDetails";
import getActionDetails from "@salesforce/apex/IncidentWitness.getActionDetails";
import { constPopupVariables, USER_PROFILE_NAME } from 'c/constants';
import { deleteRecord } from "lightning/uiRecordApi";
import * as sharedData from "c/sharedData";
import images from "@salesforce/resourceUrl/images";

export default class IncidentWitness extends LightningElement {
    _newWitnessModal;
    columns;
    totalRecords;
    norecorddisplay = true;
    totalRecordsCount = 0;
    currentWitnessData;
    btnLabel = "ADD";
    showPerson = false;
    showStaff = false;
    showOthers = false;

    norecord = images + "/deficiencyIcon.svg";
    descriptionIcon = images + '/description-icon.svg';

    typeDisable = false;
    statementDisable = false;
    isDisableName = false;
    isDisabled = false;
    personDisabled = false;
    openmodel = false;
    deleteWitnessId;
    Spinner = true;
    editMode = true;


    //obj
    personValues;
    PersonValue;
    Person;
    pkcVal = {};
    selectedPersonKidChildDropdownValueId;
    DOB__c;
    IdentifierNumber__c;
    Assign__c;
    staffValues;
    Staff;
    selectedStaffDropdownValueId;
    JobTitle__c;
    EmployeeType__c;
    addOthersContact = {};
    addContact = {};
    recordType = 'Witness';
    RecordTypeId;
    FirstName;
    LastName;
    WitnessStatement__c;
    Others;
    title;
    WitnessTitle;
    WitnessOtherId;


    editWitnessDetails;
    stmtdisable = true;
    showCommentsPop = false;
    commentsOfPad;

    witnessTypeValue = 'Person/Kid';

    //Pagination Details
    norecorddisplay = true;
    totalRecordsCount = 0;
    totalRecords;
    page = 1;
    perpage = 5;
    setPagination = 5;

    closeCommentsPop() {
        this.showCommentsPop = false
    }
    constructor() {
        super();
        this.columns = [{
            label: "FIRST NAME",
            fieldName: "FirstName",
            type: "text"
        },
        {
            label: "LAST NAME",
            fieldName: "LastName",
            type: "text"
        },
        {
            label: "WITNESS TYPE",
            fieldName: "Role__c",
            type: "text"
        },
        {
            label: "WITNESS STATEMENT",
            fieldName: "WitnessStatement__c",
            type: 'button',
            typeAttributes: {
                label: {
                    fieldName: 'WitnessStatement__c'
                },
                name: 'witnessstatement',
                title: {
                    fieldName: 'WitnessStatement__c'
                }
            },
            cellAttributes: {
                iconName: 'utility:description',
                class: {
                    fieldName: 'commentsClass'
                }
            }
        },

        {
            type: 'action',
            typeAttributes: { rowActions: this.getRowActions },
            cellAttributes: {
                iconName: 'utility:threedots_vertical',
                iconAlternativeText: 'Actions',
                class: { fieldName: "statusClass" }
            }
        }
        ];
    }

    getRowActions(row, doneCallback) {
        const actions = [];
        let userProfileName = sharedData.getUserProfileName();
        let incidentStatus = sharedData.getIncidentStatus();

        if (userProfileName === USER_PROFILE_NAME.USER_CASE_WRKR || (incidentStatus !== undefined && (incidentStatus !== "Pending" || incidentStatus !== "Draft"))) {
            actions.push({
                'label': 'View',
                'iconName': 'action:preview',
                'name': 'View'
            });
        } else
            actions.push({
                'label': 'Edit',
                'iconName': 'action:edit',
                'name': 'Edit'
            }, {
                'label': 'Delete',
                'iconName': 'action:delete',
                'name': 'Delete'
            });

        // simulate a trip to the server
        setTimeout(() => {
            doneCallback(actions);
        }, 200);

    }


    get caseId() {
        return sharedData.getCaseId();
    }


    get userProfileName() {
        return sharedData.getUserProfileName();
    }

    get incidentStatus() {
        return sharedData.getIncidentStatus();
    }

    //openWitnessModal from Header Button Child comp Through AllTabs Comp
    @api get openWitnessModal() {
        return this._newWitnessModal;
    }

    set openWitnessModal(value) {
        this._newWitnessModal = value;
        this.title = 'Add Witness Details';
        this.WitnessTitle = 'Witness Type';
        this.btnLabel = 'ADD';
        this.Person =  'Person/Kid';
        this.PersonValue = '';
        this.WitnessStatement__c = '';
        this.witnessTypeValue = 'Person/Kid';
        this.Staff = '';
        this.showPerson = true;
        this.showStaff = false;
        this.showOthers = false;
    }

    //closemdal dispatch function
    closeModal() {
        this.stmtdisable = true;
        this.isDisabled = false;
        this.personDisabled = false;
        this._newWitnessModal = false;
        this.openmodel = false;
        this.DOB__c = '';
        this.IdentifierNumber__c = '';
        this.JobTitle__c = '';
        this.EmployeeType__c = '';
        this.FirstName = '';
        this.LastName = '';
        this.PersonValue = '';
        this.WitnessStatement__c = '';
        this.witnessTypeValue = 'Person/Kid';
        this.Staff = '';
        const onClickId = new CustomEvent("popclosemodal", {
            detail: {
                first: false
            }
        });
        this.dispatchEvent(onClickId);
        this.dispatchCustomEventToHandleAddWitnessInfoPopUp();
    }
    dispatchCustomEventToHandleAddWitnessInfoPopUp() {
        const tooglecmps = new CustomEvent("witnesstogglecomponents", {
            detail: {
                Component: "CloseWitness"
            }
        });
        this.dispatchEvent(tooglecmps);
    }

    get options() {
        return [{
            label: "Person/Kid/Child",
            value: "Person/Kid"
        },
        {
            label: "Staff",
            value: "Staff"
        },
        {
            label: "Others",
            value: "Witness"
        }
        ];
    }

    //Witness Type Click 
    handleChange(event) {
        let type = event;
        if (event.target !== undefined)
            type = event.target.value;
        if (type == "Person/Kid/Child" || type == "Person/Kid") {
            this.Person = type;
            this.showPerson = true;
            this.showStaff = false;
            this.showOthers = false;
        } else if (type == "Staff") {
            this.Person = type;
            this.showPerson = false;
            this.showStaff = true;
            this.showOthers = false;
        } else if (type == "Others" || type == "Witness") {
            this.Person = type;
            this.showPerson = false;
            this.showStaff = false;
            this.showOthers = true;
        }
    }

    //onchange Person/Kid Data
    handleChangePkc(event) {
        let Persons = event.target.value;
        let PersonKid = event.target.name;
        if (PersonKid === "Person/kid/child") {
            event.target.options.map((item) => {

                if (Persons === item.value) {
                    this.selectedPersonKidChildDropdownValueId = item.id;
                }
            });
            this.dataPkc(this.selectedPersonKidChildDropdownValueId);
        }
    }

    //Staff Data
    handleChangeStaff(event) {
        let Staffs = event.target.value;
        let Staff = event.target.name;
        if (Staff === "Staff") {
            event.target.options.map((item) => {
                if (Staffs === item.value) {
                    this.selectedStaffDropdownValueId = item.id;
                }
            });
            this.dataStaff(this.selectedStaffDropdownValueId);
        }
    }

    //Other Data
    handleChangeOthers(event) {

        if (event.target.name == 'FirstName') {
            this.FirstName = event.target.value;
        } else if (event.target.name == 'LastName') {
            this.LastName = event.target.value;
        }
    }

    //comments Data
    commentsHandler(event) {
        this.WitnessStatement__c = event.detail.value;
        if (this.WitnessStatement__c.length > 0) {
            this.stmtdisable = false;
        } else {
            this.stmtdisable = true;
        }
    }

    dataPkc = async (id) => {
        try {

            let data = await getSelectedPersonKidChildDropdownValueData({

                id: id
            });
            data.map(item1 => {
                this.isDisabled = true;
                this.DOB__c = utils.formatDate(item1.Contact__r.DOB__c),
                    this.IdentifierNumber__c = item1.Contact__r.IdentifierNumber__c,
                    this.WitnessStatement__c = item1.WitnessStatement__c != undefined ? item1.WitnessStatement__c : ''
            })

        } catch (err) {
            this.dispatchEvent(utils.handleError(err));
        }
    };

    dataStaff = async (id) => {
        try {

            let data = await getSelectedStaffDropdownValueData({

                id: id
            });
            data.map(item1 => {
                this.isDisabled = true;
                this.JobTitle__c = item1.Contact__r.JobTitle__c,
                    this.EmployeeType__c = item1.Contact__r.EmployeeType__c,
                    this.WitnessStatement__c = item1.WitnessStatement__c != undefined ? item1.WitnessStatement__c : ''

            })

        } catch (err) {
            this.dispatchEvent(utils.handleError(err));
        }
    };

    //PersonKid Options data function
    @wire(getPkcDetails, {
        objInfo: {
            sobjectType: "Actor__c"
        },
        IncidentDetails: "$caseId",


    })
    wiredPkcDetails({
        error,
        data
    }) {
        if (data) {
            let personwithOptions = [];
            data.forEach((key) => {
                this.Assign__c = key.Assign__c;

                let obj = {};
                obj.label = key.Contact__r && key.Contact__r.FirstName__c + " " + key.Contact__r.LastName__c;
                obj.value = key.Contact__r && key.Contact__r.LastName;
                obj.id = key.Id;
                personwithOptions.push(obj);
            });

            this.personValues = personwithOptions;
        } else if (error) { }
    }

    //Staff Options data function
    @wire(getStaffDetails, {
        objInfo: {
            sobjectType: "Actor__c"
        },
        StaffDetails: "$caseId",
    })
    wiredStaffDetails({
        error,
        data
    }) {
        if (data) {
            let staffwithOptions = [];
            data.forEach((key) => {
                let objStaff = {};
                objStaff.label = key.Contact__r && key.Contact__r.FirstName__c + " " + key.Contact__r.LastName__c;
                objStaff.value = key.Contact__r && key.Contact__r.LastName;
                objStaff.id = key.Id;
                staffwithOptions.push(objStaff);
            });

            this.staffValues = staffwithOptions;
        } else if (error) { }
    }

    connectedCallback() {
        this.WitnessDetailsLoad();
        if (this.recordType != undefined) {
            getRecordType({
                name: this.recordType
            })
                .then((result) => {
                    this.RecordTypeId = result;
                }).catch((errors) => {
                    return this.dispatchEvent(utils.handleError(errors));
                });
        }
    }

    validationCheck() {
        if (!this.Person) {
            return this.dispatchEvent(utils.toastMessage('Please Choose Witness Type', "warning"));
        }

        const allValid = [
            ...this.template.querySelectorAll("lightning-radio-group")
        ].reduce((validSoFar, inputCmp) => {
            inputCmp.reportValidity();
            return validSoFar && inputCmp.checkValidity();
        }, true);
        if (!allValid)
            return this.dispatchEvent(utils.toastMessage(constPopupVariables.VALIDATION_WARN, "warning"));
        this.handleSave();
    }

    handleSave() {
        if (this.Person == 'Person/Kid') {
            if (!(this.DOB__c && this.IdentifierNumber__c)) {
                return this.dispatchEvent(utils.toastMessage("Please Select Person/kid/child", "Warning"));
            }
            let myObj = {
                sobjectType: 'Actor__c'
            };

            myObj.Id = this.selectedPersonKidChildDropdownValueId;
            myObj.RoleName__c = true;
            myObj.WitnessStatement__c = this.WitnessStatement__c;

            InsertUpdateContactActor({
                saveObjInsert: myObj
            })
                .then(result => {
                    this.DOB__c = '';
                    this.IdentifierNumber__c = '';
                    this.WitnessStatement__c = '';
                    this.dispatchEvent(utils.toastMessage("Witness Person/kid Details Saved Successfully", "success"));
                    this.openWitnessModal = false;
                    this.stmtdisable = true;
                    this.WitnessDetailsLoad();
                }).catch((errors) => {
                    let config = {
                        friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE,
                        errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
                    }
                    return this.dispatchEvent(utils.handleError(errors, config));
                });

        } else if (this.Person == 'Staff') {
            if (!(this.JobTitle__c && this.EmployeeType__c)) {
                return this.dispatchEvent(utils.toastMessage("Please Select Staff", "Warning"));
            }

            let myObj2 = {
                sobjectType: 'Actor__c'
            };

            myObj2.Id = this.selectedStaffDropdownValueId;
            myObj2.RoleName__c = true;
            myObj2.WitnessStatement__c = this.WitnessStatement__c;

            InsertUpdateContactActor({
                saveObjInsert: myObj2
            })
                .then(result => {
                    this.JobTitle__c = '';
                    this.EmployeeType__c = '';
                    this.WitnessStatement__c = '';
                    this.witnessTypeValue = 'Person/Kid';
                    this.dispatchEvent(utils.toastMessage("Witness Staff Details Saved Successfully", "success"));
                    this.openWitnessModal = false;
                    this.stmtdisable = true;
                    this.WitnessDetailsLoad();
                }).catch((errors) => {
                    let config = {
                        friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE,
                        errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
                    }
                    return this.dispatchEvent(utils.handleError(errors, config));
                });

        } else if (this.Person == 'Witness') {
            if (!this.FirstName) {
                return this.dispatchEvent(utils.toastMessage(`Please Enter First Name `, "warning"))
            }
            if (!this.LastName) {
                return this.dispatchEvent(utils.toastMessage(`Please Enter Last Name `, "warning"))
            }
            let myObj3 = {
                sobjectType: 'Contact'
            };
           
            myObj3.FirstName__c = this.FirstName;
            myObj3.LastName__c = this.LastName;
            myObj3.LastName = myObj3.FirstName__c + ' ' + myObj3.LastName__c;
            myObj3.RecordTypeId = this.RecordTypeId;
            this.addContact.WitnessStatement__c = this.WitnessStatement__c;

            InsertUpdateContactActor({
                saveObjInsert: myObj3
            }).then((result3) => {
                this.addContact.Id = this.WitnessOtherId; 
                this.addContact.sobjectType = "Actor__c";
                this.addContact.Contact__c = result3;
                this.addContact.Referral__c = this.caseId;
                this.addContact.Role__c = 'Witness';
                this.addContact.RoleName__c = true;
              
                InsertUpdateContactActor({
                    saveObjInsert: this.addContact
                }).then((result4) => {
                    this.FirstName = '';
                    this.LastName = '';
                    this.WitnessStatement__c = '';
                    this.witnessTypeValue = 'Person/Kid';
                    this.WitnessOtherId = undefined;
                    this.dispatchEvent(
                        utils.toastMessage("Witness Details Saved Successfully", "success"));
                    this.openWitnessModal = false;
                    this.stmtdisable = true;
                    this.WitnessDetailsLoad();
                }).catch((errors) => {
                    this.dispatchEvent(
                        utils.toastMessage("Error in saving record", "Error"));
                });

            }).catch((errors) => {
                this.dispatchEvent(
                    utils.toastMessage("Error in saving record", "Error"));
            });

        }
        this.closeModal();
    }


    //Function used to show the Data list Witness
    WitnessDetailsLoad() {
        getWitnessDetails({
            IncidentId: this.caseId
        })
            .then((result) => {
                if (result.length > 0) {
                    const witnessDispatch = new CustomEvent('witnessdecisonflag', {
                        detail: {
                            WitnessDataflag: true
                        }
                    });
                    this.dispatchEvent(witnessDispatch);
                    let currentData = [];
                    result.forEach((item) => {
                        let statusClass = "tripledots tripleDot_sec_del";
                        if (this.userProfileName === USER_PROFILE_NAME.USER_CASE_WRKR || (this.incidentStatus !== undefined && (this.incidentStatus !== "Pending" || this.incidentStatus !== "Draft")))
                            statusClass = "tripledots";
                        currentData.push({
                            FirstName: item.Contact__r != undefined ? item.Contact__r.FirstName__c != undefined ? item.Contact__r.FirstName__c : '' : '',
                            LastName: item.Contact__r != undefined ? item.Contact__r.LastName__c != undefined ? item.Contact__r.LastName__c : '' : '',
                            Role__c: item.Role__c != undefined ? item.Role__c : '',
                            WitnessStatement__c: item.WitnessStatement__c != undefined ? item.WitnessStatement__c : '',
                            commentsClass: item.WitnessStatement__c !== undefined ? 'btnbdrIpad blue' : 'svgRemoveicon',
                            Id: item.Id,
                            statusClass: statusClass
                        });

                    });
                    result = currentData;
                    this.totalRecords = currentData;
                    this.totalRecordsCount = this.totalRecords.length;
                    this.pageData();
                }
                if (result.length == 0) {
                    this.norecorddisplay = false;
                } else {
                    this.norecorddisplay = true;
                }
            })
            .catch(errors => {
                let config = {
                    friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
                    errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
                }
                return this.dispatchEvent(utils.handleError(errors, config));

            });
    }


    //Edit function
    WitnessEditDetails(id) {
        getActionDetails({
            EId: id
        })
            .then((result) => {
                this.editWitnessDetails = [...result];
                this.handleChange(result[0].Role__c);
                this.WitnessStatement__c = result[0].WitnessStatement__c;
                this.witnessTypeValue = result[0].Role__c;
                this.personDisabled = true;
                if (result[0].Role__c == 'Person/Kid') {
                    this.selectedPersonKidChildDropdownValueId = result[0].Id;
                    this.Person = result[0].Role__c;
                    this.PersonValue = result[0].Contact__r.LastName !== undefined ? result[0].Contact__r.LastName : '';
                    this.DOB__c = result[0].Contact__r.DOB__c !== undefined ? utils.formatDate(result[0].Contact__r.DOB__c) : '';
                    this.IdentifierNumber__c = result[0].Contact__r.IdentifierNumber__c !== undefined ? result[0].Contact__r.IdentifierNumber__c : '';
                } else if (result[0].Role__c == 'Staff') {
                    this.selectedStaffDropdownValueId = result[0].Id;
                    this.Person = result[0].Role__c;
                    this.Staff = result[0].Contact__r.LastName !== undefined ? result[0].Contact__r.LastName : '';
                    this.JobTitle__c = result[0].Contact__r.JobTitle__c !== undefined ? result[0].Contact__r.JobTitle__c : '';
                    this.EmployeeType__c = result[0].Contact__r.EmployeeType__c !== undefined ? result[0].Contact__r.EmployeeType__c : '';
                } else if (result[0].Role__c == 'Witness') {
                    this.WitnessOtherId = result[0].Id;
                    this.Person = result[0].Role__c;
                    this.FirstName = result[0].Contact__r.FirstName__c !== undefined ? result[0].Contact__r.FirstName__c : '';
                    this.LastName = result[0].Contact__r.LastName__c !== undefined ? result[0].Contact__r.LastName__c : '';
                }
            })
            .catch(errors => {
                let config = {
                    friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
                    errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
                }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
    }


    handleRowAction(event) {
        const action = event.detail.action;
        const row = event.detail.row;
        switch (action.name) {
            case 'Edit':
                this.isDisabled = true;
                this.openWitnessModal = true;
                this.btnLabel = 'UPDATE';
                this.editMode = true;
                this.WitnessEditDetails(row.Id);
                break;
            case 'View':
                this.typeDisable = true;
                this.statementDisable = true;
                this.isDisableName = true;
                this.isDisabled = true;
                this.stmtdisable = true;
                this.personDisabled = true;
                this.openWitnessModal = true;
                this.editMode = false;
                this.WitnessEditDetails(row.Id);
                break;
            case 'Delete':
                this.openmodel = true;
                this.deleteWitnessId = row.Id;
                break;
            case 'witnessstatement':
                this.commentsOfPad = row.WitnessStatement__c;
                this.showCommentsPop = true;
                break;
        }
    }

    deleteFrSure() {
        let witness = this.currentWitnessData.filter(data => data.Id == this.deleteWitnessId)[0];
        let witnesstype = witness.Role__c;
        if (witnesstype == 'Witness') {
            this.openmodel = false;
            this.Spinner = true;
            deleteRecord(this.deleteWitnessId)
                .then(() => {
                    this.Spinner = false;
                    this.dispatchEvent(utils.toastMessage(constPopupVariables.DELETESUCCESS, "success"));
                    this.WitnessDetailsLoad();
                }).catch(error => {
                    this.Spinner = false;
                    this.dispatchEvent(utils.toastMessage(constPopupVariables.UNAUTHOREDEL, "error"));
                })
        } else if (witnesstype == 'Person/Kid' || witnesstype == 'Staff') {
            this.openmodel = false;
            this.Spinner = true;

            let myObj2 = {
                sobjectType: 'Actor__c'
            };

            myObj2.Id = this.deleteWitnessId;
            myObj2.RoleName__c = false;
            myObj2.WitnessStatement__c = null;

            InsertUpdateContactActor({
                saveObjInsert: myObj2
            }).then((result) => {
                this.Spinner = false;
                this.dispatchEvent(utils.toastMessage(constPopupVariables.DELETESUCCESS, "success"));
                this.WitnessDetailsLoad();
            }).catch(error => {
                this.Spinner = false;
                this.dispatchEvent(utils.toastMessage(constPopupVariables.UNAUTHOREDEL, "error"));
            });
        }
    }

    //Pagination
    pageData() {
        let page = this.page;
        let perpage = this.perpage;
        let startIndex = (page * perpage) - perpage;
        let endIndex = (page * perpage);
        this.currentWitnessData = this.totalRecords.slice(startIndex, endIndex);
        if (this.currentWitnessData.length == 0) {
            if (this.page != 1) {
                this.page = this.page - 1;
                this.pageData();
            }
        }
        this.Spinner = false;
    }

    hanldeProgressValueChange(event) {
        this.page = event.detail;
        this.pageData();
    }

}