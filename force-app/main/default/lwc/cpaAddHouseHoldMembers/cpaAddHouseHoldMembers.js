/**
 * @Author        : Pratheeba.V
 * @CreatedOn     : August 3, 2020
 * @Purpose       : CPA House Hold Member Add Screen
 **/
import { LightningElement, wire, api } from 'lwc';
import getRecordType from '@salesforce/apex/CpaSearchHouseHoldMembers.getRecordType';
import getMultiplePicklistValues from '@salesforce/apex/CpaSearchHouseHoldMembers.getMultiplePicklistValues';
import fetchHouseHoldMemberDetails from '@salesforce/apex/CpaSearchHouseHoldMembers.fetchHouseHoldMemberDetails';
import checkApplicant from '@salesforce/apex/CpaSearchHouseHoldMembers.checkApplicant';
import fetchHouseHoldMemberFrmActorDetails from '@salesforce/apex/CpaSearchHouseHoldMembers.fetchHouseHoldMemberFrmActorDetails';
import saveProfilePic from '@salesforce/apex/CpaSearchHouseHoldMembers.saveProfilePic';
import updateProfilePic from '@salesforce/apex/CpaSearchHouseHoldMembers.updateProfilePic';
import getRefContactExistDet from '@salesforce/apex/CpaSearchHouseHoldMembers.getRefContactExistDet';
import { createRecord, updateRecord } from "lightning/uiRecordApi";
import { refreshApex } from "@salesforce/apex";
import CONTACT_OBJECT from '@salesforce/schema/Contact';
import IDHOUSEHOLDMEM_FIELD from '@salesforce/schema/Contact.Id';
import ACTOR_OBJECT from '@salesforce/schema/Actor__c';
import ACTORID_FIELD from '@salesforce/schema/Actor__c.Id';
import { utils } from 'c/utils';
import * as sharedData from "c/sharedData";
import { constPopupVariables, CJAMS_OBJECT_QUERIES } from 'c/constants';

let convertedDataURI; //holds image data

export default class CpaAddHouseHoldMembers extends LightningElement {
    holdMember = {
        Signsofmentalillness__c: 'Unknown',
        Dangertoself__c: 'Unknown',
        Dangertoworker__c: 'Unknown',
        Appearanceofmentallyimpaired__c: 'Unknown',
        PersonRole__c: 'Other',
        DeclaredDisable__c: 'No',
        LicensedChildCareProvider__c: 'No',
        ApplyingforChildCareProvider__c: 'No',
        LicensePreviouslyDenied__c: 'No',
        Caringforaged__c: 'No'
    };
    //Validation
    dangerToSelf = false;
    apperanceOfMentallyImp = false;
    dangerToWorkerReas = false;
    signsOfMentalI = false;
    currentDate = utils.formatDateYYYYMMDD(new Date());
    childCareLicenseNumber = false;
    deniedLicenseExplain = false;

    //PickList Tracks
    title = 'Add New Household Member';
    titleOptions;
    primCitizenshipValues;
    primLanguageValues;
    nationalityValues;
    genderValues;
    religionValues;
    skinToneValues;
    hairClrValues;
    livingSituationValues;
    personRoleValues;
    roleValues;
    dangerToSelfValues;
    mentallyImpiredValues;
    dangerToWorkerValues;
    signsMentalIllValues;
    Role__c;
    declaredDisableValues;
    licensedChildProvider;
    applyingChildProvider;
    applyingFrChildCareProviderValues;
    deniedLicenseValues;
    caringFrAgedValues;
    @api searchHolderShow = false;
    @api isDisabled = false;
    @api addressAndPathComponent;
    @api cpaData;
    saveOrUpdate = 'Save';
    cpaId;
    @api cpaDataNew;
    @api searchPage;

    //Profile Pic Track
    profilePic;
    downloadImage = '/sfc/servlet.shepherd/version/download/';
    get acceptedFilesFormat() {
        return ['.jpg', '.jpeg', '.png'];
    }
    file;
    fileContents;
    fileReader;
    conVersionId;
    imageUptate = false;
    isApplicant = false;

    connectedCallback() {
        let a = JSON.parse(this.cpaData);   
        if(a.rowActionValue) {
            this.cpaId = a.rowActionValue.CPAId;
            if (a.rowActionValue.CPAStatus == "Suspended" || a.rowActionValue.CPAStatus == "Active") 
            this.isDisabled = true;
        } else {
            this.cpaId = this.cpaDataNew;
        }
    }

    @wire(getRecordType, {
        name: 'Household Members'
    })
    recordTypeDetails(data) {
        try {
            if (data.data) {
                this.recordTypeId = data.data;
            } 
        } catch (error) {
            this.dispatchEvent(utils.handleError(error));
        }
    }

    @wire(getRefContactExistDet, {
        providerId: '$providerId'
    })
    refContDetaisl(data) {
        this.wiredExistDataDetails = data;
        try {
            if (data.data != undefined && data.data.length > 0) {
                this.existDataDetails = data.data[0];
            } else if (data.error) {
                let errors = data.error;
                if (errors)
                    return this.dispatchEvent(utils.handleError(errors));
            }
        } catch (error) {
            throw error;
        }
    }

    get houseHoldMemberId() {
        return sharedData.getHouseHoldContactId();
    }

    get providerId() {
        return sharedData.getProviderId();
    }

    get allPickListFields() {
        return CJAMS_OBJECT_QUERIES.FETCH_FIELDS_FOR_PUBLIC_HOUSEHOLD_PICKLIST;
    }

    editRecordMode() {
        let idTo;
        let type;
        if (this.providerId !== undefined) {
            idTo = this.providerId;
            type = 'Provider__c';
        }
        fetchHouseHoldMemberFrmActorDetails({
            id: idTo,
            contactId: this.houseHoldMemberId,
            type
        }).then(
            result => {
                try {
                    let datas = JSON.parse(result);
                    if (datas[0].housHoldRecord.length > 0) {
                        this.originalHoldMemberValue = Object.assign({}, datas[0].housHoldRecord[0]);
                        let a = Object.assign({}, datas[0].housHoldRecord[0].Contact__r);
                        delete a.attributes;
                        this.holdMember = a;
                        this.Role__c = (this.searchPage) ? '' : datas[0].housHoldRecord[0].Role__c;
                        if (datas[0].profilePicUrl !== undefined && datas[0].profilePicUrl !== '') {
                            this.conVersionId = datas[0].profilePicUrl;
                            this.profilePic = this.downloadImage + datas[0].profilePicUrl;
                            let className = this.template.querySelector('.profile-pic-back');
                            if (className !== undefined && className !== null)
                                className.className = 'profile-picA';
                        }
                        this.handleCheckBoxValues();
                        if (this.searchHolderShow === false) {
                            this.title = 'Edit Household Member';
                            this.saveOrUpdate = 'Update';
                        } else {
                            this.isDisabled = true;
                            return this.dispatchEvent(utils.toastMessage('This Household Member Already Added for the Refferal!..', "warning"));
                        }
                    } else this.fetchForSearch();
                } catch (error) {
                    throw error;
                }
            }).catch((errors) => {
                return this.dispatchEvent(utils.handleError(errors));
            });
    }

    fetchForSearch() {
        fetchHouseHoldMemberDetails({
            id: this.houseHoldMemberId
        }).then(result => {
            if (result != undefined && result.length > 0 && this.searchHolderShow === true) {
                let datas = JSON.parse(result);
                this.originalHoldMemberValue = Object.assign({}, datas[0].housHoldRecord[0]);
                this.holdMember = Object.assign({}, datas[0].housHoldRecord[0]);
                if (datas[0].profilePicUrl !== undefined && datas[0].profilePicUrl !== '') {
                    this.conVersionId = datas[0].profilePicUrl;
                    this.profilePic = this.downloadImage + datas[0].profilePicUrl;
                    let className = this.template.querySelector('.profile-pic-back');
                    if (className !== undefined && className !== null)
                        className.className = 'profile-picA';
                }
                this.handleCheckBoxValues();
                this.title = 'Add New Household Member';
                this.saveOrUpdate = 'Save';
            }
        }).catch((errors) => {
            return this.dispatchEvent(utils.handleError(errors));
        });
    }

    @wire(getMultiplePicklistValues, {
        objInfo: 'Contact',
        picklistFieldApi: '$allPickListFields'
    })
    wiredTitle(data) {
        try {
            if (data.data != undefined && data.data !== '') {
                this.titleOptions = data.data.Title__c;
                this.primCitizenshipValues = data.data.PrimaryCitizenship__c;
                this.primLanguageValues = data.data.PrimaryLanguage__c;
                this.nationalityValues = data.data.Nationality__c;
                this.genderValues = data.data.Gender__c;
                this.religionValues = data.data.Religion__c;
                this.skinToneValues = data.data.SkinTone__c;
                this.livingSituationValues = data.data.LivingSituation__c;
                this.hairClrValues = data.data.HairColor__c;
                this.personRoleValues = data.data.PersonRole__c;
                this.roleValues = data.data.Role__c;
                this.dangerToSelfValues = data.data.Dangertoself__c;
                this.mentallyImpiredValues = data.data.Appearanceofmentallyimpaired__c;
                this.dangerToWorkerValues = data.data.Dangertoworker__c;
                this.signsMentalIllValues = data.data.Signsofmentalillness__c;
                this.declaredDisableValues = data.data.DeclaredDisable__c;
                this.licensedChildProvider = data.data.LicensedChildCareProvider__c;
                this.applyingFrChildCareProviderValues = data.data.ApplyingforChildCareProvider__c;
                this.deniedLicenseValues = data.data.LicensePreviouslyDenied__c;
                this.caringFrAgedValues = data.data.Caringforaged__c;
                if (this.houseHoldMemberId !== undefined && this.houseHoldMemberId !== '') this.editRecordMode();
                else this.Spinner = false;
                refreshApex(this.wiredExistDataDetails);
            } else if (data.error) {
                let errors = data.error;
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const {
                        title,
                        message,
                        errorType
                    } = error;
                    this.dispatchEvent(utils.toastMessageWithTitle(title, message, errorType));
                } else {
                    this.dispatchEvent(utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE));
                }
            }
        } catch (error) {
            throw error;
        }
    }

    checkApplicantExists(){
        checkApplicant({
            cpaId: this.cpaId
        }).then(result => {
            let a = result.find(x => x.Role__c === 'Applicant');
            if(a) 
             this.isApplicant = true;
            // result.map((item) => {
            //     if (['Applicant'].includes(item.Role__c)) {
            //         return this.dispatchEvent(utils.toastMessage(constPopupVariables.HOUSEHOLD_APPLICANT, "warning"));
            //     }
            // }); 
        }).catch(errors => {
            return this.dispatchEvent(utils.handleError(errors));
        });
    }

    onChangeHandlers(event) {
        switch (event.target.name) {
            case 'title':
                this.holdMember.Title__c = event.target.value;
                break;
            case 'livingSituationDesc':
                this.holdMember.LivingSituationDescription__c = event.target.value;
                break;
            case 'appxAge':
                event.target.value = event.target.value.replace(/(\D+)/g, "");
                this.holdMember.ApproximateAge__c = event.target.value;
                break;
            case 'hgtInInch':
                event.target.value = event.target.value.replace(/(\D+)/g, "");
                this.holdMember.HeightinInch__c = event.target.value;
                break;
            case 'wgtInPound':
                event.target.value = event.target.value.replace(/(\D+)/g, "");
                this.holdMember.WeightsinPound__c = event.target.value;
                break;
            case 'gender':
                this.holdMember.Gender__c = event.target.value;
                break;
            case 'hairClr':
                this.holdMember.HairColor__c = event.target.value;
                break;
            case 'skinTone':
                this.holdMember.SkinTone__c = event.target.value;
                break;
            case 'firstName':
                this.holdMember.FirstName__c = event.target.value;
                break;
            case 'middleName':
                this.holdMember.MiddleName__c = event.target.value;
                break;
            case 'lastName':
                this.holdMember.LastName__c = event.target.value;
                break;
            case 'DOB':
                this.holdMember.DOB__c = event.target.value;
                this.holdMember.ApproximateAge__c = utils.getAgeByDOB(this.holdMember.DOB__c);
                break;
            case 'physicalMark':
                this.holdMember.PhysicalMark__c = event.target.value;
                break;
            case 'isUsClient':
                this.holdMember.IsaUsClient__c = event.target.checked;
                break;
            case 'isSSNVerified':
                this.holdMember.SSNVerified__c = event.target.checked;
                break;
            case 'stateOrDriveId':
                this.holdMember.StateIDDriversLicense__c = event.target.value;
                break;
            case 'ssn':
                event.target.value = event.target.value.replace(/(\D+)/g, "");
                this.holdMember.SSN__c = utils.formattedSSN(event.target.value);
                break;
            case 'primCitizen':
                this.holdMember.PrimaryCitizenship__c = event.target.value;
                break;
            case 'primLang':
                this.holdMember.PrimaryLanguage__c = event.target.value;
                break;
            case 'nationality':
                this.holdMember.Nationality__c = event.target.value;
                break;
            case 'dod':
                this.holdMember.DateofDeath__c = event.target.value;
                break;
            case 'occupation':
                this.holdMember.Occupation__c = event.target.value;
                break;
            case 'religion':
                this.holdMember.Religion__c = event.target.value;
                break;
            case 'startdate':
                this.holdMember.StartDate__c = event.target.value;
                break;
            case 'enddate':
                this.holdMember.EndDate__c = event.target.value;
                break;
            case 'childPreviAdopted':
                this.holdMember.ChildpreviouslyAdopted__c = event.target.checked;
                break;
            case 'livingSitution':
                this.holdMember.LivingSituation__c = event.target.value;
                break;
            case 'livingSituationDesc':
                this.holdMember.LivingSituationDescription__c = event.target.value;
                break;
            case 'roleValues':
                if (event.target.value === 'Applicant') {
                    this.Role__c = event.target.value;
                    return this.checkApplicantExists();
                } else if (event.target.value === 'Co-Applicant' && this.originalHoldMemberValue === undefined) {
                    if (this.existDataDetails !== undefined && this.existDataDetails.coApplicant > 0) {
                        event.target.value = undefined;
                        return this.dispatchEvent(utils.toastMessage(constPopupVariables.HOUSEHOLD_COAPPLICANT, "warning"));
                    } else {
                        this.Role__c = event.target.value;
                    }
                } else if (event.target.value === 'Co-Applicant' && this.originalHoldMemberValue !== undefined && this.originalHoldMemberValue.Role__c !== 'Co-Applicant') {
                    if (this.existDataDetails !== undefined && this.existDataDetails.coApplicant > 0) {
                        event.target.value = undefined;
                        return this.dispatchEvent(utils.toastMessage(constPopupVariables.HOUSEHOLD_COAPPLICANT, "warning"));
                    } else {
                        this.Role__c = event.target.value;
                    }
                } else {
                    this.Role__c = event.target.value;
                }
                break;
            case 'sexOffender':
                this.holdMember.SexOffenderregisteryChecked__c = event.target.checked;
                break;
            case 'judiCaseSearch':
                this.holdMember.JudiciarycaseSearch__c = event.target.checked;
                break;
            case 'substanceNewBorn':
                this.holdMember.Substanceexposednewborn__c = event.target.checked;
                break;
            case 'safeHveBaby':
                this.holdMember.Safeheavenbaby__c = event.target.checked;
                break;
            case 'dangerToSelfReas':
                this.holdMember.DangertoselfReason__c = event.target.value;
                break;
            case 'dangerToWorkerReas':
                this.holdMember.DangertoworkerReason__c = event.target.value;
                break;
            case 'signsOfMentalI':
                this.holdMember.MentalillnessReason__c = event.target.value;
                break;
            case 'apperanceOfMentallyImp':
                this.holdMember.MentallyimpairedReason__c = event.target.value;
                break;
            case 'householdOther':
                this.holdMember.PersonRole__c = event.target.value;
                break;
            case 'dangerToSelf':
                this.holdMember.Dangertoself__c = event.target.value;
                this.dangerToSelf = this.holdMember.Dangertoself__c === 'Yes' ? true : false;
                break;
            case 'apperanceOfMentallyImpPick':
                this.holdMember.Appearanceofmentallyimpaired__c = event.target.value;
                this.apperanceOfMentallyImp = this.holdMember.Appearanceofmentallyimpaired__c === 'Yes' ? true : false;
                break;
            case 'dangerToWorker':
                this.holdMember.Dangertoworker__c = event.target.value;
                this.dangerToWorkerReas = this.holdMember.Dangertoworker__c === 'Yes' ? true : false;
                break;
            case 'signsMentalIll':
                this.holdMember.Signsofmentalillness__c = event.target.value;
                this.signsOfMentalI = this.holdMember.Signsofmentalillness__c === 'Yes' ? true : false;
                break;
            case 'declaredDisable':
                this.holdMember.DeclaredDisable__c = event.target.value;
                break;
            case 'lincensedChildProvider':
                this.holdMember.LicensedChildCareProvider__c = event.target.value;
                this.childCareLicenseNumber = this.holdMember.LicensedChildCareProvider__c === 'Yes' ? true : false;
                break;
            case 'childCareLicenseNumberText':
                this.holdMember.ChildCareLicenseNumber__c = event.target.value;
                break;
            case 'currentlyApplyingFrLicense':
                this.holdMember.ApplyingforChildCareProvider__c = event.target.value;
                break;
            case 'deniedLicense':
                this.holdMember.LicensePreviouslyDenied__c = event.target.value;
                this.deniedLicenseExplain = this.holdMember.LicensePreviouslyDenied__c === 'Yes' ? true : false;
                break;
            case 'deniedLicenseExplainText':
                this.holdMember.ExplanantionforLicenseDenial__c = event.target.value;
                break;
            case 'caringFrAged':
                this.holdMember.Caringforaged__c = event.target.value;
                break;
        }
    }

    handleCheckBoxValues() {
        this.template.querySelectorAll('[data-element="isUsClient"]')
            .forEach(element => {
                element.checked = this.holdMember.IsaUsClient__c;
            });
        this.template.querySelectorAll('[data-element="isSSNVerified"]')
            .forEach(element => {
                element.checked = this.holdMember.SSNVerified__c;
            });
        this.template.querySelectorAll('[data-element="childPreviAdopted"]')
            .forEach(element => {
                element.checked = this.holdMember.ChildpreviouslyAdopted__c;
            });
        this.template.querySelectorAll('[data-element="sexOffender"]')
            .forEach(element => {
                element.checked = this.holdMember.SexOffenderregisteryChecked__c;
            })
        this.template.querySelectorAll('[data-element="judiCaseSearch"]')
            .forEach(element => {
                element.checked = this.holdMember.JudiciarycaseSearch__c;
            });
        this.template.querySelectorAll('[data-element="substanceNewBorn"]')
            .forEach(element => {
                element.checked = this.holdMember.Substanceexposednewborn__c;
            });
        this.template.querySelectorAll('[data-element="safeHveBaby"]')
            .forEach(element => {
                element.checked = this.holdMember.Safeheavenbaby__c;
            });
        this.dangerToSelf = this.holdMember.Dangertoself__c === 'Yes' ? true : false;
        this.apperanceOfMentallyImp = this.holdMember.Appearanceofmentallyimpaired__c === 'Yes' ? true : false;
        this.dangerToWorkerReas = this.holdMember.Dangertoworker__c === 'Yes' ? true : false;
        this.signsOfMentalI = this.holdMember.Signsofmentalillness__c === 'Yes' ? true : false;
        this.childCareLicenseNumber = this.holdMember.LicensedChildCareProvider__c === 'Yes' ? true : false;
        this.deniedLicenseExplain = this.holdMember.LicensePreviouslyDenied__c === 'Yes' ? true : false;
        this.Spinner = false;
    }

    validationBfSaveOrUpdate() {
        const allValid = [
            ...this.template.querySelectorAll("lightning-input"),
            ...this.template.querySelectorAll("lightning-combobox")
        ].reduce((validSoFar, inputCmp) => {
            inputCmp.reportValidity();
            return validSoFar && inputCmp.checkValidity();
        }, true);
        if(this.Role__c=='Applicant') {
            if(this.isApplicant)
                return this.dispatchEvent(utils.toastMessage(constPopupVariables.HOUSEHOLD_APPLICANT, "warning"));
        }        
        if (!allValid)
            return this.dispatchEvent(utils.toastMessage(constPopupVariables.VALIDATION_WARN, "warning"));
        else 
            this.isDisabled=true;
        if (this.holdMember.SSN__c !== undefined && this.holdMember.SSN__c.length !== 11)
            return this.dispatchEvent(utils.toastMessage('Please Check the SSN Number!..', "warning"));
        else
            this.isDisabled=true;
        this.holdMember.Completedfields__c = Object.keys(this.holdMember).length;
        this.saveOrUpdateHouseHoldMember();
    }

    saveOrUpdateHouseHoldMember() {
        this.Spinner = true;
        this.holdMember.LastName = this.holdMember.FirstName__c + " " + this.holdMember.LastName__c;
        let fields = this.holdMember;
        if (this.houseHoldMemberId === undefined && this.searchHolderShow === false)
            this.saveHouseHoldMember(fields);
        else
            this.updateHouseHoldMember(fields);
    }

    saveHouseHoldMember(fields) {
        fields.RecordTypeId = this.recordTypeId;
        const recordInput = {
            apiName: CONTACT_OBJECT.objectApiName,
            fields
        };

        createRecord(recordInput)
            .then(result => {
                try {
                    if (result.id) {
                        let fields = {};
                        fields.Role__c = this.Role__c;
                        fields.CPAHomes__c = this.cpaId;
                        fields.Contact__c = result.id;
                        fields.Provider__c = this.providerId;
                        fields.ClearanceStatus__c = 'Pending';
                        const recordInput = {
                            apiName: ACTOR_OBJECT.objectApiName,
                            fields
                        }
                        createRecord(recordInput)
                            .then(actorResult => {
                            });
                    }
                    if (result.id && this.file !== undefined && this.fileContents !== undefined) {
                        let base64 = 'base64,';
                        let content = this.fileContents.indexOf(base64) + base64.length;
                        convertedDataURI = this.fileContents.substring(content);
                        saveProfilePic({
                            strProfileImg: convertedDataURI,
                            recId: result.id
                        }).then(result => {
                            this.dispatchEvent(utils.toastMessage(constPopupVariables.HOUSEHOLD_SUCCESS, "success"));
                            this.redirectToHouseHoldMemberList();
                        }).catch((errors) => {
                            return this.dispatchEvent(utils.handleError(errors));
                        });
                    } else {
                        this.dispatchEvent(utils.toastMessage(constPopupVariables.HOUSEHOLD_SUCCESS, "success"));
                        this.redirectToHouseHoldMemberList();
                    }
                } catch (error) {
                    throw error;
                }
            });
    }

    updateHouseHoldMember(fields) {
        fields[IDHOUSEHOLDMEM_FIELD.fieldApiName] = this.houseHoldMemberId;
        const recordInput = {
            fields
        };
        updateRecord(recordInput)
            .then(result => {
                try {
                    if (result.id) {
                        if (this.searchHolderShow === true) {
                            let fields = {};
                            fields.Role__c = this.Role__c;
                            fields.CPAHomes__c = this.cpaId;
                            fields.Contact__c = result.id;
                            fields.Provider__c = this.providerId;
                            fields.ClearanceStatus__c = 'Pending';
                            const recordInput = {
                                apiName: ACTOR_OBJECT.objectApiName,
                                fields
                            }
                            createRecord(recordInput)
                                .then(actorResult => {
                                });
                        } else {
                            let fields = {};
                            fields.Role__c = this.Role__c;
                            fields.CPAHomes__c = this.cpaId;
                            fields.Contact__c = result.id;
                            fields.Provider__c = this.providerId;
                            fields.ClearanceStatus__c = 'Pending';
                            fields[ACTORID_FIELD.fieldApiName] = this.originalHoldMemberValue.Id;
                            const recordInput = {
                                fields
                            }
                            updateRecord(recordInput)
                                .then(actorResult => {
                                });
                        }
                    }
                    if (this.imageUptate === true) {
                        let base64 = 'base64,';
                        let content = this.fileContents.indexOf(base64) + base64.length;
                        convertedDataURI = this.fileContents.substring(content);
                        updateProfilePic({
                            strProfileImg: convertedDataURI,
                            recId: this.conVersionId
                        }).then(result => {
                            this.dispatchEvent(utils.toastMessage(constPopupVariables.HOUSEHOLD_UPDATE, "success"));
                            this.redirectToHouseHoldMemberList();
                        }).catch((errors) => {
                            return this.dispatchEvent(utils.handleError(errors));
                        });
                    } else if (this.file !== undefined && this.fileContents !== undefined) {
                        let base64 = 'base64,';
                        let content = this.fileContents.indexOf(base64) + base64.length;
                        convertedDataURI = this.fileContents.substring(content);

                        saveProfilePic({
                            strProfileImg: convertedDataURI,
                            recId: result.id
                        })
                            .then(result => {
                                if (this.searchHolderShow === false)
                                    this.dispatchEvent(utils.toastMessage(constPopupVariables.HOUSEHOLD_UPDATE, "success"));
                                else this.dispatchEvent(utils.toastMessage(constPopupVariables.HOUSEHOLD_SUCCESS, "success"));
                                this.redirectToHouseHoldMemberList();
                            }).catch((errors) => {
                                return this.dispatchEvent(utils.handleError(errors));
                            });
                    } else {
                        this.dispatchEvent(utils.toastMessage(constPopupVariables.HOUSEHOLD_UPDATE, "success"));
                        this.redirectToHouseHoldMemberList();
                    }

                } catch (error) {
                    throw error;
                }
            });
    }

    //Redirection To HouseHold Members Dashboard
    redirectToHouseHoldMemberList() {
        const onClickId = new CustomEvent('redirecttoholdmembersdashboard', {
            detail: {
                first: false
            }
        });
        this.dispatchEvent(onClickId);
        this.Spinner = false;
        this.pageRefresh();
    }

    redirectToHouseHoldMemberSearch() {
        const onClickId = new CustomEvent('redirecttoholdmembersearch', {
            detail: {
                first: false
            }
        });
        this.dispatchEvent(onClickId);
        this.pageRefresh();
    }

    //Image Upload
    handleUpload(event) {
        if (this.profilePic !== undefined && this.profilePic !== '')
            this.imageUptate = true;
        const uploadedFiles = event.detail.files;
        if (uploadedFiles.length > 0) {
            this.file = uploadedFiles[0];

            this.fileReader = new FileReader();
            // set onload function of FileReader object  
            this.fileReader.onloadend = (() => {
                this.fileContents = this.fileReader.result;
                this.profilePic = this.fileContents;
                let className = this.template.querySelector('.profile-pic-back');
                if (className !== undefined && className !== null)
                    className.className = 'profile-picA';
            });
            this.fileReader.readAsDataURL(this.file);
        } else return;
    }

    pageRefresh() {
        this.holdMember = {
            Signsofmentalillness__c: 'Unknown',
            Dangertoself__c: 'Unknown',
            Dangertoworker__c: 'Unknown',
            Appearanceofmentallyimpaired__c: 'Unknown',
            PersonRole__c: 'Other'
        };
        this.fileContents = undefined;
        this.profilePic = undefined;
        let className = this.template.querySelector('.profile-picA');
        if (className !== undefined && className !== null)
            className.className = 'profile-picA profile-pic-back';
        this.file = undefined;
    }
}