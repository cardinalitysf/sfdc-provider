/* eslint-disable vars-on-top */
/* eslint-disable no-shadow */
/***
 * @Author: Janaswini S
 * @CreatedOn: April 15, 2020
 * @Purpose: Providers Monitoring Add/View
 **/
import {
  LightningElement,
  track,
  wire
} from 'lwc';

import getApplicationId from '@salesforce/apex/providersMonitoring.getApplicationId';
import getLicensesId from '@salesforce/apex/providersMonitoring.getLicensesId';
import getMonitoringDetails from '@salesforce/apex/ProvidersMonitoring.getMonitoringDetails';
import InsertUpdateMonitoring from '@salesforce/apex/ProvidersMonitoring.InsertUpdateMonitoring';
import getAddressDetails from '@salesforce/apex/ProvidersMonitoring.getAddressDetails';
import getProgramDetails from '@salesforce/apex/ProvidersMonitoring.getProgramDetails';
import {
  getObjectInfo
} from 'lightning/uiObjectInfoApi';
import Monitoring__c_OBJECT from '@salesforce/schema/Monitoring__c';
import {
  getPicklistValuesByRecordType
} from 'lightning/uiObjectInfoApi';
import getAppProviderid from '@salesforce/apex/ProvidersMonitoring.getAppProviderid';
import viewMonitoringDetails from '@salesforce/apex/ProvidersMonitoring.viewMonitoringDetails';
import editMonitoringDetails from '@salesforce/apex/ProvidersMonitoring.editMonitoringDetails';
import getApplicationProviderStatus from "@salesforce/apex/ProvidersMonitoring.getApplicationProviderStatus";
import getMonitoringStatus from "@salesforce/apex/ProvidersMonitoring.getMonitoringStatus";
import * as sharedData from 'c/sharedData';
import {
  utils
} from 'c/utils';
import {
  refreshApex
} from '@salesforce/apex';
import { CJAMS_CONSTANTS } from "c/constants";
import images from '@salesforce/resourceUrl/images';

//Sundar adding this method for getting Expired License Details
import getApplicationLicenseStatus from '@salesforce/apex/ProvidersMonitoring.getApplicationLicenseStatus';


let i = 0;

const actions = [{
  label: 'Edit',
  name: 'Edit',
  iconName: 'utility:edit',
  target: '_self'
},
{
  label: 'View',
  name: 'View',
  iconName: 'utility:preview',
  target: '_self'
},
];
const columns = [{
  label: 'VISIT ID',
  fieldName: 'linkName',
  type: 'button',
  typeAttributes: {
    label: {
      fieldName: 'Name'
    }
  }
},
{
  label: 'PROGRAM TYPE',
  fieldName: 'ProgramType__c',
  type: 'button',
  sortable: true,
  typeAttributes: {
    name: 'dontRedirect',
    label: {
      fieldName: 'ProgramType__c'
    }
  },
  cellAttributes: {
    class: "title"
  }
},
{
  label: 'VISIT DATE',
  fieldName: 'VisitationStartDate__c',
  sortable: true
},
{
  label: 'SITE',
  fieldName: 'SiteAddress',
  type: 'button',
  sortable: true,
  typeAttributes: {
    name: 'dontRedirect',
    label: {
      fieldName: 'SiteAddress'
    }
  },
  cellAttributes: {
    class: "title"
  }
},
{
  label: 'FREQUENCY',
  fieldName: 'SelectPeriod__c',
  sortable: true
},
{
  label: 'APPLICATION',
  fieldName: 'ApplicationName',
  sortable: true
},
{
  label: 'LICENSE NUMBER',
  fieldName: 'LicenseName',
  sortable: true
},
{
  label: 'STATUS',
  fieldName: 'Status__c',
  cellAttributes: {
    class: {
      fieldName: 'CSSClass'
    }
  }
},

{
  label: 'Action',
  fieldName: 'id',
  initialWidth: 100,
  type: "button",
  typeAttributes: {
    iconName: 'utility:preview',
    name: 'View',
    title: 'View',
    initialWidth: 20,
    iconPosition: 'left',
    target: '_self'
  },
},
{
  fieldName: 'id',
  type: "button",
  typeAttributes: {
    iconName: 'utility:edit',
    name: 'Edit',
    title: 'Edit',
    initialWidth: 20,
    class: 'action-position',
    iconPosition: 'left',
    target: '_self'
  },
},
];

export default class ProvidersMonitoring extends LightningElement {
  @track siteaddress;
  @track selectperiod;
  @track columns = columns;
  @track visitationstartdate;
  @track announced;
  @track timevisit;
  @track openmodel = false;
  @track viewmodel = false;
  @track currentPageMonitoringData;
  @track norecorddisplay = true;
  @track totalRecords;
  @track totalRecordsCount = 0;
  @track page = 1;
  @track licenseNumbers;
  perpage = 10;
  setPagination = 5;
  @track addEditMonitoring = {
    SelectPeriod__c: '',
    VisitationStartDate__c: '',
    VisitationEndDate__c: '',
    AnnouncedUnannouced__c: '',
    TimetoVisit__c: '',
    JointlyInspectedWith__c: '',
    License__c: '',
    ProgramName__c: '',
    ProgramType__c: ''
  };
  @track Program__c;
  @track SiteAddress;
  @track ProgramType__c;
  addressId;
  applicationId;
  @track inspectedwithValues;
  @track monitoringID;
  @track ApplicationLicenseId__c;
  @track defaultSortDirection = 'asc';
  @track sortDirection = 'asc';
  @track sortedBy = 'VisitationStartDate__c';
  @track renewalmindate = '1970-01-01';
  @track renewalmaxdate = '1970-01-02';
  @track freezeMonitoringScreen = false;
  @track Spinner = true;
  @track licensesIdList;
  @track objapplicationid;
  @track resultLicence;
  @track resultLicenceId;
  @track resultLicenceName;
  @track primaryLicenseId;
  @track ProgramType__c;
  @track Application__Name;
  @track ProgramName__c;
  @track recordId;
  @track LicenseValue;
  @track items = []; //this will hold key, value pair
  @track value = ''; //initialize combo box value
  @track currentDate;
  @track selectFrequencyValues;
  @track selectPeriodValues;

  @track chosenValue = '';

  @track btnDisable = false;
  @track title;

  applicationforApiCall = this.getapplicationid;
  attachmentIcon = images + '/application-monitoring.svg';

  //Sundar - Checking SuperUser Login
  @track btnLabel;
  finalStatus;
  @track isUpdateFlag = false;
  @track isDisableEndDate = false;
  @track isLicenseExpired = false;



  get isSuperUserFlow() {
    let userProfile = sharedData.getUserProfileName();

    if (userProfile === 'Supervisor')
      return true;
    else
      return false;
  }

  //get Application Status
  @wire(getApplicationProviderStatus, {
    applicationId: '$applicationforApiCall'
  })
  applicationStatus({
    error,
    data
  }) {
    if (data) {
      if (data.length > 0 && (data[0].Status__c == "Caseworker Submitted" || data[0].Status__c == "Approved" || data[0].Status__c == "Supervisor Rejected" || data[0].Status__c == "Rejected" || data[0].Status__c == "Appeal")) {
        this.freezeMonitoringScreen = true;
        this.finalStatus = data[0].Status__c;
      } else {
        this.freezeMonitoringScreen = false;
      }
    } else if (error) {
      let errors = error;
      if (errors) {
        let error = JSON.parse(errors.body.message);
        const { title, message, errorType } = error;
        this.dispatchEvent(
          utils.toastMessageWithTitle(title, message, errorType)
        );
      } else {
        this.dispatchEvent(
          utils.toastMessage(
            CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
            CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
          )
        );
      }
    }
  }

  //Declare Monitoring object into one variable
  @wire(getObjectInfo, {
    objectApiName: Monitoring__c_OBJECT
  })
  objectInfo;

  //Function used to get all picklist values from Monitoring object
  @wire(getPicklistValuesByRecordType, {
    objectApiName: Monitoring__c_OBJECT,
    recordTypeId: '$objectInfo.data.defaultRecordTypeId'
  })
  getAllPicklistValues({
    error,
    data
  }) {
    if (data) {
      // inspectedwith
      let inspectedwithOptions = [];
      data.picklistFieldValues.JointlyInspectedWith__c.values.forEach(key => {
        inspectedwithOptions.push({
          label: key.label,
          value: key.value
        })
      });
      this.inspectedwithValues = inspectedwithOptions;

      //timevisitValues
      let timevisitOptions = [];
      data.picklistFieldValues.TimetoVisit__c.values.forEach(key => {

        timevisitOptions.push({
          label: key.label,
          value: key.value
        })
      });
      this.timevisitValues = timevisitOptions;

      //selectPeriodValues
      let selectPeriodOptions = [];
      if (this.isLicenseExpired) {
        data.picklistFieldValues.SelectPeriod__c.values.forEach(key => {
          selectPeriodOptions.push({
            label: key.label,
            value: key.value
          })
        });
      } else {
        data.picklistFieldValues.SelectPeriod__c.values.forEach(key => {
          if (key.label != 'Re-Licensure') {
            selectPeriodOptions.push({
              label: key.label,
              value: key.value
            })
          }
        });
      }
      this.selectPeriodValues = selectPeriodOptions;

      //selectAnnouncedValues
      let selectAnnouncedOptions = [];
      data.picklistFieldValues.AnnouncedUnannouced__c.values.forEach(key => {
        selectAnnouncedOptions.push({
          label: key.label,
          value: key.value
        })
      });
      this.selectAnnouncedValues = selectAnnouncedOptions;
    } else if (error) {
      this.error = JSON.stringify(error);
    }
  }

  //Function used to get License number  

  //gettter to return items which is mapped with options attribute
  get roleOptions() {
    return this.items;
  }

  //this value will be shown as selected value of combobox item
  get selectedValue() {
    return this.chosenValue;
  }

  //Function used to get provider id from shared data
  get getproviderid() {
    return sharedData.getProviderId();
  }

  get getUserProfileName() {
    return sharedData.getUserProfileName();
  }

  get siteAddressValues() {
    return [{
      label: this.SiteAddress,
      value: this.SiteAddress
    },];
  }

  // Used to sort the 'Age' column
  sortBy(field, reverse, primer) {
    const key = primer ?
      function (x) {
        return primer(x[field]);
      } :
      function (x) {
        return x[field];
      };

    return function (a, b) {
      a = key(a);
      b = key(b);
      return reverse * ((a > b) - (b > a));
    };
  }

  onHandleSort(event) {
    const {
      fieldName: sortedBy,
      sortDirection
    } = event.detail;
    const cloneData = [...this.currentPageMonitoringData];
    cloneData.sort(this.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1));
    this.currentPageMonitoringData = cloneData;
    this.sortDirection = sortDirection;
    this.sortedBy = sortedBy;
  }

  connectedCallback() {
    this.isSuperUserFlow;
    this.getLicenseInfo();
    this.monitoringDetailsLoad();
    var today = new Date();
    var monthDigit = today.getMonth() + 1;
    if (monthDigit <= 9) {
      monthDigit = '0' + monthDigit;
    }
    var dayDigit = today.getDate();
    if (dayDigit <= 9) {
      dayDigit = '0' + dayDigit;
    }
    var date = today.getFullYear() + "-" + monthDigit + "-" + dayDigit;
    this.currentDate = date;
  }

  getLicenseInfo() {
    getApplicationId({
      providerId: this.getproviderid
    })
      .then((data) => {
        let currentData = [];
        data.forEach((row) => {
          let rowData = {};
          rowData.Id = row.Id;
          currentData.push(rowData);
        });
        this.objapplicationid = currentData;

        this.getLicensesFromApplication();
      })
      .catch((error) => {
        this.error = error.message;
      });
  }

  getLicensesFromApplication() {
    let stringapplicatio = '';
    for (let i = 0; i < this.objapplicationid.length; i++) {
      stringapplicatio += '\'' + this.objapplicationid[i].Id + '\',';
    }
    stringapplicatio = stringapplicatio.substr(0, stringapplicatio.length - 1);

    getLicensesId({
      applicationID: stringapplicatio
    })
      .then((data) => {
        for (i = 0; i < data.length; i++) {
          this.items = [...this.items, {
            value: data[i].Id,
            label: data[i].Name
          }];
        }
        let currentData = [];
        data.forEach((row) => {
          let rowData = {};
          rowData.Id = row.Id;
          rowData.Name = row.Name;
          rowData.Endate = row.EndDate__c;
          currentData.push(rowData);
        });
        this.licensesIdList = currentData;
        if (this.licensesIdList.length === 0) {
          this.dispatchEvent(utils.toastMessage("Cannot add monitoring for no license provided", "warning"));
        }
      })
      .catch((error) => {
        this.error = error.message;
      });
  }

  getValuesFromLicense() {
    getAppProviderid({
      applicationId: this.primaryLicenseId
    })
      .then(result => {
        this.Application__Name = result[0].Application__r.Name;
        this.ProgramName__c = result[0].ProgramName__c;
        this.ProgramType__c = result[0].ProgramType__c;
        this.Application__r = result[0].Application__r.Id;

        if (this.Application__r != undefined) {
          getAddressDetails({
            applicationid: this.Application__r
          }).then(result => {
            if (result.length == 0) {
              this.addressId = null;
              this.SiteAddress = null;
            } else {
              this.addressId = result[0].Id;
              this.SiteAddress = result[0].AddressLine1__c + ',' + result[0].City__c + ',' + result[0].County__c;
            }
          })
            .catch((errors) => {
              if (errors) {
                let error = JSON.parse(errors.body.message);
                const { title, message, errorType } = error;
                this.dispatchEvent(
                  utils.toastMessageWithTitle(title, message, errorType)
                );
              } else {
                this.dispatchEvent(
                  utils.toastMessage(
                    CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
                    CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
                  )
                );
              }
            })
        }

      })
  }

  monitoringDetailsLoad() {
    getMonitoringDetails({
      applicationid: this.getproviderid,
      userType: this.getUserProfileName
    })
      .then(result => {
        this.totalRecordsCount = result.length;
        if (this.totalRecordsCount == 0) {
          this.norecorddisplay = true;

        } else {
          this.norecorddisplay = false;
        }
        this.totalRecords = result.map(row => {
          let StatusClass = null;
          if (row.MonitoringStatus__c == 'Submitted') {
            StatusClass = 'Review';
          } else if (row.MonitoringStatus__c === '' || row.MonitoringStatus__c === null || row.MonitoringStatus__c === undefined) {
            StatusClass = 'Approved';
          } else {
            StatusClass = row.MonitoringStatus__c;
          }
          return {
            Id: row.Id,
            Name: row.Name,
            ProgramType__c: row.ProgramType__c,
            SiteAddress: row.SiteAddress__r ? row.SiteAddress__r.AddressLine1__c + ',' + row.SiteAddress__r.City__c : null,
            ApplicationName: row.ApplicationLicenseId__r.Name,
            ApplicationId: row.ApplicationLicenseId__r.Id,
            SelectPeriod__c: row.SelectPeriod__c,
            VisitationStartDate__c: utils.formatDate(row.VisitationStartDate__c),
            linkName: '/' + row.Id,
            Status__c: (row.MonitoringStatus__c === '' || row.MonitoringStatus__c === null || row.MonitoringStatus__c === undefined) ? 'Completed' : row.MonitoringStatus__c,
            SiteAddressId: row.SiteAddress__c,
            LicenseName: row.License__c ? row.License__r.Name : ' - ',
            LicenseId: row.License__c ? row.License__r.Id : ' - ',
            CSSClass: StatusClass,
          }

        });
        this.pageData();
        this.Spinner = false;
      })
      .catch((errors) => {
        if (errors) {
          let error = JSON.parse(errors.body.message);
          const { title, message, errorType } = error;
          this.dispatchEvent(
            utils.toastMessageWithTitle(title, message, errorType)
          );
        } else {
          this.dispatchEvent(
            utils.toastMessage(
              CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
              CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
            )
          );
        }
      })
  }

  //Function used to show the monitoring based on the pages
  pageData = () => {
    let page = this.page;
    let perpage = this.perpage;
    let startIndex = (page * perpage) - perpage;
    let endIndex = (page * perpage);
    this.currentPageMonitoringData = this.totalRecords.slice(startIndex, endIndex);
    if (this.currentPageMonitoringData.length == 0) {
      if (this.page != 1) {
        this.page = this.page - 1;
        this.pageData();
      }
    }
  }

  //Function used to get the page number from child pagination component
  hanldeProgressValueChange(event) {
    this.page = event.detail;
    this.pageData();
  }

  //get Application License Status
  @wire(getApplicationLicenseStatus, {
    applicationId: '$applicationforApiCall'
  })
  licenseStatus({
    error,
    data
  }) {
    if (data) {
      if (data.length > 0 && data[0].Status__c == "License Expired") {
        this.isLicenseExpired = true;
      } else {
        this.isLicenseExpired = false;
      }
    } else if (error) {
      let errors = error;
      if (errors) {
        let error = JSON.parse(errors.body.message);
        const { title, message, errorType } = error;
        this.dispatchEvent(
          utils.toastMessageWithTitle(title, message, errorType)
        );
      } else {
        this.dispatchEvent(
          utils.toastMessage(
            CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
            CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
          )
        );
      }
    }
  }


  //Function used to edit the existing monitoring 
  handleRowAction(event) {
    let monitoringrowid = event.detail.row.Id;
    if (event.detail.action.name == undefined) {
      sharedData.setMonitoringId(event.detail.row.Id);
      sharedData.setMonitoringAddressId(event.detail.row.SiteAddressId);
      sharedData.setMonitoringStatus(event.detail.row.MonitoringStatus__c);
      sharedData.setMonitoringStatus(event.detail.row.Status__c);
      sharedData.setLicenseId(event.detail.row.LicenseId);
      sharedData.setApplicationId(event.detail.row.ApplicationId);
      const oncaseid = new CustomEvent('redirecteditmonitering', {
        detail: {
          first: false
        }
      });
      this.dispatchEvent(oncaseid);

    } else if (event.detail.action.name == "Edit") {
      getMonitoringStatus({
        monitoringid: monitoringrowid
      }).then(result => {
        if (result[0].MonitoringStatus__c == 'Completed') {
          return this.dispatchEvent(utils.toastMessage(`Completed monitoring cant be edited.`, "warning"));
        }
        if (this.getUserProfileName === 'Supervisor') {
          this.freezeMonitoringScreen = true;
          return this.dispatchEvent(utils.toastMessage(`Cannot edit monitoring for ${this.finalStatus} application`, "warning"));
        }

        //Sundar Adding This
        if (['Submitted', 'Approved', 'Rejected'].includes(result[0].MonitoringStatus__c) || ['Completed'].includes(result[0].MonitoringStatus__c))
          return this.dispatchEvent(utils.toastMessage(`Cannot edit ${result[0].MonitoringStatus__c} monitoring`, "warning"));

        this.openmodel = true;
        this.viewmodel = false;
        this.title = event.detail.action.name == "Edit" ? "EDIT MONITORING" : this.title;
        this.btnLabel = event.detail.action.name == "Edit" ? "UPDATE" : this.btnLabel;
        this.btnDisable = false;
        this.isDisableEndDate = false;

        editMonitoringDetails({
          editMonitoringDetails: monitoringrowid
        }).then(result => {
          this.ProgramName__c = result[0].Program__c;
          this.ProgramType__c = result[0].ProgramType__c;
          this.Application__Name = result[0].ApplicationLicenseId__r.Name;
          this.SiteAddress__c = result[0].SiteAddress__c;
          this.addEditMonitoring.SelectPeriod__c = result[0].SelectPeriod__c;
          this.addEditMonitoring.VisitationStartDate__c = result[0].VisitationStartDate__c;
          this.addEditMonitoring.VisitationEndDate__c = result[0].VisitationEndDate__c;
          this.addEditMonitoring.AnnouncedUnannouced__c = result[0].AnnouncedUnannouced__c;
          this.addEditMonitoring.TimetoVisit__c = result[0].TimetoVisit__c;
          this.addEditMonitoring.JointlyInspectedWith__c = result[0].JointlyInspectedWith__c;
          this.addEditMonitoring.License__c = result[0].License__c ? result[0].License__c : ' - ';
          this.monitoringID = result[0].Id;
          this.renewalmindate = result[0].VisitationStartDate__c;
          this.renewalmaxdate = result[0].VisitationStartDate__c + 20;

        })
          .catch((errors) => {
            if (errors) {
              let error = JSON.parse(errors.body.message);
              const { title, message, errorType } = error;
              this.dispatchEvent(
                utils.toastMessageWithTitle(title, message, errorType)
              );
            } else {
              this.dispatchEvent(
                utils.toastMessage(
                  CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
                  CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
                )
              );
            }
          })
      })
        .catch((errors) => {
          if (errors) {
            let error = JSON.parse(errors.body.message);
            const { title, message, errorType } = error;
            this.dispatchEvent(
              utils.toastMessageWithTitle(title, message, errorType)
            );
          } else {
            this.dispatchEvent(
              utils.toastMessage(
                CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
                CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
              )
            );
          }
        })

    } else if (event.detail.action.name == "View") {
      this.openmodel = false;
      this.viewmodel = true;
      viewMonitoringDetails({
        viewMonitoringDetails: monitoringrowid
      }).then(result => {
        this.ProgramName__c = result[0].Program__c;
        this.ProgramType__c = result[0].ProgramType__c;
        this.Application__Name = result[0].ApplicationLicenseId__r.Name;
        this.SiteAddress__c = result[0].SiteAddress__c;
        this.addEditMonitoring.SelectPeriod__c = result[0].SelectPeriod__c;
        this.addEditMonitoring.VisitationStartDate__c = result[0].VisitationStartDate__c;
        this.addEditMonitoring.VisitationEndDate__c = result[0].VisitationEndDate__c;
        this.addEditMonitoring.AnnouncedUnannouced__c = result[0].AnnouncedUnannouced__c;
        this.addEditMonitoring.TimetoVisit__c = result[0].TimetoVisit__c;
        this.addEditMonitoring.JointlyInspectedWith__c = result[0].JointlyInspectedWith__c;
        this.addEditMonitoring.License__c = result[0].License__r.Name ? result[0].License__r.Name : ' - ';
        this.monitoringID = result[0].Id;
      })
        .catch((errors) => {
          if (errors) {
            let error = JSON.parse(errors.body.message);
            const { title, message, errorType } = error;
            this.dispatchEvent(
              utils.toastMessageWithTitle(title, message, errorType)
            );
          } else {
            this.dispatchEvent(
              utils.toastMessage(
                CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
                CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
              )
            );
          }
        })
    }
  }

  openmodal() {
    if (this.freezeMonitoringScreen)
      return this.dispatchEvent(utils.toastMessage(`Cannot add monitoring for ${this.finalStatus} application`, "warning"));

    this.btnLabel = "SAVE";
    this.addEditMonitoring = {};
    this.monitoringID = undefined;
    this.viewmodel = false;
    this.title = 'Add Monitoring';
    this.btnDisable = true;
    this.openmodel = true;
    this.isDisableEndDate = true;
    this.ProgramName__c = null;
    this.ProgramType__c = null;
    this.Application__Name = null;
    this.SiteAddress = null;

    if (this.isLicenseExpired)
      this.addEditMonitoring.SelectPeriod__c = 'Re-Licensure';
    else
      this.addEditMonitoring = {};
  }

  closeModal() {
    this.openmodel = false;
    this.viewmodel = false;
  }

  handleChange(event) {
    this.btnDisable = false;
    if (event.target.name == 'Program__c') {
      this.ProgramName__c = event.target.value;
    } else if (event.target.name == 'ProgramType__c') {
      this.ProgramType__c = event.target.value;
    } else if (event.target.name == 'SiteAddress__c') {
      this.SiteAddress__c = event.detail.value;
    } else if (event.target.name == 'SelectPeriod__c') {
      this.addEditMonitoring.SelectPeriod__c = event.detail.value;
    } else if (event.target.name == 'VisitationStartDate__c') {
      this.addEditMonitoring.VisitationStartDate__c = event.target.value;
      this.renewalmindate = event.target.value;
      this.renewalmaxdate = event.target.value + 20;
      this.isDisableEndDate = false;
    } else if (event.target.name == 'VisitationEndDate__c') {
      this.addEditMonitoring.VisitationEndDate__c = event.target.value;
    } else if (event.target.name == 'AnnouncedUnannouced__c') {
      this.addEditMonitoring.AnnouncedUnannouced__c = event.detail.value;
    } else if (event.target.name == 'TimetoVisit__c') {
      this.addEditMonitoring.TimetoVisit__c = event.detail.value;
    } else if (event.target.name == 'JointlyInspectedWith__c') {
      this.addEditMonitoring.JointlyInspectedWith__c = event.detail.value;
    } else if (event.target.name == 'LicenceName') {
      this.addEditMonitoring.License__c = event.detail.value;
      this.primaryLicenseId = this.addEditMonitoring.License__c;
      this.getValuesFromLicense();
    }
  }

  handleSave() {
    if (this.addEditMonitoring && !this.addEditMonitoring.License__c)
      return this.dispatchEvent(utils.toastMessage("License Number is mandatory", "warning"));

    if (!this.ProgramName__c)
      return this.dispatchEvent(utils.toastMessage("Program is mandatory", "warning"));

    if (this.addEditMonitoring && !this.addEditMonitoring.SelectPeriod__c)
      return this.dispatchEvent(utils.toastMessage("Select Frequency is mandatory", "warning"));

    if (!this.ProgramType__c)
      return this.dispatchEvent(utils.toastMessage("Program Type is mandatory", "warning"));

    if (!this.SiteAddress)
      return this.dispatchEvent(utils.toastMessage("Site Address is mandatory", "warning"));

    if (!this.Application__Name)
      return this.dispatchEvent(utils.toastMessage("Application Id is mandatory", "warning"));

    if (this.addEditMonitoring && !this.addEditMonitoring.VisitationStartDate__c)
      return this.dispatchEvent(utils.toastMessage("Visitation Start Date is mandatory", "warning"));

    if (this.addEditMonitoring && this.addEditMonitoring.VisitationEndDate__c && (new Date(this.addEditMonitoring.VisitationStartDate__c).getTime() > new Date(this.addEditMonitoring.VisitationEndDate__c).getTime()))
      return this.dispatchEvent(utils.toastMessage("Visitation Start Date should be greater than Visitation End Date", "warning"));

    this.btnDisable = true;

    this.addEditMonitoring.sobjectType = 'Monitoring__c';
    this.addEditMonitoring.SiteAddress__c = this.addressId;
    this.addEditMonitoring.License__c = this.primaryLicenseId;
    this.addEditMonitoring.ProgramName__c = this.ProgramName__c;
    this.addEditMonitoring.ProgramType__c = this.ProgramType__c;
    this.addEditMonitoring.MonitoringStatus__c = 'Draft';
    this.addEditMonitoring.ApplicationLicenseId__c = this.Application__r;
    if (this.monitoringID) {
      this.addEditMonitoring.Id = this.monitoringID;
      this.isUpdateFlag = true;
    } else {
      this.addEditMonitoring.MonitoringStatus__c = 'Draft';
      this.isUpdateFlag = false;
    }
    InsertUpdateMonitoring({
      objSobjecttoUpdateOrInsert: this.addEditMonitoring
    })
      .then(() => {
        if (this.isUpdateFlag)
          this.dispatchEvent(utils.toastMessage("Monitoring has been updated successfully", "success"));
        else
          this.dispatchEvent(utils.toastMessage("Monitoring has been added successfully", "success"));

        this.addEditMonitoring = {};
        this.btnDisable = false;
        this.closeModal();
        this.monitoringDetailsLoad();
      })
      .catch((errors) => {
        this.btnDisable = false;
        if (errors) {
          let error = JSON.parse(errors.body.message);
          const { title, message, errorType } = error;
          this.dispatchEvent(
            utils.toastMessageWithTitle(title, message, errorType)
          );
        } else {
          this.dispatchEvent(
            utils.toastMessage(
              CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
              CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
            )
          );
        }
      })

  }

}