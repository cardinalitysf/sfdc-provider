/**
 * @Author        : Sindhu Venkateswarlu
 * @CreatedOn     : APRIL 6 ,2020
 * @Purpose       : Responsible for fetch or upsert the Narrative Details.
 * @UpdatedBy : Sundar K(Whole Functionality updated)
 * @UpdatedOn : Apr 16, 2020
 **/

import {
    LightningElement,
    track,
    wire,
    api
} from 'lwc';

import {
    utils
} from "c/referralUtils";
import * as sharedData from "c/sharedData";
import getNarrativeDetails from '@salesforce/apex/applicationproviderNarrative.getNarrativeDetails';
import updateNarrativeDetails from '@salesforce/apex/applicationproviderNarrative.updateNarrativeDetails';
import getApplicationProviderStatus from "@salesforce/apex/applicationproviderNarrative.getApplicationProviderStatus";
import {
    CJAMS_CONSTANTS
} from 'c/constants';

export default class ApplicationproviderNarrative extends LightningElement {
    @track Narrative__c;
    @track applicationForApiCall = this.ApplicationId;
    @track richTxtDisable = false;
    @track saveBtnDisable = false;

    //Get Application Id
    @api
    get ApplicationId() {
        return sharedData.getApplicationId();
    }

    //Get Provider Id
    @api
    get ProviderId() {
        return sharedData.getProviderId();
    }

    //Connected Call Back method
    connectedCallback() {
        getNarrativeDetails({
                applicationId: this.ApplicationId
            })
            .then(result => {
                this.Narrative__c = result[0].Narrative__c;
            })
    }

    //Get Provider Application Status
    @wire(getApplicationProviderStatus, {
        applicationId: '$applicationForApiCall'
    })
    wiredApplicationDetails({
        error,
        data
    }) {
        if (data) {
            if (data.length > 0) {
                if (data[0].Status__c == CJAMS_CONSTANTS.PROVIDER_APP_FINAL_STATUS_REVIEW || data[0].Status__c == CJAMS_CONSTANTS.PROVIDER_APP_FINAL_STATUS_APPEAL || data[0].Status__c == CJAMS_CONSTANTS.APPLICATION_FINAL_STATUS_REVIEW || data[0].Status__c == CJAMS_CONSTANTS.PROVIDER_APP_FINAL_STATUS_REJECTED || data[0].Status__c == CJAMS_CONSTANTS.APPLICATION_FINAL_STATUS_APPROVED || data[0].Status__c == CJAMS_CONSTANTS.APPLICATION_FINAL_STATUS_SUPERVISOR_REJECTED) {
                    this.richTxtDisable = true;
                    this.saveBtnDisable = true;
                } else {
                    this.richTxtDisable = false;
                    this.saveBtnDisable = false;
                }
            }
        }
    }

    //Narrative On Change functionality
    narrativeOnChange(event) {
        this.Narrative__c = event.target.value;
    }

    //Speech To Text Functionality
    handleSpeechToText(event) {
        this.Narrative__c = event.detail.speechValue;
    }

    //Save Method for Narrative
    saveNarrativeDetails() {
        this.template.querySelector('c-common-speech-to-text').stopSpeech();
        let myObj = {
            'sobjectType': 'Application__c'
        };

        myObj.Narrative__c = this.Narrative__c;

        if (this.ApplicationId)
            myObj.Id = this.ApplicationId;

        updateNarrativeDetails({
                objSobjecttoUpdateOrInsert: myObj
            })
            .then(result => {
                this.Narrative__c = null;

                this.dispatchEvent(utils.toastMessage("Provider Narrative details saved successfully", "success"));
            })
            .catch(error => {
                this.dispatchEvent(utils.toastMessage("Error in saving record", "error"));
            });
    }
}