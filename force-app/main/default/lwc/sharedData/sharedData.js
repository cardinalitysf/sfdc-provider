/**
 * @Author        : V.S.Marimuthu
 * @CreatedOn     : Feb 26 ,2020
 * @Purpose       : All the variables that are needed to be shared have to get;set; through this class
 **/

let providerDatas = {};

// #region Provider Id
const setProviderId = (providerId) => {
    providerDatas.providerId = providerId;
};
const getProviderId = () => providerDatas.providerId;
// #endregion


// #region Case Id
const setCaseId = (caseId) => {
    providerDatas.caseId = caseId;
};
const getCaseId = () => providerDatas.caseId;
// #endregion


// #region Case Id
const setCommunicationValue = (getCommunucation) => {
    providerDatas.getCommunucation = getCommunucation;
};
const getCommunicationValue = () => providerDatas.getCommunucation;
// #endregion


// #region Case Id
const setReceivedDateValue = (getReceivedDate) => {
    providerDatas.getReceivedDate = getReceivedDate;
};
const getReceivedDateValue = () => providerDatas.getReceivedDate;
// #endregion

// #region Case Id
const setButtonDisableforDecisionTab = (blnButtonValue) => {
    providerDatas.blnButtonValue = blnButtonValue;
};
const getButtonDisableforDecisionTab = () => providerDatas.blnButtonValue;
// #endregion

// #region Application Id
const setApplicationId = (applicationId) => {
    providerDatas.applicationId = applicationId;
};
const getApplicationId = () => providerDatas.applicationId;
// #endregion

// #region Staff Id
const setStaffId = (staffId) => {
    providerDatas.staffId = staffId;
};
const getStaffId = () => providerDatas.staffId;
// #endregion

// #Begin Monitoring Id
const setMonitoringId = (monitoringId) => {
    providerDatas.monitoringId = monitoringId;
};
const getMonitoringId = () => providerDatas.monitoringId;
// #end Monitoring Id

// #Begin Monitoring Status
const setMonitoringStatus = (monitoringStatus) => {
    providerDatas.monitoringStatus = monitoringStatus;
};
const getMonitoringStatus = () => providerDatas.monitoringStatus;
// #end Monitoring Status

// set and get referral id start
const setPublicReferralData = (referralNumber, status) => {
    
    providerDatas.publicReferralRowData = `${referralNumber}, ${status}`
}
const getPublicReferralData = () => providerDatas.publicReferralRowData

// set and get referral id end

// #Begin Monitoring Address Id
const setMonitoringAddressId = (monitoringAddressId) => {
    providerDatas.monitoringAddressId = monitoringAddressId;
};
const getMonitoringAddressId = () => providerDatas.monitoringAddressId;
// #end Monitoring Address Id

// #region Contract Id
const setContractId = (contractId) => {
    providerDatas.contractId = contractId;
};
const getContractId = () => providerDatas.contractId;
// #endregion

// #region LicenseId Id
const setLicenseId = (licenseId) => {
    providerDatas.licenseId = licenseId;
};
const getLicenseId = () => providerDatas.licenseId;
// #endregion

// #region ContractAction
const setContractAction = (contractAction) => {
    providerDatas.contractAction = contractAction;

};
const getContractAction = () => providerDatas.contractAction;
// #endregion

// #Begin User Profile Name
const setUserProfileName = (profileName) => {
    providerDatas.profileName = profileName;
};
const getUserProfileName = () => providerDatas.profileName;
// #end User Profile Name

//HouseHoldContactId from PublicProviderHouseHoldMemberHome
const setHouseHoldContactId = (HouseHoldContactId) => {
    providerDatas.HouseHoldContactId = HouseHoldContactId;
};
const getHouseHoldContactId = () => providerDatas.HouseHoldContactId;
// #end HouseHoldContactId

// #Begin Judisdiction 
const setJurisdiction = (jurisdiction) => {
    providerDatas.jurisdiction = jurisdiction;
};
const getJurisdiction = () => providerDatas.jurisdiction;
// #end Judisdiction 


// #Begin PublicTrainingSessionNumber 
const setPublicTrainingrowData = (sessionNumber, id, status) => {
    providerDatas.publicTrainingrowData = `${sessionNumber}, ${id}, ${status}`
};
const getPublicTrainingrowData = () => providerDatas.publicTrainingrowData;
// #end PublicTrainingSessionNumber 

// #Application Status Set / Get Start
const setApplicationStatus = (status) => {
    providerDatas.applicationStatus = status;
};
const getApplicationStatus = () => providerDatas.applicationStatus;
// #Application Status Set / Get End

// set reconsideration id start 
const setReconsiderationId = (id) => {
    providerDatas.reconsiderationId = id;
}

const getReconsiderationId = () => providerDatas.reconsiderationId
// set reconsideration id end

// set reconsideration status start 
const setReconsiderationStatus = (status) => {
    
    providerDatas.reconsiderationStatus = status;
}

const getReconsiderationStatus = () => providerDatas.reconsiderationStatus
// set reconsideration status end

const setPublicReferralStatus = (status) => {
    providerDatas.redirectStatusReferral = status;
}

const getPublicReferralStatus = () => providerDatas.redirectStatusReferral

const setComplaintsStatus = (status) => {
    providerDatas.ComplaintsStatus = status;
}

const getComplaintsStatus = () => providerDatas.ComplaintsStatus;

const setPrivateOrPublicType = (status) => {
    providerDatas.PrivateOrPublicType = status;
}

const getPrivateOrPublicType = () => providerDatas.PrivateOrPublicType;

const setOwnerIdCommunityUser = (ownerId) => {
    providerDatas.UserOwnerId = ownerId;
}

const getOwnerIdCommunityUser = () => providerDatas.UserOwnerId;

const setPersonKidIncidentId = (recordId) => {
    providerDatas.RowEditId = recordId;
}

const getPersonKidIncidentId = () => providerDatas.RowEditId;

const setContactId = (contactId) => {
    providerDatas.RowEditContactId = contactId;
}

const getContactId = () => providerDatas.RowEditContactId;

const setAddressId = (addressId) => {
    providerDatas.RowEditAddressId = addressId;
}

const getAddressId = () => providerDatas.RowEditAddressId;

const setActionPersonKid = (actionName) => {
    providerDatas.ActionPersonKid = actionName;
}

const getActionPersonKid = () => providerDatas.ActionPersonKid;

// set Incident status starts 
const setIncidentStatus = (status) => {
    providerDatas.IncidentStatus = status;
}

const getIncidentStatus = () => providerDatas.IncidentStatus;

// set Incident status ends

// #region CPAHome Id
const setCPAHomeId = (cpaHomeId) => {
    providerDatas.cpaHomeId = cpaHomeId;
};
const getCPAHomeId = () => providerDatas.cpaHomeId;
// #endregion


// #Case Worker Id
const setCaseworkerId = (caseWorkerId) => {
    providerDatas.caseWorkerId = caseWorkerId;
};
const getCaseworkerId = () => providerDatas.caseWorkerId;
// #end Case Worker

export {
    getProviderId,
    setProviderId,
    getCaseId,
    setCaseId,
    setCommunicationValue,
    getCommunicationValue,
    setReceivedDateValue,
    getReceivedDateValue,
    setButtonDisableforDecisionTab,
    getButtonDisableforDecisionTab,
    setApplicationId,
    getApplicationId,
    setStaffId,
    getStaffId,
    setMonitoringId,
    getMonitoringId,
    setMonitoringStatus,
    getMonitoringStatus,
    setMonitoringAddressId,
    getMonitoringAddressId,
    setContractId,
    getContractId,
    setLicenseId,
    getLicenseId,
    setContractAction,
    getContractAction,
    setUserProfileName,
    getUserProfileName,
    setHouseHoldContactId,
    getHouseHoldContactId,
    setJurisdiction,
    getJurisdiction,
    setPublicReferralData,
    getPublicReferralData,
    getPublicTrainingrowData,
    setPublicTrainingrowData,
    setApplicationStatus,
    getApplicationStatus,
    setReconsiderationId,
    getReconsiderationId,
    setReconsiderationStatus,
    getReconsiderationStatus,
    setPublicReferralStatus,
    getPublicReferralStatus,
    setComplaintsStatus,
    getComplaintsStatus,
    setPrivateOrPublicType,
    getPrivateOrPublicType,
    setOwnerIdCommunityUser,
    getOwnerIdCommunityUser,
    setPersonKidIncidentId,
    getPersonKidIncidentId,
    setContactId,
    getContactId,
    setAddressId,
    getAddressId,
    setActionPersonKid,
    getActionPersonKid,
    setCPAHomeId,
    getCPAHomeId,
    setIncidentStatus,
    getIncidentStatus,
    setCaseworkerId,
    getCaseworkerId
};