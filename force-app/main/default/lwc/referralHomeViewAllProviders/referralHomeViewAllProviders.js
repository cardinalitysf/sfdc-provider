/**
 * @Author        : Preethi Bojarajan
 * @CreatedOn     : Feb 25,2020
 * @Purpose       : Referral Dashboard with Pending, Approved and Rejected
 * @updatedBy     : Preethi Bojarajan
 * @updatedOn     : May 01,2020
 **/
import {
  LightningElement,
  track,
  wire,
  api
} from "lwc";

//Import Sort
import {
  refreshApex
} from "@salesforce/apex";

//Import Apex
import getReferalCount from "@salesforce/apex/referalTable.getReferalCount";
import getReferalList from "@salesforce/apex/referalTable.getReferalList";

//Utils
import * as sharedData from "c/sharedData";

const cardDatas = [{
    Id: "1",
    title: "Pending",
    key: "draft",
    count: 0
  },
  {
    Id: "2",
    title: "Approved",
    key: "approved",
    count: 0
  },
  {
    Id: "3",
    title: "Rejected",
    key: "rejected",
    count: 0
  },
  {
    Id: "4",
    title: "Total Applications",
    key: "total",
    count: 0
  }
];

const columns = [{
    label: "Referral Number",
    fieldName: "id",
    sortable: true,
    type: "button",
    typeAttributes: {
      label: {
        fieldName: "CaseNumber"
      },
      class: "blue",
      target: "_self",
      color: "blue"
    }
  },
  {
    label: "Referral Type",
    fieldName: "ProviderType",
    sortable: true,
    cellAttributes: {
      class: "fontclrGrey"
    }
  },
  {
    label: "License Type",
    fieldName: "Program",
    sortable: true,
    typeAttributes: {
      label: {
        fieldName: "Program"
      }
    },
    cellAttributes: {
      class: "fontclrGrey"
    }
  },
  {
    label: "Name",
    fieldName: "Name",
    sortable: true,
    cellAttributes: {
      class: "fontclrGrey"
    }
  },
  {
    label: "STATEMENT OF NEED",
    fieldName: "SON",
    cellAttributes: {
      class: "fontclrGrey"
    }
  },
  {
    label: "REQUEST FOR PROPOSAL",
    fieldName: "RFP",
    cellAttributes: {
      class: "fontclrGrey"
    }
  },
  {
    label: "Status",
    fieldName: "Status",
    type: "text",
    cellAttributes: {
      class: {
        fieldName: "statusClass"
      }
    }
  }
];

export default class ReferralHomeViewAllProviders extends LightningElement {
  @track cardDatas = cardDatas;
  @track columns = columns;
  @track referalCount = [];
  @track totalRecords = [];
  @track activePage = 0;
  @track pageLoader = false;
  @track openModelwindow = false;
  //Search Function Start
  @api searchKey = "";

  // for sprint - 5 dashboard
  @track referralName = true;

  get cardClasses() {
    return [{
        title: "pending",
        card: "pending-cls",
        init: true
      },
      {
        title: "approved",
        card: "approved-cls",
        init: false
      },
      {
        title: "rejected",
        card: "rejected-cls",
        init: false
      },
      {
        title: "total",
        card: "total-cls",
        init: false
      }
    ];
  }

  //Common Data Formating Function Start
  formatData(data) {
    let currentData = [];
    data.forEach((row) => {
      let rowData = {};
      rowData.CaseNumber = row.CaseNumber;
      rowData.Program = row.Program__c;
      rowData.Status = row.Status;
      rowData.id = row.Id;
      if (row.Account) {
        rowData.ProviderType = row.Account.ProviderType__c;
        rowData.Name = row.Account.Name;
        rowData.SON = row.Account.SON__c;
        rowData.RFP = row.Account.RFP__c;
        rowData.statusClass = row.Status;
      }
      currentData.push(rowData);
    });
    return currentData;
  }
  // Common Data Formating Function End

  @wire(getReferalCount, {
    searchKey: "$searchKey"
  })
  wiredReferalResult(result) {
    this.referalCount = result;
    if (result.data) {
      let init = 0;
      let search = null;
      let datas = result.data[0];
      for (let key of this.cardDatas) {
        key.count = datas[key.key];
      }
      this.getDataTableDatas(init, search);
    }
  }

  getDataTableDatas(id, search) {
    this.activePage = id;
    getReferalList({
      searchKey: search,
      type: this.cardDatas[id].key
    }).then((data) => {
      let currentData = this.formatData(data);
      this.totalRecords = currentData;
      this.pageLoader = true;
    });
  }

  handleDataTableDatas(event) {
    let search = null;
    this.getDataTableDatas(event.detail, search);
  }

  handleChange(event) {
    this.searchKey = event.target.value;
    this.getDataTableDatas(this.activePage, this.searchKey);
    this.template.querySelector("c-dashboard-data-table").searchFn();
  }

  // Redirection Function
  handleRedirection(event) {
    sharedData.setCaseId(event.detail.redirectId);
    const onClickId = new CustomEvent("redirectfromdashboard", {
      bubbles: true,
      composed: true,
      detail: {
        first: false,
        second: false,
        caseid: event.detail.redirectId,
        pagetype: "case"
      }
    });
    this.dispatchEvent(onClickId);
  }

  createNewReferral() {
    // sharedData.setProviderId(undefined);
    // const onClickId = new CustomEvent("redirectcaseid", {
    //   detail: {
    //     first: false,
    //     second: true,
    //     caseid: undefined
    //   }
    // });
    this.dispatchEvent(onClickId);
    if (this.referralName === true) {
      sharedData.setProviderId(undefined);
      const onClickId = new CustomEvent("redirectcaseid", {
        detail: {
          first: false,
          second: true,
          caseid: undefined
        }
      });
      this.dispatchEvent(onClickId);
    } else {
      sharedData.setProviderId(undefined);
      const onClickId = new CustomEvent("redirectcaseid", {
        detail: {
          first: false,
          second: true,
          caseid: undefined
        }
      });
      this.dispatchEvent(onClickId);
    }

  }
  createNewReferralByButtonClick() {
    sharedData.setCaseId(undefined)
    this.openModelwindow = true;
  }
  intimateParentToCloseModal() {
    this.openModelwindow = false;
    this.referralName = false;
  }
  handleReferral = (event) => {
    this.referralName = event.detail.name === "Private";
  };

  handlerRedirect = (event) => {
    event.detail === true ? this.createNewReferral() : null;
  };
}