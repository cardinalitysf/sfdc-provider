import { LightningElement ,track} from 'lwc';

export default class CustomizeCombobox extends LightningElement {
    value = 'inProgress';
    @track state = {
        progress: this.value,
        progressRequired: '',
        progressDisabled: ''
    };

    get options2() {
        return [
            { label: 'New', value: 'new' },
            { label: 'In Progress', value: 'inProgress'  },
            { label: 'Finished', value: 'finished'}
        ];
    }
    get options() {
        return [
            { label: 'Marimuthu', value: 'Marimuthu' ,status:'Pending' },
            { label: 'Thomas William', value: 'Thomas William' ,status:'Completed' },
            { label: 'Handry Archer', value: 'Handry Archer' ,status:'pending'},
            { label: 'Tom Hanks', value: 'Tom Hanks' ,status:'pending'},
            { label: 'Murugan is a bad boy', value: 'Murugan is a bad boy' ,status:'pending'}
        
        ];
    }
    handleChange(event) {
        this.state[event.target.name] = event.detail.value;
    }
}
