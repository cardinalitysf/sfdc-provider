/**
 * @Author        : V.S.Marimuthu
 * @CreatedOn     : March 22 ,2020
 * @Purpose       : COntains all the constant values used in application
 * @UpdatedBy	  : Sundar K -> July 23, 2020 -> added BUTTON_LABELS, DATATABLE_BUTTON_LABELS, TOAST_HEADER_LABELS methods
 **/
export const CJAMS_CONSTANTS = {
  FRIENDLY_ERROR_MESSAGE:
    "There was an error while processing the request. Please try again later.",
  APPLICATION_ERROR_MESSAGE: "Error in saving Application",
  SELECTED_HISTORY_MESSAGE: "Error in selected History",
  DOCUMENT_ERROR_MESSAGE: "Error in saving record",
  DELETE_ERROR_MESSAGE: "Error in delete record",
  PROVIDER_APP_REVIEW_STATUS_PENDING: "None",
  PROVIDER_APP_REVIEW_STATUS_FOR_REVIEW: "Submit for Review",
  PROVIDER_APP_REVIEW_STATUS_APPEAL: "Appeal",
  APPLICATION_REVIEW_STATUS_APPROVED: "Approved",
  APPLICATION_REVIEW_STATUS_REJECTED: "Rejected",
  PROVIDER_APP_FINAL_STATUS_PENDING: "Pending",
  PROVIDER_APP_FINAL_STATUS_REVIEW: "For Review",
  PROVIDER_APP_FINAL_STATUS_NEW: "New",
  APPLICATION_SUBMITTED_TO_SUPERVISOR: "Submitted to Supervisor",
  PROVIDER_APP_FINAL_STATUS_REJECTED: "Rejected",
  APPLICATION_FINAL_STATUS_SUPERVISOR_REJECTED: "Supervisor Rejected",
  PROVIDER_APP_FINAL_STATUS_APPEAL: "Appeal",
  APPLICATION_FINAL_STATUS_REVIEW: "Caseworker Submitted",
  APPLICATION_FINAL_STATUS_APPROVED: "Approved",
  CONTRACT_STATUS_INPROCESS: "In process",
  CONTRACT_STATUS_SUBMITTED: "Submitted",
  CONTRACT_STATUS_ACTIVE: "Active",
  CONTRACT_STATUS_INACTIVE: "Inactive",
  CONTRACT_STATUS_EXPIRED: "Expired",
  CONTRACT_STATUS_RETURNED: "Returned",
  REVIEW_STATUS_NONE: "None",
  REVIEW_STATUS_SUBMITFORREVIEW: "Submit for Review",
  REVIEW_STATUS_APPROVED: "Approve",
  REVIEW_STATUS_REJECT: "Reject",
  REVIEW_STATUS_REWORK: "Return to Caseworker",
  NO_RECORDS_FOUND: "No records found for the selected criteria",
  ASSIGNED_FOR_TRAINING: "Assigned for Training",
  REVIEW_TRAINING_UNATTENDED: "Training Unattended",
  REVIEW_TRAINING_ATTENDED: "Training Completed",
  REVIEW_STATUS_DRAFT: "Draft",
  REVIEW_STATUS_FOR_TRAINING: "Submit for Training",
  REVIEW_STATUS_ROLE: "Applicant",
  CASE_OBJECT_NAME: "Case",
  CASE_LOOKUP_OBJECT: "Case__c",
  APPLICATION_OBJECT_NAME: "Application__c",
  NARRATIVE_STATUS: [
    "Caseworker Submitted",
    "Approved",
    "Rejected",
    "Submitted",
    "Supervisor Rejected",
    "Submitted to Supervisor",
    "Accepted",
    "Disputed",
    "Awaiting Confirmation"
  ],
  PROVIDER_OBJECT_NAME: "Account",
  MONITORING_OBJECT_NAME: "Monitoring__c",
  PRIVATE_RECORD_TYPE_NAME: "Private",
  RESOURCEHOME_PROGRAM: "Resource Home",
  KINSHIP_PROGRAM: "Kinship",
  RECONSIDERATION_OBJECT_NAME: "Reconsideration__c",
  CASE_RECORDTYPE_COMPLAINTS: "Complaints",
  ERROR_TOASTMESSAGE: "error",
  ERROR_EXCEPTION: "error occurred,Please try again",
  COMPLAINTS_PRIVATE_DATAEXCEPTION: "Sorry,Failed to load PrivateComplaints list details ,Please Try Again..!",
  COMPLAINTS_PUBLIC_DATAEXCEPTION: "Sorry,Failed to load PublicComplaints list details ,Please Try Again..!",
  COMPLAINT_STATUS_SUBMITTOCW: "Submitted to Caseworker",
  COMPLAINT_STATUS_SUBMITTOPROVIDER: "Awaiting Confirmation",
  COMPLAINT_STATUS_SUBMITFORAKM: "Submit for Acknowledgement",
  COMPLAINT_DROPDOWN_REMOVED: "Removed",
  ERROR_GOOGLE_API_STREET: "Error in Fetching Google API Street Address",
  ERROR_GOOGLE_API_SELECT_ADDR: "Error in Selected Address",
  ERROR_IN_FETCHING_IMAGE: "Error in Fetcching Images"
};

export const constPopupVariables = {
  SUCCESS: "SUCCESS",
  ERROR: "ERROR",
  INSERTSUCCESS: "SUCCESSFULLY INSERTED",
  UPDATESUCCESS: "SUCCESSFULLY UPDATED",
  DELETESUCCESS: "SUCCESSFULLY DELETED",
  APPROVED: "APPROVED",
  officeTab: "Office Inspection Added Successfully",
  plantTab: "Physical Plant Site Added Successfully",
  staffTab: "Staff Record Added Successfully",
  officeTabUpdate: "Office Inspection Updated Successfully",
  plantTabUpdate: "Physical Plant Site Updated Successfully",
  staffTabUpdate: "Staff Record Updated Successfully",
  savingErrorRec: "Error In Saving Record, Please Try Again..!",
  comarSave: "Comar Has Been Saved Successfully..!",
  UNEDIT: "You Can't Edit Sumbitted / Approved Records..!",
  UNDELETE: "You Can't Delete Sumbitted / Approved Records..!",
  UNAUTHOREDEL: "You don't have Access to Delete..!",
  STAFF_HAV_REF_DEL: "You can't delete the Staff having Refferal",
  VALIDATION_WARN: "Please fill the Mandatory fields..!",
  HOUSEHOLD_SUCCESS: "House Hold Member Added Successfully..!",
  HOUSEHOLD_UPDATE: "House Hold Member Updated Successfully..!",
  HOUSEHOLD_APPLICANT: "There Should be Only One Applicant for the Referral..!",
  HOUSEHOLD_COAPPLICANT: "There Should be Only One Co-Applicant for the Referral..!",
  REFCHECK_SUCCESS: "Reference Member Added Successfully..!",
  REFCHECK_UPDATE: "Reference Member Updated Successfully..!",
  BASIC_INFO_FILL: "Please fill Basic Information Details..!",
  COMPLAINT_REF_SUCCESS: "Complaint Referral Added Sucessfully..!",
  COMPLAINT_REF_UPDATE: "Complaint Referral Updated Sucessfully..!",
  PROVIDER_DET_INCORRECT: "No Provider Available. Please Check with Provider Details..!",
  STAFF_DETAIL_FILL: 'Please fill Any of the Details to Save..!',
  STAFF_DETAIL_SAVE_SUCCESS: 'Staff Details Saved Successfully..!',
  STAFF_DETAIL_UPDATE_SUCESS: 'Staff Details Updated Successfully..!',
};

//Function used to set hardcoded county when Modal Opens
export const CJAMS_CONSTANTS_COUNTYARR = [
  {
    label: "Adams",
    value: "Adams"
  },
  {
    label: "Allen",
    value: "Allen"
  },
  {
    label: "Bartholomew",
    value: "Bartholomew"
  },
  {
    label: "Benton",
    value: "Benton"
  },
  {
    label: "Blackford",
    value: "Blackford"
  },
  {
    label: "Boone",
    value: "Boone"
  },
  {
    label: "Brown",
    value: "Brown"
  },
  {
    label: "Carroll",
    value: "Carroll"
  },
  {
    label: "Cass",
    value: "Cass"
  },
  {
    label: "Clark",
    value: "Clark"
  },
  {
    label: "Clay",
    value: "Clay"
  },
  {
    label: "Clinton",
    value: "Clinton"
  },
  {
    label: "Crawford",
    value: "Crawford"
  },
  {
    label: "Daviess",
    value: "Daviess"
  },
  {
    label: "Dearborn",
    value: "Dearborn"
  },
  {
    label: "Decatur",
    value: "Decatur"
  },
  {
    label: "DeKalb",
    value: "DeKalb"
  },
  {
    label: "Delaware",
    value: "Delaware"
  },
  {
    label: "Dubois",
    value: "Dubois"
  },
  {
    label: "Elkhart",
    value: "Elkhart"
  },
  {
    label: "Fayette",
    value: "Fayette"
  },
  {
    label: "Floyd",
    value: "Floyd"
  },
  {
    label: "Fountain",
    value: "Fountain"
  },
  {
    label: "Franklin",
    value: "Franklin"
  },
  {
    label: "Fulton",
    value: "Fulton"
  },
  {
    label: "Gibson",
    value: "Gibson"
  },
  {
    label: "Grant",
    value: "Grant"
  },
  {
    label: "Greene",
    value: "Greene"
  },
  {
    label: "Hamilton",
    value: "Hamilton"
  },
  {
    label: "Hancock",
    value: "Hancock"
  },
  {
    label: "Harrison",
    value: "Harrison"
  },
  {
    label: "Hendricks",
    value: "Hendricks"
  },
  {
    label: "Henry",
    value: "Henry"
  },
  {
    label: "Howard",
    value: "Howard"
  },
  {
    label: "Huntington",
    value: "Huntington"
  },
  {
    label: "Jackson",
    value: "Jackson"
  },
  {
    label: "Jasper",
    value: "Jasper"
  },
  {
    label: "Jay",
    value: "Jay"
  },
  {
    label: "Jefferson",
    value: "Jefferson"
  },
  {
    label: "Jennings",
    value: "Jennings"
  },
  {
    label: "Johnson",
    value: "Johnson"
  },
  {
    label: "Knox",
    value: "Knox"
  },
  {
    label: "Kosciusko",
    value: "Kosciusko"
  },
  {
    label: "LaGrange",
    value: "LaGrange"
  },
  {
    label: "Lake",
    value: "Lake"
  },
  {
    label: "LaPorte",
    value: "LaPorte"
  },
  {
    label: "Lawrence",
    value: "Lawrence"
  },
  {
    label: "Madison",
    value: "Madison"
  },
  {
    label: "Marion",
    value: "Marion"
  },
  {
    label: "Marshall",
    value: "Marshall"
  },
  {
    label: "Martin",
    value: "Martin"
  },
  {
    label: "Miami",
    value: "Miami"
  },
  {
    label: "Monroe",
    value: "Monroe"
  },
  {
    label: "Montgomery",
    value: "Montgomery"
  },
  {
    label: "Morgan",
    value: "Morgan"
  },
  {
    label: "Newton",
    value: "Newton"
  },
  {
    label: "Noble",
    value: "Noble"
  },
  {
    label: "Ohio",
    value: "Ohio"
  },
  {
    label: "Orange",
    value: "Orange"
  },
  {
    label: "Owen",
    value: "Owen"
  },
  {
    label: "Parke",
    value: "Parke"
  },
  {
    label: "Perry",
    value: "Perry"
  },
  {
    label: "Pike",
    value: "Pike"
  },
  {
    label: "Porter",
    value: "Porter"
  },
  {
    label: "Posey",
    value: "Posey"
  },
  {
    label: "Pulaski",
    value: "Pulaski"
  },
  {
    label: "Putnam",
    value: "Putnam"
  },
  {
    label: "Randolph",
    value: "Randolph"
  },
  {
    label: "Ripley",
    value: "Ripley"
  },
  {
    label: "Rush",
    value: "Rush"
  },
  {
    label: "St. Joseph",
    value: "St. Joseph"
  },
  {
    label: "Scott",
    value: "Scott"
  },
  {
    label: "Shelby",
    value: "Shelby"
  },
  {
    label: "Spencer",
    value: "Spencer"
  },
  {
    label: "Starke",
    value: "Starke"
  },
  {
    label: "Steuben",
    value: "Steuben"
  },
  {
    label: "Sullivan",
    value: "Sullivan"
  },
  {
    label: "Switzerland",
    value: "Switzerland"
  },
  {
    label: "Tippecanoe County",
    value: "Tippecanoe County"
  },
  {
    label: "Tipton",
    value: "Tipton"
  },
  {
    label: "Union",
    value: "Union"
  },
  {
    label: "Vanderburgh",
    value: "Vanderburgh"
  },
  {
    label: "Vermillion",
    value: "Vermillion"
  },
  {
    label: "Vigo",
    value: "Vigo"
  },
  {
    label: "Wabash",
    value: "Wabash"
  },
  {
    label: "Warren",
    value: "Warren"
  },
  {
    label: "Warrick",
    value: "Warrick"
  },
  {
    label: "Washington",
    value: "Washington"
  },
  {
    label: "Wayne",
    value: "Wayne"
  },
  {
    label: "Wells",
    value: "Wells"
  },
  {
    label: "White",
    value: "White"
  },
  {
    label: "Whitley",
    value: "Whitley"
  }
];

export const CJAMS_OBJECT_QUERIES = {
  SELECT_FIELDS_FOR_PUBLIC_REFERRAL_ATTACHMENT:
    "Id,Status,CaseNumber,ReceivedDate__c,CreatedDate,Owner.FirstName,Owner.Lastname,Origin",
  FETCH_FIELDS_FOR_PUBLIC_HOUSEHOLD_PICKLIST:
    "Title__c,PrimaryCitizenship__c,PrimaryLanguage__c,Nationality__c,Gender__c,Religion__c,SkinTone__c,LivingSituation__c,HairColor__c,PersonRole__c,Role__c,Dangertoself__c,Appearanceofmentallyimpaired__c,Dangertoworker__c,Signsofmentalillness__c,DeclaredDisable__c,LicensedChildCareProvider__c,ApplyingforChildCareProvider__c,LicensePreviouslyDenied__c,Caringforaged__c",
  SELECT_FIELDS_FOR_PUBLIC_APPLICATION_ATTACHMENT:
    "id,CreatedDate,Provider__c,Name,Status__c",
  FETCH_FIELDS_FOR_PUBLIC_REF_CHECK_PICKLIST:
    "TypeofContact__c,RelationshiptoApplicant__c,SchoolReferenceCheck__c,ReferenceRecommends__c,Relation__c",
  SELECT_FIELDS_FOR_PRIVATE_PROVIDER_ATTACHMENT:
    "Id,Name,Status__c,ProviderId__c,CreatedDate,Owner.FirstName,Owner.Lastname",
  SELECT_FIELDS_FOR_PRIVATE_MONITORING_ATTACHMENT:
    "Id, Name,ApplicationLicenseId__c, AnnouncedUnannouced__c, JointlyInspectedWith__c,SelectPeriod__c, Status__c, SiteAddress__c, VisitationEndDate__c, VisitationStartDate__c, TimetoVisit__c",
  SELECT_FIELDS_FOR_PUBLIC_RECONSIDERATION_ATTACHMENT:
    "Id,Name,ReconsiderationDate__c,Status__c",
  FETCH_FIELDS_FOR_COMPLAINTS_REFERRAL_PICKLIST: "Origin,SourceofInformation__c,ComplaintSource__c",
  FETCH_FIELDS_FOR_STAFF_PICKLIST: 'AffiliationType__c,EmployeeType__c',
  FETCH_FIELDS_FOR_ACTOR_PICKLIST: 'InjurySeverityLevel__c,PrimaryStaffMember__c,StaffAssaulted__c,InjurySustained__c'
};

export const USER_PROFILE_NAME = {
  USER_SUPERVISOR: "Supervisor",
  USER_SYS_ADMIN: "System Administrator",
  USER_CASE_WRKR: "Caseworker",
  USER_INTAKE_WRKR: "Intake Worker", 
  USER_PROVIDER_COMMUNITY: "Provider Community"
};

export const DASHBORD_LABELS = {
  BTN_PUBLIC: "Public",
  BTN_PRIVATE: "Private"
};

//Button labels declaration
export const BUTTON_LABELS = {
  BTN_SAVE: "Save",
  BTN_SAVE_AS_DRAFT: "Save As Draft",
  BTN_SUBMIT: "Submit",
  BTN_UPDATE: "Update",
  BTN_CANCEL: "Cancel"
};

//DataTable button labels declaration
export const DATATABLE_BUTTON_LABELS = {
  BTN_EDIT: "Edit",
  BTN_VIEW: "View",
  BTN_DELETE: "Delete",
  BTN_GENERATE: "Generate"
};

//Toast Message Header labels declaration
export const TOAST_HEADER_LABELS = {
  TOAST_SUCCESS: "success",
  TOAST_ERROR: "error",
  TOAST_WARNING: "warning",
  TOAST_INFO: "informational"
};
