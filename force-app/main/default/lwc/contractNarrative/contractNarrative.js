/*
    Author         : Sindhu
    @CreatedOn     : MARCH 21 ,2020
    @Purpose       : Contract Narrative Details .

*/

import {
    LightningElement,
    track,
    api,
    wire
} from 'lwc';
import {
    utils
} from 'c/utils';
import * as sharedData from "c/sharedData";
//import getContractNarrativeDetails from '@salesforce/apex/providerNarrative.getContractNarrativeDetails';
import updateNarrativeDetails from '@salesforce/apex/providerNarrative.updateNarrativeDetails';
import getHistoryOfNarrativeDetails from '@salesforce/apex/providerNarrative.getHistoryOfNarrativeDetails';
import getSelectedHistoryRec from '@salesforce/apex/providerNarrative.getSelectedHistoryRec';
import images from '@salesforce/resourceUrl/images';
import getContractStatus from "@salesforce/apex/providerNarrative.getContractStatus";

import {
    refreshApex
} from "@salesforce/apex";
export default class ContractNarrative extends LightningElement {
    @track result;
    @track data = [];

    @track narrativeValue;

    @track visibleHistoryData = false;

    @track richTextDisable = false;
    @track btnDisable = false;
    @track openNarrative = true;
    @track disableAdd = false;
    @track btnspeechDisable = false;


    attachmentIcon = images + '/document-newIcon.svg';
    recordId;
    wireNarrativeHistoryDetails;
    contractForApiCall = this.ContractId;


    @api
    get ContractId() {
        return sharedData.getContractId();
    }

    get isSuperUserFlow() {
        let userProfile = sharedData.getUserProfileName();

        if (userProfile === 'Supervisor')
            return true;
        else
            return false;
    }

    connectedCallback() {
        this.isSuperUserFlow;
    }

    openNarrativeModel() {
        this.openNarrative = true;
        this.btnDisable = false;
        this.richTextDisable = false;
        this.btnspeechDisable = false;
        this.narrativeValue = '';
    }



    @wire(getHistoryOfNarrativeDetails, {
        providerId: '$contractForApiCall'
    })
    wiredNarrativeHistoryDetails(result) {
        this.wireNarrativeHistoryDetails = result;

        if (result.data) {
            if (result.data.length > 0) {
                this.visibleHistoryData = true;

                this.data = result.data.map((row, index) => {
                    return {
                        Id: row.Id,
                        author: row.LastModifiedBy.Name,
                        lastModifiedDate: row.LastModifiedDate,
                        dataIndex: index
                    }
                });
            } else {
                this.visibleHistoryData = false;
            }
        }
    }
    @wire(getContractStatus, {
        contractId: '$contractForApiCall'
    })
    wiredApplicationDetails({
        error,
        data
    }) {
        if (data) {
            if (data.length > 0 && (data[0].ContractStatus__c == 'Active' || data[0].ContractStatus__c == 'Inactive' || data[0].ContractStatus__c == 'Expired' || data[0].ContractStatus__c == 'Submitted')) {
                this.btnDisable = true;
                this.richTextDisable = true;
                this.disableAdd = true;
                this.btnspeechDisable = true;
            } else {
                this.btnDisable = false;
                this.richTextDisable = false;
                this.disableAdd = false;
                this.btnspeechDisable = false;

            }
        }
    }
    handleSpeechToText(event) {

        this.narrativeValue = event.detail.speechValue;
    }

    cancelHandler(){
        this.narrativeValue ='';
    }
    //Narrative On Change Functionality
    narrativeOnChange(event) {

        this.narrativeValue = null;
        this.narrativeValue = event.target.value;
    }

    //Save Functionality
    saveNarrativeDetails() {
        this.template.querySelector('c-common-speech-to-text').stopSpeech();
        if (!this.narrativeValue) {
            this.dispatchEvent(utils.toastMessage("Please enter Narrative", "error"));

        } else {

            let myObj = {
                'sobjectType': 'Contract__c'
            };

            myObj.Narrative__c = this.narrativeValue;

            if (this.ContractId)
                myObj.Id = this.ContractId;

            updateNarrativeDetails({
                    objSobjecttoUpdateOrInsert: myObj
                })
                .then(result => {
                    this.narrativeValue = '';
                    this.dispatchEvent(utils.toastMessage("Contract Narrative Saved Successfully", "success"));
                    return refreshApex(this.wireNarrativeHistoryDetails);

                })


        }
    }

    //Get respective history record
    narrativeHistoryOnClick(event) {
        this.recordId = event.currentTarget.dataset.id;
        let dataIndex = event.currentTarget.dataset.value;
        let datas = this.template.querySelectorAll('.divHighLight');

        for (let i = 0; i < datas.length; i++) {
            if (i == dataIndex)
                datas[i].className = "slds-nav-vertical__item divHighLight slds-is-active";
            else
                datas[i].className = "slds-nav-vertical__item divHighLight";
        }

        getSelectedHistoryRec({
                recId: this.recordId
            })
            .then(result => {
                if (result.length > 0 && result[0].RichNewValue__c) {
                    this.narrativeValue = result[0].RichNewValue__c;
                    this.btnDisable = true;
                    this.richTextDisable = true;
                    this.btnspeechDisable = true;

                }
            })
    }
}