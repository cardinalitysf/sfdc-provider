/**
 * @Author        : Vijayaraj M
 * @CreatedOn     : June 03 , 2020
 * @Purpose       : This component for ContactNotes reusability Js file **/

import { LightningElement, track, api, wire } from "lwc";
import getAllContactDetails from "@salesforce/apex/commonContactNotes.getAllContactDetails";
import editContactDetails from "@salesforce/apex/commonContactNotes.editContactDetails";
import myResource from "@salesforce/resourceUrl/styleSheet";
import * as sharedData from "c/sharedData";

import { loadStyle } from "lightning/platformResourceLoader";
import { refreshApex } from "@salesforce/apex";
import { utils } from "c/utils";
import { CJAMS_OBJECT_QUERIES } from "c/constants";
import { CJAMS_CONSTANTS } from "c/constants";
import { constPopupVariables } from "c/constants";

import {
  getPicklistValuesByRecordType,
  getObjectInfo
} from "lightning/uiObjectInfoApi";
import {
  createRecord,
  updateRecord,
  deleteRecord
} from "lightning/uiRecordApi";

import CONTACTNOTES_OBJECT from "@salesforce/schema/ContactNotes__c";
import ID_FIELD from "@salesforce/schema/ContactNotes__c.Id";
import CONTACTTYPE_FIELD from "@salesforce/schema/ContactNotes__c.ContactType__c";
import LOCATION_FIELD from "@salesforce/schema/ContactNotes__c.Location__c";
import PURPOSE_FIELD from "@salesforce/schema/ContactNotes__c.Purpose__c";
import DATE_FIELD from "@salesforce/schema/ContactNotes__c.Date__c";
import TIME_FIELD from "@salesforce/schema/ContactNotes__c.Time__c";
import CONTACTNAME_FIELD from "@salesforce/schema/ContactNotes__c.ContactName__c";
import CONTACTROLE_FIELD from "@salesforce/schema/ContactNotes__c.ContactRole__c";
import PHONE_FIELD from "@salesforce/schema/ContactNotes__c.Phone__c";
import EMAIL_FIELD from "@salesforce/schema/ContactNotes__c.Email__c";
import NARRATIVE_FIELD from "@salesforce/schema/ContactNotes__c.Narrative__c";
import APPLICATION_FIELD from "@salesforce/schema/ContactNotes__c.Application__c";
import RECONSIDERATION_FIELD from "@salesforce/schema/ContactNotes__c.Reconsideration__c";
import CASE_FIELD from "@salesforce/schema/ContactNotes__c.Case__c";
import images from "@salesforce/resourceUrl/images";
//import getRecordType from '@salesforce/apex/commonContactNotes.getRecordType';
//import RECORDTYPEID_FIELD from '@Salesforce/schema/ContactNotes__c.RecordTypeId';
import getApplicationProviderStatus from "@salesforce/apex/commonContactNotes.getApplicationProviderStatus";

const actions = [
  { label: "Edit", name: "Edit", iconName: "utility:edit", target: "_self" },
  { label: "View", name: "View", iconName: "utility:preview", target: "_self" },
  {
    label: "Delete",
    name: "Delete",
    iconName: "utility:delete",
    target: "_self"
  }
];

const columns = [
  {
    label: "MODE OF CONTACT",
    fieldName: "ContactType__c",
    type: "text",
    sortable: true,
    initialWidth: 170
  },
  {
    label: "LOCATION",
    type: "button",
    sortable: true,
    typeAttributes: {
      name: "dontRedirect",
      label: { fieldName: "Location__c" }
    },
    cellAttributes: {
      class: "title"
    }
  },
  {
    label: "PURPOSE",
    type: "button",
    sortable: true,
    typeAttributes: {
      name: "dontRedirect",
      label: { fieldName: "Purpose__c" }
    },
    cellAttributes: {
      class: "title"
    }
  },
  {
    label: "DATE & TIME",
    fieldName: "DateTime",
    type: "text",
    initialWidth: 170
  },
  {
    label: "CONTACT NAME",
    fieldName: "ContactName__c",
    type: "text",
    sortable: true
  },
  {
    label: "CONTACT ROLE",
    type: "button",
    sortable: true,
    typeAttributes: {
      name: "dontRedirect",
      label: { fieldName: "ContactRole__c" }
    },
    cellAttributes: {
      class: "title"
    }
  },
  { label: "PHONE", fieldName: "Phone__c", type: "text" },
  {
    label: "EMAIL",
    type: "button",
    typeAttributes: {
      name: "dontRedirect",
      label: { fieldName: "Email__c" }
    },
    cellAttributes: {
      class: "title"
    }
  },
  {
    type: "action",
    typeAttributes: { rowActions: actions },
    cellAttributes: {
      iconName: "utility:threedots_vertical",
      iconAlternativeText: "Actions",
      class: "tripledots"
    }
  }
];

export default class CommonContactNotes extends LightningElement {
  //@api getData;
  @api sobjectName;
  //@api recordTypeName;
  @track columns = columns;
  @track contactDetails = {};
  @track noRecordsFound = false;

  //Sort Function
  @track defaultSortDirection = "asc";
  @track sortDirection = "asc";
  @track sortedBy = "ContactType__c";

  //Pagination
  @track totalContactsCount = 0;
  @track totalContacts = [];
  @track page = 1;
  @track currentPageContactData;
  setPagination = 5;
  perpage = 10;

  @track disabled = false;
  @track freezeContactScreen = false;

  @api recordId;
  @track buttonDisable = false;

  //applicationforApiCall = this.getData;
  wiredContactDetails;
  contactId;
  attachmentIcon = images + "/contact-newIcon.svg";

  @track openModel = false;
  @track deleteModel = false;
  @track viewModel = false;
  @track deleteId;

  @track title;
  @track finalStatus;

  //Sundar - Checking SuperUser Login
  @track isSuperUserFlow = false;
  @track Spinner = true;

  //Sundar Added this
  @track btnLabel;

  /*renderedCallback() {
        Promise.all([
            loadStyle(this, myResource + '/styleSheet.css'),
        ])
    }*/

  // get applicationId() {
  //     //return sharedData.getApplicationId();
  //     return 'a0A0w000000WfB6EAK';
  // }

  _getContactNotesData;
  @api get getContactNotesData() {
    return this._getContactNotesData;
  }
  set getContactNotesData(value) {
    this._getContactNotesData = value;
  }
  _selectQueryToGetData;
  // // @wire(getRecordType, {
  // //     // sObjectRecordTypeName: '$recordTypeName'
  // // })
  // recordTypeDetails(data) {
  //     if (data.data != undefined && data.data.length > 0) {
  //     this.recordTypeId = data.data[0].Id;
  //     }
  // }

  // Check Supervisor Login
  get profileName() {
    return sharedData.getUserProfileName();
  }

  connectedCallback() {
    this.buildQueryFOrApex();
    if (this.profileName === "Supervisor") return (this.isSuperUserFlow = true);

    return (this.isSuperUserFlow = false);
  }

  //Get List Table Values
  @wire(getAllContactDetails, {
    commonContactNotesId: "$getContactNotesData",
    sObjectType: "$sobjectName"
    // sObjectRecordTypeName:'$recordTypeName'
  })
  allContactData(result) {
    this.wiredContactDetails = result;

    if (result.data) {
      this.totalContactsCount = result.data.length;

      if (this.totalContactsCount == 0) this.noRecordsFound = true;
      else this.noRecordsFound = false;

      this.totalContacts = result.data.map((row) => {
        return {
          Id: row.Id,
          ContactType__c: row.ContactType__c,
          Location__c: row.Location__c,
          Purpose__c: row.Purpose__c,
          DateTime:
            utils.formatDate(row.Date__c) +
            " " +
            utils.formatTime12Hr(row.Time__c),
          ContactName__c: row.ContactName__c,
          ContactRole__c: row.ContactRole__c,
          Phone__c: row.Phone__c,
          Email__c: row.Email__c
        };
      });

      this.pageData();
      refreshApex(this.wiredContactDetails);
      this.Spinner = false;
    }
  }

  //Pagination
  pageData() {
    let page = this.page;
    let perpage = this.perpage;
    let startIndex = page * perpage - perpage;
    let endIndex = page * perpage;
    this.currentPageContactData = this.totalContacts.slice(
      startIndex,
      endIndex
    );

    if (this.currentPageContactData.length == 0) {
      if (this.page != 1) {
        this.page = this.page - 1;
        this.pageData();
      }
    }
  }

  // Sorting the Data Table Column Function End
  sortBy(field, reverse, primer) {
    const key = primer
      ? function (x) {
        return primer(x[field]);
      }
      : function (x) {
        return x[field];
      };

    return function (a, b) {
      a = key(a);
      b = key(b);
      if (a === undefined) a = "";
      if (b === undefined) b = "";
      a = typeof a === "number" ? a : a.toLowerCase();
      b = typeof b === "number" ? b : b.toLowerCase();

      return reverse * ((a > b) - (b > a));
    };
  }

  //Data Table Sorting Function
  onHandleSort(event) {
    const { fieldName: sortedBy, sortDirection: sortDirection } = event.detail;
    const cloneData = [...this.currentPageContactData];

    cloneData.sort(this.sortBy(sortedBy, sortDirection === "asc" ? 1 : -1));

    this.currentPageContactData = cloneData;
    this.sortDirection = sortDirection;
    this.sortedBy = sortedBy;
  }

  //Page Change Action in Pagination Bar
  hanldeProgressValueChange(event) {
    this.page = event.detail;
    this.pageData();
  }

  //Open Contact Model Box
  openContactModel() {
    if (this.freezeContactScreen)
      return this.dispatchEvent(
        utils.toastMessage(
          `Cannot add contact notes for ${this.finalStatus} application`,
          "warning"
        )
      );

    if (!this.contactId) {
      this.title = "ADD CONTACT DETAILS";
      this.btnLabel = "SAVE";

      let currentMonth =
        new Date().getMonth() + 1 < 10
          ? "0" + (new Date().getMonth() + 1)
          : new Date().getMonth() + 1;
      let currentDate =
        new Date().getDate() < 10
          ? "0" + new Date().getDate()
          : new Date().getDate();
      let currentMinute =
        new Date().getMinutes() < 10
          ? "0" + new Date().getMinutes()
          : new Date().getMinutes();
      let currentHour =
        new Date().getHours() < 10
          ? "0" + new Date().getHours()
          : new Date().getHours();
      this.contactDetails = {
        Date__c:
          new Date().getFullYear() + "-" + currentMonth + "-" + currentDate,
        Time__c: currentHour + ":" + currentMinute + ":00.000Z"
      };
    }

    this.openModel = true;
  }

  //Edit/Delete Model Action
  handleRowAction(event) {
    if (
      event.detail.action.name == "Edit" ||
      event.detail.action.name == "View"
    ) {
      if (this.freezeContactScreen && event.detail.action.name == "Edit")
        return this.dispatchEvent(
          utils.toastMessage(
            `Cannot edit contact notes for ${this.finalStatus} application`,
            "warning"
          )
        );

      let selectedContactId = event.detail.row.Id;

      this.openModel = event.detail.action.name == "Edit" ? true : false;
      this.viewModel = event.detail.action.name == "View" ? true : false;
      this.title =
        event.detail.action.name == "Edit"
          ? "EDIT CONTACT DETAILS"
          : this.title;
      this.btnLabel =
        event.detail.action.name == "Edit" ? "UPDATE" : this.btnLabel;

      this.disabled = false;

      editContactDetails({
        selectedContactId
      })
        .then((result) => {
          if (result.length > 0) {
            this.contactDetails = {
              ContactType__c: result[0].ContactType__c,
              Location__c: result[0].Location__c,
              Purpose__c: result[0].Purpose__c,
              Date__c: result[0].Date__c,
              Time__c: utils.formatTime24Hr(result[0].Time__c),
              ContactName__c: result[0].ContactName__c,
              ContactRole__c: result[0].ContactRole__c,
              Email__c: result[0].Email__c,
              Phone__c: result[0].Phone__c,
              Narrative__c: result[0].Narrative__c
            };
            this.contactId = result[0].Id;
          } else {
            this.dispatchEvent(
              utils.toastMessage("Error in getting contact details", "error")
            );
          }
        })
        .catch((errors) => {
          return this.dispatchEvent(utils.handleError(errors));
        });
    } else if (event.detail.action.name == "Delete") {
      if (this.freezeContactScreen)
        return this.dispatchEvent(
          utils.toastMessage(
            `Cannot delete contact notes for ${this.finalStatus} application`,
            "warning"
          )
        );

      this.deleteModel = true;
      this.deleteId = event.detail.row.Id;
    }
  }

  handleDelete() {
    deleteRecord(this.deleteId)
      .then(() => {
        this.deleteModel = false;
        this.dispatchEvent(
          utils.toastMessage("Contact Details deleted successfully", "Success")
        );

        return refreshApex(this.wiredContactDetails);
      })
      .catch((errors) => {
        this.deleteModel = false;
        return this.dispatchEvent(utils.handleError(errors));
        //return refreshApex(this.wiredContactDetails);
      });

  }

  closeDeleteModal() {
    this.deleteModel = false;
  }

  //Get Object Info
  @wire(getObjectInfo, {
    objectApiName: CONTACTNOTES_OBJECT
  })
  objectInfo;

  //Get PickList Values from Object
  @wire(getPicklistValuesByRecordType, {
    objectApiName: CONTACTNOTES_OBJECT,
    recordTypeId: "$objectInfo.data.defaultRecordTypeId"
  })
  wiredPicklistValues({ error, data }) {
    if (data) {
      //Getting Contact Type Custom field Picklist Values
      this.contactTypeOptions = data.picklistFieldValues.ContactType__c.values.map(
        (key) => {
          return {
            label: key.label,
            value: key.value
          };
        }
      );

      //Getting Location Custom field Picklist Values
      this.locationOptions = data.picklistFieldValues.Location__c.values.map(
        (key) => {
          return {
            label: key.label,
            value: key.value
          };
        }
      );

      //Getting Purpose Custom field Picklist Values
      this.purposeOptions = data.picklistFieldValues.Purpose__c.values.map(
        (key) => {
          return {
            label: key.label,
            value: key.value
          };
        }
      );

      //Getting Contact Role Custom field Picklist Values
      this.roleOptions = data.picklistFieldValues.ContactRole__c.values.map(
        (key) => {
          return {
            label: key.label,
            value: key.value
          };
        }
      );
    } else {
      this.dispatchEvent(
        utils.toastMessage("Error in fetching Picklist Values", "error")
      );
    }
  }

  //Fields On Change Event Handler
  contactOnChange(event) {
    if (event.target.name == "ContactType") {
      this.contactDetails.ContactType__c = event.detail.value;
    } else if (event.target.name == "Location") {
      this.contactDetails.Location__c = event.detail.value;
    } else if (event.target.name == "Purpose") {
      this.contactDetails.Purpose__c = event.detail.value;
    } else if (event.target.name == "Date") {
      this.contactDetails.Date__c = event.target.value;
    } else if (event.target.name == "Time") {
      this.contactDetails.Time__c = event.target.value;
    } else if (event.target.name == "ContactName") {
      this.contactDetails.ContactName__c = event.target.value;
    } else if (event.target.name == "ContactRole") {
      this.contactDetails.ContactRole__c = event.detail.value;
    } else if (event.target.name == "Phone") {
      event.target.value = event.target.value.replace(/(\D+)/g, "");
      this.contactDetails.Phone__c = utils.formattedPhoneNumber(
        event.target.value
      );
    } else if (event.target.name == "Email") {
      this.contactDetails.Email__c = event.target.value;
    } else if (event.target.name == "Narrative") {
      this.contactDetails.Narrative__c = event.target.value;
    }
  }

  getDocumentDetailsFromApex() {
    getApplicationProviderStatus({
      commonContactNotesId: this.getContactNotesData,
      sObjectType: this.sobjectName,
      strFieldsToQuery: this._selectQueryToGetData
    })
      .then((data) => {
        if (
          data.length > 0 &&
          (data[0].Status__c ==
            CJAMS_CONSTANTS.APPLICATION_FINAL_STATUS_REVIEW ||
            data[0].Status__c ==
            CJAMS_CONSTANTS.APPLICATION_FINAL_STATUS_APPROVED ||
            data[0].Status__c ==
            CJAMS_CONSTANTS.PROVIDER_APP_FINAL_STATUS_REJECTED ||
            data[0].Status__c ==
            CJAMS_CONSTANTS.APPLICATION_FINAL_STATUS_SUPERVISOR_REJECTED)
        ) {
          this.finalStatus =
            data[0].Status__c == CJAMS_CONSTANTS.APPLICATION_FINAL_STATUS_REVIEW
              ? "Submitted"
              : data[0].Status__c;
          this.freezeContactScreen = true;
          this.buttonDisable = true;
        } else if (
          data.length > 0 &&
          (data[0].Status == CJAMS_CONSTANTS.APPLICATION_FINAL_STATUS_REVIEW ||
            data[0].Status ==
            CJAMS_CONSTANTS.APPLICATION_FINAL_STATUS_APPROVED ||
            data[0].Status ==
            CJAMS_CONSTANTS.PROVIDER_APP_FINAL_STATUS_REJECTED ||
            data[0].Status ==
            CJAMS_CONSTANTS.APPLICATION_FINAL_STATUS_SUPERVISOR_REJECTED)
        ) {
          this.finalStatus =
            data[0].Status == CJAMS_CONSTANTS.APPLICATION_FINAL_STATUS_REVIEW
              ? "Submitted"
              : data[0].Status;
          this.freezeContactScreen = true;
          this.buttonDisable = true;
        } else {
          this.freezeContactScreen = false;
          this.buttonDisable = false;
        }
      })
      .catch((errors) => {
        return this.dispatchEvent(utils.handleError(errors));
      });
  }

  buildQueryFOrApex() {
    switch (this.sobjectName) {
      case CJAMS_CONSTANTS.CASE_LOOKUP_OBJECT:
        this._selectQueryToGetData =
          CJAMS_OBJECT_QUERIES.SELECT_FIELDS_FOR_PUBLIC_REFERRAL_ATTACHMENT;
        break;
      case CJAMS_CONSTANTS.APPLICATION_OBJECT_NAME:
        this._selectQueryToGetData =
          CJAMS_OBJECT_QUERIES.SELECT_FIELDS_FOR_PUBLIC_APPLICATION_ATTACHMENT;
        break;

      case CJAMS_CONSTANTS.RECONSIDERATION_OBJECT_NAME:
        this._selectQueryToGetData =
          CJAMS_OBJECT_QUERIES.SELECT_FIELDS_FOR_PUBLIC_RECONSIDERATION_ATTACHMENT;
        break;
    }
    this.getDocumentDetailsFromApex();
  }

  //Create or Update Contact Details
  saveContactDetails() {
    this.disabled = true;

    const allValid = [
      ...this.template.querySelectorAll("lightning-input"),
      ...this.template.querySelectorAll("lightning-combobox")
    ].reduce((validSoFar, inputFields) => {
      inputFields.reportValidity();
      return validSoFar && inputFields.checkValidity();
    }, true);

    if (allValid) {
      if (
        this.contactDetails &&
        this.contactDetails.Phone__c &&
        this.contactDetails.Phone__c.length != 14
      ) {
        this.disabled = false;
        return this.dispatchEvent(
          utils.toastMessage("Invalid Phone", "warning")
        );
      }

      const fields = {};

      fields[
        CONTACTTYPE_FIELD.fieldApiName
      ] = this.contactDetails.ContactType__c;
      fields[LOCATION_FIELD.fieldApiName] = this.contactDetails.Location__c;
      fields[PURPOSE_FIELD.fieldApiName] = this.contactDetails.Purpose__c;
      fields[DATE_FIELD.fieldApiName] = this.contactDetails.Date__c;
      fields[TIME_FIELD.fieldApiName] = this.contactDetails.Time__c;
      fields[
        CONTACTNAME_FIELD.fieldApiName
      ] = this.contactDetails.ContactName__c;
      fields[
        CONTACTROLE_FIELD.fieldApiName
      ] = this.contactDetails.ContactRole__c;
      fields[PHONE_FIELD.fieldApiName] = this.contactDetails.Phone__c;
      fields[EMAIL_FIELD.fieldApiName] = this.contactDetails.Email__c;
      fields[NARRATIVE_FIELD.fieldApiName] = this.contactDetails.Narrative__c;

      if (this.sobjectName == CJAMS_CONSTANTS.APPLICATION_OBJECT_NAME) {
        fields[APPLICATION_FIELD.fieldApiName] = this.getContactNotesData;
      } else if (this.sobjectName == CJAMS_CONSTANTS.CASE_LOOKUP_OBJECT) {
        fields[CASE_FIELD.fieldApiName] = this.getContactNotesData;
      } else if (this.sobjectName == CJAMS_CONSTANTS.RECONSIDERATION_OBJECT_NAME) {
        fields[RECONSIDERATION_FIELD.fieldApiName] = this.getContactNotesData;
      }

      if (!this.contactId) {
        const recordInput = {
          apiName: CONTACTNOTES_OBJECT.objectApiName,
          fields
        };

        createRecord(recordInput)
          .then((result) => {
            this.openModel = false;
            this.contactDetails = {};
            this.contactId = null;
            this.dispatchEvent(
              utils.toastMessage(
                "Contact Notes has been saved successfully",
                "Success"
              )
            );
            this.disabled = false;

            return refreshApex(this.wiredContactDetails);
          })
          .catch((errors) => {
            this.disabled = false;
            return this.dispatchEvent(utils.handleError(errors));
          });

      } else {
        fields[ID_FIELD.fieldApiName] = this.contactId;

        const recordInput = { fields };

        updateRecord(recordInput)
          .then(() => {
            this.openModel = false;
            this.contactDetails = {};
            this.contactId = null;
            this.disabled = false;
            this.dispatchEvent(
              utils.toastMessage(
                "Contact Notes has been saved successfully",
                "Success"
              )
            );
            return refreshApex(this.wiredContactDetails);
          }).catch((errors) => {
            this.disabled = false;
            return this.dispatchEvent(utils.handleError(errors));
          });
      }
    } else {
      this.disabled = false;
      this.dispatchEvent(
        utils.toastMessage(
          constPopupVariables.VALIDATION_WARN,
          "Warning"
        )
      );
    }
  }

  //Cancel Contact Model Box
  cancelContactDetails() {
    this.contactDetails = {};
    this.openModel = false;
    this.viewModel = false;
    this.contactId = null;
  }

  //Close Contact Model Box
  closeContactModel() {
    this.contactDetails = {};
    this.openModel = false;
    this.viewModel = false;
    this.contactId = null;
  }
}
