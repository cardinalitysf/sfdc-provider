/**
 * @Author        : V.S.Marimuthu
 * @CreatedOn     : Feb 26 ,2020
 * @Purpose       : All the variables that are needed to be shared have to get;set; through this class
 **/

let providerDatas = {};

// #region Provider Id
const setProviderId = (providerId) => {
    providerDatas.providerId = providerId;

};
const getProviderId = () => providerDatas.providerId;
// #endregion


// #region Case Id
const setCaseId = (caseId) => {
    providerDatas.caseId = caseId;
};
const getCaseId = () => providerDatas.caseId;
// #endregion


// #region Case Id
const setCommunicationValue = (getCommunucation) => {
    providerDatas.getCommunucation = getCommunucation;
};
const getCommunicationValue = () => providerDatas.getCommunucation;
// #endregion


// #region Case Id
const setReceivedDateValue = (getReceivedDate) => {
    providerDatas.getReceivedDate = getReceivedDate;
};
const getReceivedDateValue = () => providerDatas.getReceivedDate;
// #endregion

// #region Case Id
const setButtonDisableforDecisionTab = (blnButtonValue) => {
    providerDatas.blnButtonValue = blnButtonValue;
};
const getButtonDisableforDecisionTab = () => providerDatas.blnButtonValue;
// #endregion

// #region Application Id
const setApplicationId = (applicationId) => {
    providerDatas.applicationId = applicationId;

};
const getApplicationId = () => providerDatas.applicationId;
// #endregion

// #region Staff Id
const setStaffId = (staffId) => {
    providerDatas.staffId = staffId;
};
const getStaffId = () => providerDatas.staffId;
// #endregion


export {
    getProviderId,
    setProviderId,
    getCaseId,
    setCaseId,
    setCommunicationValue,
    getCommunicationValue,
    setReceivedDateValue,
    getReceivedDateValue,
    setButtonDisableforDecisionTab,
    getButtonDisableforDecisionTab,
    setApplicationId,
    getApplicationId,
    setStaffId,
    getStaffId
};