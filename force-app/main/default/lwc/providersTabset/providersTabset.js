/**
 * @Author: Janaswini S
 * @CreatedOn: April 15, 2020
 * @Purpose: Providers Tabset
 * @updatedBy: Janaswini S
 * @updatedOn: April 15,2020
**/


import { LightningElement, track, api } from "lwc";
import fetchDataForTabset from '@salesforce/apex/ProvidersTabset.fetchDataForTabset';
import * as sharedData from "c/sharedData";

//Style Sheet Loading
import { loadStyle } from 'lightning/platformResourceLoader';
import myResource from '@salesforce/resourceUrl/styleSheet';

import { CJAMS_CONSTANTS } from "c/constants";
import { utils } from "c/utils";

export default class ProvidersTabset extends LightningElement {
  @track providerName;
  @api activeTab = "providerInfo";


  /* Style Sheet Loading */
  renderedCallback() {
    Promise.all([
      loadStyle(this, myResource + '/styleSheet.css')
    ])
  }

  get getproviderid() {
    return sharedData.getProviderId();
  }

  connectedCallback() {
    this.fetchDatasFromApplication();
  }

  fetchDatasFromApplication() {
    if (this.getproviderid != undefined) {
      fetchDataForTabset({
        fetchdataname: this.getproviderid
      }).then(result => {
        this.providerName = result[0].ProviderId__c ? result[0].ProviderId__c : " - ";
      })
        .catch(errors => {
          if (errors) {
            let error = JSON.parse(errors.body.message);
            const { title, message, errorType } = error;
            this.dispatchEvent(
              utils.toastMessageWithTitle(title, message, errorType)
            );
          } else {
            this.dispatchEvent(
              utils.toastMessage(
                CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
                CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
              )
            );
          }
        });
    }
  }

  handleredirectMoniteringTabs(event) {
    const onChangeId = new CustomEvent('redirecteditmonitering', { detail: { first: false } });
    this.dispatchEvent(onChangeId);
  }

  handleRedirectToContractManage(event) {
    const onChangeId = new CustomEvent('redirecttocontractmanage', { detail: { first: event.detail.first } });
    this.dispatchEvent(onChangeId);
  }

  handleRedirectToContractAdd(event) {
    const onChangeId = new CustomEvent('redirecttocontractadd', { detail: { first: event.detail.first } });
    this.dispatchEvent(onChangeId);
  }

  handleRedirectToStaffInfo(event) {
    const onChangeId = new CustomEvent('redirecttostaffinfo', { detail: { first: event.detail.first, staffId: event.detail.providerStaffId } });
    this.dispatchEvent(onChangeId);
  }

  handleredirecttoApplicationTabs(event) {
    const onChangeId = new CustomEvent('redirecteditapplication', { detail: { first: event.detail.first } });
    this.dispatchEvent(onChangeId);

  }
  PageRedirecttoDashBoard() {
    window.location.reload();
  }
}