/**
 * @Author        : Pratheeba.V
 * @CreatedOn     : July 2 ,2020
 * @Purpose       : Decison and Signature Details.
 **/
import { LightningElement, wire, track, api } from 'lwc';
import fetchPickListValue from '@salesforce/apex/ComplaintsDecision.getPickListValues';
import IntakeDecisionStatus_FIELD from '@salesforce/schema/Case.IntakeDecisionStatus__c';
import getReviewDetails from "@salesforce/apex/ComplaintsDecision.getReviewDetails";
import CaseworkerDecisionStatus_FIELD from '@salesforce/schema/Case.CaseworkerDecisionStatus__c';
import SupervisorDecisionStatus_FIELD from '@salesforce/schema/Case.SupervisorDecisionStatus__c';
import updatecasedetailsDraft from '@salesforce/apex/ComplaintsDecision.updatecasedetailsDraft';
import updatecasedetails from '@salesforce/apex/ComplaintsDecision.updatecasedetails';
import images from '@salesforce/resourceUrl/images';
import getAllUsersList from "@salesforce/apex/ComplaintsDecision.getAllUsersList";
import getDeficiencyDetails from "@salesforce/apex/ComplaintsDecision.getDeficiencyDetails";
import getMonitoringDetails from "@salesforce/apex/ComplaintsDecision.getMonitoringDetails";
import getSanctionDetails from "@salesforce/apex/ComplaintsDecision.getSanctionDetails";
import getCaseworker from "@salesforce/apex/ComplaintsDecision.getCaseworker";
import saveSign from '@salesforce/apex/ComplaintsDecision.saveSign';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';
import getDatatableDetails from "@salesforce/apex/ComplaintsDecision.getDatatableDetails";
import * as sharedData from "c/sharedData";
import { utils } from 'c/utils';
import { USER_PROFILE_NAME, CJAMS_CONSTANTS } from 'c/constants';
import {
    updateRecord
} from "lightning/uiRecordApi";
import ID_FIELD from '@salesforce/schema/Deficiency__c.Id';
import DEFICIENCYSTATUS_FIELD from '@salesforce/schema/Deficiency__c.Status__c';

const dataTableColumns = [{
    label: 'Date',
    fieldName: 'SubmittedDate__c'
},
{
    label: 'Author',
    fieldName: 'authorName'
},
{
    label: 'Designation',
    fieldName: 'authorDesignation'
},
{
    label: 'Status',
    fieldName: 'status'
},
{
    label: 'Assigned To',
    fieldName: 'assignedToName'
},
{
    label: 'Designation',
    fieldName: 'supervisorDesignation'
},
{
    label: 'Action',
    fieldName: 'id',
    type: "button",
    typeAttributes: {
        iconName: 'utility:preview',
        name: 'View',
        title: 'Click to Preview',
        variant: 'border-filled',
        class: 'view-red',
        disabled: false,
        iconPosition: 'left',
        target: '_self'
    }
},
]

const ModalColumns = [{
    label: 'Name',
    fieldName: 'Name',
    type: 'Name'
},
{
    label: 'CaseLoad',
    fieldName: 'CaseLoad__c',
    type: 'number'
},
{
    label: 'Availability',
    fieldName: 'Availability__c',
    type: 'text'
},
{
    label: 'Zip Code',
    fieldName: '',
    type: 'text'
},
{
    label: 'Type Of Worker',
    fieldName: 'ProfileName',
    type: 'text'
},
{
    label: 'Unit',
    fieldName: 'Unit__c',
    type: 'text'
},
];

// declaration of variables for calculations
let isDownFlag,
    isDotFlag = false,
    prevX = 0,
    currX = 0,
    prevY = 0,
    currY = 0;

let x = "#0000A0"; //blue color
let y = 1.5; //weight of line width and dot.       

let canvasElement, ctx; //storing canvas context
let attachment; //holds attachment information after saving the sigture on canvas
let dataURL, convertedDataURI; //holds image data

export default class ComplaintsDecision extends LightningElement {
    @track complaintStatusPicklist;
    @track ApprovalCommentsvalue;
    @track ApprovalStatus__c;
    @track bShowsignatureModal = false;
    @track openmodel = false;
    @track SignatureShowHide = false;
    @track SignatureValidationView;
    @track tierapprovalShowHide = true;
    @track isOpenModal = false;
    @track assignTierpopupshow = [];
    refreshIcon = images + '/refresh-icon.svg';
    decisionIcon = images + '/decision-icon.svg';
    @track ModalColumns = ModalColumns;
    @track caseworker;
    @track assignDisable = true;
    @track type;
    @track hideDraftBtn = false;
    @track selectStatusShowHide = false;
    @track CommonStatus_FIELD;
    @track isDisableTierApproval = false;
    @track dataTableColumns = dataTableColumns;
    @track HideDatatable = false;
    @track currenttablepage = [];
    @track ApprovalCommentsvalueview;
    @track preSelectedRows = [];
    @track workItemId = '';
    @track nextApproverId;
    @track Status;
    @track providerDecision;
    @track caseworkerApprovalId;
    @track addsign = false;
    @track AccountId;
    @track deficiencyId;
    @track defSancFlag;
    @track monitoringFlag = false;
    @track isSaveAsDraftDisable = false;
    @track isSubmitBtnDisable = false;;

    get profileName() {
        return sharedData.getUserProfileName();
    }

    get caseId() {
        return sharedData.getCaseId();
    }

    /* check whether private or public. if public show only approved and rejected in status dropdown.
    If private, it should go to provider for acceptance begins */
    get providerId() {
        return sharedData.getProviderId();
    }

    get getPrivateOrPublicType() {
        return sharedData.getPrivateOrPublicType();
    }

    get providerCommunityUserId() {
        return sharedData.getOwnerIdCommunityUser();
    }

    get complaintsStatus() {
        return sharedData.getComplaintsStatus();
    }

    @api deficiencyFlag = false;
    @api sanctionFlag;
    @api sendMonitoringFlag;
    connectedCallback() {
        let x = true;
        x = (this.AccountId != undefined) ? false : (this.providerId != undefined) ? false : true;
        if (x) {
            return this.dispatchEvent(utils.toastMessageWithTitle('Decision', 'Provider Not Found', 'Warning'));
        }

        if (this.profileName == USER_PROFILE_NAME.USER_INTAKE_WRKR || this.profileName == USER_PROFILE_NAME.USER_SYS_ADMIN) {
            this.isDisableTierApproval = false;
            this.selectStatusShowHide = true;
            this.tierapprovalShowHide = true;
            this.type = USER_PROFILE_NAME.USER_CASE_WRKR;
            this.CommonStatus_FIELD = IntakeDecisionStatus_FIELD.fieldApiName;
            this.HideDatatable = false;
            let arr = ['Submitted to Caseworker', 'Submitted to Supervisor', 'Accepted', 'Disputed', 'Approved', 'Rejected', 'Awaiting Confirmation', 'Returned'];
            if (arr.includes(this.complaintsStatus)) {
                this.isDisable = true;
                this.isDisablesignature = true;
                this.isDisableTierApproval = true;
                this.isSaveAsDraftDisable = true;
                this.isSubmitBtnDisable = true;
            }
        } else if (this.profileName == USER_PROFILE_NAME.USER_CASE_WRKR) {
            this.CommonStatus_FIELD = CaseworkerDecisionStatus_FIELD.fieldApiName;
            this.type = USER_PROFILE_NAME.USER_SUPERVISOR;
            this.tierapprovalShowHide = true;
            this.HideDatatable = true;
            let arr = ['Submitted to Supervisor', 'Accepted', 'Disputed', 'Approved', 'Rejected', 'Awaiting Confirmation', 'Pending'];
            if (arr.includes(this.complaintsStatus)) {
                this.isDisable = true;
                this.isDisablesignature = true;
                this.isDisableTierApproval = true;
                this.isSaveAsDraftDisable = true;
                this.isSubmitBtnDisable = true;
            }
        } else if (this.profileName == USER_PROFILE_NAME.USER_SUPERVISOR) {
            this.tierapprovalShowHide = false;
            this.CommonStatus_FIELD = SupervisorDecisionStatus_FIELD.fieldApiName;
            this.HideDatatable = true;
            let arr = ['Submitted to Caseworker', 'Approved', 'Rejected', 'Awaiting Confirmation', 'Returned', 'Pending', 'New'];
            if (arr.includes(this.complaintsStatus)) {
                this.isDisable = true;
                this.isDisablesignature = true;
                this.isDisableTierApproval = true;
                this.isSaveAsDraftDisable = true;
                this.isSubmitBtnDisable = true;
            }
        }

        getCaseworker({ caseid: this.caseId })
            .then(res => {
                if (res.length > 0) {
                    this.AccountId = (res[0].AccountId !== undefined) ? res[0].AccountId : '';
                    this.caseworkerApprovalId = (res[0].Caseworker__c !== undefined) ? res[0].Caseworker__c : '';
                }
            }).catch(errors => {
                return this.dispatchEvent(utils.handleError(errors));
            })
        this.decisionVal();
        this.tierUser();
        this.getPicklistOnLoad();
        this.getReviewDetailsInitialLoad();
        this.getDatatablevalues();
    }

    getPicklistOnLoad() {
        fetchPickListValue({ objInfo: { 'sobjectType': 'Case' }, picklistFieldApi: this.CommonStatus_FIELD })
            .then(res => {
                if (this.profileName == USER_PROFILE_NAME.USER_SUPERVISOR && this.getPrivateOrPublicType == 'Public') {
                    let statusType = [];
                    statusType = res.filter(item => item.label != CJAMS_CONSTANTS.COMPLAINT_STATUS_SUBMITFORAKM);
                    this.complaintStatusPicklist = statusType;
                } else {
                    this.complaintStatusPicklist = res;
                }
            })
            .catch(errors => {
                return this.dispatchEvent(utils.handleError(errors));
            })
    }

    closeModalAction() {
        this.openmodel = false;
    }

    tierUser() {
        getAllUsersList({
            profileName: this.type
        })
            .then(data => {
                let userslist = [];
                userslist = data.filter(item => item.Name != "Automated Process" && item.Name != "Platform Integration User" && item.Name != "System");
                this.assignTierpopupshow = userslist;
            }).catch(errors => {
                return this.dispatchEvent(utils.handleError(errors));
            })
    }

    decisionVal() {
        if (this.profileName == USER_PROFILE_NAME.USER_SUPERVISOR) {
            getSanctionDetails({ caseid: this.caseId })
                .then(res => {
                    //let y = true;
                    //y = (res.length > 0) ? false : (this.sanctionFlag != false) ? false : (this.ApprovalStatus__c != undefined) ? (this.ApprovalStatus__c != CJAMS_CONSTANTS.REVIEW_STATUS_APPROVED) ? false : true : null;
                    if (res.length > 0) {
                        this.defSancFlag = true;
                    } else {
                        this.defSancFlag = false;
                    }
                }).catch(errors => {
                    return this.dispatchEvent(utils.handleError(errors));
                })
        }
        getDeficiencyDetails({ caseid: this.caseId })
            .then(res => {
                if (this.profileName == USER_PROFILE_NAME.USER_CASE_WRKR) {
                    if (res.length > 0) {
                        this.localDefFlag = true;
                        this.providerDecision = (res[0].ProviderDecision__c !== undefined) ? res[0].ProviderDecision__c : '';
                    } else {
                        this.localDefFlag = false;
                        //return this.dispatchEvent(utils.toastMessageWithTitle('Decision', 'Deficiency Not Found', 'Warning'));
                    }
                } else if (this.profileName == USER_PROFILE_NAME.USER_SUPERVISOR) {
                    this.deficiencyId = (res.length > 0) ? res[0].Id : '';
                    this.providerDecision = (res.length > 0) ? (res[0].ProviderDecision__c !== undefined) ? res[0].ProviderDecision__c : '' : '';
                }
            }).catch(errors => {
                return this.dispatchEvent(utils.handleError(errors));
            })
        if (this.profileName == USER_PROFILE_NAME.USER_CASE_WRKR) {
            getMonitoringDetails({ caseid: this.caseId })
                .then(res => {
                    
                    if (res.length > 0) {
                        let statusArray = [];
                        res.forEach(row => {
                            statusArray.push(row.Status__c);
                        });
                        
                        if (statusArray.includes('Draft')) {
                            this.monitoringFlag = false;
                        } else {
                            this.monitoringFlag = true;
                        }
                    } else {
                        this.monitoringFlag = false;
                    }
                    
                }).catch(errors => {
                    return this.dispatchEvent(utils.handleError(errors));
                })
        }
        /*if (this.profileName == USER_PROFILE_NAME.USER_INTAKE_WRKR || this.profileName == USER_PROFILE_NAME.USER_SYS_ADMIN) {
            if (this.defSancFlag)
                this.handleAfterSubmit();
            else
                this.defSancFlag = true;
        }*/
    }

    /* tier approver popup begins */
    handleRowSelection = event => {
        if (this.profileName == USER_PROFILE_NAME.USER_INTAKE_WRKR || this.profileName == USER_PROFILE_NAME.USER_SYS_ADMIN) {
            this.caseworker = event.detail.selectedRows[0].Id;
            this.nextApproverId = event.detail.selectedRows[0].Id;
        } else if (this.profileName == USER_PROFILE_NAME.USER_CASE_WRKR) {
            this.supervisor = event.detail.selectedRows[0].Id;
            this.nextApproverId = event.detail.selectedRows[0].Id;
        }
        this.assignDisable = false;
    }

    dataAction(event) {
        let actiondt = event.detail.row;
        this.AuthorValue = event.detail.row.authorName;
        this.DateValue = event.detail.row.SubmittedDate__c;
        this.StatusValue = event.detail.row.status;
        this.ApprovalCommentsvalueview = event.detail.row.ApprovalCommentsvalue;
        this.SignatureValue = '/sfc/servlet.shepherd/version/download/' + event.detail.row.SignatureValue;
        this.openmodel = true;
    }

    AuthorChange(event) {
        this.AuthorValue = event.target.value;
    }
    StatusChange(event) {
        this.StatusValue = event.target.value;
    }
    DateChange(event) {
        this.DateValue = event.target.value;
    }

    handleCloseModal() {
        this.isOpenModal = false;
    }

    handleAssignModal() {
        let myObj = {
            'sobjectType': 'Case'
        };
        if (this.profileName == USER_PROFILE_NAME.USER_INTAKE_WRKR || this.profileName == USER_PROFILE_NAME.USER_SYS_ADMIN) {
            myObj.Caseworker__c = this.caseworker;
        } else if (this.profileName == USER_PROFILE_NAME.USER_CASE_WRKR) {
            myObj.Supervisor__c = this.supervisor;
        }
        myObj.SubmittedDate__c = utils.formatDateYYYYMMDD(new Date());
        if (this.caseId)
            myObj.Id = this.caseId;
        updatecasedetailsDraft({
            objSobjecttoUpdateOrInsert: myObj
        })
            .then(result => {
                this.isOpenModal = false;
            })
            .catch(error => {
                this.isOpenModal = false;
            });
    }

    /* tier approver popup ends */

    getReviewDetailsInitialLoad() {
        getReviewDetails({
            caseid: this.caseId
        })
            .then(result => {
                if (result.length > 0) {
                    if (this.profileName == USER_PROFILE_NAME.USER_CASE_WRKR) {
                        this.ApprovalStatus__c = result[0].CaseworkerDecisionStatus__c;
                        this.ApprovalCommentsvalue = result[0].CaseworkerComments__c;
                    } else if (this.profileName == USER_PROFILE_NAME.USER_INTAKE_WRKR || this.profileName == USER_PROFILE_NAME.USER_SYS_ADMIN) {
                        this.ApprovalStatus__c = result[0].IntakeDecisionStatus__c;
                        this.ApprovalCommentsvalue = result[0].IntakeComments__c;
                    } else if (this.profileName == USER_PROFILE_NAME.USER_SUPERVISOR) {
                        this.ApprovalStatus__c = result[0].SupervisorDecisionStatus__c;
                        this.ApprovalCommentsvalue = result[0].SupervisorComments__c;
                    }
                    if (result[0].ApprovalStatus__c == CJAMS_CONSTANTS.REVIEW_STATUS_NONE) {
                        this.isDisableTierApproval = true;
                        this.isSubmitBtnDisable = true;
                    }
                    if (!result[0].LicenseStartdate__c && !result[0].LicenseEnddate__c) {
                        this.getLicenseDateFlag = true;
                    } else {
                        this.getLicenseDateFlag = false;
                    }
                    this.Spinner = false;
                }
            }).catch(errors => {
                return this.dispatchEvent(utils.handleError(errors));
            })
    }

    /* add signature save begins */
    /*
        handler to perform save operation.
        save signature as attachment.
        after saving shows success or failure message as toast
    */
    handleSaveClick() {
        //set to draw behind current content
        ctx.globalCompositeOperation = "destination-over";
        ctx.fillStyle = "#FFF"; //white
        ctx.fillRect(0, 0, canvasElement.width, canvasElement.height);

        //convert to png image as dataURL
        dataURL = canvasElement.toDataURL("image/png");
        //convert that as base64 encoding
        convertedDataURI = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");

        //call Apex method imperatively and use promise for handling sucess & failure
        saveSign({
            strSignElement: convertedDataURI,
            recId: this.caseId
        })
            .then(result => {
                this.bShowsignatureModal = false;
                this.isDisableTierApproval = this.ApprovalStatus__c == CJAMS_CONSTANTS.REVIEW_STATUS_REJECT ||
                    this.ApprovalStatus__c == CJAMS_CONSTANTS.REVIEW_STATUS_NONE ?
                    true :
                    false;
                this.addsign = true;
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: ' Signature saved Successfully',
                        variant: 'success',
                    }),
                );
            })
            .catch(errors => {
                return this.dispatchEvent(utils.handleError(errors));
            });

    }
    /* add signature save ends */

    /* canvas begins */
    @api recordId;

    //event listeners added for drawing the signature within shadow boundary
    constructor() {
        super();
        this.template.addEventListener('mousemove', this.handleMouseMove.bind(this));
        this.template.addEventListener('mousedown', this.handleMouseDown.bind(this));
        this.template.addEventListener('mouseup', this.handleMouseUp.bind(this));
        this.template.addEventListener('mouseout', this.handleMouseOut.bind(this));
    }
    renderedCallback() {
        canvasElement = this.template.querySelector('canvas');
        ctx = canvasElement.getContext("2d");
    }
    //handler for mouse move operation
    handleMouseMove(event) {
        this.searchCoordinatesForEvent('move', event);
    }

    //handler for mouse down operation
    handleMouseDown(event) {
        this.searchCoordinatesForEvent('down', event);
    }

    //handler for mouse up operation
    handleMouseUp(event) {
        this.searchCoordinatesForEvent('up', event);
    }

    //handler for mouse out operation
    handleMouseOut(event) {
        this.searchCoordinatesForEvent('out', event);
    }

    searchCoordinatesForEvent(requestedEvent, event) {
        if (requestedEvent === 'down') {
            this.setupCoordinate(event);
            isDownFlag = true;
            isDotFlag = true;
            if (isDotFlag) {
                this.drawDot();
                isDotFlag = false;
            }
        }
        if (requestedEvent === 'up' || requestedEvent === "out") {
            isDownFlag = false;
        }
        if (requestedEvent === 'move') {
            if (isDownFlag) {
                this.setupCoordinate(event);
                this.redraw();
            }
        }
    }

    //This method is primary called from mouse down & move to setup cordinates.
    setupCoordinate(eventParam) {
        //get size of an element and its position relative to the viewport 
        //using getBoundingClientRect which returns left, top, right, bottom, x, y, width, height.
        const clientRect = canvasElement.getBoundingClientRect();
        prevX = currX;
        prevY = currY;
        currX = eventParam.clientX - clientRect.left;
        currY = eventParam.clientY - clientRect.top;
    }

    //For every mouse move based on the coordinates line to redrawn
    redraw() {
        ctx.beginPath();
        ctx.moveTo(prevX, prevY);
        ctx.lineTo(currX, currY);
        ctx.strokeStyle = x; //sets the color, gradient and pattern of stroke
        ctx.lineWidth = y;
        ctx.closePath(); //create a path from current point to starting point
        ctx.stroke(); //draws the path
    }

    //this draws the dot
    drawDot() {
        ctx.beginPath();
        ctx.fillStyle = x; //blue color
        ctx.fillRect(currX, currY, y, y); //fill rectrangle with coordinates
        ctx.closePath();
    }

    handleClearClick() {
        this.SignatureShowHide = false;
        ctx.clearRect(0, 0, canvasElement.width, canvasElement.height);
    }

    handleFilesChange(event) {
        let strFileNames = '';
        // Get the list of uploaded files
        const uploadedFiles = event.detail.files;
        for (let i = 0; i < uploadedFiles.length; i++) {
            strFileNames += uploadedFiles[i].name + ', ';
        }
        this.dispatchEvent(
            new ShowToastEvent({
                title: 'Success!!',
                message: strFileNames + ' Files uploaded Successfully!!!',
                variant: 'success',
            }),
        );
    }
    /* canvas ends */

    handleOpenModal() {
        this.isOpenModal = true;
        let localvar = [...this.assignTierpopupshow]
        for (var i = 0; i < localvar.length; i++) {
            var x = {
                ProfileName: localvar[i].Profile.Name
            };
            var y = Object.assign(x, localvar[i]);
            localvar[i] = y;
        }
        this.assignTierpopupshow = localvar;
    }

    statusHandler(event) {
        this.ApprovalStatus__c = event.detail.value;
        if (this.ApprovalStatus__c == CJAMS_CONSTANTS.APPLICATION_REVIEW_STATUS_APPROVED)
            this.isDisableTierApproval = false;

        if (this.ApprovalStatus__c == CJAMS_CONSTANTS.REVIEW_STATUS_NONE)
            this.isSubmitBtnDisable = true;
        else
            this.isSubmitBtnDisable = false;
    }

    commentschangeHandler(event) {
        this.ApprovalCommentsvalue = event.detail.value;
    }

    signaturemodal() {
        this.bShowsignatureModal = true;
    }

    openmodal() {
        this.openmodel = true;
    }

    closeModal() {
        this.openmodel = false;
        this.bShowsignatureModal = false;
    }

    /* save as draft begins */
    saveAsDraftMethod() {
        let myObj = {
            'sobjectType': 'Case'
        };
        if (this.profileName == USER_PROFILE_NAME.USER_INTAKE_WRKR || this.profileName == USER_PROFILE_NAME.USER_SYS_ADMIN) {
            myObj.IntakeDecisionStatus__c = this.ApprovalStatus__c;
            myObj.IntakeComments__c = this.ApprovalCommentsvalue;
        } else if (this.profileName == USER_PROFILE_NAME.USER_CASE_WRKR) {
            myObj.CaseworkerDecisionStatus__c = this.ApprovalStatus__c;
            myObj.CaseworkerComments__c = this.ApprovalCommentsvalue;
        } else if (this.profileName == USER_PROFILE_NAME.USER_SUPERVISOR) {
            myObj.SupervisorDecisionStatus__c = this.ApprovalStatus__c;
            myObj.SupervisorComments__c = this.ApprovalCommentsvalue;
        }
        if (this.caseId)
            myObj.Id = this.caseId;
        updatecasedetailsDraft({
            objSobjecttoUpdateOrInsert: myObj
        })
            .then(result => {
                this.dispatchEvent(utils.toastMessage("Complaint submitted successfully", "success"));
                window.location.reload();
            })
            .catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.ERROR_GOOGLE_API_SELECT_ADDR, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
    }
    /* save as draft ends */

    getDatatablevalues() {
        getDatatableDetails({
            caseid: this.caseId
        })
            .then(result => {
                let resultArray = [];
                result = JSON.parse(result);
                if (result.length > 0) {
                    this.SignatureValidation = result[0].signatureUrl;
                    this.workItemId = result[0].cases != undefined ? (result[0].cases.Workitems != undefined ? result[0].cases.Workitems.records[0].Id : '') : '';
                    for (var i = 0; i < result.length; i++) {
                        if (result[i].cases.StepsAndWorkitems !== undefined) {
                            let datas = result[i].cases.StepsAndWorkitems.records;
                            let customStatus = '';
                            for (let data in datas) {
                                let signUrlIndex = result[i].signatureUrl.findIndex(sign => sign.OwnerId === datas[data].ActorId);
                                let signUrl = signUrlIndex >= 0 ? result[i].signatureUrl[signUrlIndex].Id : '';
                                resultArray.push({
                                    authorName: datas[data].Actor.Name,
                                    status: (datas[data].ProcessNode == undefined) ? CJAMS_CONSTANTS.COMPLAINT_STATUS_SUBMITTOCW :
                                        (datas[data].StepStatus == CJAMS_CONSTANTS.PROVIDER_APP_FINAL_STATUS_PENDING) ? CJAMS_CONSTANTS.CONTRACT_STATUS_INPROCESS :
                                            (datas[data].ProcessNode.Name == '1st Approval') ? CJAMS_CONSTANTS.APPLICATION_SUBMITTED_TO_SUPERVISOR :
                                                (datas[data].ProcessNode.Name == '2nd Approval' && this.getPrivateOrPublicType != 'Public') ? CJAMS_CONSTANTS.COMPLAINT_STATUS_SUBMITTOPROVIDER :
                                                    (datas[data].ProcessNode.Name == '3rd Approval') ? this.providerDecision :
                                                        (datas[data].StepStatus == CJAMS_CONSTANTS.COMPLAINT_DROPDOWN_REMOVED) ? CJAMS_CONSTANTS.CONTRACT_STATUS_RETURNED : datas[data].StepStatus,
                                    assignedToName: datas[parseInt(data) - 1] !== undefined ? datas[parseInt(data) - 1].Actor.Name : ' - ',
                                    ApprovalCommentsvalue: datas[data].Comments,
                                    authorDesignation: (datas[data].Actor.Profile.Name == USER_PROFILE_NAME.USER_SYS_ADMIN) ? USER_PROFILE_NAME.USER_INTAKE_WRKR : datas[data].Actor.Profile.Name,
                                    supervisorDesignation: datas[parseInt(data) - 1] !== undefined ? (datas[data - 1].Actor.Profile.Name == USER_PROFILE_NAME.USER_SYS_ADMIN) ? USER_PROFILE_NAME.USER_INTAKE_WRKR : datas[data - 1].Actor.Profile.Name : ' - ',
                                    SubmittedDate__c: utils.formatDate(datas[data].CreatedDate),
                                    SignatureValue: signUrl
                                });
                            }
                        }
                    }
                }
                this.currenttablepage = resultArray;
                if (resultArray.length > 1) {
                    this.SignatureShowHide = true;
                    if (resultArray.length > 3) {
                        if (this.profileName == USER_PROFILE_NAME.USER_SUPERVISOR) {
                            this.addsign = true;
                            this.SignatureValidationView = '/sfc/servlet.shepherd/version/download/' + resultArray[resultArray.length - 3].SignatureValue;
                        } else if (this.profileName == USER_PROFILE_NAME.USER_CASE_WRKR) {
                            this.addsign = true;
                            this.SignatureValidationView = '/sfc/servlet.shepherd/version/download/' + resultArray[resultArray.length - 2].SignatureValue;
                        } else {
                            this.addsign = true;
                            this.SignatureValidationView = '/sfc/servlet.shepherd/version/download/' + resultArray[resultArray.length - 1].SignatureValue;
                        }
                    } else if (resultArray.length > 2) {
                        if (this.profileName == USER_PROFILE_NAME.USER_SUPERVISOR) {
                            this.SignatureShowHide = false;
                        } else if (this.profileName == USER_PROFILE_NAME.USER_CASE_WRKR) {
                            this.addsign = true;
                            this.SignatureValidationView = '/sfc/servlet.shepherd/version/download/' + resultArray[resultArray.length - 2].SignatureValue;
                        } else {
                            this.addsign = true;
                            this.SignatureValidationView = '/sfc/servlet.shepherd/version/download/' + resultArray[resultArray.length - 1].SignatureValue;
                        }
                    } else {
                        if (this.profileName == USER_PROFILE_NAME.USER_SUPERVISOR) {
                            this.SignatureShowHide = false;
                        } else if (this.profileName == USER_PROFILE_NAME.USER_CASE_WRKR) {
                            this.SignatureShowHide = false;
                        } else {
                            this.addsign = true;
                            this.SignatureValidationView = '/sfc/servlet.shepherd/version/download/' + resultArray[resultArray.length - 1].SignatureValue;
                        }
                    }

                } else {
                    this.SignatureShowHide = false;
                }
            }).catch(errors => {
                return this.dispatchEvent(utils.handleError(errors));
            })
    }

    handleAfterSubmit() {

        let myObj = {
            'sobjectType': 'Case'
        };

        if (this.profileName == USER_PROFILE_NAME.USER_INTAKE_WRKR || this.profileName == USER_PROFILE_NAME.USER_SYS_ADMIN) {
            myObj.IntakeDecisionStatus__c = this.ApprovalStatus__c;
            myObj.IntakeComments__c = this.ApprovalCommentsvalue;
            this.Status = (this.ApprovalStatus__c == CJAMS_CONSTANTS.REVIEW_STATUS_SUBMITFORREVIEW) ? CJAMS_CONSTANTS.REVIEW_STATUS_APPROVED : CJAMS_CONSTANTS.REVIEW_STATUS_REJECT;
        } else if (this.profileName == USER_PROFILE_NAME.USER_CASE_WRKR) {
            myObj.CaseworkerDecisionStatus__c = this.ApprovalStatus__c;
            myObj.CaseworkerComments__c = this.ApprovalCommentsvalue;
            this.Status = (this.ApprovalStatus__c == CJAMS_CONSTANTS.REVIEW_STATUS_SUBMITFORREVIEW) ? CJAMS_CONSTANTS.REVIEW_STATUS_APPROVED : CJAMS_CONSTANTS.REVIEW_STATUS_REJECT;
        } else if (this.profileName == USER_PROFILE_NAME.USER_SUPERVISOR) {
            myObj.SupervisorDecisionStatus__c = this.ApprovalStatus__c;
            myObj.SupervisorComments__c = this.ApprovalCommentsvalue;
            this.Status = (this.ApprovalStatus__c == CJAMS_CONSTANTS.COMPLAINT_STATUS_SUBMITFORAKM) ? CJAMS_CONSTANTS.REVIEW_STATUS_APPROVED :
                (this.ApprovalStatus__c == CJAMS_CONSTANTS.REVIEW_STATUS_REWORK) ? CJAMS_CONSTANTS.COMPLAINT_DROPDOWN_REMOVED :
                    (this.ApprovalStatus__c == CJAMS_CONSTANTS.REVIEW_STATUS_APPROVED) ? CJAMS_CONSTANTS.REVIEW_STATUS_APPROVED :
                        (this.ApprovalStatus__c == CJAMS_CONSTANTS.REVIEW_STATUS_REJECT) ? CJAMS_CONSTANTS.REVIEW_STATUS_REJECT : '';
            if (this.ApprovalStatus__c == CJAMS_CONSTANTS.COMPLAINT_STATUS_SUBMITFORAKM) {
                const fields = {};
                fields[ID_FIELD.fieldApiName] = this.deficiencyId;
                fields[DEFICIENCYSTATUS_FIELD.fieldApiName] = 'Pending';
                const recordInput = {
                    fields
                };
                updateRecord(recordInput)
                    .then(() => {

                    })
                    .catch(error => {
                        this.dispatchEvent(utils.toastMessage("Error in deficiency status update", "Warning"));
                    })
            }
        }
        var today = new Date();
        var monthDigit = today.getMonth() + 1;
        if (monthDigit <= 9) {
            monthDigit = '0' + monthDigit;
        }
        var dayDigit = today.getDate();
        if (dayDigit <= 9) {
            dayDigit = '0' + dayDigit;
        }
        var date = today.getFullYear() + "-" + monthDigit + "-" + dayDigit;
        myObj.SubmittedDate__c = date;
        if (this.caseId)
            myObj.Id = this.caseId;
        let workItemId = this.workItemId === '' ? '' : this.workItemId;
        if (this.profileName == USER_PROFILE_NAME.USER_SUPERVISOR && this.ApprovalStatus__c == CJAMS_CONSTANTS.REVIEW_STATUS_REWORK) {
            this.nextApproverId = this.caseworkerApprovalId; //caseworker user id
        } else if (this.profileName == USER_PROFILE_NAME.USER_SUPERVISOR && this.ApprovalStatus__c == CJAMS_CONSTANTS.COMPLAINT_STATUS_SUBMITFORAKM) {
            this.nextApproverId = this.providerCommunityUserId; //provider community user id 
        }
        updatecasedetails({
            objSobjecttoUpdateOrInsert: myObj,
            nextApproverId: this.nextApproverId,
            workItemId: workItemId,
            profileName: this.profileName,
            status: this.Status
        })
            .then(result => {
                this.dispatchEvent(utils.toastMessage("Complaint submitted successfully", "Success"));
                window.location.reload();
            })
            .catch(errors => {
                return this.dispatchEvent(utils.handleError(errors));
            });
    }

    /* submit button begins */
    submitMethod() {
        let x = (this.localDefFlag) ? false : (this.deficiencyFlag) ? false : true;
        let y = (this.defSancFlag) ? false : (this.sanctionFlag) ? false : true;
        if (!this.ApprovalStatus__c)
            return this.dispatchEvent(utils.toastMessage("Please Select Status", "Warning"));

        if (this.addsign == false)
            return this.dispatchEvent(utils.toastMessage("Signature Is Mandatory", "Warning"));

        if (this.profileName == USER_PROFILE_NAME.USER_SUPERVISOR) {
            if ((this.ApprovalStatus__c == CJAMS_CONSTANTS.REVIEW_STATUS_APPROVED)) {
                if (y)
                    return this.dispatchEvent(utils.toastMessageWithTitle('Decision', 'Sanctions Not Found', 'Warning'));
            }
        } else if (this.profileName == USER_PROFILE_NAME.USER_CASE_WRKR) {
            if (!this.monitoringFlag || this.sendMonitoringFlag == false)
                return this.dispatchEvent(utils.toastMessageWithTitle('Decision', 'Monitoring Not Found', 'Warning'));
            if (x)
                return this.dispatchEvent(utils.toastMessageWithTitle('Decision', 'Deficiency Not Found', 'Warning'));
        }
        this.handleAfterSubmit();
    }
    /* submit button ends */
}