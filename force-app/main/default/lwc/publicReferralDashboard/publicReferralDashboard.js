/**
 * @Author        : Preethi Bojarajan
 * @CreatedOn     : Jun 29,2020
 * @Purpose       : Public / Private Referral Dashboard With Card Clickable
 * @updatedBy     :
 * @updatedOn     :
 **/
import { LightningElement, track, wire } from 'lwc';

//Import Apex
import getReferalCount from "@salesforce/apex/PublicReferralDashboard.getReferalCount";
import getReferalPrivList from "@salesforce/apex/PublicReferralDashboard.getReferalPrivList";
import getPubReferralList from "@salesforce/apex/PublicReferralDashboard.getPubReferralList";

//Utils
import * as sharedData from "c/sharedData";
import { DASHBORD_LABELS, USER_PROFILE_NAME } from "c/constants";
import { getRecord } from "lightning/uiRecordApi";
import USER_ID from "@salesforce/user/Id";
import PROFILE_FIELD from "@salesforce/schema/User.Profile.Name";

const cardDatas = [{
    Id: "1",
    title: "Pending",
    key: "draft",
    count: 0
},
{
    Id: "2",
    title: "Approved",
    key: "approved",
    count: 0
},
{
    Id: "3",
    title: "Rejected",
    key: "rejected",
    count: 0
},
{
    Id: "4",
    title: "Total Applications",
    key: "total",
    count: 0
}
];

const columns = [{
    label: "Referral Number", fieldName: "Id", sortable: true, type: "button", typeAttributes: {
        label: { fieldName: "CaseNumber" }, class: "blue", target: "_self", color: "blue"
    }
},
{
    label: "Referral Type", fieldName: "ProviderType", sortable: true, cellAttributes: {
        class: "fontclrGrey"
    }
},
{
    label: "Program", fieldName: "Program", sortable: true, typeAttributes: {
        label: { fieldName: "Program" }
    }, cellAttributes: { class: "fontclrGrey" }
},
{
    label: "Program Type", fieldName: "ProgramType", sortable: true, cellAttributes: { class: "fontclrGrey" }
},
{
    label: "Applicant", fieldName: "Applicant", cellAttributes: { class: "fontclrGrey" }
},
{
    label: "Co-Applicant", fieldName: "CoApplicant", cellAttributes: { class: "fontclrGrey" }
},
{
    label: "Status", fieldName: "Status", type: "text", cellAttributes: { class: { fieldName: "statusClass" } }
}
];

const privateColumns = [{
    label: "Referral Number", fieldName: "Id", sortable: true, type: "button", typeAttributes: {
        label: { fieldName: "CaseNumber" }, class: "blue", target: "_self", color: "blue"
    }
},
{
    label: "Referral Type", fieldName: "ProviderType", sortable: true, cellAttributes: {
        class: "fontclrGrey"
    }
},
{
    label: "License Type", fieldName: "Program", sortable: true, typeAttributes: {
        label: { fieldName: "Program" }
    }, cellAttributes: { class: "fontclrGrey" }
},
{
    label: "Name", fieldName: "Name", sortable: true, cellAttributes: { class: "fontclrGrey" }
},
{
    label: "STATEMENT OF NEED", fieldName: "SON", cellAttributes: { class: "fontclrGrey" }
},
{
    label: "REQUEST FOR PROPOSAL", fieldName: "RFP", cellAttributes: { class: "fontclrGrey" }
},
{
    label: "Status", fieldName: "Status", type: "text", cellAttributes: { class: { fieldName: "statusClass" } }
}
];

export default class PublicReferralDashboard extends LightningElement {
    @track cardDatas = cardDatas;
    @track columns = columns;
    @track privateColumns = privateColumns;
    wiredReferalCount;
    @track totalRecords = [];
    @track activePage = 0;
    @track providerType = DASHBORD_LABELS.BTN_PUBLIC;
    @track searchKey = '';
    @track showPublicData = true;
    @track openModelwindow = false;

    //User Details
    userProfileName;
    supervisorFlow;

    get cardClasses() {
        return [{
            title: "pending",
            card: "pending-cls",
            init: true
        },
        {
            title: "approved",
            card: "approved-cls",
            init: false
        },
        {
            title: "rejected",
            card: "rejected-cls",
            init: false
        },
        {
            title: "total",
            card: "total-cls",
            init: false
        }
        ];
    }

    @wire(getRecord, {
        recordId: USER_ID,
        fields: [PROFILE_FIELD]
    }) wireuser({ error, data }) {
        if (error) {
            this.error = error;
        } else if (data) {
            this.userProfileName = data.fields.Profile.displayValue;
            sharedData.setUserProfileName(data.fields.Profile.displayValue);
            if (this.userProfileName === USER_PROFILE_NAME.USER_SUPERVISOR)
                this.supervisorFlow = true;
        }
    }
    handleredirecttoNewComplaintsTabs() {
        const onredirect = new CustomEvent("redirecttonewcomplaintstabs", {
            detail: {
                first: true
            }
        });
        this.dispatchEvent(onredirect);
    }

    //Common Data Formating Function Start
    formatData(data) {
        let currentData = [];
        if (this.providerType === DASHBORD_LABELS.BTN_PRIVATE)
            data.forEach((row) => {
                let rowData = {};
                rowData.CaseNumber = row.CaseNumber;
                rowData.Program = row.Program__c;
                rowData.Status = row.Status;
                rowData.Id = row.Id;
                rowData.statusClass = row.Status;
                if (row.Account) {
                    rowData.providerId = row.Account.Id;
                    rowData.ProviderType = row.Account.ProviderType__c;
                    rowData.Name = row.Account.Name;
                    rowData.SON = row.Account.SON__c;
                    rowData.RFP = row.Account.RFP__c;
                }
                currentData.push(rowData);
            });
        else
            data.forEach((row) => {
                let rowData = {};
                rowData.CaseNumber = row.CaseNumber;
                rowData.Program = row.Program__c;
                rowData.ProgramType = row.ProgramType__c;
                rowData.Status = row.Status;
                rowData.Id = row.Id;
                rowData.ProviderType = row.Account !== undefined ? row.Account.ProviderType__c : 'Public';
                rowData.providerId = row.Account !== undefined ? row.Account.Id : undefined;
                if (row.Actors__r !== undefined && row.Actors__r.length > 0) {
                    let applicant = row.Actors__r.filter(actor => actor.Role__c === 'Applicant');
                    let coApplicant = row.Actors__r.filter(actor => actor.Role__c === 'Co-Applicant');
                    rowData.Applicant = (applicant.length > 0 && applicant[0].Contact__r !== undefined) ? applicant[0].Contact__r.LastName : '';
                    rowData.CoApplicant = (coApplicant.length > 0 && coApplicant[0].Contact__r !== undefined) ? coApplicant[0].Contact__r.LastName : '';
                } else {
                    rowData.Applicant = ' - ';
                    rowData.CoApplicant = ' - ';
                }
                if (row.Status === 'Assigned for Training')
                    rowData.statusClass = 'Appeal';
                else if (row.Status === 'Training Unattended')
                    rowData.statusClass = 'Expired';
                else if (row.Status === 'Training Completed')
                    rowData.statusClass = 'Approved';
                else
                    rowData.statusClass = row.Status;
                currentData.push(rowData);
            });
        this.template.querySelector('c-common-dashboard').searchFn();
        return currentData;
    }
    // Common Data Formating Function End

    @wire(getReferalCount, {
        searchKey: "$searchKey",
        type: "$providerType"
    })
    wiredReferalResult(result) {
        try {
            this.wiredReferalCount = result;
            if (result.data) {
                let init = 0;
                let search = '';
                let datas = result.data[0];
                for (let key of this.cardDatas) {
                    key.count = datas[key.key];
                }
                this.getDataTableDatas(init, search);
            }
        } catch (error) {
            throw error;
        }
    }

    getDataTableDatas(id, search) {
        this.activePage = id;
        if (this.providerType === DASHBORD_LABELS.BTN_PRIVATE)
            getReferalPrivList({
                searchKey: search,
                type: this.cardDatas[id].key
            }).then((data) => {
                try {
                    let currentData = [];
                    if (data.length > 0) currentData = this.formatData(data);
                    this.totalRecords = currentData;
                } catch (error) {
                    throw error;
                }
            });
        else getPubReferralList({
            searchKey: search,
            type: this.cardDatas[id].key
        }).then((data) => {
            try {
                let currentData = [];
                if (data.length > 0) currentData = this.formatData(data);
                this.totalRecords = currentData;
            } catch (error) {
                throw error;
            }
        });
    }

    handleDataTableDatas(event) {
        let search = '';
        this.getDataTableDatas(event.detail, search);
    }

    handleChange(event) {
        this.searchKey = event.target.value;
        this.getDataTableDatas(this.activePage, this.searchKey);
        this.template.querySelector('c-common-dashboard').searchFn();
    }

    // Redirection Function
    handleRedirection(event) {
        sharedData.setCaseId(event.detail.redirectId);
        sharedData.setProviderId(event.detail.providerId);
        sharedData.setPublicReferralStatus(event.detail.status);
        if (this.providerType === DASHBORD_LABELS.BTN_PRIVATE) {
            const onClickId = new CustomEvent("redirecttoprivatetabset", {
                detail: {
                    first: false,
                    second: false,
                    caseid: event.detail.redirectId,
                    pagetype: "private"
                }
            });
            this.dispatchEvent(onClickId);
        } else {
            const onClickId = new CustomEvent("redirecttopublictabset", {
                detail: {
                    first: false,
                    second: false,
                    caseid: event.detail.redirectId,
                    pagetype: "public"
                }
            });
            this.dispatchEvent(onClickId);
        }
    }

    //On Public / Private Toggle

    handleDashboardDatas(event) {
        this.providerType = event.detail;
        if (this.providerType === DASHBORD_LABELS.BTN_PUBLIC) {
            this.columns = columns;
        } else if (this.providerType === DASHBORD_LABELS.BTN_PRIVATE) {
            this.columns = privateColumns;
        }
        let init = 0;
        let search = this.searchKey;
        this.getDataTableDatas(init, search);
    }

    /* Pop-up New Referral Model Start */
    openModel() {
        sharedData.setCaseId(undefined)
        this.openModelwindow = true;
    }

    intimateParentToCloseModal() {
        this.openModelwindow = false;
    }

    handleHomePageRedirection(event) {
        const onClickNewDirection = new CustomEvent('redirecttoprivate', {
            detail: {
                first: false,
                pageType: event.detail.pageType
            }
        });
        this.dispatchEvent(onClickNewDirection);
    }
    /* Pop-up New Referral Model End */

}