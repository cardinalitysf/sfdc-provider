import { LightningElement } from "lwc";
import * as referralsharedDatas from "c/sharedData";
import { CJAMS_CONSTANTS } from "c/constants";

/**
 * @Author        : Vijayaraj M
 * @CreatedOn     : June 05 , 2020
 * @Purpose       : This component for ContactNotes reusability Js file **/

 export default class PrivateApplicationContactNotes extends LightningElement {
  get applicationId() {
    return referralsharedDatas.getApplicationId();
   
  }

  get sobjectName() {
    return CJAMS_CONSTANTS.APPLICATION_OBJECT_NAME;
  }
}
