/**
 * @Author        : B.Balamurugan
 * @CreatedOn     : April 30, 2020
 * @Purpose       : Application License Date Approval generation.
 * @updatedBy     : 
 * @updatedOn     : 
 **/


import {
    LightningElement,
    track,
    wire
} from "lwc";
import images from "@salesforce/resourceUrl/images";
import {
    loadScript
} from "lightning/platformResourceLoader";
import getapplicationDetails from "@salesforce/apex/ApplicationLicenseInformation.getapplicationDetails";
import getLicensesId from "@salesforce/apex/ApplicationLicenseInformation.getLicensesId";
import getSignatureAsBas64Image from "@salesforce/apex/ApplicationLicenseInformation.getSignatureAsBas64Image";
import UpdateLicenseEndDate from "@salesforce/apex/ApplicationLicenseInformation.UpdateLicenseEndDate";
import * as sharedData from "c/sharedData";
import {
    utils
} from "c/utils";
import generatePDF from "@salesforce/resourceUrl/kendo";
import rxjs from "@salesforce/resourceUrl/Rxjs";
import {
    refreshApex
} from "@salesforce/apex";
import {
    CJAMS_CONSTANTS
} from 'c/constants';



export default class ApplicationLicenseInformation extends LightningElement {
    licenseBeds = images + "/license-bedIcon.svg";
    ageRange = images + "/agerangeIcon.svg";
    iQRange = images + "/iQ-RangeIcon.svg";
    genderIcon = images + "/genderIcon.svg";

    @track LicenseNumber;
    @track Program;
    @track ProgramType;
    @track ProviderName;
    @track BedCapacity;
    @track SiteID;
    @track SiteAddress;
    @track Gender;
    @track licensesIdList = [];
    @track LicenseStartDate;
    @track LicenseEndDate;
    @track MinAge;
    @track MaxAge;
    @track MinIQ;
    @track MaxIQ;
    @track MonitoringPrimID;
    @track disdate = true;
    @track isSaveButtonShow = false;
    @track applicationId;
    @track myview = true;
    @track saveDisable = false;
    @track myload = false;
    @track openmodel = false;
    @track signUrl = "";
    @track CaseWorkerName = "";
    @track SumDate = "";
    @track BillingAddress = "";
    @track FirstName = "";
    @track LastName = "";
    @track generatePDF = generatePDF + "/pdfgen.html";
    @track Status__c = "";
    @track disableLicenseStartDate = false;
    @track disableLicenseEndDate = false;
    @track isBtnDisable = false;
    @track Spinner = true;

    //  Get Dynamic Application id
    get getApplicationId() {
        return sharedData.getApplicationId();
    }

    get getProviderId() {
        return sharedData.getProviderId();
    }



    //Get data fetching using ConnectedCallback
    connectedCallback() {
        loadScript(this, rxjs + "/Rx.min.js")
            .then(() => { })
            .catch((error) => { });
        this.getapplicationDetails();
    }


    //Fetch Details of Application
    getapplicationDetails() {
        getapplicationDetails({
            applicationDetails: this.getApplicationId
        }).then(
            (result) => {

                if (result.length > 0) {
                    this.Program = result[0].Program__c;
                    this.ProgramType = result[0].ProgramType__c;
                    this.ProviderName = result[0].Provider__r.Name;
                    this.LicenseStartDate = utils.formatDate(result[0].LicenseStartdate__c);
                    this.LicenseEndDate = utils.formatDate(result[0].LicenseEnddate__c);

                    this.Gender = result[0].Gender__c;
                    this.BedCapacity = result[0].Capacity__c;
                    this.MinAge = result[0].MinAge__c;
                    this.MaxAge = result[0].MaxAge__c;
                    this.MinIQ = result[0].MinIQ__c;
                    this.MaxIQ = result[0].MaxIQ__c;
                    this.FirstName = result[0].Provider__r ? result[0].Provider__r.FirstName__c : ' - ';
                    this.LastName = result[0].Provider__r ? result[0].Provider__r.LastName__c : ' - ';
                    this.BillingStreet = result[0].Provider__r ? result[0].Provider__r.BillingStreet : ' - ';
                    this.CaseWorkerName = result[0].Caseworker__r ? result[0].Caseworker__r.Name : ' - ';
                    this.SumDate = utils.formatDate(result[0].SubmittedDate__c);

                    if (result[0].Address__r && result[0].Address__r.length > 0) {
                        let addressArr = result[0].Address__r;
                        this.SiteAddress = addressArr[0].AddressLine1__c;
                        this.SiteID = addressArr[0].Name;
                    } else {
                        this.SiteAddress = ' - ';
                        this.SiteID = ' - ';
                    }

                    if (this.LicenseStartDate && this.LicenseEndDate) {
                        var today = new Date();
                        var monthDigit = today.getMonth() + 1;
                        if (monthDigit <= 9) {
                            monthDigit = "0" + monthDigit;
                        }
                        var dayDigit = today.getDate();
                        if (dayDigit <= 9) {
                            dayDigit = "0" + dayDigit;
                        }

                        var date = today.getFullYear() + "-" + monthDigit + "-" + dayDigit;

                        if (date > this.LicenseEndDate) {
                            this.disdate = false;
                            this.isSaveButtonShow = true;
                            this.isBtnDisable = false;
                        } else {
                            this.disdate = true;
                        }
                    } else if (!this.LicenseStartDate || !this.LicenseEndDate) {
                        this.disdate = false;
                    }

                }
                this.Spinner = false;
            }
        ).catch(errors => {
            let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
            return this.dispatchEvent(utils.handleError(errors, config));
        });
    }

    handleChange(event) {

        if (event.target.name == "LicenseStartDate__c") {
            this.LicenseStartDate = event.target.value;
        } else if (event.target.name == "LicenseEndDate__c") {
            this.LicenseEndDate = event.target.value;
        }
    }

    handleSave() {
        if (!this.LicenseStartDate) {
            this.dispatchEvent(utils.toastMessage("Please Select License Start Date", "warning"));
        } else if (!this.LicenseEndDate) {

            this.dispatchEvent(utils.toastMessage("Please Select License End Date", "warning"));
        } else {
            let myobjcasenew = {
                sObject: "Application__c"
            };
            myobjcasenew.LicenseStartdate__c = this.LicenseStartDate;
            myobjcasenew.LicenseEnddate__c = this.LicenseEndDate;
            myobjcasenew.Id = this.getApplicationId;


            UpdateLicenseEndDate({
                objSobjecttoUpdateOrInsert: myobjcasenew
            })
                .then((result) => {

                    this.dispatchEvent(
                        utils.toastMessage(
                            "Application License details has been updated successfully",
                            "success"
                        )
                    );

                    const onredirecttolicecnce = new CustomEvent('licencedate', {
                        detail: {
                            first: false
                        }
                    });
                    this.dispatchEvent(onredirecttolicecnce);

                    return refreshApex(this.getapplicationDetails);

                })
                .catch(errors => {
                    let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                    return this.dispatchEvent(utils.handleError(errors, config));
                });
        }
    }


    //get license number auto-Populated after Decision Approved Submitted.
    @wire(getLicensesId, {
        applicationID: "$getApplicationId"
    })
    getLicenseDetails(result) {

        if (result.data) {
            result.data.map((item) => {
                if (item.Status__c === "For Review") {
                    this.disableLicenseStartDate = false;
                    this.disableLicenseEndDate = false;
                    this.isSaveButtonShow = true;
                    this.isBtnDisable = false;
                    this.LicenseNumber = ' - ';

                } else if (["Caseworker Submitted", "Rejected", "Supervisor Rejected"].includes(item.Status__c)) {
                    this.disableLicenseEndDate = true;
                    this.disableLicenseStartDate = true;
                    this.isSaveButtonShow = true;
                    this.isBtnDisable = true;
                    this.myload = false;
                    this.LicenseNumber = ' - ';
                } else if (item.Status__c === "Approved") {

                    this.LicenseNumber = item.Licences__r && item.Licences__r.length > 0 ? item.Licences__r[0].Name : " - ";
                    this.myload = true;
                    this.disableLicenseEndDate = true;
                    this.disableLicenseStartDate = true;
                    this.isSaveButtonShow = false;
                    this.isBtnDisable = false;
                }

            });

            var base64Image = Rx.Observable.from(
                getSignatureAsBas64Image({
                    appID: this.getApplicationId
                })
            ).subscribe((result) => {
                this.signUrl = "data:image/png;base64," + result;
            });

        }
    }




    handleView() {
        this.openmodel = true;
    }

    //Pdf Generation Function part

    clickgeneratePDF() {
        this.template
            .querySelector("iframe")
            .contentWindow.postMessage(
                this.template.querySelector(".printpdf").innerHTML,
                "*"
            );
    }

    openmodal() {
        this.openmodel = true;
    }

    closeModal() {
        this.openmodel = false;
    }
}