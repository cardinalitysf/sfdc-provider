/**
     * @Author        : Janaswini S
     * @CreatedOn     : August 04,2020
     * @Purpose       : Incident Dashboard
**/

import { LightningElement, wire } from 'lwc';

//Import Apex
import getReferalCount from "@salesforce/apex/IncidentDashboard.getReferalCount";
import getReferalPrivList from "@salesforce/apex/IncidentDashboard.getReferalPrivList";
import getUserEmail from '@salesforce/apex/IncidentDashboard.getUserEmail';

//Utils
import {
    utils
} from "c/utils";
import * as sharedData from "c/sharedData";
import { USER_PROFILE_NAME, CJAMS_CONSTANTS } from "c/constants";
import { getRecord } from "lightning/uiRecordApi";
import USER_ID from "@salesforce/user/Id";
import PROFILE_FIELD from "@salesforce/schema/User.Profile.Name";

const cardDatas = [{
    Id: "1",
    title: "Pending",
    key: "draft",
    count: 0
},
{
    Id: "2",
    title: "Acknowledged",
    key: "approved",
    count: 0
},
{
    Id: "3",
    title: "Rejected",
    key: "rejected",
    count: 0
},
{
    Id: "4",
    title: "Total Incidents",
    key: "total",
    count: 0
}
];

const columns = [{
    label: "Incident Id", fieldName: "CaseNumber", sortable: true, type: "button", typeAttributes: {
        label: { fieldName: "CaseNumber" }, class: "blue", target: "_self", color: "blue"
    }
},
{
    label: "licensing agency", fieldName: "Agency__c", sortable: true, typeAttributes: {
        label: { fieldName: "IncidentType__c" }
    }, cellAttributes: {
        class: "fontclrGrey"
    }
},
{
    label: "person/kid/child", fieldName: "youthData", sortable: true, typeAttributes: {
        label: { fieldName: "Program" }
    }, cellAttributes: { class: "fontclrGrey" }
},
{
    label: "created date", fieldName: "IncidentDate__c", sortable: true, cellAttributes: { class: "fontclrGrey" }
},
{
    label: "type", fieldName: "IncidentType__c", typeAttributes: {
        label: { fieldName: "IncidentType__c" }
    }, cellAttributes: { class: "fontclrGrey" }
},
{
    label: "Status", fieldName: "Status", type: "text", cellAttributes: { class: { fieldName: "statusClass" } }
}
];

const privateColumns = [{
    label: "Incident ID", fieldName: "CaseNumber", sortable: true, type: "button", typeAttributes: {
        label: { fieldName: "CaseNumber" }, class: "blue", target: "_self", color: "blue"
    }
},
{
    label: "PROVIDER NAME", fieldName: "Name", sortable: true, cellAttributes: {
        class: "fontclrGrey"
    }
},
{
    label: "PROVIDER ID", fieldName: "ProviderId__c", sortable: true, cellAttributes: { class: "fontclrGrey" }
},
{
    label: "licensing agency", fieldName: "Agency__c", sortable: true, typeAttributes: {
        label: { fieldName: "IncidentType__c" }
    }, cellAttributes: { class: "fontclrGrey" }
},
{
    label: "Youth id", fieldName: "youthData", cellAttributes: { class: "fontclrGrey" }
},
{
    label: "incident created date", fieldName: "IncidentDate__c", cellAttributes: { class: "fontclrGrey" }
},
{
    label: "type", fieldName: "IncidentType__c", typeAttributes: {
        label: { fieldName: "IncidentType__c" }
    }, cellAttributes: { class: "fontclrGrey" }
},
{
    label: "Status", fieldName: "Status", type: "text", cellAttributes: { class: { fieldName: "statusClass" } }
}
];

export default class IncidentDashboard extends LightningElement {
    cardDatas = cardDatas;
    columns = columns;
    privateColumns = privateColumns;
    wiredReferalCount;
    totalRecords = [];
    activePage = 0;
    searchKey = '';
    showPublicData = true;
    providerId;
    Spinner = true;

    //profile based view
    showSystemAdminDatas = false;
    showCaseWorkerCardDatas = false;

    //User Details
    userProfileName;
    supervisorFlow = false;

    get cardClasses() {
        return [{
            title: "pendingIncident",
            card: "pendingIncident-cls",
            init: true
        },
        {
            title: "acknowldgedIncident",
            card: "acknowldgedIncident-cls",
            init: false
        },
        {
            title: "rejectedIncident",
            card: "rejectedIncident-cls",
            init: false
        },
        {
            title: "total",
            card: "total-cls",
            init: false
        }
        ];
    }

    @wire(getRecord, {
        recordId: USER_ID,
        fields: [PROFILE_FIELD]
    }) wireuser({ error, data }) {
        if (error) {
            this.error = error;
        } else if (data) {
            this.userProfileName = data.fields.Profile.displayValue;
            sharedData.setUserProfileName(data.fields.Profile.displayValue);
            if (this.userProfileName === USER_PROFILE_NAME.USER_SUPERVISOR || this.userProfileName === USER_PROFILE_NAME.USER_CASE_WRKR) {
                this.supervisorFlow = true;
                this.providerId = '';
                this.showCaseWorkerCardDatas = true;
            } else if (this.userProfileName === USER_PROFILE_NAME.USER_SYS_ADMIN || this.userProfileName === USER_PROFILE_NAME.USER_PROVIDER_COMMUNITY) {
                this.showSystemAdminDatas = true;
            }

        }
    }

    //Depending the Logged In User Getting the Provider Id of the User Function Start
    @wire(getUserEmail)
    email({
        error,
        data
    }) {
        try {
            if (data.length > 0) {
                this.providerId = this.supervisorFlow === true ? '' : data[0].Id;
                if(this.supervisorFlow === false )
                sharedData.setProviderId(data[0].Id);
            } else if (error) {
                this.error = error;
            }
        } catch (e) {
        }
    }
    //Depending the Logged In User Getting the Provider Id of the User Function End

    handleredirecttoNewComplaintsTabs() {
        const onredirect = new CustomEvent("redirecttonewincidenttabs", {
            detail: {
                first: true
            }
        });
        this.dispatchEvent(onredirect);
    }

    //Common Data Formating Function Start
    formatData(data) {
        let currentData = [];
        data.forEach((row) => {
            let rowData = {};
            rowData.CaseNumber = row.CaseNumber;
            rowData.Agency__c = row.Agency__c !== undefined ? row.Agency__c : '';
            if(this.userProfileName === undefined || this.userProfileName === USER_PROFILE_NAME.USER_SYS_ADMIN || this.userProfileName === USER_PROFILE_NAME.USER_PROVIDER_COMMUNITY ){
                rowData.Status = (row.Status == 'Submitted to Caseworker') ? 'Submitted' : row.Status;
            } else {
                rowData.Status = (row.Status == 'Submitted to Caseworker') ? 'Pending' : row.Status;
            }
            rowData.statusClass = (rowData.Status == 'Acknowledged') ? 'Approved' : rowData.Status;
            rowData.Id = row.Id;
            
            rowData.IncidentDate__c = utils.formatDate(row.IncidentDate__c);
            rowData.IncidentType__c = row.IncidentType__c;
            rowData.youthData = (row.Actors__r != undefined && row.Actors__r[0].Contact__r.LastName !== undefined) ? row.Actors__r[0].Contact__r.LastName + '(' + row.Actors__r.length + ')' : ' - '
            rowData.CPAHome__c = row.CPAHome__c !== undefined ? row.CPAHome__c : '';
            rowData.Application__c = row.Application__c !== undefined ? row.Application__c : '';
            rowData.Caseworker__c = row.Caseworker__c !== undefined ? row.Caseworker__c : '';

            if (row.Account) {
                rowData.providerId = row.Account.Id;
                rowData.ProviderId__c= row.Account.ProviderId__c;
                rowData.Name = row.Account.Name;
            }
            currentData.push(rowData);
        });
        
        // this.template.querySelector('c-common-dashboard').searchFn();
        return currentData;
        
    }
    // Common Data Formating Function End

    @wire(getReferalCount, {
        searchKey: "$searchKey",
        providerId: "$providerId"
    })
    wiredReferalResult(result) {
        this.wiredReferalCount = result;
        if (result.data) {
            let init = 0;
            let search = '';
            let datas = result.data[0];
            for (let key of this.cardDatas) {
                key.count = datas[key.key];
            }
            this.getDataTableDatas(init, search);
        } else if (result.error) {
            let errors = result.error;
            if (errors) {
                let error = JSON.parse(errors.body.message);
                const {
                    title,
                    message,
                    errorType
                } = error;
                this.dispatchEvent(utils.toastMessageWithTitle(title, message, errorType));
            } else {
                this.dispatchEvent(utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE));
            }
        }
    }

    getDataTableDatas(id, search) {
        this.activePage = id;
        let byType = this.cardDatas[id].key;
        if (this.userProfileName === undefined || this.userProfileName === USER_PROFILE_NAME.USER_SYS_ADMIN || this.userProfileName === USER_PROFILE_NAME.USER_PROVIDER_COMMUNITY)
            byType = 'total';
        getReferalPrivList({
            searchKey: search,
            type: byType,
            providerId: this.providerId
        }).then((data) => {
            
            try {
                let currentData = [];
                
                if (data.length > 0) currentData = this.formatData(data);
                this.totalRecords = currentData;
                this.Spinner = false;
                
            } catch (error) {
                throw error;
            }
        })
    }

    handleDataTableDatas(event) {
        let search = '';
        this.getDataTableDatas(event.detail, search);
    }

    handleChange(event) {
        this.searchKey = event.target.value;
        this.getDataTableDatas(this.activePage, this.searchKey);
        this.template.querySelector('c-common-dashboard').searchFn();
    }

    // Redirection Function
    handleRedirection(event) {
        sharedData.setCaseId(event.detail.redirectId);
        if(this.supervisorFlow === true){
            sharedData.setProviderId(event.detail.providerId);
        }        
        let cpaHomeId = this.totalRecords.filter(
            (i) => i.Id == event.detail.redirectId
        )[0];
        sharedData.setApplicationId(cpaHomeId.Application__c);
        sharedData.setCPAHomeId(cpaHomeId.CPAHome__c)
        sharedData.setIncidentStatus(cpaHomeId.Status);
        sharedData.setCaseworkerId(cpaHomeId.Caseworker__c);
        let btnHide = this.supervisorFlow === true ? true : false;
        const onClickId = new CustomEvent("redirecttoincidenttabset", {
            detail: {
                first: false,
                btnStatus: btnHide
            }
        });
        this.dispatchEvent(onClickId);

    }

    /* New Incident Redirection Start */
    redirectionNewIncident() {
        sharedData.setCaseId(undefined);
        // sharedData.setProviderId(undefined);
        sharedData.setIncidentStatus(undefined);
        sharedData.setApplicationId(undefined);
        sharedData.setCPAHomeId(undefined);
        sharedData.setCaseworkerId(undefined);
        let btnHide = this.supervisorFlow === true ? true : false;
        this.redirectionIncidentTabset(
            "redirecttoincidenttabset", btnHide
        );
    }

    redirectionIncidentTabset(tabToRedirect, btnHide) {
        const onClickId = new CustomEvent(tabToRedirect, {
            detail: {
                first: false,
                btnStatus: btnHide
            }
        });
        this.dispatchEvent(onClickId);
    }

    /*  Incident Redirection End */

}