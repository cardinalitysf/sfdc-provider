import {
    LightningElement,
    track,
    wire,
    api
} from 'lwc';

export default class PublicProviderAllTabSetCmps extends LightningElement {
    @track activeTab = 'basicInfo';
    @track isView = false;
    @track searchHolder = false;
    @track showResult = false;
    @track showListParent;
    @track meetingFlag;
    @track intentFlag;

    @track showDashBoard = false;
    @track showPublicProviderHeader = true;
    @track showPublicProviderAddmember = false;
    @track showPublicProviderAddmemberSearch = false;
    @track showPublicProviderDashBoard = false;
    _childRedirectToPublicProvider;
    @api
    get childRedirectToPublicProvider() {
        return this._childRedirectToPublicProvider;
    }

    set childRedirectToPublicProvider(value) {
        this._childRedirectToPublicProvider = value;
        if (this._childRedirectToPublicProvider) {
            this.showPublicProviderDashBoard = false;
            this.showPublicProviderHeader = true;
        }
    }

    handleAddMember() {
        this.showPublicProviderAddmember = true;
        this.showPublicProviderHeader = false;
        this.searchHolder = false;
    }

    connectedCallback() {
        if (this._childRedirectToPublicProvider) {
            this.showPublicProviderDashBoard = false;
            this.showPublicProviderHeader = true;
        }
    }

    handleHomePageRedirection(evt) {
        this.showPublicProviderDashBoard = false;
    }

    handleNewProviderPageRedirection(evt) {
        this.showPublicProviderDashBoard = false;
    }

    handleAddMemberSearch(event) {
        this.showPublicProviderAddmemberSearch = true;
        this.showPublicProviderHeader = event.detail.first;
        this.showPublicProviderAddmember = event.detail.first;
        this.showResult = false;
    }

    handleAddMemberHome(event) {
        this.showPublicProviderAddmemberSearch = event.detail.first;
        this.activeTab = 'houseHoldMember';
        this.showPublicProviderHeader = true;
    }

    redirectHandleAddMember(event) {
        this.showPublicProviderAddmemberSearch = event.detail.first;
        this.showListParent = event.detail.searchLists;
        this.showPublicProviderAddmember = true;
        this.searchHolder = true;
    }

    redirectToHousHoldDashboard(event) {
        this.showPublicProviderAddmember = event.detail.first;
        this.activeTab = 'houseHoldMember';
        this.showPublicProviderHeader = true;
    }
    handleAddMemberSearchFromAddHouse(event) {
        this.showPublicProviderAddmemberSearch = true;
        this.showPublicProviderHeader = event.detail.first;
        this.showPublicProviderAddmember = event.detail.first;
        this.showResult = true;
    }

    handleAddMemberView(event) {
        this.showPublicProviderAddmember = true;
        this.showPublicProviderHeader = false;
        this.isView = true;
    }
    handleMeeting(event) {
        this.meetingFlag = event.detail.meetingFlag;
    }

    handleintent(event) {
        this.intentFlag = event.detail.intentFlag;
    }

    redirectToReferralHome(event) {
        const onClickNewDirection = new CustomEvent('redirecttoreferralhome', {
            detail: {
                first: event.detail.first
            }
        });
        this.dispatchEvent(onClickNewDirection);
    }
}