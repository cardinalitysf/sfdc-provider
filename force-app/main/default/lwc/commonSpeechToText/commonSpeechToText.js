import {
    LightningElement,
    track,
    api
} from 'lwc';
import speechToText from "@salesforce/resourceUrl/SpeechToText";
export default class CommonSpeechToText extends LightningElement {
    @track speechToTextURL = speechToText + "/speech.html";

    connectedCallback() {
        window.addEventListener("message", event => {
            if (event.data.event === 'speechText') {
                this.dispatchEvent(new CustomEvent('speechtotext', {
                    detail: {
                        speechValue: event.data.data.v1
                    }
                }));
            }
        });
    }
    @api
    stopSpeech() {
        this.template
            .querySelector("iframe")
            .contentWindow.postMessage(
                '',
                "*"
            );

    }
}