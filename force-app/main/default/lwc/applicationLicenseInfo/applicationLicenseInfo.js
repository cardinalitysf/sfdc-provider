/**
 * @Author        : M.Vijayaraj
 * @CreatedOn     : April 17, 2020
 * @Purpose       : Application License Pdf generation.
 * @updatedBy     : 
 * @updatedOn     : 
 **/

 import { LightningElement, track, wire, api } from "lwc";
import { loadScript } from "lightning/platformResourceLoader";
import getlicenseDetails from "@salesforce/apex/applicationLicenseInfo.getlicenseDetails";
import getSignatureAsBas64Image from "@salesforce/apex/applicationLicenseInfo.getSignatureAsBas64Image";
import editlicenseDetails from "@salesforce/apex/applicationLicenseInfo.editlicenseDetails";
import { utils } from "c/utils";
import * as sharedData from "c/sharedData";
import images from "@salesforce/resourceUrl/images";
import generatePDF from "@salesforce/resourceUrl/kendo";
import rxjs from "@salesforce/resourceUrl/Rxjs";
const columns = [
  {
    label: "LICENSE NO",
    fieldName: "Name",
    type: "text"
  },
  {
    label: "PROGRAM NAME",
    fieldName: "ProgramName__c",
    type: "text"
  },
  {
    label: "PROGRAM TYPE",
    fieldName: "ProgramType__c",
    type: "text",
    initialWidth: 350
  },
  {
    label: "START DATE",
    fieldName: "StartDate__c",
    type: "date"
  },
  {
    label: "END DATE",
    fieldName: "EndDate__c",
    type: "date",
    initialWidth: 190
  },
  {
    label: "Action",
    fieldName: "Id",
    type: "button",
    initialWidth: 100,
    typeAttributes: {
      iconName: "utility:preview",
      name: "Edit",
      title: "Edit",
      initialWidth: 20,
      disabled: false,
      iconPosition: "left",
      target: "_self"
    }
  },
  {
    type: "button",
    fieldName: "Id",
    typeAttributes: {
      iconName: "utility:download",
      name: "Download",
      title: "Download",
      initialWidth: 20,
      class: "action-position",
      disabled: false,
      iconPosition: "left",
      target: "_self"
    }
  }
];
export default class ApplicationLicenseInfo extends LightningElement {
  @track generatePDF = generatePDF + "/pdfgen.html";
  @track columns = columns;
  @track data = [];
  @track columns = columns;
  @track currentPageLicenseData;
  @track openmodel = false;
  @track openmodel1 = false;
  @track value;
  @track norecorddisplay = true;
  @track LicenseNo = "";
  @track ProgramName = "";
  @track ProgramType = "";
  @track stid;
  @track StartDate = "";
  @track EndDate = "";
  @track MinAge = "";
  @track MaxAge = "";
  @track Gender = "";
  @track Capacity = "";
  @track FirstName = "";
  @track LastName = "";
  @track SiteAddress = "";
  @track signUrl = "";
  @track CaseWorkerName = "";
  @track SumDate = "";
  @track ProviderName = "";
  @track BillingAddress = "";
  @track licenseid;
  attachmentIcon = images + "/application-licenseinfo.svg";

 
 

  openmodal() {
    this.openmodel = true;
  }
  openmodal1() {
    this.openmodel = true;
  }
  closeModal() {
    this.openmodel = false;
  }
  closeModal1() {
    this.openmodel1 = false;
  }

  //Get data fetching using ConnectedCallback
  connectedCallback() {
    loadScript(this, rxjs + "/Rx.min.js")
      .then(() => {})
      .catch((error) => {});

    this.license();
  }
  license() {
    getlicenseDetails({
      licensedetails: this.ApplicationId
    }).then((result) => {
      this.currentPageLicenseData = result;

      if (this.currentPageLicenseData.length == 0) {
        this.norecorddisplay = false;
      } else {
        this.norecorddisplay = true;
      }
      //this.pageData();
    });
  }

  //Edit  Functinality

  handleRowAction(event) {
    this.licenseid = event.detail.row.Id;
    this.LicenseNo = event.detail.row.Name;
    this.ProgramName = event.detail.row.ProgramName__c;
    this.ProgramType = event.detail.row.ProgramType__c;
    this.StartDate = utils.formatDate(event.detail.row.StartDate__c);
    this.EndDate = utils.formatDate(event.detail.row.EndDate__c);

    var base64Image = Rx.Observable.from(
      getSignatureAsBas64Image({
        appID: this.ApplicationId
      })
    ).subscribe((result) => {
      this.signUrl = "data:image/png;base64," + result;
    });

    editlicenseDetails({
      editlicensedetails: this.ApplicationId
    })
      .then((result) => {
        if (result[0].Licences__r.length != 0) {
          this.MinAge = result[0].Licences__r[0].Application__r.MinAge__c;
          this.MaxAge = result[0].Licences__r[0].Application__r.MaxAge__c;
          this.Gender = result[0].Licences__r[0].Application__r.Gender__c;
          this.Capacity = result[0].Licences__r[0].Application__r.Capacity__c;
        }

        if (!result[0].Address__r) {
          this.SiteAddress = "";
        } else {
          this.SiteAddress = result[0].Address__r[0].AddressLine1__c;
        }

        this.FirstName = result[0].Provider__r.FirstName__c;
        this.LastName = result[0].Provider__r.LastName__c;
        this.ProviderName = result[0].Provider__r.Name;
        this.BillingStreet = result[0].Provider__r.BillingStreet;
        this.CaseWorkerName = result[0].Caseworker__r.Name;
        this.SumDate = utils.formatDate(result[0].SubmittedDate__c);
        this.stid = result[0].Id;
      })
      .catch((error) => {
        this.dispatchEvent(
          utils.toastMessage("Error in fetching record", "error")
        );
      });

    let conList = this.template.querySelector(".PopupPdfDownload");
    if (event.detail.action.name == "Edit") {
      this.openmodel = true;

      if (conList.classList.contains("popupDisplayNone")) {
        conList.classList.add("popupDisplayBlock");
        conList.classList.remove("popupDisplayNone");
      }
    } else if (event.detail.action.name == "Download") {
      this.openmodel = true;
      if (conList.classList.contains("popupDisplayBlock")) {
        conList.classList.add("popupDisplayNone");
        conList.classList.remove("popupDisplayBlock");
      }
      setTimeout(() => {
        this.clickgeneratePDF();
      }, 800);
    }
  }
  clickgeneratePDF() {
    this.template
      .querySelector("iframe")
      .contentWindow.postMessage(
        this.template.querySelector(".printpdf").innerHTML,
        "*"
      );
  }

  //  Get Dynamic Application id

  get ApplicationId() {
    return sharedData.getApplicationId();
  }
}