/**
 * @Author        : Sindhu Venkateswarlu
 * @CreatedOn     : APRIL 6 ,2020
 * @Purpose       : Appication Program  Details.
 * @UpdatedBy : Sundar K(Whole Functionality)
 * @UpdatedOn : Apr 16, 2020
 **/
import {
  LightningElement,
  track,
  wire,
  api
} from "lwc";
import getPickListValues from "@salesforce/apex/ApplicationProviderProgramDetails.getPickListValues";
import updateapplicationdetails from "@salesforce/apex/ApplicationProviderProgramDetails.updateOrInsertSOQL";
import updateOrInsertSOQLnew from "@salesforce/apex/ApplicationProviderProgramDetails.updateOrInsertSOQLnew";
import fetchDataForProgramDetails from "@salesforce/apex/ApplicationProviderProgramDetails.fetchDataForProgramDetails";
import getAccreditationValues from "@salesforce/apex/ApplicationProviderProgramDetails.getAccreditationValues";
import {
  utils
} from "c/utils";
import * as sharedData from "c/sharedData";
import rangeSlider from "@salesforce/resourceUrl/RangeSlider";
import {
  refreshApex
} from "@salesforce/apex";

//Sundar Modified Button Freeze
import getProviderApplicationStatus from "@salesforce/apex/ApplicationProviderProgramDetails.getProviderApplicationStatus";
import {
  CJAMS_CONSTANTS
} from 'c/constants';

export default class ApplicationproviderprogramDetails extends LightningElement {
  @track Program;
  @track ProgramType;
  @track ProgramName;
  @track TaxIdvalue;
  @track Accreditation;
  @track Gender;
  @track Capacity;
  @track MinAgevalue;
  @track MaxAgevalue;
  @track MinIQvalue;
  @track MaxIQvalue;
  @track Profitvalue;
  @track loaded = false;
  //buttonactive = true;
  rangeSliderAgeURL = rangeSlider + "/Age.html";
  rangeSliderIQURL = rangeSlider + "/IQ.html";
  wiredProgramResult;

  //Sundar Modified this to freeze provider Application
  @track disableProgramDetailsFields = false;
  @track applicationForApiCall = this.ApplicationId;
  @track isSaveBtnDisabled = false;
  @track addressNotFound = false;

  //Get Application Id
  @api
  get ApplicationId() {
    return sharedData.getApplicationId();
  }

  //Get Provider Id
  @api
  get ProviderId() {
    return sharedData.getProviderId();
  }

  //Get Slider Age URL
  get sliderAgeURL() {
    return this.rangeSliderAgeURL;
  }

  //Get Slider ID URL
  get sliderIQURL() {
    return this.rangeSliderIQURL;
  }

  //Get Accreditation Pick List values
  @wire(getAccreditationValues)
  getAccreditationValues({
    error,
    data
  }) {
    if (data) {
      if (data.length > 0) {
        this.accreditationOptions = data.map(row => {
          return {
            label: row.RefValue__c,
            value: row.RefValue__c
          };
        });
        return refreshApex(this.wiredProgramResult);
      } else {
        this.dispatchEvent(utils.toastMessage("No Accreditation found", "error"));
      }
    } else if (error) {
      this.dispatchEvent(utils.toastMessage("Error in Fetching Accreditation", "error"));
    }
  }

  //Get Profit Pick List Values
  @wire(getPickListValues, {
    objInfo: {
      sobjectType: "Account"
    },
    picklistFieldApi: "Profit__c"
  })
  wireCheckPicklistProfitvalue({
    error,
    data
  }) {
    if (data) {
      this.ProfitOptions = data;

      return refreshApex(this.wiredProgramResult);
    } else if (error) {

    }
  }

  //Get Gender Pick List Values
  @wire(getPickListValues, {
    objInfo: {
      sobjectType: "Application__c"
    },
    picklistFieldApi: "Gender__c"
  })
  wireCheckPicklistGender({
    error,
    data
  }) {
    if (data) {
      this.GenderOptions = data;
      return refreshApex(this.wiredProgramResult);
    } else if (error) {}
  }

  //Connected Call back method
  connectedCallback() {
    setTimeout(() => {
      fetchDataForProgramDetails({
          fetchdataname: this.ApplicationId
        })
        .then(result => {
          this.wiredProgramResult = result;

          this.Program = result[0].Program__c;
          this.ProgramType = result[0].ProgramType__c;
          this.ProgramName = result[0].Provider__r.Name;
          this.TaxIdvalue = result[0].Provider__r.TaxId__c;

          if (result[0].Provider__r.Accrediation__c != undefined) {
            this.Accreditation = result[0].Provider__r.Accrediation__c;
          }
          if (result[0].Gender__c != undefined) {
            this.Gender = result[0].Gender__c;
          }

          //This condition is used to check validation for provider application before submitting
          if (result[0].Capacity__c != null && result[0].MinAge__c != null && result[0].MaxAge__c != null && result[0].MinIQ__c != null && result[0].MaxIQ__c != null && result[0].Gender__c != null) {
            this.eventDeclaration();
          }

          this.Capacity = result[0].Capacity__c;
          this.MinAgevalue = result[0].MinAge__c;
          this.MaxAgevalue = result[0].MaxAge__c;
          this.MinIQvalue = result[0].MinIQ__c;
          this.MaxIQvalue = result[0].MaxIQ__c;
          this.rangeSliderAgeURL = rangeSlider + "/Age.html?MinIQvalue=" + this.MinAgevalue + "&MaxIQvalue=" + this.MaxAgevalue +"&disableSlider=" + this.disableProgramDetailsFields;
          this.rangeSliderIQURL = rangeSlider + "/IQ.html?MinIQvalue=" + this.MinIQvalue + "&MaxIQvalue=" + this.MaxIQvalue +"&disableSlider=" + this.disableProgramDetailsFields;

          if (result[0].Provider__r.Profit__c != undefined) {
            this.Profitvalue = result[0].Provider__r.Profit__c;
          }

          this.loaded = !this.loaded;
        })
        .catch(error => {

        });
    }, 1000);

    window.addEventListener("message", event => {
      let iframevalue = event.data.split(":");

      switch (iframevalue[0]) {
        case "MinAge":
          this.MinAgevalue = iframevalue[1];
          break;
        case "MaxAge":
          this.MaxAgevalue = iframevalue[1];
          break;
        case "MinIQ":
          this.MinIQvalue = iframevalue[1];
          break;
        case "MaxIQ":
          this.MaxIQvalue = iframevalue[1];
          break;
        default:
          // code block
      }
    });
  }

  //Sundar Updated this function for freeze the screen
  //Get Provider Application Status
  @wire(getProviderApplicationStatus, {
    applicationId: '$applicationForApiCall'
  })
  wiredApplicationDetails({
    error,
    data
  }) {
    if (data) {
      if (data.length > 0) {
        if (data[0].Status__c == CJAMS_CONSTANTS.PROVIDER_APP_FINAL_STATUS_REVIEW || data[0].Status__c == CJAMS_CONSTANTS.PROVIDER_APP_FINAL_STATUS_APPEAL || data[0].Status__c == CJAMS_CONSTANTS.APPLICATION_FINAL_STATUS_REVIEW || data[0].Status__c == CJAMS_CONSTANTS.PROVIDER_APP_FINAL_STATUS_REJECTED || data[0].Status__c == CJAMS_CONSTANTS.APPLICATION_FINAL_STATUS_APPROVED || data[0].Status__c == CJAMS_CONSTANTS.APPLICATION_FINAL_STATUS_SUPERVISOR_REJECTED) {
          this.isSaveBtnDisabled = true;
          this.disableProgramDetailsFields = true;
        } else {
          this.isSaveBtnDisabled = false;
          this.disableProgramDetailsFields = false;
        }
      }
    }
  }

  handleAddressLengthFromChild(evt) {
    this.addressNotFound = evt.detail.addressLength === false ? true : false;
  }

  fncMinValue(minValue) {}
  handleChangeTax(event) {
    event.target.value = event.target.value.replace(/(\D+)/g, "");
    this.TaxIdvalue = utils.formattedTaxID(event.target.value);
  }
  handleChangeProgramType(event) {
    this.ProgramType = event.detail.value;
  }

  handleChangeProgram(event) {
    this.Program = event.detail.value;
  }

  handleCapacityChange(event) {
    this.Capacity = event.detail.value;
  }

  handleChangeProgramName(event) {
    this.ProgramName = event.detail.value;
  }

  handlechangeminAge(event) {
    this.MinAgevalue = event.detail.value;
  }

  handlechangemaxAge(event) {
    this.MaxAgevalue = event.detail.value;
  }

  handlechangeminIQ(event) {
    this.MinIQvalue = event.detail.value;
  }

  handleChangeMaxIQ(event) {
    this.MaxIQvalue = event.detail.value;
  }

  handleChangegender(event) {
    this.Gender = event.detail.value;
  }

  handleRadioButton(event) {
    this.Profitvalue = event.detail.value;
  }
  @wire(getAccreditationValues)
  getAccreditationValues({
    error,
    data
  }) {
    if (data) {
      if (data.length > 0) {
        this.accreditationOptions = data.map(row => {
          return {
            label: row.RefValue__c,
            value: row.RefValue__c
          };
        });
        return refreshApex(this.wiredProgramResult);
      } else {
        this.dispatchEvent(
          utils.toastMessage("No Accreditation found", "error")
        );
      }
    } else if (error) {
      this.dispatchEvent(
        utils.toastMessage("Error in Fetching Accreditation", "error")
      );
    }
  }

  handleChangeAccreditation(event) {
    this.Accreditation = event.target.value;
  }



  /*get isSaveButtonDisabled() {
    try {
      this.buttonactive =
        (this.template.querySelector(".mandateProgram") ?
          this.template.querySelector(".mandateProgram").value != undefined :
          true) &&
        (this.template.querySelector(".mandateProgramType") ?
          this.template.querySelector(".mandateProgramType").value !=
          undefined :
          true) &&
        (this.template.querySelector(".mandateTaxID") ?
          this.template.querySelector(".mandateTaxID").value != "" :
          true) &&
        (this.template.querySelector(".mandateProgramName") ?
          this.template.querySelector(".mandateProgramName").value != "" :
          true) &&
        (this.template.querySelector(".mandateAccrediation") ?
          this.template.querySelector(".mandateAccrediation").value != "" :
          true) &&
        (this.template.querySelector(".mandateCapacity") ?
          this.template.querySelector(".mandateCapacity").value != "" :
          true) &&
        (this.template.querySelector(".mandateGender") ?
          this.template.querySelector(".mandateGender").value != "" :
          true) &&
        (this.template.querySelector(".mandateMinAge") ?
          this.template.querySelector(".mandateMinAge").value != undefined :
          true) &&
        (this.template.querySelector(".mandateMaxAge") ?
          this.template.querySelector(".mandateMaxAge").value != undefined :
          true) &&
        (this.template.querySelector(".mandateMinIQ") ?
          this.template.querySelector(".mandateMinIQ").value != undefined :
          true) &&
        (this.template.querySelector(".mandateMaxIQ") ?
          this.template.querySelector(".mandateMaxIQ").value != "" :
          true);
    } catch (ex) {
      this.buttonactive = true;
    }

    //sharedData.setButtonDisableforReviewTab(this.buttonactive);
    return !this.buttonactive;
  }*/

  handleClickRequired(evt) {
    const allValid = [
      ...this.template.querySelectorAll("lightning-input"),
      ...this.template.querySelectorAll("lightning-combobox")
    ].reduce((validSoFar, inputCmp) => {
      inputCmp.reportValidity();
      return validSoFar && inputCmp.checkValidity();
    }, true);
    let button = this.template.querySelector("lightning-button");

    if (allValid) {
      button.disabled = false;
    } else {
      button.disabled = true;
    }
  }
  renderedCallback() {
    this.isSaveButtonDisabled;
  }
  handleClicksave() {
    let myobjcase = {
      sobjectType: "Application__c"
    };
    myobjcase.Gender__c = this.Gender;
    myobjcase.Capacity__c = this.Capacity;

    if (this.ApplicationId) {
      myobjcase.Id = this.ApplicationId;
    }
    if (!this.MinAgevalue) {
      this.MinAgevalue = '0';
    }
    if (!this.MinIQvalue) {
      this.MinIQvalue = '100';
    }
    myobjcase.MinAge__c = this.MinAgevalue;
    myobjcase.MaxAge__c = this.MaxAgevalue;
    myobjcase.MinIQ__c = this.MinIQvalue;
    myobjcase.MaxIQ__c = this.MaxIQvalue;
    myobjcase.ProgramType = this.ProgramType;
    myobjcase.Program = this.Program;
    if (!myobjcase.Gender__c) {
      this.dispatchEvent(utils.toastMessage("Gender is mandatory", "warning"));
    } else if (Number(myobjcase.MaxAge__c) < Number(myobjcase.MinAge__c)) {
      this.dispatchEvent(
        utils.toastMessage("Max Age should be greater than Min Age", "warning")
      );
    } else if (!myobjcase.MaxAge__c) {
      this.dispatchEvent(utils.toastMessage("Age is mandatory", "warning"));
    } else if (myobjcase.MaxAge__c == 0) {
      this.dispatchEvent(utils.toastMessage("Please enter Max Age", "warning"));
    } else if (Number(myobjcase.MaxIQ__c) < Number(myobjcase.MinIQ__c)) {
      this.dispatchEvent(
        utils.toastMessage("Max IQ should be greater than Min IQ", "warning")
      );
    } else if (!myobjcase.MaxIQ__c) {
      this.dispatchEvent(utils.toastMessage("IQ is mandatory", "warning"));
    } else if (myobjcase.MaxIQ__c == 0) {
      this.dispatchEvent(utils.toastMessage("Please enter Max IQ", "warning"));
    } else if (!myobjcase.Capacity__c) {
      this.dispatchEvent(utils.toastMessage("Capacity is mandatory", "warning"));
    } else if (!myobjcase.ProgramType) {
      this.dispatchEvent(utils.toastMessage("Program Type is mandatory", "warning"));
    } else if (!myobjcase.Program) {
      this.dispatchEvent(utils.toastMessage("Program is mandatory", "warning"));
    } else if (this.addressNotFound) {
      this.dispatchEvent(utils.toastMessage("Minimum one program site address is required", "warning"));
    } else {
      updateapplicationdetails({
        objSobjecttoUpdateOrInsert: myobjcase
      }).then(
        result => {
          //this.dispatchEvent(utils.toastMessage("Application Saved Successfully", "success"));
        }
      );
      let myobjcasenew = {
        sobjectType: "Account"
      };
      myobjcasenew.Accrediation__c = this.Accreditation;
      myobjcasenew.Profit__c = this.Profitvalue;
      myobjcasenew.Name = this.ProgramName;
      myobjcasenew.TaxId__c = this.TaxIdvalue;

      if (this.ProviderId) {
        myobjcasenew.Id = this.ProviderId;
      }


      if (!myobjcasenew.TaxId__c) {
        this.dispatchEvent(
          utils.toastMessage("Program Tax Id is mandatory", "warning")
        );
      } else if (!myobjcasenew.Name) {
        this.dispatchEvent(
          utils.toastMessage("Program Name is mandatory", "warning")
        );
      } else if (!myobjcasenew.Accrediation__c) {
        this.dispatchEvent(
          utils.toastMessage("Accreditation is mandatory", "warning")
        );
      } else {
        updateOrInsertSOQLnew({
          objSobjecttoUpdateOrInsert: myobjcasenew
        }).then(result => {
          this.eventDeclaration();

          this.dispatchEvent(
            utils.toastMessage("Application Saved Successfully", "success")
          );
        });
        // .catch(error => {
        //     this.dispatchEvent(utils.toastMessage("Error in creating record", "error"));

        // });
        //}
      }
    }
  }

  eventDeclaration() {
    this.dispatchEvent(
      new CustomEvent("checkvalidationinfinalpage", {
        detail: {
          validate: true
        }
      })
    );
  }
}