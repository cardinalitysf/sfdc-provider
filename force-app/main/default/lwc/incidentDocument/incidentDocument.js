/**
 * @Author        : Tamilarasan G
 * @CreatedOn     : August 14,2020
 * @Purpose       : Incident Document
**/

 import { LightningElement } from 'lwc';
 import * as referralsharedDatas from "c/sharedData";
 import {
     CJAMS_CONSTANTS
 } from 'c/constants';

export default class IncidentDocument extends LightningElement {

    get incidentId() {
        return referralsharedDatas.getCaseId();
    }
    get sobjectNameToFetch() {
        return CJAMS_CONSTANTS.CASE_OBJECT_NAME;
    }
}