/**
 * @Author        : Janaswini S
 * @CreatedOn     : March 20, 2020
 * @Purpose       : This component contains Progress indicator of staffs
 **/

import {
  LightningElement,
  track,
  api
} from "lwc";
import * as sharedData from "c/sharedData";
import {
  utils
} from "c/utils";

export default class StaffProgressIndicator extends LightningElement {
  @track selectedStep = "Personnel Section";
  @track selectedStepPrev = "Staff Certification";
  @track PersonnelSection = true;
  @track StaffCertification = false;
  @track StaffTrainings = false;
  @track ClearanceInfo = false;
  @track AssignProgram = false;

  @api openmodel = false;

  //Sundar Adding this code
  @track isSelectStep1 = false;
  @api validatecheckfromprogramdetails = false;

  @api
  get providerid() {
    return sharedData.getProviderId();
  }

  get staffid() {
    return sharedData.getStaffId();
  }

  get isSelectStep4() {
    return this.selectedStep === "Clearance Info";
  }

  @api
  openmodal() {
    ;
    this.openmodel = true;
    this.isSelectStep1 = this.selectedStep === "Personnel Section" ? false : true;
    this.PersonnelSection = true;
    this.AssignProgram = false;
    this.StaffCertification = false;
    this.StaffTrainings = false;
    this.ClearanceInfo = false;
  }

  @api
  closeModal(event) {
    this.openmodel = false;
    event.preventDefault();
    const selectedEvent = new CustomEvent("progressvaluechange", {
      detail: true
    });
    this.dispatchEvent(selectedEvent);
    sharedData.setStaffId("");
    this.selectedStep = "Personnel Section";
    this.PersonnelSection = true;
    this.AssignProgram = false;
    this.StaffCertification = false;
    this.StaffTrainings = false;
    this.ClearanceInfo = false;
  }

  handleNextFromChild() {
    this.selectedStep = "Staff Certification";
    this.PersonnelSection = false;
    this.AssignProgram = false;
    this.StaffCertification = true;
    this.StaffTrainings = false;
    this.ClearanceInfo = false;
    this.isSelectStep1 = true;
  }
  handleNext(event) {
    this.isSelectStep1 = true;
    var getselectedStep = this.selectedStep;
    if (getselectedStep === "Personnel Section") {
      this.isSelectStep1 = false;
      this.template
        .querySelector("c-staff-personal-section")
        .handleSubmit(event);
    } else if (getselectedStep === "Staff Certification") {
      this.selectedStep = "Staff Trainings";
      this.PersonnelSection = false;
      this.AssignProgram = false;
      this.StaffCertification = false;
      this.StaffTrainings = true;
      this.ClearanceInfo = false;
    } else if (getselectedStep === "Staff Trainings") {
      this.selectedStep = "Clearance Info";
      this.PersonnelSection = false;
      this.AssignProgram = false;
      this.StaffCertification = false;
      this.StaffTrainings = false;
      this.ClearanceInfo = true;
    }
  }

  handlePrev() {
    var getselectedStep = this.selectedStep;
    if (getselectedStep === "Staff Certification") {
      this.selectedStep = "Personnel Section";
      this.PersonnelSection = true;
      this.AssignProgram = false;
      this.StaffCertification = false;
      this.StaffTrainings = false;
      this.ClearanceInfo = false;
      this.isSelectStep1 = false;
    } else if (getselectedStep === "Staff Trainings") {
      this.selectedStep = "Staff Certification";
      this.PersonnelSection = false;
      this.AssignProgram = false;
      this.StaffCertification = true;
      this.StaffTrainings = false;
      this.ClearanceInfo = false;
    } else if (getselectedStep === "Clearance Info") {
      this.selectedStep = "Staff Trainings";
      this.PersonnelSection = false;
      this.AssignProgram = false;
      this.StaffCertification = false;
      this.StaffTrainings = true;
      this.ClearanceInfo = false;
    }
  }

  handleFinishFromChild() {
    this.closeModal(event);
  }

  handleFinish(event) {
    this.template.querySelector('c-clearance-information').handleValidation();

  }

  selectStep1() {
    if (this.selectedStep === "Personnel Section") {
      this.PersonnelSection = true;
      this.AssignProgram = false;
      this.StaffCertification = false;
      this.StaffTrainings = false;
      this.ClearanceInfo = false;
    }
  }

  selectStep2(event) {
    if (this.selectedStep === "Staff Certification") {
      this.PersonnelSection = false;
      this.AssignProgram = false;
      this.StaffCertification = true;
      this.StaffTrainings = false;
      this.ClearanceInfo = false;
    }
  }

  selectStep3() {
    if (this.selectedStep === "Staff Trainings") {
      this.PersonnelSection = false;
      this.AssignProgram = false;
      this.StaffCertification = false;
      this.StaffTrainings = true;
      this.ClearanceInfo = false;
    }
  }

  selectStep4() {
    if (this.selectedStep === "Clearance Info") {
      this.PersonnelSection = false;
      this.AssignProgram = false;
      this.StaffCertification = false;
      this.StaffTrainings = false;
      this.ClearanceInfo = true;
    }
  }

  handleValidationCheck(event) {
    if (event.detail.validate) {
      return this.dispatchEvent(utils.toastMessage("Please fill Corporate/State/Federal detials.", "warning"));
    } else {

      this.dispatchEvent(utils.toastMessage("Staff/Board Information details are updated successfully", "success"));
      this.closeModal(event);
    }

  }
}