/**
 * @Author        : Sindhu Venkateswarlu
 * @CreatedOn     : Juy 1, 2020
 * @Purpose       : Activity in monitoring for Complaints module.
 
 **/
import { LightningElement, track, wire } from "lwc";
import { utils } from "c/utils";
import InsertUpdateActivityInfo from "@salesforce/apex/ComplaintsMonitoringActivities.InsertUpdateActivityInfo";
import getActivityValueFromReference from "@salesforce/apex/ComplaintsMonitoringActivities.getActivityValueFromReference";
import UpdateTask from "@salesforce/apex/ComplaintsMonitoringActivities.UpdateTask";
import deleteActivityDetails from "@salesforce/apex/ComplaintsMonitoringActivities.deleteActivityDetails";
import getActivityListFromTask from "@salesforce/apex/ComplaintsMonitoringActivities.getActivityListFromTask";
import editActivityDetails from "@salesforce/apex/ComplaintsMonitoringActivities.editActivityDetails";
import fetchStatusValue from "@salesforce/apex/ComplaintsMonitoringActivities.getActivitySubType";
import { refreshApex } from "@salesforce/apex";
import * as sharedData from "c/sharedData";
import images from "@salesforce/resourceUrl/images";
import { registerListener, unregisterAllListeners } from "c/pubsub";
import { CurrentPageReference } from "lightning/navigation";

//Activity data table
const columns = [
  {
    label: "Activity Name",
    fieldName: "ActivityName__c",
    type: "text"
  },
  {
    label: "Completion Date",
    fieldName: "CompletionDate__c",
    type: "date",
    editable: false,
    typeAttributes: {
      month: "2-digit",
      day: "2-digit",
      year: "numeric"
    }
  },
  {
    label: "Comments",
    fieldName: "Comments__c",
    type: "text",
    initialWidth: 400
  },
  {
    label: "Status",
    fieldName: "Status__c",
    type: "text",

    cellAttributes: {
      class: {
        fieldName: "Status__c"
      }
    }
  },
  {
    label: "Action",
    fieldName: "Id",
    type: "button",
    initialWidth: 100,
    typeAttributes: {
      iconName: "utility:edit_form",
      name: "Edit",
      title: "Edit",
      initialWidth: 20,
      disabled: false,
      iconPosition: "left",
      target: "_self"
    }
  },
  {
    type: "button",
    typeAttributes: {
      iconName: "utility:preview",
      name: "View",
      title: "View",
      initialWidth: 20,
      class: "action-position",
      disabled: false,
      iconPosition: "left",
      target: "_self"
    }
  }
];

export default class ComplaintsMonitoringActivities extends LightningElement {
  //  @track getMonitorId;
  @wire(CurrentPageReference) pageRef;
  @track openmodel = false;
  @track columns = columns;
  @track norecorddisplay = true;
  @track addoreditActivity = {
    ActivityName__c: "",
    CompletionDate__c: "",
    Comments__c: "",
    Status__c: ""
  };
  @track ActivityOptions;
  @track selected = [];
  @track ActivityData;
  @track editmodel = false;
  @track Viewmodel = false;
  @track Status__c;
  @track ListSelectedData;
  refreshActivity;
  @track PageSpinner = true;
  // suggestion
  @track suggestionData;
  activityrowid;
  @track disabledOfComplete;
  // selected data
  @track selectedData = [];
  disableToastMessage = true;
  attachmentIcon = images + "/application-activites.svg";
  @track SupervisorHide = true;
  @track removeActivityError;
  @track saveDisable = false;

  //dynamic MonitoringId.

  get getMonitorId() {
    
    return sharedData.getMonitoringId();
  }
  get getMonitorStatus() {
    
    return sharedData.getMonitoringStatus();
  }
  get getUserProfileName() {
    // return 'Supervisor'
    return sharedData.getUserProfileName();
  }

  ListOfSelected() {
    for (let i = 0; i < this.ListSelectedData.length; i++) {
      this.suggestionData.forEach((item, index) => {
        if (item.value === this.ListSelectedData[i].ActivityName__c) {
          this.selectedData.push(item);
          this.suggestionData.splice(index, 1);
        }
      });
    }
  }

  // fetch Data from reference__C obj for suggested activity
  @wire(getActivityValueFromReference)
  wireAcitivityValue({ data, error }) {
    if (data) {
      let suggestionData = data.map((item) =>
        Object.assign(
          {
            ...item
          },
          {
            label: item.Activity__c,
            value: item.Activity__c
          }
        )
      );
      this.suggestionData = suggestionData;
    }
  }
  connectedCallback() {
    registerListener(
      "RefreshActivityPage",
      this.handleRefreshActivityPage,
      this
    );
  }
  handleRefreshActivityPage(val) {
    setTimeout(() => {
      refreshApex(this.refreshActivity);
    }, 1000);
  }
  @wire(getActivityListFromTask, {
    ActivityData: "$getMonitorId"
  })
  wireListSelectedValue(result) {
    this.refreshActivity = result;
    this.ListSelectedData = result.data;
    if (result.data == 0) {
      this.norecorddisplay = false;
    } else {
      this.norecorddisplay = true;
    }
    if (this.getUserProfileName === "Supervisor") {
      this.SupervisorHide = false;
    }
    setTimeout(() => {
      this.PageSpinner = false;
    }, 1000);
  }
  //Activity Modal fuction
  ActivityOpenModal() {
    if (this.getMonitorStatus === "Draft") {
      this.openmodel = true;
      this.ListOfSelected();
    } else {
      this.dispatchEvent(
        utils.toastMessage(
          "Cannot add activity for completed monitoring",
          "warning"
        )
      );
    }
    var disabledOfComplete = [...this.ListSelectedData];
    disabledOfComplete = disabledOfComplete.filter(function (item) {
      return item.Status__c == "Completed";
    });
    this.removeActivityError = disabledOfComplete;
  }
  //remove function from Right
  handleRightSelection(event) {
    // new code
    let targetValue = event.currentTarget.dataset.value;
    this.suggestionData.forEach((item, index) => {
      if (item.value === targetValue) {
        this.selectedData.push(item);
        this.suggestionData.splice(index, 1);
      }
    });
  }
  //remove function from Left
  handleLeftSelection(event) {
    var disabledComplete = [...this.ListSelectedData];
    disabledComplete = disabledComplete.filter(function (item) {
      return item.ActivityName__c == event.currentTarget.dataset.value;
    });
    let targetValue = event.currentTarget.dataset.value;
    this.selectedData.forEach((item, index) => {
      if (item.value === targetValue) {
        if (
          disabledComplete.length == 0 ||
          disabledComplete[0].Status__c !== "Completed"
        ) {
          this.suggestionData.push(item);
          this.selectedData.splice(index, 1);
        } else {
          this.dispatchEvent(
            utils.toastMessage("Cannot Remove Completed Activity", "warning")
          );
        }
      }
    });
  }
  //Add All And Remove All with DisableComplete function.
  AddAllActivity() {
    this.selectedData = this.selectedData.concat(this.suggestionData);
    this.suggestionData = [];
  }

  RemoveAllActivity() {
    if (this.removeActivityError.length !== 0) {
      this.dispatchEvent(
        utils.toastMessage(
          "Cannot Remove All With Completed Activity",
          "warning"
        )
      );
    } else {
      this.suggestionData = this.suggestionData.concat(this.selectedData);
      this.selectedData = [];
    }
  }

  closeModal() {
    this.openmodel = false;
  }

  handleCloseModal() {
    this.editmodel = false;
    this.Viewmodel = false;
  }

  @wire(fetchStatusValue, {
    objInfo: {
      sobjectType: "Task"
    },
    picklistFieldApi: "Status__c"
  })
  wiredStatusDetails({ error, data }) {
    if (data) {
      this.StatusOptions = data;
    } else if (error) {
    }
  }

  // Save method For Dual list box

  saveMethod() {
    this.saveDisable = false;
    this.disableToastMessage = true;
    //Assigning global variable to local array as it will not tamper the global data
    let dataToInsert = [...this.selectedData];
    let dataToDelete = [...this.suggestionData];
    let dataFromDB = [...this.ListSelectedData];

    // Calculcating the datas that are to be inserted in to Task object
    dataToInsert = dataToInsert.filter(function (cv) {
      return !dataFromDB.find(function (e) {
        return e.ActivityName__c == cv.Activity__c;
      });
    });

    // Calculcating the datas that are to be deleted from task object
    dataToDelete = dataFromDB.filter(function (cv) {
      return dataToDelete.find(function (e) {
        return e.Activity__c == cv.ActivityName__c;
      });
    });

    // Insert array length greater than zero then insert else no insertion
    if (dataToInsert.length > 0) {
      this.saveDisable = true;
      InsertUpdateActivityInfo({
        strMonitor: JSON.stringify(dataToInsert),
        strMonitoringId: this.getMonitorId,
        strAction: "Insert"
      })
        .then((result) => {
          //If Success show toast
          this.handleSucessMessage(result);
          this.saveDisable = false;
        })
        .catch((error) => {
          if (errors) {
  
            let error = JSON.parse(errors.body.message);
            const { title, message, errorType } = error;
            this.dispatchEvent(
              utils.toastMessageWithTitle(title, message, errorType)
            );
          } else {
            this.dispatchEvent(
              utils.toastMessage(CJAMS_CONSTANTS.ERROR_TOASTMESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
            );
          }
        });
    } // If

    // Delete array length greater than zero then delete else no deletion of records
    if (dataToDelete.length > 0) {
      InsertUpdateActivityInfo({
        strMonitor: JSON.stringify(dataToDelete),
        strMonitoringId: this.getMonitorId,
        strAction: "Delete"
      })
        .then((result) => {
          this.handleSucessMessage(result);
        })
        .catch((error) => {
          if (errors) {
  
            let error = JSON.parse(errors.body.message);
            const { title, message, errorType } = error;
            this.dispatchEvent(
              utils.toastMessageWithTitle(title, message, errorType)
            );
          } else {
            this.dispatchEvent(
              utils.toastMessage(CJAMS_CONSTANTS.ERROR_TOASTMESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
            );
          }
        });
    }
    //toastMessage for save without changes
    if (dataToInsert.length == 0) {
      this.handleSucessMessage();
    }
  }

  handleSucessMessage(result) {
    if (this.disableToastMessage) {
      this.dispatchEvent(
        utils.toastMessage("Activity Updated Successfully", "success")
      );
      this.closeModal();
    }
    this.disableToastMessage = false;
    return refreshApex(this.refreshActivity);
  }
  //Edit And Save For Data List
  handleRowAction(event) {
    this.activityrowid = event.detail.row.Id;
    if (event.detail.action.name == "Edit") {
      if (this.getMonitorStatus !== "Completed") {
        this.editmodel = true;
        editActivityDetails({
          editactivitydetails: this.activityrowid
        })
          .then((result) => {
            this.addoreditActivity = result[0];
            this.addoreditActivity.CompletionDate__c =
              result[0].CompletionDate__c;
            this.addoreditActivity.Status__c = result[0].Status__c;
            this.addoreditActivity.Comments__c = result[0].Comments__c;
          })
          .catch((error) => {
            this.dispatchEvent(
              utils.toastMessage("Error in fetching edit record", "error")
            );
          });
      } else {
        this.dispatchEvent(
          utils.toastMessage(
            "Cannot edit activity for completed monitoring",
            "warning"
          )
        );
      }
    } else if (event.detail.action.name == "View") {
      this.Viewmodel = true;
      editActivityDetails({
        editactivitydetails: this.activityrowid
      })
        .then((result) => {
          this.addoreditActivity = result[0];
          this.addoreditActivity.CompletionDate__c =
            result[0].CompletionDate__c;
          this.addoreditActivity.Status__c = result[0].Status__c;
          this.addoreditActivity.Comments__c = result[0].Comments__c;
        })
        .catch((error) => {
          this.dispatchEvent(
            utils.toastMessage("Error in fetching edit record", "error")
          );
        });
    }
    // else if (event.detail.action.name == "Delete") {
    //     this.handleDelete();
    // }
  }

  handleDelete() {
    deleteActivityDetails({
      deleteActivityDetails: this.activityrowid
    })
      .then((result) => {
        this.dispatchEvent(
          utils.toastMessage("Record successfully deleted", "success")
        );
        return refreshApex(this.refreshActivity);
      })
      .catch((error) => {
        this.dispatchEvent(
          utils.toastMessage("Error in deleting record", "error")
        );
      });
  }
  handleChange(event) {
    if (event.target.name == "CompletionDate__c") {
      this.addoreditActivity.CompletionDate__c = event.target.value;
    } else if (event.target.name == "Status__c") {
      this.addoreditActivity.Status__c = event.target.value;
    } else if (event.target.name == "Comments__c") {
      this.addoreditActivity.Comments__c = event.target.value;
    }
  }

  saveEditActivity() {
    let objTask = {
      sobjectType: "Task"
    };
    objTask.Id = this.activityrowid;
    var inp = [
      ...this.template.querySelectorAll("lightning-input"),
      ...this.template.querySelectorAll("lightning-combobox")
    ];
    inp.forEach((element) => {
      switch (element.name) {
        case "CompletionDate":
          objTask.CompletionDate__c = element.value;
          break;
        case "Status":
          objTask.Status__c = element.value;
          break;
        case "Comments":
          objTask.Comments__c = element.value;
          break;
      }
    });
    UpdateTask({
      objSobjecttoUpdateOrInsert: objTask
    })
      .then((result) => {
        this.dispatchEvent(
          utils.toastMessage(
            "Monitoring activity has been updated successfully",
            "success"
          )
        );
        this.editmodel = false;
        return refreshApex(this.refreshActivity);
      })
      .catch((error) => {
        this.dispatchEvent(
          utils.toastMessage("Error in creating record", "error")
        );
      });
  }
}
