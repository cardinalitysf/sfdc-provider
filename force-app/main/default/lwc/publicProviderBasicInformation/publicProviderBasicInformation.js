/**
 * @Author        : M.Vijayaraj
 * @CreatedOn     : May 22, 2020
 * @Purpose       : This component for Public provider Basic Information
 **/
import {
  LightningElement,
  track,
  wire,
  api
} from "lwc";
import images from "@salesforce/resourceUrl/images";
import fatchPickListValuePPBasicInfo from "@salesforce/apex/PublicProviderBasicInformation.getPickListValuespublicproviderBasicInfo";
import getPickListValuesAgeGroup from "@salesforce/apex/PublicProviderBasicInformation.getPickListValuesAgeGroup";
import getPickListValuesAgeGroupKinship from "@salesforce/apex/PublicProviderBasicInformation.getPickListValuesAgeGroupKinship";
import getReferenceListForKinship from "@salesforce/apex/PublicProviderBasicInformation.getReferenceListForkinship";
import getReferenceListForResourceHome from "@salesforce/apex/PublicProviderBasicInformation.getReferenceListForResourceHome";
import saveCaseRecord from "@salesforce/apex/PublicProviderBasicInformation.updateOrInsertSOQL";
import getIdAfterInsertSOQL from "@salesforce/apex/PublicProviderHeader.getIdAfterInsertSOQL";
import getCaseDetails from "@salesforce/apex/PublicProviderBasicInformation.getCaseDetails";
let myvalue;
import {
  loadStyle,
  loadScript
} from "lightning/platformResourceLoader";
import FullCalendarJS from "@salesforce/resourceUrl/Slider";

import {
  utils
} from "c/utils";
import * as sharedData from "c/sharedData";
import {
  CJAMS_CONSTANTS
} from "c/constants";

export default class PublicProviderBasicInformation extends LightningElement {
  @track PublicProviderPPBasicInfo;
  @track ReferenceKinshipOptions;
  @track ReferenceResourceHomeOptions;
  @track ageGroupData;
  @track programType;
  @track inquirySource;
  @track Capacity;
  @track Status;

  //Disabled section Start
  @track isDisabledkinship = false;
  @track isDisabledresourceHome = false;
  @track PageDefaultSave = true;
  //Disabled section End

  @track ageGroupDatakinship;
  @track program;
  @track getAccountId;
  @track ProgramResourceHome;
  @track ProgramKinship;
  // @track CaseId='5000w000001NXD8AAO'

  //Program wise fetching for resource Home Start
  @track ResourceHomeProgramType;
  @track ResourceHomeInquirySource;
  @track ResourceHomeCapacity = 0;
  @track ResourceHomeAgeGroup;
  //Program wise fetching for resource Home End

  //Program wise fetching for Kinship Start
  @track KinshipProgramType;
  @track KinshipAgeGroup;
  @track KinshipInquirySource;
  @track KinshipCapacity = 0;

  //Program wise fetching for Kinship End

  //Multipicklist track values start
  @track pillSingleValueForPtype;
  @track pillSingleValueForPtypeCount;
  @track activeForPType = false;
  @track pillValueForPtype = [];
  @track pillSingleValueCondition = true;
  //Multipicklist track values end

  //Multipicklist track values kinship start
  @track pillSingleValueForPtypekinship;
  @track pillSingleValueForPtypeCountkinship;
  @track activeForPTypekinship = false;
  @track pillValueForPtypekinship = [];
  @track pillSingleValueConditionkinship = true;
  //Multipicklist track values kinship end

  //Case Details Fetched Start
  @track programType;
  @track Agegroup;
  @track InquirySource;
  @track Capacity;
  //Case Details Fetched End

  @track approvalStatus;
  @track Spinner = true;

  // Box click to change image normal to highlight Declaration Start
  resourceIcon = images + "/pp-resourcehome.svg";
  kinshipIcon = images + "/pp-dna.svg";
  resourceIconhightLight = images + "/pp-highlightresourcehome.svg";
  kinshipIconhightLight = images + "/pp-highlighdna.svg";
  // Box click to change image normal to highlight Declaration End

  //Get InquirySource  using @wire Start
  @wire(fatchPickListValuePPBasicInfo, {
    objInfoPPBasicInfo: {
      sobjectType: "Case"
    },
    picklistFieldApiPPBasicInfo: "InquirySource__c"
  })
  PPBasicInfo({
    error,
    data
  }) {
    /*eslint-disable*/

    if (data) {
      this.PublicProviderPPBasicInfo = data;
    } else if (error) { }
  }
  //Get InquirySource  using @wire End

  //Get ResourceHome ProgramType  using @wire Start
  @wire(getReferenceListForResourceHome, {
    objInfo: {
      sobjectType: "ReferenceValue__c"
    },
    pickListFieldApi: "RefValue__c"
  })
  wiredForResourceHomeDetails({
    error,
    data
  }) {
    if (data) {
      let resourceHome = data.map((item) => {
        let a = Object.assign({}, item);
        a.label = item.RefValue__c;
        a.value = item.RefValue__c;
        return a;
      });
      this.ReferenceResourceHomeOptions = resourceHome;
    } else if (error) { }
  }
  //Get ResourceHome ProgramType  using @wire End

  //Get Kinship ProgramType  using @wire Start
  @wire(getReferenceListForKinship, {
    objInfo: {
      sobjectType: "ReferenceValue__c"
    },
    pickListFieldApi: "RefValue__c"
  })
  wiredForKinshipDetails({
    error,
    data
  }) {
    if (data) {
      let kinship = data.map((item) => {
        let b = Object.assign({}, item);
        b.label = item.RefValue__c;
        b.value = item.RefValue__c;
        return b;
      });
      this.ReferenceKinshipOptions = kinship;
    } else if (error) { }
  }
  //Get Kinship ProgramType  using @wire End

  //Get AgeGroup for ResourceHome using @wire Start
  @wire(getPickListValuesAgeGroup, {
    objInfoAgeGroup: {
      sobjectType: "Account"
    },
    picklistFieldApiAgeGroup: "AgeGroup__c"
  })
  ageGroup({
    error,
    data
  }) {
    /*eslint-disable*/

    if (data) {
      this.ageGroupData = data;
      let certificationTypeOptions = [];
      data.forEach((key) => {
        certificationTypeOptions.push({
          label: key.label,
          value: key.value + "a"
        });
      });
      this.ageGroupDatakinship = certificationTypeOptions;
    } else if (error) { }
  }

  boxbordercolorchange() {

    if (
      this.approvalStatus == CJAMS_CONSTANTS.APPLICATION_REVIEW_STATUS_APPROVED ||
      this.approvalStatus == CJAMS_CONSTANTS.APPLICATION_REVIEW_STATUS_REJECTED
    ) {
      this.isDisabledresourceHome = true;
      this.isDisabledkinship = true;
      this.template.querySelector(".box-col1").classList.add("disabledbutton");
    }

    this.pillValueForPtypekinship = [];
    if (this.pillValueForPtype.length == 0) {
      this.checkboxunchecked();
    }

    this.template.querySelector(
      ".iconpadbot"
    ).src = this.resourceIconhightLight;
    this.template.querySelector(".iconpadbot1").src = this.kinshipIcon;
    this.template
      .querySelector(".box-col1")
      .classList.add("boxbordercolorchange");
    this.template
      .querySelector(".box-col3")
      .classList.remove("boxbordercolorchange");

    if (this.Status == CJAMS_CONSTANTS.APPLICATION_REVIEW_STATUS_APPROVED || this.Status == CJAMS_CONSTANTS.APPLICATION_REVIEW_STATUS_REJECTED) {
      this.isDisabledkinship = true;
      this.isDisabledresourceHome = true;
    } else {
      this.isDisabledkinship = true;
      this.isDisabledresourceHome = false;
    }

    this.PageDefaultSave = false;
    this.program = CJAMS_CONSTANTS.RESOURCEHOME_PROGRAM;

    this.template
      .querySelector(".my_slider")
      .classList.remove("disabledbutton");
    this.template.querySelector(".my_slider2").classList.add("disabledbutton");

    //Data reset for Kinship Start
    this.template.querySelector(".ptyperesetKinship").value = "";
    this.template.querySelector(".rsourceresetforKinship").value = "";
    var slider2 = this.template.querySelector("div.my_slider2");
    var display2 = this.template.querySelector("lightning-input.amount2");
    this.loadSlider(slider2, display2);
    this.template.querySelector(".amount2").value = 1;
    //Data reset for Kinship End
  }

  boxbordercolorchangeForkinship() {
    if (
      this.approvalStatus == CJAMS_CONSTANTS.APPLICATION_REVIEW_STATUS_APPROVED ||
      this.approvalStatus == CJAMS_CONSTANTS.APPLICATION_REVIEW_STATUS_REJECTED
    ) {
      this.isDisabledresourceHome = true;
      this.isDisabledkinship = true;
      this.template.querySelector(".box-col3").classList.add("disabledbutton");
    }

    this.pillValueForPtype = [];
    if (this.pillvalueForPTypekinship.length == 0) {
      this.checkboxunchecked();
    }

    this.template.querySelector(
      ".iconpadbot1"
    ).src = this.kinshipIconhightLight;
    this.template.querySelector(".iconpadbot").src = this.resourceIcon;
    this.template
      .querySelector(".box-col3")
      .classList.add("boxbordercolorchange");
    this.template
      .querySelector(".box-col1")
      .classList.remove("boxbordercolorchange");

    if (this.Status == CJAMS_CONSTANTS.APPLICATION_REVIEW_STATUS_APPROVED || this.Status == CJAMS_CONSTANTS.APPLICATION_REVIEW_STATUS_REJECTED) {
      this.isDisabledkinship = true;
      this.isDisabledresourceHome = true;
    } else {
      this.isDisabledkinship = false;
      this.isDisabledresourceHome = true;
    }
    this.PageDefaultSave = false;
    this.program = CJAMS_CONSTANTS.KINSHIP_PROGRAM;

    this.template.querySelector(".my_slider").classList.add("disabledbutton");
    this.template
      .querySelector(".my_slider2")
      .classList.remove("disabledbutton");

    //Data reset for Resource Home Start
    this.template.querySelector(".ptyperesetforResource").value = "";
    this.template.querySelector(".rsourceresetforResource").value = "";
    // this.template.querySelector(".agegroupresetforResource").value = "";

    var slider = this.template.querySelector("div.my_slider");
    var display = this.template.querySelector("lightning-input.amount");
    this.loadSlider(slider, display);
    this.template.querySelector(".amount").value = 1;
    //Data reset for Resource Home End
  }

  handleonChange1(event) {
    if (event.target.name == "ProgramType") {
      this.programType = event.target.value;
    }
  }

  handleonChange2(event) {
    if (event.target.name == "inquirySource") {
      this.inquirySource = event.target.value;
    }
  }

  //Save Functionality

  handleSave() {
    if (!this.CommunicationValue) {
      return this.dispatchEvent(
        utils.toastMessage("Please Enter Communication value", "Warning")
      );
    }
    if (!this.Jurisdiction) {
      return this.dispatchEvent(
        utils.toastMessage("Please Enter Jurisdiction value", "Warning")
      );
    }
    if (!this.programType) {
      return this.dispatchEvent(
        utils.toastMessage("Please Enter Program Type value", "Warning")
      );
    }
    if (this.program == CJAMS_CONSTANTS.RESOURCEHOME_PROGRAM) {
      if (!this.getStateEmployeeValues()) {
        return this.dispatchEvent(
          utils.toastMessage("Please Enter AgeGroup value", "Warning")
        );
      }
    } else {
      if (!this.getStateEmployeeValueskinship()) {
        return this.dispatchEvent(
          utils.toastMessage("Please Enter AgeGroup value", "Warning")
        );
      }
    }

    if (!this.inquirySource) {
      return this.dispatchEvent(
        utils.toastMessage("Please Enter Referral Source value", "Warning")
      );
    }

    let myobjcaseAccount = {
      sobjectType: "Account"
    };
    myobjcaseAccount.Jurisdiction__c = this.Jurisdiction;

    if (this.program == CJAMS_CONSTANTS.RESOURCEHOME_PROGRAM) {
      myobjcaseAccount.Capacity__c = this.template.querySelector(
        ".amount"
      ).value;
      myobjcaseAccount.AgeGroup__c = this.getStateEmployeeValues();
    } else {
      myobjcaseAccount.Capacity__c = this.template.querySelector(
        ".amount2"
      ).value;
      myobjcaseAccount.AgeGroup__c = this.getStateEmployeeValueskinship();
    }

    if (myobjcaseAccount.Capacity__c <= 0) {
      return this.dispatchEvent(
        utils.toastMessage("Please Enter Capacity value", "Warning")
      );
    }

    if (myobjcaseAccount.Capacity__c > 10) {
      return this.dispatchEvent(
        utils.toastMessage("Exceeded Capacity Value", "Warning")
      );
    }

    myobjcaseAccount.Name = "Public Provider";
    myobjcaseAccount.ProviderType__c = "Public";

    getIdAfterInsertSOQL({
      objIdSobjecttoUpdateOrInsert: myobjcaseAccount
    })
      .then((result) => {
        this.getAccountId = result;
        sharedData.setProviderId(this.getAccountId);
        this.caseRecordSaveMethod();
      })
      .catch((errors) => {
        return this.dispatchEvent(utils.handleError(errors));
      });
  }

  caseRecordSaveMethod() {
    let myobjcase = {
      sobjectType: "Case"
    };
    myobjcase.ProgramType__c = this.programType;
    myobjcase.InquirySource__c = this.inquirySource;
    myobjcase.Id = this.CaseId;
    myobjcase.Origin = this.CommunicationValue;
    myobjcase.ReceivedDate__c = this.ReceivedDateValue;
    myobjcase.Program__c = this.program;
    myobjcase.Status = CJAMS_CONSTANTS.PROVIDER_APP_FINAL_STATUS_PENDING;
    myobjcase.AccountId = this.getAccountId;

    saveCaseRecord({
      objSobjecttoUpdateOrInsert: myobjcase
    })
      .then((result) => {
        this.dispatchEvent(
          utils.toastMessage("Basic Information saved successfully", "success")
        );
        this.providerBasicInfoCaseDetails();
      })
      .catch((errors) => {
        return this.dispatchEvent(utils.handleError(errors));
      });
  }


  defer() {
    if ($) {
      this.providerBasicInfoCaseDetails();
    } else {
      setTimeout(function () {
        this.defer()
      }, 50);
    }
  }

  providerBasicInfoCaseDetails() {
    getCaseDetails({
      casedetails: this.CaseId
    }).then((result) => {
      this.Spinner = false;
      // this.boxbordercolorchange();

      if (result.length > 0 && result[0].Program__c != undefined) {


        if (result[0].Program__c == CJAMS_CONSTANTS.RESOURCEHOME_PROGRAM) {
          this.ResourceHomeProgramType = result[0].ProgramType__c;
          this.ResourceHomeAgeGroup = result[0].Account.AgeGroup__c;
          this.ResourceHomeInquirySource = result[0].InquirySource__c;
          this.ResourceHomeCapacity = result[0].Account.Capacity__c;
          this.Id = result[0].Id;
          this.Status = result[0].Status;
          this.programType = result[0].ProgramType__c;
          this.inquirySource = result[0].InquirySource__c;

          var sliderel = this.template.querySelector("div.my_slider");
          $(sliderel).slider("value", this.ResourceHomeCapacity);

          // this.template.querySelector(".iconpadbot").src = this.resourceIconhightLight;
          // this.template.querySelector(".iconpadbot1").src = this.kinshipIcon;
          // this.template.querySelector(".box-col1").classList.add("boxbordercolorchange");
          // this.template.querySelector(".box-col3").classList.remove("boxbordercolorchange");
          this.boxbordercolorchange();
        } else if (result[0].Program__c == CJAMS_CONSTANTS.KINSHIP_PROGRAM) {
          this.KinshipProgramType = result[0].ProgramType__c;
          this.KinshipAgeGroup = result[0].Account.AgeGroup__c;
          this.KinshipInquirySource = result[0].InquirySource__c;
          this.KinshipCapacity = result[0].Account.Capacity__c;
          this.Id = result[0].Id;
          this.Status = result[0].Status;
          this.programType = result[0].ProgramType__c;
          this.inquirySource = result[0].InquirySource__c;

          var slider2 = this.template.querySelector("div.my_slider2");
          $(slider2).slider("value", this.KinshipCapacity);
          // this.template.querySelector(".iconpadbot1").src = this.kinshipIconhightLight;
          // this.template.querySelector(".iconpadbot").src = this.resourceIcon;
          // this.template.querySelector(".box-col3").classList.add("boxbordercolorchange");
          // this.template.querySelector(".box-col1").classList.remove("boxbordercolorchange");

          this.boxbordercolorchangeForkinship();
        }
        if (
          result[0].Status != undefined &&
          (result[0].Status == CJAMS_CONSTANTS.APPLICATION_REVIEW_STATUS_APPROVED || result[0].Status == CJAMS_CONSTANTS.APPLICATION_REVIEW_STATUS_REJECTED)
        ) {
          this.approvalStatus = result[0].Status;
          
          this.template
            .querySelector(".box-col1")
            .classList.add("disabledbutton");
          this.template
            .querySelector(".box-col3")
            .classList.add("disabledbutton");

          // this.statusWisePageFreeze=true;
          this.isDisabledresourceHome = true;
          this.isDisabledkinship = true;
          this.PageDefaultSave = true;
        }
      } else {
        this.boxbordercolorchange();
      }

      this.programTypeCheckBoxSelectUnselectDuringPageLoad();
      this.programTypeCheckBoxSelectUnselectDuringPageLoadKinship();
    })
      .catch((errors) => {
        return this.dispatchEvent(utils.handleError(errors));
      });
  }

  //Code start multi select for AgeGroup
  get pillvalueForPType() {
    if (this.pillValueForPtype.length > 1) {
      this.pillSingleValueForPtype = this.pillValueForPtype[0].value;
      this.pillSingleValueCondition = false;
      this.pillSingleValueForPtypeCount = this.pillValueForPtype.length - 1;

      return this.pillSingleValueForPtype;
    } else {
      this.pillSingleValueCondition = true;
      return this.pillValueForPtype;
    }
  }

  handleMouseOutButton(evr) {
    this.activeForPType = false;
  }
  get toggledivForPtype() {
    return this.pillValueForPtype.length == 0 ? false : true;
  }
  handlePType(evt) {
    this.programTypeCheckBoxSelectUnselectDuringPageLoad();
    this.activeForPType = this.activeForPType ? false : true;
  }

  handleRemove(evt) {
    this.pillValueForPtype = this.remove(
      this.pillValueForPtype,
      "value",
      evt.target.label
    );
    let i;
    let checkboxes = this.template.querySelectorAll("[data-id=checkbox]");
    checkboxes.forEach((element) => {
      if (element.value == evt.target.label) {
        if (element.checked) {
          element.checked = false;
        }
      }
    });
    this.activeForPType = true;
  }
  get tabClassForPType() {
    return this.activeForPType ?
      "slds-popover slds-popover_full-width slds-popover_show" :
      "slds-popover slds-popover_full-width slds-popover_hide";
  }
  handleclickofPTypeCheckBox(evt) {
    if (evt.target.checked) {
      this.pillValueForPtype.push({
        value: evt.target.value
      });
    } else {
      this.pillValueForPtype = this.remove(
        this.pillValueForPtype,
        "value",
        evt.target.value
      );
    }
    evt.target.checked != evt.target.checked;
  }
  remove(array, key, value) {
    const index = array.findIndex((obj) => obj[key] === value);
    return index >= 0 ? [...array.slice(0, index), ...array.slice(index + 1)] :
      array;
  }

  getStateEmployeeValues() {
    var sArray = [];
    this.pillValueForPtype.forEach(function (item) {
      sArray.push(item.value);
    });
    return sArray.join(";");
  }

  programTypeCheckBoxSelectUnselectDuringPageLoad() {
    let localObject = [];
    if (this.ResourceHomeAgeGroup) {
      this.ResourceHomeAgeGroup.split(/\s*;\s*/).forEach(function (myString) {
        localObject.push({
          value: myString
        });
      });

      var inp = [...this.template.querySelectorAll("[data-id=checkbox]")];
      inp.forEach((element) => {
        localObject.forEach(function (item) {
          if (item.value == element.value) element.checked = true;
        });
      });
      this.pillValueForPtype = localObject;
    }
  }

  checkboxunchecked() {
    var inp = [...this.template.querySelectorAll("[data-id=checkbox]")];
    inp.forEach((element) => {
      element.checked = false;
    });
  }

  //Code end multi select for AgeGroup

  //Code start multi select for AgeGroup in kinship
  get pillvalueForPTypekinship() {
    if (this.pillValueForPtypekinship.length > 1) {
      this.pillSingleValueForPtypekinship = this.pillValueForPtypekinship[0].value;
      this.pillSingleValueConditionkinship = false;
      this.pillSingleValueForPtypeCountkinship =
        this.pillValueForPtypekinship.length - 1;

      return this.pillSingleValueForPtypekinship;
    } else {
      this.pillSingleValueConditionkinship = true;
      return this.pillValueForPtypekinship;
    }
  }

  handleMouseOutButtonkinship(evr) {
    this.activeForPTypekinship = false;
  }
  get toggledivForPtypekinship() {
    return this.pillValueForPtypekinship.length == 0 ? false : true;
  }
  handlePTypekinship(evt) {
    this.programTypeCheckBoxSelectUnselectDuringPageLoadKinship();
    this.activeForPTypekinship = this.activeForPTypekinship ? false : true;
  }

  handleRemovekinship(evt) {
    this.pillValueForPtypekinship = this.remove(
      this.pillValueForPtypekinship,
      "value",
      evt.target.label
    );
    let i;
    let checkboxes = this.template.querySelectorAll("[data-id=checkbox]");
    checkboxes.forEach((element) => {
      if (element.value == evt.target.label) {
        if (element.checked) {
          element.checked = false;
        }
      }
    });
    this.activeForPTypekinship = true;
  }
  get tabClassForPTypekinship() {
    return this.activeForPTypekinship ?
      "slds-popover slds-popover_full-width slds-popover_show" :
      "slds-popover slds-popover_full-width slds-popover_hide";
  }
  handleclickofPTypeCheckBoxkinship(evt) {
    if (evt.target.checked) {
      this.pillValueForPtypekinship.push({
        value: evt.target.value
      });
    } else {
      this.pillValueForPtypekinship = this.remove(
        this.pillValueForPtypekinship,
        "value",
        evt.target.value
      );
    }
    evt.target.checked != evt.target.checked;
  }

  getStateEmployeeValueskinship() {
    var sArray = [];
    this.pillValueForPtypekinship.forEach(function (item) {
      sArray.push(item.value);
    });
    return sArray.join(";");
  }

  programTypeCheckBoxSelectUnselectDuringPageLoadKinship() {
    let localObject = [];
    if (this.KinshipAgeGroup) {
      this.KinshipAgeGroup.split(/\s*;\s*/).forEach(function (myString) {
        localObject.push({
          value: myString
        });
      });

      var inp = [...this.template.querySelectorAll("[data-id=checkbox]")];
      inp.forEach((element) => {
        localObject.forEach(function (item) {
          if (item.value == element.value) element.checked = true;
        });
      });
      this.pillValueForPtypekinship = localObject;
    }
  }

  //Code end multi select for AgeGroup in kinship

  get CommunicationValue() {
    return sharedData.getCommunicationValue();
  }

  get CaseId() {
    return sharedData.getCaseId();
    //return "5000w000001NkJZAA0";
  }

  get ReceivedDateValue() {
    return sharedData.getReceivedDateValue();
  }

  get Jurisdiction() {
    return sharedData.getJurisdiction();
  }

  // SLider

  fullCalendarJsInitialised = false;
  _textvalue;

  get textvalue() {
    return myvalue;
  }
  set textvalue(value) {
    this._textvalue = value;
  }
  renderedCallback() {
    // Performs this operation only on first render
    if (this.fullCalendarJsInitialised) {
      return;
    }
    this.fullCalendarJsInitialised = true;

    Promise.all([
      // First step: load FullCalendar core
      loadStyle(this, FullCalendarJS + "/jquery-ui.css"),
      loadScript(this, FullCalendarJS + "/jquery.min.js")
    ])
      .then(() => {
        this.textvalue = "2";
        // Second step: Load the plugins in a new promise
        Promise.all([
          loadScript(this, FullCalendarJS + "/jquery-ui.min.js")
        ]).then(() => {
          // Third step: calls your calendar builder once the plugins have been also loaded
          //  this.initialiseFullCalendarJs();
          var sliderel = this.template.querySelector("div.my_slider");
          var displayel = this.template.querySelector("lightning-input.amount");
          this.loadSlider(sliderel, displayel);

          var slider2 = this.template.querySelector("div.my_slider2");
          var display2 = this.template.querySelector("lightning-input.amount2");
          this.loadSlider(slider2, display2);
          this.defer();

        });
      })
      .catch((errors) => {
        return this.dispatchEvent(utils.handleError(errors));
      });
  }

  loadSlider(sliderel, displayel) {
    $(sliderel)
      .slider({
        value: 1,
        min: 1,
        max: 10,
        step: 1,
        orientation: "horizontal",
        range: "min",
        slide: function (event, ui) {
          $(displayel).val(ui.value);
        }
      })
      .each(function () {
        var opt = $(this).data().uiSlider.options;
        var vals = opt.max - opt.min;
        var spacing = 100 / (opt.max - 1);
        for (var i = 0; i <= vals; i++) {
          var el = $("<label>" + (i + 1) + "</label>")
            .css("left", (i / vals) * 100 + "%")
            .css("position", "absolute")
            .css("width", "20px")
            .css("margin-left", "-10px")
            .css("text-align", "center")
            .css("margin-top", "20px");
          $(sliderel).append(el);
          $('<span class="ui-slider-tick-mark"></span>')
            .css("left", spacing * i + "%")
            .css("display", "inline-block")
            .css("width", "2px")
            .css("background", "#CCC")
            .css("height", "7px")
            .css("position", "absolute")
            .css("top", "10px")
            .appendTo($(sliderel));
        }
      });
  }

  handleChange(evt) {
    if (evt.currentTarget.value > 10)
      return this.dispatchEvent(
        utils.toastMessage("exceeded Capacity Value", "Warning")
      );
    var sliderel = this.template.querySelector("div.my_slider");
    $(sliderel).slider("value", evt.currentTarget.value);
  }

  handleChange1(evt1) {
    if (evt1.currentTarget.value > 10) {
      return this.dispatchEvent(
        utils.toastMessage("exceeded Capacity Value", "Warning")
      );
    } else {
      var slider2 = this.template.querySelector("div.my_slider2");
      $(slider2).slider("value", evt1.currentTarget.value);
    }
  }

  PageRedirecttoDashBoard() {
    window.location.reload();
  }
}