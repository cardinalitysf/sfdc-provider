import {
    LightningElement,
    track

} from 'lwc';
import * as referralsharedDatas from "c/sharedData";
import {
    CJAMS_CONSTANTS
} from 'c/constants';


export default class PublicProvidersReconsiderationNarrative extends LightningElement {
    
    get reconsiderationID() {
        return referralsharedDatas.getReconsiderationId();
    //return 'a0V9D000000axtYUAQ';
    }


    get sobjectName() {
      
    // return CJAMS_CONSTANTS.RECONSIDERATION_OBJECT_NAME;
    return 'Reconsideration__c';
    }
}