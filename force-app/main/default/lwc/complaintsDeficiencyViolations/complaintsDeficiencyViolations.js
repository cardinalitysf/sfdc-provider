/**
 * @Author        : G sathishkumar ,
 * @CreatedOn     : July 01,2020
 * @Purpose       : ComplaintsDeficiencyViolations.
 **/

import { LightningElement, track, wire, api } from "lwc";
import getDeficiencyViolation from "@salesforce/apex/ComplaintsDeficiencyViolations.getDeficiencyViolation";
import getDeficiencyCase from "@salesforce/apex/ComplaintsDeficiencyViolations.getDeficiencyCase";
import insertUpdateDeficiencyViolationsDetails from "@salesforce/apex/ComplaintsDeficiencyViolations.insertUpdateDeficiencyViolationsDetails";
import editDeficiencyViolationDetails from "@salesforce/apex/ComplaintsDeficiencyViolations.editDeficiencyViolationDetails";

import Deficiency__c_OBJECT from "@salesforce/schema/Deficiency__c";
import { getPicklistValuesByRecordType } from "lightning/uiObjectInfoApi";
import { getObjectInfo } from "lightning/uiObjectInfoApi";
import { utils } from "c/utils";
import * as sharedData from "c/sharedData";
import images from "@salesforce/resourceUrl/images";
import { CJAMS_CONSTANTS} from "c/constants";

// const actions = [
//          { label: "View", name: "View", iconName: "utility:preview", target: "_self" },
//          { label: "Edit", name: "Edit", iconName: "utility:edit", target: "_self" },
//   ];
const columns = [
  { label: "CAP Number", fieldName: "Name", type: "text" },
  { label: "COMPLAINT NUMBER", fieldName: "Complaint__r", type: "text" },
  { label: "DEFICIENCY/VIOLATION", fieldName: "Deficiency__c", type: "text" },
  { label: "CITATION", fieldName: "Citation__c", type: "text" },
  {
    label: "FREQUENCY OF DEFICIENCY",
    fieldName: "FrequencyofDeficiency__c",
    type: "text"
  },
  {
    label: "IMPACT OF DEFICIENCY",
    fieldName: "ImpactofDeficiency__c",
    type: "text"
  },
  {
    label: "SCOPE OF DEFICIENCY",
    fieldName: "ScopeofDeficiency__c",
    type: "text"
  }

  // {
  //     type: 'action',
  //     typeAttributes: { rowActions: actions },
  //     cellAttributes: {
  //         iconName: 'utility:threedots_vertical',
  //         iconAlternativeText: 'Actions',
  //         class: "tripledots"
  //     }
  // }
];

export default class ComplaintsDeficiencyViolations extends LightningElement {
  _newDeficiencyViolationModal;
  @track columns = columns;
  @track title;
  @track currentDeficiencyViolationData;
  @track disabledComplaint = false;
  @track complaintNumber;
  @track complaintDeficiencyViolationOptions;
  @track complaintFrequencyOptions;
  @track complaintImpactOptions;
  @track complaintScopeOptions;
  @track addDeficiencyViolations = {
    Deficiency__c: "",
    Citation__c: "",
    FrequencyofDeficiency__c: "",
    ImpactofDeficiency__c: "",
    ScopeofDeficiency__c: "",
    Findings__c: "",
    Comments__c: ""
  };
  @track isBtnDisable = false;
  @track isUpdateFlag = false;
  @track deficiencyViolationID;
  @track norecorddisplay = true;
  @track disabledForView = false;
  @track ViewHide = true;
  deficiencyViolationIcon = images + "/deficiencyIcon.svg";

  get CaseId() {
    return sharedData.getCaseId();
    // return '5000w000001fDZdAAM';
  }

  get providerId() {
    return sharedData.getProviderId();
    //  return '0010w00000AbXPNAA3';
  }

  get getUserProfileName() {
    return sharedData.getUserProfileName();
    // return 'Supervisor';
  }

  //openPetModal from Header Button Child comp Through AllTabs Comp
  @api get showButtonDeficiency() {
    return this._newDeficiencyViolationModal;
  }

  set showButtonDeficiency(value) {
    this._newDeficiencyViolationModal = value;
    this.deficiencyCaseDetailsLoad();
    this.deficiencyViolationID = undefined;
    this.addDeficiencyViolations = {};
    this.title = "Add Deficiency/Violation";
    this.btnLabel = "SAVE";
    this.isBtnDisable = true;
    this.isUpdateFlag = false;
    this.disabledForView = false;
    this.ViewHide = true;
  }

  //closemdal dispatch function
  closeModal() {
    this._newDeficiencyViolationModal = false;
    this.dispatchCustomEventToHandleAddDeficiencyInfoPopUp();
  }
  dispatchCustomEventToHandleAddDeficiencyInfoPopUp() {
    const tooglecmps = new CustomEvent("deficiencytogglecomponents", {
      detail: {
        Component: "CloseDeficiency"
      }
    });
    this.dispatchEvent(tooglecmps);
    this.addDeficiencyViolations = {};
    this.openDeficiencyModal = false;
    this.ViewHide = false;
  }

  handleChange(event) {
    this.isBtnDisable = false;
    if (event.target.name == "Deficiency__c") {
      this.addDeficiencyViolations.Deficiency__c = event.target.value;
    } else if (event.target.name == "Citation__c") {
      this.addDeficiencyViolations.Citation__c = event.target.value;
    } else if (event.target.name == "FrequencyofDeficiency__c") {
      this.addDeficiencyViolations.FrequencyofDeficiency__c =
        event.target.value;
    } else if (event.target.name == "ImpactofDeficiency__c") {
      this.addDeficiencyViolations.ImpactofDeficiency__c = event.target.value;
    } else if (event.target.name == "ScopeofDeficiency__c") {
      this.addDeficiencyViolations.ScopeofDeficiency__c = event.target.value;
    } else if (event.target.name == "Findings__c") {
      this.addDeficiencyViolations.Findings__c = event.target.value;
    } else if (event.target.name == "Comments__c") {
      this.addDeficiencyViolations.Comments__c = event.target.value;
    }
  }

  connectedCallback() {
    this.deficiencyViolationDetailsLoad();
    this.deficiencyCaseDetailsLoad();
  }

  //Function used to show the existing deficiencyViolation
  deficiencyViolationDetailsLoad() {
    getDeficiencyViolation({ compId: this.CaseId })
      .then((result) => {
        this.currentDeficiencyViolationData = result;
        if (this.currentDeficiencyViolationData.length == 0) {
          this.norecorddisplay = false;
        } else {
          this.norecorddisplay = true;
          const deficiencyDispatch = new CustomEvent("deficiencydispatchdata", {
            detail: {
              deficiencyFlag: true
            }
          });
          this.dispatchEvent(deficiencyDispatch);
        }

        if (result.length > 0) {
          const deficiencyBtndisable = new CustomEvent("deficiencybtndisable", {
            detail: {
              deficiencybtndis: true
            }
          });
          this.dispatchEvent(deficiencyBtndisable);
          this.currentDeficiencyViolationData = result.map((item) => {
            return {
              Id: item.Id,
              Name: item.Name,
              Complaint__r: item.Complaint__r.CaseNumber,
              Deficiency__c: item.Deficiency__c,
              Citation__c: item.Citation__c,
              FrequencyofDeficiency__c: item.FrequencyofDeficiency__c,
              ImpactofDeficiency__c: item.ImpactofDeficiency__c,
              ScopeofDeficiency__c: item.ScopeofDeficiency__c,
              Findings__c: item.Findings__c,
              Comments__c: item.Comments__c,
              buttonDisabled:
                this.getUserProfileName == "Supervisor" ? false : true
            };
          });
          result = this.currentDeficiencyViolationData;
        }
      })
      .catch(errors => {
        let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
        return this.dispatchEvent(utils.handleError(errors, config));
      });
  }

  //Function used to show the complaintNumber
  deficiencyCaseDetailsLoad() {
    getDeficiencyCase({ Complaint: this.CaseId })
      .then((res) => {
        if (res) {
          this.complaintNumber = res[0].CaseNumber;
          this.disabledComplaint = true;
        }
      })
      .catch(errors => {
        let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
        return this.dispatchEvent(utils.handleError(errors, config));
      });
  }

  //Declare Deficiency object into one variable
  @wire(getObjectInfo, { objectApiName: Deficiency__c_OBJECT })
  objectInfo;

  //Function used to get all picklist values from Deficiency object
  @wire(getPicklistValuesByRecordType, {
    objectApiName: Deficiency__c_OBJECT,
    recordTypeId: "$objectInfo.data.defaultRecordTypeId"
  })
  getAllPicklistValues({ error, data }) {
    if (data) {
      // Deficiency Field Picklist values
      let deficiencyOptions = [];
      data.picklistFieldValues.Deficiency__c.values.forEach((key) => {
        deficiencyOptions.push({
          label: key.label,
          value: key.value
        });
      });
      this.complaintDeficiencyViolationOptions = deficiencyOptions;

      // FrequencyofDeficiency Field Picklist values
      let frequencyofDeficiencyOptions = [];
      data.picklistFieldValues.FrequencyofDeficiency__c.values.forEach(
        (key) => {
          frequencyofDeficiencyOptions.push({
            label: key.label,
            value: key.value
          });
        }
      );
      this.complaintFrequencyOptions = frequencyofDeficiencyOptions;

      // ImpactofDeficiency Field Picklist values
      let impactofDeficiencyOptions = [];
      data.picklistFieldValues.ImpactofDeficiency__c.values.forEach((key) => {
        impactofDeficiencyOptions.push({
          label: key.label,
          value: key.value
        });
      });
      this.complaintImpactOptions = impactofDeficiencyOptions;

      // ScopeofDeficiency Field Picklist values
      let scopeofDeficiencyOptions = [];
      data.picklistFieldValues.ScopeofDeficiency__c.values.forEach((key) => {
        scopeofDeficiencyOptions.push({
          label: key.label,
          value: key.value
        });
      });
      this.complaintScopeOptions = scopeofDeficiencyOptions;
    } else if (error) {
      this.error = JSON.stringify(error);
    }
  }

  handleSave() {
    if (!this.addDeficiencyViolations.Deficiency__c) {
      this.dispatchEvent(
        utils.toastMessage("Please enter a deficiency", "warning")
      );
    } else if (!this.addDeficiencyViolations.Citation__c) {
      this.dispatchEvent(
        utils.toastMessage("Please enter a citation", "warning")
      );
    } else if (!this.addDeficiencyViolations.FrequencyofDeficiency__c) {
      this.dispatchEvent(
        utils.toastMessage("Please enter a frequencyofdeficiency", "warning")
      );
    } else if (!this.addDeficiencyViolations.ImpactofDeficiency__c) {
      this.dispatchEvent(
        utils.toastMessage("Please enter a impactofdeficiency", "warning")
      );
    } else if (!this.addDeficiencyViolations.ScopeofDeficiency__c) {
      this.dispatchEvent(
        utils.toastMessage("Please enter a scopeofdeficiency", "warning")
      );
    } else if (!this.addDeficiencyViolations.Findings__c) {
      this.dispatchEvent(
        utils.toastMessage("Please enter a findings ", "warning")
      );
    } else {
      this.isBtnDisable = true;
      this.addDeficiencyViolations.sobjectType = "Deficiency__c";
      this.addDeficiencyViolations.Complaint__c = this.CaseId;
      this.addDeficiencyViolations.Provider__c = this.providerId;
      this.addDeficiencyViolations.Status__c = "New";
      this.addDeficiencyViolations.Id = this.deficiencyViolationID;

      if (this.deficiencyViolationID) this.isUpdateFlag = true;
      else this.isUpdateFlag = false;
      insertUpdateDeficiencyViolationsDetails({
        saveObjInsert: this.addDeficiencyViolations
      })
        .then((result) => {
          this.deficiencyViolationDetailsLoad();
          let myobjcase = {
            sobjectType: "Case"
          };
          myobjcase.CAPNumber__c = result;
          myobjcase.Id = this.CaseId;
          insertUpdateDeficiencyViolationsDetails({
            saveObjInsert: myobjcase
          }).then((result) => {

          }).catch(errors => {
            let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
            return this.dispatchEvent(utils.handleError(errors, config));
          });
          if (this.isUpdateFlag)
            this.dispatchEvent(
              utils.toastMessage(
                "Deficiencyviolations details are updated successfully",
                "success"
              )
            );
          else
            this.dispatchEvent(
              utils.toastMessage(
                "Deficiencyviolations details are added successfully",
                "success"
              )
            );

          this.addDeficiencyViolations = {};
          this.closeModal();
          this.isBtnDisable = false;
          this.deficiencyViolationDetailsLoad();
        })
        .catch(errors => {
          let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
          return this.dispatchEvent(utils.handleError(errors, config));
        });
    }
  }

  handleRowAction(event) {
    let selectedDeficiencyId = event.detail.row.Id;

    if (event.detail.action.name == "Edit") {
      this._newDeficiencyViolationModal = true;
      this.title = "Edit Deficiency/Violation";
      this.btnLabel = "UPDATE";
      this.isBtnDisable = false;
      this.disabledForView = false;
      this.ViewHide = true;

      editDeficiencyViolationDetails({
        editDeficiencyDetail: selectedDeficiencyId
      })
        .then((result) => {
          this.addDeficiencyViolations.Deficiency__c = result[0].Deficiency__c;
          this.addDeficiencyViolations.Citation__c = result[0].Citation__c;
          this.addDeficiencyViolations.FrequencyofDeficiency__c =
            result[0].FrequencyofDeficiency__c;
          this.addDeficiencyViolations.ImpactofDeficiency__c =
            result[0].ImpactofDeficiency__c;
          this.addDeficiencyViolations.ScopeofDeficiency__c =
            result[0].ScopeofDeficiency__c;
          this.addDeficiencyViolations.Findings__c = result[0].Findings__c;
          this.addDeficiencyViolations.Comments__c = result[0].Comments__c;
          this.deficiencyViolationID = result[0].Id;
        })
        .catch(errors => {
          let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
          return this.dispatchEvent(utils.handleError(errors, config));
        });
    } else if (event.detail.action.name == "View") {
      this._newDeficiencyViolationModal = true;
      this.title = "View Deficiency/Violation";
      this.isBtnDisable = false;
      this.disabledForView = true;
      this.ViewHide = false;

      editDeficiencyViolationDetails({
        editDeficiencyDetail: selectedDeficiencyId
      })
        .then((result) => {
          this.addDeficiencyViolations.Deficiency__c = result[0].Deficiency__c;
          this.addDeficiencyViolations.Citation__c = result[0].Citation__c;
          this.addDeficiencyViolations.FrequencyofDeficiency__c =
            result[0].FrequencyofDeficiency__c;
          this.addDeficiencyViolations.ImpactofDeficiency__c =
            result[0].ImpactofDeficiency__c;
          this.addDeficiencyViolations.ScopeofDeficiency__c =
            result[0].ScopeofDeficiency__c;
          this.addDeficiencyViolations.Findings__c = result[0].Findings__c;
          this.addDeficiencyViolations.Comments__c = result[0].Comments__c;
        })
        .catch(errors => {
          let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
          return this.dispatchEvent(utils.handleError(errors, config));
        });
    }
  }

  constructor() {
    super();
    let deleteIcon = true;
    for (let i = 0; i < this.columns.length; i++) {
      if (this.columns[i].type == "action") {
        deleteIcon = false;
      }
    }
    if (deleteIcon) {
      this.columns.push({
        type: "action",
        typeAttributes: { rowActions: this.getRowActions },
        cellAttributes: {
          iconName: "utility:threedots_vertical",
          iconAlternativeText: "Actions",
          class: "tripledots blueIcons"
        }
      });
    }
  }
  getRowActions(row, doneCallback) {
    const actions = [];
    if (row["buttonDisabled"] == true) {
      actions.push(
        {
          label: "View",
          name: "View",
          iconName: "utility:preview",
          target: "_self"
        },
        {
          label: "Edit",
          name: "Edit",
          iconName: "utility:edit",
          target: "_self"
        }
      );
    } else {
      actions.push({
        label: "View",
        name: "View",
        iconName: "utility:preview",
        target: "_self"
      });
    }
    // simulate a trip to the server
    setTimeout(() => {
      doneCallback(actions);
    }, 200);
  }
}
