/**
 * @Author        : Preethi Bojarajan
 * @CreatedOn     : Jun 04, 2020
 * @Purpose       : Public Application's Household Member's Reference Check
 * @updatedBy     : 
 * @updatedOn     : 
 **/
import { LightningElement, track, api, wire } from 'lwc';

//Apex Class
import getRecordType from '@salesforce/apex/PublicApplicationReferenceChecks.getRecordType';
import getMultiplePicklistValues from '@salesforce/apex/PublicApplicationReferenceChecks.getMultiplePicklistValues';
import getHouseHoldMembers from '@salesforce/apex/PublicApplicationReferenceChecks.getHouseHoldMembers';
import fetchRefCheckedMemDetails from '@salesforce/apex/PublicApplicationReferenceChecks.fetchRefCheckedMemDetails';
import fetchAppRefCheckDetails from '@salesforce/apex/PublicApplicationReferenceChecks.fetchAppRefCheckDetails';


//Utils
import { constPopupVariables, CJAMS_OBJECT_QUERIES } from 'c/constants';
import * as sharedData from "c/sharedData";
import { utils } from 'c/utils';

//Style Sheet
import myResource from '@salesforce/resourceUrl/styleSheet';
import { loadStyle } from 'lightning/platformResourceLoader';

//Wired uiApi
import { createRecord, updateRecord, deleteRecord } from "lightning/uiRecordApi";
import { refreshApex } from "@salesforce/apex";
import ACTORDET_OBJECT from '@salesforce/schema/ActorDetails__c';
import ADDRESS_OBJECT from '@salesforce/schema/Address__c';

//Google Api Apex
import getFullDetails from "@salesforce/apex/GetStreetAddressFromGoogleAPI.getPlaceDetails";
import getAPIStreetAddress from "@salesforce/apex/GetStreetAddressFromGoogleAPI.getSuggestions";
import getAllStates from "@salesforce/apex/PublicApplicationReferenceChecks.getStateDetails";

import images from '@salesforce/resourceUrl/images';

export default class PublicApplicationReferenceChecks extends LightningElement {
    //Pop-Up Tracks
    @track title = 'Add Reference Check Details';
    @api referenceDetPop = false;
    @track isDisabled = false;
    @track relationHouseHold = false;
    @track saveOrUpdateLabel = 'Save';
    @track noIconShow = true;

    //Table Records
    @track columns = [];
    @track wiredTableMemDet;
    @track referenceMemers = [];
    descriptionIcon = images + '/description-icon.svg';
    moreIcon = images + '/more.svg';
    @track showCommentsPop = false;
    @track commentsOfPad;
    @track totalRecordsCount = 0;
    @track page = 1;
    perpage = 10;
    setPagination = 5;
    @track currentPageRecords;
    @track Spinner = true;

    //Save/Update Track
    @track referenceCheck = {};
    @track addressOfOther = {};
    @track recordTypeId = null;

    //Edit / View / Delete Tracks
    @track editRecordId;
    @track fetchedWholeData;
    @track openmodel = false;

    //Picklist Tracks
    @track typeOfContactValues;
    @track realationshipValues;
    @track householdmemValues;
    @track schoolRefCheckValues;
    @track refRecommentsValues;
    @track relationValues;

    //googleApi Tracks
    active = false;
    @track selectedCounty;
    @track strStreetAddress;
    @track strStreetAddresPrediction = [];
    @track filteredCountyArr = [];
    @track selectedState;
    @track selectedCity;
    @track selectedZipCode;
    @track selectedCounty;
    @track ShippingStreet;
    @track showStateBox = false;
    @track stateFetchResult = [];
    @track stateSearchTxt;
    @track filteredStateArr = [];
    @track showCountyBox = false;
    @track countySearchTxt;
    @track filteredCountyArr = [];
    @track countyArr = [];
    //@track addressDetails = {};
    @track addressInformationId = null;

    //Sundar Adding this
    @track isDisableActionBtn = false;

    renderedCallback() {
        Promise.all([
            loadStyle(this, myResource + '/styleSheet.css'),
        ])
    }

    constructor() {
        super();
        this.columns = [
            { label: 'DATE OF REFERENCE CHECK', fieldName: 'Date__c', sortable: true },
            { label: 'RELATIVE', fieldName: 'Relation__c', sortable: true },
            { label: 'REFERENCE NAME', fieldName: 'Name' },
            { label: 'REFERENCE RECOMMENDS', fieldName: 'ReferenceRecommends__c' },
            { label: 'TYPE OF CONTACT', fieldName: 'TypeofContact__c' },
            { label: 'SCHOOL RECOMMENDS', fieldName: 'SchoolReferenceCheck__c' },
            {
                label: 'COMMENTS', fieldName: 'Comments__c', type: 'button', typeAttributes: {
                    label: {
                        fieldName: 'Comments__c'
                    },
                    name: 'comments',
                    title: {
                        fieldName: 'Comments__c'
                    }
                },
                cellAttributes: {
                    iconName: 'utility:description',
                    class: {
                        fieldName: 'commentsClass'
                    }
                }
            },
            {
                type: 'action', fieldName: 'id', typeAttributes: { rowActions: this.getRowActions },
                cellAttributes: {
                    iconName: 'utility:threedots_vertical',
                    iconAlternativeText: 'Actions',
                    class: "tripledots"
                }
            },
        ]
    }



    getRowActions(row, doneCallback) {
        const actions = [];
        actions.push({
            'label': 'View',
            'iconName': 'action:preview',
            'name': 'viewRecord'
        });
        actions.push({
            'label': 'Edit',
            'iconName': 'action:edit',
            'name': 'editRecord'
        });
        actions.push({
            'label': 'Delete',
            'iconName': 'action:delete',
            'name': 'deleteRecord'
        });
        // simulate a trip to the server
        setTimeout(() => {
            doneCallback(actions);
        }, 200);
    }

    get providerId() {
        return sharedData.getProviderId();
    }

    @wire(getHouseHoldMembers, {
        provId: '$providerId'
    })
    householdMemberDet(data) {
        try {
            if (data.data != undefined && data.data.length > 0) {
                let currentData = [];
                data.data.forEach(data => {
                    if (data.Contact__r !== undefined) {
                        let obj = {};
                        obj.value = data.Id;
                        obj.label = data.Contact__r.LastName;
                        obj.contactId = data.Contact__c;
                        currentData.push(obj);
                    }
                });
                this.householdmemValues = currentData;
            } else if (data.error) {
                let errors = result.error;
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const { title, message, errorType } = error;
                    this.dispatchEvent(utils.toastMessageWithTitle(title, message, errorType));
                } else {
                    this.dispatchEvent(utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE));
                }
            }
        } catch (error) {
            throw error;
        }
    }

    @wire(getRecordType, {
        name: 'Reference Check'
    })
    recordTypeDetails(data) {
        try {
            if (data.data) {
                this.recordTypeId = data.data;
            } else if (data.error) {
                let errors = result.error;
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const { title, message, errorType } = error;
                    this.dispatchEvent(utils.toastMessageWithTitle(title, message, errorType));
                } else {
                    this.dispatchEvent(utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE));
                }
            }
        } catch (error) {
            throw error;
        }
    }

    closeAddRefModel() {
        this.referenceDetPop = false;
        const onRedirectAddRefOff = new CustomEvent("redirecttoaddrefoff", {
            detail: {
                first: false
            }
        });
        this.dispatchEvent(onRedirectAddRefOff);
        this.refreshPop();
    }

    closeCommentsPop() {
        this.showCommentsPop = false
    }

    get allPickListFields() {
        return CJAMS_OBJECT_QUERIES.FETCH_FIELDS_FOR_PUBLIC_REF_CHECK_PICKLIST;
    }

    @wire(fetchRefCheckedMemDetails, {
        provId: '$providerId',
        recId: '$recordTypeId'
    })
    wiredRefMemDet(data) {
        try {
            this.wiredTableMemDet = data;
            if (data.data != undefined && data.data !== '' && data.data.length > 0) {
                this.noIconShow = false;

                const selectedEvent = new CustomEvent("referencecheckflag", {
                    detail: {
                        referencecheckflag: true
                    }
                });
                this.dispatchEvent(selectedEvent);

                let currentData = [];
                data.data.forEach(data => {
                    let obj = {};
                    obj.Id = data.Id;
                    obj.Date__c = data.Date__c !== undefined ? utils.formatDate(data.Date__c) : '';
                    obj.Relation__c = data.Relation__c !== undefined ? data.Relation__c : '';
                    if (data.Actor__r == undefined)
                        obj.Name = data.FirstName__c !== undefined ? data.FirstName__c + ' ' + data.LastName__c : data.LastName__c;
                    else obj.Name = data.Actor__r.Contact__r.FirstName__c + ' ' + data.Actor__r.Contact__r.LastName__c;
                    obj.ReferenceRecommends__c = data.ReferenceRecommends__c !== undefined ? data.ReferenceRecommends__c : '';
                    obj.TypeofContact__c = data.TypeofContact__c !== undefined ? data.TypeofContact__c : '';
                    obj.SchoolReferenceCheck__c = data.SchoolReferenceCheck__c !== undefined ? data.SchoolReferenceCheck__c : '';
                    obj.Comments__c = data.Comments__c !== undefined ? data.Comments__c : ' - ';
                    obj.commentsClass = data.Comments__c !== undefined ? 'btnbdrIpad blue' : 'svgRemoveicon';
                    obj.RelationshiptoApplicant__c = data.RelationshiptoApplicant__c;
                    obj.actionClass = 'actionClass';
                    currentData.push(obj);
                });
                this.referenceMemers = currentData;
                this.totalRecordsCount = this.referenceMemers.length;
                this.pageData();
                setTimeout(() => {
                    this.comarRegulationsSet();
                }, 0);
            } else {
                this.Spinner = false;
                this.noIconShow = true;
                const selectedEvent = new CustomEvent("referencecheckflag", {
                    detail: {
                        referencecheckflag: false
                    }
                });
                this.dispatchEvent(selectedEvent);
            }
        } catch (error) {
            throw error;

        }
    }

    comarRegulationsSet() {
        let comarReg = this.template.querySelectorAll('.linepad');
        let comarCheckBox = this.template.querySelectorAll('.pad');
        if (this.referenceMemers.length >= 3) {
            comarReg[0].className = 'linepad checkedReference';
            comarCheckBox[0].className = 'pad checked';
        } else {
            comarReg[0].className = 'linepad unCheckedReference';
            comarCheckBox[0].className = 'pad unchecked'
        }
        if (this.referenceMemers.filter(ref => ref.TypeofContact__c === 'Face-to-face').length >= 2) {
            comarReg[1].className = 'linepad checkedReference';
            comarCheckBox[1].className = 'pad checked';
        } else {
            comarReg[1].className = 'linepad unCheckedReference';
            comarCheckBox[1].className = 'pad unchecked'
        }
        if (this.referenceMemers.filter(ref => ref.Relation__c === 'Non Relative').length >= 2) {
            comarReg[2].className = 'linepad checkedReference';
            comarCheckBox[2].className = 'pad checked';
        } else {
            comarReg[2].className = 'linepad unCheckedReference';
            comarCheckBox[2].className = 'pad unchecked'
        }
        this.Spinner = false;
    }

    @wire(getMultiplePicklistValues, {
        objInfo: 'ActorDetails__c',
        picklistFieldApi: '$allPickListFields'
    })
    wiredTitle(data) {
        try {
            if (data.data != undefined && data.data !== '') {
                this.typeOfContactValues = data.data.TypeofContact__c;
                this.realationshipValues = data.data.RelationshiptoApplicant__c;
                this.schoolRefCheckValues = data.data.SchoolReferenceCheck__c;
                this.refRecommentsValues = data.data.ReferenceRecommends__c;
                this.relationValues = data.data.Relation__c;
            } else if (data.error) {
                let errors = result.error;
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const { title, message, errorType } = error;
                    this.dispatchEvent(utils.toastMessageWithTitle(title, message, errorType));
                } else {
                    this.dispatchEvent(utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE));
                }
            }
        } catch (error) {
            throw error;
        }
    }

    onChangeHandlers(event) {
        switch (event.target.name) {
            case 'dorc':
                this.referenceCheck.Date__c = event.target.value;
                break;
            case 'typeOfCont':
                this.referenceCheck.TypeofContact__c = event.target.value;
                break;
            case 'relationBtnAppli':
                this.referenceCheck.RelationshiptoApplicant__c = event.target.value;
                if (this.referenceCheck.RelationshiptoApplicant__c === 'Household Member')
                    this.relationHouseHold = true;
                else this.relationHouseHold = false;
                break;
            case 'householdmem':
                this.referenceCheck.Actor__c = event.target.value;
                break;
            case 'firstName':
                this.referenceCheck.FirstName__c = event.target.value;
                break;
            case 'lastName':
                this.referenceCheck.LastName__c = event.target.value;
                break;
            case 'mobileNumber':
                event.target.value = event.target.value.replace(/(\D+)/g, "");
                this.addressOfOther.Phone__c = utils.formattedPhoneNumber(event.target.value);
                break;
            case 'address1':
                this.getStreetAddress(event);
                break;
            case 'address2':
                this.addressOfOther.AddressLine2__c = event.target.value;
                break;
            case 'city':
                this.addressOfOther.City__c = event.target.value;
                break;
            case 'state':
                this.addressOfOther.State__c = event.target.value;
                this.stateOnChange(event);
                break;
            case 'county':
                this.addressOfOther.County__c = event.target.value;
                this.countyOnChange(event);
                break;
            case 'zipcode':
                this.addressOfOther.ZipCode__c = event.target.value;
                break;
            case 'schoolRefCheck':
                this.referenceCheck.SchoolReferenceCheck__c = event.target.value;
                break;
            case 'refRecomments':
                this.referenceCheck.ReferenceRecommends__c = event.target.value;
                break;
            case 'relation':
                this.referenceCheck.Relation__c = event.target.value;
                break;
            case 'comments':
                this.referenceCheck.Comments__c = event.target.value;
                break;
        }
    }

    handleSpeechToText(event) {
        this.referenceCheck.Comments__c = event.detail.speechValue;
    }

    saveOrUpdateRefCheck() {
        const allValid = [
            ...this.template.querySelectorAll("lightning-input"),
            ...this.template.querySelectorAll("lightning-combobox"),
            ...this.template.querySelectorAll("lightning-radio-group")
        ].reduce((validSoFar, inputCmp) => {
            inputCmp.reportValidity();
            return validSoFar && inputCmp.checkValidity();
        }, true);
        if (!allValid)
            return this.dispatchEvent(utils.toastMessage(constPopupVariables.VALIDATION_WARN, "warning"));
        let datas = { referenceCheck: this.referenceCheck, addressDetails: this.addressOfOther };
        if (this.editRecordId !== undefined && this.editRecordId !== '') this.updateRefCheck(datas);
        else this.saveRefCheck(datas);
    }

    saveRefCheck(datas) {
        this.isDisableActionBtn = true;

        let fields = datas.referenceCheck;
        fields.RecordTypeId = this.recordTypeId;
        fields.Provider__c = this.providerId;
        const recordInput = {
            apiName: ACTORDET_OBJECT.objectApiName,
            fields
        };
        createRecord(recordInput)
            .then(result => {
                try {
                    if (result.id && result.RelationshiptoApplicant__c !== 'Household Member') {
                        let fields = datas.addressDetails;
                        fields.ActorDetails__c = result.id;
                        fields.AddressType__c = 'Reference Check'
                        const recordInput = {
                            apiName: ADDRESS_OBJECT.objectApiName,
                            fields
                        };
                        createRecord(recordInput)
                            .then(result => {
                                this.isDisableActionBtn = false;
                                this.dispatchEvent(utils.toastMessage(constPopupVariables.REFCHECK_SUCCESS, "success"));
                                //this.Spinner = false;
                                this.closeAddRefModel();
                            })
                    } else {
                        this.isDisableActionBtn = false;
                        this.dispatchEvent(utils.toastMessage(constPopupVariables.REFCHECK_SUCCESS, "success"));
                        //this.Spinner = false;
                        this.closeAddRefModel();
                    }
                } catch (error) {
                    throw error;
                }
            });
    }

    updateRefCheck(datas) {
        let fields = datas.referenceCheck;
        const recordInput = {
            fields
        };
        updateRecord(recordInput)
            .then(result => {
                try {
                    if (result.id && result.RelationshiptoApplicant__c !== 'Household Member') {
                        let fields = datas.addressDetails;
                        const recordInput = {
                            fields
                        };
                        updateRecord(recordInput)
                            .then(result => {
                                this.dispatchEvent(utils.toastMessage(constPopupVariables.REFCHECK_UPDATE, "success"));
                                //this.Spinner = false;
                                this.closeAddRefModel();
                            });
                    } else {
                        this.dispatchEvent(utils.toastMessage(constPopupVariables.REFCHECK_UPDATE, "success"));
                        //this.Spinner = false;
                        this.closeAddRefModel();
                    }
                } catch (error) {
                    throw error;
                }
            });
    }

    refreshPop() {
        this.referenceCheck = {};
        this.addressOfOther = {};
        this.editRecordId = '';
        this.isDisabled = false;
        refreshApex(this.wiredTableMemDet);
    }

    handleRowAction(event) {
        const action = event.detail.action;
        const row = event.detail.row;
        let relationshipToApplicant = this.referenceMemers.filter(ref => ref.Id === row.Id)[0].RelationshiptoApplicant__c;
        let modelName = relationshipToApplicant === 'Household Member' ? 'ActorDetails__c' : 'Address__c';
        switch (action.name) {
            case 'comments':
                this.commentsOfPad = row.Comments__c;
                this.showCommentsPop = true;
                break;
            case 'editRecord':
                this.editRecordId = row.Id;
                this.isDisabled = false;
                this.fetchRefCheckedRecord(modelName);
                break;
            case 'viewRecord':
                this.editRecordId = row.Id;
                this.isDisabled = true;
                this.fetchRefCheckedRecord(modelName);
                break;
            case 'deleteRecord':
                this.editRecordId = row.Id;
                this.openmodel = true;
                break;
        }
    }

    toggleEditDelView() {
        let conList = this.template.querySelectorAll('.intlist');

        conList.forEach((item) => {
            let indvRow = item;
            let indvRowId = item.getAttribute('id');
            if (indvRowId.includes(this.editRecordId)) {
                if (indvRow.classList.contains("slds-showd")) {
                    indvRow.classList.add("slds-hide");
                    indvRow.classList.remove("slds-showd");
                } else {
                    indvRow.classList.remove("slds-hide");
                    indvRow.classList.add("slds-showd");
                }
            } else {
                indvRow.classList.add("slds-hide");
                indvRow.classList.remove("slds-showd");
            }
        });
    }

    deleteFrSure() {
        this.openmodel = false;
        this.Spinner = true;
        deleteRecord(this.editRecordId)
            .then(() => {
                this.Spinner = false;
                this.dispatchEvent(utils.toastMessage(constPopupVariables.DELETESUCCESS, "success"));
                this.comarRegulationsSet();
                return refreshApex(this.wiredTableMemDet);
            }).catch(error => {
                this.Spinner = false;
                this.dispatchEvent(utils.toastMessage(constPopupVariables.UNAUTHOREDEL, "error"));
            })
    }

    closeModal() {
        this.openmodel = false;
        this.editRecordId = '';
    }

    fetchRefCheckedRecord(modelName) {
        fetchAppRefCheckDetails({
            actorDetId: this.editRecordId,
            model: modelName
        }).then(
            result => {
                try {
                    if (result.length > 0) {
                        this.fetchedWholeData = [...result];
                        if (result[0].ActorDetails__r === undefined) {
                            this.referenceCheck = Object.assign({}, result[0]);
                            this.relationHouseHold = true;
                        } else {
                            this.relationHouseHold = false;
                            this.referenceCheck = Object.assign({}, result[0].ActorDetails__r);
                            let a = Object.assign({}, result[0])
                            delete a.ActorDetails__r
                            this.addressOfOther = a;
                        }
                        this.referenceDetPop = true;
                    }
                } catch (error) {
                    throw error;
                }
            }).catch((errors) => {
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const { title, message, errorType } = error;
                    this.dispatchEvent(utils.toastMessageWithTitle(title, message, errorType));
                } else {
                    this.dispatchEvent(utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE));
                }
            });
    }

    hideViewEditPop() {
        let threedotClass = this.template.querySelectorAll(".intlist");
        threedotClass.forEach((i) => {
            let indvRow = i;
            indvRow.classList.add("slds-hide");
            indvRow.classList.remove("slds-showd");
        });
        this.editRecordId = '';
    }

    //Google api code starts from here

    get streetAddressLength() {
        return this.strStreetAddresPrediction != "";
    }
    get strStreetAddresPredictionUI() {
        return this.strStreetAddresPrediction;
    }
    get strStreetAddresPredictionLength() {
        if (this.addressOfOther.AddressLine1__c)
            return this.strStreetAddresPrediction.predictions ? true : false;
    }

    //Method used to show and hide the drop-down of suggestions
    get tabClass() {
        return this.active ?
            "slds-dropdown slds-dropdown_length-5 slds-dropdown_fluid slds-show" :
            "slds-dropdown slds-dropdown_length-5 slds-dropdown_fluid slds-hide";
    }


    //Function used to fetch all states from Address Object when Screen Loads
    @wire(getAllStates)
    getAllStates({
        error,
        data
    }) {
        if (data) {
            if (data.length > 0) {
                let stateData = data.map(row => {
                    return {
                        Id: row.Id,
                        Value: row.RefValue__c
                    };
                });
                this.stateFetchResult = stateData;
                // this.countyInitialLoad();
            } else {
                this.dispatchEvent(utils.toastMessage("No states found", "error"));
            }
        } else if (error) {
            this.dispatchEvent(
                utils.toastMessage("Error in Fetching states", "error")
            );
        }
    }

    //Function used to set selected state
    setSelectedCounty(event) {
        this.addressOfOther.County__c = event.currentTarget.dataset.value;
        this.showCountyBox = false;
    }

    //Function used to set selected state
    setSelectedState(event) {
        this.addressOfOther.State__c = event.currentTarget.dataset.value;
        this.showStateBox = false;
    }

    //Function used to capture on change county in an array
    countyOnChange(event) {
        let countySearchTxt = event.target.value;

        if (!countySearchTxt) this.showCountyBox = false;
        else {
            this.filteredCountyArr = this.countyArr
                .filter(val => val.value.indexOf(countySearchTxt) > -1)
                .map(cresult => {
                    return cresult;
                });
            this.showCountyBox = this.filteredCountyArr.length > 0 ? true : false;
        }
    }

    getStreetAddress(event) {
        this.addressOfOther.AddressLine1__c = event.target.value;
        if (!event.target.value) {
            this.addressOfOther.County__c = "";
            this.addressOfOther.State__c = "";
            this.addressOfOther.City__c = "";
            this.addressOfOther.ZipCode__c = "";
        } else {
            getAPIStreetAddress({
                input: this.addressOfOther.AddressLine1__c
            })
                .then(result => {
                    this.strStreetAddresPrediction = JSON.parse(result);
                    this.active =
                        this.strStreetAddresPrediction.predictions.length > 0 ?
                            true :
                            false;
                })
                .catch(error => {
                    this.dispatchEvent(
                        utils.toastMessage(
                            "Error in Fetching Google API Street Address",
                            "error"
                        )
                    );
                });
        }
    }

    //Function used to select the address
    selectStreetAddress(event) {
        this.addressOfOther.AddressLine1__c = event.target.dataset.description;

        getFullDetails({
            placeId: event.target.dataset.id
        })
            .then(result => {
                var main = JSON.parse(result);
                try {
                    this.addressOfOther.City__c = main.result.address_components[2].long_name;
                    this.addressOfOther.County__c = main.result.address_components[4].long_name;
                    this.addressOfOther.State__c = main.result.address_components[5].long_name;
                    this.addressOfOther.ZipCode__c = main.result.address_components[7].long_name;
                } catch (ex) { }
                this.active = false;
            })
            .error(error => {
                this.dispatchEvent(
                    utils.toastMessage("Error in Selected Address", "error")
                );
            });
    }

    //Function used for capturing on change states in an array
    stateOnChange(event) {
        let stateSearchTxt = event.target.value;

        if (!stateSearchTxt) this.showStateBox = false;
        else {
            this.filteredStateArr = this.stateFetchResult
                .filter(val => val.Value.indexOf(stateSearchTxt) > -1)
                .map(sresult => {
                    return sresult;
                });
            this.showStateBox = this.filteredStateArr.length > 0 ? true : false;
        }
    }

    //ends here

    //Pagination Function Start
    pageData = () => {
        let page = this.page;
        let perpage = this.perpage;
        let startIndex = page * perpage - perpage;
        let endIndex = page * perpage;
        this.currentPageRecords = this.referenceMemers.slice(startIndex, endIndex);
        this.noRecordsFound = this.currentPageRecords.length > 0 ? false : true;
        this.Spinner = false;
    };

    hanldeProgressValueChange(event) {
        this.page = event.detail;
        this.pageData();
    }
    //Pagination Function End
}