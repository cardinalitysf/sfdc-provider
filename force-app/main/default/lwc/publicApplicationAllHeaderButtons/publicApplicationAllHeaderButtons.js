import { LightningElement, api, track, wire } from 'lwc';
import APPLICATIONSTATUS_NAME from "@salesforce/schema/Application__c.Name";
import { getRecord } from "lightning/uiRecordApi";
import * as sharedData from 'c/sharedData';

export default class PublicApplicationAllHeaderButtons extends LightningElement {
  //Button show/Hide
  @api showAddService = false;
  @api showPetDetails = false;
  @api showRefChecks = false;
  @api showAssignTraining = false;
  @api showAddHomeStudy = false;

  @track ApplicationName;

  get ApplicationId() {
    return sharedData.getApplicationId();
  }
  @wire(getRecord, {
    recordId: "$ApplicationId",
    fields: [APPLICATIONSTATUS_NAME]
  })
  wireuser({ error, data }) {
    if (error) {
      this.error = error;
    } else if (data) {
        this.ApplicationName = data.fields.Name.value;
    }
}

  // method for redirecting a button popup  
  RedirectServiceInfo() {
    const onredirectservice = new CustomEvent(
      "redirecttoserviceinfo",
      {
        detail: {
          first: true
        }
      }
    );
    this.dispatchEvent(onredirectservice);
  }

  RedirectPetInfo() {
    const onredirectpet = new CustomEvent(
      "redirecttopetinfo",
      {
        detail: {
          first: true
        }
      }
    );
    this.dispatchEvent(onredirectpet);
  }

  redirectToAddReference() {
    const onRedirectAddRef = new CustomEvent("redirecttoaddref", {
      detail: {
        first: true
      }
    });
    this.dispatchEvent(onRedirectAddRef);
  }

  RedirectSessionInfo() {
    const onredirectsession = new CustomEvent(
      "redirecttosessioninfo",
      {
        detail: {
          first: true
        }
      }
    );
    this.dispatchEvent(onredirectsession);
  }
  openStudyModel() {
    
    const onredirectstudy = new CustomEvent(
      "redirecttostudyvisit",
      {
        detail: {
          first: true
        }
      }
    );
    this.dispatchEvent(onredirectstudy);
  }

  redirectToPubAppDashboard() {
    const onClickId = new CustomEvent('redirecttopubappdashboard', {
      detail: {
        first: true
      }
    });
    this.dispatchEvent(onClickId);
  }
}






