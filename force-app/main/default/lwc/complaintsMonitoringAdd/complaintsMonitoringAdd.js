/***
 * @Author: Sindhu Venkateswarlu
 * @CreatedOn: July 1, 2020
 * @Purpose: Complaints Monitoring Add/List/Edit
 **/

import { LightningElement, track, wire } from "lwc";
import getMonitoringDetails from "@salesforce/apex/ComplaintsMonitoringAdd.getMonitoringDetails";
import InsertUpdateMonitoring from "@salesforce/apex/ComplaintsMonitoringAdd.InsertUpdateMonitoring";
import getAddressDetails from "@salesforce/apex/ComplaintsMonitoringAdd.getAddressDetails";
import getProgramDetails from "@salesforce/apex/ComplaintsMonitoringAdd.getProgramDetails";
import { getObjectInfo } from "lightning/uiObjectInfoApi";
import MONITORING_OBJECT from "@salesforce/schema/Monitoring__c";
import { getPicklistValuesByRecordType } from "lightning/uiObjectInfoApi";
import editMonitoringDetails from "@salesforce/apex/ComplaintsMonitoringAdd.editMonitoringDetails";
import viewMonitoringDetails from "@salesforce/apex/ComplaintsMonitoringAdd.viewMonitoringDetails";
import getMonitoringStatus from "@salesforce/apex/ComplaintsMonitoringAdd.getMonitoringStatus";

import * as sharedData from "c/sharedData";
import { utils } from "c/utils";
import images from "@salesforce/resourceUrl/images";
import {
  deleteRecord
} from "lightning/uiRecordApi";
import {
  CJAMS_CONSTANTS
} from 'c/constants';

const actions = [
  {
    label: "Edit",
    name: "Edit",
    iconName: "utility:edit",
    target: "_self"
  },
  {
    label: "View",
    name: "View",
    iconName: "utility:preview",
    target: "_self"
  },
  {
    label: "Delete",
    name: "Delete",
    iconName: "utility:delete",
    target: "_self"
  }
];

const columns = [
  {
    label: "Visit Id",
    fieldName: "linkName",
    type: "button",
    initialWidth: 100,
    typeAttributes: {
      label: {
        fieldName: "Name"
      }
    }
  },
  {
    label: "Complaint Number",
    fieldName: "CaseNumber",
    sortable: true
  },
  {
    label: "License Number",
    sortable: true,
    fieldName: "License",
    cellAttributes: {
      class: "title"
    }
  },

  {
    label: "Program",
    fieldName: "Program__c",
    sortable: true
  },
  {
    label: "Visit Start Date",
    fieldName: "VisitationStartDate__c",
    sortable: true
  },
  {
    label: "Visit End Date",
    fieldName: "VisitationEndDate__c",
    sortable: true
  },
  {
    label: "Announced / Unannouced",
    fieldName: "AnnouncedUnannouced",
    sortable: true
  },

  {
    label: "Status",
    fieldName: "Status__c",
    cellAttributes: {
      class: {
        fieldName: "statusClass"
      }
    }
  }
];

export default class ComplaintsMonitoringAdd extends LightningElement {
  @track siteaddress;
  @track selectperiod;
  @track columns = columns;
  @track visitationstartdate;
  @track announced;
  @track timevisit;
  @track currentPageMonitoringData;
  @track norecorddisplay;

  //Pagination Flags
  @track totalRecords;
  @track totalRecordsCount = 0;
  @track page = 1;
  perpage = 10;
  setPagination = 5;

  @track addEditMonitoring = {};
  @track Program__c;
  @track ProgramType__c;
  @track License__r;
  @track SiteAddress;
  addressId;
  @track Spinner = true;
  applicationId;
  @track inspectedwithValues;
  @track monitoringID;
  @track ApplicationLicenseId__c;
  @track defaultSortDirection = "asc";
  @track sortDirection = "asc";
  @track sortedBy = "VisitationStartDate__c";
  @track renewalmindate = "1970-01-01";
  @track renewalmaxdate = "1970-01-02";
  @track freezeMonitoringScreen = false;
  caseforApiCall = this.getCaseId;
  attachmentIcon = images + "/application-monitoring.svg";
  @track title;

  //Monitoring Add Flags
  @track openmodel = false;
  @track viewmodel = false;
  @track deleteModel = false;
  @track btnDisable = false;
  @track isLicenseExpired = false;
  @track isDisableEndDate = false;
  @track btnLabel = false;
  finalStatus;
  @track isUpdateFlag = false;
  @track AssignbtnDisable = false;


  //Function used to get Case id from shared data
  get getCaseId() {
    return sharedData.getCaseId();
  }
  get getProviderId() {
    return sharedData.getProviderId();
  }
 get getLicenseId()
 {
  return sharedData.getLicenseId();
 }
  //SuperUser Login Check
  get profileName() {
    return sharedData.getUserProfileName();
  }

  //Get Site Address Value
  get siteAddressValues() {
    return [
      {
        label: this.SiteAddress,
        value: this.SiteAddress
      }
    ];
  }

  get ComplaintsStatus() {
    return sharedData.getComplaintsStatus();
  }

  //Connected Call Back method
  connectedCallback() {
    if (this.profileName !== "Supervisor") {
      if (
        [
          "Submitted to supervisor",
          "Accepted",
          "Disputed",
          "Approved",
          "Rejected"
        ].includes(this.ComplaintsStatus)
      ) {
        this.AssignbtnDisable = true;
      } else {
        this.AssignbtnDisable = false;
      }
    }
    else if (this.profileName === "Supervisor") {
      this.AssignbtnDisable = true;
    }

    getAddressDetails({
      applicationid: this.getCaseId
    })
      .then((result) => {
        if (result.length == 0) {
          this.addressId = null;
          this.SiteAddress = null;
        } else {
          this.addressId = result[0].Address__c;
          if (
            result[0].Address__r != undefined ||
            result[0].Address__r.length > 0
          ) {
            this.SiteAddress =
              (result[0].Address__r.AddressLine1__c != undefined
                ? result[0].Address__r.AddressLine1__c
                : "") +
              "," +
              (result[0].Address__r.City__c != undefined
                ? result[0].Address__r.City__c
                : "") +
              "," +
              (result[0].Address__r.County__c != undefined
                ? result[0].Address__r.County__c
                : "");
          }
        }
      })
      .catch((error) => {
        this.dispatchEvent(
          utils.toastMessage("Error in fetching  record", "error")
        );
      });

    getProgramDetails({
      caseId: this.getCaseId
    })
      .then((result) => {
        if (result.length == 0) {
          this.Program__c = "";
          this.ProgramType__c = "";
          this.CaseNumber = "";
          this.License__r = "";
        } else {
          this.Program__c = result[0].License__r != undefined ? result[0].License__r.ProgramName__c : '';
          this.ProgramType__c = result[0].License__r != undefined ? result[0].License__r.ProgramType__c : '';
          this.License__r = result[0].License__r != undefined ? result[0].License__r.Name : '';
          this.CaseNumber = result[0].CaseNumber;
        }
      })
      .catch((error) => {
        this.dispatchEvent(
          utils.toastMessage("Error in fetching record", "error")
        );
      });
    this.monitoringDetailsLoad();
  }

  //Get Monitoring Details
  monitoringDetailsLoad() {
    getMonitoringDetails({
      monitoringid: this.getCaseId
    })
      .then((result) => {
        this.totalRecordsCount = result.length;
        if (this.totalRecordsCount == 0) {
          this.norecorddisplay = true;
        } else {
          this.norecorddisplay = false;

          let data = [...result];
          if (data.length > 0) {
            let statusArray = [];
            data.forEach(row => {
              statusArray.push(row.Status__c);
            });
            
            if (statusArray.includes('Draft')) {
              

              const monitoringDispatch = new CustomEvent('monitoringdecisonflag', {
                detail: {
                  decisionflag:true
                }
              });
              this.dispatchEvent(monitoringDispatch);


            }

          }

        }

        this.totalRecords = result.map((row) => {
          return {
            Id: row.Id,
            Name: row.Name,
            License: row.Complaint__r.License__r.Name,
            Program__c: row.Complaint__r != undefined ? row.Complaint__r.License__r != undefined ? row.Complaint__r.License__r.ProgramName__c : "" : "",
            CaseNumber: row.Complaint__r != undefined ? row.Complaint__r.License__r != undefined ? row.Complaint__r.CaseNumber : '' : '',
            VisitationStartDate__c: utils.formatDate(
              row.VisitationStartDate__c
            ),
            VisitationEndDate__c: utils.formatDate(row.VisitationEndDate__c),
            AnnouncedUnannouced: row.AnnouncedUnannouced__c,
            linkName: "/" + row.Id,
            Status__c: row.Status__c,
            statusClass: row.Status__c,
            SiteAddressId: row.SiteAddress__c,
            buttonDisabled: (this.AssignbtnDisable) ? true : false

          };
        });
        result = this.totalRecords; //reassigning data for show hide
        this.pageData();

      })
      .catch((errors) => {
        if (errors) {

          let error = JSON.parse(errors.body.message);
          const { title, message, errorType } = error;
          this.dispatchEvent(
            utils.toastMessageWithTitle(title, message, errorType)
          );
        } else {
          this.dispatchEvent(
            utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
          );
        }
      });
  }

  //Function used to show the monitoring based on the pages
  pageData = () => {
    let page = this.page;
    let perpage = this.perpage;
    let startIndex = page * perpage - perpage;
    let endIndex = page * perpage;
    this.currentPageMonitoringData = this.totalRecords.slice(
      startIndex,
      endIndex
    );
    if (this.currentPageMonitoringData.length == 0) {
      if (this.page != 1) {
        this.page = this.page - 1;
        this.pageData();
      }
    }
    this.Spinner = false;
  };

  // Used to sort the 'Age' column
  sortBy(field, reverse, primer) {
    const key = primer
      ? function (x) {
        return primer(x[field]);
      }
      : function (x) {
        return x[field];
      };

    return function (a, b) {
      a = key(a);
      b = key(b);
      return reverse * ((a > b) - (b > a));
    };
  }

  //Sorting Method
  onHandleSort(event) {
    const { fieldName: sortedBy, sortDirection } = event.detail;
    const cloneData = [...this.currentPageMonitoringData];
    cloneData.sort(this.sortBy(sortedBy, sortDirection === "asc" ? 1 : -1));
    this.currentPageMonitoringData = cloneData;
    this.sortDirection = sortDirection;
    this.sortedBy = sortedBy;
  }

  //Function used to get the page number from child pagination component
  hanldeProgressValueChange(event) {
    this.page = event.detail;
    this.pageData();
  }



  //Declare Monitoring object into one variable
  @wire(getObjectInfo, {
    objectApiName: MONITORING_OBJECT
  })
  objectInfo;

  //Function used to get all picklist values from Monitoring object
  @wire(getPicklistValuesByRecordType, {
    objectApiName: MONITORING_OBJECT,
    recordTypeId: "$objectInfo.data.defaultRecordTypeId"
  })
  getAllPicklistValues({ error, data }) {
    if (data) {
      // inspectedwith
      let inspectedwithOptions = [];
      data.picklistFieldValues.JointlyInspectedWith__c.values.forEach((key) => {
        inspectedwithOptions.push({
          label: key.label,
          value: key.value
        });
      });
      this.inspectedwithValues = inspectedwithOptions;

      //timevisitValues
      let timevisitOptions = [];
      data.picklistFieldValues.TimetoVisit__c.values.forEach((key) => {
        timevisitOptions.push({
          label: key.label,
          value: key.value
        });
      });
      this.timevisitValues = timevisitOptions;

      //selectPeriodValues
      let selectPeriodOptions = [];
      if (this.isLicenseExpired) {
        data.picklistFieldValues.SelectPeriod__c.values.forEach((key) => {
          selectPeriodOptions.push({
            label: key.label,
            value: key.value
          });
        });
      } else {
        data.picklistFieldValues.SelectPeriod__c.values.forEach((key) => {
          if (key.label != "Re-Licensure") {
            selectPeriodOptions.push({
              label: key.label,
              value: key.value
            });
          }
        });
      }
      this.selectPeriodValues = selectPeriodOptions;

      //selectAnnouncedValues
      let selectAnnouncedOptions = [];
      data.picklistFieldValues.AnnouncedUnannouced__c.values.forEach((key) => {
        selectAnnouncedOptions.push({
          label: key.label,
          value: key.value
        });
      });
      this.selectAnnouncedValues = selectAnnouncedOptions;
    } else if (error) {
      this.dispatchEvent(
        utils.toastMessage("Error in fetching record", "error")
      );
      this.error = JSON.stringify(error);
    }
  }

  //Model Opening For Add/Edit/View.
  openmodal() {
    if (this.freezeMonitoringScreen)
      return this.dispatchEvent(
        utils.toastMessage(
          `Cannot add monitoring for ${this.finalStatus} application`,
          "warning"
        )
      );

    this.title = "ADD MONITORING";
    this.btnLabel = "SAVE";

    this.addEditMonitoring = {
      // SelectPeriod__c: this.totalRecordsCount == 0 ? 'Initial' : '',
      Program__c: this.Program__c,
      ProgramType__c: this.ProgramType__c,
      CaseNumber: this.CaseNumber,
      License__r: this.License__r
    };
    //this.isDisableSelectPeriod = this.totalRecordsCount == 0 ? true : false;
    this.isDisableEndDate = true;
    this.monitoringID = null;
    this.btnDisable = true;
    this.openmodel = true;
    this.viewmodel = false;
  }

  //Closing the Modal
  closeModal() {
    this.openmodel = false;
    this.viewmodel = false;
  }

  //Function used to edit the existing monitoring
  handleRowAction(event) {
    let monitoringrowid = event.detail.row.Id;
    if (event.detail.action.name == undefined) {
      sharedData.setMonitoringId(event.detail.row.Id);
      sharedData.setProviderId(this.getProviderId);
      sharedData.setMonitoringAddressId(event.detail.row.SiteAddressId);
      sharedData.setMonitoringStatus(event.detail.row.Status__c);

      const oncaseid = new CustomEvent("redirecteditmonitering", {
        detail: {
          first: true
        }
      });
      this.dispatchEvent(oncaseid);
    } else if (event.detail.action.name == "Edit") {
      getMonitoringStatus({
        monitoringid: monitoringrowid
      })
        .then((result) => {
          if (result[0].Status__c == "Completed")
            return this.dispatchEvent(
              utils.toastMessage(
                `Completed monitoring cant be edited.`,
                "warning"
              )
            );

          if (this.freezeMonitoringScreen)
            return this.dispatchEvent(
              utils.toastMessage(
                `Cannot edit monitoring for ${this.finalStatus} application`,
                "warning"
              )
            );

          this.title = "EDIT MONITORING";
          this.btnLabel = "UPDATE";
          this.isDisableEndDate = false;
          //this.isDisableSelectPeriod = false;
          this.openmodel = true;
          this.btnDisable = false;
          this.viewmodel = false;
          this.deleteModel = false;
          editMonitoringDetails({
            editMonitoringDetails: monitoringrowid
          })
            .then((result) => {
              this.addEditMonitoring.Program__c = result[0].Complaint__r != undefined ? result[0].Complaint__r.License__r != undefined ? result[0].Complaint__r.License__r.ProgramName__c : '' : '';
              this.addEditMonitoring.ProgramType__c = result[0].Complaint__r != undefined ? result[0].Complaint__r.License__r != undefined ? result[0].Complaint__r.License__r.ProgramType__c : '' : '';
              this.addEditMonitoring.CaseNumber = result[0].Complaint__r != undefined ? result[0].Complaint__r.CaseNumber : '';
              this.addEditMonitoring.License__r = result[0].Complaint__r != undefined ? result[0].Complaint__r.License__r != undefined ? result[0].Complaint__r.License__r.Name : '' : '';

              this.SiteAddress__c = result[0].SiteAddress__c;
              this.addEditMonitoring.SelectPeriod__c =
                result[0].SelectPeriod__c;
              this.addEditMonitoring.VisitationStartDate__c =
                result[0].VisitationStartDate__c;
              this.addEditMonitoring.VisitationEndDate__c =
                result[0].VisitationEndDate__c;
              this.addEditMonitoring.AnnouncedUnannouced__c =
                result[0].AnnouncedUnannouced__c;
              this.addEditMonitoring.TimetoVisit__c = result[0].TimetoVisit__c;
              this.addEditMonitoring.JointlyInspectedWith__c =
                result[0].JointlyInspectedWith__c;
              this.monitoringID = result[0].Id;
              this.renewalmindate = result[0].VisitationStartDate__c;
              this.renewalmaxdate = result[0].VisitationStartDate__c + 20;
            })
            .catch((error) => {
              if (errors) {

                let error = JSON.parse(errors.body.message);
                const { title, message, errorType } = error;
                this.dispatchEvent(
                  utils.toastMessageWithTitle(title, message, errorType)
                );
              } else {
                this.dispatchEvent(
                  utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                );
              }
            });
        })
        .catch((error) => {
          if (errors) {

            let error = JSON.parse(errors.body.message);
            const { title, message, errorType } = error;
            this.dispatchEvent(
              utils.toastMessageWithTitle(title, message, errorType)
            );
          } else {
            this.dispatchEvent(
              utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
            );
          }
        });
    } else if (event.detail.action.name == "View") {
      this.title = "VIEW MONITORING";
      this.openmodel = false;
      this.viewmodel = true;
      this.deleteModel = false;

      viewMonitoringDetails({
        viewMonitoringDetails: monitoringrowid
      })
        .then((result) => {
          this.addEditMonitoring.Program__c = result[0].Complaint__r != undefined ? result[0].Complaint__r.License__r != undefined ? result[0].Complaint__r.License__r.ProgramName__c : '' : '';
          this.addEditMonitoring.ProgramType__c = result[0].Complaint__r != undefined ? result[0].Complaint__r.License__r != undefined ? result[0].Complaint__r.License__r.ProgramType__c : '' : '';
          this.addEditMonitoring.CaseNumber = result[0].Complaint__r != undefined ? result[0].Complaint__r.CaseNumber : '';
          this.addEditMonitoring.License__r = result[0].Complaint__r != undefined ? result[0].Complaint__r.License__r != undefined ? result[0].Complaint__r.License__r.Name : undefined : undefined;

          this.SiteAddress__c = result[0].SiteAddress__c;
          this.addEditMonitoring.SelectPeriod__c = result[0].SelectPeriod__c;
          this.addEditMonitoring.VisitationStartDate__c =
            result[0].VisitationStartDate__c;
          this.addEditMonitoring.VisitationEndDate__c =
            result[0].VisitationEndDate__c;
          this.addEditMonitoring.AnnouncedUnannouced__c =
            result[0].AnnouncedUnannouced__c;
          this.addEditMonitoring.TimetoVisit__c = result[0].TimetoVisit__c;
          this.addEditMonitoring.JointlyInspectedWith__c =
            result[0].JointlyInspectedWith__c;
          this.monitoringID = result[0].Id;
        })
        .catch((errors) => {
          if (errors) {

            let error = JSON.parse(errors.body.message);
            const { title, message, errorType } = error;
            this.dispatchEvent(
              utils.toastMessageWithTitle(title, message, errorType)
            );
          } else {
            this.dispatchEvent(
              utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
            );
          }
        });
    } else if (event.detail.action.name == "Delete") {
      this.closeModal();
      this.deleteModel = true;
      this.deleteId = event.detail.row.Id;
    }
  }
  handleDelete() {
    deleteRecord(this.deleteId)
      .then(() => {
        this.deleteModel = false;
        this.deleteId = null;
        this.monitoringDetailsLoad();
        this.dispatchEvent(
          utils.toastMessage(
            "Monitoring  Details deleted successfully",
            "success"
          )
        );
      })
      .catch((error) => {
        this.deleteModel = false;
        this.dispatchEvent(
          utils.toastMessage(
            "Error in delete Monitoring details",
            "warning"
          )
        );
      });
  }

  closeDeleteModal() {
    this.deleteModel = false;
  }

  //On Change functionality
  handleChange(event) {
    this.btnDisable = false;
    if (event.target.name == "CaseNumber") {
      this.addEditMonitoring.CaseNumber = event.target.value;
    } else if (event.target.name == "License__r") {
      this.addEditMonitoring.License__r = event.target.value;
    } else if (event.target.name == "Program__c") {
      this.addEditMonitoring.Program__c = event.target.value;
    } else if (event.target.name == "ProgramType__c") {
      this.addEditMonitoring.ProgramType__c = event.target.value;
    } else if (event.target.name == "SiteAddress__c") {
      this.addEditMonitoring.SiteAddress__c = event.target.value;
    } else if (event.target.name == "SelectPeriod__c") {
      this.addEditMonitoring.SelectPeriod__c = event.target.value;
    } else if (event.target.name == "VisitationStartDate__c") {
      this.addEditMonitoring.VisitationStartDate__c = event.target.value;
      this.renewalmindate = event.target.value;
      this.renewalmaxdate = event.target.value + 20;
      this.isDisableEndDate = false;
    } else if (event.target.name == "VisitationEndDate__c") {
      this.addEditMonitoring.VisitationEndDate__c = event.target.value;
    } else if (event.target.name == "AnnouncedUnannouced__c") {
      this.addEditMonitoring.AnnouncedUnannouced__c = event.target.value;
    } else if (event.target.name == "TimetoVisit__c") {
      this.addEditMonitoring.TimetoVisit__c = event.target.value;
    } else if (event.target.name == "JointlyInspectedWith__c") {
      this.addEditMonitoring.JointlyInspectedWith__c = event.target.value;
    }
  }

  handleSave() {
    if (!this.Program__c)
      return this.dispatchEvent(utils.toastMessage("Program is mandatory", "warning"));

    if (!this.ProgramType__c)
      return this.dispatchEvent(utils.toastMessage("Program Type is mandatory", "warning"));

    if (!this.SiteAddress)
      return this.dispatchEvent(utils.toastMessage("Site Address is mandatory", "warning"));

    if (this.addEditMonitoring && !this.addEditMonitoring.SelectPeriod__c)
      return this.dispatchEvent(utils.toastMessage("Select Frequency is mandatory", "warning"));

    if (
      this.addEditMonitoring &&
      !this.addEditMonitoring.VisitationStartDate__c
    )
      return this.dispatchEvent(
        utils.toastMessage("Visitation Start Date is mandatory", "warning")
      );

    if (
      this.addEditMonitoring &&
      this.addEditMonitoring.VisitationEndDate__c &&
      new Date(this.addEditMonitoring.VisitationStartDate__c).getTime() >
      new Date(this.addEditMonitoring.VisitationEndDate__c).getTime()
    )
      return this.dispatchEvent(
        utils.toastMessage(
          "Visitation Start Date should be greater than Visitation End Date",
          "warning"
        )
      );

    this.btnDisable = true;  
    this.addEditMonitoring.sobjectType = "Monitoring__c";
    this.addEditMonitoring.SiteAddress__c = this.addressId;
    this.addEditMonitoring.Complaint__c = this.getCaseId;
    this.addEditMonitoring.License__c = this.getLicenseId;

    if (this.monitoringID) {
      this.isUpdateFlag = true;
      this.addEditMonitoring.Id = this.monitoringID;


    } else {
      this.isUpdateFlag = false;
      this.addEditMonitoring.Status__c = "Draft";
    }

    InsertUpdateMonitoring({
      objSobjecttoUpdateOrInsert: this.addEditMonitoring
    })
      .then((result) => {
        if (this.isUpdateFlag)
          this.dispatchEvent(
            utils.toastMessage(
              "Complaints Monitoring has been updated successfully",
              "success"
            )
          );
        else
          this.dispatchEvent(
            utils.toastMessage(
              "Complaints Monitoring has been added successfully",
              "success"
            )
          );

        this.monitoringDetailsLoad();
        this.addEditMonitoring = {};
        this.btnDisable = false;
        this.openmodel = false;
      })
      .catch(errors => {
        if (errors) {
          let error = JSON.parse(errors.body.message);
          const { title, message, errorType } = error;
          this.dispatchEvent(
            utils.toastMessageWithTitle(title, message, errorType)
          );
        } else {
          this.dispatchEvent(
            utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
          );
        }
      });


  }
  constructor() {
    super();
    let ViewIcon = true;
    for (let i = 0; i < this.columns.length; i++) {
      if (this.columns[i].type == 'action') {
        ViewIcon = false;
      }
    }
    if (ViewIcon) {
      this.columns.push(
        {
          type: 'action', typeAttributes: { rowActions: this.getRowActions }, cellAttributes: {
            iconName: 'utility:threedots_vertical',
            iconAlternativeText: 'Actions',
            class: "tripledots"
          }
        }
      )
    }
  }
  getRowActions(row, doneCallback) {
    const actions = [];
    if (row['buttonDisabled'] == true) {
      actions.push({
        'label': 'View',
        'name': 'View',
        'iconName': 'utility:preview',
        'target': '_self'
      });
    } else {
      actions.push({
        'label': 'Edit',
        'name': 'Edit',
        'iconName': 'utility:edit',
        'target': '_self'
      },
        {
          'label': 'Delete',
          'name': 'Delete',
          'iconName': 'utility:delete',
          'target': '_self'
        }, {
        'label': 'View',
        'name': 'View',
        'iconName': 'utility:preview',
        'target': '_self'
      });
    }
    // simulate a trip to the server
    setTimeout(() => {
      doneCallback(actions);
    }, 300);
  }

}
