import {
  LightningElement,
  track,
  api,
  wire
} from 'lwc';
import fetchDataForAddOrEdit from "@salesforce/apex/StaffAssignProgram.fetchDataForAddOrEdit";
import updateOrInsertSOQL from "@salesforce/apex/StaffAssignProgram.updateOrInsertSOQL";
// import getPickListValues from "@salesforce/apex/StaffAssignProgram.getAccrediationList";
import * as sharedData from "c/sharedData";
import {
  utils
} from "c/utils";
import selectRecord from "@salesforce/apex/DMLOperationsHandler.selectSOQL";

export default class StaffAssignProgram extends LightningElement {
  @track selectedVals = [];
  @track ProgramType;
  @track AccreditationOptions;
  @track AccountId;
  @track EmployeeType;
  @track Name;
  @track LastName;

  get staffid() {

    return sharedData.getStaffId();
  }

  get optionsProgramTypes() {
    return [{
        label: "Diagnostic Evaluation and Treatment Programs(DETP)",
        value: "Diagnostic Evaluation and Treatment Programs(DETP)"
      },
      {
        label: "Group Home Programs(GHP)",
        value: "Group Home Programs(GHP)"
      },
      {
        label: "Medically Fragile Programs(MFP)",
        value: "Medically Fragile Programs(MFP)"
      },
      {
        label: "Psychiatric Respite Programs(PRP)",
        value: "Psychiatric Respite Programs(PRP)"
      },
      {
        label: "Teen Parent Program",
        value: "Teen Parent Program"
      },
      {
        label: "State Operated Residential Educational Facility",
        value: "State Operated Residential Educational Facility"
      }
    ];
  }

  disconnectedCallback() {
    this.Name = '';
    this.ProgramType = '';
    this.EmployeeType = ''
  }

  connectedCallback() {
    if (this.staffid != undefined) {
      fetchDataForAddOrEdit({
        fetchdataname: this.staffid
      }).then(result => {
        this.Name = result[0].Name;
        this.EmployeeType = result[0].EmployeeType__c;
        this.ProgramType = result[0].ProgramType__c;

      });
    }
  }

  @api
  handleSubmit(event) {
    event.preventDefault();
    var inp = [
      ...this.template.querySelectorAll("lightning-input"),
      ...this.template.querySelectorAll("lightning-combobox")
      // ...this.template.querySelectorAll("lightning-radio-group")
    ];
    const allValid = [
        ...this.template.querySelectorAll("lightning-combobox")
      ]
      .reduce((validSoFar, inputFields) => {
        inputFields.reportValidity();
        return validSoFar && inputFields.checkValidity();
      }, true);
    if (allValid) {

      let objStaff = {
        sobjectType: "Contact"
      };

      objStaff.Id = sharedData.getStaffId();
      objStaff.AccountId = sharedData.getProviderId();
      // objStaff.Name = 'testing'; 
      inp.forEach(element => {
        if (element.name == "ProgramType") {
          objStaff.ProgramType__c = element.value;
          // if(!objStaff.ProgramType__c) { 
          //   return this.dispatchEvent(utils.toastMessage("Program Type is mandatory","warning"));
          //  }

        }
      });


      updateOrInsertSOQL({
          objSobjecttoUpdateOrInsert: objStaff
        })


        .then(result => {
          // sharedData.setStaffId(result)
          this.dispatchEvent(utils.toastMessage("Personnel Section Saved Successfully", "success"));
          // this.template.querySelector('c-staff-progress-indicator').closeModal(event);

        })
        .catch(error => {
          this.dispatchEvent(utils.toastMessage("Error in creating record", "error"));

        });
    }

  }
  handleChange(event) {
    this.selectedVals.push(event.detail.value);

  }
}