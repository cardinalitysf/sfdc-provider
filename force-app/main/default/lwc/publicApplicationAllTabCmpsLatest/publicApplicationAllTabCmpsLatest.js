import { LightningElement,track } from 'lwc';
import * as sharedData from 'c/sharedData';

export default class PublicApplicationAllTabCmpsLatest extends LightningElement {
    
//Customizeed Tabset Function Start

    @track showPublicApplicationTabs = true;
    @track NextIcon = false;
    @track PreviousIcon = false;
    @track PreviousValue = [];
    @track NextValue = 0;
    @track tabMenus;
    @track windowWidthIncludeArrow;
    @track LightningTabItems = [
        {name:'tab1',title:'BASIC INFORMATION',value:'BASIC INFORMATION'},
        {name:'tab2',title:'HOME INFORMATION',value:'HOME INFORMATION'},
        {name:'tab3',title:'HOUSEHOLD MEMBERS',value:'HOUSEHOLD MEMBERS'},
        {name:'tab4',title:'BACKGROUND CHECKS',value:'BACKGROUND CHECKS'},
        {name:'tab5',title:'SERVICES',value:'SERVICES'},
        {name:'tab6',title:'HOUSEHOLD CHECKLIST',value:'HOUSEHOLD CHECKLIST'},
        {name:'tab7',title:'HOUSEHOLD MEMBER CHECKLIST',value:'HOUSEHOLD MEMBER CHECKLIST'},
        {name:'tab8',title:'HOME STUDY VISIT',value:'HOME STUDY VISIT'},
        {name:'tab9',title:'CONTACT NOTES',value:'CONTACT NOTES'},
        {name:'tab10',title:'NARRATIVE',value:'NARRATIVE'},
        {name:'tab11',title:'DOCUMENTS',value:'DOCUMENTS'},
        {name:'tab12',title:'REFERENCE CHECKS',value:'REFERENCE CHECKS'},
        {name:'tab13',title:'PET INFO',value:'PET INFO'},
        {name:'tab14',title:'PRE-SERVICE TRAINING',value:'PRE-SERVICE TRAINING'},
        {name:'tab15',title:'PLACEMENT SPECIFICATIONS',value:'PLACEMENT SPECIFICATIONS'},
        {name:'tab16',title:'DECISION',value:'DECISION'},
    ]


    connectedCallback(){


        

        //mousemove
        window.addEventListener('resize', evt => {
            let windowWidth = window.innerWidth;
            this.tabMenus = this.template.querySelectorAll('.slds-tabs_default__item');
            this.windowWidthIncludeArrow = windowWidth - 80; 
            this.NextValue = 0; 
            this.PreviousValue = [];  
            this.handleTabsLoad();
          });
    }

    handleTabsLoad(){
            // let windowWidth = this.template.querySelector('.tabsetwidth').clientWidth;

            this.PreviousValue.push(this.NextValue);

            if(this.NextValue == 0){
                this.PreviousIcon = false;
            }
            else{
                this.PreviousIcon = true;
            }
            let tabMenusClientWidth = 0;
            for(let i=0;i<this.tabMenus.length;i++){
                if(i < this.NextValue){
                    this.tabMenus[i].classList.remove("slds-showd");
                    this.tabMenus[i].classList.add("slds-hide"); 
                }
                else{
                    this.tabMenus[i].classList.remove("slds-hide");
                    this.tabMenus[i].classList.add("slds-showd");  
                }
                this.NextIcon = false;
            }

            let x = 0;
            for(let i=this.NextValue;i<this.tabMenus.length;i++){
                tabMenusClientWidth += this.tabMenus[i].clientWidth;
                if(tabMenusClientWidth > this.windowWidthIncludeArrow){
                    this.tabMenus[i].classList.remove("slds-showd");
                    this.tabMenus[i].classList.add("slds-hide"); 
                    this.NextIcon = true;

                        this.NextValue = i-x;
                        x++;
                }

            }        
    }

    activeTabValue(value){
        for(let i=0;i<this.tabMenus.length;i++){
            if(value == this.tabMenus[i].dataset.id){
                this.tabMenus[i].classList.add("slds-is-active");
            }
            else{
                this.tabMenus[i].classList.remove("slds-is-active");
            }
        }
    }

    handleTabs(event){

        this.activeTabValue(event.target.name);

        let allTabsCount = this.template.querySelectorAll('.slds-tabs_default__content');

        allTabsCount.forEach((item) => {
            let indvRow = item;
            let indvRowId = item.getAttribute('id');
            if (indvRowId.includes(event.target.name+'-')) {
                    indvRow.classList.remove("slds-hide");
                    indvRow.classList.add("slds-showd");

            } else {
                indvRow.classList.add("slds-hide");
                indvRow.classList.remove("slds-showd");
            }
        });

        this.activeTabBtnValue = event.target.name;

        this.handleActiveTabButtons();

    }

    handleNextTabs(){
        this.PreviousIcon = true;
        this.handleTabsLoad();
        
    }
    handlePreviousTabs(){
        this.NextValue = this.PreviousValue[this.PreviousValue.length - 2];
        this.PreviousValue.splice(this.PreviousValue.length-2);
        this.handleTabsLoad();
    } 
//Customizeed Tabset Function End

//Data Exchange Functions for Parent and Child Component

    //Initial App Start
    @track showPublicApplicationDashboard = true;
    @track hideHeaderButton = false;
    @track showPublicApplicationTabs = false;

    @track openServiceInfoModal;
    @track openPetInfoModal;
    @track activeTab = 'basicInfo';
    @track openSessionModal;
    @track homeVisitModal;

    //Button Handling Functions
    @track openSessionModal;
    @track showPop = false;
    @track activeTabBtnValue = null;
    @track showReferenceAddBtn = false;
    @track showAddService = false;
    @track showPetDetails = false;
    @track showAssignTraining = false;
    @track showAddHomeStudy = false;

    //HHM
    @track isView = false;
    @track searchHolder = false;
    @track showResult = false;
    @track meetingFlag;
    @track showListParent;

    @track showPublicProviderAddmember = false;
    @track showPublicProviderAddmemberSearch = false;
    @track sessionHoursFlag = false;
    @track homeStudyCompletedFlag = false;
    @track referenceCheckFlag = false;
    @track prideSessionNotFound = false;

    //supervisor
    @track ApplicationStatus;
    @track ApplicationId;

    get getUserProfileName() {
        return sharedData.getUserProfileName();
     }

    // Active Tab Button show/hide
    handleActiveTabButtons() {
        if (this.activeTabBtnValue === 'tab12') {
            this.showReferenceAddBtn = true;
            this.showAddService = false;
            this.showPetDetails = false;
            this.showAssignTraining = false;
            this.showAddHomeStudy = false;
        } else if (this.activeTabBtnValue === 'tab13') {
            this.showPetDetails = true;
            this.showReferenceAddBtn = false;
            this.showAddService = false;
            this.showAssignTraining = false;
            this.showAddHomeStudy = false;
        } else if (this.activeTabBtnValue === 'tab8') {
            this.showPetDetails = false;
            this.showReferenceAddBtn = false;
            this.showAddService = false;
            this.showAssignTraining = false;
            this.showAddHomeStudy = true;
        } else if (this.activeTabBtnValue === 'tab5') {
            this.showPetDetails = false;
            this.showReferenceAddBtn = false;
            this.showAddService = true;
            this.showAssignTraining = false;
            this.showAddHomeStudy = false;
        } else if (this.activeTabBtnValue === 'tab14') {
            this.showPetDetails = false;
            this.showReferenceAddBtn = false;
            this.showAddService = false;
            this.showAssignTraining = true;
            this.showAddHomeStudy = false;
        } else{
            this.showPetDetails = false;
            this.showReferenceAddBtn = false;
            this.showAddService = false;
            this.showAssignTraining = false;
            this.showAddHomeStudy = false;
        }
        this.supervisor();
    }

    redirectToServiceInfo(event) {
        this.openServiceInfoModal = true;
        this.activeTab = "AddServices";
    }
    handleToggleComponentsForService(event) {
        this.openServiceInfoModal = false;
    }
    redirectToPetInfo(event) {
        this.openPetInfoModal = true;
        this.activeTab = "PetInfo";
    }

    handleToggleComponents(event) {
        this.openPetInfoModal = false;
    }

    redirectToSessionInfo(event) {
        this.openSessionModal = true;
        this.activeTab = "TrainService";
    }

    handleToggleSessionComponents(event) {
        this.openSessionModal = false;
    }

    redirecttoStudyVisit(event) {
        this.homeVisitModal = true;
        this.activeTab = "StudyVisit";
    }
    handleToggleComponentsForHomeVisit(event) {
        this.homeVisitModal = false;
    }
    redirectToAddRefPop() {
        this.showPop = true;
    }

    redirectAddOff(event) {
        this.showPop = event.detail.first;
    }


    //HHM
    handleAddMemberSearch(event) {
        this.showPublicProviderAddmemberSearch = true;
        this.showPublicProviderAddmember = event.detail.first;
        this.showPublicApplicationTabs = event.detail.first;
        this.showResult = false;
        this.hideHeaderButton = false;
    }

    handleAddMember() {
        this.showPublicProviderAddmember = true;
        this.searchHolder = false;
        this.showPublicApplicationTabs = false;
        this.hideHeaderButton = false;
    }

    handleAddMemberView(event) {
        this.showPublicProviderAddmember = true;
        this.isView = true;
        this.hideHeaderButton = false;
    }
    handleMeeting(event) {
        this.meetingFlag = event.detail.meetingFlag;
    }

    redirectToHousHoldDashboard(event) {
        this.showPublicProviderAddmember = event.detail.first;
        this.activeTab = 'houseHoldMember';
        this.showPublicApplicationTabs = true;
        this.hideHeaderButton = true;
    }
    handleAddMemberSearchFromAddHouse(event) {
        this.showPublicProviderAddmemberSearch = true;
        this.showPublicProviderAddmember = event.detail.first;
        this.showPublicApplicationTabs = event.detail.first;
        this.showResult = true;
        this.hideHeaderButton = false;

    }
    handleAddMemberHome(event) {
        this.showPublicProviderAddmemberSearch = event.detail.first;
        this.activeTab = 'houseHoldMember';
        this.showPublicApplicationTabs = true;
        this.hideHeaderButton = true;
        setTimeout(() => {
            this.NextValue = 0;
            let windowWidth = window.innerWidth;
            this.tabMenus = this.template.querySelectorAll('.slds-tabs_default__item');
            this.windowWidthIncludeArrow = windowWidth - 80;
            this.handleTabsLoad();
            this.handleTabs({target:{name:'tab3'}});

        }, 500);
    }
    redirectHandleAddMember(event) {
        this.showPublicProviderAddmemberSearch = event.detail.first;
        this.showListParent = event.detail.searchLists;
        this.showPublicProviderAddmember = true;
        this.searchHolder = true;
        this.hideHeaderButton = false;
    }
    handleComarRegulations(event) {
      
    }
    handleSessionFlag(event) {
        this.sessionHoursFlag = event.detail.sessionFlag;
    }
    handleHomeStudyCompletedFlag(event) {
        this.homeStudyCompletedFlag = event.detail.homestudycompletedflag;
    }
    handleReferenceCheckFlag(event) {
        this.referenceCheckFlag = event.detail.referencecheckflag;
    }

    redirectToPubAppTabs(event) {
        this.showPublicApplicationDashboard = event.detail.first;
        this.hideHeaderButton = !event.detail.first;
        this.showPublicApplicationTabs = !event.detail.first;
        this.ApplicationStatus=sharedData.getApplicationStatus();
        this.supervisor();
        setTimeout(() => {
            let windowWidth = window.innerWidth;
            this.tabMenus = this.template.querySelectorAll('.slds-tabs_default__item');
            this.windowWidthIncludeArrow = windowWidth - 80;
            this.handleTabsLoad();
            this.handleTabs({target:{name:'tab1'}});
            

        }, 500);
    }

    redirectToDashboard(event) {
        this.showPublicApplicationDashboard = event.detail.first;
        this.hideHeaderButton = !event.detail.first;
        this.showPublicApplicationTabs = !event.detail.first;
    }

    //sp hide button
    handleSupervisorButton(event) {
        this.showAssignTraining = event.detail.first;
    }

    handlePrivateAppTabInfo(event) {
        this.showPrivateApplicationTabs = !event.detail.first;
        this.showPublicApplicationDashboard = event.detail.first
    }
    supervisor(){
        if (this.getUserProfileName != "Supervisor") {
            if (
              [
                "Caseworker Submitted",
                "Approved",
                "Rejected",
                "Supervisor Rejected"
              ].includes(this.ApplicationStatus)
            ) {
                this.showPetDetails = false;
                this.showReferenceAddBtn = false;
                this.showAddService = false;
                this.showAssignTraining = false;
                this.showAddHomeStudy = false;
            }
          }
          if (this.getUserProfileName == "Supervisor") {
            if (["Approved", "Supervisor Rejected"].includes(this.ApplicationStatus)) {
                this.showPetDetails = false;
                this.showReferenceAddBtn = false;
                this.showAddService = false;
                this.showAssignTraining = false;
                this.showAddHomeStudy = false;        
                }
          }
        }
        handlePrideFlag(event){
            this.prideSessionNotFound = event.detail.pridesessionnotfound;
        }

}