/**
 * @Author        : Sundar Karuppalagu
 * @CreatedOn     : JUNE 02 ,2020
 * @Purpose       : Public Application Screen Decision Process
 **/

import { LightningElement, wire, track, api } from 'lwc';

import getDecisionDetails from '@salesforce/apex/PublicApplicationDecision.getDecisionDetails';
import getRecommendationPickListValues from '@salesforce/apex/PublicApplicationDecision.getRecommendationPickListValues';
import getAllSuperUsersList from '@salesforce/apex/PublicApplicationDecision.getAllSuperUsersList';
import getAssignedSuperUserTableDetails from '@salesforce/apex/PublicApplicationDecision.getAssignedSuperUserTableDetails';
import saveSign from '@salesforce/apex/PublicApplicationDecision.saveSign';
//import getBase64Image from '@salesforce/apex/applicationLicenseInfo.getSignatureAsBas64Image';

import { updateRecord } from 'lightning/uiRecordApi';
//import { loadStyle } from 'lightning/platformResourceLoader';

import ID_FIELD from '@salesforce/schema/Application__c.Id';
import COMMONDECISION_FIELD from '@salesforce/schema/Application__c.CommonDecision__c';
import APPROVALCOMMENTS_FIELD from '@salesforce/schema/Application__c.ApprovalComments__c';
import SUPERVISORCOMMENTS_FIELD from '@salesforce/schema/Application__c.SupervisorComments__c';
import APPLICATIONSTATUS_FIELD from '@salesforce/schema/Application__c.Status__c';
import SUPERVISOR_FIELD from '@salesforce/schema/Application__c.Supervisor__c';
import CASEWORKER_FIELD from '@salesforce/schema/Application__c.Caseworker__c';
import SUBMITTEDDATE_FIELD from '@salesforce/schema/Application__c.SubmittedDate__c';
//import SIGNATURE_FIELD from '@salesforce/schema/Application__c.IsSignature__c';
import userId from '@salesforce/user/Id';
import images from '@salesforce/resourceUrl/images';
//import myResource from '@salesforce/resourceUrl/styleSheet';

import { CJAMS_CONSTANTS } from 'c/constants';
import { utils } from 'c/utils';
import * as sharedData from "c/sharedData";

//Supervisor Flow Check
import updateapplicationdetails from '@salesforce/apex/PublicApplicationDecision.updateapplicationdetails';

//HomeStudyCheck
import getHomeStudyDetails from "@salesforce/apex/PublicApplicationHomeStudyVisit.getHomeStudyDetails";
import getSessionTraining from '@salesforce/apex/PublicApplicationServiceTraining.getSessionTraining';
import getSessionAttendedHours from '@salesforce/apex/PublicApplicationServiceTraining.getSessionAttendedHours';
import fetchRefCheckedMemDetails from '@salesforce/apex/PublicApplicationReferenceChecks.fetchRefCheckedMemDetails';
import getRecordType from '@salesforce/apex/PublicApplicationReferenceChecks.getRecordType';

//Assinged Super User Data Table Header Columns Declaration
const assignedSuperUserTableHeader = [{
        label: 'Assigned Date',
        fieldName: 'SubmittedDate__c',
        type: 'text'
    }, {
        label: 'Author',
        fieldName: 'caseWorkerName',
        type: 'text'
    }, {
        label: 'Designation',
        fieldName: 'caseWorkerDesignation',
        type: 'text'
    }, {
        label: 'Assigned To',
        fieldName: 'superVisorName',
        type: 'text'
    }, {
        label: 'Designation',
        fieldName: 'superVisorDesignation',
        type: 'text'
    }, {
        label: 'Status',
        fieldName: 'assignedStatus',
        type: 'text'
    }, {
        label: 'Action',
        fieldName: 'Id',
        type: "button",
        typeAttributes: {
            iconName: 'utility:preview',
            name: 'View',
            title: 'Click to Preview',
            variant: 'border-filled',
            class: 'view-red',
            disabled: false,
            iconPosition: 'left',
            target: '_self'
        }
    }
]

//Tier Approval Modal for Supervisor Header Columns Declaration
const superUserTableColumns = [{
        label: 'Name',
        fieldName: 'Name',
        type: 'text'
    }, {
        label: 'CaseLoad',
        fieldName: 'CaseLoad__c',
        type: 'number'
    }, {
        label: 'Availability',
        fieldName: 'Availability__c',
        type: 'text'
    }, {
        label: 'Zip Code',
        fieldName: '',
        type: 'text'
    }, {
        label: 'Type Of Worker',
        fieldName: 'ProfileName',
        type: 'text'
    }, {
        label: 'Unit',
        fieldName: 'Unit__c',
        type: 'text'
    }
];

//Signature model declaration
let isDownFlag,
    isDotFlag = false,
    prevX = 0,
    currX = 0,
    prevY = 0,
    currY = 0;

let x = "#0000A0"; //blue color
let y = 1.5; //weight of line width and dot
let canvasElement, ctx; //storing canvas context
let attachment; //holds attachment information after saving the sigture on canvas
let dataURL, convertedDataURI; //holds image data

//Class Name declaration in LWC
export default class PublicApplicationDecision extends LightningElement {
    //Fields declaration
    @track decisionDetails = {};
    @track recommendationOptions = [];

    //Assign Super User Modal Table declaration
    @track superUserTableColumns = superUserTableColumns;
    @track superUsersTableData = [];
    @track openSuperUserModel = false;
    @track assignBtnDisable = false;
    @track selectedRows = [];

    //Fetch Assigned Super User Table Data Declaration
    @track assignedSuperUserTableHeader = assignedSuperUserTableHeader;
    @track assignedSuperUserTableData = [];
    @track isSuperUserAssigned = false;
    @track openPreviewModel = false;
    @track previewDetails = {};
    isSuperUserSelected = false;
    Supervisor__c;
    Caseworker__c;
    SubmittedDate__c;

    //Supervisor Flow Declarations
    @track workItemId;
    superUserSelectedReviewStatus;

    //Disable fields using flags
    @track isRecommendationDisable = false;
    @track isCommentsDisable = false;
    @track isSignatureDisable = false;
    @track isTierApprovalBtnDisable = false;
    @track isDraftBtnDisable = false;
    @track isSubmitBtnDisable = false;

    //Buttons visibilty check using flags
    @track showSignatureBtn = false;
    @track showTierofApprovalBtn = false;

    //Decision Icon Image
    decisionIcon = images + '/decision-icon.svg';
    refreshIcon = images + '/refresh-icon.svg';

    applicationStatus;

    //Image Base64 format initialization
    imageBase64Format;
    isSignatureRemoved = false;

    //Signature fields declaration
    @track openSignatureModel = false;
    @track getContext;
    SignatureValidation;
    @track SignatureValidationView;
    @track SignatureShowHide = false;
    @track Spinner = true;

    @track recordTypeId = null;

    //_sessionHrFlag = false;
    //_homeStudyFlag = false;
    //_prideSessionNotFound = false;
    _serviceTrainingNotFound = false;
    _27HrsNotCompleted = false;
    _homeStudyCompletedFlag = false;
    _comarReferenceCheckCompleted = false;
    _threeRef = false;
    _twoRefF2F = false;
    _twoRefRelation = false;

    @track referenceMembers = [];

    @api homeStudyCompletedFlag;
    @api sessionHoursFlag;
    @api referenceCheckFlag;
    @api prideSessionNotFound;

    /*@api
    get prideSessionNotFound () {
        return this._prideSessionNotFound;
    }

    set prideSessionNotFound(value) {
        this._prideSessionNotFound = value;
    }

    @api
    get sessionHoursFlag () {
        return this._sessionHrFlag;
    }

    set sessionHoursFlag(value) {
        this._sessionHrFlag = value;
    }

    @api
    get homeStudyCompletedFlag () {
        return this._homeStudyFlag;
    }

    set homeStudyCompletedFlag(value) {
        this._homeStudyFlag = value;
    }

    @api referenceCheckFlag = false;*/

    //Get Application ID
    get applicationId() {
        return sharedData.getApplicationId();
    }

    get providerId() {
        return sharedData.getProviderId();
    }

    //Get Super User Profile
    get isSuperUserFlow() {
        let userProfile = sharedData.getUserProfileName();

        if (userProfile === 'Supervisor')
            return true;
        else
            return false;
    }

    //Initial Loading Method
    connectedCallback() {
        //this.homeStudyCompletedFlag;
        //this.sessionHoursFlag;
        getDecisionDetails({
            applicationId: this.applicationId
        })
        .then(result => {
            if (result.length > 0) {
                this.applicationStatus = result[0].Status__c;

                if (this.isSuperUserFlow === false) {
                    //Getting fields value from Database
                    this.decisionDetails.CommonDecision__c = result[0].CommonDecision__c == CJAMS_CONSTANTS.CONTRACT_STATUS_RETURNED ? '' : result[0].CommonDecision__c;                    
                    this.decisionDetails.ApprovalComments__c = result[0].ApprovalComments__c;
                    this.superUserSelectedReviewStatus = ['Approved', 'Returned'].includes(result[0].CommonDecision__c) ? result[0].CommonDecision__c : "";
    
                    if (['Caseworker Submitted', 'Approved'].includes(result[0].Status__c) && ['Submitted', 'Approved'].includes(result[0].CommonDecision__c)) {
                        //Condition 1
                        this.isRecommendationDisable = true;
                        this.isCommentsDisable = true;
                        this.isSignatureDisable = true;
                        this.isTierApprovalBtnDisable = true;
                        this.isDraftBtnDisable = true;
                        this.isSubmitBtnDisable = true;
                        this.showSignatureBtn = true;
                        this.showTierofApprovalBtn = false;
                    } else if (['Rejected', 'Supervisor Rejected', 'Assigned for Training'].includes(result[0].Status__c) && ['Rejected', 'Submit for Training'].includes(result[0].CommonDecision__c)) {
                        //Condition 2
                        this.isRecommendationDisable = true;
                        this.isCommentsDisable = true;
                        this.isSignatureDisable = true;
                        this.isTierApprovalBtnDisable = true;
                        this.isDraftBtnDisable = true;
                        this.isSubmitBtnDisable = true;
                        this.showSignatureBtn = false;
                        this.showTierofApprovalBtn = false;
                    } else if (['Training Completed', 'Training Incompleted'].includes(result[0].Status__c) && result[0].CommonDecision__c == 'Submit for Training') {
                        this.isRecommendationDisable = false;
                        this.isCommentsDisable = false;
                        this.isSignatureDisable = false;
                        this.isTierApprovalBtnDisable = false;
                        this.isDraftBtnDisable = false;
                        this.isSubmitBtnDisable = true;
                        this.showSignatureBtn = false;
                        this.showTierofApprovalBtn = false;
                    } else if (this.decisionDetails.CommonDecision__c == 'Rejected' || this.decisionDetails.CommonDecision__c == 'Submit for Training') {
                        //Condition 3
                        this.isRecommendationDisable = false;
                        this.isCommentsDisable = false;
                        this.isSignatureDisable = false;
                        this.isTierApprovalBtnDisable = false;
                        this.isDraftBtnDisable = false;
                        this.isSubmitBtnDisable = false;
                        this.showSignatureBtn = false;
                        this.showTierofApprovalBtn = false;
                    } else if (this.decisionDetails.CommonDecision__c == 'Submitted') {
                        //Condition 4
                        this.isRecommendationDisable = false;
                        this.isCommentsDisable = false;
                        this.isSignatureDisable = false;
                        this.isTierApprovalBtnDisable = false;
                        this.isDraftBtnDisable = false;
                        this.isSubmitBtnDisable = false;
                        this.showSignatureBtn = true;
                        this.showTierofApprovalBtn = true;
                    } else {
                        //Condition 5
                        this.isRecommendationDisable = false;
                        this.isCommentsDisable = false;
                        this.isSignatureDisable = true;
                        this.isTierApprovalBtnDisable = true;
                        this.isDraftBtnDisable = false;
                        this.isSubmitBtnDisable = true;
                        this.showSignatureBtn = false;
                        this.showTierofApprovalBtn = false;
                    }

                    if ((this.decisionDetails.CommonDecision__c == undefined || this.decisionDetails.CommonDecision__c == null || this.decisionDetails.CommonDecision__c == 'Submit for Training') && ['Pending', 'Assigned for Training'].includes(this.applicationStatus)) {
                        this.serviceTrainingCheck();
                    } else {
                        this.getAssignedSuperUserTableValues();
                    }
                } else {
                    //Getting fields value from Database
                    this.decisionDetails.CommonDecision__c = result[0].CommonDecision__c == CJAMS_CONSTANTS.CONTRACT_STATUS_SUBMITTED ? "" : result[0].CommonDecision__c;
                    this.decisionDetails.SupervisorComments__c = result[0].SupervisorComments__c;
                    this.superUserSelectedReviewStatus = result[0].CommonDecision__c;
    
                    if (['Approved'].includes(result[0].Status__c)) {
                        //Condition 1
                        this.isRecommendationDisable = true;
                        this.isCommentsDisable = true;
                        this.isSignatureDisable = true;
                        this.isTierApprovalBtnDisable = true;
                        this.isSubmitBtnDisable = true;
                        this.isSuperUserAssigned = true;
                        this.showSignatureBtn = true;
                        this.showTierofApprovalBtn = true;
                    } else if (['Supervisor Rejected'].includes(result[0].Status__c)) {
                        //Condition 2
                        this.isRecommendationDisable = true;
                        this.isCommentsDisable = true;
                        this.isSignatureDisable = true;
                        this.isTierApprovalBtnDisable = true;
                        this.isSubmitBtnDisable = true;
                        this.isSuperUserAssigned = false;
                        this.showSignatureBtn = false;
                        this.showTierofApprovalBtn = false;
                    } else {
                        //Condition 3
                        this.isRecommendationDisable = false;
                        this.isCommentsDisable = false;
                        this.isSignatureDisable = false;
                        this.isTierApprovalBtnDisable = false;
                        this.isSubmitBtnDisable = false;
                        this.isSuperUserAssigned = true;
                        this.showSignatureBtn = true;
                        this.showTierofApprovalBtn = false;
                    }
                    this.getAssignedSuperUserTableValues();
                }
                this.getPickListValues();
            }
        })
    }

    getAssignedSuperUserTableValues() {
        getAssignedSuperUserTableDetails({
            applicationId: this.applicationId
        })
        .then(result => {
            if (result.length == 0)
                return this.isSuperUserAssigned = false;

            result = JSON.parse(result);

            this.SignatureValidation = result[0].signatureUrl;
            let resultArray = [];

            if (this.isSuperUserFlow === false && result[0].applicationRecord.Supervisor__c !== undefined) {
                this.isSuperUserAssigned = true;

                resultArray.push({
                    SubmittedDate__c: utils.formatDate(result[0].applicationRecord.SubmittedDate__c),
                    caseWorkerName: result[0].applicationRecord.Caseworker__r.Name,
                    caseWorkerDesignation: result[0].applicationRecord.Caseworker__r.Unit__c,
                    assignedStatus: result[0].applicationRecord.CommonDecision__c,
                    superVisorName: result[0].applicationRecord.Supervisor__r.Name,
                    comments: result[0].applicationRecord.ApprovalComments__c,
                    superVisorDesignation: 'Supervisor',
                    SignatureValue: result[0].signatureUrl,
                    supervisorID: result[0].applicationRecord.Supervisor__c
                });
                this.assignedSuperUserTableData = [...resultArray];
                this.selectedRows.splice(0, this.selectedRows.length);
                this.selectedRows.push(this.assignedSuperUserTableData[0].supervisorID);

                if (!this.SignatureValidation) {
                    this.SignatureShowHide = false;
                } else {
                    this.SignatureShowHide = true;
                    this.SignatureValidationView = '/sfc/servlet.shepherd/version/download/' + result[0].signatureUrl;
                }
            }

            for (var i=0; i<result.length; i++) {
                if (result[i].applicationRecord.Steps !== undefined) {
                    if (result[i].applicationRecord.Steps.totalSize === 1) {
                        let signUrlIndex = result[i].signatureUrl.findIndex(sign => sign.OwnerId === result[i].applicationRecord.Steps.records[0].ActorId);
                        let signUrl = signUrlIndex >= 0 ? result[i].signatureUrl[signUrlIndex].Id : '';
                        resultArray.push({
                            caseWorkerName: result[i].applicationRecord.Steps.records[0].Actor.Name,
                            assignedStatus: result[i].applicationRecord.Steps.records[0].StepStatus == 'Started' ? 'Submitted' : (result[i].applicationRecord.StepStatus == 'Removed' ? 'Returned' : result[0].applicationRecord.StepStatus),
                            superVisorName: result[i].applicationRecord.Workitems.records[0].Actor.Name,
                            comments: result[i].applicationRecord.Steps.records[0].Comments,
                            caseWorkerDesignation: result[i].applicationRecord.Steps.records[0].Actor.Profile.Name == 'Supervisor' ? 'Supervisor' : 'OLM',
                            superVisorDesignation: result[i].applicationRecord.Workitems.records[0].Actor.Profile.Name,
                            SubmittedDate__c: utils.formatDate(result[i].applicationRecord.Steps.records[0].CreatedDate),
                            SignatureValue: signUrl
                        });
                    } else if (result[i].applicationRecord.Steps.totalSize > 1) {
                        let datas = result[i].applicationRecord.Steps.records;
                        for (let data in datas) {
                            let signUrlIndex = result[i].signatureUrl.findIndex(sign => sign.OwnerId === result[i].applicationRecord.Steps.records[i].ActorId);
                            let signUrl = signUrlIndex >= 0 ? result[i].signatureUrl[signUrlIndex].Id : '';
                            resultArray.push({
                                caseWorkerName: datas[data].Actor.Name,
                                assignedStatus: datas[data].StepStatus == 'Started' ? 'Submitted' :  (datas[data].StepStatus == 'Removed' ? 'Returned' : datas[data].StepStatus),
                                superVisorName: datas[parseInt(data) - 1] !== undefined ? datas[parseInt(data) - 1].Actor.Name : ' - ',
                                comments: datas[data].Comments,
                                caseWorkerDesignation: datas[data].Actor.Profile.Name == 'Supervisor' ? 'Supervisor' : 'OLM',
                                superVisorDesignation: datas[parseInt(data) - 1] !== undefined ? datas[data - 1].Actor.Profile.Name : ' - ',
                                SubmittedDate__c: utils.formatDate(datas[data].CreatedDate),
                                SignatureValue: signUrl
                            });

                            if(datas[data].StepStatus == "Removed" && this.isSuperUserFlow === false){
                                this.selectedRows.push(datas[data].Actor.Id);
                            }
                        }
                    }
    
                    if (result[i].applicationRecord.Steps.totalSize === 1)
                        this.workItemId = result[i].applicationRecord.Workitems.records[0].Id;

                    this.isSuperUserAssigned = true;
                }
            }
            this.assignedSuperUserTableData = [...resultArray];

            if (resultArray.length > 0) {
                this.SignatureShowHide = true;
                if (resultArray.length > 1) {
                    if (this.isSuperUserFlow === true) {
                        this.SignatureValidationView = '/sfc/servlet.shepherd/version/download/' + resultArray[resultArray.length - 2].SignatureValue;
                    } else {
                        this.SignatureValidationView = '/sfc/servlet.shepherd/version/download/' + resultArray[resultArray.length - 1].SignatureValue;
                    }
                } else {
                    if (this.isSuperUserFlow === true) {
                        this.SignatureShowHide = false;
                    } else {
                        this.SignatureValidationView = '/sfc/servlet.shepherd/version/download/' + resultArray[resultArray.length - 1].SignatureValue;
                    }
                }
            }/* else {
                this.SignatureShowHide = false;
            }*/

            this.homeStudyCheck();
            //this.comarRecordType();
            //this.Spinner = false;
        })
    }

    //Get Picklist values
    getPickListValues() {
        getRecommendationPickListValues({
            objInfo: {
                sobjectType: 'Application__c'
            },
            picklistFieldApi: 'CommonDecision__c'
        })
        .then(data => {
            if (data.length > 0) {
                let picklistArray = [];

                if (this.isSuperUserFlow) {
                    data.forEach(key => {
                        if (['Approve', 'Return to Caseworker', 'Reject'].includes(key.label)) {
                            picklistArray.push({
                                label: key.label,
                                value: key.value
                            });
                        }
                    })
                } else {
                    if (!this.superUserSelectedReviewStatus) {
                        if (['Pending', 'Assigned for Training'].includes(this.applicationStatus)) {
                            data.forEach(key => {
                                if (['Submit for Training', 'Reject'].includes(key.label)) {
                                    picklistArray.push({
                                        label: key.label,
                                        value: key.value
                                    })
                                }
                            });
                        } else if (['Training Completed', 'Training Incompleted', 'Caseworker Submitted', 'Rejected'].includes(this.applicationStatus)) {
                            data.forEach(key => {
                                if (['Submit for Review', 'Reject'].includes(key.label)) {
                                    picklistArray.push({
                                        label: key.label,
                                        value: key.value
                                    })
                                }
                            });
                        }
                    } else {
                        if (['Returned'].includes(this.applicationStatus)) {
                            data.forEach(key => {
                                if (['Submit for Review', 'Reject'].includes(key.label)) {
                                    picklistArray.push({
                                        label: key.label,
                                        value: key.value
                                    })
                                }
                            });
                        } else {
                            data.forEach(key => {
                                if (['Reject', 'Approve', 'Return to Caseworker'].includes(key.label)) {
                                    picklistArray.push({
                                        label: key.label,
                                        value: key.value
                                    })
                                }
                            })
                        }
                    }
                }
                this.recommendationOptions = [...picklistArray];
            }
        })
        .catch(error => {
            this.dispatchEvent(utils.toastMessage("Error in fetching Status Picklist Values. please check", "warning"));
        })
    }

    serviceTrainingCheck() {
        getSessionTraining({
            applicationid: this.applicationId
        }).then(result => {
            this._serviceTrainingNotFound = result.length > 0 ? true : false;
            this.Spinner = false;
        })
    }

    homeStudyCheck() {
        getHomeStudyDetails({
            fetchdataname: this.applicationId
        })
        .then(result => {
            this._homeStudyCompletedFlag = result.length > 0 ? true : false;
            this.serviceTraining27hrsCompletedCheck();
        })
    }

    serviceTraining27hrsCompletedCheck() {
        getSessionAttendedHours({
            applicationid: this.applicationId
        })
        .then((result) => {
            if (result.length > 0) {
                let hours = Math.floor(result[0].totalhours / 60);
                this._27HrsNotCompleted = hours >= 27 ? true : false;
                this.comarRecordType();
            } else {
                this._27HrsNotCompleted = false;
                this.comarRecordType();
            }
        })
    }

    comarRecordType() {
        getRecordType({
            name: 'Reference Check'
        })
        .then(result => {
            if (result) {
                this.recordTypeId = result;
                this.referenceCheck();
            } else {
                this._comarReferenceCheckCompleted = false;
                this.Spinner = false;
            }
        })
    }

    referenceCheck() {
        fetchRefCheckedMemDetails({
            provId: this.providerId,
            recId: this.recordTypeId
        })
        .then(result => {
            if (result.length > 0) {
                this._comarReferenceCheckCompleted = true;
                this.referenceMembers = result.map(data => {
                    return {
                        Id : data.Id,
                        Date__c : data.Date__c,
                        Relation__c : data.Relation__c,
                        Name : data.Actor__r == undefined ? (data.FirstName__c !== undefined ? data.FirstName__c + ' ' + data.LastName__c : data.LastName__c) : data.Actor__r.Contact__r.FirstName__c + ' ' + data.Actor__r.Contact__r.LastName__c,
                        ReferenceRecommends__c : data.ReferenceRecommends__c !== undefined ? data.ReferenceRecommends__c : '',
                        TypeofContact__c : data.TypeofContact__c !== undefined ? data.TypeofContact__c : '',
                        SchoolReferenceCheck__c : data.SchoolReferenceCheck__c !== undefined ? data.SchoolReferenceCheck__c : '',
                        Comments__c : data.Comments__c !== undefined ? data.Comments__c : ' - ',
                        commentsClass : data.Comments__c !== undefined ? 'btnbdrIpad blue' : 'svgRemoveicon',
                        RelationshiptoApplicant__c : data.RelationshiptoApplicant__c,
                        actionClass : 'actionClass'
                    }
                });
                this.comarRegulationsSet();
            } else {
                this._comarReferenceCheckCompleted = false;
                this.Spinner = false;
            }
        })
    }

    comarRegulationsSet() {
        if (this.referenceMembers.length >= 3) {
            this._threeRef = true;
        }
        if (this.referenceMembers.filter(ref => ref.TypeofContact__c === 'Face-to-face').length >= 2) {
            this._twoRefF2F = true;
        }
        if (this.referenceMembers.filter(ref => ref.Relation__c === 'Non Relative').length >= 2) {
            this._twoRefRelation = true;
        }
        this.Spinner = false;
    }

    //Fields On Change method
    decisionOnChange(event) {
        if (event.target.name == 'Recommendation') {
            if (this.isSuperUserFlow === true) {
                this.isSubmitBtnDisable = ['Approved', 'Rejected', 'Returned'].includes(event.detail.value) == -1 ? true : false;
                this.decisionDetails.CommonDecision__c = event.detail.value;

                this.isSignatureDisable = false;
                this.isTierApprovalBtnDisable = false;
                this.showSignatureBtn = true;
                this.showTierofApprovalBtn = false;
            } else {
                this.isSubmitBtnDisable = false;
                this.decisionDetails.CommonDecision__c = event.detail.value;

                if (['Submit for Training', 'Rejected'].includes(event.detail.value)) {
                    this.isSignatureDisable = false;
                    this.isTierApprovalBtnDisable = false;
                    this.showSignatureBtn = false;
                    this.showTierofApprovalBtn = false;
                } else if (['Submitted', 'Approved', 'Returned'].includes(event.detail.value)) {
                    this.isSignatureDisable = false;
                    this.isTierApprovalBtnDisable = false;
                    this.showSignatureBtn = true;
                    this.showTierofApprovalBtn = true;
                }
                this.isSuperUserAssigned = ['Submit for Training', 'Rejected'].includes(event.detail.value) ? false : (this.assignedSuperUserTableData.length > 0 ? true : false);
            }
        } else if (event.target.name == 'Comments') {
            if (this.isSuperUserFlow === true) {
                this.decisionDetails.SupervisorComments__c = event.target.value;
            } else {
                this.decisionDetails.ApprovalComments__c = event.target.value;
            }
        }
    }

    //Get Supervisor Users List to assign
    @wire(getAllSuperUsersList)
    wiredSuperUserDetails({ error, data }) {
        if (data) {
            if (data.length > 0) {
                this.superUsersTableData = data.filter(item => item.Profile.Name == 'Supervisor');
            } else {
                this.dispatchEvent(utils.toastMessage("Supervisor users not found. please check.", "warning"));
            }
        } else if (error) {
            this.dispatchEvent(utils.toastMessage("Error in fetching Supervisor users. please check", "warning"));
        }
    }

    //On Click event for Tier Approval Model
    handleSuperUserModel() {
        if (!this.decisionDetails.CommonDecision__c)
            return this.dispatchEvent(utils.toastMessage("Please select status", "warning"));

        if (this.isSuperUserFlow === true)
            return this.dispatchEvent(utils.toastMessage("Sorry, you do not have access to add superusers.", "warning"));

        this.openSuperUserModel = true;
        this.assignBtnDisable = true;
        let localvar = [...this.superUsersTableData];
        this.superUsersTableData = localvar.map((row) => {
            return Object.assign({
                ZipCode: '',
                ProfileName: row.Profile.Name
            }, row)
        });
    }

    //Row Selection in Super User Model
    handleSuperUserRowSelection(event) {
        this.isSuperUserSelected = true;
        this.Supervisor__c = event.detail.selectedRows[0].Id
        this.Caseworker__c = userId;
        this.assignBtnDisable = false;
    }

    //Assigning Super User 
    handleAssignMethod() {
        if (!this.Supervisor__c)
            return this.dispatchEvent(utils.toastMessage("Atleast one supervisor selection is mandatory", "warning"));

        this.assignBtnDisable = true;

        let currentDate = new Date().getDate() < 10 ? '0' + new Date().getDate() : new Date().getDate();
        let currentMonth = ((new Date().getMonth()) + 1) < 10 ? '0' + ((new Date().getMonth()) + 1) : ((new Date().getMonth()) + 1);

        this.SubmittedDate__c = new Date().getFullYear() + '-' + currentMonth + '-' + currentDate;

        const fields = {};

        fields[ID_FIELD.fieldApiName] = this.applicationId;
        fields[COMMONDECISION_FIELD.fieldApiName] = this.decisionDetails.CommonDecision__c;
        fields[APPROVALCOMMENTS_FIELD.fieldApiName] = this.decisionDetails.ApprovalComments__c;
        fields[SUPERVISOR_FIELD.fieldApiName] = this.Supervisor__c;
        fields[CASEWORKER_FIELD.fieldApiName] = this.Caseworker__c;
        fields[SUBMITTEDDATE_FIELD.fieldApiName] = this.SubmittedDate__c;

        const recordInput = {
            fields
        };

        updateRecord(recordInput)
            .then(() => {
                this.assignBtnDisable = false;
                this.getAssignedSuperUserTableValues();
                this.openSuperUserModel = false;

                this.dispatchEvent(utils.toastMessage("Supervisor has been assigned successfully", "success"));
            })
            .catch(error => {
                this.assignBtnDisable = false;
                this.openSuperUserModel = false;
                this.dispatchEvent(utils.toastMessage("Error found in assigning supervisor. please check", "warning"));
            })
    }

    //Close Super User Model
    superUserCloseModel() {
        this.openSuperUserModel = false;
        this.assignBtnDisable = false;
    }

    //Preview Model Action method
    handleRowAction(event) {
        this.openPreviewModel = true;

        this.previewDetails = {
            authorName: event.detail.row.caseWorkerName,
            assignedDate: event.detail.row.SubmittedDate__c,
            assignedStatus: event.detail.row.assignedStatus,
            comments: event.detail.row.comments,
            SignatureValue: '/sfc/servlet.shepherd/version/download/' + event.detail.row.SignatureValue
        }
    }

    //This method used to Close the Preview Model when user has clicked DONE/Close Button
    previewCloseModel() {
        this.openPreviewModel = false;
    }

    /**********Canvas Signature******/

    //Open Signature Model
    handleSignatureModel() {
        this.openSignatureModel = true;
        canvasElement = this.template.querySelector('.sign-box');
        ctx = canvasElement.getContext("2d");
        //this.drawCanvasImage();
    }

    //Upload Signature from Local System
    handleFilesChange(event) {
        let strFileNames = '';

        const uploadedFiles = event.detail.files;

        for (let i = 0; i < uploadedFiles.length; i++) {
            strFileNames += uploadedFiles[i].name + ', ';
        }

        this.dispatchEvent(utils.toastMessage(`${strFileNames} files uploaded successfully`, "success"));
    }

    //clear the signature from canvas
    handleClearClick() {
        ctx.clearRect(0, 0, canvasElement.width, canvasElement.height);
        this.isSignatureRemoved = true;
        //dataURL = "";
    }

    //event listeners added for drawing the signature within shadow boundary
    constructor() {
        super();
        this.template.addEventListener('mousemove', this.handleMouseMove.bind(this));
        this.template.addEventListener('mousedown', this.handleMouseDown.bind(this));
        this.template.addEventListener('mouseup', this.handleMouseUp.bind(this));
        this.template.addEventListener('mouseout', this.handleMouseOut.bind(this));
    }

    //handler for mouse move operation
    handleMouseMove(event) {
        this.searchCoordinatesForEvent('move', event);
    }

    //handler for mouse down operation
    handleMouseDown(event) {
        this.searchCoordinatesForEvent('down', event);
    }

    //handler for mouse up operation
    handleMouseUp(event) {
        this.searchCoordinatesForEvent('up', event);
    }

    //handler for mouse out operation
    handleMouseOut(event) {
        this.searchCoordinatesForEvent('out', event);
    }

    handleSaveClick() {
        //set to draw behind current content
        ctx.globalCompositeOperation = "destination-over";
        ctx.fillStyle = "#FFF"; //white
        ctx.fillRect(0, 0, canvasElement.width, canvasElement.height);

        //convert to png image as dataURL
        dataURL = canvasElement.toDataURL("image/png");

        //convert that as base64 encoding
        convertedDataURI = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");

        //call Apex method imperatively and use promise for handling sucess & failure
        saveSign({
                strSignElement: convertedDataURI,
                recId: this.applicationId
            })
            .then(() => {
                this.getAssignedSuperUserTableValues();
                this.openSignatureModel = false;

                this.dispatchEvent(utils.toastMessage("Signature has been saved successfully", "success"));
            })
            .catch(error => {
                this.dispatchEvent(utils.toastMessage("Error in creating signature. please check", "warning"));
            })
    }

    searchCoordinatesForEvent(requestedEvent, event) {
        if (requestedEvent === 'down') {
            this.setupCoordinate(event);
            isDownFlag = true;
            isDotFlag = true;
            if (isDotFlag) {
                this.drawDot();
                isDotFlag = false;
            }
        }

        if (requestedEvent === 'up' || requestedEvent === "out") {
            isDownFlag = false;
        }

        if (requestedEvent === 'move') {
            if (isDownFlag) {
                this.setupCoordinate(event);
                this.redraw();
            }
        }
    }

    //This method is primary called from mouse down & move to setup cordinates.
    setupCoordinate(eventParam) {
        //get size of an element and its position relative to the viewport 
        //using getBoundingClientRect which returns left, top, right, bottom, x, y, width, height.
        const clientRect = canvasElement.getBoundingClientRect();
        prevX = currX;
        prevY = currY;
        currX = eventParam.clientX - clientRect.left;
        currY = eventParam.clientY - clientRect.top;
    }

    //For every mouse move based on the coordinates line to redrawn
    redraw() {
        ctx.beginPath();
        ctx.moveTo(prevX, prevY);
        ctx.lineTo(currX, currY);
        ctx.strokeStyle = x; //sets the color, gradient and pattern of stroke
        ctx.lineWidth = y;
        ctx.closePath(); //create a path from current point to starting point
        ctx.stroke(); //draws the path
    }

    //this draws the dot
    drawDot() {
        ctx.beginPath();
        ctx.fillStyle = x; //blue color
        ctx.fillRect(currX, currY, y, y); //fill rectrangle with coordinates
        ctx.closePath();
    }

    //retrieve canvase and context
    renderedCallback() {
        /*Promise.all([
            loadStyle(this, myResource + '/styleSheet.css'),
        ])*/

        canvasElement = this.template.querySelector('canvas');
        ctx = canvasElement.getContext("2d");

        //This method used to get the Uploaded Image and convert into Canvas Image Base64 Format
        /*getBase64Image({
                appID: this.applicationId
            })
            .then(result => {
                this.imageBase64Format = "data:image/png;base64," + result;
                //this.drawCanvasImage();
            })*/
    }

    //This method used to set Image format as Canvas Image format.
    /*drawCanvasImage() {
        let signatureImage = new Image();
        signatureImage.onload = function () {
            ctx.drawImage(this, 0, 0);
        }
        signatureImage.setAttribute("src", this.imageBase64Format);
    }*/

    //Close Signature Model
    signatureCloseModel() {
        this.openSignatureModel = false;
    }
    /* Signature Model End */

    // Draft Method On Click
    handleDraftMethod() {
        if (!this.decisionDetails.CommonDecision__c)
            return this.dispatchEvent(utils.toastMessage("Please Select Status", "warning"));

        this.isDraftBtnDisable = true;

        let currentDate = new Date().getDate() < 10 ? '0' + new Date().getDate() : new Date().getDate();
        let currentMonth = ((new Date().getMonth()) + 1) < 10 ? '0' + ((new Date().getMonth()) + 1) : ((new Date().getMonth()) + 1);

        const fields = {};

        fields[ID_FIELD.fieldApiName] = this.applicationId;
        fields[COMMONDECISION_FIELD.fieldApiName] = this.decisionDetails.CommonDecision__c;
        fields[APPROVALCOMMENTS_FIELD.fieldApiName] = this.decisionDetails.ApprovalComments__c;
        fields[APPLICATIONSTATUS_FIELD.fieldApiName] = this.applicationStatus;

        /*if (this.SignatureValidation)
            fields[SIGNATURE_FIELD.fieldApiName] = true;*/

        if (this.isSuperUserSelected === true)
            fields[SUBMITTEDDATE_FIELD.fieldApiName] = new Date().getFullYear() + '-' + currentMonth + '-' + currentDate;

        const recordInput = {
            fields
        };

        updateRecord(recordInput)
        .then(() => {
            this.isDraftBtnDisable = false;
            this.dispatchEvent(utils.toastMessage("Application decision has been saved successfully", "success"));
            window.location.reload();
        })
        .catch(error => {
            this.isDraftBtnDisable = false;
            this.dispatchEvent(utils.toastMessage("Error in saving decision details. please check", "warning"));
        })
    }

    /*isEmptyCanvas() {
        var emptyCanvas = document.createElement("canvas");
        return (emptyCanvas.toDataURL() === canvasElement.toDataURL());
    }*/

    //Submit Method
    handleSubmitMethod() {
        if (!this.decisionDetails.CommonDecision__c)
            return this.dispatchEvent(utils.toastMessage("Please Select Status", "warning"));

        if (this.decisionDetails.CommonDecision__c == 'Submit for Training' && this.prideSessionNotFound === false && this._serviceTrainingNotFound === false)
            return this.dispatchEvent(utils.toastMessage("Service Training is mandatory", "warning"));

        if (this.decisionDetails.CommonDecision__c == 'Submitted') {
            if (!this.SignatureValidation)
                return this.dispatchEvent(utils.toastMessage("Signature is mandatory", "warning"));

            if (this.assignedSuperUserTableData.length == 0)
                return this.dispatchEvent(utils.toastMessage("Tier of Approval is mandatory", "warning"));

            if (this.sessionHoursFlag === false && this._27HrsNotCompleted === false)
                return this.dispatchEvent(utils.toastMessage("Pre-Service Training 27hrs is not completed", "warning"));

            if (this.homeStudyCompletedFlag === false && this._homeStudyCompletedFlag === false)
                return this.dispatchEvent(utils.toastMessage("Home Study Details are not completed", "warning"));

            if (this.referenceCheckFlag === false && (this._comarReferenceCheckCompleted === false || this._threeRef === false || this._twoRefF2F === false || this._twoRefRelation === false))
                return this.dispatchEvent(utils.toastMessage("Comar Reference Check Details are not completed", "warning"));
        }

        if (this.decisionDetails.CommonDecision__c == 'Approved' && !this.SignatureValidation)
            return this.dispatchEvent(utils.toastMessage("Signature is mandatory", "warning"));

        /*if (canvasElement.toDataURL() === 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAACWCAYAAABkW7XSAAAEaElEQVR4Xu3UgQkAMAwCwXb/oS10i4fLBHIG77YdR4AAgYDANViBlkQkQOALGCyPQIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiDwAIGUVl1ZhH69AAAAAElFTkSuQmCC') {
            return this.dispatchEvent(utils.toastMessage("Signature Is Mandatory", "warning"));
        }*/

        this.isSubmitBtnDisable = true;

        let currentDate = new Date().getDate() < 10 ? '0' + new Date().getDate() : new Date().getDate();
        let currentMonth = ((new Date().getMonth()) + 1) < 10 ? '0' + ((new Date().getMonth()) + 1) : ((new Date().getMonth()) + 1);

        const fields = {};

        fields[ID_FIELD.fieldApiName] = this.applicationId;
        fields[COMMONDECISION_FIELD.fieldApiName] = this.decisionDetails.CommonDecision__c;

        if (this.isSuperUserFlow === true)
            fields[SUPERVISORCOMMENTS_FIELD.fieldApiName] = this.decisionDetails.SupervisorComments__c;
        else
            fields[APPROVALCOMMENTS_FIELD.fieldApiName] = this.decisionDetails.ApprovalComments__c;

        if (this.decisionDetails.CommonDecision__c == 'Submit for Training') {
            fields[APPLICATIONSTATUS_FIELD.fieldApiName] = 'Assigned for Training';
        } else if (this.decisionDetails.CommonDecision__c == 'Submitted') {
            fields[APPLICATIONSTATUS_FIELD.fieldApiName] = 'Caseworker Submitted';
            fields[SUBMITTEDDATE_FIELD.fieldApiName] = new Date().getFullYear() + '-' + currentMonth + '-' + currentDate;
        } else if (this.decisionDetails.CommonDecision__c == 'Rejected' && this.isSuperUserFlow === false) {
            fields[APPLICATIONSTATUS_FIELD.fieldApiName] = 'Rejected';
            fields[SUPERVISOR_FIELD.fieldApiName] = null;
            fields[CASEWORKER_FIELD.fieldApiName] = null;
            fields[SUBMITTEDDATE_FIELD.fieldApiName] = null;
        } else if (this.decisionDetails.CommonDecision__c == 'Approved') {
            fields[APPLICATIONSTATUS_FIELD.fieldApiName] = 'Approved';
        } else if (this.decisionDetails.CommonDecision__c == 'Returned') {
            fields[APPLICATIONSTATUS_FIELD.fieldApiName] = 'Returned';
        } else if (this.decisionDetails.CommonDecision__c == 'Rejected' && this.isSuperUserFlow === true) {
            fields[APPLICATIONSTATUS_FIELD.fieldApiName] = 'Supervisor Rejected';
        } else {
            fields[APPLICATIONSTATUS_FIELD.fieldApiName] = this.applicationStatus;
        }

        let selectedRows = this.selectedRows[0] == undefined ? '' : this.selectedRows[0];
        let workItemId = this.workItemId === '' ? '' : this.workItemId;

        const recordInput = {
            fields
        };

        updateRecord(recordInput)
        .then((result) => {
            this.isSubmitBtnDisable = false;
            if (this.decisionDetails.CommonDecision__c == 'Submit for Training') {
                this.dispatchEvent(utils.toastMessage("Application has been submitted successfully", "success"));
                window.location.reload();
            } else if ((['Submitted', 'Approved', 'Returned'].includes(this.decisionDetails.CommonDecision__c)) || (this.isSuperUserFlow === true && this.decisionDetails.CommonDecision__c == 'Rejected')) {
                let resultObj = {
                    Id: result.id,
                    ApprovalComments__c: result.fields.ApprovalComments__c.value,
                    SupervisorComments__c : result.fields.SupervisorComments__c.value,
                    CommonDecision__c: result.fields.CommonDecision__c.value,
                    Caseworker__c: result.fields.Caseworker__c.value
                };

                let finalStatus = this.decisionDetails.CommonDecision__c == 'Returned' ? 'Reassigned' : ((this.decisionDetails.CommonDecision__c == 'Rejected' && this.isSuperUserFlow === true) ? 'Rejected by Supervisor' : this.decisionDetails.CommonDecision__c);

                updateapplicationdetails({
                        resultObj: resultObj,
                        supervisorId: selectedRows,
                        workItemId: workItemId
                    })
                    .then(() => {
                        this.dispatchEvent(utils.toastMessage(`Application has been ${finalStatus} successfully`, "success"));
                        window.location.reload();
                    })
                    .catch(error => {
                        
                        this.dispatchEvent(utils.toastMessage("Error in submitting application details. please check", "warning"));
                    })
            } else {
                this.dispatchEvent(utils.toastMessage("Application has been rejected successfully", "success"));
                window.location.reload();
            }
        })
        .catch(error => {
            this.isSubmitBtnDisable = false;
            
            this.dispatchEvent(utils.toastMessage("Error in submitting application details. please check", "warning"));
        })
        
    }
}