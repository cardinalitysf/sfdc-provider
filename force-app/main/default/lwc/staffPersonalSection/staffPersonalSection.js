/**
 * @Author        : Janaswini S
 * @CreatedOn     : March 20, 2020
 * @Purpose       : This component contains Personal  Section of staffs
 **/

import { LightningElement, track, api, wire } from "lwc";
import updateOrInsertSOQLReturnId from "@salesforce/apex/StaffPersonalSelection.updateOrInsertSOQLReturnId";
import getPickListValues from "@salesforce/apex/StaffPersonalSelection.getPickListValues";
import fetchDataForAddOrEdit from "@salesforce/apex/StaffPersonalSelection.fetchDataForAddOrEdit";
import * as sharedData from "c/sharedData";
import { utils } from "c/utils";
import { CJAMS_CONSTANTS } from "c/constants";


export default class StaffPersonalSection extends LightningElement {
  @track FirstName = "";
  @track value = "";
  @track LastName = "";
  @track AffiliationType = "";
  @track JobTitle = "";
  @track EmployeeType = "";
  @track StartDate = "";
  @track EndDate = "";
  @track Phone = "";
  @track Email = "";
  @track ReasonforLeaving = "";
  @track Comments = "";
  @track ProgramType;
  @track ReasonforLeavingOptions;
  @track selectedVals = [];
  @track ObjFetchData;
  @track EmployeeTypeOptions;
  @track AffiliationTypeOptions;
  @track mindate = "1970-01-01";
  @track maxdate = "1970-01-02";
  @track isDisableReasonForLeaving = true;

  //Sundar adding this for date
  @track isDisableEndDate = true;
  selectedStartDate;

  @api
  get providerid() {
    return sharedData.getProviderId();
  }

  get staffid() {
    return sharedData.getStaffId();
  }

  @wire(getPickListValues, {
    objInfo: {
      sobjectType: "Contact"
    },
    picklistFieldApi: "ReasonforLeaving__c"
  })
  wireCheckPicklistReasonforLeaving({ error, data }) {
    if (data) {
      this.ReasonforLeavingOptions = data;
    } else if (error) {
    }
  }

  @wire(getPickListValues, {
    objInfo: {
      sobjectType: "Contact"
    },
    picklistFieldApi: "AffiliationType__c"
  })
  wireCheckPicklistAffiliationType({ error, data }) {
    if (data) {
      this.AffiliationTypeOptions = data;
    } else if (error) {
    }
  }

  @wire(getPickListValues, {
    objInfo: {
      sobjectType: "Contact"
    },
    picklistFieldApi: "EmployeeType__c"
  })
  wireCheckPicklistEmployeeType({ error, data }) {
    if (data) {
      this.EmployeeTypeOptions = data;
    } else if (error) {
    }
  }

  connectedCallback() {
    this.handleFetch();
  }

  handleFetch() {
    if (
      this.staffid != undefined ||
      this.staffid != "" ||
      this.staffid != null
    ) {
      fetchDataForAddOrEdit({
        fetchdataname: this.staffid
      })
        .then((result) => {
          try {
            this.ObjFetchData = result[0].Id;
            this.FirstName = result[0].FirstName__c;
            this.LastName = result[0].LastName__c;
            this.AffiliationType = result[0].AffiliationType__c;
            this.JobTitle = result[0].JobTitle__c;
            this.EmployeeType = result[0].EmployeeType__c;
            this.StartDate = result[0].StartDate__c;
            this.EndDate = result[0].EndDate__c;
            this.Phone = result[0].Phone;
            this.Email = result[0].Email;
            this.ReasonforLeaving = result[0].ReasonforLeaving__c;
            this.Comments = result[0].Comments__c;
            this.Name = result[0].Name;
            this.ProgramType = result[0].ProgramType__c;
            this.mindate = result[0].StartDate__c;
            if (result[0].StartDate__c) {
              this.selectedStartDate = result[0].StartDate__c;
              this.maxdate = new Date(
                new Date(this.selectedStartDate).setDate(
                  new Date(this.selectedStartDate).getDate() + 30
                )
              );
              this.maxdate = utils.formatDateYYYYMMDD(this.maxdate);
            }
            this.isDisableEndDate = false;
          } catch (e) { }
        })
        .catch((errors) => {
          if (errors) {
            let error = JSON.parse(errors.body.message);
            const { title, message, errorType } = error;
            this.dispatchEvent(
              utils.toastMessageWithTitle(title, message, errorType)
            );
          } else {
            this.dispatchEvent(
              utils.toastMessage(
                CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
                CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
              )
            );
          }
        });
    }
  }

  @api
  reset() {
    this.FirstName = "";
    this.value = "";
    this.LastName = "";
    this.AffiliationType = "";
    this.JobTitle = "";
    this.EmployeeType = "";
    this.StartDate = "";
    this.EndDate = "";
    this.Phone = "";
    this.Email = "";
    this.ReasonforLeaving = "";
    this.Comments = "";
  }

  handleStartDate(e) {
    if (e.target.value) {
      this.StartDate = e.target.value;
      this.mindate = e.target.value;
      this.selectedStartDate = e.target.value;
      this.maxdate = new Date(
        new Date(this.selectedStartDate).setDate(
          new Date(this.selectedStartDate).getDate() + 30
        )
      );
      this.maxdate = utils.formatDateYYYYMMDD(this.maxdate);
      this.isDisableEndDate = false;
    }
  }

  handleEndDate(e) {
    if (!e.target.value) {
      this.isDisableReasonForLeaving = true;
      this.ReasonforLeaving = " ";
    } else {
      this.EndDate = e.target.value;
      this.isDisableReasonForLeaving = false;
    }
  }

  handleMobileChange(evt) {
    evt.target.value = evt.target.value.replace(/(\D+)/g, "");
    this.Phone = utils.formattedPhoneNumber(evt.target.value);
  }

  handleChange(event) {
    this.selectedVals.push(event.detail.value);
  }

  @api
  handleSubmit(event) {
    event.preventDefault();

    var inp = [
      ...this.template.querySelectorAll("lightning-input"),
      ...this.template.querySelectorAll("lightning-combobox")
    ];
    const allValid = [
      ...this.template.querySelectorAll("lightning-input"),
      ...this.template.querySelectorAll("lightning-combobox")
    ].reduce((validSoFar, inputFields) => {
      inputFields.reportValidity();
      return validSoFar && inputFields.checkValidity();
    }, true);

    if (allValid) {
      let objStaff = {
        sobjectType: "Contact"
      };
      objStaff.Id = this.ObjFetchData;
      objStaff.AccountId = sharedData.getProviderId();
      objStaff.Name = "testing";

      inp.forEach((element) => {
        if (element.name == "FirstName") {
          objStaff.FirstName__c = element.value;
        } else if (element.name == "LastName") {
          objStaff.LastName__c = element.value;
          objStaff.LastName =
            objStaff.FirstName__c + " " + objStaff.LastName__c;
        } else if (element.name == "AffiliationType") {
          objStaff.AffiliationType__c = element.value;
        } else if (element.name == "JobTitle") {
          objStaff.JobTitle__c = element.value;
        } else if (element.name == "EmployeeType") {
          objStaff.EmployeeType__c = element.value;
        } else if (element.name == "StartDate") {
          objStaff.StartDate__c = element.value;
        } else if (element.name == "EndDate") {
          objStaff.EndDate__c = element.value;
        } else if (element.name == "Phone") {
          objStaff.Phone = element.value;
        } else if (element.name == "Email") {
          objStaff.Email = element.value;
        } else if (element.name == "ReasonforLeaving") {
          objStaff.ReasonforLeaving__c = element.value;
        } else if (element.name == "Comments") {
          objStaff.Comments__c = element.value;
        }
      });

      updateOrInsertSOQLReturnId({
        objSobjecttoUpdateOrInsert: objStaff
      })
        .then((result) => {
          sharedData.setStaffId(result);
          this.dispatchEvent(
            utils.toastMessage("Staff Member Saved Successfully", "success")
          );
          this.dispatchEvent(
            new CustomEvent("intimateparentfornext", {
              detail: {
                shownext: true
              }
            })
          );
        })
        .catch((errors) => {
          if (errors) {
            let error = JSON.parse(errors.body.message);
            const { title, message, errorType } = error;
            this.dispatchEvent(
              utils.toastMessageWithTitle(title, message, errorType)
            );
          } else {
            this.dispatchEvent(
              utils.toastMessage(
                CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
                CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
              )
            );
          }
        });
    }
  }
}
