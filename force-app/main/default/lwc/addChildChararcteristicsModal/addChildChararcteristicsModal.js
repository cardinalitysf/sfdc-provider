import { LightningElement, api, wire, track } from "lwc";

export default class AddChildChararcteristicsModal extends LightningElement {
  @api _childCharacteristics;
  @track suggestionData = [];
  @track selectedData = [];
  @api _listSelected;

  @api get childCharacteristics() {
    return this._childCharacteristics;
  }

  set childCharacteristics(value) {
    this.setAttribute("childCharacteristics", value);
    this._childCharacteristics = value;
    this.suggestionData = JSON.parse(JSON.stringify(value));
  }

  @api get listSelected() {
    return this._listSelected;
  }

  set listSelected(value) {
    this.setAttribute("listSelected", value);
    this._listSelected = value;
  }

  connectedCallback() {
    let suggestionData = JSON.parse(JSON.stringify(this.suggestionData));
    this._listSelected.map((data, i) => {
      suggestionData.filter((item, index) => {
        if (data.ChildCharacteristics__c === item.Activity__c) {
          this.selectedData.push({
            Activity__c: data.ChildCharacteristics__c
          });
          suggestionData.splice(index, 1);
        }
      });
    });
    this.suggestionData = suggestionData;

  }

  //    handle suggestion data functionslity start
  handleSuggestionToSelectedData = (event) => {
    let targetValue = event.target.dataset.value;
    this.suggestionData.map((data, index) => {
      if (data.Activity__c === targetValue) {
        this.selectedData.push({
          Activity__c: data.Activity__c
        });
        this.suggestionData.splice(index, 1);
      }
    });
  };

  // handle suggestion data functionality end


  // handle Add all child characteristics functionality start
  addAllChildCharacteristics() {
    this.selectedData = this.selectedData.concat(this.suggestionData);
    this.suggestionData = [];
  }
  // handle Add all child characteristics functionality end


  // removeAllChildCharactertics functionality start
  removeAllChildCharacteristics = () => {
    this.suggestionData = this.suggestionData.concat(this.selectedData);
    this.selectedData = [];
  }
  // removeAllChildCharactertics functionality end

  // handle selected data functionality start
  handleSelectedToSuggestionData = (event) => {
    let targetValue = event.target.dataset.value;

    this.selectedData.map((item, index) => {
      if (item.Activity__c === targetValue) {
        this.suggestionData.push({
          Activity__c: item.Activity__c
        });
        this.selectedData.splice(index, 1);
      }
    });
  };

  // handle selected data functionality end

  //   childCharacteristicsModal functionality start
  childCharacteristicsModal = () => {
    const modalClose = new CustomEvent("openaddchildcharacteristicsmodal", {
      detail: { openAddChildCharacteristicsModal: false }
    });
    this.dispatchEvent(modalClose);
  };
  //   childCharacteristicsModal functionality end

  // save selected data functionality start
  saveSelectedData = () => {
    const selectedData = new CustomEvent("selecteddatachildcharacteristics", {
      detail: {
        selectedData: this.selectedData,
        suggestionData: this.suggestionData
      }
    });
    this.dispatchEvent(selectedData);
  };

  // save selected data functionality end
}