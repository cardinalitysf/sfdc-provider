import {
    LightningElement,
    track

} from 'lwc';
import * as referralsharedDatas from "c/sharedData";
import {
    CJAMS_CONSTANTS
} from 'c/constants';


export default class PublicProvidersReconsiderationDocument extends LightningElement {
    get reconsiderationID() {
        return referralsharedDatas.getReconsiderationId();
      // return 'a0V9D000000ax1DUAQ';
    }
    get sobjectNameToFetch() {
        return CJAMS_CONSTANTS.RECONSIDERATION_OBJECT_NAME;
    }

}