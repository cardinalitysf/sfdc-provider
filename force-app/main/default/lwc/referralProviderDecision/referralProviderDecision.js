/**
 * @Author        : BalaMurugan
 * @CreatedOn     : Apr 12,2020
 * @Purpose       : Referral Based Decision Status 
 **/


import {
    LightningElement,
    track,
    wire,
    api
} from "lwc";
import getUserDetails from "@salesforce/apex/referralDecision.getUserDetails";
import fatchPickListValue from "@salesforce/apex/referralDecision.getPickListValues";
import getDecisionDetails from "@salesforce/apex/referralDecision.getDecisionDetails";
import insertDecisionReport from "@salesforce/apex/referralDecision.updateOrInsertSOQL";
import getProviderDetails from "@salesforce/apex/referralDecision.getProviderDetails";
import getProvidercommunity from "@salesforce/apex/referralDecision.getProvidercommunity";
import * as sharedData from "c/sharedData";
import {
    utils
} from "c/utils";

import {
    CJAMS_CONSTANTS
} from 'c/constants';

export default class ReferralProviderDecision extends LightningElement {
    @track stageNameValues;
    @track statusnew;
    @track comments;
    @track providercomm;
    @track usercreation;
    @track decisionStatus = {};
    @track isDisable = false;
    @track isSaveAsDraftDisable = false;
    @track isBtnDisable = false;
    @track isStatusDisable = false;
    @track Spinner = true;

    //Fetch Provider id
    get getProviderId() {
        return sharedData.getProviderId();
    }

    //Fetch Case id
    get caseid() {
        return sharedData.getCaseId();
    }

    get coummincationValue() {
        return sharedData.getCommunicationValue();
    }

    get getProviderTabValues() {
        return sharedData.getButtonDisableforDecisionTab();
    }


    connectedCallback() {
        //Fetching values on both ProviderId and CaseId is Avaiable
        getProviderDetails({
                newprovider: this.getProviderId
            })
            .then(result => {
                this.usercreation = result;
            })
            .catch(errors => {
                let config = {
                    friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
                    errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
                }
                return this.dispatchEvent(utils.handleError(errors, config));
            });

        getDecisionDetails({
                Ereferral: this.caseid
            })
            .then(result => {
                if (result.length > 0) {
                    this.decisionStatus = result[0];
                    this.bool = true;
                    this.statusnew = result[0].DecisionStatus__c;
                    this.comments = result[0].Comments__c;

                    if (result[0].Status == "Approved" || result[0].Status == "Rejected") {
                        this.isBtnDisable = true;
                        this.isStatusDisable = true;
                        this.isDisable = true;
                        this.isSaveAsDraftDisable = true;
                        this.commentsDisabled = true;
                    } else {
                        this.isBtnDisable = false;
                        this.isDisable = false;
                        this.isSaveAsDraftDisable = false;
                        this.commentsDisabled = false;
                        this.isStatusDisable = false;
                    }
                }
                this.Spinner = false;
            })
            .catch(errors => {
                this.Spinner = false;
                let config = {
                    friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
                    errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
                }
                return this.dispatchEvent(utils.handleError(errors, config));
            });

        getProvidercommunity()
            .then(result => {
                this.providercomm = result;
            })
            .catch(errors => {
                let config = {
                    friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
                    errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
                }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
    }

    //PickList for DecisionStatus
    @wire(fatchPickListValue, {
        objInfo: {
            sobjectType: "Case"
        },
        picklistFieldApi: "DecisionStatus__c"
    }) wireduserDetails2({
        error,
        data
    }) {
        if (data) {
            this.stageNameValues = data.filter(row => {
                return row.label !== CJAMS_CONSTANTS.REVIEW_STATUS_APPROVED && row.label !== CJAMS_CONSTANTS.REVIEW_STATUS_REJECT && row.label !== CJAMS_CONSTANTS.REVIEW_STATUS_FOR_TRAINING
            });
        } else if (error) {

        }
    }

    changeHandler(event) {
        this.statusnew = event.detail.value;
        this.isSaveAsDraftDisable = false;

        if (this.getProviderId != undefined && this.caseid != undefined && this.coummincationValue != undefined && this.statusnew != "None") {
            this.isBtnDisable = false;
        } else {
            this.isBtnDisable = true;
        }
    }

    commentsHandler(event) {
        this.comments = event.detail.value;
    }

    //Save Case object values while Save Draft
    DraftMethod() {
        if (!this.statusnew)
            this.dispatchEvent(utils.toastMessage("Please Select Status", "Warning"));

        let rdmNumber = Math.round(new Date().getTime() / 1000);
        let myObjCase = {
            sobjectType: "Case"
        };
        myObjCase.Status = "Draft";
        myObjCase.DecisionStatus__c = this.statusnew;
        myObjCase.Comments__c = this.comments;

        if (this.caseid)
            myObjCase.Id = this.caseid;

        insertDecisionReport({
                objSobjecttoUpdateOrInsert: myObjCase,
                rdmNumber: rdmNumber
            })
            .then(result => {
                this.dispatchEvent(utils.toastMessage("Referral saved as draft successfully", "Success"));
                window.location.reload();
            })
            .catch(errors => {
                let config = {
                    friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE,
                    errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
                }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
    }

    //Save Case object values when user inserting based on provider community
    saveMethod() {

        if (!this.statusnew)
            return this.dispatchEvent(utils.toastMessage("Please Select Status", "Warning"));

        let rdmNumber = Math.round(new Date().getTime() / 1000);
        let myobjcase = {
            sobjectType: "Case"
        };
        myobjcase.Status = this.statusnew;
        myobjcase.DecisionStatus__c = this.statusnew;
        myobjcase.Comments__c = this.comments;

        if (this.caseid)
            myobjcase.Id = this.caseid;
        insertDecisionReport({
                objSobjecttoUpdateOrInsert: myobjcase,
                newprovider: this.getProviderId,
                rdmNumber: rdmNumber
            })
            .then(result => {

                this.dispatchEvent(utils.toastMessage("Referral Submitted Successfully", "Success"));
                setTimeout(() => {
                    window.location.reload();
                }, 1000);
            })
            .catch(errors => {
                let config = {
                    friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE,
                    errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
                }
                // return this.dispatchEvent(utils.handleError(errors, config));
            });
    }

    //Filter Particular User
    @wire(getUserDetails)
    wireduserDetails({
        error,
        data
    }) {
        if (data) {
            if (data.length > 0)
                this.userDetails = data.filter(
                    item =>
                    item.Name !== "Automated Process" &&
                    item.Name !== "Platform Integration User"
                );
        } else if (error) {

        }
    }
}