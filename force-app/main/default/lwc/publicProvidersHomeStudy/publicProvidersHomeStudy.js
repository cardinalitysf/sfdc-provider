/**
 * @Author        : Sindhu Venkateswarlu
 * @CreatedOn     : June 19, 2020
 * @Purpose       : This component contains Home Study Visit Details For Public Provider.
 **/
import {
    LightningElement,
    track,
    wire,
    api
} from 'lwc';

import getHomeStudyDetails from "@salesforce/apex/PublicProvidersHomeStudy.getHomeStudyDetails";
import editHomeStudyDetails from "@salesforce/apex/PublicProvidersHomeStudy.editHomeStudyDetails";
import InsertUpdateHomeStudyInfo from "@salesforce/apex/PublicProvidersHomeStudy.InsertUpdateHomeStudyInfo";
import getPersons from "@salesforce/apex/PublicProvidersHomeStudy.getPersons";

import {
    utils
} from 'c/utils';
import * as sharedData from "c/sharedData";
import images from '@salesforce/resourceUrl/images';


import {
    getPicklistValuesByRecordType,
    getObjectInfo
} from 'lightning/uiObjectInfoApi';
import {
    updateRecord,
    deleteRecord
} from "lightning/uiRecordApi";
import {
    refreshApex
} from "@salesforce/apex";
import { getRecord } from "lightning/uiRecordApi";


import RECONSIDERATIONSTATUS_FIELD from "@salesforce/schema/Reconsideration__c.Status__c";
import ACTORDETAILS_OBJECT from '@salesforce/schema/ActorDetails__c';
import ID_FIELD from '@salesforce/schema/ActorDetails__c.Id';
import COMMENTS_FIELD from '@salesforce/schema/ActorDetails__c.Comments__c';
import DATE_FIELD from '@salesforce/schema/ActorDetails__c.Date__c';
import DURATION_FIELD from '@salesforce/schema/ActorDetails__c.Duration__c';
import ENDTIME_FIELD from '@salesforce/schema/ActorDetails__c.EndTime__c';
import HOMEADDESS_FIELD from '@salesforce/schema/ActorDetails__c.HomeAddess__c';
import INTERVIEWLOCATION_FIELD from '@salesforce/schema/ActorDetails__c.InterviewLocation__c';
import STARTTIME_FIELD from '@salesforce/schema/ActorDetails__c.StartTime__c';


const actions = [{
    label: 'Edit',
    name: 'Edit',
    iconName: 'utility:edit',
    target: '_self'
},
{
    label: 'View',
    name: 'View',
    iconName: 'utility:preview',
    target: '_self'
},
{
    label: 'Delete',
    name: 'Delete',
    iconName: 'utility:delete',
    target: '_self'
}
];
const columns = [{
    label: 'Person Name',
    fieldName: 'Actor__c',
    type: 'text',
},
{
    label: 'Interview Date',
    fieldName: 'Date__c',
    type: 'text',

},
{
    label: 'Start Time',
    fieldName: 'StartTime__c',
    type: 'date',
    editable: false,
    typeAttributes: { hour: "2-digit", minute: "2-digit" }
},
{
    label: 'End Time',
    fieldName: 'EndTime__c',
    type: 'date',
    editable: false,
    typeAttributes: { hour: "2-digit", minute: "2-digit" }
},
{
    label: 'Duration',
    fieldName: 'Duration__c',
    type: 'text'
},
{
    label: 'Location',
    fieldName: 'InterviewLocation__c',
    type: 'text'
},
{
    label: 'Comments',
    fieldName: 'Comments__c',
    type: 'button',
    typeAttributes: {
        label: {
            fieldName: 'Comments__c'
        },
        name: 'Comments',
        title: {
            fieldName: 'Comments__c'
        }
    },
    cellAttributes: {
        iconName: 'utility:description',
        iconAlternativeText: 'Description',
        class: {
            fieldName: 'descriptionClass'
        }
    }
},

];
export default class PublicProvidersHomeStudy extends LightningElement {
    attachmentIcon = images + '/homestudyIcon.svg';
    @track columns = columns;
    @track selectPersonOptions;
    @track noRecordsFound = false;
    @track showInterviewLocation = false;
    @track commentsModel = false;
    @track topicCommentsPopup;
    descriptionIcon = images + '/description-icon.svg';
    @track mintime;

    @track studyDetails = {};
    @track currentPageStudyData;
    @track activeForPType = false;
    @track pillValueForPtype = [];
    @track pillSingleValueForPtype;
    @track pillSingleValueForPtypeCount;
    @track pillSingleValueCondition = true;

    //wiredHomestudyVisit;
    HomestudyVisitId;
    @track reconsiderationforApiCall = this.ReconsiderationId;
    @track diabledView = false;
    @track deleteModel = false;
    @track addDisabled = false;
    @track homeAddress;
    @track HomeAddess__c;
    @track btnLabel;
    @track AssignbtnDisable = false;
    @track ReconsiderationStatus;

    //Pagination
    @track totalHomeStudyVisitCount = 0;
    @track totalHomeStudy = [];
    @track page = 1;
    @track currentPageHomeStudyData;
    setPagination = 5;
    perpage = 10;

    //Sort Function
    @track defaultSortDirection = 'asc';
    @track sortDirection = 'asc';
    @track sortedBy = "Date__c";
    _newStudyVisitModal;

    @track mintime;
    deleteId;
    @track ProviderId;

    get ReconsiderationId() {
        return sharedData.getReconsiderationId();
    }
    //       get providerId() {
    //            return sharedData.getProviderId();
    //   }

    get getUserProfileName() {
        return sharedData.getUserProfileName();
    }

    //openPetModal from Header Button Child comp Through AllTabs Comp
    @api get openStudyModel() {
        return this._newStudyVisitModal;

    }
    set openStudyModel(value) {
        this.btnLabel = 'ADD';
        this._newStudyVisitModal = value;
        this.commentsModel = false;
        this.diabledView = false;
        this.addDisabled = false;
        this.pillValueForPtype = [];


        this.title = "ADD HOME VISIT DETAILS";
        let currentMonth = ((new Date().getMonth()) + 1) < 10 ? '0' + ((new Date().getMonth()) + 1) : ((new Date().getMonth()) + 1);
        let currentDate = new Date().getDate() < 10 ? '0' + new Date().getDate() : new Date().getDate();
        let currentMinute = new Date().getMinutes() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes();
        let currentHour = new Date().getHours() < 10 ? '0' + new Date().getHours() : new Date().getHours();
        this.studyDetails = {
            Date__c: new Date().getFullYear() + '-' + currentMonth + '-' + currentDate,
            StartTime__c: currentHour + ':' + currentMinute + ':00.000',
            Duration__c: '00 : 00'
        };
        this.mintime = this.studyDetails.StartTime__c;
        this.showInterviewLocation = true;
    }
    //closemdal dispatch function

    closeStudyModel() {

        this._newStudyVisitModal = false;
        this.dispatchCustomEventToHandleHomeVisit();
        this.commentsModel = false;


    }
    dispatchCustomEventToHandleHomeVisit() {
        const tooglecmps = new CustomEvent("togglecomponents", {
            detail: {
                Component: "CloseStudyInfo"
            }
        });
        this.dispatchEvent(tooglecmps);
        this.Actor__c = '';
        this.studyDetails = {};
        this.homeVisitMembers = [];
        this.pillValueForPtype = [];



    }

    connectedCallback() {
        //this.ReconsiderationId;
        this.ProviderId = sharedData.getProviderId();

        this.persons();

        this.StudyInfo();
    }
    persons() {
        
        getPersons({
            providerid: this.ProviderId
        }).then((data) => {
            if (data.length > 0) {
                var selectHouseholdOptions = [];
                data.forEach((row) => {
                    selectHouseholdOptions.push({
                        Id: row.Id,
                        label: row.Contact__r.FirstName__c + ' ' + row.Contact__r.LastName__c,
                        value: row.Contact__r.FirstName__c + ' ' + row.Contact__r.LastName__c + ' - ' + row.Role__c,
                        role: row.Role__c,
                    });
                });
                this.selectPersonOptions = selectHouseholdOptions;
            }
        });
    }
    StudyInfo() {
        getHomeStudyDetails({
            fetchdataname: this.ReconsiderationId
        })
            .then((result) => {
                //this.wiredHomestudyVisit = result;
                if (result) {
                    this.totalHomeStudyVisitCount = result.length;
                    if (this.totalHomeStudyVisitCount == 0)
                        this.noRecordsFound = true;
                    else
                        this.noRecordsFound = false;

                    this.totalHomeStudy = result.map(row => {
                        return {
                            Id: row.Id,
                            Actor__c: row.Actor__r != undefined ? row.Actor__r.Contact__r.Name : undefined,
                            Date__c: utils.formatDate(row.Date__c),
                            StartTime__c: row.StartTime__c,
                            EndTime__c: row.EndTime__c,
                            Duration__c: row.Duration__c,
                            HomeAddess__c: row.HomeAddess__c,
                            InterviewLocation__c: row.InterviewLocation__c,
                            Comments__c: (row.Comments__c) ? (row.Comments__c.length > 13) ? row.Comments__c.substring(0, 9) + '...' : row.Comments__c : '',
                            descriptionClass: (row.Comments__c) ? 'btnbdrIpad blue' : 'svgRemoveicon',
                            popupdescription: (row.Comments__c) ? row.Comments__c : '',
                            buttonDisabled: (this.AssignbtnDisable) ? true : false
                        }
                    });
                    //result = this.totalHomeStudy;
                    if (this.totalHomeStudy.length > 0) {
                        const selectedEvent = new CustomEvent("homestudycompletedflag", {
                            detail: {
                                homestudycompletedflag: true,
                            }
                        });
                        this.dispatchEvent(selectedEvent);
                    }

                }
                this.pageData();

            })
    }

    @wire(getRecord, {
        recordId: "$ReconsiderationId",
        fields: [RECONSIDERATIONSTATUS_FIELD]
    })
    wireuser({ error, data }) {
        if (error) {
            this.error = error;
        } else if (data) {
            this.ReconsiderationStatus = data.fields.Status__c.value;

            if (data) {
                if (this.getUserProfileName != "Supervisor") {
                    if (
                        [
                            "Submitted",
                            "Approved",
                            "Rejected"

                        ].includes(this.ReconsiderationStatus)
                    ) {
                        this.AssignbtnDisable = true;
                    } else {
                        this.AssignbtnDisable = false;
                    }
                }
                if (this.getUserProfileName == "Supervisor") {
                    if (["Approved", "Supervisor Rejected"].includes(this.ReconsiderationStatus)) {
                        this.AssignbtnDisable = true;
                    }
                }
            }
        }
        this.StudyInfo();
    }


    //Pagination
    pageData() {
        let page = this.page;
        let perpage = this.perpage;
        let startIndex = (page * perpage) - perpage;
        let endIndex = (page * perpage);
        this.totalHomeStudyVisitCount = this.tototalHomeStudytalHomeStudy ? this.tototalHomeStudytalHomeStudy.slice(startIndex, endIndex) : 0;
        if (this.currentPageStudyData) {
            if (this.currentPageStudyData.length == 0) {
                if (this.page != 1) {
                    this.page = this.page - 1;
                    this.pageData();
                }
            }
        }
    }

    //Page Change Action in Pagination Bar
    hanldeProgressValueChange(event) {
        this.page = event.detail;
        this.pageData();
    }

    // Sorting the Data Table Column Function End
    sortBy(field, reverse, primer) {
        const key = primer ?
            function (x) {
                return primer(x[field]);
            } :
            function (x) {
                return x[field];
            };

        return function (a, b) {
            a = key(a);
            b = key(b);
            if (a === undefined) a = '';
            if (b === undefined) b = '';
            a = typeof (a) === 'number' ? a : a.toLowerCase();
            b = typeof (b) === 'number' ? b : b.toLowerCase();

            return reverse * ((a > b) - (b > a));
        };
    }

    //Data Table Sorting Function
    onHandleSort(event) {
        const {
            fieldName: sortedBy,
            sortDirection: sortDirection
        } = event.detail;
        const cloneData = [...this.currentPageHomeStudyData];

        cloneData.sort(this.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1));

        this.currentPageHomeStudyData = cloneData;
        this.sortDirection = sortDirection;
        this.sortedBy = sortedBy;
    }

    //Code start multi select in Persons
    get pillvalueForPType() {
        if (this.pillValueForPtype.length > 1) {
            this.pillSingleValueForPtype = this.pillValueForPtype[0].value;
            this.pillSingleValueCondition = false;
            this.pillSingleValueForPtypeCount = this.pillValueForPtype.length - 1;
            return this.pillSingleValueForPtype;
        } else {
            this.pillSingleValueCondition = true;
            return this.pillValueForPtype;
        }

    }

    handleMouseOutButton(evr) {
        this.activeForPType = false;
    }
    get toggledivForPtype() {
        return this.pillValueForPtype.length == 0 ? false : true;
    }
    handlePType(evt) {
        //if (!this.isDisableProgramType)
        this.programTypeCheckBoxSelectUnselectDuringPageLoad();
        this.activeForPType = this.activeForPType ? false : true;
    }

    handleRemove(evt) {
        this.pillValueForPtype = this.remove(
            this.pillValueForPtype,
            "value",
            evt.target.label
        );
        let i;
        let checkboxes = this.template.querySelectorAll("[data-id=checkbox]");
        checkboxes.forEach(element => {
            if (element.value == evt.target.label) {
                if (element.checked) {
                    element.checked = false;
                }
            }
        });
        this.activeForPType = true;
    }
    get tabClassForPType() {
        return this.activeForPType ?
            "slds-popover slds-popover_full-width slds-popover_show" :
            "slds-popover slds-popover_full-width slds-popover_hide";
    }
    handleclickofPTypeCheckBox(evt) {
        if (evt.target.checked) {

            let id = evt.target.id.split('-')[0];
            this.pillValueForPtype.push({
                value: evt.target.value,
                Id: id
            });
        } else {
            this.pillValueForPtype = this.remove(
                this.pillValueForPtype,
                "value",
                evt.target.value
            );
        }
        this.homeVisitMembers = this.pillValueForPtype;
        evt.target.checked != evt.target.checked;
    }
    remove(array, key, value) {
        const index = array.findIndex(obj => obj[key] === value);
        return index >= 0 ? [...array.slice(0, index), ...array.slice(index + 1)] :
            array;
    }

    getStateEmployeeValues() {
        var sArray = [];
        this.pillValueForPtype.forEach(function (item) {
            sArray.push(item.value);
        });
        return sArray.join(";");
    }

    programTypeCheckBoxSelectUnselectDuringPageLoad() {
        let localObject = [];
        if (this.studyDetails.Actor__c) {
            this.studyDetails.Actor__c.split(/\s*;\s*/).forEach(function (myString) {
                localObject.push({
                    value: myString
                });
            });

            var inp = [...this.template.querySelectorAll("[data-id=checkbox]")];
            inp.forEach(element => {
                localObject.forEach(function (item) {
                    if (item.value == element.value) element.checked = true;
                });
            });
            this.pillValueForPtype = localObject;
        }
    }
    closeCommentsModal() {
        this.commentsModel = false
    }

    //Code end multi select in Persons
    //Edit/Delete Model Action

    handleRowAction(event) {
        this.commentsModel = false;
        if (event.detail.action.name == "Comments") {
            this.commentsModel = true;
            this.topicCommentsPopup = event.detail.row.Comments__c;
        }
        let selectedLicenseId = event.detail.row.Id;
        if (event.detail.action.name == 'Edit') {

            //this.handleclickofPTypeCheckBox();
            this.title = event.detail.action.name == 'Edit' ? "EDIT Home Study Visit Details" : this.title;
            this.btnLabel = event.detail.action.name == 'Edit' ? 'UPDATE' : this.btnLabel;
            //this.isDisableEndDate = false;
            this.addDisabled = false;
            this.diabledView = false;
            this.deleteModel = false;
            this.commentsModel = false;

            editHomeStudyDetails({
                selectedHomeVisitId: selectedLicenseId
            })
                .then(result => {
                    if (result.length > 0) {
                        this.studyDetails = {
                            Actor__c: result[0].Actor__r != undefined ? result[0].Actor__r.Contact__r.Name : undefined,
                            Date__c: result[0].Date__c,
                            StartTime__c: utils.formatTime24Hr(result[0].StartTime__c),
                            EndTime__c: utils.formatTime24Hr(result[0].EndTime__c),
                            Duration__c: result[0].Duration__c,
                            HomeAddess__c: result[0].HomeAddess__c,
                            Comments__c: result[0].Comments__c,
                            InterviewLocation__c: result[0].InterviewLocation__c,
                        };
                        //  this.pillValueForPtype=this.studyDetails.Actor__c;
                        this.programTypeCheckBoxSelectUnselectDuringPageLoad();
                        this.HomestudyVisitId = result[0].Id;
                        this._newStudyVisitModal = true;




                    }
                    else {
                        this.dispatchEvent(utils.toastMessage("Error in getting Homes Study Visit details", "warning"));
                    }
                })




        } else if (event.detail.action.name == 'Delete') {
            this.closeStudyModel();
            this.deleteModel = true;
            this.deleteId = event.detail.row.Id;

        } else if (event.detail.action.name == 'View') {
            this.title = event.detail.action.name == 'View' ? " Home Study Visit Details" : this.title;
            this.addDisabled = true;
            this.diabledView = true;
            this.deleteModel = false;
            this.commentsModel = false;


            editHomeStudyDetails({
                selectedHomeVisitId: selectedLicenseId
            })
                .then(result => {
                    if (result.length > 0) {
                        this.studyDetails = {

                            Id: result[0].Id,
                            Actor__c: result[0].Actor__r != undefined ? result[0].Actor__r.Contact__r.Name : undefined,
                            Date__c: result[0].Date__c,
                            StartTime__c: utils.formatTime24Hr(result[0].StartTime__c),
                            EndTime__c: utils.formatTime24Hr(result[0].EndTime__c),
                            Duration__c: result[0].Duration__c,
                            HomeAddess__c: result[0].HomeAddess__c,
                            Comments__c: result[0].Comments__c,
                            InterviewLocation__c: result[0].InterviewLocation__c,
                        };
                        this.programTypeCheckBoxSelectUnselectDuringPageLoad();
                        this.HomestudyVisitId = result[0].Id;
                        this._newStudyVisitModal = true;


                    } else {
                        this.dispatchEvent(utils.toastMessage("Error in getting Homes Study Visit details", "warning"));
                    }
                })

        }
    }



    handleDelete() {
        deleteRecord(this.deleteId)
            .then(() => {
                this.deleteModel = false;
                this.closeStudyModel();
                this.showInterviewLocation = true;
                this.StudyInfo();
                this.deleteId = null;
                this.dispatchEvent(utils.toastMessage('Home Study Visit Details deleted successfully', "success"));


            })
            .catch(error => {
                this.deleteModel = false;
                this.dispatchEvent(utils.toastMessage("Error in delete Home Study Visit details", "warning"));
                this.StudyInfo();
            });
    }

    closeDeleteModal() {
        this.deleteModel = false;
    }

    //Fields On Change Event Handler
    homeStudyOnChange(event) {

        if (event.target.name == 'Date') {
            this.studyDetails.Date__c = event.target.value;
        } else if (event.target.name == 'StartTime') {
            this.studyDetails.StartTime__c = event.target.value;
            this.mintime = event.target.value;
            this.studyDetails.EndTime__c = null;
            this.studyDetails.Duration__c = null;
        } else if (event.target.name == 'EndTime') {
            this.studyDetails.EndTime__c = event.target.value;
            if (this.studyDetails.StartTime__c != null)
                this.studyDetails.Duration__c = utils.calTimeDifference(this.studyDetails.StartTime__c, this.studyDetails.EndTime__c)
        } else if (event.target.name == 'HomeAddess__c') {
            this.studyDetails.HomeAddess__c = event.detail.value;
            if (this.studyDetails.HomeAddess__c == 'No') {
                this.showInterviewLocation = false;
            } else {
                this.showInterviewLocation = true;
            }
        } else if (event.target.name == 'Comments') {
            this.studyDetails.Comments__c = event.target.value;
        } else if (event.target.name == 'InterviewLocation') {
            this.studyDetails.InterviewLocation__c = event.target.value;
        }
    }


    addHomeDetails() {
        const allValid = [
            ...this.template.querySelectorAll('lightning-input'),
            ...this.template.querySelectorAll('lightning-combobox')
        ]
            .reduce((validSoFar, inputFields) => {
                inputFields.reportValidity();
                return validSoFar && inputFields.checkValidity();
            }, true);

        if (allValid) {
            if (this.studyDetails.StartTime__c == this.studyDetails.EndTime__c)
                return this.dispatchEvent(utils.toastMessage("End Time should be greater than Start Time", "warning"));

            if (!this.studyDetails.Duration__c)
                return this.dispatchEvent(utils.toastMessage("Duration is mandatory", "warning"));

            if (this.pillValueForPtype.length == 0)
                return this.dispatchEvent(utils.toastMessage("Please choose Select Persons", "warning"));

            if (this.showInterviewLocation === false && !this.studyDetails.InterviewLocation__c)
                return this.dispatchEvent(utils.toastMessage("Interview Location is mandatory", "warning"));

            this.addDisabled = true;
            if (this.HomestudyVisitId == undefined) {

                let objToInsert = [];
                this.homeVisitMembers.forEach(homeStudyElem => {
                    let homeStudyFields = {

                        Date__c: '',
                        StartTime__c: '',
                        EndTime__c: '',
                        Duration__c: '',
                        HomeAddess__c: '',
                        Comments__c: '',
                        InterviewLocation__c: ''
                    };
                    homeStudyFields.Actor__c = homeStudyElem.Id;
                    homeStudyFields.Reconsideration__c = this.reconsiderationforApiCall;
                    homeStudyFields.Date__c = utils.formatDateYYYYMMDD(this.studyDetails.Date__c);
                    homeStudyFields.HomeAddess__c = this.studyDetails.HomeAddess__c;
                    homeStudyFields.InterviewLocation__c = this.studyDetails.InterviewLocation__c;
                    homeStudyFields.Comments__c = this.studyDetails.Comments__c;
                    homeStudyFields.StartTime__c = this.studyDetails.StartTime__c;
                    homeStudyFields.EndTime__c = this.studyDetails.EndTime__c;
                    homeStudyFields.Duration__c = this.studyDetails.Duration__c;

                    objToInsert.push(homeStudyFields);

                })
                InsertUpdateHomeStudyInfo({
                    strHomeStudyDetails: JSON.stringify(objToInsert)
                }).then(result => {
                    
                    this.closeStudyModel();
                    this.dispatchEvent(utils.toastMessage("Home Study Details  are Saved Successfully", "Success"));
                    this.addDisabled = false;

                    const selectedEvent = new CustomEvent("homestudycompletedflag", {
                        detail: {
                            homestudycompletedflag: true,
                        }
                    });
                    this.dispatchEvent(selectedEvent);

                    this.StudyInfo();
                }).catch(error => {
                    this.dispatchEvent(utils.toastMessage("Home Study Details  are not Saved", "Warning"));
                });
            } else {
                const fields = {};

                fields[ID_FIELD.fieldApiName] = this.HomestudyVisitId;
                fields[DATE_FIELD.fieldApiName] = this.studyDetails.Date__c;
                fields[STARTTIME_FIELD.fieldApiName] = this.studyDetails.StartTime__c;
                fields[ENDTIME_FIELD.fieldApiName] = this.studyDetails.EndTime__c;
                fields[DURATION_FIELD.fieldApiName] = this.studyDetails.Duration__c;
                fields[HOMEADDESS_FIELD.fieldApiName] = this.studyDetails.HomeAddess__c;
                fields[COMMENTS_FIELD.fieldApiName] = this.studyDetails.Comments__c;
                fields[INTERVIEWLOCATION_FIELD.fieldApiName] = this.studyDetails.InterviewLocation__c;

                const recordInput = {
                    fields
                };

                updateRecord(recordInput)
                    .then(() => {
                        this.studyDetails = {};
                        this.HomestudyVisitId = null;
                        this.addDisabled = false;
                        this.closeStudyModel();

                        this.dispatchEvent(utils.toastMessage("Home Visit Details has been updated successfully", "success"));
                        this.StudyInfo();
                    })
                    .catch(error => {
                        this.dispatchEvent(utils.toastMessage("Error in saving Home Visit Details. Please check.", "warning"));
                    })

            }
        } else {
            this.dispatchEvent(utils.toastMessage("Error in adding Home Visit Details. Please check.", "warning"));
        }



    }

    //Declare ActorDetails object into one variable
    @wire(getObjectInfo, {
        objectApiName: ACTORDETAILS_OBJECT
    })
    objectInfo;

    //Function used to get all picklist values from ActorDetails object
    @wire(getPicklistValuesByRecordType, {
        objectApiName: ACTORDETAILS_OBJECT,
        recordTypeId: '$objectInfo.data.defaultRecordTypeId'
    })
    getAllPicklistValues({
        error,
        data
    }) {
        if (data) {
            this.error = null;

            // Category Type Field Picklist values
            let homeAddressOptions = [];
            data.picklistFieldValues.HomeAddess__c.values.forEach(key => {
                homeAddressOptions.push({
                    label: key.label,
                    value: key.value
                })
            });
            this.homeAddress = homeAddressOptions;
        } else if (error) {
            this.error = JSON.stringify(error);
        }
    }

    //if decision status = approved show only view otherwise show edit and delete begins */
    constructor() {
        super();
        let ViewIcon = true;
        for (let i = 0; i < this.columns.length; i++) {
            if (this.columns[i].type == 'action') {
                ViewIcon = false;
            }
        }
        if (ViewIcon) {
            this.columns.push(
                {
                    type: 'action', typeAttributes: { rowActions: this.getRowActions }, cellAttributes: {
                        iconName: 'utility:threedots_vertical',
                        iconAlternativeText: 'Actions',
                        class: "tripledots"
                    }
                }
            )
        }
    }
    getRowActions(row, doneCallback) {
        const actions = [];
        if (row['buttonDisabled'] == true) {
            actions.push({
                'label': 'View',
                'name': 'View',
                'iconName': 'utility:preview',
                'target': '_self'
            });
        } else {
            actions.push(
                { label: 'Edit', name: 'Edit', iconName: 'utility:edit', target: '_self' },
                { label: 'View', name: 'View', iconName: 'utility:preview', target: '_self' },
                { label: 'Delete', name: 'Delete', iconName: 'utility:delete', target: '_self' }
            );

        }
        // simulate a trip to the server
        setTimeout(() => {
            doneCallback(actions);
        }, 200);
    }
}
