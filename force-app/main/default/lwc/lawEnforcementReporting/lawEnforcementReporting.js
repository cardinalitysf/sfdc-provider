/**
 * @Author        : Saranraj
 * @CreatedOn     : Aug 03 ,2020
 * @Purpose       : law enforcement
 * @updatedBy     :
 * @updatedOn     :
 **/

import { LightningElement, wire } from "lwc";
import CONTACT_OBJECT from "@salesforce/schema/Contact";
import getRecordTypeValue from "@salesforce/apex/IncidentFosterParentLawEnforcement.getRecordTypeValue";
import CONTACT_ID from "@salesforce/schema/Contact.Id";
import CONTACT_LAST_NAME from "@salesforce/schema/Contact.LastName__c";
import CONTACT_L_Name from "@salesforce/schema/Contact.LastName";
import CONTACT_FIRST_NAME from "@salesforce/schema/Contact.FirstName__c";
import CONTACT_PHONE from "@salesforce/schema/Contact.Phone";
import CONTACT_DATE from "@salesforce/schema/Contact.LawEnforcementDate__c";
import CONTACT_TIME from "@salesforce/schema/Contact.LawEnforcementTime__c";
import CONTACT_REPORT_NUMBER from "@salesforce/schema/Contact.ReportNumber__c";
import CONTACT_RECORD_TYPE_Id from "@salesforce/schema/Contact.RecordTypeId";
import ACTOR_OBJECT from "@salesforce/schema/Actor__c";
import CONTACT_FIELD_FROM_ACTOR from "@salesforce/schema/Actor__c.Contact__c";
import REFERRAL_FIELD_FROM_ACTOR from "@salesforce/schema/Actor__c.Referral__c";
import ROLE_FIELD_FROM_ACTOR from "@salesforce/schema/Actor__c.Role__c";
import getLawEnforcementDataTableData from "@salesforce/apex/IncidentFosterParentLawEnforcement.getLawEnforcementDataTableData";

import * as sharedData from "c/sharedData";
import { utils } from "c/utils";
import { refreshApex } from "@salesforce/apex";
import images from '@salesforce/resourceUrl/images';

import USER_ID from '@salesforce/user/Id';
import PROFILE_FIELD from '@salesforce/schema/User.Profile.Name';


// for Api
import {
  getRecord,
  createRecord,
  updateRecord,
  deleteRecord
} from "lightning/uiRecordApi";

export default class LawEnforcementReporting extends LightningElement {
  recordTypeId;
  recordTypeName;
  lawEnforcementFirstName;
  lawEnforcementLastName;
  lawEnforcementDate;
  lawEnforcementTime;
  lawEnforcementPhone;
  lawEnforcementReportNumber;
  incidentRecordTypeId;
  incidentRecordTypeName;
  lawEnforcementModal = false;
  lawEnformentDataTableData;
  showAddLawEnforcementBtn = false;
  showUpdateBtn = false;
  lawEnforcementId;
  userProfileName;
  showLawEnforcementPreviewModal =false;
  showDoneBtn = false;
  showAddBtn =true;
  disableBtnForCaseworker = false

  // lawEnforcement state value end

  referralId = "";
  cpahomesId = ""

  attachmentIcon = images + '/law-enforcementIcon.svg';

  get lawEnforcementColumn() {
    if(this.userProfileName !== 'Caseworker'){
      return [
        {
          label: "First Name",
          fieldName: "firstName",
          cellAttributes: {
            class: "test"
          }
        },
        {
          label: "Last Name",
          fieldName: "lastName",
          cellAttributes: {
            class: "test"
          }
        },
        {
          label: "Phone",
          fieldName: "phone",
          cellAttributes: {
            class: "test"
          }
        },
        {
          label: "Date",
          fieldName: "date",
          cellAttributes: {
            class: "test"
          },
          type: 'date',
          typeAttributes: {
            month: '2-digit',
            day: '2-digit',
            year: 'numeric'
        }
        },
        {
          label: "Time",
          fieldName: "time",
          cellAttributes: {
            class: "test"
          }
        },
        {
          label: "Report Number",
          fieldName: "reportNumber",
          cellAttributes: {
            class: "test"
          }
        },
        {
          label: "Action",
          fieldName: "id",
          type: "button",
          initialWidth: 90,
          typeAttributes: {
            iconName: "utility:edit",
            name: "Edit",
            title: "Edit",
            initialWidth: 20,
            disabled: false,
            iconPosition: "left",
            target: "_self"
          }
        },
        {
          type: "button",
          fieldName: "id",
          typeAttributes: {
            iconName: "utility:delete",
            name: "Delete",
            title: "Delete",
            initialWidth: 20,
            class: "del-red del-position-law",
            disabled: false,
            iconPosition: "left",
            target: "_self"
          }
        }
      ];
    }else{
      return [
        {
          label: "First Name",
          fieldName: "firstName",
          cellAttributes: {
            class: "test"
          }
        },
        {
          label: "Last Name",
          fieldName: "lastName",
          cellAttributes: {
            class: "test"
          }
        },
        {
          label: "Phone",
          fieldName: "phone",
          cellAttributes: {
            class: "test"
          }
        },
        {
          label: "Date",
          fieldName: "date",
          cellAttributes: {
            class: "test"
          },
          type: 'date',
          typeAttributes: {
            month: '2-digit',
            day: '2-digit',
            year: 'numeric'
        }
        },
        {
          label: "Time",
          fieldName: "time",
          cellAttributes: {
            class: "test"
          }
        },
        {
          label: "Report Number",
          fieldName: "reportNumber",
          cellAttributes: {
            class: "test"
          }
        },
        {
          label: "Action",
          fieldName: "id",
          type: "button",
          initialWidth: 90,
          typeAttributes: {
            iconName: "utility:preview",
            name: "Preview",
            title: "Preview",
            initialWidth: 20,
            disabled: false,
            iconPosition: "left",
            target: "_self"
          }
        }
       
      ];
    }
   
  }

  connectedCallback() {
    this.getCaseId()
    this.getLawEnforcementDataTableData();
  }

  @wire(getRecord, {
    recordId: USER_ID,
    fields: [PROFILE_FIELD]
}) wireuser({
    error,
    data
}) {
    if (error) {
        this.error = error;
    } else if (data) {
        this.userProfileName = data.fields.Profile.displayValue;
        this.disableBtnForCaseworker = data.fields.Profile.displayValue === "Caseworker"
    }
}

  getCaseId() {
    this.referralId = sharedData.getCaseId()
    return sharedData.getCaseId()
  }

  get getCPAHomeId() {
    this.cpahomesId = sharedData.getCPAHomeId()
    return sharedData.getCPAHomeId()
  }

  getLawEnforcementDataTableData = async () => {
    let temp = [];
    try {
      let response = await getLawEnforcementDataTableData({
        referralId: this.referralId
      });

      response &&
        response.map((item) => {

          temp = [
            ...temp,
            {
              Id: item.Id,
              contactId: item.Contact__r && item.Contact__r.Id,
              firstName: item.Contact__r && item.Contact__r.FirstName__c,
              lastName: item.Contact__r && item.Contact__r.LastName__c,
              date: item.Contact__r && item.Contact__r.LawEnforcementDate__c,
              time: item.Contact__r && utils.formatTime12Hr(item.Contact__r.LawEnforcementTime__c),
              reportNumber: item.Contact__r && item.Contact__r.ReportNumber__c,
              phone: item.Contact__r && item.Contact__r.Phone
            }
          ];
        
        });
      this.lawEnformentDataTableData = temp;
      this.showAddLawEnforcementBtn = this.lawEnformentDataTableData.length > 0;
    } catch (err) {
      this.dispatchEvent(utils.handleError(err))
    }
  };

  handleOpenModal = () => {
    this.lawEnforcementModal = true;
    this.showUpdateBtn = false
    this.lawEnforcementId = ""
    this.lawEnforcementFirstName = ""
    this.lawEnforcementLastName = ""
    this.lawEnforcementPhone = ""
    this.lawEnforcementReportNumber = ""
    this.lawEnforcementDate = ""
    this.lawEnforcementTime = ""

  };

  handleCloseModal = () => {
    this.lawEnforcementModal = false;
    this.getLawEnforcementDataTableData()
  }

  @wire(getRecordTypeValue, { recordType: "Law Enforcement" })
  getRecordTypeValueFromApex(result) {
    try {
      if (result.data) {
        this.recordTypeId = result && result.data
        // this.recordTypeName = result && result.data[0].Name;
      }
    } catch (err) {
      this.dispatchEvent(utils.handleError(err));
    }
  }

  handleOnchangeLawEnforcement = (event) => {
    let targetName = event.target.name;
    let targetValue = event.target.value;

    if (targetName === "First Name") {
      this.lawEnforcementFirstName = targetValue;
    } else if (targetName === "Last Name") {
      this.lawEnforcementLastName = targetValue;
    } else if (targetName === "Phone") {
      targetValue = targetValue.replace(/(\D+)/g, '');
      this.lawEnforcementPhone = utils.formattedPhoneNumber(targetValue);
    } else if (targetName === "Date") {
      this.lawEnforcementDate = targetValue;
    } else if (targetName === "Time") {
      this.lawEnforcementTime = targetValue;
    } else if (targetName === "Report Number") {
      this.lawEnforcementReportNumber = targetValue;
    } else {
      return "";
    }
  };

  handleSaveLawEnforcement = (event) => {
    let btnType = event.target.title;
    const allValid = [
      ...this.template.querySelectorAll('lightning-input')
  ]
      .reduce((validSoFar, inputFields) => {
          inputFields.reportValidity();
          return validSoFar && inputFields.checkValidity();
      }, true);
  if (allValid) {
     
    let fields = {};

    // for contact object
    btnType === "Update"
      ? (fields[CONTACT_ID.fieldApiName] = this.lawEnforcementId)
      : null;
    fields[CONTACT_RECORD_TYPE_Id.fieldApiName] = this.recordTypeId;
    fields[CONTACT_FIRST_NAME.fieldApiName] = this.lawEnforcementFirstName;
    fields[CONTACT_LAST_NAME.fieldApiName] = this.lawEnforcementLastName;
    fields[CONTACT_DATE.fieldApiName] = this.lawEnforcementDate;
    fields[CONTACT_TIME.fieldApiName] = this.lawEnforcementTime;
    fields[CONTACT_PHONE.fieldApiName] = this.lawEnforcementPhone;
    fields[
      CONTACT_L_Name.fieldApiName
    ] = `${this.lawEnforcementLastName} - dummy last name`;
    fields[
      CONTACT_REPORT_NUMBER.fieldApiName
    ] = this.lawEnforcementReportNumber;

    let recordData = {
      apiName: CONTACT_OBJECT.objectApiName,
      fields
    };

    let updateRecordData = {
      fields
    };

    if (btnType === "Update") {
      updateRecord(updateRecordData)
        .then((response) => {
            this.lawEnforcementModal = false
            this.getLawEnforcementDataTableData()
            this.dispatchEvent(utils.toastMessage("updated successfully", "success"))
        })
        .catch((err) => {
           this.dispatchEvent(utils.handleError(err))
        });
    } else {
      createRecord(recordData)
        .then((response) => {
          this.createDataInActor(response.id);
        })
        .catch((err) => {
          this.dispatchEvent(
            utils.toastMessage("Error in saving record", "Error"));
        });
    }

  }else{
    return  this.dispatchEvent(
      utils.toastMessage("Please Fill All Mandatory Fields", "warning"));
  }   
  };

  createDataInActor = (id) => {
    let fields = {};

    fields[CONTACT_FIELD_FROM_ACTOR.fieldApiName] = id;
    fields[REFERRAL_FIELD_FROM_ACTOR.fieldApiName] = this.referralId;
    fields[ROLE_FIELD_FROM_ACTOR.fieldApiName] = "Law Enforcement";

    let recordData = {
      apiName: ACTOR_OBJECT.objectApiName,
      fields
    };


    createRecord(recordData)
      .then((actorResponse) => {
        this.lawEnforcementModal = false
        this.getLawEnforcementDataTableData()
        this.dispatchEvent(utils.toastMessage("created successfully", "success"))
      })
      .catch((err) => {
        this.dispatchEvent(utils.handleError(err))
      });
  };

  handleLawEnforcementRowAction = (event) => {
    const actionName = event.detail.action.name;
    const row = event.detail.row;
    switch (actionName) {
      case "Delete":
        this.showLawEnforcementDeleteModal(row);
        break;
      case "Edit":
        this.showLawEnforcementEditModal(row);
        break;
        case "Preview":
          this.showLawEnforcementPreviewModal = true;
          this.showLawEnforcementEditModal(row,'Preview');
          break;
      default:
    }
  };


  showLawEnforcementDeleteModal = (row) => {
    deleteRecord(row.Id)
      .then((response) => {
        this.getLawEnforcementDataTableData()
        this.dispatchEvent(
          utils.toastMessage("Deleted Successfully", "success")
        );
      })
      .catch((err) => {
        this.dispatchEvent(
          utils.toastMessage("Something went wrong", "warning")
        );
      });
  };

  showLawEnforcementEditModal = (row,Preview) => {
    this.showDoneBtn = Preview?true:false;
    this.showUpdateBtn = Preview?false:true;
    this.showAddBtn = false;
    this.lawEnforcementModal = true;
    this.lawEnforcementId = row.contactId;
    this.lawEnforcementFirstName = row.firstName;
    this.lawEnforcementLastName = row.lastName;
    this.lawEnforcementPhone = row.phone;
    this.lawEnforcementReportNumber = row.reportNumber;
    this.lawEnforcementDate = row.date;
    this.lawEnforcementTime = row.time;
  };
}
