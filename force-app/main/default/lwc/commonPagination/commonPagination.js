/**
 * @Author        : Naveen .S
 * @CreatedOn     : Mar 13 ,2020
 * @Purpose       : Common Pagination based on Total Count Records.
 **/

import { LightningElement, track, api } from 'lwc';


export default class CommonPagination extends LightningElement {

    @api getCount;
    @api page;
    @api setsize;
    @api perpage;
    @track pages = [];
    totalpage = true;
    @track pagePerTime;
    @track pageInTotal;

    renderedCallback() {
        this.renderButtons();
    }

    renderButtons = () => {
        this.template.querySelectorAll('button').forEach((but) => {
            but.className = this.page === parseInt(but.dataset.id, 10) ? 'slds-button slds-button_neutral active' : 'slds-button slds-button_neutral';
        });
    }

    get pagesList() {
        let i = 1;
        let numberOfPages = Math.ceil(this.getCount / this.perpage);
        this.paginationRange = [];
        let pagesArr = [];
        while (
            pagesArr.push(i++) < numberOfPages
        ) { }
        this.pageInTotal = numberOfPages;
        let mid = Math.floor(this.setsize / 2) + 1;
        if (this.page > mid) {
            let pagecount = pagesArr.slice(this.page - mid, this.page + mid - 1);
            this.pagePerTime = pagecount[pagecount.length - 1];
            return this.pages = pagesArr.slice(this.page - mid, this.page + mid - 1);
        } else {
            let pagecount = pagesArr.slice(0, this.setsize);
            this.pagePerTime = pagecount[pagecount.length - 1];
            return this.pages = pagesArr.slice(0, this.setsize);
        }
    }

    pageData = () => {
        let page = this.page;
        let perpage = this.perpage;
        let startIndex = (page * perpage) - perpage;
        let endIndex = (page * perpage);
        return this.pages.slice(startIndex, endIndex);
    }

    get hasPrev() {
        return this.page > 1;
    }

    get hasNext() {
        return this.page < this.pageInTotal
    }

    onNext = () => {
        ++this.page;
        const selectedEvent = new CustomEvent("progressvaluechange", {
            detail: this.page
        });
        this.dispatchEvent(selectedEvent);
    }

    onPrev = () => {
        --this.page;
        const selectedEvent = new CustomEvent("progressvaluechange", {
            detail: this.page
        });
        this.dispatchEvent(selectedEvent);
    }

    onPageClick = (e) => {
        this.page = parseInt(e.target.dataset.id, 10);
        const selectedEvent = new CustomEvent("progressvaluechange", {
            detail: this.page
        });
        this.dispatchEvent(selectedEvent);
    }

    get currentPageData() {
        return this.pageData();
    }

    setPages = () => {
        let numberOfPages = Math.ceil(this.getCount / this.perpage);
        for (let index = 1; index <= numberOfPages; index++) {
            this.pages.push(index);
        }
    }

}