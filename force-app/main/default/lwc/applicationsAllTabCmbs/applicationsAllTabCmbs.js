/**
 * @Author        : Preethi Bojarajan
 * @CreatedOn     : Apr 7,2020
 * @Purpose       : Application full module control funcions 
 * @updatedBy     : Preethi Bojarajan
 * @updatedOn     : Apr 8 ,2020
 **/
import {
    LightningElement,
    track, api
} from 'lwc';

//Style Sheet Load
import {
    loadStyle
} from 'lightning/platformResourceLoader';
import myResource from '@salesforce/resourceUrl/styleSheet';

export default class ApplicationsAllTabCmbs extends LightningElement {
    @api showDashboard = false;
    @api showApplicationTabSet = false;
    @track showMoniteringTabSet = false;
    @track activeTab = 'general';


    handleredirectApplicationTabs(event) {
        this.showDashboard = !event.detail.first;
      
        if (this.showDashboard == true) {
            this.showApplicationTabSet = true;
        } else {
            this.showApplicationTabSet = false;
        }
    }

    handleredirectMoniteringTabs(event) {
        this.showApplicationTabSet = event.detail.first;
        if (this.showApplicationTabSet == true) {
            this.activeTab = 'monitoring';
            //Added for screen control by priti 
            this.showMoniteringTabSet = false;
        } else this.showMoniteringTabSet = true;
    }

    /* Style Sheet Loading */
    renderedCallback() {
        Promise.all([
            loadStyle(this, myResource + '/styleSheet.css')
        ])
    }
}