import { LightningElement, track, wire } from 'lwc';
import * as sharedData from "c/sharedData";
import getContractNo from "@salesforce/apex/ProvidersContractDashboard.getContractId";

export default class ContractTabs extends LightningElement {

    //Sundar added this line
    @track contractNo;
    contractforApiCall = this.contractId;

    //get Contract ID from shared Data
    get contractId () {
        return sharedData.getContractId();
    }

    @wire(getContractNo, {
        contractId: '$contractforApiCall'
    })
    contractDetails({ error, data }) {
        if (data) {
            this.contractNo = data[0].Name;
        } else if (error) {
            this.error = error;
        }
    }

    //This function used to redirect back to contract dashboard
    RedirecttoContractDashboard(){
        const onredirecttocontract = new CustomEvent('redirecttocontractdashboard', {
            detail: {
                first: true
            }
        });
        this.dispatchEvent(onredirecttocontract);
    }

    handleContractDashboard(event) {
        this.RedirecttoContractDashboard();
    }
}