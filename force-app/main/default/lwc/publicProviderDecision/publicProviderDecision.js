/**
 * @Author        : BalaMurugan
 * @CreatedOn     : May 28,2020
 * @Purpose       : Referral Based Decision Status 
 **/

import { LightningElement, track, wire, api } from 'lwc';
import images from '@salesforce/resourceUrl/images';
import fatchPickListValue from "@salesforce/apex/PublicProviderDecision.getPickListValues";
import getDecisionDetails from "@salesforce/apex/PublicProviderDecision.getDecisionDetails";
import getContactApplicantDetails from "@salesforce/apex/PublicProviderDecision.getContactApplicantDetails";
import updateStatus from "@salesforce/apex/PublicProviderDecision.updateStatus";
import getMeetingDetails from "@salesforce/apex/PublicProviderDecision.getMeetingDetails";
import * as sharedData from "c/sharedData";
import { utils } from "c/utils";
import { CJAMS_CONSTANTS } from 'c/constants';


export default class PublicProviderDecision extends LightningElement {
    decisionIcon = images + '/decision-icon.svg';
    @track stageNameValues;
    @track statusnew;
    @track comments;
    @track providercomm;
    @track decisionStatus;
    @track isDisable = false;
    @track isSaveAsDraftDisable = false;
    @track isBtnDisable = true;
    @track isStatusDisable = false;
    @track role = '';
    @track meetingdata;

    _caseStatus;
    caseforApiCall = this.caseid;


    //Fetch Case id
    get caseid() {

        return sharedData.getCaseId();
    }



    @api getMeetingInfoFlag;
    connectedCallback() {
        //Fetching values on CaseId is Available

        getDecisionDetails({
            Ereferral: this.caseid
        })
            .then(result => {
                if (result.length > 0) {
                    this.decisionStatus = result[0].DecisionStatus__c;
                    this.bool = true;
                    this.statusnew = result[0].DecisionStatus__c;
                    this.comments = result[0].Comments__c;

                    //Sundar Adding this
                    this._caseStatus = result[0].Status;

                    if (result[0].Status == CJAMS_CONSTANTS.APPLICATION_REVIEW_STATUS_APPROVED || result[0].Status == CJAMS_CONSTANTS.PROVIDER_APP_FINAL_STATUS_REJECTED || result[0].Status == CJAMS_CONSTANTS.ASSIGNED_FOR_TRAINING) {
                        this.isBtnDisable = true;
                        this.isStatusDisable = true;
                        this.isDisable = true;
                        this.isSaveAsDraftDisable = true;
                        this.commentsDisabled = true;
                    } else if (result[0].Status == CJAMS_CONSTANTS.REVIEW_TRAINING_UNATTENDED || result[0].Status == CJAMS_CONSTANTS.REVIEW_TRAINING_ATTENDED) {
                        this.isBtnDisable = false;
                        this.isDisable = false;
                        this.isSaveAsDraftDisable = false;
                        this.commentsDisabled = false;
                        this.isStatusDisable = false;
                    }
                }
                this.Spinner = false;

                this.fetchPickListData();
            })
            .catch(errors => {
                this.Spinner = false;
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });

        this.getAllActorDetails();
        this.getAllMeetingDetails();
    }



    //fetching Picklist values
    fetchPickListData() {
        fatchPickListValue({
            objInfo: {
                sobjectType: "Case"
            },
            picklistFieldApi: "DecisionStatus__c"
        })
            .then(result => {
                if (['New', 'Pending', 'Draft'].includes(this._caseStatus)) {
                    this.stageNameValues = result.filter(row => {
                        return row.label !== CJAMS_CONSTANTS.APPLICATION_REVIEW_STATUS_APPROVED && row.label !== CJAMS_CONSTANTS.PROVIDER_APP_FINAL_STATUS_REJECTED && row.label !== CJAMS_CONSTANTS.REVIEW_STATUS_NONE && row.label !== CJAMS_CONSTANTS.REVIEW_STATUS_APPROVED
                    });
                } else if (this._caseStatus == CJAMS_CONSTANTS.ASSIGNED_FOR_TRAINING) {
                    this.stageNameValues = result.filter(row => {
                        return row.label == CJAMS_CONSTANTS.REVIEW_STATUS_FOR_TRAINING
                    });
                } else if (this._caseStatus == CJAMS_CONSTANTS.REVIEW_TRAINING_UNATTENDED) {
                    this.stageNameValues = result.filter(row => {
                        return row.label !== CJAMS_CONSTANTS.APPLICATION_REVIEW_STATUS_APPROVED && row.label !== CJAMS_CONSTANTS.PROVIDER_APP_FINAL_STATUS_REJECTED && row.label !== CJAMS_CONSTANTS.REVIEW_STATUS_APPROVED && row.label !== CJAMS_CONSTANTS.REVIEW_STATUS_NONE
                    });
                }
                else {
                    this.stageNameValues = result.filter(row => {
                        return row.label !== CJAMS_CONSTANTS.APPLICATION_REVIEW_STATUS_APPROVED && row.label !== CJAMS_CONSTANTS.PROVIDER_APP_FINAL_STATUS_REJECTED && row.label !== CJAMS_CONSTANTS.REVIEW_STATUS_NONE && row.label !== CJAMS_CONSTANTS.REVIEW_STATUS_FOR_TRAINING
                    });
                }
            })
            .catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
    }

    //fetch the applicant from contact using Actor object
    getAllActorDetails() {
        getContactApplicantDetails(
            {
                caseid: this.caseid
            }
        ).then((data) => {

            if (data) {

                let val = data.filter(item => {
                    return item.Role__c == CJAMS_CONSTANTS.REVIEW_STATUS_ROLE
                });
                this.role = val[0].Role__c;
            }
        }).catch(errors => {
            let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
            return this.dispatchEvent(utils.handleError(errors, config));
        });
    }

    //check data available in meeting
    getAllMeetingDetails() {
        getMeetingDetails(
            {
                caseid: this.caseid
            }
        ).then((data) => {

            if (data) {
                this.meetingdata = data;

            }
        }).catch(errors => {
            let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
            return this.dispatchEvent(utils.handleError(errors, config));
        });
    }

    changeHandler(event) {
        this.statusnew = event.detail.value;
        this.isSaveAsDraftDisable = false;
        this.isBtnDisable = false;

        // if (this.caseid != undefined && this.commincationValue != undefined) {
        //     this.isBtnDisable = false;
        // } else {
        //     this.isBtnDisable = true;
        // }
    }

    commentsHandler(event) {
        this.comments = event.detail.value;
    }

    //Save Case object values As Save Draft
    DraftMethod() {
        //     if (!this.statusnew)
        //    return  this.dispatchEvent(utils.toastMessage("Please Select Status", "Warning"));


        let myObjCase = {
            sobjectType: "Case"
        };
        myObjCase.Status = CJAMS_CONSTANTS.REVIEW_STATUS_DRAFT;
        myObjCase.DecisionStatus__c = this.statusnew;
        myObjCase.Comments__c = this.comments;


        if (this.caseid)
            myObjCase.Id = this.caseid;
        updateStatus({
            objSobjecttoUpdateOrInsert: myObjCase,

        })
            .then(result => {
                this.dispatchEvent(utils.toastMessage("Referral saved as draft successfully", "Success"));
                window.location.reload();
            })
            .catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });

    }

    //Save Case object values when user inserting based on provider community
    saveMethod() {

        if (!this.statusnew) {

            return this.dispatchEvent(utils.toastMessage("Please Select Status", "Warning"));
        }

        if (this.role != CJAMS_CONSTANTS.REVIEW_STATUS_ROLE) {
            return this.dispatchEvent(utils.toastMessage("No Applicant Available", "Warning"));

        }

        let myobjcase = {
            sobjectType: "Case"
        };


        myobjcase.DecisionStatus__c = this.statusnew;
        myobjcase.Comments__c = this.comments;
        myobjcase.Id = this.caseid;



        if (this.statusnew == CJAMS_CONSTANTS.REVIEW_STATUS_FOR_TRAINING) {
            myobjcase.Status = CJAMS_CONSTANTS.ASSIGNED_FOR_TRAINING
            const selectedEvent = new CustomEvent("dataintentstatustrue", {
                detail: {
                    intentFlag: true,
                }
            });
            this.dispatchEvent(selectedEvent);
            if (this.meetingdata.length == 0) {
                return this.dispatchEvent(utils.toastMessage("No Training Assigned", "Warning"));
            }


        }
        else if (this.statusnew == CJAMS_CONSTANTS.REVIEW_STATUS_APPROVED) {
            myobjcase.Status = CJAMS_CONSTANTS.APPLICATION_REVIEW_STATUS_APPROVED
        }
        else if (this.statusnew == CJAMS_CONSTANTS.REVIEW_STATUS_REJECT) {
            myobjcase.Status = CJAMS_CONSTANTS.APPLICATION_REVIEW_STATUS_REJECTED
        } else {

        }
        updateStatus({
            objSobjecttoUpdateOrInsert: myobjcase,

        })
            .then(result => {
                this.dispatchEvent(utils.toastMessage("Referral Submitted Successfully", "Success"));

                window.location.reload();
            })
            .catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
    }

}
