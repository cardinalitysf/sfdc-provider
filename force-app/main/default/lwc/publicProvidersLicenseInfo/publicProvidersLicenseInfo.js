/**
 * @Author        : Naveen S
 * @CreatedOn     : June 18 ,2020
 * @Purpose       : Public Provider Licenses Information 
 * @updatedBy     :
 * @updatedOn     :
 **/

import {
    LightningElement,
    track
} from "lwc";
import myResource from '@salesforce/resourceUrl/styleSheet';
import {
    loadStyle
} from 'lightning/platformResourceLoader';
import images from '@salesforce/resourceUrl/images';
import {
    utils
} from "c/utils";
import { CJAMS_CONSTANTS } from 'c/constants';
import getApplicationId from "@salesforce/apex/PublicProvidersLicenseInfo.getApplicationId";
import getLicenseInformation from "@salesforce/apex/PublicProvidersLicenseInfo.getLicenseInformation";
import getCoApplicantDetails from "@salesforce/apex/PublicProvidersLicenseInfo.getCoApplicantDetails";
import fetchDataForAddress from "@salesforce/apex/PublicProvidersLicenseInfo.fetchDataForAddress";
import fetchHomeInformation from '@salesforce/apex/PublicProvidersLicenseInfo.fetchHomeInformation';
import getHouseholdMembers from '@salesforce/apex/PublicProvidersLicenseInfo.getHouseholdMembers';
import * as sharedData from "c/sharedData";
import getContactImage from "@salesforce/apex/PublicProvidersLicenseInfo.getContactImage";


export default class PublicProvidersLicenseInfo extends LightningElement {

    @track Program;
    @track ProgramType;
    @track ApplicationId;
    @track childrenResiding;
    @track bedRooms;
    @track Gender;
    @track licensesIdList = [];
    @track effectiveDate;
    @track expirationDate;
    @track MinAge;
    @track MaxAge;
    @track Race;
    @track ApplicationNumber;
    @track OpenLicenseCard = true;
    @track NoLicenseCard = false;
    @track hasApplicantName;
    @track CoApplicantName;
    @track Spinner = true;
    @track getAddressHome = "Home Info";
    @track getAddressPayment = "payment";
    @track paymentAddressLine1 = null;
    @track paymentAddressLine2 = null;
    @track paymentMobile;
    @track paymentEmail;
    @track homeAddressLine1;
    @track homeAddressLine2;
    @track homeMobile;
    @track homeEmail;
    @track hasContactid;
    @track signUrl;
    @track noimages = images + "/user.svg";
    siteicon = images + '/ppslicenseIcon.svg';
    addressIcon = images + '/ppsaddressIcon.svg';
    email = images + '/ppsemail.svg';
    mobile = images + '/vibratemobile.svg';
    paymentaddress = images + '/payment-address.svg';
    providernameicon = images + '/provider-nameicon.svg';
    ppsbedIcon = images + '/ppsbedIcon.svg';
    ppskidsbaby = images + '/ppskidsbaby.svg';
    ppsgender = images + '/ppsgender.svg';
    raceIcon = images + '/raceIcon.svg';
    restroom = images + '/restroom.svg';
    fireIcon = images + '/fire.svg';
    homeIcon = images + '/ppshome.svg';

    get getProviderId() {
        return sharedData.getProviderId();
    }

    renderedCallback() {
        Promise.all([
            loadStyle(this, myResource + '/styleSheet.css')
        ])
    }

    connectedCallback() {
        this.getlicensesInformation();
    }

    getlicensesInformation() {
        getApplicationId({
            providerId: this.getProviderId
        })
            .then((data) => {
                if (data.length > 0) {
                    this.objapplicationid = data[0].Id;
                    this.getLicensesFromApplication();
                }
            })
            .catch(errors => {
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const { title, message, errorType } = error;
                    this.dispatchEvent(
                        utils.toastMessageWithTitle(title, message, errorType)
                    );
                } else {
                    this.dispatchEvent(
                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                    );
                }
            });
    }
    getLicensesFromApplication() {
        getHouseholdMembers({
            providerid: this.getProviderId
        })
            .then((data) => {
                if (data.length > 0) {
                    if (data[0].Contact__r.Title__c != undefined) {
                        this.hasApplicantName = data[0].Contact__r.Title__c + data[0].Contact__r.FirstName__c + ' ' + data[0].Contact__r.LastName__c;
                    } else {
                        this.hasApplicantName = data[0].Contact__r.FirstName__c + ' ' + data[0].Contact__r.LastName__c;
                    }
                    if (data[0].Contact__r.Id != undefined) {
                        this.hasContactid = data[0].Contact__r.Id;
                    }

                    getContactImage({
                        ContactID: this.hasContactid
                    }).then((result) => {
                        if (result.length > 0) {
                            this.signUrl =
                                "/sfc/servlet.shepherd/version/download/" +
                                result[0].ContentDocument.LatestPublishedVersionId;
                        }
                        else {
                            this.signUrl = this.noimages;
                        }
                    }
                    );
                    this.licensesIdList = data.map((row, index) => {
                        return {
                            Id: row.Id,
                            Name: row.Contact__r.FirstName__c + ' ' + row.Contact__r.LastName__c,
                            Title: row.Contact__r.Title__c != undefined ? row.Contact__r.Title__c : " ",
                            Role: row.Role__c,
                            dataIndex: index
                        }
                    });
                    if (this.licensesIdList.length > 0) {
                        this.licenseInfoVisible = true;
                        this.NoLicenseCard = false;
                        this.OpenLicenseCard = true;
                        this.getLicenseInformation(this.objapplicationid);
                        this.Spinner = false;
                    } else {
                        this.Spinner = false;
                        this.licenseInfoVisible = false;
                        this.NoLicenseCard = true;
                        this.OpenLicenseCard = false;
                    }
                }
                this.Spinner = false;
            })
            .catch(errors => {
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const { title, message, errorType } = error;
                    this.dispatchEvent(
                        utils.toastMessageWithTitle(title, message, errorType)
                    );
                } else {
                    this.dispatchEvent(
                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                    );
                }
            });
    }

    licenseInformation(event) {
        let dataIndex = event.currentTarget.dataset.value;
        let datas = this.template.querySelectorAll('.divHighLight');
        for (let i = 0; i < datas.length; i++) {
            if (i == dataIndex)
                datas[i].className = "slds-nav-vertical__item divHighLight slds-is-active";
            else
                datas[i].className = "slds-nav-vertical__item divHighLight";
        }
        this.getLicenseInformation(this.objapplicationid);
    }

    getLicenseInformation(recordId) {
        getLicenseInformation({
            applicationID: recordId
        })
            .then((result) => {
                if (result.length > 0 && result[0].Name) {
                    this.ApplicationNumber = result[0].Name;
                    this.Program = result[0].Program__c ? result[0].Program__c : ' - ';
                    this.ProgramType = result[0].ProgramType__c ? result[0].ProgramType__c : ' - ';
                    this.effectiveDate = result[0].LicenseStartdate__c ? utils.formatDate(result[0].LicenseStartdate__c) : ' - ';
                    this.expirationDate = result[0].LicenseEnddate__c ? utils.formatDate(result[0].LicenseEnddate__c) : ' - ';
                    this.MinAge = result[0].MinAge__c ? result[0].MinAge__c : ' 0 ';
                    this.MaxAge = result[0].MaxAge__c ? result[0].MaxAge__c : ' 0 ';
                    this.Gender = result[0].Gender__c ? result[0].Gender__c : ' - ';
                    this.Race = result[0].Race__c ? result[0].Race__c : ' - ';
                    this.fetchDatasFromAddressPayment();
                    this.fetchDatasFromAddressHome();
                    if (this.objapplicationid != undefined) {
                        getCoApplicantDetails({
                            providerid: this.getProviderId
                        })
                            .then((data) => {
                                if (data.length > 0) {
                                    if (data[0].Contact__r.Title__c != undefined) {
                                        this.CoApplicantName = data[0].Contact__r.Title__c + data[0].Contact__r.FirstName__c + ' ' + data[0].Contact__r.LastName__c;
                                    }
                                    else {
                                        this.CoApplicantName = data[0].Contact__r.FirstName__c + ' ' + data[0].Contact__r.LastName__c;
                                    }
                                }
                            })
                            .catch(errors => {
                                if (errors) {
                                    let error = JSON.parse(errors.body.message);
                                    const { title, message, errorType } = error;
                                    this.dispatchEvent(
                                        utils.toastMessageWithTitle(title, message, errorType)
                                    );
                                } else {
                                    this.dispatchEvent(
                                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                                    );
                                }
                            });
                    }

                    //fetchHomeInformation
                    if (this.objapplicationid != undefined) {
                        fetchHomeInformation({
                            providerid: this.getProviderId
                        })
                            .then((data) => {
                                if (data.length > 0) {
                                    this.childrenResiding = data[0].ChildrenResiding__c ? data[0].ChildrenResiding__c : ' - ';
                                    this.bedRooms = data[0].Bedrooms__c ? data[0].Bedrooms__c : ' - ';
                                }
                            })
                    }
                }
            })
            .catch(errors => {
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const { title, message, errorType } = error;
                    this.dispatchEvent(
                        utils.toastMessageWithTitle(title, message, errorType)
                    );
                } else {
                    this.dispatchEvent(
                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                    );
                }
            });
    }
    fetchDatasFromAddressPayment() {
        fetchDataForAddress({
            fetchdataname: this.getProviderId,
            fetchdatatype: this.getAddressPayment
        }).then(result => {
            if (result.length > 0) {
                this.paymentAddressLine1 = result[0].AddressLine1__c ? result[0].AddressLine1__c : ' - ';
                this.paymentAddressLine2 = result[0].AddressLine2__c ? result[0].AddressLine2__c : ' - ';
                this.paymentMobile = result[0].Phone__c ? result[0].Phone__c : ' - ';
                this.paymentEmail = result[0].Email__c ? result[0].Email__c : ' - ';
            }
            this.Spinner = false;
        })
            .catch(errors => {
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const { title, message, errorType } = error;
                    this.dispatchEvent(
                        utils.toastMessageWithTitle(title, message, errorType)
                    );
                } else {
                    this.dispatchEvent(
                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                    );
                }
            });
    }

    fetchDatasFromAddressHome() {
        fetchDataForAddress({
            fetchdataname: this.getProviderId,
            fetchdatatype: this.getAddressHome
        }).then(result => {
            if (result.length > 0) {
                this.homeAddressLine1 = result[0].AddressLine1__c ? result[0].AddressLine1__c : ' - ';
                this.homeAddressLine2 = result[0].AddressLine2__c ? result[0].AddressLine2__c : ' - ';
                this.homeMobile = result[0].Phone__c ? result[0].Phone__c : ' - ';
                this.homeEmail = result[0].Email__c ? result[0].Email__c : ' - ';
            }
        })
            .catch(errors => {
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const { title, message, errorType } = error;
                    this.dispatchEvent(
                        utils.toastMessageWithTitle(title, message, errorType)
                    );
                } else {
                    this.dispatchEvent(
                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                    );
                }
            });
    }

    redirectToApplication() {
        const oncaseid = new CustomEvent('redirecteditapplication', {
            detail: {
                first: false
            }
        });
        this.dispatchEvent(oncaseid);
    }
}