/**
 * @Author        : Naveen S
 * @CreatedOn     : Jun 2,2020
 * @Purpose       : Complaints Monitering Tabs control 
 * @updatedBy     : Naveen S
 * @updatedOn     : Jun 2,2020
 **/

import {
    LightningElement,
    track,
    wire
} from 'lwc';

//Style Sheet Loading
import {
    loadStyle
} from 'lightning/platformResourceLoader';
import myResource from '@salesforce/resourceUrl/styleSheet';
//For Header Show
import * as sharedData from "c/sharedData";
import getMoniteringId from '@salesforce/apex/applicationMoniterHeader.getMoniteringId';

export default class MoniteringTabs extends LightningElement {
    @track activeTabValue = null;
    //For Header Show
    @track moniteringName = null;

    //Provider Name Show in the Header Function Start
    get monitoringId() {
       return sharedData.getMonitoringId();
    //    return 'a0K0p0000019K0qEAE';
    }

    @wire(getMoniteringId, {
        Id: '$monitoringId'
    })
    providerIdOf({
        error,
        data
    }) {
        if (data) {
            this.moniteringName = data[0].Name;
        } else if (error) {
            this.error = error;
        }
    }
    //Provider Name Show in the Header Function End


    handleActiveTab(event) {
        this.activeTabValue = event.target.value;
    }

    /* Style Sheet Loading */
    renderedCallback() {
        Promise.all([
            loadStyle(this, myResource + '/styleSheet.css')
        ])
    }

    redirectToMonitering() {
        const oncaseid = new CustomEvent('redirecteditmonitering', {
            detail: {
                first: false
            }
        });
        this.dispatchEvent(oncaseid);
    }
}