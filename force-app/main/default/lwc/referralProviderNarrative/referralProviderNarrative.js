import {
    LightningElement,
    track,
    wire
} from "lwc";

//Importing Apex Methods
import saveCaseRecord from "@salesforce/apex/ReferralproviderNarrative.updatenarrativedetails";
import fetchDataForNarrative from "@salesforce/apex/ReferralproviderNarrative.fetchDataForNarrative";

import * as sharedData from "c/sharedData";
import {
    utils
} from "c/utils";
import getReferralProviderStatus from "@salesforce/apex/ReferralproviderNarrative.getReferralProviderStatus";

export default class ReferralProviderNarrative extends LightningElement {

    @track narrativeDescription;
    @track caseidToFetchValue = this.caseid;

    //Sundar modified
    @track btnDisable = false;
    @track richTxtDisable = false;

    get caseid() {
        return sharedData.getCaseId();
    }

    get providerid() {
        return sharedData.getProviderId();
    }

    connectedCallback() {
        fetchDataForNarrative({
            fetchdatanamenew: this.caseid
        }).then(result => {
            this.narrativeDescription = result[0].Description;
        });
    }

    @wire(getReferralProviderStatus, {
        caseId: "$caseidToFetchValue"
    })
    providerStatus({
        error,
        data
    }) {
        if (data) {
            if (
                data.length > 0 &&
                (data[0].Status == "Approved" ||
                    data[0].Status == "Rejected")
            ) {
                this.btnDisable = true;
                this.richTxtDisable = true;
            } else {
                this.btnDisable = false;
                this.richTxtDisable = false;
            }
        }
    }
    handleSpeechToText(event) {

        this.narrativeDescription = event.detail.speechValue;
    }
    narrativeOnChange(event) {
        this.narrativeDescription = event.target.value;
    }

    //Save Functionality
    handleSave() {
        this.template.querySelector('c-common-speech-to-text').stopSpeech();
        let myCaseObj = {
            sobjectType: "Case"
        };
        myCaseObj.Description = this.narrativeDescription;
        myCaseObj.Id = this.caseid;
        saveCaseRecord({
                objSobjecttoUpdateOrInsert: myCaseObj
            })
            .then(result => {
                this.narrativeDescription = null;
                this.dispatchEvent(
                    utils.toastMessage("Referral Saved Successfully", "success")
                );
            })
            .catch(error => {
                this.dispatchEvent(
                    utils.toastMessage("Error in creating record", "error")
                );
            });
    }
}