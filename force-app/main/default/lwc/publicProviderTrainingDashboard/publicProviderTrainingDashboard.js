import {
    LightningElement,
    track,
    api,
    wire
} from "lwc";
import getTrainingData from "@salesforce/apex/publicProviderDashboard.getTrainingData";
import getAllInstructorDetail from "@salesforce/apex/publicProviderDashboard.getAllInstructorDetails";
import {
    loadStyle
} from "lightning/platformResourceLoader";
import myResource from "@salesforce/resourceUrl/styleSheet";
import * as sharedData from "c/sharedData";
import {
    utils
} from "c/utils";
import {
    registerListener,
    unregisterAllListeners
} from "c/pubsub";
import {
    CurrentPageReference
} from "lightning/navigation";
import {
    refreshApex
} from "@salesforce/apex";

const actions = [{
        label: "Edit",
        name: "Edit",
        iconName: "utility:edit",
        target: "_self"
    },
    {
        label: "View",
        name: "View",
        iconName: "utility:preview",
        target: "_self"
    }
];
export default class PublicProviderTrainingDashboard extends LightningElement {
    @track addInstructorModalVisible = false;
    @track totalTraining = "Training Sessions";
    @track instructor = "Instructors";
    _totalTrainingData = [];
    @track _totalTrainingDataLength;
    @track _totalInstructorDataLength;
    @track searchString = "";
    @track spinner = true;
    @api hidePagination;
    @track trainingDatatable = false;
    @track instructorDatatable = true;
    @track clickedRowId;
    @track _viewBtnClicked = false;
    @track _editBtnClicked = false;
    @track addInstructorButtonDisbled = false;
    @track addTraingButtonDisbled = true;
    @wire(CurrentPageReference) pageRef;
    _wiredTrainingData;
    _wiredInstructorData;

    @api get options() {
        return [{
                label: "Training Number",
                fieldName: "Name",
                cellAttributes: {
                    class: "fontclrGrey"
                },
                type: "button",
                sortable: true,
                typeAttributes: {
                    label: {
                        fieldName: "Name"
                    },
                    target: "_self"
                }
            },
            {
                label: "Type",
                type: "button",
                typeAttributes: {
                    name: "dontRedirect",
                    label: {
                        fieldName: "Type__c"
                    }
                },
                cellAttributes: {
                    class: "title"
                }
            },
            {
                label: "Session Type",
                type: "button",
                typeAttributes: {
                    name: "dontRedirect",
                    label: {
                        fieldName: "SessionType__c"
                    }
                },
                cellAttributes: {
                    class: "title"
                }
            },
            // {
            //   label: "Session Number",
            //   fieldName: "SessionNumber__c",
            //   type: "text"
            // },
            {
                label: "Date",
                fieldName: "Date",
                type: "text"
            },
            {
                label: "Time",
                fieldName: "Time__c",
                type: "text"
            },
            {
                label: "Duration",
                fieldName: "Duration__c",
                type: "text"
            },
            {
                label: "No of Trainings Assigned",
                fieldName: "Nooftraineeattended__c",
                type: "text"
            },
            {
                label: "Status",
                fieldName: "Status__c",
                type: "text",
                cellAttributes: {
                    class: {
                        fieldName: "statusClass"
                    }
                }
            },
            {
                label: "",
                type: "action",
                typeAttributes: {
                    rowActions: actions
                },
                cellAttributes: {
                    iconName: "utility:threedots_vertical",
                    iconAlternativeText: "Actions",
                    class: "tripledots"
                }
            }
        ];
    }

    @api get getInstructorDatatable() {
        return this.instructorDatatable;
    }

    @api get getModalVisible() {
        return this.addInstructorModalVisible;
    }
    _editInstructorModal = false;
    @api get editInstructorModal() {
        return this._editInstructorModal;
    }

    @api get totalTrainingData() {
        return this._totalTrainingData;
    }

    @api get totalTrainingDataLength() {
        return this._totalTrainingDataLength;
    }
    connectedCallback() {
        registerListener(
            "RefreshPublicProviderTrainingDashBoard",
            this.handleRefreshActivityPage,
            this
        );
    }
    handleRefreshActivityPage() {
        refreshApex(this._wiredTrainingData);
        refreshApex(this._wiredInstructorData);
    }
    renderedCallback() {
        Promise.all([loadStyle(this, myResource + "/styleSheet.css")]);
    }

    @wire(getAllInstructorDetail)
    getAllInstructorDetailsFromApex(result) {
        this._wiredInstructorData = result;
        let data = result.data;
        this._totalInstructorDataLength = data && data.length;
    }
    disconnectedCallback() {
        unregisterAllListeners(this);
    }
    @wire(getTrainingData, {
        searchString: "$searchString"
    })
    getTrainingDataFromApex(result) {
        this._wiredTrainingData = result;
        let temp = [];
        let id = 1;
        let data = result.data;
        // this.spinner = true

        try {
            data &&
                data.map((item) => {
                    
                    temp.push({
                        Name: item && item.Name,
                        Id: item && item.Id,
                        uniqueId: id,
                        Type__c: item && item.Type__c,
                        SessionType__c: item && item.SessionType__c,
                        Date: this.dateField(item),
                        Status__c: item && item.Status__c,
                        statusClass: this.statusHandler(item && item.Status__c),
                        Duration__c: this.checkSessionTypeProdeData(
                            item.SessionType__c,
                            item,
                            item.Duration__c
                        ),
                        SessionNumber__c: this.sessionNumberDataField(item),

                        Time__c: this.timeDataField(item),
                        Nooftraineeattended__c: item.NoofTraineeAssigned__c
                    });
                    id++;
                    // if (item.SessionType__c === "PRIDE") {
                    //   item.Meeting_Info__r &&
                    //     item.Meeting_Info__r.map((meetingData) => {
                    //       temp.push({
                    //         Name: item && item.Name,
                    //         uniqueId: id,
                    //         Id: item && item.Id,
                    //         Type__c: item && item.Type__c,
                    //         SessionType__c: item && item.SessionType__c,
                    //         SessionNumber__c: meetingData && meetingData.SessionNumber__c,
                    //         Date: this.dateFormat(meetingData && meetingData.Date__c),
                    //         Status__c: item.Status__c && item.Status__c,
                    //         statusClass: this.statusHandler(
                    //           item.Status__c && item.Status__c
                    //         ),
                    //         Duration__c: meetingData && meetingData.Duration__c,
                    //         Time__c:
                    //           utils.formatTime12Hr(
                    //             meetingData && meetingData.StartTime__c
                    //           ) +
                    //           " - " +
                    //           utils.formatTime12Hr(meetingData && meetingData.EndTime__c),
                    //         Nooftraineeattended__c:
                    //           item && item.Nooftraineeattended__c
                    //             ? item && item.Nooftraineeattended__c
                    //             : "-"
                    //       });
                    //       id++;
                    //     });
                    // } else {
                    //   temp.push({
                    //     Name: item && item.Name,
                    //     Id: item && item.Id,
                    //     uniqueId: id,
                    //     Type__c: item && item.Type__c,
                    //     SessionType__c: item && item.SessionType__c,
                    //     SessionNumber__c: "-",
                    //     Date: this.dateFormat(item && item.Date__c),
                    //     Status__c: item && item.Status__c,
                    //     statusClass: this.statusHandler(item && item.Status__c),
                    //     Duration__c: item && item.Duration__c,
                    //     Time__c:
                    //       utils.formatTime12Hr(item.StartTime__c) +
                    //       " - " +
                    //       utils.formatTime12Hr(item.EndTime__c),
                    //     Nooftraineeattended__c:
                    //       item && item.Nooftraineeattended__c
                    //         ? item && item.Nooftraineeattended__c
                    //         : "-"
                    //   });
                    //   id++;
                    // }
                });

            
            this.spinner = false;
        } catch (err) {
            if (err) {
                let error = JSON.parse(err.body.message);
                const {
                    title,
                    message,
                    errorType
                } = error;
                this.dispatchEvent(
                    utils.toastMessageWithTitle(title, message, errorType)
                );
            } else {
                this.dispatchEvent(
                    utils.toastMessage(
                        CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
                        CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
                    )
                );
            }
        }
        this._totalTrainingData = temp;
        this._totalTrainingDataLength = temp && temp.length;
    }

    sessionNumberDataField = (item) => {
        if (item.SessionType__c === "PRIDE") {
            return item.Meeting_Info__r && item.Meeting_Info__r[0].SessionNumber__c;
            // item.Meeting_Info__r && item.Meeting_Info__r.map(data => {
            //   
            //   return data.SessionNumber__c ? data.SessionNumber__c : "-"
            // })
        }
    };

    dateField = (item) => {
        if (item.SessionType__c === "PRIDE") {
            return this.dateFormat(
                item.Meeting_Info__r && item.Meeting_Info__r[0].Date__c
            );
        } else {
            return this.dateFormat(item.Date__c && item.Date__c);
        }
    };

    checkSessionTypeProdeData = (sessionType, item, dataField) => {
        if (sessionType === "PRIDE") {
            return item.Meeting_Info__r && item.Meeting_Info__r[0].Duration__c;
        } else {
            return item.Duration__c && item.Duration__c;
        }
    };

    timeDataField = (item) => {
        if (item.SessionType__c === "PRIDE") {
            return (
                utils.formatTime12Hr(
                    item.Meeting_Info__r && item.Meeting_Info__r[0].StartTime__c
                ) +
                " - " +
                utils.formatTime12Hr(
                    item.Meeting_Info__r && item.Meeting_Info__r[0].EndTime__c
                )
            );
        } else {
            return (
                utils.formatTime12Hr(item.StartTime__c) +
                " - " +
                utils.formatTime12Hr(item.EndTime__c)
            );
        }
    };

    statusHandler = (status) => {
        if (status === "Completed") {
            return "Completed";
        } else if (status === "Draft") {
            return "Draft";
        } else if (status === "In Process") {
            return "Draft";
        } else if (status === "Scheduled") {
            return "Scheduled";
        } else {
            return "";
        }
    };

    // dateFormat function start
    dateFormat = (date) => {
        if (date !== undefined) {
            let spiltedData = date.split("-");
            return `${spiltedData[1]}/${spiltedData[2]}/${spiltedData[0]}`;
        }
    };
    // dateFormat function end

    // handleBtnRedirect start
    handleBtnRedirect = (event) => {
        // let _ComponentToRedirect;
        if (event.target.name === "Add Training") {
            const tooglecmps = new CustomEvent("togglecomponents", {
                detail: {
                    Component: "AddTraining"
                }
            });
            this.dispatchEvent(tooglecmps);
        } else {
            this.addInstructorModalVisible = true;
        }
    };

    handleInstructorModal(event) {
        if (event.detail.Component === "CloseInstructor") {
            this.addInstructorModalVisible = false;
        } else {
            this.addInstructorModalVisible = false;
        }
    }
    // handleBtnRedirect end

    // handleSearch functionality start

    handleSearch = (event) => {
        this.searchString = event.target.value;
    };
    // handleSearch functionality end

    // getRowIdFromDatatable start

    getRowIdFromDatatable = (event) => {

        this._totalTrainingData.map((item) => {
            if (item.uniqueId === event.detail.uniqueId) {
                this.clickedRowId = item.Id && item.Id;
                sharedData.setPublicTrainingrowData(
                    item.SessionNumber__c ? item.SessionNumber__c : "-",
                    item.Id && item.Id,
                    item.Status__c ? item.Status__c : "-"
                );
            }
        });

        const tooglecmps = new CustomEvent("togglecomponents", {
            detail: {
                Component: event.detail.actionType,
                id: this.clickedRowId
            }
        });
        this.dispatchEvent(tooglecmps);
    };

    // getRowIdFromDatatable end

    // handleStatusCard start

    handleStatusCard(event) {
        if (event.currentTarget.dataset.title === "Training Sessions") {
            this.trainingDatatable = true;
            this.instructorDatatable = false;
            this.addInstructorButtonDisbled = true;
            this.addTraingButtonDisbled = false;
            this.activeClass = "";
            this.template
                .querySelector(".cardtraining")
                .classList.remove("cardtraining-cls");
            this.template
                .querySelector(".cardinstructor")
                .classList.add("cardinstructor-cls");
        } else {
            this.trainingDatatable = false;
            this.instructorDatatable = true;
            this.addInstructorButtonDisbled = false;
            this.addTraingButtonDisbled = true;
            this.activeClass = "";
            this.template
                .querySelector(".cardinstructor")
                .classList.remove("cardinstructor-cls");
            this.template
                .querySelector(".cardtraining")
                .classList.add("cardtraining-cls");
        }
    }

    // handleStatusCard end

    // handleEditBtn functionality start

    handleEditBtn = (event) => {
        this._editBtnClicked = event.detail.editBtnClick;
    };
    // handleEditBtn functionality end

    // handleViewBtn functionality start

    handleViewBtn = (event) => {
        this._viewBtnClicked = event.detail.viewBtnClick;
    };
    // handleViewBtn functionality end
}