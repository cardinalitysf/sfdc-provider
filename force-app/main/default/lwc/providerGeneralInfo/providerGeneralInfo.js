/**
 * @Author        : Naveen S
 * @CreatedOn     : April 21 ,2020
 * @Purpose       : General Information of Provider
 * @updatedBy     :
 * @updatedOn     :
 **/

import {
  LightningElement,
  track
} from "lwc";
import { CJAMS_CONSTANTS } from 'c/constants';
import * as sharedData from "c/sharedData";
import getProviderDetails from "@salesforce/apex/providerGeneralInfo.getProviderDetails";
import fetchDataForAddress from "@salesforce/apex/providerGeneralInfo.fetchDataForAddress";
import getApplicationDetails from "@salesforce/apex/providerGeneralInfo.getApplicationDetails";
import myResource from '@salesforce/resourceUrl/styleSheet';
import {
  loadStyle
} from 'lightning/platformResourceLoader';
import images from '@salesforce/resourceUrl/images';
import {
  utils
} from "c/utils";

export default class ProviderGeneralInfo extends LightningElement {
  siteicon = images + '/site-address.svg';
  mainaddress = images + '/main-address.svg';
  paymentaddress = images + '/payment-address.svg';
  providernameicon = images + '/provider-nameicon.svg';
  phone = images + '/phone.svg';
  mobile = images + '/mobile.svg';
  fax = images + '/fax.svg';
  email = images + '/email.svg';
  bedcapacity = images + '/bed-capacity.svg';
  agerange = images + '/age-range.svg';
  iqrange = images + '/iq-range.svg';
  gender = images + '/gender.svg';
  phone2 = images + '/phone2.svg';
  email2 = images + '/email2.svg';
  @track Spinner = true;
  @track getAddressSite = "site";
  @track getAddressMain = "main";
  @track getAddressPayment = "payment";
  @track Program;
  @track ProgramType;
  @track RFP;
  @track TaxId;
  @track SON;
  @track Corporation;
  @track ParentCorporation;
  @track ProviderEmail;
  @track ProviderCellNumber;
  @track ProviderPhone;
  @track ProviderName;
  @track ProviderFirstName;
  @track ProviderLastName;
  @track ProviderFax;
  @track nameDisplay = '';
  @track resultSiteError = false;
  @track resultSite = [];
  @track resultSitePhoneError = false;
  @track resultSiteEmailError = false;
  @track resultMainProvider = [];
  @track resultMainProviderError = false;
  @track resultPaymentError = false;
  @track resultPayment = [];
  @track resultPaymentPhoneError = false;
  @track resultPaymentEmailError = false;
  @track resultMainError = false;
  @track resultMain = [];
  @track resultMainPhoneError = false;
  @track resultMainEmailError = false;
  get getProviderId() {
    return sharedData.getProviderId();
  }
  renderedCallback() {
    Promise.all([
      loadStyle(this, myResource + '/styleSheet.css')
    ])
  }
  connectedCallback() {
    this.getProviderBasicInformation();
    this.fetchDatasFromAddressSite();
    this.fetchDatasFromAddressMain();
    this.fetchDatasFromAddressPayment();
  }

  getProviderBasicInformation() {
    getProviderDetails({
      providerID: this.getProviderId
    }).then(result => {
      this.RFP = result[0].RFP__c ? result[0].RFP__c : ' - ';
      this.TaxId = result[0].TaxId__c ? result[0].TaxId__c : ' - ';
      this.SON = result[0].SON__c ? result[0].SON__c : ' - ';
      this.Corporation = result[0].Corporation__c ? result[0].Corporation__c : ' - ';
      this.ParentCorporation = result[0].ParentCorporation__c ? result[0].ParentCorporation__c : ' - ';
      this.ProviderEmail = result[0].Email__c ? result[0].Email__c : ' - ';
      this.ProviderCellNumber = result[0].CellNumber__c ? result[0].CellNumber__c : ' - ';
      this.ProviderPhone = result[0].Phone ? result[0].Phone : ' - ';
      this.ProviderName = result[0].Name ? result[0].Name : ' - ';
      const x = this.ProviderName;
      this.nameDisplay = x.substring(0, 1).toUpperCase();
      this.ProviderFirstName = result[0].FirstName__c;
      this.ProviderLastName = result[0].LastName__c;
      this.ProviderFax = result[0].Fax ? result[0].Fax : ' - ';
      this.getProgramsInformation();
    })
      .catch(errors => {
        if (errors) {
          let error = JSON.parse(errors.body.message);
          const { title, message, errorType } = error;
          this.dispatchEvent(
            utils.toastMessageWithTitle(title, message, errorType)
          );
        } else {
          this.dispatchEvent(
            utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
          );
        }
      });
  }
  fetchDatasFromAddressSite() {

    fetchDataForAddress({
      fetchdataname: this.getProviderId,
      fetchdatatype: this.getAddressSite
    })
      .then(result => {
        if (result.length > 0) {
          let currentData = []
          result.forEach(res => {
            let obj = {}
            obj.AddressLine1 = res.AddressLine1__c;
            obj.AddressLine2 = res.AddressLine2__c;
            obj.email = res.Email__c != undefined ? res.Email__c : ' - ';
            obj.phone = res.Phone__c != undefined ? res.Phone__c : ' - ';
            currentData.push(obj)
          })
          let arr = currentData;
          var elements = arr.reduce(function (previous, current) {
            var object = previous.filter(object => object.AddressLine1 === current.AddressLine1);
            if (object.length == 0) {
              previous.push(current);
            }
            return previous;
          }, []);
          this.resultSite = elements;
          if (this.resultSite == undefined || this.resultSite == null || this.resultSite == '' || this.resultSite == []) {
            this.resultSiteError = true;
          }
        }
      })
      .catch(errors => {
        if (errors) {
          let error = JSON.parse(errors.body.message);
          const { title, message, errorType } = error;
          this.dispatchEvent(
            utils.toastMessageWithTitle(title, message, errorType)
          );
        } else {
          this.dispatchEvent(
            utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
          );
        }
      });
  }
  fetchDatasFromAddressPayment() {
    fetchDataForAddress({
      fetchdataname: this.getProviderId,
      fetchdatatype: this.getAddressPayment
    }).then(result => {
      if (result.length > 0) {
        let currentData = []
        result.forEach(res => {
          let obj = {}
          obj.AddressLine1 = res.AddressLine1__c;
          obj.AddressLine2 = res.AddressLine2__c;
          obj.email = res.Email__c != undefined ? res.Email__c : ' - ';
          obj.phone = res.Phone__c != undefined ? res.Phone__c : ' - ';
          currentData.push(obj)
        })
        this.resultPayment = currentData;
        if (this.resultPayment == undefined || this.resultPayment == null || this.resultPayment == '' || this.resultPayment == []) {
          this.resultPaymentError = true;
        }
      }
      this.Spinner = false;
    })
      .catch(errors => {
        if (errors) {
          let error = JSON.parse(errors.body.message);
          const { title, message, errorType } = error;
          this.dispatchEvent(
            utils.toastMessageWithTitle(title, message, errorType)
          );
        } else {
          this.dispatchEvent(
            utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
          );
        }
      });
  }
  fetchDatasFromAddressMain() {
    fetchDataForAddress({
      fetchdataname: this.getProviderId,
      fetchdatatype: this.getAddressMain
    }).then(result => {
      if (result.length > 0) {
        let currentData = []
        result.forEach(res => {
          let obj = {}
          obj.AddressLine1 = res.AddressLine1__c;
          obj.AddressLine2 = res.AddressLine2__c;
          obj.email = res.Email__c != undefined ? res.Email__c : ' - ';
          obj.phone = res.Phone__c != undefined ? res.Phone__c : ' - ';
          currentData.push(obj)
        })
        this.resultMain = currentData;
        if (this.resultMain == undefined || this.resultMain == null || this.resultMain == '' || this.resultMain == []) {
          this.resultMainError = true;
        }
      }
    })
      .catch(errors => {
        if (errors) {
          let error = JSON.parse(errors.body.message);
          const { title, message, errorType } = error;
          this.dispatchEvent(
            utils.toastMessageWithTitle(title, message, errorType)
          );
        } else {
          this.dispatchEvent(
            utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
          );
        }
      });
  }
  getProgramsInformation() {
    getApplicationDetails({
      providerId: this.getProviderId
    })
      .then((data) => {
        let _program = data.map(e => e.Program__c).join(",");
        this.Program = Array.from(new Set(_program.split(','))).toString();
        let _programType = data.map(e => e.ProgramType__c).join(",");
        this.ProgramType = Array.from(new Set(_programType.split(','))).toString();
      })
      .catch(errors => {
        if (errors) {
          let error = JSON.parse(errors.body.message);
          const { title, message, errorType } = error;
          this.dispatchEvent(
            utils.toastMessageWithTitle(title, message, errorType)
          );
        } else {
          this.dispatchEvent(
            utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
          );
        }
      });
  }
}