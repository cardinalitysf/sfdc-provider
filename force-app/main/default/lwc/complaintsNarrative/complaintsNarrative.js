/**
 * @Author        : Vijayaraj.M
 * @CreatedOn     : July 02 ,2020
 * @Purpose       : ComplaintsNarrative  Js file
 
 **/

import {
    LightningElement,
    track

} from 'lwc';
import * as referralsharedDatas from "c/sharedData";
import {
    CJAMS_CONSTANTS
} from 'c/constants';

export default class ComplaintsNarrative extends LightningElement {
    
    get caseID() {
    return referralsharedDatas.getCaseId();
//return '5000w000001jy5TAAQ';
}

get recordType(){
 return CJAMS_CONSTANTS.CASE_RECORDTYPE_COMPLAINTS;
}

get sobjectName() {
  
return CJAMS_CONSTANTS.CASE_OBJECT_NAME;
//return 'Case';
}}