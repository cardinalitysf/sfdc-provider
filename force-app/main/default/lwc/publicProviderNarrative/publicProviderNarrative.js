/** 
*    Author : Balamurugan B
*   Modified On : May 15, 2020
**/

import {
    LightningElement,
    track,
    api,
    wire
} from 'lwc';

import {
    utils
} from 'c/utils';
import * as sharedData from "c/sharedData";
import getNarrativeDetails from '@salesforce/apex/PublicProviderNarrative.getNarrativeDetails';
import updateNarrativeDetails from '@salesforce/apex/PublicProviderNarrative.updateNarrativeDetails';
import getHistoryOfNarrativeDetails from '@salesforce/apex/PublicProviderNarrative.getHistoryOfNarrativeDetails';
import getSelectedHistoryRec from '@salesforce/apex/PublicProviderNarrative.getSelectedHistoryRec';
import images from '@salesforce/resourceUrl/images';
import getPublicProviderStatus from "@salesforce/apex/PublicProviderNarrative.getPublicProviderStatus";

import {
    refreshApex
} from "@salesforce/apex";
import { CJAMS_CONSTANTS } from "c/constants";

export default class PublicProviderNarrative extends LightningElement {
    @track result;
    @track data = [];
    @track narrativeValue;
    @track visibleHistoryData = false;
    @track openNewModal = false;
    @track richTextDisable = false;
    @track btnDisable = false;
    @track openNarrative = true;
    @track disableAdd = false;
    @track btnspeechDisable = false;
    @track btnShowHide = true;

    attachmentIcon = images + '/document-newIcon.svg';
    recordId;
    wireNarrativeHistoryDetails;
    //applicationForApiCall = this.caseid;

    //Fetch Case id
    get caseid() {
        //  return '5009D000002f3ZQQAY';
        return sharedData.getCaseId();
    }

    @api
    get ProviderId() {
        return sharedData.getProviderId();
    }
    openNarrativeModel() {
        this.openNarrative = true;
        this.btnDisable = false;
        this.richTextDisable = false;
        this.btnspeechDisable = false;
        this.narrativeValue = '';
    }

    // //get User Details
    // @wire(getUserEmail)
    // email({ error, data }) {
    // if (data) {
    //     let userData = data[0];
    //     if (userData.Profile.Name === 'Supervisor') {
    //     this.userType = 'Supervisor';
    //     } else {
    //     this.userType = 'System';
    //     }
    // } else if (error) {
    //     this.error = error;
    // }
    // }

    @wire(getNarrativeDetails, {
        applicationId: '$caseid'
    })
    wiredNarratvieDetails({
        error,
        data
    }) {
        if (data) {
            try {
                if (data.length > 0) {
                    this.narrativeValue = data[0].Description;
                }
            } catch (error) {
                if (error) {
                    let error = JSON.parse(error.body.message);
                    const { title, message, errorType } = error;
                    this.dispatchEvent(
                        utils.toastMessageWithTitle(title, message, errorType)
                    )
                }
                else {
                    this.dispatchEvent(
                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                    );
                }
            }
        }
    }

    @wire(getHistoryOfNarrativeDetails, {
        applicationId: '$caseid'
    })
    wiredNarrativeHistoryDetails(result) {
        this.wireNarrativeHistoryDetails = result;

        if (result.data) {
            try {
                if (result.data.length > 0) {

                    this.visibleHistoryData = true;

                    this.data = result.data.map((row, index) => {
                        return {
                            Id: row.Id,
                            author: row.LastModifiedBy.Name,
                            lastModifiedDate: row.LastModifiedDate,
                            dataIndex: index
                        }
                    });
                } else {
                    this.visibleHistoryData = false;
                }
            } catch (error) {
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const { title, message, errorType } = error;
                    this.dispatchEvent(
                        utils.toastMessageWithTitle(title, message, errorType)
                    );
                } else {
                    this.dispatchEvent(
                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                    );
                }

            }
        }
    }

    @wire(getPublicProviderStatus, {
        applicationId: '$caseid'
    })
    wiredApplicationDetails({
        error,
        data
    }) {
        if (data) {
            if (data.length > 0 && (data[0].Status == 'Approved' || data[0].Status == 'Rejected')) {
                this.btnDisable = true;
                this.richTextDisable = true;
                this.disableAdd = true;
                this.btnspeechDisable = true;
                this.btnShowHide = false;
            } else {
                this.btnDisable = false;
                this.richTextDisable = false;
                this.disableAdd = false;
                this.btnspeechDisable = false;
                this.btnShowHide = true;
            }
        }
    }


    handleSpeechToText(event) {

        this.narrativeValue = event.detail.speechValue;
    }
    //Narrative On Change Functionality
    narrativeOnChange(event) {
        this.narrativeValue = event.target.value;
    }

    cancelHandler() {
        this.narrativeValue = '';
    }

    //Save Functionality
    saveNarrativeDetails() {
        this.template.querySelector('c-common-speech-to-text').stopSpeech();
        let myObj = {
            'sobjectType': 'Case'
        };

        myObj.Description = this.narrativeValue;

        if (this.caseid)
            myObj.Id = this.caseid;

        updateNarrativeDetails({
            objSobjecttoUpdateOrInsert: myObj
        })
            .then(result => {

                this.narrativeValue = '';

                this.dispatchEvent(utils.toastMessage("Application Saved Successfully", "success"));

                return refreshApex(this.wireNarrativeHistoryDetails);
            }).catch((errors) => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
    }

    //Get respective history record
    narrativeHistoryOnClick(event) {
        this.recordId = event.currentTarget.dataset.id;

        let dataIndex = event.currentTarget.dataset.value;
        let datas = this.template.querySelectorAll('.divHighLight');

        for (let i = 0; i < datas.length; i++) {
            if (i == dataIndex)
                datas[i].className = "slds-nav-vertical__item divHighLight slds-is-active";
            else
                datas[i].className = "slds-nav-vertical__item divHighLight";
        }

        getSelectedHistoryRec({
            recId: this.recordId
        })
            .then(result => {
                if (result.length > 0 && result[0].RichNewValue__c) {
                    this.narrativeValue = result[0].RichNewValue__c;
                    this.btnDisable = true;
                    this.richTextDisable = true;
                    this.btnspeechDisable = true;
                } else {
                    this.narrativeValue = null;
                }
            }).catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
    }
}