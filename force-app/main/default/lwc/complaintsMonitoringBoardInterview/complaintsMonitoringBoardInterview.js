/**
 * @Author        : Naveen S
 * @CreatedOn     : July 02 ,2020
 * @Purpose       : Complaints Monitering  for Staff Interview / Board Interview
 * @updatedBy     : Naveen S
 * @updatedOn     : July 02 ,2020
 **/
import {
  LightningElement,
  track,
  wire,
  api
} from "lwc";

// Utils
/* import { utils } from 'c/utils'; */
import {
  utils
} from "c/utils";
import * as sharedData from "c/sharedData";
import {
  loadStyle
} from "lightning/platformResourceLoader";
import myResource from "@salesforce/resourceUrl/styleSheet";
import {
  constPopupVariables
} from "c/constants";

import { CJAMS_CONSTANTS } from 'c/constants';
//For Create, Refresh Records in Table utils
import {
  refreshApex
} from "@salesforce/apex";
import {
  createRecord,
  deleteRecord,
  updateRecord
} from "lightning/uiRecordApi";
import {
  CurrentPageReference
} from "lightning/navigation";
import {
  fireEvent
} from "c/pubsub";

import REFVALUE_OBJECT from "@salesforce/schema/ReferenceValue__c";
import REGULATIONS_FIELD from "@salesforce/schema/ReferenceValue__c.Regulations__c";
import QUESTION_FIELD from "@salesforce/schema/ReferenceValue__c.Question__c";
import TYPE_FIELD from "@salesforce/schema/ReferenceValue__c.Type__c";
import RECORDTYPEID_FIELD from "@salesforce/schema/ReferenceValue__c.RecordTypeId";

// for provider question
import MonitoringIdFromProRecQues from "@salesforce/schema/ProviderRecordQuestion__c.Monitoring__c";
import StatusFromProRecQues from "@salesforce/schema/ProviderRecordQuestion__c.Status__c";
import RecordTypeIdFromProRecQues from "@salesforce/schema/ProviderRecordQuestion__c.RecordTypeId";
import SiteIdFromProRecQues from "@salesforce/schema/ProviderRecordQuestion__c.Site__c";

import StaffIdIdFromProRecQues from "@salesforce/schema/ProviderRecordQuestion__c.Name__c";
import StaffIdIdFromProRecQuesActor from "@salesforce/schema/ProviderRecordQuestion__c.Actor__c";
import DateFieldFromProRecQues from "@salesforce/schema/ProviderRecordQuestion__c.Date__c";
import PROVIDER_RECORD_QUESTION_API from "@salesforce/schema/ProviderRecordQuestion__c";

// for provider answer
import DateFieldFromProAnsRec from "@salesforce/schema/ProviderRecordAnswer__c.Date__c";
import IdFieldFromProAnsRec from "@salesforce/schema/ProviderRecordAnswer__c.Id";
import ComarFieldFromProAnsRec from "@salesforce/schema/ProviderRecordAnswer__c.Comar__c";

import ProfileFieldFromProAnsRec from "@salesforce/schema/ProviderRecordAnswer__c.Profit__c";
import CommentsFieldFromProAnsRec from "@salesforce/schema/ProviderRecordAnswer__c.Comments__c";
import ProviderRecQuestionFieldFromProAnsRec from "@salesforce/schema/ProviderRecordAnswer__c.ProviderRecordQuestion__c";

import getRecordDetails from "@salesforce/apex/ComplaintsMonitoringBoardInterview.getRecordDetails";
import getQuestionsByType from "@salesforce/apex/ComplaintsMonitoringBoardInterview.getQuestionsByType";
import bulkAddRecordAns from "@salesforce/apex/ComplaintsMonitoringBoardInterview.bulkAddRecordAns";
import applicationStaff from "@salesforce/apex/ComplaintsMonitoringBoardInterview.applicationStaff";
import getStaffFromApplicationId from "@salesforce/apex/ComplaintsMonitoringBoardInterview.getStaffFromApplicationId";
import getProviderType from "@salesforce/apex/ComplaintsMonitoringBoardInterview.getProviderType";
import getHouseholdMembers from "@salesforce/apex/ComplaintsMonitoringBoardInterview.getHouseholdMembers";

import GetSiteIdFromApex from "@salesforce/apex/ComplaintsMonitoringBoardInterview.getSiteId";
import GetRecordTypeFromApex from "@salesforce/apex/ComplaintsMonitoringBoardInterview.getRecordType";

// For Edit Record
import EditRecordData from "@salesforce/apex/ComplaintsMonitoringBoardInterview.editRecordData";

// for update Record
import QuestionIdField from "@salesforce/schema/ProviderRecordQuestion__c.Id";
import StatusFieldFromProRecQue from "@salesforce/schema/ProviderRecordQuestion__c.Status__c";
import DateFieldFromProRecQue from "@salesforce/schema/ProviderRecordQuestion__c.Date__c";
import bulkUpdateRecordAns from "@salesforce/apex/ComplaintsMonitoringBoardInterview.bulkUpdateRecordAns";
import images from "@salesforce/resourceUrl/images";


export default class ComplaintsMonitoringBoardInterview extends LightningElement {
  //Get Active Tab Record
  @api getActiveTab = null;
  @track hasStaffEnable = true;
  @track hasStaffCreate = false;
  @track recordNewModel = false;
  @track staffLists = [];

  //DataTable Data
  @track dataTableShow = false;
  @track columns = [];
  @track tabContent = "";
  @track totalRecords = [];
  @track datas = [];
  @track btnEnable = true;
  @track questions = [];
  wiredRecordDetails;
  questionsRegulations;
  editRecordDataFromApex;

  @api staffId;
  @track emailId = "";
  @api recordTypeId = null;

  //Label for New Record Creation
  @track newRecordCreate = null;
  @track openModel = false;
  @track createComar = {};
  @track type;
  answer;

  //fieldType

  @track fieldType;
  questionFieldTypeFromChild = [];

  // for site id
  @track siteId;
  @track programSite;

  // for delete
  deleteRowId;

  // for onchange handler
  questionandAnswer = new Map();

  /* Non - Comliance Row Functions Start */
  @track value = "inProgress";
  @track finding = null;
  attachmentIcon = images + "/application-licenseinfo.svg";
  // for edit
  @track openDeleteModal = false;
  @track openEditModal;
  @track editPageOpen = false;
  @track isDisable = false;

  //Pagination Tracks    
  @track page = 1;
  perpage = 10;
  setPagination = 5;
  @track totalRecordsCount = 0;

  @track isSuperUserFlow = false;
  @track Spinner = true;
  @track providerType;

  //DataTable RowAction Method Start
  constructor() {
    super();
    this.columns = [{
      label: "Name",
      fieldName: "Name__c",
      initialWidth: 300,
      sortable: true
    },
    {
      label: "Update Date",
      fieldName: "Date__c",
      initialWidth: 200
    },
    {
      label: "Updated By",
      fieldName: "LastModifiedBy",
      initialWidth: 200
    },
    {
      label: 'Status',
      fieldName: 'Status__c',
      cellAttributes: {
        class: {
          fieldName: 'statusClass'
        }
      }
    },
    {
      label: 'Action',
      type: 'button',
      initialWidth: 100,
      fieldName: 'id',
      typeAttributes: {
        iconName: 'utility:preview',
        name: 'View',
        title: 'view',
        disabled: false,
        initialWidth: 20,
        target: '_self'
      }
    },
    {
      type: "button",
      initialWidth: 40,
      fieldName: "id",
      typeAttributes: {
        iconName: "utility:edit",
        name: "Edit",
        title: "Edit",
        disabled: false,
        target: "_self",
        initialWidth: 20,
        class: "action-position"
      }
    },
    {
      type: "button",
      fieldName: "id",
      typeAttributes: {
        iconName: "utility:delete",
        name: "Delete",
        title: "Delete",
        disabled: false,
        iconPosition: "left",
        target: "_self",
        initialWidth: 20,
        class: "del-red action-positiontrd",
      }
    }
    ];
  }
  @wire(CurrentPageReference) pageRef;
  // for siteId
  get getMonitoringAddressId() {
    return sharedData.getMonitoringAddressId();
  }

  get monitoringStatus() {
    return sharedData.getMonitoringStatus();
  }

  get applicationId() {
    return sharedData.getApplicationId();
  }

  get providerId() {
    return sharedData.getProviderId();
  }

  get monitoringId() {
    return sharedData.getMonitoringId();
  }

  //Supervisor Login Check
  get profileName() {
    return sharedData.getUserProfileName();
  }


  // get site id

  @wire(GetSiteIdFromApex, {
    siteId: "$getMonitoringAddressId"
  })
  getSiteId({
    data,
    error
  }) {
    if (data) {
      try {
        data.map((item) => {
          this.programSite = item.AddressLine1__c;
        });
      }
      catch (error) {
        if (error) {
          let error = JSON.parse(error.body.message);
          const { title, message, errorType } = error;
          this.dispatchEvent(
            utils.toastMessageWithTitle(title, message, errorType)
          )
        }
        else {
          this.dispatchEvent(
            utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
          );
        }
      }
    }
  }

  // end get site id

  //DataTable RowAction Method Start

  @wire(GetRecordTypeFromApex, {
    name: "$type"
  })
  getRecordType({
    data,
    error
  }) {
    try {
      if (data) {
        this.recordTypeId = data;
      }
    }
    catch (error) {
      if (error) {
        let error = JSON.parse(error.body.message);
        const { title, message, errorType } = error;
        this.dispatchEvent(
          utils.toastMessageWithTitle(title, message, errorType)
        )
      }
      else {
        this.dispatchEvent(
          utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
        );
      }
    }
  }

  connectedCallback() {
    this.getProviderTypeDetails();
    //  if (this.monitoringStatus !== 'Draft')
    //    this.isDisable = true;
    if (this.getActiveTab === "boardIntTab") {
      this.newRecordCreate = "Add Board Interview";
      this.type = "Board Interview";
    } else {
      this.newRecordCreate = "Add Staff Interview";
      this.type = "Staff Interview";
    }

    if (this.profileName === 'Supervisor')
      this.isSuperUserFlow = true;
    else
      this.isSuperUserFlow = false;
    this.Spinner = false;
  }

  getRowActions(row, doneCallback) {
    const actions = [];
    actions.push({
      label: "Edit",
      iconName: "action:edit",
      name: "editStaff"
    });
    actions.push({
      label: "View",
      iconName: "action:view",
      name: "viewStaff"
    });
    actions.push({
      label: "Delete",
      iconName: "action:delete",
      name: "deleteStaff"
    });
    // simulate a trip to the server
    setTimeout(() => {
      doneCallback(actions);
    }, 200);
  }

  /* Style Sheet Loading */
  /*renderedCallback() {
    Promise.all([loadStyle(this, myResource + "/styleSheet.css")]);
  }*/

  pageData() {
    let page = this.page;
    let perpage = this.perpage;
    let startIndex = page * perpage - perpage;
    let endIndex = page * perpage;
    this.datas = this.totalRecords.slice(startIndex, endIndex);
  }

  //Get Record Detais of Staff / Physical Plant / Office Function End

  //Get Record Detais of Staff / Physical Plant / Office Function Start
  @wire(getRecordDetails, {
    monitoringId: "$monitoringId",
    siteId: "$getMonitoringAddressId",
    recordType: "$recordTypeId"
  })
  recordDetails(result) {
    this.wiredRecordDetails = result;
    let currentData = [];
    try {
      if (result.data) {
        let data = result.data;

        data.map((item) => {
          let rowData = {};
          (rowData.id = item.Id),
            (rowData.Name__c = item.Name__r ? item.Name__r.FirstName__c : item.Actor__r.Contact__r.FirstName__c);

          rowData.Status__c = item.Status__c;
          rowData.LastModifiedBy = item.LastModifiedBy ?
            item.LastModifiedBy.Name :
            null;
          rowData.Date__c = utils.formatDate(item.Date__c);
          rowData.btnAlignFrt = "btnAlignFrt";
          rowData.btnAlignSec = "btnAlignSec";
          rowData.statusClass = item.Status__c;
          currentData.push(rowData);
        });

        this.totalRecords = currentData;
        this.totalRecordsCount = this.totalRecords.length;
        this.setNoRecordImage();
        this.pageData();
        this.Spinner = false;
      }
    } catch (error) {
      if (error) {
        let error = JSON.parse(error.body.message);
        const { title, message, errorType } = error;
        this.dispatchEvent(
          utils.toastMessageWithTitle(title, message, errorType)
        )
      }
      else {
        this.dispatchEvent(
          utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
        );
      }
    }
  }

  setNoRecordImage() {
    if (this.totalRecords.length > 0) {
      this.dataTableShow = false;
    } else {
      this.dataTableShow = true;
    }
  }

  changeQuestionAnswerHandle(event) {
    let fieldType = event.detail.fieldType;
    let comar = event.detail.comar;
    this.questionFieldTypeFromChild = [
      ...this.questionFieldTypeFromChild,
      {
        comar,
        fieldType
      }
    ];
    this.questionandAnswer.set(event.detail.comar, event.detail.answer);
  }

  @wire(getQuestionsByType, {
    type: "$type"
  })
  wiredquestions(data) {
    try {
      this.questionsRegulations = data;
      if (data.data != null && data.data != "") {
        let currentData = [];
        let id = 1;
        data.data.forEach((row) => {
          let rowData = {};
          rowData.rowId = id;
          rowData.Id = row.Id;
          rowData.answer = null;
          rowData.Question__c = row.Question__c;
          rowData.FieldType__c = row.FieldType__c;
          rowData.QuestionOrder__c = row.QuestionOrder__c;
          currentData.push(rowData);
          id++;
        });
        this.questions = currentData;
      }
    }
    catch (error) {
      if (error) {
        let error = JSON.parse(error.body.message);
        const { title, message, errorType } = error;
        this.dispatchEvent(
          utils.toastMessageWithTitle(title, message, errorType)
        )
      }
      else {
        this.dispatchEvent(
          utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
        );
      }
    }
  }

  // close modal

  closeNewRecord() {
    this.recordNewModel = false;
    this.editPageOpen = false;
    this, (this.hasStaffEnable = true);
  }

  // onclick Add Interview tab button functionality

  createNewRecordModelPop(event) {
    this.questions = this.questions.reduce((ds, d) => {
      let newD = d;
      if (d.answer != null) {
        newD = Object.assign({}, d, {
          answer: null
        });
      }
      return ds.concat(newD);
    }, []);

    if (this.hasStaffCreate === true) {
      if (event.target.name === "staff") {
        this.staffId = event.target.value;
      }
      if (event.target.value !== undefined) {
        this.recordNewModel = true;
        this.btnEnable = true;
        this.hasStaffCreate = false;
      }
    }
    this.hasStaffCreate = true;
    this.getProviderTypeDetails();


  }

  // End onclick Add Interview tab button functionality

  // Edit and delete actin functionality

  handleRowAction(event) {
    const action = event.detail.action;
    const row = event.detail.row;
    switch (action.name) {
      case "Edit":
        if (this.isDisable == true)
          return this.dispatchEvent(utils.toastMessage(constPopupVariables.UNEDIT, "warning"));
        this.editPageOpen = true;
        this.recordNewModel = false;
        this.hasStaffEnable = false;
        this.hasStaffCreate = false;
        this.editRecordId = row.id;
        let data = this.totalRecords.filter((q) => q.id === row.id);
        this.btnEnable = data[0].Status__c !== "Completed";
        return refreshApex(this.wiredRecordDetails);
        break;
      case "Delete":
        if (this.isDisable == true)
          return this.dispatchEvent(utils.toastMessage(constPopupVariables.UNDELETE, "warning"));
        this.deleteRowId = row.id;
        this.openDeleteModal = true;
        break;
      case "View":
        this.editable = false;
        this.editPageOpen = true;
        this.recordNewModel = false;
        this.hasStaffEnable = false;
        this.editRecordId = row.id;
        this.btnEnable = false;
        return refreshApex(this.wiredRecordDetails);
        break;
    }
  }

  // delete Functionality
  deleteHandler() {
    deleteRecord(this.deleteRowId).then((response) => {
      this.openDeleteModal = false;
      this.dispatchEvent(
        utils.toastMessage(constPopupVariables.DELETESUCCESS, "success")
      );
      return refreshApex(this.wiredRecordDetails);
    });
  }

  // delete functionality end

  // Edit and delete action functionality  end

  // Edit Functionality

  @wire(EditRecordData, {
    questionId: "$editRecordId"
  })
  editRecordDataValue(record) {
    this.editRecordDataFromApex = record;
    let currentData = [];
    // let id = 1;

    if (record.data) {
      record.data.map((item) => {
        currentData.push({
          //   rowId: id,
          Id: item.Id,
          answer: item.Comments__c ? item.Comments__c : item.Profit__c,
          Question__c: item.Comar__r ? item.Comar__r.Question__c : null,
          FieldType__c: item.Comar__r ? item.Comar__r.FieldType__c : null,
          QuestionOrder__c: item.Comar__r ?
            item.Comar__r.QuestionOrder__c :
            null
        });
      });
    }

    this.questions = currentData;
  }

  // End Edit Functionality

  // Save Record Functionality

  saveRecords(event) {
    this.isDisable = true;
    let status;
    if (event.target.value === "Draft") {
      status = "Draft";
    } else {
      status = "Completed";
    }

    let fields = {};
    fields[MonitoringIdFromProRecQues.fieldApiName] = this.monitoringId;
    fields[StatusFromProRecQues.fieldApiName] = status;
    if (this.providerType == 'Public') {
      fields[StaffIdIdFromProRecQuesActor.fieldApiName] = this.staffId;
    }
    else {
      fields[StaffIdIdFromProRecQues.fieldApiName] = this.staffId;
    }
    fields[RecordTypeIdFromProRecQues.fieldApiName] = this.recordTypeId;
    fields[SiteIdFromProRecQues.fieldApiName] = this.getMonitoringAddressId;
    fields[DateFieldFromProRecQues.fieldApiName] = new Date();
    const recordInput = {
      apiName: PROVIDER_RECORD_QUESTION_API.objectApiName,
      fields
    };
    createRecord(recordInput)
      .then((response) => {
        if (response) {
          this.providerQuestionId = response.id;
          this.formQuestionAnswerArray(response.id);
        }
        this.hasStaffCreate = false;
        this.isDisable = false;
      })
      .catch((err) => {

      });
  }

  formQuestionAnswerArray(id) {
    let questionArray = this.questions;
    let questTempArray = [];

    let list = [];
    this.questionandAnswer.forEach((data, key) => list.push(key));
    let someLIst = questionArray.filter((item) => !list.includes(item.Id));
    someLIst.map((data) => {
      questTempArray.push({
        comar: data.Id,
        providerQuestionId: id,
        Question__c: data.Question__c,
        QuestionOrder__c: data.QuestionOrder__c,
        profit: "",
        comments: ""
      });
    });

    this.questionandAnswer.forEach((data, key) => {
      let filteredArray = questionArray.filter((item) => item.Id === key);
      if (filteredArray.length > 0) {
        if (filteredArray[0].FieldType__c === "Text Box") {
          questTempArray.push({
            comar: filteredArray[0].Id,
            providerQuestionId: id,
            Question__c: filteredArray[0].Question__c,
            QuestionOrder__c: filteredArray[0].QuestionOrder__c,
            comments: data,
            profit: ""
          });
        } else if (filteredArray[0].FieldType__c === "Radio Button") {
          questTempArray.push({
            comar: filteredArray[0].Id,
            providerQuestionId: id,
            Question__c: filteredArray[0].Id,
            QuestionOrder__c: filteredArray[0].QuestionOrder__c,
            profit: data,
            comments: ""
          });
        }
      }
    });

    this.insertQuestionAnswerToProRecAns(questTempArray);
  }

  insertQuestionAnswerToProRecAns(questTempArray) {
    let data = [];
    questTempArray = questTempArray.slice(0);
    questTempArray.sort(function (a, b) {
      var x = a.QuestionOrder__c;
      var y = b.QuestionOrder__c;
      return x < y ? -1 : x > y ? 1 : 0;
    });

    questTempArray.map((item) => {
      let fieldSet = {};
      // fieldSet[DateFieldFromProAnsRec.fieldApiName] = this.formatDate(new Date()),
      (fieldSet[ComarFieldFromProAnsRec.fieldApiName] = item.comar),
        (fieldSet[CommentsFieldFromProAnsRec.fieldApiName] = item.comments ?
          item.comments :
          ""),
        (fieldSet[ProfileFieldFromProAnsRec.fieldApiName] = item.profit ?
          item.profit :
          ""),
        (fieldSet[ProviderRecQuestionFieldFromProAnsRec.fieldApiName] =
          item.providerQuestionId);
      data.push(fieldSet);
    });

    let convertedData = JSON.stringify(data);

    bulkAddRecordAns({
      datas: convertedData
    }).then((response) => {
      this.dispatchEvent(
        utils.toastMessage("Record has been Added Successfully", "success")
      );
      fireEvent(this.pageRef, "RefreshActivityPage", true);
      refreshApex(this.editRecordDataFromApex);
      refreshApex(this.wiredRecordDetails);
    }).catch(errors => {
      if (errors) {
        let error = JSON.parse(errors.body.message);
        const { title, message, errorType } = error;
        this.dispatchEvent(
          utils.toastMessageWithTitle(title, message, errorType)
        );
      } else {
        this.dispatchEvent(
          utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
        );
      }
    });
    this.recordNewModel = false;
    this.hasStaffEnable = true;
  }

  // save record functionality end

  // update Record functionality

  updateRecordData(event) {
    this.isDisable = true;
    let status;
    if (event.target.value === "Draft") {
      status = "Draft";
    } else {
      status = "Completed";
    }
    let fields = {};
    fields[QuestionIdField.fieldApiName] = this.editRecordId;
    fields[StatusFieldFromProRecQue.fieldApiName] = status;
    fields[DateFieldFromProRecQue.fieldApiName] = new Date();

    const recordInput = {
      fields
    };

    updateRecord(recordInput).then((response) =>
      this.formUpdateRecordData(this.editRecordId)
    );
    this.hasStaffCreate = false;
    this.isDisable = false;
  }

  formUpdateRecordData = (id) => {
    let questionArray = this.questions;
    let questTempArray = [];

    let list = [];
    this.questionandAnswer.forEach((data, key) => list.push(key));
    let someLIst = questionArray.filter((item) => !list.includes(item.Id));
    let profit;
    let comments;
    someLIst.map((data) => {
      if (data.FieldType__c === "Text Box") {
        if (data.answer !== undefined) {
          comments = data.answer;
          profit = "";
        } else {
          comments = "";
          profit = "";
        }
      } else if (data.FieldType__c === "Radio Button") {
        if (data.answer !== undefined) {
          profit = data.answer;
          comments = "";
        } else {
          profit = "";
          comments = "";
        }
      }

      questTempArray.push({
        Id: data.Id,
        providerQuestionId: id,
        Question__c: data.Question__c,
        profit: profit,
        comments: comments
      });
    });

    this.questionandAnswer.forEach((data, key) => {
      let filteredArray = questionArray.filter((item) => item.Id === key);
      if (filteredArray.length > 0) {
        if (filteredArray[0].FieldType__c === "Text Box") {
          questTempArray.push({
            Id: filteredArray[0].Id,
            providerQuestionId: id,
            Question__c: filteredArray[0].Question__c,
            comments: data,
            profit: ""
          });
        } else if (filteredArray[0].FieldType__c === "Radio Button") {
          questTempArray.push({
            Id: filteredArray[0].Id,
            providerQuestionId: id,
            Question__c: filteredArray[0].Id,
            profit: data,
            comments: ""
          });
        }
      }
    });

    this.updateQuestionAnswerToProRecAns(questTempArray);
  };

  updateQuestionAnswerToProRecAns = (questTempArray) => {
    let data = [];
    questTempArray = questTempArray.slice(0);
    questTempArray.sort(function (a, b) {
      var x = a.QuestionOrder__c;
      var y = b.QuestionOrder__c;
      return x < y ? -1 : x > y ? 1 : 0;
    });
    questTempArray.map((item) => {
      let fieldSet = {};
      // fieldSet[DateFieldFromProAnsRec.fieldApiName] = this.formatDate(new Date()),
      (fieldSet[IdFieldFromProAnsRec.fieldApiName] = item.Id),
        (fieldSet[CommentsFieldFromProAnsRec.fieldApiName] = item.comments ?
          item.comments :
          ""),
        (fieldSet[ProfileFieldFromProAnsRec.fieldApiName] = item.profit ?
          item.profit :
          ""),
        (fieldSet[ProviderRecQuestionFieldFromProAnsRec.fieldApiName] =
          item.providerQuestionId);
      data.push(fieldSet);
    });

    let convertedData = JSON.stringify(data);

    bulkUpdateRecordAns({
      datas: convertedData
    })
      .then((response) => {
        this.editPageOpen = false;
        this.dispatchEvent(utils.toastMessage("Record has been Updated Successfully", "success"));
        fireEvent(this.pageRef, "RefreshActivityPage", true);
        refreshApex(this.editRecordDataFromApex);
        refreshApex(this.wiredRecordDetails);
      })
      .catch(errors => {
        if (errors) {
          let error = JSON.parse(errors.body.message);
          const { title, message, errorType } = error;
          this.dispatchEvent(
            utils.toastMessageWithTitle(title, message, errorType)
          );
        } else {
          this.dispatchEvent(
            utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
          );
        }
      });
    this.closeNewRecord();
    this.hasStaffEnable = true;
  };

  // update Record functionality end

  /* On-Click Button Change Functionlity on New Record Creation Start */
  openNewRow(event) {
    if (event.target.value != undefined && event.target.value != null) {
      let allD = this.template.querySelectorAll("tbody");
      allD[event.target.value - 1].rows[1].className = "displayContent";
      allD[event.target.value - 1].rows[0].cells[1].childNodes.forEach(
        (but) => {
          but.className =
            but.className.includes("backgroComboColor") == true ?
              but.className.replace(" backgroComboColor", "") :
              but.className;
          but.className =
            2 === parseInt(but.dataset.id, 10) ?
              but.className + " backgroComboColor" :
              but.className;
        }
      );
    }
  }

  /* On-Click Button Change Functionlity on New Record Creation End */



  handleChange(event) {
    this.value = event.detail.value;
  }

  handleFindChange(event) {
    this.finding = event.detail.value;
  }

  /* Non - Comliance Row Functions End */

  /* New Comar Add POP-UP Function Start */


  closeModal() {
    this.openDeleteModal = false;
  }

  comarOnChange(event) {
    if (event.target.name == "question") {
      this.createComar.Question__c = event.detail.value;
    }
  }

  createComar() {
    const allValid = [
      ...this.template.querySelectorAll("section lightning-input")
    ].reduce((validSoFar, inputFields) => {
      inputFields.reportValidity();
      return validSoFar && inputFields.checkValidity();
    }, true);
    this.createComar.type = this.type;
    if (allValid) {
      const fields = {};
      fields[QUESTION_FIELD.fieldApiName] = this.createComar.question;
      fields[TYPE_FIELD.fieldApiName] = this.createComar.type;
      fields[RECORDTYPEID_FIELD.fieldApiName] = "Question";
    }
    const recordInput = {
      apiName: CONTACTNOTES_OBJECT.objectApiName,
      fields
    };

    createRecord(recordInput)
      .then((result) => {
        this.openModel = false;
        this.createComar = {};
        this.dispatchEvent(
          utils.toastMessage("Comar Has Been Saved Successfully!..", "success")
        );

        return refreshApex(this.wiredRecordDetails);
      })
      .catch(errors => {
        if (errors) {
          let error = JSON.parse(errors.body.message);
          const { title, message, errorType } = error;
          this.dispatchEvent(
            utils.toastMessageWithTitle(title, message, errorType)
          );
        } else {
          this.dispatchEvent(
            utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
          );
        }
      });
  }

  /* New Comar Add POP-UP Function End */

  /* Staff List Load In Staff Record Page Function Start */

  initialStaffMemberLoad() {
    applicationStaff({
      newStaff: this.providerId
    })
      .then((result) => {
        this.staffDataItem = result;
        this.AccountId = this.providerId;
        this.staffload1();
        //this.staffload2();
      })
      .catch((error) => {
        this.error = error.message;
      });
  }

  staffload1() {
    getStaffFromApplicationId({
      getcontactfromapp: this.applicationId
    })
      .then((result) => {
        try {
          this.staffDataItem1 = result;
          this.staffload2();
        }
        catch (error) {
          if (error) {
            let error = JSON.parse(error.body.message);
            const { title, message, errorType } = error;
            this.dispatchEvent(
              utils.toastMessageWithTitle(title, message, errorType)
            )
          }
          else {
            this.dispatchEvent(
              utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
            );
          }
        }
      })


  }

  staffload2() {
    let tmperrarray = [];
    this.staffDataItem1.forEach((staff) => {
      if (staff.Staff__r.FirstName__c !== undefined) {
        let obj = {};

        obj.label = staff.Staff__r.FirstName__c;
        obj.value = staff.Staff__r.Id;
        tmperrarray.push(obj);
      }
    });

    this.staffLists = tmperrarray;
  }


  getPublicStaffDetails() {
    getHouseholdMembers({
      providerid: this.providerId
    }).then((data) => {
      try {
        if (data.length > 0) {
          var selectHouseholdOptions = [];
          data.forEach((row) => {
            selectHouseholdOptions.push({
              label: row.Contact__r.FirstName__c + ' ' + row.Contact__r.LastName__c,
              value: row.Id
            });
          });
          this.staffLists = selectHouseholdOptions;
        }
      } catch (error) {
        throw error;
      }

    });
  }


  getProviderTypeDetails() {
    getProviderType({
      providerid: this.providerId
    }).then((data) => {
      try {

        if (data.length > 0) {
          this.providerType = data[0].ProviderType__c;

        }
        if (this.providerType == 'Public') {
          this.getPublicStaffDetails();
        }
        else {
          this.initialStaffMemberLoad();
        }
      }
      catch (error) {
        throw error;
      }
    });
  }

  /* Staff List Load In Staff Record Page Function End */
}