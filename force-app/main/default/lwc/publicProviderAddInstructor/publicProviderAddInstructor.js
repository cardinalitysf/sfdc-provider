/**
 * @Author        : G.sathishkumar
 * @CreatedOn     : May 13, 2020
 * @Purpose       : This component contains Instructor DETAILS .
 **/

import {
    LightningElement,
    track,
    wire,
    api
} from 'lwc';
import getAllInstructorDetails from "@salesforce/apex/PublicProviderAddInstructor.getAllInstructorDetails";
import getRecordType from "@salesforce/apex/PublicProviderAddInstructor.getRecordType";
import getTrainingDetails from "@salesforce/apex/PublicProviderAddInstructor.getTrainingDetails";
import editInstructorDetails from "@salesforce/apex/PublicProviderAddInstructor.editInstructorDetails";
import { CJAMS_CONSTANTS} from "c/constants";

import {
    refreshApex
} from "@salesforce/apex";
import {
    utils
} from 'c/utils';


import {
    getPicklistValuesByRecordType,
    getObjectInfo
} from 'lightning/uiObjectInfoApi';
import {
    createRecord,
    updateRecord,
    deleteRecord
} from "lightning/uiRecordApi";
import {
    CurrentPageReference
} from 'lightning/navigation';
import {
    fireEvent
} from 'c/pubsub';
import CONTACT_OBJECT from '@salesforce/schema/Contact';
import RECORDTYPEID_FIELD from '@salesforce/schema/Contact.RecordTypeId';
import ID_FIELD from '@salesforce/schema/Contact.Id';
import FIRSTNAME_FIELD from '@salesforce/schema/Contact.FirstName';
import LASTNAME_FIELD from '@salesforce/schema/Contact.LastName';
import GENDER_FIELD from '@salesforce/schema/Contact.Gender__c';
import PHONE_FIELD from '@salesforce/schema/Contact.Phone';
import EMAIL_FIELD from '@salesforce/schema/Contact.Email';

import images from '@salesforce/resourceUrl/images';

const columns = [

    {
        label: 'FIRST NAME',
        fieldName: 'FirstName',
        type: 'text',
    },
    {
        label: 'LAST NAME',
        fieldName: 'LastName',
        type: 'text',
    },
    {
        label: 'GENDER',
        fieldName: 'Gender__c',
        type: 'text',
    },
    {
        label: 'PHONE NUMBER',
        fieldName: 'Phone',
        type: 'text',
    },
    {
        label: 'EMAIL ADDRESS',
        fieldName: 'Email',
        type: 'text',
    },
];

export default class PublicProviderAddInstructor extends LightningElement {

    @track columns = columns;
    @track instructorDetails = {};
    @track currentPageInstructorData;
    @wire(CurrentPageReference) pageRef;

    @track disabled = false;
    @track wiredinstructorDetails;
    contactId;

    @track title;
    @api recordTypeId = null;
    @track trainingAllData;
    @track trainingCombineItem;

    descriptionIcon = images + '/description-icon.svg';
    instructorIcon = images + '/instructor-noIcon.svg';
    @track descModel = false;
    @track descriptionView;
    @track labeltext;
    @track Spinner = true;


    @track deleteModel = false;

    //Pagination Details
    @track norecorddisplay = true;
    @track totalRecordsCount = 0;
    @track totalRecords;
    @track page = 1;
    perpage = 5;
    setPagination = 5;

    @track _showDatatable;
    @track _modalVisible;


    @api get tableData() {
        return this._showDatatable;
    }

    set tableData(value) {
        this.setAttribute("getInstructorDatatable", value);
        this._showDatatable = value;

    }

    @api get modalVisible() {
        return this._modalVisible;

    }

    set modalVisible(value) {
        this.labeltext = "ADD";
        this.disabled = true;
        this.setAttribute("getModalVisible", value);
        if (!this.contactId) {
            this.title = 'Add instructor';
        }
        this._modalVisible = value;
    }

    //Open Instructor Model Box
    openInstructorModel() {
        this.labeltext = "UPDATE";
        this._modalVisible = true;
    }

    //Close Instructor Model Box
    closeInstructorModel() {
        this.instructorDetails = {};
        this.contactId = null;
        this.notifyParentToCloseModal('CloseInstructor');
        this._modalVisible = false;
    }
    notifyParentToCloseModal(strAction) {
        const tooglecmps = new CustomEvent('toggleinstructor', {
            detail: {
                Component: strAction
            }
        });
        this.dispatchEvent(tooglecmps);
    }
    //delete Instructor Model Box
    closeDeleteModal() {
        this.deleteModel = false;
    }

    instructorOnChange(event) {
        this.disabled = false;
        if (event.target.name == 'FirstName') {
            this.instructorDetails.FirstName = event.detail.value;
        } else if (event.target.name == 'LastName') {
            this.instructorDetails.LastName = event.detail.value;
        } else if (event.target.name == 'Gender') {
            this.instructorDetails.Gender__c = event.detail.value;
        } else if (event.target.name == 'PhoneNumber') {
            event.target.value = event.target.value.replace(/(\D+)/g, '');
            this.instructorDetails.Phone = utils.formattedPhoneNumber(event.target.value);
        } else if (event.target.name == 'EmailAddress') {
            this.instructorDetails.Email = event.target.value;
        }

    }



    //Get Object Info
    @wire(getObjectInfo, {
        objectApiName: CONTACT_OBJECT
    })
    objectInfo;

    //Get PickList Values from Object
    @wire(getPicklistValuesByRecordType, {
        objectApiName: CONTACT_OBJECT,
        recordTypeId: '$objectInfo.data.defaultRecordTypeId'
    })
    wiredPicklistValues({
        error,
        data
    }) {
        if (data) {
            //Getting Contact Gender Custom field Picklist Values
            this.genderOptions = data && data.picklistFieldValues.Gender__c.values.map(key => {
                return {
                    label: key.label,
                    value: key.value
                }
            });
        } else {
            this.dispatchEvent(utils.toastMessage("Warning in fetching Picklist Values", "warning"));
        }
    }
    // record type details
    @wire(getRecordType, {
        name: 'Instructor'
    })
    recordTypeDetails(result) {
        if (result.data != undefined && result.data.length > 0) {
            this.recordTypeId =result && result.data;
        }
    }
    //training list Details
    @wire(getTrainingDetails, {
        objInfo: {
            sobjectType: "Training__c"
        }
    })
    allTrainingData(result) {
        if (result.data != undefined && result.data.length > 0) {
            this.trainingAllData = result.data
        }
    }

    //Accurd dropdown fun
    toggleRow(event) {
        event.preventDefault();
        this.recId = event.target.dataset.id;

        let dataInstructor1 = this.trainingAllData.filter(item => {
            return item.Instructor1__c == this.recId;
        })
        let dataInstructor2 = this.trainingAllData.filter(item => {
            return item.Instructor2__c == this.recId;
        })

        var trainingCombineData = [...dataInstructor1, ...dataInstructor2]
        this.trainingCombineItem = trainingCombineData;
        this.toggleRowContinuation();
    }


    //Get List Table Values
    @wire(getAllInstructorDetails, {
        objInfo: {
            sobjectType: "Contact"
        }
    })
    allinstructorData(result) {
        this.wiredinstructorDetails = result;
        if (result.data != undefined && result.data.length > 0) {
            this.totalRecordsCount = result.data.length;

            if (this.totalRecordsCount == 0) {
                this.norecorddisplay = true;
            } else {
                this.norecorddisplay = false;
            }
            this.totalRecords = result.data.map(row => {
                return {
                    Id: row.Id,
                    FirstName: row.FirstName,
                    LastName: row.LastName,
                    Gender__c: row.Gender__c,
                    Phone: row.Phone,
                    Email: row.Email
                }
            });
            this.pageData();
            refreshApex(this.wiredinstructorDetails);
        } else {
            this.Spinner = false;
        }
    }


    //Edit function
    editModelDetails(event) {
        this.openInstructorModel();
        let selectedEditId = event.target.dataset.id;
        editInstructorDetails({
            selectedEditId
        })
            .then(result => {
                if (result.length > 0) {
                    this.contactId = result[0].Id;
                    if (this.contactId) {
                        this.title = 'Edit instructor';
                    }
                    this.instructorDetails = {
                        Id: result[0].Id,
                        FirstName: result[0].FirstName,
                        LastName: result[0].LastName,
                        Gender__c: result[0].Gender__c,
                        Phone: result[0].Phone,
                        Email: result[0].Email,
                    };


                } else {
                    this.dispatchEvent(utils.toastMessage("Warning in getting instructor details", "warning"));
                }
            }).catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
    }

    //Delete function
    deleteModelDetails(event) {
        this.deleteModel = true;
        this.recIdDelete = event.target.dataset.id;
    }
    // handel Delete function
    handleDelete() {
        // this.recIdDelete = event.target.dataset.id
        deleteRecord(this.recIdDelete)
            .then(() => {
                this.deleteModel = false;
                this.dispatchEvent(utils.toastMessage('Instructor has been deleted successfully', "success"));
                fireEvent(this.pageRef, "RefreshPublicProviderTrainingDashBoard", true);
                return refreshApex(this.wiredinstructorDetails);
            }).catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DELETE_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
        return refreshApex(this.wiredinstructorDetails);
    }

    //Create Instructor Details
    saveInstructorDetails() {
        this.disabled = true;

        const allValid = [
            ...this.template.querySelectorAll('lightning-input'),
            ...this.template.querySelectorAll('lightning-combobox')
        ]
            .reduce((validSoFar, inputFields) => {
                inputFields.reportValidity();
                return validSoFar && inputFields.checkValidity();
            }, true);
        if (allValid) {
            if (this.instructorDetails && this.instructorDetails.ContactNumber__c && this.instructorDetails.ContactNumber__c.length != 14) {
                this.disabled = false;
                return this.dispatchEvent(utils.toastMessage("Invalid Phone", "warning"));
            }

            const fields = {};
            fields[RECORDTYPEID_FIELD.fieldApiName] = this.recordTypeId;
            fields[FIRSTNAME_FIELD.fieldApiName] = this.instructorDetails.FirstName;
            fields[LASTNAME_FIELD.fieldApiName] = this.instructorDetails.LastName;
            fields[GENDER_FIELD.fieldApiName] = this.instructorDetails.Gender__c;
            fields[PHONE_FIELD.fieldApiName] = this.instructorDetails.Phone;
            fields[EMAIL_FIELD.fieldApiName] = this.instructorDetails.Email;
            if (!this.contactId) {
                const recordInput = {
                    apiName: CONTACT_OBJECT.objectApiName,
                    fields
                };
                createRecord(recordInput)
                    .then(result => {
                        this._modalVisible = false;
                        this.notifyParentToCloseModal('CloseInstructor');
                        this.instructorDetails = {};
                        this.contactId = null;
                        this.dispatchEvent(utils.toastMessage("Instructor has been saved successfully", "success"));
                        this.disabled = false;
                        fireEvent(this.pageRef, "RefreshPublicProviderTrainingDashBoard", true);
                        return refreshApex(this.wiredinstructorDetails);
                    }).catch(errors => {
                        let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                        return this.dispatchEvent(utils.handleError(errors, config));
                    });

            } else {

                fields[ID_FIELD.fieldApiName] = this.contactId;
                const recordInput = {
                    fields
                };
                updateRecord(recordInput)
                    .then(() => {
                        this._modalVisible = false;
                        this.notifyParentToCloseModal('CloseInstructor');
                        this.instructorDetails = {};
                        this.contactId = null;
                        this.disabled = false;
                        this.dispatchEvent(utils.toastMessage("Instrucor  has been Updated successfully", "success"));
                        fireEvent(this.pageRef, "RefreshPublicProviderTrainingDashBoard", true);
                        return refreshApex(this.wiredinstructorDetails);
                    }).catch(errors => {
                        let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                        return this.dispatchEvent(utils.handleError(errors, config));
                    });
            }
        } else {
            this.disabled = false;
            this.dispatchEvent(utils.toastMessage("Warning in saving instructor Details. Please check.", "warning"));
        }
    }

    toggleRowContinuation() {
        let conList = this.template.querySelectorAll('.conli');
        let btnList = this.template.querySelectorAll('.btn');

        btnList.forEach((item) => {
            item.className = "btn dropdown";
        });

        conList.forEach((item) => {
            let indvRow = item;
            let indvRowId = item.getAttribute('id');
            if (indvRowId.includes(this.recId)) {
                if (indvRow.classList.contains("slds-showd")) {
                    indvRow.classList.add("slds-hide");
                    indvRow.classList.remove("slds-showd");
                } else {
                    indvRow.classList.remove("slds-hide");
                    indvRow.classList.add("slds-showd");
                }
            } else {
                indvRow.classList.add("slds-hide");
                indvRow.classList.remove("slds-showd");
            }
        });
    }


    //Edit/Delete Model Action
    handleRowAction(event) {
        this.recId = event.target.dataset.id;
        this.toggleEditDelView();
    }
    toggleEditDelView() {
        let conList = this.template.querySelectorAll('.intlist');

        conList.forEach((item) => {
            let indvRow = item;
            let indvRowId = item.getAttribute('id');
            if (indvRowId.includes(this.recId)) {
                if (indvRow.classList.contains("slds-showd")) {
                    indvRow.classList.add("slds-hide");
                    indvRow.classList.remove("slds-showd");
                } else {
                    indvRow.classList.remove("slds-hide");
                    indvRow.classList.add("slds-showd");
                }
            } else {
                indvRow.classList.add("slds-hide");
                indvRow.classList.remove("slds-showd");
            }
        });
    }

    pageData = () => {
        let page = this.page;
        let perpage = this.perpage;
        let startIndex = (page * perpage) - perpage;
        let endIndex = (page * perpage);
        this.currentPageInstructorData = this.totalRecords.slice(startIndex, endIndex);

        if (this.currentPageInstructorData.length == 0) {
            if (this.page != 1) {
                this.page = this.page - 1;
                this.pageData();
            }
        }
        this.Spinner = false;
    }

    hanldeProgressValueChange(event) {
        this.page = event.detail;
        this.pageData();
    }


    // Topic Description Model box
    closeDescModal() {
        this.descModel = false;
    }
    descriptionModal(event) {
        if (event.target.dataset.id != undefined) {
            this.descModel = true;
            this.descriptionView = event.target.dataset.id;
        } else {
            this.descModel = false;
        }
    }


    ShowOfEditDelView() {
        let threedotClass = this.template.querySelectorAll(".intlist");
        threedotClass.forEach((i) => {
            let indvRow = i;
            indvRow.classList.add("slds-hide");
            indvRow.classList.remove("slds-showd");
        });
    }


}