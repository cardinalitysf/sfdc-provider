/**
 * @Author        : S. Jayachandran
 * @CreatedOn     : March 16, 2020
 * @Purpose       : ProviderCorporateInfo fetching and edit in Account object.
 * @updatedBy     : 
 * @updatedOn     : 
 **/
import {
  LightningElement,
  track,
  wire
} from "lwc";
import getAccountCorporateInfo from "@salesforce/apex/ProviderCorporateInfo.getAccountCorporateInfo";
import getAccrediationList from "@salesforce/apex/ProviderCorporateInfo.getAccrediationList";
import fatchProfitValue from "@salesforce/apex/ProviderCorporateInfo.getProfitValues";
import getProviderId from '@salesforce/apex/providerApplicationHeader.getProviderId';
import {
  utils
} from "c/utils";
import { CJAMS_CONSTANTS } from "c/constants";
import InsertUpdateCorporateInfo from "@salesforce/apex/ProviderCorporateInfo.InsertUpdateCorporateInfo";
import * as sharedData from "c/sharedData";
import getUserEmail from '@salesforce/apex/providerApplication.getUserEmail';
import {
  NavigationMixin
} from "lightning/navigation";
export default class ProviderCorporateInfo extends NavigationMixin(
  LightningElement
) {
  @track getREC1 = {
    TaxId__c: "",
    Name: "",
    Accrediation__c: "",
    Profit__c: "",
    LastName__c: "",
    FirstName__c: "",
    Website: "",
    DBAName__c: ""
  };
  @track Profit__c;
  @track providerid;

  // get providerid() {
  //   console.log(sharedData.getProviderId(),'--corporate')
  //   return sharedData.getProviderId();
  // }


  connectedCallback() {
    getUserEmail()
    .then(res=>{
        getProviderId({Id: res[0].Id})
        .then(res=> {
            this.providerid = res[0].Id;
            sharedData.setProviderId(this.providerid);
            this.InfoLoad();
        }).catch(errors => {
            return this.dispatchEvent(utils.handleError(errors));
        });
    })
    .catch(errors=>{
        return this.dispatchEvent(utils.handleError(errors));
    })
  }
  InfoLoad() {
    if (!this.providerid) {
      this[NavigationMixin.Navigate]({
        type: "standard__navItemPage",
        attributes: {
          apiName: "Dashboard"
        }
      });
    } else {
      getAccountCorporateInfo({
        providerData: this.providerid
      })
        .then(result => {
          this.getREC1.TaxId__c = result[0].TaxId__c;
          this.getREC1.Profit__c = result[0].Profit__c;
          this.getREC1.FirstName__c = result[0].FirstName__c;
          this.getREC1.LastName__c = result[0].LastName__c;
          this.getREC1.Name = result[0].Name;
          this.getREC1.Accrediation__c = result[0].Accrediation__c;
          this.getREC1.Website = result[0].Website;
          this.getREC1.DBAName__c = result[0].DBAName__c;
        })
        .catch(errors => {
          let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
          return this.dispatchEvent(utils.handleError(errors, config));
        });
    }
  }

  @wire(getAccrediationList, {
    objInfo: {
      sobjectType: "ReferenceValue__c"
    },
    pickListFieldApi: "RefValue__c"
  })
  wiredAccreDetails1({
    error,
    data
  }) {
    if (data) {
      let accre = data.map(item => {
        let a = Object.assign({}, item);
        a.label = item.RefValue__c;
        a.value = item.RefValue__c;
        return a;
      });
      this.AccreditationOptions = accre;
    } else if (error) {

    }
  }

  @wire(fatchProfitValue, {
    objInfo: {
      sobjectType: "Account"
    },
    pickListFieldApi: "Profit__c"
  })
  wiredProfitDetails({
    error,
    data
  }) {
    if (data) {
      this.ProfitNonProfitOptions = data;
    } else if (error) {

    }
  }

  //Values change func
  handleChange(event) {
    if (event.target.name == "TaxId__c") {
      event.target.value = event.target.value.replace(/(\D+)/g, "");
      this.getREC1.TaxId__c = utils.formattedTaxID(event.target.value);
    } else if (event.target.name == "Name") {
      this.getREC1.Name = event.target.value;
    } else if (event.target.name == "Profit__c") {
      this.getREC1.Profit__c = event.target.value;
    } else if (event.target.name == "FirstName__c") {
      this.getREC1.FirstName__c = event.target.value;
    } else if (event.target.name == "LastName__c") {
      this.getREC1.LastName__c = event.target.value;
    } else if (event.target.name == "Website") {
      this.getREC1.Website = event.target.value;
    } else if (event.target.name == "DBAName__c") {
      this.getREC1.DBAName__c = event.target.value;
    } else if (event.target.name == "Accrediation__c") {
      this.getREC1.Accrediation__c = event.target.value;
    }
  }

  //save method for INFO
  handleSave() {
    if (this.getREC1 && !this.getREC1.Name)
      return this.dispatchEvent(utils.toastMessage("Provider Corporate Name is mandatory", "warning"));

    if (this.getREC1 && !this.getREC1.FirstName__c)
      return this.dispatchEvent(utils.toastMessage("First Name is mandatory", "warning"));

    if (this.getREC1 && !this.getREC1.LastName__c)
      return this.dispatchEvent(utils.toastMessage("Last Name is mandatory", "warning"));

    if (this.getREC1 && !this.getREC1.TaxId__c)
      return this.dispatchEvent(utils.toastMessage("TaxId is mandatory", "warning"));

    if (this.getREC1 && !this.getREC1.Accrediation__c)
      return this.dispatchEvent(utils.toastMessage("Accrediation is mandatory", "warning"));

    this.getREC1.sobjectType = "Account";
    this.getREC1.Id = this.providerid;

    InsertUpdateCorporateInfo({
      objSobjecttoUpdateOrInsert: this.getREC1
    })
      .then(result => {
        this.dispatchEvent(
          utils.toastMessage(
            "Profile Information has been saved successfully",
            "success"
          )
        );
        this.InfoLoad();
      })
      .catch(error => {
        this.dispatchEvent(
          utils.toastMessage("Error in creating record", "error")
        );
      });
  }
}