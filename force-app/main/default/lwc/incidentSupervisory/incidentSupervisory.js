/**
 * @Author        : G sathishkumar ,
 * @CreatedOn     : Aug 03,2020
 * @Purpose       : IncidentSupervisory.
 **/
import { LightningElement, wire, track } from 'lwc';

import getQuestionsByType from '@salesforce/apex/IncidentSupervisory.getQuestionsByType';
import getRecordType from '@salesforce/apex/IncidentSupervisory.getRecordType';
import bulkAddRecordAns from '@salesforce/apex/IncidentSupervisory.bulkAddRecordAns';
import CaseFromProRecQues from "@salesforce/schema/ProviderRecordQuestion__c.Incident__c";
import RecordTypeIdFromProRecQues from "@salesforce/schema/ProviderRecordQuestion__c.RecordTypeId";
import PROVIDER_RECORD_QUESTION_API from "@salesforce/schema/ProviderRecordQuestion__c";
import getRecordQuestionId from '@salesforce/apex/IncidentSupervisory.getRecordQuestionId';
import getProviderAnswer from '@salesforce/apex/IncidentSupervisory.getProviderAnswer';
import { createRecord } from "lightning/uiRecordApi";

import getSupervisoryData from '@salesforce/apex/IncidentSupervisory.getSupervisoryData';
import insertUpdateSupervisorDetails from '@salesforce/apex/IncidentSupervisory.insertUpdateSupervisorDetails';
import getStaffMembers from '@salesforce/apex/IncidentSupervisory.getStaffMembers';


import { utils } from "c/utils";
import * as sharedData from "c/sharedData";
import images from '@salesforce/resourceUrl/images';
import { CJAMS_CONSTANTS, USER_PROFILE_NAME } from "c/constants";

export default class IncidentSupervisory extends LightningElement {

    supervisoryIcon = images + '/supervisoryIcon.svg';
    @track questions = [];
    supervisorComments;
    StaffInvolvedOptions;
    supervisorData;
    supervisorDate;

    type = 'Incident Checklist';
    RecordTypeID;
    getProviderQuestionIdfromBegining;
    allIncidentSupervisorytAnswers;
    answerUpdate = false;
    providerQuestionId;
    isUpdateFlag = false;
    supervisoryDetails = {};
    supersisorDetailsID;
    SuperVisorFlowBtnDisable=false;
    disabledBtnSave=false;


    get options() {
        return [{
            label: 'Yes',
            value: 'Yes'
        },
        {
            label: 'No',
            value: 'No'
        },
        ];
    }
    get applicationId() {
        return sharedData.getApplicationId();
        //return 'a0A0w000000etSIEAY';
    }

    get caseId() {
        return sharedData.getCaseId();
         // return '5000w000001faMsAAI';
    }
    get userProfileName() {
        return sharedData.getUserProfileName();
        // return 'Caseworker';
      }
      get incidentStatus() {
       return sharedData.getIncidentStatus();
    }

    supervisorChange(event) {

        if (event.target.name == 'SupervisorComments__c') {
            this.supervisorComments = event.target.value;
        } else if (event.target.name == 'Supervisor') {
            this.supervisorData = event.target.value;
        } else if (event.target.name == 'SupervisorDate__c') {
            this.supervisorDate = event.target.value;
        }
    }

    supervisorCheckListChange(event) {
        if (event.target != undefined && event.target.name != undefined && event.target.name != null) {
            this.questions[event.target.name].Dependent__c = event.target.value;
        }
    }

    getStaffMembersList() {
        getStaffMembers({
            applicationId: this.applicationId
        }).then((result) => {
            try {
                if (result.length > 0) {
                    let staff = result.map(item => {
                       let staffData = Object.assign({}, item);
                        staffData.label = (item.Staff__r !=undefined) ? (item.Staff__r.LastName !=undefined) ? item.Staff__r.LastName:'':'';
                        staffData.value =(item.Staff__r !=undefined) ? (item.Staff__r.Id !=undefined) ? item.Staff__r.Id:'':'';
                        return staffData;
                    });
                    this.StaffInvolvedOptions = staff;
                }
            } catch (error) {
                throw error;
            }
        }).catch((errors) => {
            let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
        });
    }

    @wire(getQuestionsByType)

    wiredquestions(data) {
        if (data.data != null && data.data != '') {
            let currentData = [];
            let id = 0;
            this.questions = [];
            data.data.forEach((row) => {
                let rowData = {};
                rowData.rowId = id;
                rowData.Activity__c = row.Activity__c;
                rowData.QuestionOrder__c = row.QuestionOrder__c;

                currentData.push(rowData);
                id++;
            });
            this.questions = currentData;
        }
    }


    @wire(getRecordType, {
        name: '$type'
    })
    recordTypeDetails(result) {
         if (result.data != undefined && result.data.length > 0) {
            this.RecordTypeID =result && result.data;
             this.getProviderQuestionID();
     }
    }

    saveSupervisorSubmit() {
        if (!this.supervisorDate) {
            this.dispatchEvent(
                utils.toastMessage("Please enter a Date", "warning")
            );
        } else {
            this.supervisoryDetails.sobjectType = "Case";
            this.supervisoryDetails.Id = this.caseId;
            this.supervisoryDetails.SupervisorComments__c = this.supervisorComments;
            this.supervisoryDetails.IncidentSupervisor__c = this.supervisorData;
            this.supervisoryDetails.SupervisorDate__c = this.supervisorDate;
            //this.supervisoryDetails.Id = this.supersisorDetailsID;

            if (this.supersisorDetailsID) this.isUpdateFlag = true;
            else this.isUpdateFlag = false;

            insertUpdateSupervisorDetails({ saveObjInsert: this.supervisoryDetails }).then((result) => {

                if (this.isUpdateFlag)

                    this.dispatchEvent(utils.toastMessage("Supervisory details updated successfully", "success"));

                else
                    this.dispatchEvent(utils.toastMessage("Supervisory details saved successfully", "success"));

            }).catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });

            if (this.answerUpdate) {
                this.answerRecordSave();
            } else {
                this.questionRecordSave();
            }

        }

    }


    connectedCallback() {
        if (this.userProfileName === USER_PROFILE_NAME.USER_CASE_WRKR || (this.incidentStatus !== undefined && (this.incidentStatus !== 'Pending'))) {
            this.SuperVisorFlowBtnDisable = true;
            this.disabledBtnSave = true;
        }

        this.getStaffMembersList();
        this.getSupervisoryDetailsLoad();
    }

    getSupervisoryDetailsLoad() {
        getSupervisoryData({ CaseID: this.caseId })
            .then((result) => {
                if (result.length > 0) {
                    this.supervisorComments =(result[0].SupervisorComments__c !=undefined) ? result[0].SupervisorComments__c:'';
                    this.supervisorDate = (result[0].SupervisorDate__c !=undefined) ? result[0].SupervisorDate__c:'';
                    this.supersisorDetailsID = (result[0].Id !=undefined) ? result[0].Id:'' ;
                    this.supervisorData =(result[0].IncidentSupervisor__r !=undefined) ? (result[0].IncidentSupervisor__r.Id !=undefined) ? result[0].IncidentSupervisor__r.Id: '':'';
                }
            })
            .catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
    }


    questionRecordSave() {
        let fields = {};
        fields[CaseFromProRecQues.fieldApiName] = this.caseId;
        fields[RecordTypeIdFromProRecQues.fieldApiName] = this.RecordTypeID;
        const recordInput = {
            apiName: PROVIDER_RECORD_QUESTION_API.objectApiName,
            fields
        };
        createRecord(recordInput)
            .then((response) => {
                if (response) {
                    this.providerQuestionId = response.id;
                    this.answerRecordSave();
                }
            })
            .catch(errors => {
                  let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
    }


    answerRecordSave() {
        var questionanswerarray = [];
        this.questions.forEach((item) => {
            questionanswerarray.push(
                {
                    'id': item.AnswerId,
                    'Comar__c': item.Id,
                    'ProviderRecordQuestion__c': this.providerQuestionId,
                    'Dependent__c': item.Dependent__c,
                }
            );
        });

        let questionstosave = JSON.stringify(questionanswerarray);

        bulkAddRecordAns({
            datas: questionstosave
        })
            .then(result => {
                this.getProviderQuestionID();
               // this.dispatchEvent(utils.toastMessage("Records Has Been Updated Successfully!..", "success"));
            }).catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
    }




    getProviderQuestionID() {
        getRecordQuestionId({ caseId: this.caseId, recordTypeId: this.RecordTypeID }).then((result) => {
            var arrayRecordQuestion = [];
            result.forEach((item) => {
                arrayRecordQuestion.push(item.Id);
            });
            this.getProviderQuestionIdfromBegining = arrayRecordQuestion.toString();
            this.getProviderAnswerDetails();
        }).catch(errors => {
            let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
            return this.dispatchEvent(utils.handleError(errors, config));
        });
    }



    getProviderAnswerDetails() {
        getProviderAnswer({ getAnswers: this.getProviderQuestionIdfromBegining }).then((result) => {
            this.allIncidentSupervisorytAnswers = result;
            if (this.allIncidentSupervisorytAnswers.length > 0) {
                this.allIncidentSupervisorytAnswers.forEach((item, index) => {
                    this.questions[index].AnsDependent = (item.Dependent__c != undefined) ? item.Dependent__c:'';

                    this.questions[index].AnswerId = (item.Id != undefined) ? item.Id:'';
                    this.questions[index].Comar__c = (item.Comar__c != undefined) ? item.Comar__c:'';
                    this.questions[index].Dependent__c = (item.Dependent__c != undefined) ? item.Dependent__c:'';
                    this.providerQuestionId = (item.ProviderRecordQuestion__c != undefined) ? item.ProviderRecordQuestion__c:'';
                    this.answerUpdate = true;

                });
            } else {
                this.answerUpdate = false;
                this.questions.forEach((item) => {
                    item.AnsDependent = '';
                });
            }
        }).catch(errors => {  
            let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
            return this.dispatchEvent(utils.handleError(errors, config));
        });
    }



}