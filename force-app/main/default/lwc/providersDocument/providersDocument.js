/**
 * @Author        : BalaMurugan
 * @CreatedOn     : Apr 20,2020
 * @Purpose       : Attachments based on Providers
 **/

    import {
        LightningElement,
        track
    
    } from 'lwc';
    import * as referralsharedDatas from "c/sharedData";
    import {
        CJAMS_CONSTANTS
    } from 'c/constants';
    
    
    export default class ProvidersDocument extends LightningElement {
        get providerId() {
            return referralsharedDatas.getProviderId();
        }
        get sobjectNameToFetch() {
            return CJAMS_CONSTANTS.PROVIDER_OBJECT_NAME;
        }
    
    }