/**
 * @Author        : Preethi Bojarajan
 * @CreatedOn     : March 20 ,2020
 * @Purpose       : Contains all Constant Variables
 * @updatedBy     : Preethi Bojarajan
 * @updatedOn     : Apr 13, 2020
 **/

export const constPopupVariables = {
    SUCCESS: 'SUCCESS',
    ERROR: 'ERROR',
    INSERTSUCCESS: 'SUCCESSFULLY INSERTED',
    UPDATESUCCESS: 'SUCCESSFULLY UPDATED',
    DELETESUCCESS: 'SUCCESSFULLY DELETED',
    APPROVED: 'APPROVED',
    officeTab : 'Office Inspection Added Successfully',
    plantTab : 'Physical Plant Site Added Successfully',
    staffTab : 'Staff Record Added Successfully',
    officeTabUpdate : 'Office Inspection Updated Successfully',
    plantTabUpdate : 'Physical Plant Site Updated Successfully',
    staffTabUpdate : 'Staff Record Updated Successfully',
    savingErrorRec : 'Error In Saving Record, Please Try Again!..',
    comarSave : 'Comar Has Been Saved Successfully!..',
}

export const constFunctionVariables = {

}