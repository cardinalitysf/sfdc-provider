/**
 * @Author        : K.Sundar
 * @CreatedOn     : APRIL 18, 2020
 * @Purpose       : This component contains IRC RATES for Supervisor verified PROVIDER LICENSE APPLICATION
 **/

import {
    LightningElement,
    track,
    wire
} from 'lwc';
import getAllIRCRateDetails from '@salesforce/apex/ProvidersIRC.getAllIRCRateDetails';
import editIRCRateDetails from '@salesforce/apex/ProvidersIRC.editIRCRateDetails';
import getProviderLicenseContractStatus from '@salesforce/apex/ProvidersIRC.getProviderLicenseContractStatus';
import getAvailableLicenseTypesForProvider from '@salesforce/apex/ProvidersIRC.getAvailableLicenseTypesForProvider';
import ircRateHistoryForId from '@salesforce/apex/ProvidersIRC.ircRateHistoryForId';
import getIrcRateCode from '@salesforce/apex/ProvidersIRC.getIrcRateCode';

import {
    getPicklistValuesByRecordType,
    getObjectInfo
} from 'lightning/uiObjectInfoApi';
import {
    createRecord,
    updateRecord
} from 'lightning/uiRecordApi';

import IRCRATES_OBJECT from '@salesforce/schema/IRCRates__c';
import ID_FIELD from '@salesforce/schema/IRCRates__c.Id';
import RATETYPE_FIELD from '@salesforce/schema/IRCRates__c.RateType__c';
import LICENSETYPE_FIELD from '@salesforce/schema/IRCRates__c.LicenseType__c';
import IRCBEDCAPACITY_FIELD from '@salesforce/schema/IRCRates__c.IRCBedCapacity__c';
import STARTDATE_FIELD from '@salesforce/schema/IRCRates__c.StartDate__c';
import ENDDATE_FIELD from '@salesforce/schema/IRCRates__c.EndDate__c';
import PERDIEMRATE_FIELD from '@salesforce/schema/IRCRates__c.PerDiemRate__c';
import MONTHLY_FIELD from '@salesforce/schema/IRCRates__c.Monthly__c';
import ANNUAL_FIELD from '@salesforce/schema/IRCRates__c.Annual__c';
import RATECODE_FIELD from '@salesforce/schema/IRCRates__c.RateCode__c';
import STATUS_FIELD from '@salesforce/schema/IRCRates__c.Status__c';
import PROVIDER_FIELD from '@salesforce/schema/IRCRates__c.Provider__c';

import myResource from '@salesforce/resourceUrl/styleSheet';
import images from '@salesforce/resourceUrl/images';
import * as sharedData from 'c/sharedData';
import {
    refreshApex
} from '@salesforce/apex';
import {
    utils
} from 'c/utils';
import {
    CJAMS_CONSTANTS, TOAST_HEADER_LABELS
} from 'c/constants';

//List Screen Data Table Header Action Columns Declaration
const actions = [{
    label: 'Edit',
    name: 'Edit',
    iconName: 'utility:edit',
    target: '_self'
}, {
    label: 'History',
    name: 'History',
    iconName: 'standard:channel_program_history',
    target: '_self'
}, {
    label: 'View',
    name: 'View',
    iconName: 'utility:preview',
    target: '_self'
}];

//List Screen Data Table Header Columns Declaration
const columns = [{
    label: 'PROVIDER RATE ID',
    fieldName: 'Name',
    type: 'text',
    sortable: true
}, {
    label: 'PROGRAM',
    fieldName: 'ProgramName__c',
    type: 'text',
    sortable: true
}, {
    label: 'License TYPE',
    type: 'button',
    sortable: true,
    initialWidth: 150,
    typeAttributes: {
        name: 'dontRedirect',
        label: {
            fieldName: 'LicenseTypeFullName'
        }
    },
    cellAttributes: {
        class: "title"
    }
}, {
    label: '$ PER DIEM RATE',
    fieldName: 'PerDiemRate__c',
    type: 'number'
}, {
    label: 'START DATE',
    fieldName: 'StartDate__c',
    type: 'text',
    sortable: true
}, {
    label: 'END DATE',
    fieldName: 'EndDate__c',
    type: 'text',
    sortable: true
}, {
    label: 'IRC BED CAPACITY',
    fieldName: 'IRCBedCapacity__c',
    type: 'number',
    sortable: true
}, {
    label: 'RATE CODE',
    fieldName: 'RateCode__c',
    type: 'text'
}, {
    label: 'STATUS',
    fieldName: 'Status__c',
    type: 'text',
    cellAttributes: {
        class: 'Active'
    }
}, {
    type: 'action',
    typeAttributes: {
        rowActions: actions
    },
    cellAttributes: {
        iconName: 'utility:threedots_vertical',
        iconAlternativeText: '',
        class: "tripledots"
    },
}];

//History Model Data Table Columns Declaration
const ircRateHistoryColumns = [{
    label: 'PROVIDER RATE ID',
    fieldName: 'Name',
    type: 'text',
    initialWidth: 140
}, {
    label: 'LICENSE TYPE',
    fieldName: 'LicenseTypeFullName',
    type: 'text',
    initialWidth: 170
}, {
    label: '$ PER DIEM RATE',
    fieldName: 'PerDiemRate__c',
    type: 'number'
}, {
    label: 'START DATE',
    fieldName: 'StartDate__c',
    type: 'text'
}, {
    label: 'END DATE',
    fieldName: 'EndDate__c',
    type: 'text'
}, {
    label: 'IRC BED CAPACITY',
    fieldName: 'IRCBedCapacity__c',
    type: 'number',
    initialWidth: 150
}, {
    label: 'RATE CODE',
    fieldName: 'RateCode__c',
    type: 'text'
}, {
    label: 'STATUS',
    fieldName: 'Status__c',
    type: 'text',
    cellAttributes: {
        class: 'Inactive'
    }
}];

//Class Name declaration
export default class ProvidersIRC extends LightningElement {
    //Variable Initializations
    @track columns = columns;
    @track ircRateDetails = {};
    @track noRecordsFound = false;

    //Initialization to Data Table Sort Function
    @track defaultSortDirection = 'asc';
    @track sortDirection = 'asc';
    @track sortedBy = "Id";

    //Initialization to Pagination
    @track totalIRCRatesCount = 0;
    @track totalIRCRates = [];
    @track page = 1;
    @track currentPageIRCRateData;
    setPagination = 5;
    perpage = 10;

    //Flags Initialization to Freeze fields
    @track createBtnDisabled = false;
    @track licenseTypeDisabled = false;

    //Pop Up Model Flags Initialization
    @track openModel = false;
    @track historyModel = false;
    @track viewModel = false;

    //Initialization to get Provider Application Data
    providerforApiCall = this.providerId;
    wiredIRCRateDetails;
    ircRateId;
    attachmentIcon = images + '/document-newIcon.svg';

    //History Table Initialization
    @track ircRateHistoryColumns = ircRateHistoryColumns;
    @track noHistoryRecordsFound = false;
    @track ircRateHistoryData = [];

    //Picklist value Initialization
    @track licenseTypeOptions = [];
    @track createOrEditLicenseTypeOptions = [];
    @track rateTypeOptions = [];
    @track totalLicensesArray = [];

    //Previous IRC Rate Details Initialization
    @track previousIRCRateDetails = {};
    @track previousIRCRateId;
    @track Spinner = true;
    @track ircRateCode;
    @track editModelCreateRecordFlag = false;

    @track minstartdate = '1970-01-01';
    @track minenddate = '1970-01-01';

    @track title;
    finalStatus;

    wiredIRCRateHistoryDetails;



    //Get Provider ID
    get providerId() {
        return sharedData.getProviderId();
    }

    //Get IRC Rate Code finally added
    get IRCRateCode() {
        return this.ircRateCode;
    }

    //Getter Array method for License Options
    get licenseoptions() {
        if (this.totalLicensesArray.length > 0) {
            let localWiredData = this.wiredIRCRateDetails.data;

            this.licenseTypeOptions = this.totalLicensesArray.filter(cv => {
                return !localWiredData.find(e => {
                    return e.LicenseType__c == cv.value;
                });
            });
        }

        return this.licenseTypeOptions;
    }

    //Sundar Adding this line
    get isSuperUserFlow() {
        let userProfile = sharedData.getUserProfileName();

        if (userProfile === 'Supervisor')
            return true;
        else
            return false;
    }

    //Connected Call Back
    connectedCallback() {
        this.isSuperUserFlow;
        this.initialLoad();
    }

    //Initial On Load Method to getting IRC Rate Code
    initialLoad() {
        getIrcRateCode()
            .then(result => {
                if (result.length == 0) {
                    this.ircRateCode = 'R-0001';
                } else {
                    this.rateCodeGeneration(result[0].RateCode__c);
                }
            })
    }

    //Get Data Table Values for Active IRC Rates to the Provider
    @wire(getAllIRCRateDetails, {
        providerId: '$providerforApiCall'
    })
    allIRCRateData(result) {
        this.wiredIRCRateDetails = result;

        if (result.data) {
            this.totalIRCRatesCount = result.data.length;

            if (this.totalIRCRatesCount == 0)
                this.noRecordsFound = true;
            else
                this.noRecordsFound = false;

            this.licenseTypeOptions = [...this.licenseoptions];

            this.totalIRCRates = result.data.map(row => {
                return {
                    Id: row.Id,
                    Name: row.Name,
                    LicenseType__c: row.LicenseType__c,
                    ProgramName__c: row.LicenseType__r.ProgramName__c,
                    LicenseTypeFullName: row.LicenseType__r.Name + ' - ' + row.LicenseType__r.ProgramType__c,
                    IRCBedCapacity__c: row.IRCBedCapacity__c,
                    StartDate__c: utils.formatDate(row.StartDate__c),
                    EndDate__c: utils.formatDate(row.EndDate__c),
                    PerDiemRate__c: row.PerDiemRate__c,
                    RateCode__c: row.RateCode__c,
                    Status__c: row.Status__c
                }
            });

            this.pageData();
            refreshApex(this.wiredIRCRateDetails);
            this.Spinner = false;
        } else {
            this.noRecordsFound = true;
        }
    }

    //Pagination
    pageData() {
        let page = this.page;
        let perpage = this.perpage;
        let startIndex = (page * perpage) - perpage;
        let endIndex = (page * perpage);
        this.currentPageIRCRateData = this.totalIRCRates.slice(startIndex, endIndex);

        if (this.currentPageIRCRateData.length == 0) {
            if (this.page != 1) {
                this.page = this.page - 1;
                this.pageData();
            }
        }
    }

    // Sorting the Data Table Column Function End
    sortBy(field, reverse, primer) {
        const key = primer ?
            function (x) {
                return primer(x[field]);
            } :
            function (x) {
                return x[field];
            };

        return function (a, b) {
            a = key(a);
            b = key(b);
            if (a === undefined) a = '';
            if (b === undefined) b = '';
            a = typeof (a) === 'number' ? a : a.toLowerCase();
            b = typeof (b) === 'number' ? b : b.toLowerCase();

            return reverse * ((a > b) - (b > a));
        };
    }

    //Data Table Sorting Function On Click of Arrow Icon
    onHandleSort(event) {
        const {
            fieldName: sortedBy,
            sortDirection: sortDirection
        } = event.detail;
        const cloneData = [...this.currentPageIRCRateData];

        cloneData.sort(this.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1));

        this.currentPageIRCRateData = cloneData;
        this.sortDirection = sortDirection;
        this.sortedBy = sortedBy;
    }

    //Page Change Action in Pagination Bar
    hanldeProgressValueChange(event) {
        this.page = event.detail;
        this.pageData();
    }

    //Get Available License Type of Picklist Values
    @wire(getAvailableLicenseTypesForProvider, {
        providerId: '$providerforApiCall'
    })
    availableLicenseTypes({
        error,
        data
    }) {
        if (data) {
            if (data.length == 0)
                return this.dispatchEvent(utils.toastMessage("No Licenses found for provider", TOAST_HEADER_LABELS.TOAST_WARNING));

            this.licenseTypeOptions = data.map(row => {
                return {
                    label: row.Name + ' - ' + row.ProgramType__c,
                    value: row.Id
                }
            });

            this.totalLicensesArray = [...this.licenseoptions];
        } else if (error) {
            this.dispatchEvent(utils.toastMessage("Error in fetching License Picklist Values", TOAST_HEADER_LABELS.TOAST_WARNING));
        }
    }

    //Get Object Info
    @wire(getObjectInfo, {
        objectApiName: IRCRATES_OBJECT
    })
    objectInfo;

    //Get PickList Values from IRC Rate Object
    @wire(getPicklistValuesByRecordType, {
        objectApiName: IRCRATES_OBJECT,
        recordTypeId: '$objectInfo.data.defaultRecordTypeId'
    })
    wiredPicklistValues({
        error,
        data
    }) {
        if (data) {

            //Getting Rate Type Custom field Picklist Values
            this.rateTypeOptions = data.picklistFieldValues.RateType__c.values.map(key => {
                return {
                    label: key.label,
                    value: key.value
                }
            });
        } else if (error) {
            this.dispatchEvent(utils.toastMessage("Error in fetching Picklist Values", TOAST_HEADER_LABELS.TOAST_WARNING));
        }
    }

    //Create New IRC Rate for Provider
    openIRCRateModel() {
        if (this.licenseTypeOptions.length == 0)
            return this.dispatchEvent(utils.toastMessage("IRC Rates can be created only for Providers Active Licenses", TOAST_HEADER_LABELS.TOAST_WARNING));

        if (!this.ircRateId) {
            this.title = 'ADD IRC RATES';

            let currentMonth = ((new Date().getMonth()) + 1) < 10 ? '0' + ((new Date().getMonth()) + 1) : ((new Date().getMonth()) + 1);
            let currentDate = new Date().getDate() < 10 ? '0' + new Date().getDate() : new Date().getDate();
            let nextYear = new Date().setFullYear(new Date().getFullYear() + 1);

            this.ircRateDetails = {
                RateType__c: 'Original',
                StartDate__c: new Date().getFullYear() + '-' + currentMonth + '-' + currentDate,
                EndDate__c: new Date(nextYear).getFullYear() + '-' + currentMonth + '-' + currentDate,
                RateCode__c: this.IRCRateCode
            };

            this.ircRateId = null;
            this.minenddate = this.ircRateDetails.StartDate__c;
            this.editModelCreateRecordFlag = false;
            this.createOrEditLicenseTypeOptions = [...this.licenseTypeOptions];
        }

        this.openModel = true;
    }

    //On Click Event to see Edit/View/History Model
    handleRowAction(event) {
        //Edit/View model actions for displaying Active IRC Rate of Provider
        if (event.detail.action.name == 'Edit' || event.detail.action.name == 'View') {

            this.createOrEditLicenseTypeOptions = [...this.totalLicensesArray];

            let selectedIRCRateId = event.detail.row.Id;
            let selectedIRCRateLicenseId = event.detail.row.LicenseType__c;

            if (event.detail.action.name == 'Edit') {
                getProviderLicenseContractStatus({
                        selectedIRCRateLicenseId
                    })
                    .then(result => {
                        if (result.length == 0) {
                            this.title = 'IRC RATES';
                            this.openModel = true;
                            this.createBtnDisabled = true;
                            this.licenseTypeDisabled = true;
                            this.viewModel = false;
                            this.editModelCreateRecordFlag = true;

                            return this.editIRCRateScreen(selectedIRCRateId);
                        }

                        this.finalStatus = (result[0].ContractStatus__c == CJAMS_CONSTANTS.CONTRACT_STATUS_ACTIVE ? CJAMS_CONSTANTS.CONTRACT_STATUS_ACTIVE : (result[0].ContractStatus__c == CJAMS_CONSTANTS.CONTRACT_STATUS_EXPIRED ? CJAMS_CONSTANTS.CONTRACT_STATUS_EXPIRED : (result[0].ContractStatus__c == CJAMS_CONSTANTS.CONTRACT_STATUS_SUBMITTED ? CJAMS_CONSTANTS.CONTRACT_STATUS_SUBMITTED : (result[0].ContractStatus__c == CJAMS_CONSTANTS.CONTRACT_STATUS_INACTIVE ? CJAMS_CONSTANTS.CONTRACT_STATUS_INACTIVE : result[0].ContractStatus__c))));

                        if (result.length > 0 && (result[0].ContractStatus__c == CJAMS_CONSTANTS.CONTRACT_STATUS_SUBMITTED || result[0].ContractStatus__c == CJAMS_CONSTANTS.CONTRACT_STATUS_ACTIVE || result[0].ContractStatus__c == CJAMS_CONSTANTS.CONTRACT_STATUS_INACTIVE || result[0].ContractStatus__c == CJAMS_CONSTANTS.CONTRACT_STATUS_EXPIRED))
                            return this.dispatchEvent(utils.toastMessage(`Cannot edit IRC Rates for ${this.finalStatus} Contract provider application`, TOAST_HEADER_LABELS.TOAST_WARNING));

                        this.title = 'IRC RATES';
                        this.openModel = true;
                        this.createBtnDisabled = true;
                        this.licenseTypeDisabled = true;
                        this.viewModel = false;
                        this.editModelCreateRecordFlag = true;

                        return this.editIRCRateScreen(selectedIRCRateId);
                    })
            } else {
                this.openModel = false;
                this.createBtnDisabled = false;
                this.licenseTypeDisabled = false;
                this.viewModel = true;
                this.editModelCreateRecordFlag = false;

                return this.editIRCRateScreen(selectedIRCRateId);
            }
        } else if (event.detail.action.name == 'History') {
            this.historyModel = true;
            this.licenseTypeDisabled = false;
            this.createBtnDisabled = false;
            this.editModelCreateRecordFlag = false;
            this.createOrEditLicenseTypeOptions = [...this.licenseTypeOptions];

            let selectedIRCRateLicenseType = event.detail.row.LicenseType__c;

            return this.getAllIRCRateHistoryData(selectedIRCRateLicenseType);
        }
    }

    editIRCRateScreen(selectedIRCRateId) {
        editIRCRateDetails({
                selectedIRCRateId
            })
            .then(result => {
                if (result.length > 0) {
                    this.ircRateDetails = {
                        RateType__c: result[0].RateType__c,
                        LicenseType__c: result[0].LicenseType__c,
                        IRCBedCapacity__c: result[0].IRCBedCapacity__c,
                        StartDate__c: result[0].StartDate__c,
                        EndDate__c: result[0].EndDate__c,
                        PerDiemRate__c: result[0].PerDiemRate__c,
                        Monthly__c: Number(result[0].Monthly__c.toFixed(2)),
                        Annual__c: Number(result[0].Annual__c.toFixed(2)),
                        RateCode__c: result[0].RateCode__c
                    };
                    this.ircRateId = result[0].Id;
                    this.ircRateCode = this.ircRateDetails.RateCode__c;
                    this.initialLoad();

                    //Store value for Previous Rate Capturing
                    if (this.editModelCreateRecordFlag) {
                        this.previousIRCRateDetails = {
                            RateType__c: result[0].RateType__c,
                            LicenseType__c: result[0].LicenseType__c,
                            IRCBedCapacity__c: result[0].IRCBedCapacity__c,
                            StartDate__c: result[0].StartDate__c,
                            EndDate__c: result[0].EndDate__c,
                            PerDiemRate__c: result[0].PerDiemRate__c,
                            Monthly__c: Number(result[0].Monthly__c.toFixed(2)),
                            Annual__c: Number(result[0].Annual__c.toFixed(2)),
                            RateCode__c: result[0].RateCode__c
                        };
                        this.previousIRCRateId = result[0].Id;
                    } else {
                        this.previousIRCRateDetails = {};
                        this.previousIRCRateId = null;
                    }
                }
            })
    }

    //Get Selected Row of License Expired IRC Rates to this Provider 
    getAllIRCRateHistoryData(selectedIRCRateLicenseType) {
        ircRateHistoryForId({
                selectedIRCRateLicenseType
            })
            .then(result => {
                this.wiredIRCRateHistoryDetails = result;

                if (result.length == 0)
                    return this.noHistoryRecordsFound = true;

                this.ircRateHistoryData = result.map(row => {
                    return {
                        Id: row.Id,
                        Name: row.Name,
                        LicenseType__c: row.LicenseType__c,
                        LicenseTypeFullName: row.LicenseType__r.Name + " - " + row.LicenseType__r.ProgramType__c,
                        PerDiemRate__c: row.PerDiemRate__c,
                        StartDate__c: utils.formatDate(row.StartDate__c),
                        EndDate__c: utils.formatDate(row.EndDate__c),
                        IRCBedCapacity__c: row.IRCBedCapacity__c,
                        RateCode__c: row.RateCode__c,
                        Status__c: row.Status__c
                    }
                });

                refreshApex(this.wiredIRCRateHistoryDetails);
            })
            .catch(error => {
                this.dispatchEvent(utils.toastMessage("Error in getting IRC Rate History Details", TOAST_HEADER_LABELS.TOAST_WARNING));
            })
    }

    //Fields On Change Event Handler
    ircRatesOnChange(event) {
        this.createBtnDisabled = false;

        if (event.target.name == 'RateType') {
            this.ircRateDetails.RateType__c = event.detail.value;
        } else if (event.target.name == 'AvailableLicenseTypes') {
            this.ircRateDetails.LicenseType__c = event.detail.value;
        } else if (event.target.name == 'IRCBedCapacity') {
            this.ircRateDetails.IRCBedCapacity__c = Number(event.target.value);
        } else if (event.target.name == 'StartDate') {
            this.ircRateDetails.StartDate__c = event.target.value;
            this.minenddate = this.ircRateDetails.StartDate__c;
        } else if (event.target.name == 'EndDate') {
            this.ircRateDetails.EndDate__c = event.target.value;
        } else if (event.target.name == 'PerDiemRates') {
            this.ircRateDetails.PerDiemRate__c = Number(event.target.value);
            this.ircRateDetails.Annual__c = Number((event.target.value * 365).toFixed(2));
            this.ircRateDetails.Monthly__c = Number((this.ircRateDetails.Annual__c / 12).toFixed(2));
        }
    }

    //Create or Update IRC Rate Details
    createIRCRateDetails() {
        const allValid = [
                ...this.template.querySelectorAll('lightning-input'),
                ...this.template.querySelectorAll('lightning-combobox')
            ]
            .reduce((validSoFar, inputFields) => {
                inputFields.reportValidity();
                return validSoFar && inputFields.checkValidity();
            }, true);

        if (allValid) {
            if (this.ircRateDetails && !this.ircRateDetails.RateCode__c)
                return this.dispatchEvent(utils.toastMessage("IRC Rate Code is mandatory", TOAST_HEADER_LABELS.TOAST_WARNING));

            if (this.ircRateDetails && (new Date(this.ircRateDetails.EndDate__c).setHours(0,0,0,0) < new Date(this.ircRateDetails.StartDate__c).setHours(0,0,0,0)))
                return this.dispatchEvent(utils.toastMessage("End Date should be greater than Start Date", TOAST_HEADER_LABELS.TOAST_WARNING));

            this.createBtnDisabled = true;

            if (this.ircRateId && this.compareObjects(this.previousIRCRateDetails, this.ircRateDetails)) {
                this.openModel = false;
                this.ircRateId = null;
                this.ircRateDetails = {};
                this.createBtnDisabled = false;
                this.licenseTypeDisabled = false;
                this.editModelCreateRecordFlag = false;

                this.dispatchEvent(utils.toastMessage("IRC Rates has been saved successfully", TOAST_HEADER_LABELS.TOAST_SUCCESS));
            } else {
                if (!this.ircRateId)
                    return this.createNewIRCRecord();

                const fields = {};

                fields[ID_FIELD.fieldApiName] = this.previousIRCRateId;
                fields[STATUS_FIELD.fieldApiName] = 'Expired';

                const prevRecordInput = {
                    fields
                };

                updateRecord(prevRecordInput)
                    .then(() => {
                        this.previousIRCRateId = null;
                        this.previousIRCRateDetails = {};

                        setTimeout(() => {
                            this.createNewIRCRecord();
                        }, 1000);
                    })
                    .catch(error => {
                        this.dispatchEvent(utils.toastMessage("Error in creating IRC Rates. Please check.", TOAST_HEADER_LABELS.TOAST_WARNING));
                    })
            }
        } else {
            this.dispatchEvent(utils.toastMessage("Error in creating IRC Rates. Please check.", TOAST_HEADER_LABELS.TOAST_WARNING));
        }
    }

    createNewIRCRecord() {
        const fields = {};

        fields[RATETYPE_FIELD.fieldApiName] = this.ircRateDetails.RateType__c;
        fields[LICENSETYPE_FIELD.fieldApiName] = this.ircRateDetails.LicenseType__c;
        fields[IRCBEDCAPACITY_FIELD.fieldApiName] = this.ircRateDetails.IRCBedCapacity__c;
        fields[STARTDATE_FIELD.fieldApiName] = this.ircRateDetails.StartDate__c;
        fields[ENDDATE_FIELD.fieldApiName] = this.ircRateDetails.EndDate__c;
        fields[PERDIEMRATE_FIELD.fieldApiName] = this.ircRateDetails.PerDiemRate__c;
        fields[MONTHLY_FIELD.fieldApiName] = this.ircRateDetails.Monthly__c;
        fields[ANNUAL_FIELD.fieldApiName] = this.ircRateDetails.Annual__c;
        fields[RATECODE_FIELD.fieldApiName] = this.editModelCreateRecordFlag ? this.ircRateCode : this.ircRateDetails.RateCode__c;
        fields[PROVIDER_FIELD.fieldApiName] = this.providerforApiCall;
        fields[STATUS_FIELD.fieldApiName] = 'Active';

        const recordInput = {
            apiName: IRCRATES_OBJECT.objectApiName,
            fields
        };

        createRecord(recordInput)
            .then(result => {
                this.openModel = false;
                this.ircRateDetails = {};
                this.ircRateId = null;
                this.licenseTypeDisabled = false;
                this.createBtnDisabled = false;
                this.initialLoad();

                this.dispatchEvent(utils.toastMessage("IRC Rates has been created successfully", TOAST_HEADER_LABELS.TOAST_SUCCESS));

                refreshApex(this.wiredIRCRateDetails);
                refreshApex(this.wiredIRCRateHistoryDetails);
            })
            .catch(error => {
                this.createBtnDisabled = false;
                this.dispatchEvent(utils.toastMessage("Error in creating IRC Rates Details. Please check.", TOAST_HEADER_LABELS.TOAST_WARNING));
            })
    }

    //Cancel IRC Rate Model Box
    cancelIRCRateDetails() {
        this.ircRateDetails = {};
        this.openModel = false;
        this.viewModel = false;
        this.licenseTypeDisabled = false;
        this.ircRateId = null;
        this.previousIRCRateId = null;
        this.editModelCreateRecordFlag = false;
        this.createBtnDisabled = false;
        this.initialLoad();
    }

    //Close IRC Rate Model Box
    closeIRCRateModel() {
        this.ircRateDetails = {};
        this.openModel = false;
        this.viewModel = false;
        this.ircRateId = null;
        this.historyModel = false;
        this.licenseTypeDisabled = false;
        this.editModelCreateRecordFlag = false;
        this.noHistoryRecordsFound = false;
        this.createBtnDisabled = false;
        this.initialLoad();
    }

    rateCodeGeneration(ircRateCode) {
        ircRateCode = Number(ircRateCode.split('-')[1]) + 1;

        if (ircRateCode < 10) {
            this.ircRateCode = 'R-000' + ircRateCode;
        } else if (ircRateCode >= 10 && ircRateCode < 100) {
            this.ircRateCode = 'R-00' + ircRateCode;
        } else if (ircRateCode >= 100 && ircRateCode < 1000) {
            this.ircRateCode = 'R-0' + ircRateCode;
        } else if (ircRateCode >= 1000 && ircRateCode < 10000) {
            this.ircRateCode = 'R-' + ircRateCode;
        }
    }

    compareObjects(a, b) {
        return JSON.stringify(a) === JSON.stringify(b);
    }
}