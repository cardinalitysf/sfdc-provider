// /**
//  * @Author        : Janaswini S
//  * @CreatedOn     : May 15, 2020
//  * @Purpose       : Public Provider's Home Information
//  **/

import {
  LightningElement,
  track,
  wire,
  api
} from 'lwc';
import getMultiplePicklistValues from "@salesforce/apex/PublicApplicationHomeInfo.getMultiplePicklistValues";
import fetchHomeInformation from "@salesforce/apex/PublicProviderHomeInformation.fetchHomeInformation";
import images from "@salesforce/resourceUrl/images";
import * as sharedData from 'c/sharedData';
import homeSlider from "@salesforce/resourceUrl/HomeSlider";

import {
  createRecord,
  updateRecord
} from "lightning/uiRecordApi";
import HOME_INFORMATION_OBJECT from '@salesforce/schema/HomeInformation__c';
import CASE_OBJECT_FIELD from '@salesforce/schema/HomeInformation__c.Provider__c';
import ID_HOME_INFORMATION_FIELD from '@salesforce/schema/HomeInformation__c.Id';

import {
  utils
} from 'c/utils';
import {
  CJAMS_CONSTANTS
} from 'c/constants';

export default class PublicProviderHomeInformation extends LightningElement {
  @track homeInformation = {
    ChildrenResiding__c: 1,
    Bedrooms__c: 1,
    PoolLocated__c: "In Ground",
    SwimmingPool__c: "No",
    LicensedAgency__c: "No",
    LicensedProvider__c: "No",
    WaterfrontProperty__c: "No",
  };
  @track homeInformationId = null;
  @track btnGrp;
  @track LicensedAgency;
  @track LicensedProvider;
  @track LicensedProviderOnchange;
  @track poolLocated;
  @track SwimmingPool;
  @track WaterfrontProperty;
  @track poolLocatedYes = false;
  @track agencyNameYes = false;
  @track providerDetailsYes = false;
  @track Spinner = true;
  @track saveDisable = true;
  @track openModelwindow = false;
  @track disableStatus = false;

  homeInformationIcon = images + "/home-information.svg";
  privateIcon = images + "/office-building.svg";
  existingIcon = images + "/clipboard.svg";
  plusIcon = images + "/plus-new-referal.svg";
  publicIcon = images + "/houseIcon.svg";

  _bedRoomSliderValue;
  _childrenSliderValue;
  _bedRoomSlider = homeSlider + "/BedRoom.html";
  _childrenSlider = homeSlider + "/Children.html";

  get bedRoomSlider() {
    return this._bedRoomSlider;
  }
  get childrenSlider() {
    return this._childrenSlider;
  }



  connectedCallback() {
    if (this.getPublicReferralStatus == undefined || this.getPublicReferralStatus == "Pending" || this.getPublicReferralStatus == "Assigned for Training") {
      this.disableStatus = false;
    } else {
      this.disableStatus = true;
    }
    window.addEventListener("message", event => {
      let iframevalue = event.data.split(":");
      switch (iframevalue[0]) {
        case "BedRoom":
          this._bedRoomSliderValue = iframevalue[1];
          break;
        case "Children":
          this._childrenSliderValue = iframevalue[1];
          break;

        default:
        // code block
      }
    });
  }

  get caseId() {
    return sharedData.getCaseId();
  }

  get providerId() {
    return sharedData.getProviderId();
  }

  get getPublicReferralStatus() {
    return sharedData.getPublicReferralStatus();
  }

  @wire(getMultiplePicklistValues, {
    objInfo: 'HomeInformation__c',
    picklistFieldApi: 'LicensedAgency__c,LicensedProvider__c,PoolLocated__c,SwimmingPool__c,WaterfrontProperty__c'
  })
  wiredTitle(data) {
    try {
      if (data.data != undefined && data.data !== '') {
        this.LicensedAgency = data.data.LicensedAgency__c;
        this.LicensedProvider = data.data.LicensedProvider__c;
        this.poolLocated = data.data.PoolLocated__c;
        this.SwimmingPool = data.data.SwimmingPool__c;
        this.WaterfrontProperty = data.data.WaterfrontProperty__c;
      }
    } catch (error) {
      throw error;
    }
  }

  @wire(fetchHomeInformation, {
    id: '$providerId'
  })
  fettchDetails(data) {
    if (data.data != undefined && data.data.length > 0) {
      this.homeInformationId = data.data[0].Id;
      this._bedRoomSliderValue = data.data[0].Bedrooms__c;
      this._childrenSliderValue = data.data[0].ChildrenResiding__c;
      this._bedRoomSlider = homeSlider + "/BedRoom.html?Bedroom=" + this._bedRoomSliderValue + "&disableSlider=" + this.disableStatus;
      this._childrenSlider = homeSlider + "/Children.html?Children=" + this._childrenSliderValue + "&disableSlider=" + this.disableStatus;
      this.homeInformation.AgencyName__c = data.data[0].AgencyName__c;
      this.homeInformation.LicensedProviderDetail__c = data.data[0].LicensedProviderDetail__c;
      data.data[0].LicensedAgency__c == 'Yes' ? this.agencyNameYes = true : this.agencyNameYes = false;
      this.homeInformation.LicensedAgency__c = data.data[0].LicensedAgency__c;
      data.data[0].LicensedProvider__c == 'Yes' ? this.providerDetailsYes = true : this.providerDetailsYes = false;
      this.homeInformation.LicensedProvider__c = data.data[0].LicensedProvider__c;
      this.homeInformation.PoolLocated__c = data.data[0].PoolLocated__c;
      data.data[0].SwimmingPool__c == 'Yes' ? this.poolLocatedYes = true : this.poolLocatedYes = false;
      this.homeInformation.SwimmingPool__c = data.data[0].SwimmingPool__c;
      this.homeInformation.WaterfrontProperty__c = data.data[0].WaterfrontProperty__c;
      this.Spinner = false;
    } else if (data.error) {
      let errors = data.error;
      if (errors) {
        let error = JSON.parse(errors.body.message);
        const { title, message, errorType } = error;
        this.dispatchEvent(
          utils.toastMessageWithTitle(title, message, errorType)
        );
      } else {
        this.dispatchEvent(
          utils.toastMessage(
            CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
            CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
          )
        );
      }
    }
    else {
      this._bedRoomSlider = homeSlider + "/BedRoom.html?disableSlider=" + this.disableStatus;
      this._childrenSlider = homeSlider + "/Children.html?disableSlider=" + this.disableStatus;
    }
    this.Spinner = false;
  }

  handleChange(event) {
    this.homeInformation.AgencyName__c = event.target.value;
  }

  handlechangeDetails(event) {
    this.homeInformation.LicensedProviderDetail__c = event.target.value;
  }

  handleSectionResiding(event) {
    this.homeInformation.ChildrenResiding__c = event.target.value;
    this.activeSectionMessageResiding = event.target.value;
    if (!this.homeInformation.ChildrenResiding__c) {
      return this.dispatchEvent(utils.toastMessage("Please enter a children residing", "error"));
    }
    this.saveDisable = false;
  }

  handleSectionBedroom(event) {
    this.homeInformation.Bedrooms__c = event.target.value;
    this.activeSectionMessageBedroom = event.target.value;
    if (!this.homeInformation.Bedrooms__c) {
      return this.dispatchEvent(utils.toastMessage("Please enter the bedrooms", "error"));
    }
    this.saveDisable = false;
  }

  handleButtonGroupLicensedAgency(event) {
    this.homeInformation.LicensedAgency__c = event.target.value;
    if (this.homeInformation.LicensedAgency__c == 'Yes') {
      this.agencyNameYes = true;
    } else {
      this.agencyNameYes = false;
    }
  }

  handleButtonGroupWater(event) {
    this.homeInformation.WaterfrontProperty__c = event.target.value;
  }

  handleButtonGroupSwimmingPool(event) {
    this.homeInformation.SwimmingPool__c = event.target.value;
    if (this.homeInformation.SwimmingPool__c == 'Yes') {
      this.poolLocatedYes = true;
    } else {
      this.poolLocatedYes = false;
    }
  }

  handleButtonGroupPoolLocated(event) {
    this.homeInformation.PoolLocated__c = event.target.value;
  }

  LicensedProvideronchange(event) {
    this.homeInformation.LicensedProvider__c = event.target.value;
    if (this.homeInformation.LicensedProvider__c == 'Yes') {
      this.providerDetailsYes = true;
    } else {
      this.providerDetailsYes = false;
    }
  }


  handleSubmit(event) {

    event.preventDefault();
    let fields = this.homeInformation;
    this.homeInformation.ChildrenResiding__c = this._childrenSliderValue;
    this.homeInformation.Bedrooms__c = this._bedRoomSliderValue;
    if (!this.homeInformation.ChildrenResiding__c) {
      return this.dispatchEvent(utils.toastMessage("Please enter a children residing", "warning"));
    }
    if (this.homeInformation.ChildrenResiding__c > 10) {
      return this.dispatchEvent(
        utils.toastMessage("Exceeded Children Residing Value more than 10", "warning")
      );
    }
    if (!this.homeInformation.Bedrooms__c) {
      return this.dispatchEvent(utils.toastMessage("Please enter the bedrooms", "warning"));
    }
    if (this.homeInformation.Bedrooms__c > 10) {
      return this.dispatchEvent(
        utils.toastMessage("Exceeded Bedrooms Value more than 10", "warning")
      );
    }
    this.Spinner = true;
    if (this.homeInformationId === undefined || this.homeInformationId === null || this.homeInformationId === '') {
      fields[CASE_OBJECT_FIELD.fieldApiName] = this.providerId;
      const recordInput = {
        apiName: HOME_INFORMATION_OBJECT.objectApiName,
        fields
      };
      createRecord(recordInput)
        .then(() => {
          this.Spinner = false;
          this.dispatchEvent(utils.toastMessage("Successfully Saved Home Information", "success"));
        })
        .catch((errors) => {
          if (errors) {
            let error = JSON.parse(errors.body.message);
            const { title, message, errorType } = error;
            this.dispatchEvent(
              utils.toastMessageWithTitle(title, message, errorType)
            );
          } else {
            this.dispatchEvent(
              utils.toastMessage(
                CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
                CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
              )
            );
          }
        })

    } else {
      fields[ID_HOME_INFORMATION_FIELD.fieldApiName] = this.homeInformationId;
      const recordInput = {
        fields
      };

      updateRecord(recordInput)
        .then(() => {
          try {
            this.Spinner = false;

            this.dispatchEvent(utils.toastMessage("Successfully Updated Home Information", "success"));
          } catch (error) {
            throw error;
          }
        })
        .catch((errors) => {
          if (errors) {
            let error = JSON.parse(errors.body.message);
            const { title, message, errorType } = error;
            this.dispatchEvent(
              utils.toastMessageWithTitle(title, message, errorType)
            );
          } else {
            this.dispatchEvent(
              utils.toastMessage(
                CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
                CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
              )
            );
          }
        })
    }
  }

  onHandleClear() {
    this.homeInformation;
    this.homeInformation.ChildrenResiding__c = 1;
    this.activeSectionMessageResiding = 1;
    this.homeInformation.Bedrooms__c = 1;
    this.activeSectionMessageBedroom = 1;
    this.homeInformation.AgencyName__c = '';
    this.homeInformation.LicensedProviderDetail__c = "";
    this.homeInformation.LicensedAgency__c = "";
    this.homeInformation.LicensedProvider__c = "";
    this.homeInformation.SwimmingPool__c = "";
    this.homeInformation.WaterfrontProperty__c = "";
    this.homeInformation.PoolLocated__c = "";

  }

}