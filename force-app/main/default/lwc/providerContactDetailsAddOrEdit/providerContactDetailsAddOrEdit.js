/**
 * @Author        : K.Sundar
 * @CreatedOn     : March 17, 2020
 * @Purpose       : This component contains ADDRESS INFO
 * @UpdatedBy     : Sundar K
 * @UpdatedOn     : MAY 14, 2020
 **/

import {
    LightningElement,
    track,
    wire,
    api
} from 'lwc';
import fetchAddressTypeValues from '@salesforce/apex/providerContactDetails.getAddressTypeValues';
import getAllContactDetails from "@salesforce/apex/providerContactDetails.getAllContactDetails";

import insertContactDetails from "@salesforce/apex/providerContactDetails.insertContactDetails";

import getAPIStreetAddress from "@salesforce/apex/GetStreetAddressFromGoogleAPI.getSuggestions";
import getFullDetails from "@salesforce/apex/GetStreetAddressFromGoogleAPI.getPlaceDetails";

import editContactDetails from "@salesforce/apex/providerContactDetails.editContactDetails";

import getAllStates from "@salesforce/apex/providerContactDetails.getStateDetails";

import {
    loadStyle
} from 'lightning/platformResourceLoader';
import myResource from '@salesforce/resourceUrl/styleSheet';

import {
    deleteRecord
} from "lightning/uiRecordApi";

import {
    utils
} from 'c/utils';
import * as sharedData from 'c/sharedData';
import getUserEmail from '@salesforce/apex/providerApplication.getUserEmail';
import getProviderId from '@salesforce/apex/providerApplicationHeader.getProviderId';

export default class ContactDetailsAddOrEdit extends LightningElement {
    @track columns = [];
    @track openModel = false;
    @track deleteModal = false;
    @track addressTypeOptions;
    @track addressDetails = {};
    @track strStreetAddresPrediction = [];
    @track selectedCounty;
    @track selectedState;
    @track selectedCity;
    @track selectedZipCode;
    @track addressId;
    @track deleteId;
    @track showStateBox = false;
    @track stateFetchResult = [];
    @track stateSearchTxt;
    @track filteredStateArr = [];
    @track showCountyBox = false;
    @track countySearchTxt;
    @track filteredCountyArr = [];
    @track countyArr = [];
    @track noRecordsFound = false;
    @track isUpdateFlag = false;
    active = false;
    buttonactive = true;
    @track deleteLoader = false;
    //Sort Function
    @track defaultSortDirection = 'asc';
    @track sortDirection = 'asc';
    @track sortedBy;

    providerforApiCall = this.providerid;

    //Pagination Part
    @track totalContactsCount = 0;
    @track totalContacts = [];
    @track page = 1;
    @track currentPageContactData;
    setPagination = 5;
    perpage = 10;

    @track disabled = false;
    @track disableType = false;

    @track title;
    @track btnLabel;
    @track providerid;

    renderedCallback() {
        Promise.all([
            loadStyle(this, myResource + '/styleSheet.css'),
        ])
    }

    // get providerid() {
    //     return sharedData.getProviderId();
    // }

    connectedCallback() {
        getUserEmail()
        .then(res=>{
            getProviderId({Id: res[0].Id})
            .then(res=> {
                this.providerid = res[0].Id;
                sharedData.setProviderId(this.providerid);
                this.handleContactDetails();
            }).catch(errors => {
                return this.dispatchEvent(utils.handleError(errors));
            });
        })
        .catch(errors=>{
            return this.dispatchEvent(utils.handleError(errors));
        })
    }

    handleContactDetails() {
        getAllContactDetails({providerId:this.providerid})
        .then(result=>{
            if (result) {
                this.totalContactsCount = result.length;
    
                if (this.totalContactsCount > 0)
                    this.noRecordsFound = false;
                else
                    this.noRecordsFound = true;
    
                this.totalContacts = result.map(row => {
                    return {
                        Id: row.Id,
                        AddressType__c: row.AddressType__c,
                        AddressLine1__c: row.AddressLine1__c,
                        Email__c: row.Email__c,
                        Phone__c: row.Phone__c
                    }
                });
                this.pageData();
            }
        }).catch(errors => {
            return this.dispatchEvent(utils.handleError(errors));
        });
    }

    get strStreetAddresPredictionLength() {
        return this.strStreetAddresPrediction.predictions ? true : false;
    }

    get tabClass() {
        return this.active ?
            "slds-dropdown slds-dropdown_length-5 slds-dropdown_fluid slds-show" :
            "slds-dropdown slds-dropdown_length-5 slds-dropdown_fluid slds-hide"
    }

    constructor() {
        super();

        this.columns = [{
            label: 'Address',
            fieldName: 'AddressLine1__c',
            type: 'text',
            sortable: true
        },
        {
            label: 'Type',
            fieldName: 'AddressType__c',
            type: 'text'
        },
        {
            label: 'Email',
            fieldName: 'Email__c',
            type: 'text'
        },
        {
            label: 'Phone',
            fieldName: 'Phone__c',
            type: 'text'
        },
        {
            label: 'Action',
            fieldName: 'Id',
            type: "button",
            initialWidth: 120,
            typeAttributes: {
                iconName: 'utility:edit',
                name: 'Edit',
                title: 'Edit',
                initialWidth: 20,
                disabled: false,
                iconPosition: 'left',
                target: '_self'

            }
        }, {
            type: "button",
            typeAttributes: {
                iconName: 'utility:delete',
                name: 'Delete',
                title: 'Delete',
                initialWidth: 20,
                class: 'del-red del-position',
                disabled: false,
                iconPosition: 'left',
                target: '_self'
            }
        }
        ];
    }

    //Pagination
    pageData = () => {
        let page = this.page;
        let perpage = this.perpage;
        let startIndex = (page * perpage) - perpage;
        let endIndex = (page * perpage);
        this.currentPageContactData = this.totalContacts.slice(startIndex, endIndex);

        if (this.currentPageContactData.length == 0) {
            if (this.page != 1) {
                this.page = this.page - 1;
                this.pageData();
            }
        }
    }

    hanldeProgressValueChange(event) {
        this.page = event.detail;
        this.pageData();
    }

    //open contact dialog box
    openContactModel() {
        if (!this.addressId) {
            this.addressDetails = {};
            this.selectedState = "";
            this.selectedCity = "";
            this.selectedCounty = "";
            this.selectedZipCode = "";
            this.disableType = false;
        }
        this.disabled = true;
        this.openModel = true;
        this.title = "Add Contact Details";
        this.btnLabel = "SAVE";
        this.addressTypeInitialLoad();
        this.stateInitialLoad();
        this.countyInitialLoad();
    }

    //close contact dialog box
    closeContactModel() {
        this.addressDetails = {};
        this.selectedCounty = "";
        this.selectedState = "";
        this.selectedCity = "";
        this.selectedZipCode = "";
        this.openModel = false;
    }

    //Function used to fetch Address Type Values from Address Object when Modal Opens
    addressTypeInitialLoad() {
        fetchAddressTypeValues({
            objInfo: {
                sobjectType: 'Address__c'
            },
            pickListFieldApi: 'AddressType__c'
        })
            .then(data => {
                if (data.length > 0) {
                    let pickListArray = [];
                    data.forEach(key => {
                        if (['Main', 'Site', 'Payment'].includes(key.label)) {
                            pickListArray.push({
                                label: key.label,
                                value: key.value
                            });
                        }
                    });
                    this.addressTypeOptions = [...pickListArray];
                } else {
                    this.dispatchEvent(utils.toastMessage("Address Type not found.", "error"));
                }
            })
            .catch(error => {
                this.dispatchEvent(utils.toastMessage("Error in Fetching Address Types", "error"));
            })
    }

    //Function used to fetch all states from Address Object when Modal Opens
    stateInitialLoad() {
        getAllStates()
            .then(result => {
                if (result.length > 0) {
                    this.stateFetchResult = result.map(row => {
                        return {
                            Id: row.Id,
                            Value: row.RefValue__c
                        }
                    });
                } else {
                    this.dispatchEvent(utils.toastMessage("No states found", "error"));
                }
            })
            .catch(error => {
                this.dispatchEvent(utils.toastMessage("Error in fetching states", "error"));
            })
    }

    //Function used to capture on change states in an array
    stateOnChange(event) {
        let stateSearchTxt = event.target.value;

        if (!stateSearchTxt)
            this.showStateBox = false;
        else {
            this.filteredStateArr = this.stateFetchResult.filter(val => val.Value.indexOf(stateSearchTxt) > -1).map(sresult => {
                return sresult;
            });
            this.showStateBox = this.filteredStateArr.length > 0 ? true : false;
        }
    }

    //Function used to set selected state
    setSelectedState(event) {
        this.selectedState = event.currentTarget.dataset.value;
        this.showStateBox = false;
    }

    //Function used to set hardcoded county when Modal Opens
    countyInitialLoad() {
        this.countyArr = [{
            label: "Adams",
            value: "Adams"
        },
        {
            label: "Allen",
            value: "Allen"
        },
        {
            label: "Bartholomew",
            value: "Bartholomew"
        },
        {
            label: "Benton",
            value: "Benton"
        },
        {
            label: "Blackford",
            value: "Blackford"
        },
        {
            label: "Boone",
            value: "Boone"
        },
        {
            label: "Brown",
            value: "Brown"
        },
        {
            label: "Carroll",
            value: "Carroll"
        },
        {
            label: "Cass",
            value: "Cass"
        },
        {
            label: "Clark",
            value: "Clark"
        },
        {
            label: "Clay",
            value: "Clay"
        },
        {
            label: "Clinton",
            value: "Clinton"
        },
        {
            label: "Crawford",
            value: "Crawford"
        },
        {
            label: "Daviess",
            value: "Daviess"
        },
        {
            label: "Dearborn",
            value: "Dearborn"
        },
        {
            label: "Decatur",
            value: "Decatur"
        },
        {
            label: "DeKalb",
            value: "DeKalb"
        },
        {
            label: "Delaware",
            value: "Delaware"
        },
        {
            label: "Dubois",
            value: "Dubois"
        },
        {
            label: "Elkhart",
            value: "Elkhart"
        },
        {
            label: "Fayette",
            value: "Fayette"
        },
        {
            label: "Floyd",
            value: "Floyd"
        },
        {
            label: "Fountain",
            value: "Fountain"
        },
        {
            label: "Franklin",
            value: "Franklin"
        },
        {
            label: "Fulton",
            value: "Fulton"
        },
        {
            label: "Gibson",
            value: "Gibson"
        },
        {
            label: "Grant",
            value: "Grant"
        },
        {
            label: "Greene",
            value: "Greene"
        },
        {
            label: "Hamilton",
            value: "Hamilton"
        },
        {
            label: "Hancock",
            value: "Hancock"
        },
        {
            label: "Harrison",
            value: "Harrison"
        },
        {
            label: "Hendricks",
            value: "Hendricks"
        },
        {
            label: "Henry",
            value: "Henry"
        },
        {
            label: "Howard",
            value: "Howard"
        },
        {
            label: "Huntington",
            value: "Huntington"
        },
        {
            label: "Jackson",
            value: "Jackson"
        },
        {
            label: "Jasper",
            value: "Jasper"
        },
        {
            label: "Jay",
            value: "Jay"
        },
        {
            label: "Jefferson",
            value: "Jefferson"
        },
        {
            label: "Jennings",
            value: "Jennings"
        },
        {
            label: "Johnson",
            value: "Johnson"
        },
        {
            label: "Knox",
            value: "Knox"
        },
        {
            label: "Kosciusko",
            value: "Kosciusko"
        },
        {
            label: "LaGrange",
            value: "LaGrange"
        },
        {
            label: "Lake",
            value: "Lake"
        },
        {
            label: "LaPorte",
            value: "LaPorte"
        },
        {
            label: "Lawrence",
            value: "Lawrence"
        },
        {
            label: "Madison",
            value: "Madison"
        },
        {
            label: "Marion",
            value: "Marion"
        },
        {
            label: "Marshall",
            value: "Marshall"
        },
        {
            label: "Martin",
            value: "Martin"
        },
        {
            label: "Miami",
            value: "Miami"
        },
        {
            label: "Monroe",
            value: "Monroe"
        },
        {
            label: "Montgomery",
            value: "Montgomery"
        },
        {
            label: "Morgan",
            value: "Morgan"
        },
        {
            label: "Newton",
            value: "Newton"
        },
        {
            label: "Noble",
            value: "Noble"
        },
        {
            label: "Ohio",
            value: "Ohio"
        },
        {
            label: "Orange",
            value: "Orange"
        },
        {
            label: "Owen",
            value: "Owen"
        },
        {
            label: "Parke",
            value: "Parke"
        },
        {
            label: "Perry",
            value: "Perry"
        },
        {
            label: "Pike",
            value: "Pike"
        },
        {
            label: "Porter",
            value: "Porter"
        },
        {
            label: "Posey",
            value: "Posey"
        },
        {
            label: "Pulaski",
            value: "Pulaski"
        },
        {
            label: "Putnam",
            value: "Putnam"
        },
        {
            label: "Randolph",
            value: "Randolph"
        },
        {
            label: "Ripley",
            value: "Ripley"
        },
        {
            label: "Rush",
            value: "Rush"
        },
        {
            label: "St. Joseph",
            value: "St. Joseph"
        },
        {
            label: "Scott",
            value: "Scott"
        },
        {
            label: "Shelby",
            value: "Shelby"
        },
        {
            label: "Spencer",
            value: "Spencer"
        },
        {
            label: "Starke",
            value: "Starke"
        },
        {
            label: "Steuben",
            value: "Steuben"
        },
        {
            label: "Sullivan",
            value: "Sullivan"
        },
        {
            label: "Switzerland",
            value: "Switzerland"
        },
        {
            label: "Tippecanoe County",
            value: "Tippecanoe County"
        },
        {
            label: "Tipton",
            value: "Tipton"
        },
        {
            label: "Union",
            value: "Union"
        },
        {
            label: "Vanderburgh",
            value: "Vanderburgh"
        },
        {
            label: "Vermillion",
            value: "Vermillion"
        },
        {
            label: "Vigo",
            value: "Vigo"
        },
        {
            label: "Wabash",
            value: "Wabash"
        },
        {
            label: "Warren",
            value: "Warren"
        },
        {
            label: "Warrick",
            value: "Warrick"
        },
        {
            label: "Washington",
            value: "Washington"
        },
        {
            label: "Wayne",
            value: "Wayne"
        },
        {
            label: "Wells",
            value: "Wells"
        },
        {
            label: "White",
            value: "White"
        },
        {
            label: "Whitley",
            value: "Whitley"
        }
        ]
        return this.countyArr;
    }

    //Function used to capture on change county in an array
    countyOnChange(event) {
        let countySearchTxt = event.target.value;

        if (!countySearchTxt)
            this.showCountyBox = false;
        else {
            this.filteredCountyArr = this.countyArr.filter(val => val.value.indexOf(countySearchTxt) > -1).map(cresult => {
                return cresult;
            });
            this.showCountyBox = this.filteredCountyArr.length > 0 ? true : false;
        }
    }

    //Function used to set selected city
    setSelectedCounty(event) {
        this.selectedCounty = event.currentTarget.dataset.value;
        this.showCountyBox = false;
    }

    //getStreetAddress from Google API when user manually input
    getStreetAddress(event) {
        this.addressDetails.addressLine1 = event.target.value;

        if (!event.target.value) {
            this.selectedCounty = "";
            this.selectedState = "";
            this.selectedCity = "";
            this.selectedZipCode = "";
        } else {
            getAPIStreetAddress({
                input: this.addressDetails.addressLine1
            })
                .then(result => {
                    this.strStreetAddresPrediction = JSON.parse(result);
                    this.active = this.strStreetAddresPrediction.predictions.length > 0 ? true : false;
                })
                .catch(error => {
                    this.dispatchEvent(utils.toastMessage("Error in Fetching Google API Street Address", "error"));
                })
        }
    }

    selectStreetAddress(event) {
        this.addressDetails.addressLine1 = event.target.dataset.description;

        getFullDetails({
            placeId: event.target.dataset.id
        })
            .then(result => {
                var main = JSON.parse(result);
                try {
                    var cityTextbox = this.template.querySelector(".dummyCity");
                    this.selectedCity = main.result.address_components[2].long_name;

                    var countyComboBox = this.template.querySelector(".dummyCounty");
                    this.selectedCounty = main.result.address_components[4].long_name;

                    var stateComboBox = this.template.querySelector(".dummyState");
                    this.selectedState = main.result.address_components[5].long_name;

                    var ZipCodeTextbox = this.template.querySelector(".dummyZipCode");
                    this.selectedZipCode = main.result.address_components[7].long_name;
                } catch (ex) { }
                this.active = false;
            })
            .error(error => {
                this.dispatchEvent(utils.toastMessage("Error in Selected Address", "error"));
            });
    }

    //All onchange event handler
    addressOnChange(event) {
        this.disabled = false;
        if (event.target.name == 'AddressType') {
            this.addressDetails['type'] = event.detail.value;
        } else if (event.target.name == 'AddressLine2') {
            this.addressDetails['addressLine2'] = event.target.value;
        } else if (event.target.name == 'State') {
            this.selectedState = event.target.value;
        } else if (event.target.name == 'City') {
            this.selectedCity = event.target.value;
        } else if (event.target.name == 'ZipCode') {
            this.selectedZipCode = event.target.value.replace(/(\D+)/g, "");
        } else if (event.target.name == 'County') {
            this.selectedCounty = event.target.value;
        } else if (event.target.name == 'Email') {
            this.addressDetails['email'] = event.target.value;
        } else if (event.target.name == 'Phone') {
            event.target.value = event.target.value.replace(/(\D+)/g, '');
            this.addressDetails['phone'] = utils.formattedPhoneNumber(event.target.value);
        }
    }

    // Sorting the Data Table Column Function End
    sortBy(field, reverse, primer) {
        const key = primer ?
            function (x) {
                return primer(x[field]);
            } :
            function (x) {
                return x[field];
            };

        return function (a, b) {
            a = key(a);
            b = key(b);
            if (a === undefined) a = '';
            if (b === undefined) b = '';
            a = typeof (a) === 'number' ? a : a.toLowerCase();
            b = typeof (b) === 'number' ? b : b.toLowerCase();

            return reverse * ((a > b) - (b > a));
        };
    }

    onHandleSort(event) {
        const {
            fieldName: sortedBy,
            sortDirection: sortDirection
        } = event.detail;
        const cloneData = [...this.currentPageContactData];
        cloneData.sort(this.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1));
        this.currentPageContactData = cloneData;
        this.sortDirection = sortDirection;
        this.sortedBy = sortedBy;
    }

    handleRowAction(event) {
        if (event.detail.action.name == 'Edit') {
            let selectedAddressId = event.detail.row.Id;
            this.title = "Edit Contact Details";
            this.btnLabel = "UPDATE";
            this.openModel = true;

            editContactDetails({
                selectedAddressId: selectedAddressId
            })
                .then(result => {
                    if (result.length > 0) {
                        this.addressDetails.type = result[0].AddressType__c;
                        this.addressDetails.addressLine1 = result[0].AddressLine1__c;
                        this.addressDetails.addressLine2 = result[0].AddressLine2__c;
                        this.selectedState = result[0].State__c;
                        this.selectedCity = result[0].City__c;
                        this.selectedCounty = result[0].County__c;
                        this.selectedZipCode = result[0].ZipCode__c;
                        this.addressDetails.email = result[0].Email__c;
                        this.addressDetails.phone = result[0].Phone__c;
                        this.addressId = result[0].Id;
                        this.disableType = true;
                        this.disabled = false;
                    } else {
                        this.dispatchEvent(utils.toastMessage("Error in getting record", "error"));
                    }
                })
                .catch(error => {
                    this.dispatchEvent(utils.toastMessage("Error in getting record", "error"));
                })
        } else if (event.detail.action.name == 'Delete') {
            this.deleteModal = true;
            this.deleteId = event.detail.row.Id;
        }
    }

    //Function used to delete Contact
    handleDelete() {
        this.deleteLoader = true;
        deleteRecord(this.deleteId)
            .then(() => {
                this.handleContactDetails();
                this.deleteLoader = false;
                this.deleteModal = false;
                this.addressId = null;
                this.dispatchEvent(utils.toastMessage('Address has been deleted successfully', "success"));
            })
            .catch(error => {
                this.handleContactDetails();
                this.deleteModal = false;
                this.deleteLoader = false;
                this.dispatchEvent(utils.toastMessage("Error in deleting contact notes. please check.", "error"));
            })
    }

    closeDeleteModal() {
        this.deleteModal = false;
    }

    //upsert contact details
    saveContactDetails() {
        let inp = [
            ...this.template.querySelectorAll("lightning-input"),
            ...this.template.querySelectorAll("lightning-combobox")
        ];

        let contactObj = {
            sobjectType: 'Address__c'
        };

        contactObj.Provider__c = this.providerid;

        if (this.addressId != null) {
            contactObj.Id = this.addressId;
            this.isUpdateFlag = true;
        } else {
            this.isUpdateFlag = false;
        }

        inp.forEach(element => {
            switch (element.name) {
                case "AddressType":
                    contactObj.AddressType__c = element.value;
                    break;
                case "AddressLine1":
                    contactObj.AddressLine1__c = element.value;
                    break;
                case "AddressLine2":
                    contactObj.AddressLine2__c = element.value;
                    break;
                case "State":
                    contactObj.State__c = element.value;
                    break;
                case "City":
                    contactObj.City__c = element.value;
                    break;
                case "ZipCode":
                    contactObj.ZipCode__c = element.value;
                    break;
                case "County":
                    contactObj.County__c = element.value;
                    break;
                case "Email":
                    contactObj.Email__c = element.value;
                    break;
                case "Phone":
                    contactObj.Phone__c = element.value;
                    break;
            }
        });

        if (!contactObj.AddressType__c) {
            this.dispatchEvent(utils.toastMessage("Address Type is mandatory", "warning"));
        } else if (!contactObj.AddressLine1__c) {
            this.dispatchEvent(utils.toastMessage("Address Line1 is mandatory", "warning"));
        } else if (!contactObj.State__c) {
            this.dispatchEvent(utils.toastMessage("State is mandatory", "warning"));
        } else if (!contactObj.City__c) {
            this.dispatchEvent(utils.toastMessage("City is mandatory", "warning"));
        } else if (!contactObj.ZipCode__c) {
            this.dispatchEvent(utils.toastMessage("ZipCode is mandatory", "warning"));
        } else if (!contactObj.County__c) {
            this.dispatchEvent(utils.toastMessage("County is mandatory", "warning"));
        } else if (contactObj.ZipCode__c && ((/^\d{1,5}$/).test(contactObj.ZipCode__c) === false) || contactObj.ZipCode__c.length != 5) {
            this.dispatchEvent(utils.toastMessage("Invalid ZipCode", "warning"));
        }
        /*else if (contactObj.Email__c && (new RegExp(/^(([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+([,.](([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+)*$/).test(contactObj.Email1__c) === false)) {
                   this.dispatchEvent(utils.toastMessage("Invalid Email", "warning"));
               } else if (contactObj.Phone__c && (new RegExp(/^([\(]{1}[0-9]{3}[\)]{1}[ ]{1}[0-9]{3}[\-]{1}[0-9]{4})$/).test(contactObj.Phone__c) === false)) {
                   this.dispatchEvent(utils.toastMessage("Invalid Phone", "warning"));
               }*/
        else {
            if (contactObj.Phone__c && contactObj.Phone__c.length != 14)
                return this.dispatchEvent(utils.toastMessage("Invalid Phone", "warning"));

            this.disabled = true;

            insertContactDetails({
                objSobjecttoUpdateOrInsert: contactObj
            })
                .then(result => {
                    this.handleContactDetails();
                    this.disabled = false;
                    this.openModel = false;
                    this.addressDetails = {};
                    this.selectedState = null;
                    this.selectedCity = null;
                    this.selectedCounty = null;
                    this.selectedZipCode = null;
                    this.addressId = null;
                    
                    if (this.isUpdateFlag) {
                        this.dispatchEvent(utils.toastMessage("Contact Details has been updated successfully", "success"));
                    } else {
                        this.dispatchEvent(utils.toastMessage("Contact Details has been added successfully", "success"));
                    }
                    //return this.handleContactDetails();
                })
                .catch(error => {
                    this.disabled = false;
                    this.dispatchEvent(utils.toastMessage("Error in saving contact details. please check.", "error"));
                });
        }
    }

    cancelContactDetails() {
        this.disabled = false;
        this.addressDetails = {};
        this.selectedState = "";
        this.selectedCity = "";
        this.selectedCounty = "";
        this.selectedZipCode = "";
        this.openModel = false;
        this.addressId = null;
    }
}