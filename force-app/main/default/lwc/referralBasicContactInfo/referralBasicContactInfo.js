/**
 * @Author        : Saranraj
 * @CreatedOn     : Apr 16, 2020
 * @Purpose       : Referral Basic Contact Information.
 * @updatedBy     :
 * @updatedOn     :
 **/

import {LightningElement, track, wire, api} from 'lwc';

const columns = [
    { label: 'Selected Characteristics', fieldName: 'Selected Characteristics' },
    {
        label: "Action",
        type: "button",
        initialWidth: 100,
        fieldName: "id",
        typeAttributes: {
            iconName: "utility:delete",
            name: "Delete",
            title: "Delete",
            disabled: false,
            target: "_self",
            initialWidth: 20
        }
    },
];

export default class ReferralBasicContactInfo extends LightningElement {

    @track columns = columns;
}