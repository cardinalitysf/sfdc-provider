/**
 * @Author        : Pratheeba.V
 * @CreatedOn     : August 4 ,2020
 * @Purpose       : CPA HouseHoldMembers List
 **/
import { LightningElement,api } from 'lwc';
import getContactHouseHoldMembersDetails from "@salesforce/apex/CpaSearchHouseHoldMembers.getContactHouseHoldMembersDetails";
import images from "@salesforce/resourceUrl/images";
import { deleteRecord } from "lightning/uiRecordApi";
import {
    CJAMS_CONSTANTS
} from "c/constants";
import {
    utils
} from "c/utils";
import * as sharedData from "c/sharedData";

const columns = [{
    label: 'PERSON TYPE',
    fieldName: 'Role__c',
    type: 'text'
},
{
    label: 'FIRST NAME',
    fieldName: 'FirstName__c',
    type: 'text'
},
{
    label: 'LAST NAME',
    fieldName: 'LastName__c',
    type: 'text',
},
{
    label: 'GENDER',
    fieldName: 'Gender__c',
    type: 'text',
},
{
    label: 'START DATE',
    fieldName: 'StartDate__c',
    type: 'date',
},
{
    label: 'END DATE',
    fieldName: 'EndDate__c',
    type: 'date',
},
{
    label: 'CONTACT NUMBER (MOBILE)',
    fieldName: 'Phone',
},
];

export default class CpaHouseHoldMembersList extends LightningElement {
    ContactDetailsOfCard;
    norecorddisplay = true;
    defaultDesignClass = "mcard houseHoldMemberList";
    attachmentIcon = images + "/humanpictosIcon.svg";
    ContactID;
    signUrl;
    Spinner = true;
    ImgOfHHMembers;
    deleteModel = false;
    recIdDelete;
    editdeleteShowHide = true;
    addSearchbtnShowHide = true;
    getapplicationDetails;
    noimages = images + "/user.svg";
    disabled = false;
    AssignbtnDisable;
    columns = columns;
    recordId;
    deleteModel = false;
    disableDelClick = false;
    @api cpaDatas;
    cpaHomeId;
    @api cpaDataNew;
    disabledView;

    handleRowAction(event) {
        let recordId = event.detail.row.Id;
        sharedData.setHouseHoldContactId(recordId);
        if (event.detail.action.name == "Edit") {
            const onaddid = new CustomEvent("redirectfromedit", {
                detail: {
                    first: false
                }
            });
            this.dispatchEvent(onaddid);
        } else if (event.detail.action.name == "View") {
            const viewEvent = new CustomEvent("redirectfromview", {
                detail: {
                    isView: false
                }
            });
            this.dispatchEvent(viewEvent);
        } else if (event.detail.action.name == "Delete") {
            this.deleteRecordForincident = event.detail.row.Id;
            this.deleteModel = true;
        }
    }

    //Delete incident Record
    handleDelete() {
        this.disableDelClick = true;
        deleteRecord(this.deleteRecordForincident)
            .then(() => {
                this.deleteModel = false;
                this.dispatchEvent(
                    utils.toastMessage("Person Details deleted successfully", "success")
                );
                this.disableDelClick = false;
                this.getAllContactDetails();
            })
            .catch((error) => {
                this.disableDelClick = false;
                this.deleteModel = false;
                this.dispatchEvent(
                    utils.toastMessage("Warning in delete Person details", "Warning")
                );
            });
    }
    closeDeleteModal() {
        this.deleteModel = false;
    }

    addSearchHouseMemberRedirect() {
        const onsearchredirectid = new CustomEvent(
            "redirecttoaddhouseholdmembersearch",
            {
                detail: {
                    first: true
                }
            }
        );
        this.dispatchEvent(onsearchredirectid);
    }

    connectedCallback() {
        let a = JSON.parse(this.cpaDatas);   
        if(a.rowActionValue){
            this.cpaHomeId = a.rowActionValue.CPAId;
            if (a.rowActionValue.CPAStatus == "Suspended" || a.rowActionValue.CPAStatus == "Active") 
            this.disabledView = true;
        } 
        else {
            this.cpaHomeId = this.cpaDataNew;
        }           
            
        setTimeout(() => {
            this.getAllContactDetails();
        }, 1000);
    }
    @api
    getAllContactDetails() {
        let a = JSON.parse(this.cpaDatas);   
        if(a.rowActionValue)
            this.cpaHomeId = a.rowActionValue.CPAId;
        else
           this.cpaHomeId = this.cpaDataNew;

        if (!this.cpaHomeId) {
            this.norecorddisplay = false;
            this.Spinner = false;
            return;
        }
        getContactHouseHoldMembersDetails({
            cpaHomeId: this.cpaHomeId
        }).then((data) => {
            this.Spinner = false;
            if (data) {
                let contactID = [];
                if (data.length == 0) {
                    this.norecorddisplay = false;
                } else {
                    const selectedEvent = new CustomEvent("datacardhhmembertrue", {
                        detail: {
                            meetingFlag: true
                        }
                    });
                    this.dispatchEvent(selectedEvent);
                    this.norecorddisplay = true;
                    this.ContactDetailsOfCard = data.map((row) => {
                        contactID.push(row.Contact__r.Id);
                        return {
                            ActorId: row.Id,
                            Id: row.Contact__r.Id,
                            Role__c: row.Role__c,
                            Gender__c: row.Contact__r.Gender__c,
                            FirstName__c: row.Contact__r.FirstName__c,
                            LastName__c: row.Contact__r.LastName__c != undefined ? row.Contact__r.LastName__c : " ",
                            StartDate__c: row.Contact__r.StartDate__c,
                            EndDate__c: row.Contact__r.EndDate__c,
                            Phone: row.Contact__r.Phone,
                            buttonDisabled: (this.disabledView) ? this.disabledView : false
                        };
                    });
                    this.ContactID = contactID.toString();
                }
            }
        }).catch(errors => {
            let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
            return this.dispatchEvent(utils.handleError(errors, config));
        });
    }

    bactToAddressRedirect() {
        const onhouseholdredirect = new CustomEvent('redirecttoaddresspage', {
            detail: {
                first: true
            }
        });
        this.dispatchEvent(onhouseholdredirect);
    }

    /**if decision status = approved show only view otherwise show view and delete begins */
    constructor() {
        super();
        let deleteIcon = true;
        for (let i = 0; i < this.columns.length; i++) {
            if (this.columns[i].type == 'action') {
                deleteIcon = false;
            }
        }
        if (deleteIcon) {
            this.columns.push(
                {
                    type: 'action', typeAttributes: { rowActions: this.getRowActions }, cellAttributes: {
                        iconName: 'utility:threedots_vertical',
                        iconAlternativeText: 'Actions',
                        class: "tripledots"
                    }
                }
            )
        }
    }

    getRowActions(row, doneCallback) {
        const actions = [];
        if (row['buttonDisabled'] == true) {
            actions.push({
                'label': 'View',
                'name': 'View',
                'iconName': 'utility:preview',
                'target': '_self'
            });
        } else {
            actions.push({
                label: 'Edit',
                name: 'Edit',
                iconName: 'utility:edit',
                target: '_self'
            },
            {
                label: 'View',
                name: 'View',
                iconName: 'utility:preview',
                target: '_self'
            },
            {
                label: 'Delete',
                name: 'Delete',
                iconName: 'utility:delete',
                target: '_self'
            });
        }
        // simulate a trip to the server
        setTimeout(() => {
            doneCallback(actions);
        }, 200);
    }
    /**if decision status = approved show only view otherwise show view and delete ends */
}