/**
 * @Author        : Praveen
 * @CreatedOn     : Apr 15,2020
 * @Purpose       : Attachments based on Icon
 **/


import {
    LightningElement,
    track,
    api
} from 'lwc';

export default class ReferralAttachmentsIcon extends LightningElement {
    @track doctype;
    @api
    get documentType() {
        return this.doctype
    }
    set documentType(value) {
        switch (value.toUpperCase()) {
            case 'IMAGES':
            case 'PNG':
            case 'JPG':
            case 'SVG':
                this.doctype = 'doctype:image';
                break;
            case 'PDF':
                this.doctype = 'doctype:pdf';
                break;
            case 'MP4':
                this.doctype = 'doctype:video';
                break;
            case 'MP3':
                this.doctype = 'doctype:audio';
                break;
            case 'WORD_X':
                this.doctype = 'doctype:word';
                break;
            case 'POWER_POINT_X':
                this.doctype = 'doctype:ppt';
                break;
            case 'POWER_POINT_X':
                this.doctype = 'doctype:ppt';
                break;
            default:
                // code block
        }
    }
}