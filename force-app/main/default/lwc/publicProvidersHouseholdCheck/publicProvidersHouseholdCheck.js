// /**
//  * @Author        : G sathishkumar
//  * @CreatedOn     : june 17, 2020
//  * @Purpose       : PublicProvidersHouseholdCheck
//  **/


import { LightningElement, track, wire } from 'lwc';

import * as sharedData from 'c/sharedData';
import { utils } from "c/utils";

import getQuestionsByType from '@salesforce/apex/PublicProvidersHouseholdCheck.getQuestionsByType';
import getRecordType from '@salesforce/apex/PublicProvidersHouseholdCheck.getRecordType';
import bulkAddRecordAns from '@salesforce/apex/PublicProvidersHouseholdCheck.bulkAddRecordAns';
import ReconsiderationFromProRecQues from "@salesforce/schema/ProviderRecordQuestion__c.Reconsideration__c";
import RecordTypeIdFromProRecQues from "@salesforce/schema/ProviderRecordQuestion__c.RecordTypeId";
import PROVIDER_RECORD_QUESTION_API from "@salesforce/schema/ProviderRecordQuestion__c";
import getRecordQuestionId from '@salesforce/apex/PublicProvidersHouseholdCheck.getRecordQuestionId';
import getProviderAnswer from '@salesforce/apex/PublicProvidersHouseholdCheck.getProviderAnswer';
import { createRecord } from "lightning/uiRecordApi";
import RECONSIDERATIONSTATUS_FIELD from '@salesforce/schema/Reconsideration__c.Status__c';
import { getRecord } from 'lightning/uiRecordApi';
import { CJAMS_CONSTANTS} from "c/constants";
export default class PublicProvidersHouseholdCheck extends LightningElement {

  @track questions = [];
  @track receivedDate = utils.formatDateYYYYMMDD(new Date());
  @track RecordTypeID;
  @track type = 'Household';
  @track providerQuestionId;
  @track getProviderQuestionIdfromBegining;
  @track allHouseholdChecklistAnswers;
  @track answerUpdate = false;
  @track SuperVisorFlowBtnDisable = false;
  @track reconsiderationStatus;
  @track Spinner = false;


  get options() {
    return [
      { label: 'Yes', value: 'Yes' },
      { label: 'No', value: 'No' },
    ];
  }


  get reconsiderationId() {
    return sharedData.getReconsiderationId();
    //  return 'a0V0w000000Ryj5EAC';
  }


  get getUserProfileName() {
    return sharedData.getUserProfileName();
    // return "Supervisor";
    // return 'Caseworker';
  }

  statusChange(event) {
    if (event.target != undefined && event.target.name != undefined && event.target.name != null) {
      this.questions[event.target.name].HouseholdStatus__c = event.target.value;
    }
  }
  //Function used to get the data from Received date field
  receiveddatechange(event) {
    if (event.target != undefined && event.target.name != undefined && event.target.name != null) {
      this.questions[event.target.name].Date__c = event.target.value;
    }
  }

  commentChange(event) {

    if (event.target != undefined && event.target.name != undefined && event.target.name != null) {
      this.questions[event.target.name].Comments__c = event.target.value;
    }
  }
  //save function
  handleSubmit() {
    if (this.answerUpdate) {
      this.answerRecordSave();
    }
    else {
      this.questionRecordSave();
    }
  }



  questionRecordSave() {
    let fields = {};
    fields[ReconsiderationFromProRecQues.fieldApiName] = this.reconsiderationId;
    fields[RecordTypeIdFromProRecQues.fieldApiName] = this.RecordTypeID;
    const recordInput = {
      apiName: PROVIDER_RECORD_QUESTION_API.objectApiName,
      fields
    };
    createRecord(recordInput)
      .then((response) => {
        if (response) {
          this.providerQuestionId = response.id;
          this.answerRecordSave();
        }
      })
      .catch(errors => {
        let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
        return this.dispatchEvent(utils.handleError(errors, config));
      });
  }


  @wire(getQuestionsByType)

  wiredquestions(data) {
    if (data.data != null && data.data != '') {
      let currentData = [];
      let id = 0;
      this.questions = []
      data.data.forEach((row) => {
        let rowData = {};
        rowData.rowId = id;
        rowData.Question__c = row.Question__c;
        rowData.Date__c = utils.formatDateYYYYMMDD(new Date());
        currentData.push(rowData);
        id++;
      });
      this.questions = currentData;
    }
  }


  @wire(getRecordType, {
    name: '$type'
  })
  recordTypeDetails(result) {
    if (result.data != undefined && result.data.length > 0) {
      this.RecordTypeID = result && result.data;
      this.getProviderQuestionID();
    }
  }

  answerRecordSave() {
    this.Spinner = true;
    var questionanswerarray = []
    this.questions.forEach((item) => {
      questionanswerarray.push(
        {
          'id': item.AnswerId,
          'Comar__c': item.Id,
          'Date__c': item.Date__c,
          'ProviderRecordQuestion__c': this.providerQuestionId,
          'HouseholdStatus__c': item.HouseholdStatus__c,
          'Comments__c': item.Comments__c
        }
      );
    });
    let questionstosave = JSON.stringify(questionanswerarray);
    bulkAddRecordAns({
      datas: questionstosave
    })
      .then(result => {
        this.getProviderQuestionID();
        this.Spinner = false;
        this.dispatchEvent(utils.toastMessage("Records Has Been Updated Successfully!..", "success"));
      }).catch(errors => {
        let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
        return this.dispatchEvent(utils.handleError(errors, config));
      });
  }



  getProviderQuestionID() {
    getRecordQuestionId({ reconsId: this.reconsiderationId, recordTypeId: this.RecordTypeID }).then((result) => {
      var arrayRecordQuestion = [];
      result.forEach((item) => {
        arrayRecordQuestion.push(item.Id);
      });
      this.getProviderQuestionIdfromBegining = arrayRecordQuestion.toString();
      this.getProviderAnswerDetails();
    }).catch(errors => {
      let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
      return this.dispatchEvent(utils.handleError(errors, config));
    });
  }
  getProviderAnswerDetails() {
    getProviderAnswer({ getAnswers: this.getProviderQuestionIdfromBegining }).then((result) => {
      this.allHouseholdChecklistAnswers = result;
      if (this.allHouseholdChecklistAnswers.length > 0) {
        this.allHouseholdChecklistAnswers.forEach((item, index) => {
          this.questions[index].AnsComments = item.Comments__c;
          this.questions[index].AnsStatus = item.HouseholdStatus__c;
          //this.questions[index].AnsDate = item.Date__c;
          // this.questions[index].AnsDate = item.Date__c != undefined ? item. Date__c : utils.formatDateYYYYMMDD(new Date());
          this.questions[index].AnsDate = item.HouseholdStatus__c != undefined ? item.Date__c : utils.formatDateYYYYMMDD(new Date());
          this.questions[index].AnswerId = item.Id;
          this.questions[index].Comar__c = item.Comar__c;
          this.questions[index].Date__c = item.Date__c != undefined ? item.Date__c : utils.formatDateYYYYMMDD(new Date());
          this.questions[index].Comments__c = item.Comments__c;
          this.questions[index].HouseholdStatus__c = item.HouseholdStatus__c;
          this.providerQuestionId = item.ProviderRecordQuestion__c;
          this.answerUpdate = true;

        });

      } else {
        this.answerUpdate = false;
        this.questions.forEach((item) => {
          item.AnsComments = '';
          item.AnsStatus = '';
          item.AnsDate = utils.formatDateYYYYMMDD(new Date());
        });
      }



    }).catch(errors => {
      let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
      return this.dispatchEvent(utils.handleError(errors, config));
    });
  }



  @wire(getRecord, {
    recordId: "$reconsiderationId",
    fields: [RECONSIDERATIONSTATUS_FIELD]
  })
  wireuser({
    error,
    data
  }) {
    if (error) {
      this.error = error;
    } else if (data) {
      this.reconsiderationStatus = data.fields.Status__c.value;
      if (this.reconsiderationStatus) {
        if (this.getUserProfileName != 'Supervisor') {
          if (['Submitted', 'Approved', 'Rejected'].includes(this.reconsiderationStatus)) {
            this.SuperVisorFlowBtnDisable = true;
          }
          else {
            this.SuperVisorFlowBtnDisable = false;
          }
        }
        if (this.getUserProfileName == 'Supervisor') {
          this.SuperVisorFlowBtnDisable = true;
        }
      }
    }
  }









}