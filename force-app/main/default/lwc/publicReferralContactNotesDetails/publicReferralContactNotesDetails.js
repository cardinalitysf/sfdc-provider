/**
 * @Author        : Vijayaraj M
 * @CreatedOn     : June 06 , 2020
 * @Purpose       : This component for ContactNotes reusability Js file **/

 import { LightningElement } from "lwc";
import * as referralsharedDatas from "c/sharedData";
import { CJAMS_CONSTANTS } from "c/constants";

export default class PublicReferralContactNotesDetails extends LightningElement {
  get caseId() {
    return referralsharedDatas.getCaseId();
    //return '5000w000001Nx34AAC';
  }

  get sobjectName() {
    return CJAMS_CONSTANTS.CASE_LOOKUP_OBJECT;
  }
}
