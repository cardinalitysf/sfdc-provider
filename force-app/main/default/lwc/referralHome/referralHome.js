import {
  LightningElement,
  track,
  api,
  wire
} from "lwc";
import {
  CurrentPageReference
} from 'lightning/navigation';
import {
  fireEvent
} from 'c/pubsub';
export default class ReferralHome extends LightningElement {
  @track referid;

  @api caseid;
  @api hideshowNewOrExistingProviderComponet;
  @api frompage;
  @wire(CurrentPageReference) pageRef;
  hideShowHomeAndTypesCmp = true;
  @track showPrivateReferralsTabs = false;
  @track showPublicReferralsTabs = false;
  @track pageTypeFrom;
  @track showNewComplaints = false;

  handleComponentsToggle(event) {
    this.hideShowHomeAndTypesCmp = event.detail;
  }
  handleHomePageRedirection(event) {
    if (event.detail.pageType.includes("Private")) {
      this.hideShowHomeAndTypesCmp = false;
      if (event.detail.pageType == 'Private Existing') {
        this.hideshowNewOrExistingProviderComponet = true;
      } else {
        this.hideshowNewOrExistingProviderComponet = false;
      }
    } else {
      fireEvent(this.pageRef, "RedirectToPublicPrividerFromPrivateProvider", true);
    }
  }
  handleReferralIdForPropogation(event) {
    this.hideShowHomeAndTypesCmp = false;
    this.referid = event.detail;
  }

  handleRedirectCaseId(event) {
    if (event.detail.pageType !== undefined) {
      if (event.detail.pageType.includes("Private")) {
        this.hideShowHomeAndTypesCmp = event.detail.first;
        this.showPrivateReferralsTabs = !event.detail.first;
        if (event.detail.pageType === 'Private Existing')
          this.frompage = true;
        else if (event.detail.pageType === 'Private New')
          this.frompage = false;
      } else {
        this.hideShowHomeAndTypesCmp = event.detail.first;
        this.showPrivateReferralsTabs = event.detail.first;
        this.showPublicReferralsTabs = !event.detail.first;
      }
    } else {
      if (event.detail.pagetype !== undefined && event.detail.pagetype === 'public') {
        this.hideShowHomeAndTypesCmp = event.detail.first;
        this.showPrivateReferralsTabs = event.detail.first;
        this.showPublicReferralsTabs = !event.detail.first;
      } else if (event.detail.pagetype !== undefined && event.detail.pagetype === 'private') {
        this.hideShowHomeAndTypesCmp = event.detail.first;
        this.showPrivateReferralsTabs = !event.detail.first;
        this.frompage = false;
        this.pageTypeFrom = 'case';
      }
    }
  }

  redirectToReferralHome(event) {
    this.hideShowHomeAndTypesCmp = !event.detail.first;
    this.showPrivateReferralsTabs = event.detail.first;
    this.showPublicReferralsTabs = event.detail.first;
  }

  redirecttoNewComplaintsTabs(event) {
    this.showNewComplaints = event.detail.first;
    this.hideShowHomeAndTypesCmp = false;
  }
}