import { LightningElement, track } from 'lwc';

export default class LighteningTabSetTest extends LightningElement {


    @track showPublicApplicationTabs = true;
    @track NextIcon = false;
    @track PreviousIcon = false;
    @track PreviousValue = [];
    @track NextValue = 0;
    @track tabMenus;
    @track windowWidthIncludeArrow;
    @track LightningTabItems = [
        {name:'tab1',title:'BASIC INFORMATION',value:'BASIC INFORMATION'},
        {name:'tab2',title:'HOME INFORMATION',value:'HOME INFORMATION'},
        {name:'tab3',title:'HOUSEHOLD MEMBERS',value:'HOUSEHOLD MEMBERS'},
        {name:'tab4',title:'BACKGROUND CHECKS',value:'BACKGROUND CHECKS'},
        {name:'tab5',title:'SERVICES',value:'SERVICES'},
        {name:'tab6',title:'HOUSEHOLD CHECKLIST',value:'HOUSEHOLD CHECKLIST'},
        {name:'tab7',title:'HOUSEHOLD MEMBER CHECKLIST',value:'HOUSEHOLD MEMBER CHECKLIST'},
        {name:'tab8',title:'HOME STUDY VISIT',value:'HOME STUDY VISIT'},
        {name:'tab9',title:'CONTACT NOTES',value:'CONTACT NOTES'},
        {name:'tab10',title:'NARRATIVE',value:'NARRATIVE'},
        {name:'tab11',title:'DOCUMENTS',value:'DOCUMENTS'},
        {name:'tab12',title:'REFERENCE CHECKS',value:'REFERENCE CHECKS'},
        {name:'tab13',title:'PET INFO',value:'PET INFO'},
        {name:'tab14',title:'PRE-SERVICE TRAINING',value:'PRE-SERVICE TRAINING'},
        {name:'tab15',title:'PLACEMENT SPECIFICATIONS',value:'PLACEMENT SPECIFICATIONS'},
        {name:'tab16',title:'DECISION',value:'DECISION'},
    ]


    connectedCallback(){

        setTimeout(() => {
            let windowWidth = window.innerWidth;
            this.tabMenus = this.template.querySelectorAll('.slds-tabs_default__item');
            this.windowWidthIncludeArrow = windowWidth - 80;
            this.handleTabsLoad();
            this.handleTabs({target:{name:'tab1'}});

        }, 300);
        

        //mousemove
        window.addEventListener('resize', evt => {
            let windowWidth = window.innerWidth;
            this.tabMenus = this.template.querySelectorAll('.slds-tabs_default__item');
            this.windowWidthIncludeArrow = windowWidth - 80; 
            this.NextValue = 0; 
            this.PreviousValue = [];  
            this.handleTabsLoad();
          });
    }

    handleTabsLoad(){
            // let windowWidth = this.template.querySelector('.tabsetwidth').clientWidth;

            this.PreviousValue.push(this.NextValue);

            if(this.NextValue == 0){
                this.PreviousIcon = false;
            }
            else{
                this.PreviousIcon = true;
            }
            let tabMenusClientWidth = 0;
            for(let i=0;i<this.tabMenus.length;i++){
                if(i < this.NextValue){
                    this.tabMenus[i].classList.remove("slds-showd");
                    this.tabMenus[i].classList.add("slds-hide"); 
                }
                else{
                    this.tabMenus[i].classList.remove("slds-hide");
                    this.tabMenus[i].classList.add("slds-showd");  
                }
                this.NextIcon = false;
            }

            let x = 0;
            for(let i=this.NextValue;i<this.tabMenus.length;i++){
                tabMenusClientWidth += this.tabMenus[i].clientWidth;
                if(tabMenusClientWidth > this.windowWidthIncludeArrow){
                    this.tabMenus[i].classList.remove("slds-showd");
                    this.tabMenus[i].classList.add("slds-hide"); 
                    this.NextIcon = true;

                        this.NextValue = i-x;
                        x++;
                }

            }        
    }

    activeTabValue(value){
        for(let i=0;i<this.tabMenus.length;i++){
            if(value == this.tabMenus[i].dataset.id){
                this.tabMenus[i].classList.add("slds-is-active");
            }
            else{
                this.tabMenus[i].classList.remove("slds-is-active");
            }
        }
    }

    handleTabs(event){

        this.activeTabValue(event.target.name);

        let allTabsCount = this.template.querySelectorAll('.slds-tabs_default__content');

        allTabsCount.forEach((item) => {
            let indvRow = item;
            let indvRowId = item.getAttribute('id');
            if (indvRowId.includes(event.target.name+'-')) {
                    indvRow.classList.remove("slds-hide");
                    indvRow.classList.add("slds-showd");

            } else {
                indvRow.classList.add("slds-hide");
                indvRow.classList.remove("slds-showd");
            }
        });

    }

    handleNextTabs(){
        this.PreviousIcon = true;
        this.handleTabsLoad();
        
    }
    handlePreviousTabs(){
        this.NextValue = this.PreviousValue[this.PreviousValue.length - 2];
        this.PreviousValue.splice(this.PreviousValue.length-2);
        this.handleTabsLoad();
    } 
}