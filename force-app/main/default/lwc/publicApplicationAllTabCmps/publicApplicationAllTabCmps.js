import {
    LightningElement,
    track,
    wire,
    api
} from 'lwc';
import * as sharedData from 'c/sharedData';

export default class PublicApplicationAllTabCmps extends LightningElement {
    //Initial App Start
    @api showPublicApplicationDashboard = false;
    @api hideHeaderButton = false;
    @api showPublicApplicationTabs = false;

    @track openServiceInfoModal;
    @track openPetInfoModal;
    @track activeTab = 'basicInfo';
    @track openSessionModal;
    @track homeVisitModal;

    //Button Handling Functions
    @track openSessionModal;
    @track showPop = false;
    @track activeTabValue = null;
    @track showReferenceAddBtn = false;
    @track showAddService = false;
    @track showPetDetails = false;
    @track showAssignTraining = false;
    @track showAddHomeStudy = false;

    //HHM
    @track isView = false;
    @track searchHolder = false;
    @track showResult = false;
    @track meetingFlag;
    @track showListParent;

    @track showPublicProviderAddmember = false;
    @track showPublicProviderAddmemberSearch = false;
    @track sessionHoursFlag = false;
    @track homeStudyCompletedFlag = false;
    @track referenceCheckFlag = false;
    @track prideSessionNotFound = false;

    //supervisor
    @track ApplicationStatus;
    @track ApplicationId;

    get getUserProfileName() {
        return sharedData.getUserProfileName();
    }

    //Active Tab Button show/hide
    handleActiveTabButtons(event) {
        this.activeTabValue = event.target.value;
        if (this.activeTabValue === 'referenceCheck') {
            this.showReferenceAddBtn = true;
            this.showAddService = false;
            this.showPetDetails = false;
            this.showAssignTraining = false;
            this.showAddHomeStudy = false;
        } else if (this.activeTabValue === 'PetInfo') {
            this.showPetDetails = true;
            this.showReferenceAddBtn = false;
            this.showAddService = false;
            this.showAssignTraining = false;
            this.showAddHomeStudy = false;
        } else if (this.activeTabValue === 'StudyVisit') {
            this.showPetDetails = false;
            this.showReferenceAddBtn = false;
            this.showAddService = false;
            this.showAssignTraining = false;
            this.showAddHomeStudy = true;
        } else if (this.activeTabValue === 'AddServices') {
            this.showPetDetails = false;
            this.showReferenceAddBtn = false;
            this.showAddService = true;
            this.showAssignTraining = false;
            this.showAddHomeStudy = false;
        } else if (this.activeTabValue === 'TrainService') {
            this.showPetDetails = false;
            this.showReferenceAddBtn = false;
            this.showAddService = false;
            this.showAssignTraining = true;
            this.showAddHomeStudy = false;
        } else {
            this.showPetDetails = false;
            this.showReferenceAddBtn = false;
            this.showAddService = false;
            this.showAssignTraining = false;
            this.showAddHomeStudy = false;
        }
        this.supervisor();
    }

    redirectToServiceInfo(event) {
        this.openServiceInfoModal = true;
        this.activeTab = "AddServices";
    }
    handleToggleComponentsForService(event) {
        this.openServiceInfoModal = false;
    }
    redirectToPetInfo(event) {
        this.openPetInfoModal = true;
        this.activeTab = "PetInfo";
    }

    handleToggleComponents(event) {
        this.openPetInfoModal = false;
    }

    redirectToSessionInfo(event) {
        this.openSessionModal = true;
        this.activeTab = "TrainService";
    }

    handleToggleSessionComponents(event) {
        this.openSessionModal = false;
    }

    redirecttoStudyVisit(event) {
        this.homeVisitModal = true;
        this.activeTab = "StudyVisit";
    }
    handleToggleComponentsForHomeVisit(event) {
        this.homeVisitModal = false;
    }
    redirectToAddRefPop() {
        this.showPop = true;
    }

    redirectAddOff(event) {
        this.showPop = event.detail.first;
    }


    //HHM
    handleAddMemberSearch(event) {
        this.showPublicProviderAddmemberSearch = true;
        this.showPublicProviderAddmember = event.detail.first;
        this.showPublicApplicationTabs = event.detail.first;
        this.showResult = false;
        this.hideHeaderButton = false;
    }

    handleAddMember() {
        this.showPublicProviderAddmember = true;
        this.searchHolder = false;
        this.showPublicApplicationTabs = false;
        this.hideHeaderButton = false;
        this.isView = false;
    }

    handleAddMemberView(event) {
        this.showPublicProviderAddmember = true;
        this.showPublicApplicationTabs = false;
        this.isView = true;
        this.hideHeaderButton = false;
    }
    handleMeeting(event) {
        this.meetingFlag = event.detail.meetingFlag;
    }

    redirectToHousHoldDashboard(event) {
        this.showPublicProviderAddmember = event.detail.first;
        this.activeTab = 'houseHoldMember';
        this.showPublicApplicationTabs = true;
        this.hideHeaderButton = true;
    }
    handleAddMemberSearchFromAddHouse(event) {
        this.showPublicProviderAddmemberSearch = true;
        this.showPublicProviderAddmember = event.detail.first;
        this.showPublicApplicationTabs = event.detail.first;
        this.showResult = true;
        this.hideHeaderButton = false;

    }
    handleAddMemberHome(event) {
        this.showPublicProviderAddmemberSearch = event.detail.first;
        this.activeTab = 'houseHoldMember';
        this.showPublicApplicationTabs = true;
        this.hideHeaderButton = true;
    }
    redirectHandleAddMember(event) {
        this.showPublicProviderAddmemberSearch = event.detail.first;
        this.showListParent = event.detail.searchLists;
        this.showPublicProviderAddmember = true;
        this.searchHolder = true;
        this.hideHeaderButton = false;
    }
    handleComarRegulations(event) {

    }
    handleSessionFlag(event) {
        this.sessionHoursFlag = event.detail.sessionFlag;
    }
    handleHomeStudyCompletedFlag(event) {
        this.homeStudyCompletedFlag = event.detail.homestudycompletedflag;
    }
    handleReferenceCheckFlag(event) {
        this.referenceCheckFlag = event.detail.referencecheckflag;
    }

    redirectToPubAppTabs(event) {
        this.showPublicApplicationDashboard = !event.detail.first;
        this.hideHeaderButton = !event.detail.first;
        this.showPublicApplicationTabs = !event.detail.first;
        this.ApplicationStatus = sharedData.getApplicationStatus();
        this.supervisor();
    }

    redirectToDashboard(event) {
        this.showPublicApplicationDashboard = !event.detail.first;
        this.hideHeaderButton = !event.detail.first;
        this.showPublicApplicationTabs = !event.detail.first;
    }

    //sp hide button
    handleSupervisorButton(event) {
        this.showAssignTraining = event.detail.first;
    }

    handlePrivateAppTabInfo(event) {
        this.showPrivateApplicationTabs = !event.detail.first;
        this.showPublicApplicationDashboard = !event.detail.first
    }
    supervisor() {
        if (this.getUserProfileName != "Supervisor") {
            if (
                [
                    "Caseworker Submitted",
                    "Approved",
                    "Rejected",
                    "Supervisor Rejected"
                ].includes(this.ApplicationStatus)
            ) {
                this.showPetDetails = false;
                this.showReferenceAddBtn = false;
                this.showAddService = false;
                this.showAssignTraining = false;
                this.showAddHomeStudy = false;
            }
        }
        if (this.getUserProfileName == "Supervisor") {
            this.showPetDetails = false;
            this.showReferenceAddBtn = false;
            this.showAddService = false;
            this.showAssignTraining = false;
            this.showAddHomeStudy = false;
        }
    }
    handlePrideFlag(event) {
        this.prideSessionNotFound = event.detail.pridesessionnotfound;
    }
}