/**
 * @Author        : M.Vijayaraj
 * @CreatedOn     : July 06, 2020
 * @Purpose       : This component complaintsReview Js file
 **/

import { LightningElement, wire, track } from "lwc";
import fatchPickListValue from "@salesforce/apex/complaintsReview.getPickListValues";
import saveCaseRecord from "@salesforce/apex/complaintsReview.updateOrInsertSOQL";
import getComplaintsReviewDetails from "@salesforce/apex/complaintsReview.getComplaintsReviewDetails";
import getApplicationProviderStatus from "@salesforce/apex/complaintsReview.getApplicationProviderStatus";
import * as sharedData from 'c/sharedData';
import images from '@salesforce/resourceUrl/images';
import { USER_PROFILE_NAME } from "c/constants";

import { utils } from "c/utils";

export default class ComplaintsReview extends LightningElement {
  @track complaintsReviewValues;
  @track InvestigationStartDate;
  @track InvestigationEndDate;
  @track id;
  @track mindate;
  @track modes = [];
  @track freezeReviewScreen = false;


  reviewIcon = images + '/complaints-reviewIcon.svg';

  value = [];

  get selectedValues() {
    return this.value.join(",");
  }

  CheckboxhandleChange(e) {
    this.value = e.detail.value;

  }

  //   //picklist values fetch from Object
  //   @wire(fatchPickListValue, {
  //     objInfo: {
  //       sobjectType: "Case"
  //     },
  //     picklistFieldApi: "ModesofContact__c"
  //   })
  //   wireduserDetails1({ error, data }) {
  //     /*eslint-disable*/
  // if (data) {
  //       this.complaintsReviewValues = data;
  //     } else if (error) {
  //     }
  //   }

  handleChange(event) {
    if (event.target.name == "InvestigationStartDate") {
      this.InvestigationStartDate = event.detail.value;
      this.mindate = event.detail.value;
    } else if (event.target.name == "InvestigationEndDate") {
      this.InvestigationEndDate = event.target.value;
    }
  }



  //Saving Functionality Start
  saveFunction() {
    if (!this.InvestigationStartDate) {
      return this.dispatchEvent(
        utils.toastMessage(
          `Please Enter Investigation Initiated Date `,
          "warning"
        )
      );

    }
    if (!this.InvestigationEndDate) {
      return this.dispatchEvent(
        utils.toastMessage(
          `Please Enter Investigation Completed Date`,
          "warning"
        )
      );

    }


    if (this.InvestigationStartDate > this.InvestigationEndDate) {
      return this.dispatchEvent(
        utils.toastMessage(
          `Date of Investigation Completed must be same or more than Date of Investigation Initiated`,
          "warning"
        )
      );

    }

    let myobjcase = {
      sobjectType: "Case"
    };
    myobjcase.ModesofContact__c = this.selectedValues;
    myobjcase.InvestigationInitiated__c = this.InvestigationStartDate;
    myobjcase.InvestigationCompleted__c = this.InvestigationEndDate;
    myobjcase.Id = this.caseId;

    saveCaseRecord({
      objSobjecttoUpdateOrInsert: myobjcase
    })
      .then((result) => {
        this.dispatchEvent(
          utils.toastMessage("Complaint Review Saved Successfully", "success")
        );
      })
      .catch((errors) => {
        return this.dispatchEvent(utils.handleError(errors));
      });
  }
  //Saving Functionality End

  //Get data fetching using ConnectedCallback

  connectedCallback() {
    this.review();
    this.statusWiseReview();
    this.picklist();


  }
  review() {
    getComplaintsReviewDetails({
      reviewDetails: this.caseId
    })
      .then((result) => {
        if (result.length > 0) {

          this.InvestigationStartDate = result[0].InvestigationInitiated__c;
          this.InvestigationEndDate = result[0].InvestigationCompleted__c;
          this.modes = result[0].ModesofContact__c;
          this.value.push(result[0].ModesofContact__c);
          this.id = result[0].Id;
        }

      })
      .catch((errors) => {
        return this.dispatchEvent(utils.handleError(errors));
      });
  }



  //Status wise screen freeze
  statusWiseReview() {
    getApplicationProviderStatus({
      statusDetails: this.caseId
    })
      .then((data) => {

        if (
          data.length > 0 &&
          (data[0].Status ==
            'Awaiting Confirmation' ||
            data[0].Status ==
            'Accepted' ||
            data[0].Status ==
            'Disputed' ||
            data[0].Status ==
            'Submitted to Supervisor' || data[0].Status ==
            'Approved' || data[0].Status ==
            'Rejected' || this.userProfileName == USER_PROFILE_NAME.USER_SUPERVISOR)
        ) {
          this.freezeReviewScreen = true;

        } else {
          this.freezeReviewScreen = false;

        }
      })
      .catch((errors) => {
        return this.dispatchEvent(utils.handleError(errors));
      });
  }


  //Get Picklist values using connectedCallback
  picklist() {
    fatchPickListValue({
      objInfo: {
        sobjectType: 'Case'
      },
      picklistFieldApi: 'ModesofContact__c'
    })
      .then((data) => {
        this.complaintsReviewValues = data;
      })
      .catch((errors) => {
        return this.dispatchEvent(utils.handleError(errors));
      });
  }



  get caseId() {
    return sharedData.getCaseId();
    //return "5000w000001jy5TAAQ";
  }

  get userProfileName() {
    return sharedData.getUserProfileName();

  }
}
