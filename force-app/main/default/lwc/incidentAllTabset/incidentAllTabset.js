import { LightningElement } from 'lwc';
import getCaseHeaderDetails from "@salesforce/apex/ComplaintsReferral.getCaseHeaderDetails";

export default class IncidentAllTabset extends LightningElement {
    showIncidentDashboard = true;
    showIncidentTabset = false;
    CaseNumber;
    activeTabValue;
    supervisorFlow = false;
    //Staff Details
    showStaffDetailsBtn = false;
    showAddStaffBtn = false;
    enableStaffDetails = false;

    //persons
    showPersonAddComp = false;
    ShowListPerson = true;
    showPersonKidButton = false;
    //Flag for Decision
    personkidDecisonFlag;
    wittnessDecisionFlag;
    staffDecisionFlag;

    //witness
    openWitnessButton = false;
    showWitnessButton = false;


    //activetab
    activeTab = 'Program'
    //Depending Tab show Btn Functions
    handleActiveTabButtons(event) {
        if (event.target.value === undefined || this.supervisorFlow === true) return;
        this.activeTabValue = event.target.value;
        switch (this.activeTabValue) {
            case 'staffDetails':
                this.showStaffDetailsBtn = true;
                this.showAddStaffBtn = true;
                this.enableStaffDetails = true;
                this.showPersonKidButton = false;
                this.showWitnessButton = false;
                break;
            case 'Person':
                this.showPersonKidButton = true;
                this.showStaffDetailsBtn = false;
                this.showAddStaffBtn = false;
                this.showWitnessButton = false;
                this.enableStaffDetails = false;
                break;
            case 'Witness':
                this.showPersonKidButton = false;
                this.showStaffDetailsBtn = false;
                this.showWitnessButton = true;
                this.showAddStaffBtn = false;
                this.enableStaffDetails = false;
                break;
            default:
                this.showStaffDetailsBtn = false;
                this.showAddStaffBtn = false;
                this.enableStaffDetails = false;
                this.showPersonKidButton = false;
                this.showWitnessButton = false;
        }
    }

    handleRedirectToTabset(event) {
        this.showIncidentDashboard = event.detail.first;
        let allBtnShow = event.detail.btnStatus;
        this.supervisorFlow = allBtnShow;
        this.showIncidentTabset = true;
        this.ShowListPerson = true;
        this.showPersonAddComp = false;
    }

    //person tab redirects
    redirectToAddPerson(e) {
        this.showPersonAddComp = e.detail.first;
        this.ShowListPerson = false;
        this.showIncidentTabset = false;
        this.showIncidentDashboard = false;
    }
    redirectBackToListofPerson(e) {
        this.showPersonAddComp = e.detail.first;
        this.ShowListPerson = true;
        this.showIncidentTabset = true;
        this.showIncidentDashboard = false;
        this.activeTab = 'Person';
    }

    editViewPersonKidIncident(e) {
        this.showPersonAddComp = e.detail.first;
        this.ShowListPerson = false;
        this.showIncidentTabset = false;
        this.showIncidentDashboard = false;
    }
    handlePersonkidDecisonflag(e) {
        this.personkidDecisonFlag = e.detail.PersonKidDataflag;
    }
    handlewittnessDecisionFlag(e) {
        this.wittnessDecisionFlag = e.detail.WitnessDataflag;
    }
    flagFrStaffDetailCount(event) {
        this.staffDecisionFlag = event.detail.count;
    }
    redirectBackToDashboard(event){
        this.showPersonAddComp = event.detail.first;
        this.showIncidentDashboard = !event.detail.first;
        this.showIncidentTabset = false;
        this.activeTab = 'Program';
    }

    //Staff Details
    enableStaffDetailBtn(event) {
        this.enableStaffDetails = event.detail.first
    }

    //Redirect back to dashboard
    redirectToDashboard(event) {
        this.showIncidentDashboard = !event.detail.first;
        this.showIncidentTabset = false;
    }


    clickbuttonWitness(event) {
        this.openWitnessButton = event.detail.first;
    }

    witnessPopClose(event) {
        this.openWitnessButton = event.detail.first;
    }

    handleHeaderCaseId(event){
        let caseId = event.detail.first;
        getCaseHeaderDetails({
            newreferral: caseId,
            model: 'Case'
        }).then((result) => {
            if (result != undefined && result.length > 0) {
                this.CaseNumber = result[0].CaseNumber;
            }
            }) .catch((errors) => {
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const {
                        title,
                        message,
                        errorType
                    } = error;
                    this.dispatchEvent(
                        utils.toastMessageWithTitle(title, message, errorType)
                    );
                } else {
                    this.dispatchEvent(
                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                    );
                }
            });
        }

}