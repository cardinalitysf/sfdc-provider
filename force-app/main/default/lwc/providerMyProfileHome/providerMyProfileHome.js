/**
 * @Author        : Preethi Bojarajan
 * @CreatedOn     : March 18, 2020
 * @Purpose       : My Profile Component Combined with Header
 * @updatedBy     : Preethi Bojarajan
 * @updatedOn     : March 25, 2020
 **/

import {
    LightningElement,
    track,
    wire
} from 'lwc';
import getProviderId from '@salesforce/apex/providerApplicationHeader.getProviderId';
import getProgramTypeList from "@salesforce/apex/cpaHomeDashboard.getProgramTypeList";
import getUserEmail from '@salesforce/apex/providerApplication.getUserEmail';
import * as sharedData from "c/sharedData";
import {
    utils
} from 'c/utils';
export default class ProviderMyProfileHome extends LightningElement {
    //For Header Show
    @track providerName = null;
    @track providerTabView = true;
    @track activeTab = "Corporate Information";
    @track capDetailsFromInvestigation;
    detailofCpa;
    addressHouseholdView = false;
    CPAHomeShowHide = false;
    clearanceView = false;
    cpaHomeId;
    providerId;

    connectedCallback(){
        getUserEmail()
        .then(res=>{
            getProviderId({Id: res[0].Id})
            .then(res=> {
                this.providerId = res[0].Id;
                this.providerName = res[0].Name;
                sharedData.setProviderId(this.providerId);
                getProgramTypeList({
                    ProviderId: this.providerId
                }).then(result => {
                    if (result.length > 0) {
                        this.CPAHomeShowHide = true;
                    }
                    else {
                        this.CPAHomeShowHide = false;
                    }
                }).catch(errors => {
                    return this.dispatchEvent(utils.handleError(errors));
                });
            }).catch(errors => {
                return this.dispatchEvent(utils.handleError(errors));
            });
        })
        .catch(errors=>{
            return this.dispatchEvent(utils.handleError(errors));
        })
    }

    //This function used to redirect from investigation dashboard to detail page
    handleRedirectionFromInvestigationDashboard(event) {
        this.providerTabView = event.detail.isView;
        this.capDetailsFromInvestigation = event.detail.capDetails;
    }

    //This function used to redirect from investigation detail page to dashboard
    handleRedirectionFromInvestigationDetailPage(event) {
        this.providerTabView = event.detail.isView;
        this.activeTab = "Investigation Findings";
    }
    //cpa program type redirection
    handleRedirecttocpaHome(event) {
        this.addressHouseholdView = true;
        this.providerTabView = false;
        this.clearanceView = false;
        this.detailofCpa = JSON.stringify(event.detail);
        
        
    }



    //cpa clearance redirection
    redirecttoclearance(event) {
        this.clearanceView = true;
        this.providerTabView = false;
        this.cpaHomeId = event.detail.rowActionValue.CPAId;
    }

    handleBackToCpa(event) {
        this.providerTabView = true;
        this.CPAHomeShowHide = event.detail.first;
        this.activeTab = "CPA Home";
    }

    //Sundar added this method
    handleRedirectToCpaHomeDashboard(event) {
        this.providerTabView = true;
        this.CPAHomeShowHide = event.detail.first;
        this.clearanceView = false;
        this.activeTab = "CPA Home";
    }
}