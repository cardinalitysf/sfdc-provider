/**
 * @Author        : Naveen
 * @CreatedOn     : June 01,2020
 * @Purpose       : Add BackGround and Criminal Checklist 
 * @updatedBy     : Naveen
 * @updatedOn     : 
 **/

import { LightningElement, track, wire, api } from 'lwc';
// Utils
import { utils } from 'c/utils';
import * as sharedData from "c/sharedData";
import { loadStyle } from 'lightning/platformResourceLoader';
import { refreshApex } from "@salesforce/apex";
import myResource from '@salesforce/resourceUrl/styleSheet';
import images from "@salesforce/resourceUrl/images";
import getQuestionsByType from '@salesforce/apex/PublicProvidersBackgroundCheck.getQuestionsByType';
import getRecordType from "@salesforce/apex/PublicProvidersBackgroundCheck.getRecordType";
import getRecordDetails from '@salesforce/apex/PublicProvidersBackgroundCheck.getRecordDetails';
import bulkAddRecordAns from "@salesforce/apex/PublicProvidersBackgroundCheck.bulkAddRecordAns";
import bulkUpdateRecordAns from "@salesforce/apex/PublicProvidersBackgroundCheck.bulkUpdateRecordAns";
import getHouseholdMembers from "@salesforce/apex/PublicProvidersBackgroundCheck.getHouseholdMembers";
import { createRecord, updateRecord, deleteRecord } from "lightning/uiRecordApi";
import { CurrentPageReference } from 'lightning/navigation';
import { fireEvent } from 'c/pubsub';

import editRecordDet from "@salesforce/apex/PublicProvidersBackgroundCheck.editRecordDet";
import getPickListValues from "@salesforce/apex/PublicProvidersBackgroundCheck.getPickListValues";
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getUploadedDocuments from '@salesforce/apex/PublicProvidersBackgroundCheck.getUploadedDocuments';
import updateActorFieldInContentversion from "@salesforce/apex/PublicProvidersBackgroundCheck.updateActorFieldInContentversion";

/**
 Fileds Set
 */
import PROVIDERRECQUES_OBJECT from '@salesforce/schema/ProviderRecordQuestion__c';
import RECONSIDERATION_FIELD from '@salesforce/schema/ProviderRecordQuestion__c.Reconsideration__c';
import ACTORQUES_FIELD from '@salesforce/schema/ProviderRecordQuestion__c.Actor__c';
import STATUS_FIELD from '@salesforce/schema/ProviderRecordQuestion__c.Status__c';
import RECTYPEQUES_FIELD from '@salesforce/schema/ProviderRecordQuestion__c.RecordTypeId';
import DATEQUES_FIELD from '@salesforce/schema/ProviderRecordQuestion__c.Date__c';
import IDQUES_FIELD from '@salesforce/schema/ProviderRecordQuestion__c.Id';
/* Answer */
import { CJAMS_CONSTANTS } from 'c/constants';


/* Referrence Value */

import REFVALUE_OBJECT from '@salesforce/schema/ReferenceValue__c';
import QUESTION_ORDER_FIELD from '@salesforce/schema/ReferenceValue__c.QuestionOrder__c';

import TYPE_FIELD from '@salesforce/schema/ReferenceValue__c.Type__c';
import REFKEY_FIELD from '@salesforce/schema/ReferenceValue__c.RefKey__c';

export default class PublicProvidersBackgroundCheck extends LightningElement {
    @track hasBackgroundAddBtn = true;
    @track hasHouseHoldCreate = true;
    @track hasHouseHoldEnable = true;
    @track recordNewModel = false;
    @track hasSelectBackgroundType = false;
    @track hasChecklistQuestion = false;
    @track hastypeselected = false;
    @track questionsBackgroundChecklist;
    @track questions;
    @track recordTypeId = null;
    @track type = 'Personal Security';
    @track datas = [];
    @track updateMode = false;
    wiredRecordDetails;
    uploadRecordDetails;
    @track selectHouseholdMemberOptions;
    @track hasactorid;
    @track editedQuestions = [];
    @track editRecordId;
    @track updateCriminalcheck = false;
    @track pickDisable = true;
    @track wiredEditRecordsdatas;
    @track editRecordIdPersonal;
    @track stateValues;
    @track hasOutofStateVisible = true;
    @track dataTableShow = true;
    @track outOfStateRefKey = 'Out of State';
    @track referrenceCOMARType = 'Personnel Security Checks';
    @track headerPersonalSecurityStatus = 'Incomplete';
    @track headerCriminalStatus = 'Incomplete';
    @track colorPersonalSecurityStatus = 'redSpan';
    @track colorCriminalStatus = 'redSpan';
    @track hasApprovalDate = false;
    @track outOfStateQuestionOrder;
    @track ApplicationStatus;
    @track AssignbtnDisable = false;
    @track backgroundcheck = [];
    @track finalUploadResult;
    @track btnLabel = 'Next';



    // Box click to change image normal to highlight Declaration Start

    pscIcon = images + "/pscIcon.svg";
    chIcon = images + "/chIcon.svg";
    pscIconhightLight = images + "/pscIcon-hightlight.svg";
    chIconhightLight = images + "/chIcon-highlight.svg";
    serviceIcon = images + "/bgchkIcon.svg";
    // Box click to change image normal to highlight Declaration End

    get applicationId() {
        return sharedData.getApplicationId();
        //return 'a0A0p000001AO6CEAW';
    }
    get getProviderId() {
        return sharedData.getProviderId();
        // return '0010w00000AhqWxAAJ';
    }

    get getApplicationStatus() {
        return sharedData.getApplicationStatus();
    }
    get getUserProfileName() {
        return sharedData.getUserProfileName();
    }

    get reconsiderationId() {
        return sharedData.getReconsiderationId();
    }


    //DataTable RowAction Method Start
    constructor() {
        super();
        this.columns = [{
            label: 'HouseHold Member Name',
            fieldName: 'HouseholdName',
            cellAttributes: {
                class: "fontclrGrey"
            },
            type: "button",
            typeAttributes: {
                label: {
                    fieldName: "HouseholdName"
                },
                target: "_self"
            }

        },
        {
            label: 'TYPE',
            fieldName: 'Type'
        },
        {
            label: 'PID',
            fieldName: 'PID'
        },
        {
            label: 'AGE',
            fieldName: 'Age'
        },
        {
            label: 'GENDER',
            fieldName: 'Gender'

        },
        {
            label: 'PERSON SECURITY CHECKS',
            fieldName: 'securityStatus',
            cellAttributes: {
                class: {
                    fieldName: "personalStatusClass"
                }
            }

        },
        {
            label: 'CRIMINAL HISTORY',
            fieldName: 'criminalStatus',
            cellAttributes: {
                class: {
                    fieldName: "criminalStatusClass"
                }
            }


        }

        ]
    }
    @wire(CurrentPageReference) pageRef;

    /* Style Sheet Loading */
    renderedCallback() {
        Promise.all([
            loadStyle(this, myResource + '/styleSheet.css')
        ])

    }

    connectedCallback() {
        this.getContactDetails();
        this.dataTableShow = true;


        this.ApplicationStatus = this.getApplicationStatus;

        if (this.getUserProfileName != "Supervisor") {
            if (
                [
                    "Caseworker Submitted",
                    "Rejected",
                    "Supervisor Rejected"
                ].includes(this.ApplicationStatus)
            ) {
                this.AssignbtnDisable = true;
            } else {
                this.AssignbtnDisable = false;
            }
        }
        if (this.getUserProfileName == "Supervisor") {
            if (["Supervisor Rejected"].includes(this.ApplicationStatus)) {
                this.AssignbtnDisable = true;
            }
        }



    }

    //fetch data from contact obj and show in home.
    getContactDetails() {
        getHouseholdMembers({
            providerid: this.getProviderId
        }).then((data) => {

            if (data.length > 0) {
                var selectHouseholdOptions = [];
                data.forEach((row) => {
                    selectHouseholdOptions.push({

                        label: row.Contact__r.FirstName__c + ' ' + row.Contact__r.LastName__c + "\n" + ' - ' + row.Role__c,
                        value: row.Id
                    });
                });
                this.selectHouseholdMemberOptions = selectHouseholdOptions;
            }
        })
            .catch(errors => {
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const { title, message, errorType } = error;
                    this.dispatchEvent(
                        utils.toastMessageWithTitle(title, message, errorType)
                    );
                } else {
                    this.dispatchEvent(
                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                    );
                }
            });
    }


    addOnHouseHoldOnClick(event) {

        this.hasBackgroundAddBtn = false;
        this.recordNewModel = true;
        this.hasSelectBackgroundType = true;
        this.hasChecklistQuestion = true;
        this.updateCriminalcheck = false;
        this.btnLabel = 'Next';
        this.hastypeselected = 'Personnel Security Checks';
        this.type = 'Personal Security';
        this.headerPersonalSecurityStatus = 'Incomplete';
        this.headerCriminalStatus = 'Incomplete';
        this.colorCriminalStatus = 'redSpan';
        this.colorPersonalSecurityStatus = 'redSpan';
        this.boxbordercolorchangeforSecurity();
        this.getActorbasedFileDetailsfetch();
    }



    //   radio button value
    get radioOptions() {

        return [{
            label: "Yes",
            value: "Yes"
        },
        {
            label: "No",
            value: "No" // change  NO to No
        }
        ];
    }

    @wire(getPickListValues, {
        objInfo: {
            sobjectType: "ProviderRecordAnswer__c"
        },
        picklistFieldApi: "State__c"
    })
    wireCheckPicklistCandinateAttend({
        error,
        data
    }) {
        if (data) {
            this.stateValues = data;
        } else if (error) { }
    }

    borderChangeDefalut() {

        this.template.querySelector(".box-col1").classList.add("boxbordercolorchange");
        this.template.querySelector(".box-col3").classList.remove("boxbordercolorchange");
    }

    householdSelectOnChange(event) {
        this.hasactorid = event.target.value;
        this.pickDisable = false;
    }

    boxbordercolorchangeforSecurity() {

        if (this.updateCriminalcheck == false) {

            this.template.querySelector(".box-col1").classList.add("boxbordercolorchange");
            this.template.querySelector(".box-col3").classList.remove("boxbordercolorchange");
            this.hastypeselected = 'Personnel Security Checks';
            this.type = 'Personal Security';

        } else {

            this.editRecordId = this.editRecordIdPersonal;
            refreshApex(this.wiredEditRecordsdatas);
            this.template.querySelector(".box-col1").classList.add("boxbordercolorchange");
            this.template.querySelector(".box-col3").classList.remove("boxbordercolorchange");

            if (this.headerPersonalSecurityStatus == 'Completed') {

                this.AssignbtnDisable = true;
            } else {
                this.AssignbtnDisable = false;
            }


        }

    }

    boxbordercolorchangeForCriminal() {

        if (this.updateCriminalcheck == false) {

            this.hasOutofStateVisible = false;
            this.template.querySelector(".box-col1").classList.remove("boxbordercolorchange");
            this.template.querySelector(".box-col3").classList.add("boxbordercolorchange");
            this.hastypeselected = 'Criminal History';
            this.type = 'Criminal History';
            this.btnLabel = 'Save';

        } else {
            if (this.headerCriminalStatus == 'Completed') {

                this.AssignbtnDisable = true;
            } else {
                this.AssignbtnDisable = false;
            }
            this.btnLabel = 'Update';
            this.hasOutofStateVisible = false;
            this.editRecordId = this.editRecordIdCriminal;
            refreshApex(this.wiredEditRecordsdatas);
            this.template.querySelector(".box-col3").classList.add("boxbordercolorchange");
            this.template.querySelector(".box-col1").classList.remove("boxbordercolorchange");

        }
    }

    cancelOnClick() {
        this.hasBackgroundAddBtn = true;
        this.dataTableShow = true;

        this.recordNewModel = false;
        this.hasSelectBackgroundType = false;
        this.hasChecklistQuestion = false;
        this.updateCriminalcheck = true;
        // this.recordDetails();

    }


    @wire(getRecordType, {
        name: '$type'
    })
    recordTypeDetails
        ({
            data,
            error
        }) {
        if (data) {
            this.recordTypeId = data;
        }
    }

    _finalQuestionAnswerToDB;
    @wire(getQuestionsByType, {
        type: '$hastypeselected'
    })
    wiredquestions(data) {
        try {

            if (data.data != null && data.data != '') {
                let _rowNumber = 1;
                this.questions = [];


                let _backGroundCheckQuestions = JSON.parse(JSON.stringify(data.data));

                this.questionsBackgroundChecklist = _backGroundCheckQuestions.map((row) => {
                    return Object.assign({
                        HouseholdStatus__c: null,
                        Date__c: utils.formatDateYYYYMMDD(new Date()),
                        Comments__c: null,
                        State__c: null,
                        Comar__c: row.Id,
                        Dependent__c: row.FieldType__c ? 'Yes' : 'No'


                    }, row)
                });

                for (let row in _backGroundCheckQuestions) {
                    _backGroundCheckQuestions[parseInt(row)].Comar__c = _rowNumber;
                    _backGroundCheckQuestions[parseInt(row)].HouseholdStatus__c = null;
                    _backGroundCheckQuestions[parseInt(row)].Date__c = utils.formatDateYYYYMMDD(new Date());
                    _backGroundCheckQuestions[parseInt(row)].Comments__c = null;
                    _backGroundCheckQuestions[parseInt(row)].State__c = null;



                    if (_backGroundCheckQuestions[row].FieldType__c != undefined && _backGroundCheckQuestions[row].FieldType__c == 'Dependent') {
                        _backGroundCheckQuestions[parseInt(row)].HaveDependantQues = true;
                        _backGroundCheckQuestions[parseInt(row)].SubId = _backGroundCheckQuestions[parseInt(row) + 1].Id;
                        _backGroundCheckQuestions[parseInt(row)].SubChecklistId = _backGroundCheckQuestions[parseInt(row) + 1].Id;
                        _backGroundCheckQuestions[parseInt(row)].SubRefKey__c = _backGroundCheckQuestions[parseInt(row) + 1].RefKey__c;
                        _backGroundCheckQuestions[parseInt(row)].SubQuestion__c = _backGroundCheckQuestions[parseInt(row) + 1].Question__c;
                        _backGroundCheckQuestions[parseInt(row)].SubQuestionOrder__c = _backGroundCheckQuestions[parseInt(row) + 1].QuestionOrder__c;
                        _backGroundCheckQuestions.splice(parseInt(row) + 1, 1);

                        if (_backGroundCheckQuestions[parseInt(row)].Question__c == 'Is there CPS History') {
                            _backGroundCheckQuestions[parseInt(row)].hasApprovalDate = true;
                        }

                    }
                    _rowNumber++;
                }
                this.questions = _backGroundCheckQuestions;
                this.outOfStateQuestionOrder = this.questions[this.questions.length - 1].QuestionOrder__c + 1;

            }
        }
        catch (error) {
            if (error) {
                let error = JSON.parse(error.body.message);
                const { title, message, errorType } = error;
                this.dispatchEvent(
                    utils.toastMessageWithTitle(title, message, errorType)
                )
            }
            else {
                this.dispatchEvent(
                    utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                );
            }
        }
    }

    /* DataTable  From Question*/




    @wire(getRecordDetails, {
        reconsiderationId: '$reconsiderationId'

    })

    recordDetails(data) {
        try {

            this.wiredRecordDetails = data;
            if (data) {
                data = JSON.parse(data.data);
                if (data != null && data != '') {
                    this.dataTableShow = true;
                    let currentData = [];
                    this.backgroundcheck = data.map((row) => {
                        return {
                            Type: row.actorRole,
                            HouseholdName: row.actorFirstName + ' ' + row.actorLastName,
                            Gender: row.actorGender,
                            Age: row.actorAge,
                            PID: row.actorPID,
                            securityStatus: row.personalHistoryBackGroundType,
                            criminalStatus: row.criminalBackGroundType,
                            personalHistoryBackGroundTypeId: row.personalHistoryBackGroundTypeId,
                            criminalBackGroundTypeId: row.criminalBackGroundTypeId,
                            actorId: row.actorId,
                            personalStatusClass: (row.personalHistoryBackGroundType == 'Completed') ? 'Approved' : (row.personalHistoryBackGroundType == 'Incomplete') ? 'Incomplete' : row.personalHistoryBackGroundType,
                            criminalStatusClass: (row.criminalBackGroundType == 'Completed') ? 'Approved' : (row.criminalBackGroundType == 'Incomplete') ? 'Incomplete' : row.criminalBackGroundType,

                        }

                    });


                } else {
                    this.dataTableShow = false;
                }
            }
        }
        catch (ex) { }
    }



    nextCriminalchange() {

        if (this.questions[0].Type__c == 'Criminal History') {

            this.hasBackgroundAddBtn = true;
            this.dataTableShow = true;

            this.recordNewModel = false;
            this.hasSelectBackgroundType = false;
            this.hasChecklistQuestion = false;
            this.updateCriminalcheck = true;

        } else {

            this.template.querySelector(".box-col1").classList.remove("boxbordercolorchange");
            this.template.querySelector(".box-col3").classList.add("boxbordercolorchange");
            this.hastypeselected = 'Criminal History';
            this.type = 'Criminal History';
            this.boxbordercolorchangeForCriminal();
        }



    }


    /* Create New Record Function Start */
    _questionAndAnswerToSaveInDB = [];
    handleQuestion(event) {
        try {
            this.questionsBackgroundChecklist.find(v => v.Id == event.target.name).HouseholdStatus__c = event.target.value;

        }
        catch (error) {
            if (error) {
                let error = JSON.parse(error.body.message);
                const { title, message, errorType } = error;
                this.dispatchEvent(
                    utils.toastMessageWithTitle(title, message, errorType)
                )
            }
            else {
                this.dispatchEvent(
                    utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                );
            }
        }
    }

    handleClearanceDate(event) {
        try {
            this.questionsBackgroundChecklist.find(v => v.Id == event.target.name).Date__c = event.detail.value;
        }
        catch (error) {
            if (error) {
                let error = JSON.parse(error.body.message);
                const { title, message, errorType } = error;
                this.dispatchEvent(
                    utils.toastMessageWithTitle(title, message, errorType)
                )
            }
            else {
                this.dispatchEvent(
                    utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                );
            }
        }

    }
    handleNarattive(event) {
        try {
            this.questionsBackgroundChecklist.find(v => v.Id == event.target.name).Comments__c = event.detail.value;
        }
        catch (error) {
            if (error) {
                let error = JSON.parse(error.body.message);
                const { title, message, errorType } = error;
                this.dispatchEvent(
                    utils.toastMessageWithTitle(title, message, errorType)
                )
            }
            else {
                this.dispatchEvent(
                    utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                );
            }
        }

    }

    handleStates(event) {
        try {
            this.questionsBackgroundChecklist.find(v => v.Id == event.target.name).State__c = event.target.value;
        }
        catch (error) {
            if (error) {
                let error = JSON.parse(error.body.message);
                const { title, message, errorType } = error;
                this.dispatchEvent(
                    utils.toastMessageWithTitle(title, message, errorType)
                )
            }
            else {
                this.dispatchEvent(
                    utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                );
            }
        }

    }

    createNewRecord(event) {

        let status = event.target.name == 'draft' ? 'Incomplete' : 'Completed';
        if (this.updateMode == false) {
            let fields = {};
            fields[ACTORQUES_FIELD.fieldApiName] = this.hasactorid;
            fields[RECONSIDERATION_FIELD.fieldApiName] = this.reconsiderationId;
            fields[STATUS_FIELD.fieldApiName] = status;
            fields[RECTYPEQUES_FIELD.fieldApiName] = this.recordTypeId;

            const recordInput = {
                apiName: PROVIDERRECQUES_OBJECT.objectApiName,
                fields
            };

            createRecord(recordInput)

                .then(result => {
                    if (result.id) {

                        bulkAddRecordAns({

                            datas: JSON.stringify(this.questionsBackgroundChecklist),
                            questionId: result.id

                        })
                            .then(result => {
                                fireEvent(this.pageRef, "RefreshActivityPage", true);

                                this.dispatchEvent(utils.toastMessage("Background Check saved successfully!..", "success"));
                                refreshApex(this.wiredRecordDetails);
                                this.nextCriminalchange();
                                if (this.updateCriminalcheck == false) {
                                    this.btnLabel = 'Save';
                                }
                                else {
                                    this.btnLabel = 'Update';
                                }

                            })
                            .catch(errors => {
                                if (errors) {
                                    let error = JSON.parse(errors.body.message);
                                    const { title, message, errorType } = error;
                                    this.dispatchEvent(
                                        utils.toastMessageWithTitle(title, message, errorType)
                                    );
                                } else {
                                    this.dispatchEvent(
                                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                                    );
                                }
                            });

                    }
                })
        } else {

            let fields = {};
            fields[IDQUES_FIELD.fieldApiName] = this.editRecordId;
            fields[STATUS_FIELD.fieldApiName] = status;

            const recordInput = {
                fields
            };

            updateRecord(recordInput)
                .then(result => {

                    bulkUpdateRecordAns({
                        datas: JSON.stringify(this.questionsBackgroundChecklist),
                    })
                        .then(result => {
                            this.dispatchEvent(utils.toastMessage("Background Check  Updated Successfully!..", "success"));
                            refreshApex(this.wiredRecordDetails);
                            this.nextCriminalchange();
                            if (this.updateCriminalcheck == false) {
                                this.btnLabel = 'Save';
                            }
                            else {
                                this.btnLabel = 'Update';
                            }

                        });
                })
                .catch(errors => {
                    if (errors) {
                        let error = JSON.parse(errors.body.message);
                        const { title, message, errorType } = error;
                        this.dispatchEvent(
                            utils.toastMessageWithTitle(title, message, errorType)
                        );
                    } else {
                        this.dispatchEvent(
                            utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                        );
                    }
                });

        }

    }
    /* Create New Record Function End */

    handleRowAction(event) {

        this.editRecordId = event.detail.row.personalHistoryBackGroundTypeId;

        this.hasactorid = event.detail.row.actorId;
        this.updateCriminalcheck = true;
        this.editRecordIdCriminal = event.detail.row.criminalBackGroundTypeId;
        this.editRecordIdPersonal = this.editRecordId;

        this.hasBackgroundAddBtn = false;
        this.recordNewModel = true;
        this.hasSelectBackgroundType = true;
        this.hasChecklistQuestion = true;
        this.updateMode = true;
        this.btnLabel = 'Update';
        this.headerPersonalSecurityStatus = event.detail.row.securityStatus;
        this.headerCriminalStatus = event.detail.row.criminalStatus;


        if (this.headerPersonalSecurityStatus == 'Completed') {
            this.colorPersonalSecurityStatus = 'greenSpan';
            this.AssignbtnDisable = true;
        } else {
            this.colorPersonalSecurityStatus = 'redSpan';
            this.AssignbtnDisable = false;
        }

        if (this.headerCriminalStatus == 'Completed') {
            this.colorCriminalStatus = 'greenSpan';
            this.AssignbtnDisable = true;
        } else {
            this.colorCriminalStatus = 'redSpan'
            this.AssignbtnDisable = false;
        }
        this.getActorbasedFileDetailsfetch();
    }


    //Edit Record Details Function Datatable Start
    @wire(editRecordDet, {
        questionId: '$editRecordId'
    })

    wiredEditRecords(result) {
        try {

            this.wiredEditRecordsdatas = result;

            if (result.data != undefined && result.data.length > 0) {
                //   this.recordNewModel = true;
                let currentData = [];
                let i = 1;
                let _rowNumber = 1;

                let _backGroundCheckQuestions = JSON.parse(JSON.stringify(result.data));

                this.questionsBackgroundChecklist = _backGroundCheckQuestions;

                for (let row in _backGroundCheckQuestions) {

                    _backGroundCheckQuestions[parseInt(row)].Question__c = _backGroundCheckQuestions[row].Comar__r.Question__c;
                    _backGroundCheckQuestions[parseInt(row)].ProviderRecordQuestion__c = _backGroundCheckQuestions[row].ProviderRecordQuestion__c
                    _backGroundCheckQuestions[parseInt(row)].Comar__c = _rowNumber;
                    _backGroundCheckQuestions[parseInt(row)].RefKey__c = _backGroundCheckQuestions[row].Comar__r.RefKey__c;
                    _backGroundCheckQuestions[parseInt(row)].Type__c = _backGroundCheckQuestions[row].Comar__r.Type__c;

                    if (_backGroundCheckQuestions[row].Dependent__c != undefined && _backGroundCheckQuestions[row].Dependent__c == 'Yes') {
                        _backGroundCheckQuestions[parseInt(row)].HaveDependantQues = true;
                        _backGroundCheckQuestions[parseInt(row)].SubId = _backGroundCheckQuestions[parseInt(row) + 1].Id;
                        _backGroundCheckQuestions[parseInt(row)].SubChecklistId = _backGroundCheckQuestions[parseInt(row) + 1].Id;
                        _backGroundCheckQuestions[parseInt(row)].SubRefKey__c = _backGroundCheckQuestions[parseInt(row) + 1].Comar__r.RefKey__c;
                        _backGroundCheckQuestions[parseInt(row)].SubQuestion__c = _backGroundCheckQuestions[parseInt(row) + 1].Comar__r.Question__c;
                        _backGroundCheckQuestions[parseInt(row)].SubQuestionOrder__c = _backGroundCheckQuestions[parseInt(row) + 1].QuestionOrder__c;


                        _backGroundCheckQuestions.splice(parseInt(row) + 1, 1);

                        if (_backGroundCheckQuestions[parseInt(row)].Question__c == 'Is there CPS History') {
                            _backGroundCheckQuestions[parseInt(row)].hasApprovalDate = true;
                        }
                    }

                    _rowNumber++;
                }
                this.questions = _backGroundCheckQuestions;

            }
        }
        catch (error) {
            if (error) {
                let error = JSON.parse(error.body.message);
                const { title, message, errorType } = error;
                this.dispatchEvent(
                    utils.toastMessageWithTitle(title, message, errorType)
                )
            }
            else {
                this.dispatchEvent(
                    utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                );
            }
        }
    }

    addNewOutOfState(event) {
        let fields = {};

        fields[TYPE_FIELD.fieldApiName] = this.referrenceCOMARType;
        fields[REFKEY_FIELD.fieldApiName] = this.outOfStateRefKey;
        fields[QUESTION_ORDER_FIELD.fieldApiName] = this.outOfStateQuestionOrder;


        const recordInput = {
            apiName: REFVALUE_OBJECT.objectApiName,
            fields
        };

        createRecord(recordInput)
            .then(result => {
                this.dispatchEvent(utils.toastMessage("Records Has Been Saved Successfully!..", "success"));
            })
            .catch(errors => {
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const { title, message, errorType } = error;
                    this.dispatchEvent(
                        utils.toastMessageWithTitle(title, message, errorType)
                    );
                } else {
                    this.dispatchEvent(
                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                    );
                }
            });
    }

    /* File Upload New Method Start */

    @api
    recordId = this.reconsiderationId;
    // accepted parameters
    get acceptedFormats() {
        return [".pdf", ".png", ".jpg", ".jpeg"];
    }
    handleUploadFinished(event) {
        let strFileNames = "";
        // Get the list of uploaded files
        const uploadedFiles = event.detail.files;
        // COmbine all the document ID's as comma seperated
        let _documentID = uploadedFiles
            .map(({ documentId }) => documentId)
            .join(",");

        //Update Actor for the corresponding comma seperated document ID's by calling Apex
        updateActorFieldInContentversion({
            strDocumentIds: _documentID,
            strActor: this.hasactorid
        })
            .then(result => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: "Success!!",
                        message: strFileNames + " Files uploaded Successfully!!!",
                        variant: "success"
                    })
                );
            })
            .catch(errors => {
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const { title, message, errorType } = error;
                    this.dispatchEvent(
                        utils.toastMessageWithTitle(title, message, errorType)
                    );
                } else {
                    this.dispatchEvent(
                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                    );
                }
            });

        refreshApex(this.uploadRecordDetails);
    }

    @wire(getUploadedDocuments, {
        strDocumentID: "$reconsiderationId"

    })
    selectFromfileupload(data) {
        try {
            this.uploadRecordDetails = data;
            if (data) {
                if (data.data != null && data.data != "") {
                    this.finalUploadResult = data.data;
                    let Actorrid = this.hasactorid;
                    let arrayFileUpload = [];
                    this.finalUploadResult.forEach((element) => {
                        if (element.Actor__c == Actorrid) {
                            arrayFileUpload.push({
                                id: element.Id,
                                filename:
                                    element.Title +
                                    "." +
                                    element.FileType,
                                downloadfile:
                                    "/sfc/servlet.shepherd/document/download/" +
                                    element.ContentDocumentId,
                                ContentDocumentId: element.ContentDocumentId
                            });
                        }
                    })

                    this.finalUploadResult = arrayFileUpload;
                }
            }
        }
        catch (error) {
            if (error) {
                let error = JSON.parse(error.body.message);
                const { title, message, errorType } = error;
                this.dispatchEvent(
                    utils.toastMessageWithTitle(title, message, errorType)
                )
            }
            else {
                this.dispatchEvent(
                    utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                );
            }
        }
    }

    deleteFileupload(event) {
        deleteRecord(event.target.dataset.id)
            .then(result => {
                this.dispatchEvent(
                    utils.toastMessage("Record has been deleted Successfully", "Success")
                );
                refreshApex(this.uploadRecordDetails);
            })
            .catch(errors => {
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const { title, message, errorType } = error;
                    this.dispatchEvent(
                        utils.toastMessageWithTitle(title, message, errorType)
                    );
                } else {
                    this.dispatchEvent(
                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                    );
                }
            });
    }

    getActorbasedFileDetailsfetch() {
        getUploadedDocuments({
            strDocumentID: this.reconsiderationId
        }).then((data) => {
            try {
                if (data.length > 0) {
                    this.finalUploadResult = data;
                    let Actorrid = this.hasactorid;
                    let arrayFileUpload = [];
                    this.finalUploadResult.forEach((element) => {
                        if (element.Actor__c == Actorrid) {
                            arrayFileUpload.push({
                                id: element.Id,
                                filename:
                                    element.Title +
                                    "." +
                                    element.FileType,
                                downloadfile:
                                    "/sfc/servlet.shepherd/document/download/" +
                                    element.ContentDocumentId,
                                ContentDocumentId: element.ContentDocumentId
                            });
                        }
                    })
                    this.finalUploadResult = arrayFileUpload;
                }

            }
            catch (error) {
                if (error) {
                    let error = JSON.parse(error.body.message);
                    const { title, message, errorType } = error;
                    this.dispatchEvent(
                        utils.toastMessageWithTitle(title, message, errorType)
                    )
                }
                else {
                    this.dispatchEvent(
                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                    );
                }
            }
        });
    }
    /* File Upload New Method End */
}