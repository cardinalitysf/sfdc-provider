/**
 * @Author        : Naveen S
 * @CreatedOn     : June 02,2020
 * @Purpose       : Complaints Attachments based on Monitoring 
 **/

import {
    LightningElement,
    track

} from 'lwc';
import * as referralsharedDatas from "c/sharedData";
import {
    CJAMS_CONSTANTS
} from 'c/constants';

export default class ComplaintsMonitoringDocument extends LightningElement {
    get getMonitoringStatus() {
        return referralsharedDatas.getMonitoringStatus();
        // return 'Completed';
    }

    get monitoringId() {
        let monId = referralsharedDatas.getMonitoringId();
        return monId;
    }
    get sobjectNameToFetch() {
        return CJAMS_CONSTANTS.MONITORING_OBJECT_NAME;
    }

}