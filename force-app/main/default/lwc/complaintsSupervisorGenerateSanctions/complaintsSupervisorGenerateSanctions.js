/**
	 * @Author        : K.Sundar
	 * @CreatedOn     : JULY 05, 2020
	 * @Purpose       : This component contains Sanctions for Supervisor
 **/

import { LightningElement, track } from "lwc";

import images from "@salesforce/resourceUrl/images";

export default class ComplaintsSupervisorGenerateSanctions extends LightningElement {
	@track suspensionIcon = images + "/suspension.svg";
	@track suspensionIconblue = images + "/suspension-active.svg";
	@track revocationIcon = images + "/revocation.svg";
	@track revocationIconblue = images + "/revocation-active.svg";
	@track limitationIcon = images + "/limitation.svg";
	@track limitationIconblue = images + "/limitation-active.svg";

	@track generateSanctionModel = true;
	@track isDisableNextBtn = true;

	_pageType;

	@track Spinner = false;

	handleSanctionRedirect (event) {
		this._pageType = event.currentTarget.name;
		this.isDisableNextBtn = false;

		switch(this._pageType) {
			case 'Suspension' : 
				this.template.querySelector(".box-col1").classList.add("bluebdr");
				this.template.querySelector(".box-col2").classList.remove("bluebdr");
				this.template.querySelector(".box-col3").classList.remove("bluebdr");
				this.template.querySelector(".iconsuspension").src = this.suspensionIconblue;
				this.template.querySelector(".iconrevocation").src = this.revocationIcon;
				this.template.querySelector(".iconlimitation").src = this.limitationIcon;
				break;
			case 'Revocation' : 
				this.template.querySelector(".box-col2").classList.add("bluebdr");
				this.template.querySelector(".box-col1").classList.remove("bluebdr");
				this.template.querySelector(".box-col3").classList.remove("bluebdr");
				this.template.querySelector(".iconsuspension").src = this.suspensionIcon;
				this.template.querySelector(".iconrevocation").src = this.revocationIconblue;
				this.template.querySelector(".iconlimitation").src = this.limitationIcon;
				break;
			case 'Limitation' : 
				this.template.querySelector(".box-col3").classList.add("bluebdr");
				this.template.querySelector(".box-col1").classList.remove("bluebdr");
				this.template.querySelector(".box-col2").classList.remove("bluebdr");
				this.template.querySelector(".iconsuspension").src = this.suspensionIcon;
				this.template.querySelector(".iconrevocation").src = this.revocationIcon;
				this.template.querySelector(".iconlimitation").src = this.limitationIconblue;
				break;
		}
	}

	closeModel(event) {
		event.preventDefault();
		this.generateSanctionModel = false;
		this.intimateParentToCloseModal();
	}

	intimateParentToCloseModal() {
		const intimateParentToCloseModal = new CustomEvent("intimateparenttoclosemodal", {
			detail: {
				closeModal: true
			}
		});
		this.dispatchEvent(intimateParentToCloseModal);
		this.Spinner = false;
	}

	handleRedirection(event) {
		this.Spinner = true;
		const onSanctionGenerateClick = new CustomEvent("redirecttosanctiontypepage", {
			bubbles: true,
			composed: true,
			detail: {
				pageType: this._pageType
			}
		});

		this.dispatchEvent(onSanctionGenerateClick);
		this.intimateParentToCloseModal();

		this.generateSanctionModel = false;
	}
}
