/**
 * @Author        : Preethi Bojarajan
 * @CreatedOn     : May 11, 2020
 * @Purpose       : Provider Dashboard Combine with Pubic and Private Tabs
 * @updatedBy     : Preethi Bojarajan
 * @updatedOn     : Jun 24, 2020
 **/
import { LightningElement, track, api } from 'lwc';

//Apex Call
import fetchApplicationName from "@salesforce/apex/PublicProvidersDashboard.fetchApplicationName";
//Utils 
import * as sharedData from "c/sharedData";

export default class PublicProviderAllTabCmbs extends LightningElement {
    @track showPublicApplicationDashboard=false;
    @track hideHeaderButton=false;
    @track showPublicApplicationTabs=false;
    @api showProvidersDashoard = false;
    @api showPublicApplications = false;
    @api showPrivateProvidersTabSet = false;
    @api showProviderInfo = false;
    @track applicationId;

    //Private Providers Track
    @track showDashboard = true;
    @track showProviderMonitering = false;
    @track activeTab = 'providerInfo';
    @track showContractInfo = false;
    @track showContractAdd = false;
    @track showStaffInfo = false;
    @track providerStaffId = null;
    @track showApplicationTabs = false;

    //Public Providers Track
    //Initial App Start
    @api providerName;
    @track hideHeaderButton = false;
    @track showPublicApplicationTabs = false;

    @track openServiceInfoModal;
    @track openPetInfoModal;
    @track activeTabPublic = 'basicInfo';
    @track openSessionModal;
    @track homeVisitModal;

    //Button Handling Functions
    @track openSessionModal;
    @track showPop = false;
    @track activeTabValue = null;
    @track showReferenceAddBtn = false;
    @track showAddService = false;
    @track showPetDetails = false;
    @track showAssignTraining = false;
    @track showAddHomeStudy = false;

    //HHM
    @track isView = false;
    @track searchHolder = false;
    @track showResult = false;
    @track meetingFlag;
    @track intentFlag;
    @track showListParent;

    @track showPublicProviderAddmember = false;
    @track showPublicProviderAddmemberSearch = false;
    @track sessionHoursFlag;
    @track homeStudyCompletedFlag;

    //supervisor
    @track ApplicationStatus;
    @track showRedirectPublicApplications=false;

    get providerId() {
        return sharedData.getProviderId();
    }

    handleToShowPublicProvTab(event) {
        fetchApplicationName({
            providerId: this.providerId
        }).then(result => {

            if (result.length > 0) {
                let data = result[0];
                this.applicationId = data.Id;
                sharedData.setApplicationId(this.applicationId);
                sharedData.setApplicationStatus(data.Status__c);
                this.showProvidersDashoard = !event.detail.first;
                this.showPrivateProvidersTabSet = event.detail.first;
                this.showPublicApplications = !event.detail.first;
                this.hideHeaderButton = !event.detail.first;
                this.showPublicApplicationTabs = !event.detail.first;
                this.ApplicationStatus = sharedData.getApplicationStatus();
                this.providerName = event.detail.providerName !== undefined ? event.detail.providerName : this.providerName;
                this.supervisor();
            }
        });
    }
@api 
handleToShowPrivateProvTab() {
        this.showProvidersDashoard = true;
        this.showPrivateProvidersTabSet = true;
        this.showPublicApplications = false;
        this.showProviderInfo = true;
    }

    //Private Providers Function Start
    handleProviderMoniteringTabs(event) {
        this.showProviderInfo = event.detail.first;
        this.showDashboard = event.detail.first;
        this.showProviderMonitering = true;
    }

    handleredirectToMoniter(event) {
        this.showProviderMonitering = false;
        this.showProviderInfo = event.detail.first;
        this.activeTab = 'monitoring';
    }

    handleContractManage(event) {
        this.showProviderInfo = event.detail.first;
        this.showDashboard = event.detail.first;
        this.showContractInfo = true;
        this.showContractAdd = false;
    }

    handleContractAdd(event) {
        this.showProviderInfo = event.detail.first;
        this.showContractAdd = true;
        this.showContractInfo = false;
    }

    handleContractDashboard(event) {
        this.showProviderInfo = event.detail.first;
        this.showContractAdd = false;
        this.showContractInfo = false;
        this.activeTab = 'contract';
    }

    handleredirectToStaffInfo(event) {
        if (event.detail.first === false) {
            this.showProviderInfo = event.detail.first;
            this.showStaffInfo = true;
            this.providerStaffId = event.detail.staffId;
        } else {
            this.showProviderInfo = event.detail.first;
            this.showStaffInfo = false;
            this.activeTab = 'staff';
            this.providerStaffId = null;
        }
    }

    handletoApplication(event) {
        this.showProviderInfo = event.detail.first;
        this.showApplicationTabs = true;
    }

    //Private Providers Function End

    //Public Providers Function Start
    get getUserProfileName() {
        return sharedData.getUserProfileName();
    }

    //Active Tab Button show/hide
    handleActiveTabButtons(event) {
        this.activeTabValue = event.target.value;
        if (this.activeTabValue === 'referenceCheck') {
            this.showReferenceAddBtn = true;
            this.showAddService = false;
            this.showPetDetails = false;
            this.showAssignTraining = false;
            this.showAddHomeStudy = false;
        } else if (this.activeTabValue === 'PetInfo') {
            this.showPetDetails = true;
            this.showReferenceAddBtn = false;
            this.showAddService = false;
            this.showAssignTraining = false;
            this.showAddHomeStudy = false;
        } else if (this.activeTabValue === 'StudyVisit') {
            this.showPetDetails = false;
            this.showReferenceAddBtn = false;
            this.showAddService = false;
            this.showAssignTraining = false;
            this.showAddHomeStudy = true;
        } else if (this.activeTabValue === 'AddServices') {
            this.showPetDetails = false;
            this.showReferenceAddBtn = false;
            this.showAddService = true;
            this.showAssignTraining = false;
            this.showAddHomeStudy = false;
        } else if (this.activeTabValue === 'TrainService') {
            this.showPetDetails = false;
            this.showReferenceAddBtn = false;
            this.showAddService = false;
            this.showAssignTraining = true;
            this.showAddHomeStudy = false;
        } else {
            this.showPetDetails = false;
            this.showReferenceAddBtn = false;
            this.showAddService = false;
            this.showAssignTraining = false;
            this.showAddHomeStudy = false;
        }
        this.supervisor();
    }

    redirectToServiceInfo(event) {
        this.openServiceInfoModal = true;
        this.showProvidersDashoard = "AddServices";
    }
    handleToggleComponentsForService(event) {
        this.openServiceInfoModal = false;
    }
    redirectToPetInfo(event) {
        this.openPetInfoModal = true;
        this.showProvidersDashoard = "PetInfo";
    }

    handleToggleComponents(event) {
        this.openPetInfoModal = false;
    }

    redirectToSessionInfo(event) {
        this.openSessionModal = true;
        this.showProvidersDashoard = "TrainService";
    }

    handleToggleSessionComponents(event) {
        this.openSessionModal = false;
    }

    redirecttoStudyVisit(event) {
        this.homeVisitModal = true;
        this.showProvidersDashoard = "StudyVisit";
    }
    handleToggleComponentsForHomeVisit(event) {
        this.homeVisitModal = false;
    }
    redirectToAddRefPop() {
        this.showPop = true;
    }

    redirectAddOff(event) {
        this.showPop = event.detail.first;
    }


    //HHM
    handleAddMemberSearch(event) {
        this.showPublicProviderAddmemberSearch = true;
        this.showPublicProviderAddmember = event.detail.first;
        this.showPublicApplicationTabs = event.detail.first;
        this.showResult = false;
        this.hideHeaderButton = false;
    }

    handleAddMember() {
        this.showPublicProviderAddmember = true;
        this.searchHolder = false;
        this.showPublicApplicationTabs = false;
        this.hideHeaderButton = false;
    }

    handleAddMemberView(event) {
        this.showPublicProviderAddmember = true;
        this.isView = true;
        this.hideHeaderButton = false;
    }
    handleMeeting(event) {
        this.meetingFlag = event.detail.meetingFlag;
    }

    redirectToHousHoldDashboard(event) {
        this.showPublicProviderAddmember = event.detail.first;
        this.showProvidersDashoard = 'houseHoldMember';
        this.showPublicApplicationTabs = true;
        this.hideHeaderButton = true;
    }
    handleAddMemberSearchFromAddHouse(event) {
        this.showPublicProviderAddmemberSearch = true;
        this.showPublicProviderAddmember = event.detail.first;
        this.showPublicApplicationTabs = event.detail.first;
        this.showResult = true;
        this.hideHeaderButton = false;

    }
    handleAddMemberHome(event) {
        this.showPublicProviderAddmemberSearch = event.detail.first;
        this.showProvidersDashoard = 'houseHoldMember';
        this.showPublicApplicationTabs = true;
        this.hideHeaderButton = true;
    }
    redirectHandleAddMember(event) {
        this.showPublicProviderAddmemberSearch = event.detail.first;
        this.showListParent = event.detail.searchLists;
        this.showPublicProviderAddmember = true;
        this.searchHolder = true;
        this.hideHeaderButton = false;
    }
    handleComarRegulations(event) {

    }
    handleSessionFlag(event) {
        this.sessionHoursFlag = event.detail.sessionFlag;
    }
    handleHomeStudyCompletedFlag(event) {
        this.homeStudyCompletedFlag = event.detail.homeStudyCompletedFlag;
    }

    redirectToPubAppTabs(event) {
        this.showProvidersDashoard = !event.detail.first;
        this.hideHeaderButton = !event.detail.first;
        this.showPublicApplicationTabs = !event.detail.first;
        this.ApplicationStatus = sharedData.getApplicationStatus();
        this.supervisor();
    }

    redirectToDashboard(event) {
        this.showProvidersDashoard = !event.detail.first;
        this.hideHeaderButton = !event.detail.first;
        this.showPublicApplicationTabs = !event.detail.first;
    }

    //sp hide button
    handleSupervisorButton(event) {
        this.showAssignTraining = event.detail.first;
    }

    handlePrivateAppTabInfo(event) {
        this.showPrivateApplicationTabs = !event.detail.first;
        this.showProvidersDashoard = !event.detail.first
    }
    supervisor() {
        if (this.getUserProfileName != "Supervisor") {
            if (
                [
                    "Caseworker Submitted",
                    "Approved",
                    "Rejected",
                    "Supervisor Rejected"
                ].includes(this.ApplicationStatus)
            ) {
                this.showPetDetails = false;
                this.showReferenceAddBtn = false;
                this.showAddService = false;
                this.showAssignTraining = false;
                this.showAddHomeStudy = false;
            }
        }
        if (this.getUserProfileName == "Supervisor") {
            if (["Approved", "Supervisor Rejected"].includes(this.ApplicationStatus)) {
                this.showPetDetails = false;
                this.showReferenceAddBtn = false;
                this.showAddService = false;
                this.showAssignTraining = false;
                this.showAddHomeStudy = false;
            }
        }
    }
    //Public Providers Function End

    //Handle Contarct/ Monitoring Tabs Redirection Start
    handleredirectToMoniterTo(event) {
        this.showProvidersDashoard = !event.detail.first;
        this.showPrivateProvidersTabSet = !event.detail.first;
        //this.showPublicApplications = event.detail.first;
        this.showProviderInfo = !event.detail.first;
        this.showProviderMonitering = false;
        //this.showProviderInfo = event.detail.first;
        this.activeTab = 'monitoring';
    }

    handleContractDashboardTo(event) {
        this.showProvidersDashoard = !event.detail.first;
        this.showPrivateProvidersTabSet = !event.detail.first;
        //this.showPublicApplications = event.detail.first;
        this.showProviderInfo = !event.detail.first;
        //this.showProviderInfo = event.detail.first;
        this.showContractAdd = false;
        this.showContractInfo = false;
        this.activeTab = 'contract';
    }
    //Handle Contarct/ Monitoring Tabs Redirection End

    redirectToPublicApplication(){
        this.showRedirectPublicApplications=true;
        this.showPublicApplications=false;
        this.showPublicApplicationDashboard=true;
        this.hideHeaderButton=true;
        this.showPublicApplicationTabs=true;
    }


}