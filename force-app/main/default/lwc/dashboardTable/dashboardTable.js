// Author:Saranraj
// for dashboard Table

import {
  LightningElement,
  api,
  track,
  wire
} from "lwc";
import * as sharedData from "c/sharedData";
import getDashboardData from "@salesforce/apex/Board.getDashboardData";
import {
  refreshApex
} from "@salesforce/apex";

import {
  CJAMS_CONSTANTS
} from 'c/constants';
export default class BoardTabel extends LightningElement {
  // @track _dashboardData;
  data = [];
  _refreshDashBoardData;
  @track _hidePagination;
  @track noRecordsFoundMessage = CJAMS_CONSTANTS.NO_RECORDS_FOUND;
  // sorting
  @track sortBy = "Name";
  @track sortDirection;


  @api
  get hidePagination() {
    if (this.data) {
      if (this.data.length > 0)
        this._hidePagination = false;
      else
        this._hidePagination = true;
    } else {
      this._hidePagination = true;
    }
    return this._hidePagination
  }

  set hidePagination(value) {
    this.setAttribute("hidePagination", value);
    if (this.data.length > 0)
      this._hidePagination = true;
    else
      this._hidePagination = false;


  }


  @api
  get dashboardData() {
    // return this._dashboardData;
    return this.data;
  }
  set dashboardData(value) {
    try {
      this.setAttribute("dashboardData", value);
      // this._dashboardData = value;  
      this.data = value;
      this.setPages(this.data);
      this.totalRecordsCount = value.length;
    } catch (e) {
      this.totalRecordsCount = 0;
    }
  }


  // new pagination

  @track page = 1;
  perPage = 10;
  @track pages = [];
  setSize = 5;
  @track pagePerTime;
  @track pagesLength;

  renderedCallback() {
    this.renderButtons();
  }

  renderButtons = () => {
    this.template.querySelectorAll('button').forEach((but) => {
      but.className = this.page === parseInt(but.dataset.id, 10) ? 'slds-button slds-button_neutral active' : 'slds-button slds-button_neutral';
    });
  }


  get pagesList() {
    let mid = Math.floor(this.setSize / 2) + 1;
    if (this.page > mid) {
      return this.pages.slice(this.page - mid, this.page + mid - 1);
      // let pagecount = this.pages.slice(0, this.setsize);
      // this.pagePerTime = pagecount[pagecount.length - 1];
      // return this.pages.slice(this.page - mid, this.page + mid - 1);
    } else
      // let pagecount = this.pages.slice(0, this.setsize);
      // this.pagePerTime = pagecount[pagecount.length - 1];
      return this.pages.slice(0, this.setSize);
  }

  setPages = (data) => {
    try {
      let numberOfPages = Math.ceil(JSON.stringify(data.length) / this.perPage);
      this.pagesLength = numberOfPages;
      this.pages = [];
      for (let i = 1; i <= numberOfPages; i++) {
        this.pages.push(i);
      }
    } catch (e) {}
  }

  get hasPrev() {
    return this.page > 1;
  }

  get hasNext() {
    return this.page < this.pages.length;
  }

  onNext = () => {
    ++this.page;
  }

  onPrev = () => {
    --this.page;
  }

  onPageClick(event) {
    this.page = parseInt(event.target.dataset.id, 10);
  }

  get currentPageData() {
    return this.pageData();
  }

  pageData() {
    if (this.data) {
      if (this.data.length > 0) {
        let startIndex = (this.page * this.perPage) - this.perPage;
        let endIndex = (this.page * this.perPage);
        return this.data.slice(startIndex, endIndex);
      }
    }
  }


  // new pagination end ==================/

  // for datable columns
  get options() {
    return [{
        label: "Application ID",
        fieldName: "Id",
        type: "button",
        sortable: true,
        typeAttributes: {
          label: {
            fieldName: "Name"
          },
          class: "blue",
          target: "_self"
        }
      },
      {
        label: "Provider ID",
        fieldName: "ProviderId__c",
        cellAttributes: {
          class: "fontclrGrey"
        }
      },
      {
        label: "Program Name",
        fieldName: "Program__c",
        cellAttributes: {
          class: "fontclrGrey"
        }
      },
      {
        label: "Program Type",
        fieldName: "ProgramType__c",
        cellAttributes: {
          class: "fontclrGrey"
        }
      },
      {
        label: "Status",
        fieldName: "Status",
        type: 'text',
        cellAttributes: {
          class: {
            fieldName: 'statusClass'
          }
        }
      }
    ];
  }

  // for get Application id from datatable
  handleRowAction(event) {
    sharedData.setApplicationId(event.detail.row.Id);
    sharedData.setProviderId(event.detail.row.Provider__c);
    const oncaseid = new CustomEvent('redirecteditapplication', {
      detail: {
        first: false
      }
    });
    this.dispatchEvent(oncaseid);
  }

  handleSortData(event) {
    this.sortBy = event.detail.fieldName;
    this.sortDirection = event.detail.sortDirection;
    this.sortedData(event.detail.fieldName, event.detail.sortDirection);
  }

  sortedData(fieldName, direction) {
    let parseData = JSON.parse(JSON.stringify(this.data));
    let keyValue = a => {
      return a[fieldName];
    };

    let isReverse = direction === "asc" ? 1 : -1;

    parseData.sort((x, y) => {
      x = keyValue(x) ? keyValue(x) : "";
      y = keyValue(y) ? keyValue(y) : "";

      return isReverse * ((x > y) - (y > x));
    });

    this.data = parseData;
  }

}