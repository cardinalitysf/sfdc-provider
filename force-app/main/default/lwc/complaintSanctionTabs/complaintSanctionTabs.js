/**
    * @Author        : K.Sundar
    * @CreatedOn     : JULY 05, 2020
    * @Purpose       : This component contains Sanctions Types for Supervisor
    * @UpdatedBy     : Sundar K -> July 24, 2020 -> Spinner added and 1 sec interval handled in redirection methods.
 **/

import { LightningElement, track, api } from 'lwc';

export default class ComplaintSanctionTabs extends LightningElement {
    //Local value declaration
    @track showSuspensionTab = false;
    @track showRevocationTab = false;
    @track showLimitaionTab = false;
    @track Spinner = false;

    //Global value declaration
    @api sanctionPageType;
    @api selectedSanctionId;
    @api sanctionActionName;
    @api sanctionName;


    //Connected call back method
    connectedCallback() {
        switch(this.sanctionPageType) {
            case 'Suspension' : 
                this.showSuspensionTab = true;
                this.showRevocationTab = false;
                this.showLimitaionTab = false;
                break;
            case 'Revocation' : 
                this.showRevocationTab = true;
                this.showSuspensionTab = false;
                this.showLimitaionTab = false;
                break;
            case 'Limitation' : 
                this.showSuspensionTab = false;
                this.showRevocationTab = false;
                this.showLimitaionTab = true;
                break;
        }
    }

    //Handle Complaint Sanction redirection method
    handleComplaintSanctionRedirection(event) {
        this.Spinner = true;

        //1 sec interval
        setTimeout(() => {
            const onClickComplaintDirection = new CustomEvent('redirecttocomplaintsanctiontab', {
                detail: {
                    first: true
                }
            });
            this.dispatchEvent(onClickComplaintDirection);
            this.Spinner = false;
        }, 1000);
    }

    //Handle Complaint dashboard redirection method
    handleComplaintDashboard() {
        this.Spinner = true;

        //1 sec interval
        setTimeout(() => {
            const onClickComplaintDashboard = new CustomEvent('redirecttocomplaintdashboard', {
                detail: {
                    first: true
                }
            });
            this.dispatchEvent(onClickComplaintDashboard);
            this.Spinner = false;
        }, 1000);
    }
}