/*
    Author : Sindhu
    Modified On : Apr 09, 2020
*/

import {
    LightningElement,
    track,
    api,
    wire
} from 'lwc';

import {
    utils
} from 'c/utils';
import * as sharedData from "c/sharedData";
import getNarrativeDetails from '@salesforce/apex/applicationproviderNarrative.getNarrativeDetails';
import updateNarrativeDetails from '@salesforce/apex/applicationproviderNarrative.updateNarrativeDetails';
import getHistoryOfNarrativeDetails from '@salesforce/apex/applicationproviderNarrative.getHistoryOfNarrativeDetails';
import getSelectedHistoryRec from '@salesforce/apex/applicationproviderNarrative.getSelectedHistoryRec';
import images from '@salesforce/resourceUrl/images';
import getApplicationProviderStatus from "@salesforce/apex/applicationproviderNarrative.getApplicationProviderStatus";
import getUserEmail from "@salesforce/apex/applicationproviderNarrative.getUserEmail";
import {
    CJAMS_CONSTANTS
} from 'c/constants';
import {
    refreshApex
} from "@salesforce/apex";

export default class providernarrativeHistory extends LightningElement {
    @track result;
    @track data = [];
    @track narrativeValue;
    @track visibleHistoryData = false;

    @track richTextDisable = false;
    @track btnDisable = false;
    @track openNarrative = true;
    @track btnspeechDisable = false;

    attachmentIcon = images + '/document-newIcon.svg';
    recordId;
    wireNarrativeHistoryDetails;
    applicationForApiCall = this.ApplicationId;

    freezeNarrativeScreen = false;
    finalStatus;
    @track Spinner = true;

    @api
    get ApplicationId() {
        return sharedData.getApplicationId();
    }

    @api
    get ProviderId() {
        return sharedData.getProviderId();
    }

    //Sundar Adding this line
    get isSuperUserFlow() {
        let userProfile = sharedData.getUserProfileName();
        if (userProfile === 'Supervisor')
            return true;
        else
            return false;
    }

    connectedCallback() {
        this.isSuperUserFlow;
    }

    openNarrativeModel() {
        if (this.freezeNarrativeScreen)
            return this.dispatchEvent(utils.toastMessage(`Cannot add narrative for ${this.finalStatus} application`, "warning"));

        this.openNarrative = true;
        this.btnDisable = false;
        this.richTextDisable = false;
        this.btnspeechDisable = false;
        this.narrativeValue = '';
    }

    //get User Details
    @wire(getUserEmail)
    email({
        error,
        data
    }) {
        if (data) {
            let userData = data[0];
            if (userData.Profile.Name === 'Supervisor') {
                this.userType = 'Supervisor';
            } else {
                this.userType = 'System';
            }
        } else if (error) {
            this.error = error;
        }
    }

    @wire(getNarrativeDetails, {
        applicationId: '$applicationForApiCall'
    })
    wiredNarratvieDetails({
        error,
        data
    }) {
        if (data) {
            if (data.length > 0) {
                this.narrativeValue = data[0].Narrative__c;
            }
            this.Spinner = false;
        }
    }

    @wire(getHistoryOfNarrativeDetails, {
        applicationId: '$applicationForApiCall'
    })
    wiredNarrativeHistoryDetails(result) {
        this.wireNarrativeHistoryDetails = result;

        if (result.data) {
            if (result.data.length > 0) {
                this.visibleHistoryData = true;

                this.data = result.data.map((row, index) => {
                    return {
                        Id: row.Id,
                        author: row.LastModifiedBy.Name,
                        lastModifiedDate: row.LastModifiedDate,
                        dataIndex: index
                    }
                });
            } else {
                this.visibleHistoryData = false;
            }
        }
    }

    @wire(getApplicationProviderStatus, {
        applicationId: '$applicationForApiCall'
    })
    wiredApplicationDetails({
        error,
        data
    }) {
        if (data) {
            if (data.length > 0 && ['Caseworker Submitted', 'Approved', 'Rejected', 'Supervisor Rejected'].includes(data[0].Status__c)) {
                this.btnDisable = true;
                this.richTextDisable = true;
                this.freezeNarrativeScreen = true;
                this.btnspeechDisable = true;
                this.finalStatus = data[0].Status__c;
            } else {
                this.btnDisable = false;
                this.richTextDisable = false;
                this.freezeNarrativeScreen = false;
                this.btnspeechDisable = false;
                this.finalStatus = null;
            }
        }
    }
    handleSpeechToText(event) {

        this.narrativeValue = event.detail.speechValue;
    }
    //Narrative On Change Functionality
    narrativeOnChange(event) {
        this.narrativeValue = event.target.value;
    }
    cancelHandler() {
        this.narrativeValue = '';
    }


    //Save Functionality
    saveNarrativeDetails() {
        this.template.querySelector('c-common-speech-to-text').stopSpeech();
        let myObj = {
            'sobjectType': 'Application__c'
        };

        myObj.Narrative__c = this.narrativeValue;

        if (this.ApplicationId)
            myObj.Id = this.ApplicationId;

        updateNarrativeDetails({
            objSobjecttoUpdateOrInsert: myObj
        })
            .then(result => {
                this.narrativeValue = '';

                this.dispatchEvent(utils.toastMessage("Application Narrative has been saved successfully", "success"));

                return refreshApex(this.wireNarrativeHistoryDetails);
            })
            .catch((errors) => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
    }

    //Get respective history record
    narrativeHistoryOnClick(event) {
        this.recordId = event.currentTarget.dataset.id;

        let dataIndex = event.currentTarget.dataset.value;
        let datas = this.template.querySelectorAll('.divHighLight');

        for (let i = 0; i < datas.length; i++) {
            if (i == dataIndex)
                datas[i].className = "slds-nav-vertical__item divHighLight slds-is-active";
            else
                datas[i].className = "slds-nav-vertical__item divHighLight";
        }

        getSelectedHistoryRec({
            recId: this.recordId
        })
            .then(result => {
                if (result.length > 0 && result[0].RichNewValue__c) {
                    this.narrativeValue = result[0].RichNewValue__c;
                    this.btnDisable = true;
                    this.richTextDisable = true;
                    this.btnspeechDisable = true;
                } else {
                    this.narrativeValue = null;
                }
            }).catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
    }
}