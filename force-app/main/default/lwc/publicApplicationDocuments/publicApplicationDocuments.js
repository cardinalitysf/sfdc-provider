/**
 * @Author        : Balamurugan.B
 * @CreatedOn     : June 03,2020
 * @Purpose       : Attachments based on Public Application Documents
 **/

import {
    LightningElement,
    track

} from 'lwc';
import * as referralsharedDatas from "c/sharedData";
import {
    CJAMS_CONSTANTS
} from 'c/constants';


export default class PublicApplicationDocuments extends LightningElement {
    get applicationID() {
        return referralsharedDatas.getApplicationId();
       // return 'a0A9D0000012eivUAA';
    }
    get sobjectNameToFetch() {
        return CJAMS_CONSTANTS.APPLICATION_OBJECT_NAME;
    }

    get getUserProfileName() {
        return referralsharedDatas.getUserProfileName();
    }

}