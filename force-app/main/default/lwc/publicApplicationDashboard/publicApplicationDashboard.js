/**
 * @Author        : Preethi Bojarajan
 * @CreatedOn     : Jun 07,2020
 * @Purpose       : Public / Private Application Dashboard With Card Clickable
 * @updatedBy     : Preethi Bojarajan
 * @updatedOn     : July 15, 2020
 **/

import { LightningElement, track, api, wire } from "lwc";
import getDashboardData from "@salesforce/apex/PublicApplicationDashboard.getDashboardData";
import getApplicationCount from "@salesforce/apex/PublicApplicationDashboard.getApplicationCount";
import getPrivateDashboardData from "@salesforce/apex/PublicApplicationDashboard.getPrivateDashboardData";

import { getRecord } from "lightning/uiRecordApi";
import USER_ID from "@salesforce/user/Id";
import PROFILE_FIELD from "@salesforce/schema/User.Profile.Name";
import * as sharedData from "c/sharedData";
import { refreshApex } from "@salesforce/apex";

//Card Values For Translation
import PENDING_LABEL from "@salesforce/label/c.Pending";
import APPROVED_LABEL from "@salesforce/label/c.Approved";
import REJECTED_LABEL from "@salesforce/label/c.Rejected";
import RETURNED_LABEL from "@salesforce/label/c.Returned";
import TLAPP_LABEL from "@salesforce/label/c.Total_Applications";
import APPID_LABEL from "@salesforce/label/c.Application_Id";
import REFTYPE_LABEL from "@salesforce/label/c.Referral_Type";
import PROGRAM_LABEL from "@salesforce/label/c.Program";
import PROTYPE_LABEL from "@salesforce/label/c.Program_Type";
import APPLICANT_LABEL from "@salesforce/label/c.Applicant";
import COAPPLICANT_LABEL from "@salesforce/label/c.Co_Applicant";
import STATUS_LABEL from "@salesforce/label/c.Status";
import PROVID_LABEL from "@salesforce/label/c.Provider_Id";
import PROGRAM_NAME_LABEL from "@salesforce/label/c.Program_Name";
import SYS_ADMIN_LABEL from "@salesforce/label/c.System_Administrator";
import SUPERVISOR_LABEL from "@salesforce/label/c.Supervisor";
import CASEWORKER_LABEL from "@salesforce/label/c.Caseworker";
import SRCHAPPID_LABEL from "@salesforce/label/c.Search_by_Application_Id";
import APPHEADER_LABEL from "@salesforce/label/c.Applications";

const publiccardDatas = [
  {
    Id: "1",
    title: PENDING_LABEL,
    key: "pending",
    count: 0
  },
  {
    Id: "2",
    title: APPROVED_LABEL,
    key: "approved",
    count: 0
  },
  {
    Id: "3",
    title: REJECTED_LABEL,
    key: "rejected",
    count: 0
  },
  {
    Id: "4",
    title: RETURNED_LABEL,
    key: "returned",
    count: 0
  },
  {
    Id: "5",
    title: TLAPP_LABEL,
    key: "total",
    count: 0
  }
];

const privCardDatas = [
  {
    Id: "1",
    title: PENDING_LABEL,
    key: "pending",
    count: 0
  },
  {
    Id: "2",
    title: APPROVED_LABEL,
    key: "approved",
    count: 0
  },
  {
    Id: "3",
    title: REJECTED_LABEL,
    key: "rejected",
    count: 0
  },
  {
    Id: "4",
    title: RETURNED_LABEL,
    key: "returned",
    count: 0
  },
  {
    Id: "5",
    title: TLAPP_LABEL,
    key: "total",
    count: 0
  }
];

const columns = [
  {
    label: APPID_LABEL,
    fieldName: "Id",
    sortable: true,
    type: "button",
    typeAttributes: {
      label: {
        fieldName: "Name"
      },
      class: "blue",
      target: "_self",
      color: "blue"
    }
  },
  {
    label: REFTYPE_LABEL,
    fieldName: "ProviderType",
    sortable: true,
    cellAttributes: {
      class: "fontclrGrey"
    }
  },
  {
    label: PROGRAM_LABEL,
    fieldName: "Program__c",
    sortable: true,
    typeAttributes: {
      label: {
        fieldName: "Program"
      }
    },
    cellAttributes: {
      class: "fontclrGrey"
    }
  },
  {
    label: PROTYPE_LABEL,
    fieldName: "ProgramType__c",
    sortable: true,
    cellAttributes: {
      class: "fontclrGrey"
    }
  },
  {
    label: APPLICANT_LABEL,
    fieldName: "applicant",
    cellAttributes: {
      class: "fontclrGrey"
    }
  },
  {
    label: COAPPLICANT_LABEL,
    fieldName: "coApplicant",
    cellAttributes: {
      class: "fontclrGrey"
    }
  },
  {
    label: STATUS_LABEL,
    fieldName: "Status",
    type: "text",
    cellAttributes: {
      class: {
        fieldName: "statusClass"
      }
    }
  }
];

const privateColumns = [
  {
    label: APPID_LABEL,
    fieldName: "Id",
    type: "button",
    sortable: true,
    typeAttributes: {
      label: {
        fieldName: "Name"
      },
      class: "blue",
      target: "_self"
    }
  },
  {
    label: PROVID_LABEL,
    fieldName: "ProviderId__c",
    cellAttributes: {
      class: "fontclrGrey"
    }
  },
  {
    label: PROGRAM_NAME_LABEL,
    fieldName: "Program__c",
    cellAttributes: {
      class: "fontclrGrey"
    }
  },
  {
    label: PROTYPE_LABEL,
    fieldName: "ProgramType__c",
    cellAttributes: {
      class: "fontclrGrey"
    }
  },
  {
    label: STATUS_LABEL,
    fieldName: "Status",
    type: "text",
    cellAttributes: {
      class: {
        fieldName: "statusClass"
      }
    }
  }
];

export default class PublicApplicationDashboard extends LightningElement {
  @track userProfileName;
  @track supervisorFlow = false;
  @track publiccardDatas = publiccardDatas;
  @track privCardDatas = privCardDatas;
  @track columns = columns;
  @track privateColumns = privateColumns;
  wiredApplicationCount;
  @track totalRecords = [];
  @track activePage = 0;
  @track activeProvider = "Public";
  @track searchKey = "";
  @track showPublicData = true;
  @track appHeader = APPHEADER_LABEL;
  @track searchHolder = SRCHAPPID_LABEL;

  //private Tracks
  _refreshDashBoardData;
  pendingData = [];
  approvedData = [];
  rejectedData = [];
  totalData = [];
  returnedData = [];

  @track pending = 0;
  @track approved = 0;
  @track rejected = 0;
  @track returned = 0;
  @track total = 0;

  get cardClasses() {
    return [
      {
        title: "pending",
        card: "pending-cls",
        init: true
      },
      {
        title: "approved",
        card: "approved-cls",
        init: false
      },
      {
        title: "rejected",
        card: "rejected-cls",
        init: false
      },
      {
        title: "returned",
        card: "returned-cls",
        init: false
      },
      {
        title: "total",
        card: "total-cls",
        init: false
      }
    ];
  }

  @wire(getApplicationCount, {
    searchKey: "$searchKey",
    user: "$supervisorFlow",
    type: "$activeProvider"
  })
  wiredReferalResult(result) {
    this.wiredApplicationCount = result;
    let search = this.searchKey;
    let init = 0;
    if (result.data) {
      let datas = result.data[0];
      if (this.activeProvider === 'Public')
        for (let key of this.publiccardDatas) {
          key.count = datas[key.key];
        } else for (let key of this.privCardDatas) {
          key.count = datas[key.key];
        }
      this.getDataTableDatas(init, search);
    }
  }

  @wire(getRecord, {
    recordId: USER_ID,
    fields: [PROFILE_FIELD]
  })
  wireuser({ error, data }) {
    if (error) {
      this.error = error;
    } else if (data) {
      this.userProfileName = data.fields.Profile.displayValue;
      sharedData.setUserProfileName(data.fields.Profile.displayValue);
      if (this.userProfileName === SUPERVISOR_LABEL) this.supervisorFlow = true;
    }
  }

  formatData(data) {
    let currentData = [];
    if (this.activeProvider === "Public") {
      let application = data[0].application;
      let actor = data[0].actor;
      application.forEach((app) => {
        let rowData = {};
        rowData.Id = app.Id;
        rowData.Name = app.Name !== undefined ? app.Name : " ";
        rowData.Program__c = app.Program__c !== undefined ? app.Program__c : " ";
        rowData.ProgramType__c = app.ProgramType__c !== undefined ? app.ProgramType__c : " ";
        rowData.ProviderType = app.RecordType !== undefined ? app.RecordType.Name : "Public";
        rowData.ProviderId__c = app.Provider__r !== undefined ? app.Provider__r.ProviderId__c : ' ';
        rowData.providerId = app.Provider__c;
        let coAppDet = actor.filter((ac) => ac.Provider__c === app.Provider__c && ac.Role__c === "Co-Applicant"
        );
        rowData.coApplicant =
          coAppDet.length > 0 ? coAppDet[0].Contact__r.LastName : " ";
        let appDet = actor.filter((ac) => ac.Provider__c === app.Provider__c && ac.Role__c === "Applicant");
        rowData.applicant = appDet.length > 0 ? appDet[0].Contact__r.LastName : " ";
        rowData.Status = this.statusDisplayValues(app.Status__c);
        if (app.Status__c !== "Pending" && app.Status__c !== "Approved" && app.Status__c !== "Rejected" && app.Status__c !== "Returned") {
          switch (app.Status__c) {
            case "Assigned for Training":
              rowData.statusClass = "Appeal";
              break;
            case "Training Completed":
            case "Revocation":
            case "Suspension":
            case "Limitation":
              rowData.statusClass = "Approved";
              break;
            case "Training Incompleted":
              rowData.statusClass = "Expired";
              break;
            case "Caseworker Submitted":
              rowData.statusClass = "Submitted";
              break;
            case "Supervisor Rejected":
              rowData.statusClass = "Rejected";
              break; case "Supervisor Rejected":
              rowData.statusClass = "Rejected";
              break;
          }
        } else rowData.statusClass = app.Status__c;
        currentData.push(rowData);
      });
    } else {
      data.forEach((app) => {
        let rowData = {};
        rowData.Id = app.Id;
        rowData.Name = app.Name !== undefined ? app.Name : " ";
        rowData.Program__c = app.Program__c !== undefined ? app.Program__c : " ";
        rowData.ProgramType__c = app.ProgramType__c !== undefined ? app.ProgramType__c : " ";
        rowData.ProviderType = app.RecordType !== undefined ? app.RecordType.Name : "Private";
        rowData.providerId = app.Provider__c;
        rowData.ProviderId__c = app.Provider__r !== undefined ? app.Provider__r.ProviderId__c : ' ';
        rowData.Status = this.statusDisplayValues(app.Status__c);
        if (app.Status__c !== "Pending" && app.Status__c !== "Approved" && app.Status__c !== "Rejected" && app.Status__c !== "Returned") {
          switch (app.Status__c) {
            case "Caseworker Submitted":
              if (this.userProfileName === SUPERVISOR_LABEL) {
                rowData.statusClass = "Pending";
                rowData.Status = "Pending";
              } else {
                rowData.statusClass = "Submitted";
                rowData.Status = "Submitted";
              }
              break;
            case "Supervisor Rejected":
              rowData.statusClass = "Rejected";
              break;
            case "For Review":
              rowData.statusClass = "Pending"
          }
        } else rowData.statusClass = app.Status__c;
        currentData.push(rowData);
      });
    }
    return currentData;
  }

  statusDisplayValues(status) {
    switch (status) {
      case 'Caseworker Submitted':
        return 'Submitted'
        break;
      case 'For Review':
        return 'Pending'
        break;
      case 'Revocation':
        return 'Approved'
        break;
      case 'Suspension':
        return 'Approved'
        break;
      case 'Limitation':
        return 'Approved'
        break;
      default:
        return status;
    }

  }

  getDataTableDatas(id, search) {
    this.activePage = id;
    if (this.activeProvider === "Public")
      getDashboardData({
        searchKey: search,
        type: this.publiccardDatas[id].key,
        user: this.userProfileName
      }).then((results) => {
        try {
          let data = JSON.parse(results);
          if (data[0] !== undefined && data[0].application.length > 0) {
            let currentData = this.formatData(data);
            this.totalRecords = currentData;
          } else this.totalRecords = [];
        } catch (error) {
          throw error;
        }
      });
    else
      getPrivateDashboardData({
        searchKey: search,
        type: this.privCardDatas[id].key,
        user: this.userProfileName
      }).then((data) => {
        try {
          if (data.length > 0) {
            let currentData = this.formatData(data);
            this.totalRecords = currentData;
          } else this.totalRecords = [];
        } catch (error) {
          throw error;
        }
      });
    this.template.querySelector("c-common-dashboard").searchFn();
  }

  handleDataTableDatas(event) {
    let search = this.searchKey;
    if (this.activeProvider === "Public")
      this.getDataTableDatas(event.detail, search);
    else
      this.getDataTableDatas(event.detail, search);
  }

  handleRedirection(event) {
    
    sharedData.setApplicationId(event.detail.redirectId);
    sharedData.setProviderId(event.detail.providerId);
    sharedData.setApplicationStatus(event.detail.status);
    if (this.activeProvider === "Public") {
      const onClickId = new CustomEvent("redirecttopubapptabinfo", {
        detail: {
          first: event.detail.first
        }
      });
      this.dispatchEvent(onClickId);
    } else {
      const onClickPrivateId = new CustomEvent("redircttoprivateapptabinfo", {
        detail: {
          first: event.detail.first
        }
      });
      this.dispatchEvent(onClickPrivateId);
    }
  }

  handleChange(event) {
    this.searchKey = event.target.value;
    this.getDataTableDatas(this.activePage, this.searchKey);
    if (this.supervisorFlow === false)
      refreshApex(this.wiredApplicationCount);
    this.template.querySelector("c-common-dashboard").searchFn();
  }

  handleDashboardDatas(event) {
    this.activeProvider = event.detail;
    if (this.activeProvider === "Private") {
      this.showPublicData = false;
      let init = 0;
      let search = this.searchKey;
      this.getDataTableDatas(init, search);
    } else {
      this.showPublicData = true;
      let init = 0;
      let search = this.searchKey;
      this.getDataTableDatas(init, search);
    }
    refreshApex(this.wiredApplicationCount);
    this.template.querySelector("c-common-dashboard").searchFn();
  }
}
