import { LightningElement, api, track, wire } from "lwc";
import getPublicProvidersReConsideration from "@salesforce/apex/PublicProvidersReConsideration.getPublicProvidersReConsideration";
import getPickListValues from "@salesforce/apex/CommonUtils.getPickListValues";
import * as sharedData from "c/sharedData";
import { utils } from "c/utils";
import { refreshApex } from "@salesforce/apex";
import {
  createRecord,
  deleteRecord,
  updateRecord,
  getRecord
} from "lightning/uiRecordApi";
import RECONSIDERATION_OBJECT from "@salesforce/schema/Reconsideration__c";
import RECONSIDERATION_OBJECT_TYPE from "@salesforce/schema/Reconsideration__c.ReconsiderationType__c";
import RECONSIDERATION_OBJECT_DATE from "@salesforce/schema/Reconsideration__c.ReconsiderationDate__c";
import RECONSIDERATION_OBJECT_STATUS from "@salesforce/schema/Reconsideration__c.Status__c";
import RECONSIDERATION_OBJECT_PROVIDER_ID from "@salesforce/schema/Reconsideration__c.Provider__c";
import RECONSIDERATION_OBJECT_ID from "@salesforce/schema/Reconsideration__c.Id";
import USER_ID from "@salesforce/user/Id";
import PROFILE_FIELD from "@salesforce/schema/User.Profile.Name";

const actions = [
  {
    label: "Edit",
    name: "Edit",
    iconName: "utility:edit",
    target: "_self"
  },
  {
    label: "Delete",
    name: "Delete",
    iconName: "utility:delete",
    target: "_self"
  }
];
export default class PublicProvidersReConsideration extends LightningElement {
  @api _reconsiderationModalEnable;
  @track reconsiderationTableData = [];
  @track comboBoxOption;
  @track editReconsiderationDate;
  @track editReconsiderationType;
  reconsiderationDate;
  reconsiderationType;
  refreshReconsiderationData;
  @track deleteModalOpen = false;
  @track editModalOpen = false;
  deleteDataId = "";
  editRecordId = "";
  providerId;
  userProfileName = "";
  @track showNoData = false;

  @api get columns() {
    if (this.userProfileName === "Supervisor") {
      return [
        {
          label: "Reconsideration Number",
          fieldName: "Name",
          cellAttributes: {
            class: "fontclrGrey"
          },
          type: "button",
          sortable: true,
          typeAttributes: {
            label: {
              fieldName: "Name"
            },
            target: "_self"
          }
        },
        {
          label: "Reconsideration Date",
          fieldName: "ReconsiderationDate__c",
          type: "text"
        },
        {
          label: "Reconsideration Type",
          fieldName: "ReconsiderationType__c",
          type: "text"
        },
        {
          label: "Status",
          fieldName: "Status__c",
          type: "text",
          cellAttributes: {
            class: {
              fieldName: "statusClass"
            }
          }
        }
      ];
    } else {
      return [
        {
          label: "Reconsideration Number",
          fieldName: "Name",
          cellAttributes: {
            class: "fontclrGrey"
          },
          type: "button",
          sortable: true,
          typeAttributes: {
            label: {
              fieldName: "Name"
            },
            target: "_self"
          }
        },
        {
          label: "Reconsideration Date",
          fieldName: "ReconsiderationDate__c",
          type: "text"
        },
        {
          label: "Reconsideration Type",
          fieldName: "ReconsiderationType__c",
          type: "text"
        },
        {
          label: "Status",
          fieldName: "Status__c",
          type: "text",
          cellAttributes: {
            class: {
              fieldName: "statusClass"
            }
          }
        },
        {
          type: "action",
          typeAttributes: { rowActions: actions },
          cellAttributes: {
            iconName: "utility:threedots_vertical",
            iconAlternativeText: "Actions",
            class: "tripledots"
          }
        }
      ];
    }
  }

  @api get reconsiderationBtn() {
    return this._reconsiderationModalEnable;
  }

  set reconsiderationBtn(value) {
    this.setAttribute("reconsiderationBtn", value);
    this._reconsiderationModalEnable = value;
  }

  connectedCallback() {
    this.getPickListValues();
  }

  @wire(getRecord, {
    recordId: USER_ID,
    fields: [PROFILE_FIELD]
  })
  wireuser({ error, data }) {
    if (error) {
      this.error = error;
    } else if (data) {
      this.userProfileName = data.fields.Profile.displayValue;
    }
  }

  getPickListValues = () => {
    getPickListValues({
      objInfo: {
        sobjectType: "Reconsideration__c"
      },
      picklistFieldApi: "ReconsiderationType__c"
    })
      .then((data) => {
        this.comboBoxOption = data && data;
      })
      .catch((err) => {
        this.dispatchEvent(
          utils.toastMessage("Something Went Wrong", "warning")
        );
      });
  };

  get getProviderId() {
    this.providerId = sharedData.getProviderId();
    return sharedData.getProviderId();
  }

  @wire(getPublicProvidersReConsideration, { Id: "$getProviderId" })
  getPublicProvidersReConsiderationFromApex(result) {
    this.refreshReconsiderationData = result;
    let temp = [];    
    try {
      if (this.userProfileName === "Supervisor") {
        result.data &&
          result.data.map((item) => {
            if (item.Status__c === "In progress") {
              return "";
            } else {
              temp.push({
                Id: item.Id,
                ReconsiderationType__c: item.ReconsiderationType__c,
                ReconsiderationDate__c: utils.dateFormat(
                  item.ReconsiderationDate__c
                ),
                Status__c: item.Status__c,
                statusClass: this.statusColorCodeHandler(item.Status__c),
                Name: item.Name
              });
              this.reconsiderationTableData = temp;
            }
          });
      } else {
        if(result.data.length === 0) {
          this.reconsiderationTableData = []
        }
          result.data && result.data.map((item) => {
            temp.push({
              Id: item.Id,
              ReconsiderationType__c: item.ReconsiderationType__c,
              ReconsiderationDate__c: utils.dateFormat(
                item.ReconsiderationDate__c
              ),
              Status__c: item.Status__c,
              statusClass: this.statusColorCodeHandler(item.Status__c),
              Name: item.Name
            });
            this.reconsiderationTableData = temp;
          });
      }
    } catch (err) {
      this.dispatchEvent(utils.handleError(err));
    }
    this.showNoData = this.reconsiderationTableData.length === 0;
  }

  statusColorCodeHandler = (status) => {
    if (status === "Completed") {
      return "Completed";
    } else if (status === "Draft") {
      return "Draft";
    } else if (status === "In Process") {
      return "Draft";
    } else if (status === "Scheduled") {
      return "Scheduled";
    } else if (status === "Approved") {
      return "Approved";
    } else if (status === "In progress") {
      return "Draft";
    } else if (status === "Rejected") {
      return "Rejected";
    } else if (status === "Submitted") {
      return "Submitted";
    } else if (status === "Returned") {
      return "Returned";
    } else {
      return "";
    }
  };

  handleRowAction = (event) => {
    if (event.detail.action.name === undefined) {
      sharedData.setReconsiderationId(event.detail.row.Id);
      sharedData.setReconsiderationStatus(event.detail.row.Status__c);
      const onClickId = new CustomEvent("redirecttoreconsiderationtabset", {
        detail: {
          first: false
        }
      });
      this.dispatchEvent(onClickId);
    } else {
      const actionName = event.detail.action.name;
      if (actionName === "Delete") {
        this.deleteModalOpen = true;
        this.deleteDataId = event.detail.row.Id;
      } else if (actionName === "Edit") {
        this.editReconsiderationDate = event.detail.row.ReconsiderationDate__c;
        this.editReconsiderationType =
          event.detail.row.editReconsiderationType__c;
        this.editModalOpen = true;
        this.editRecordId = event.detail.row.Id;
      } else {
        return;
      }
    }
  };
  editCloseModalFormData = () => {
    this.editModalOpen = false;
  };

  // editModalFormData start
  editModalFormData = () => {
    let fields = {};
    fields[RECONSIDERATION_OBJECT_ID.fieldApiName] = this.editRecordId;
    fields[RECONSIDERATION_OBJECT_DATE.fieldApiName] = this.reconsiderationDate;
    fields[RECONSIDERATION_OBJECT_TYPE.fieldApiName] = this.reconsiderationType;

    const recordInput = {
      fields
    };
    updateRecord(recordInput)
      .then((data) => {
        this.dispatchEvent(
          utils.toastMessage("Reconsideration Updated Successfully", "success")
        );
        this.editModalOpen = false;
        return refreshApex(this.refreshReconsiderationData);
      })
      .catch((err) => {
        this.dispatchEvent(
          utils.toastMessage("Something Went Wrong", "warning")
        );
        this.editModalOpen = false;
      });
  };
  // editModalFormData end

  handleDelete = () => {
    deleteRecord(this.deleteDataId)
      .then((data) => {
        this.dispatchEvent(
          utils.toastMessage("Reconsideration Deleted Successfully", "success")
        );
        return refreshApex(this.refreshReconsiderationData);
      })
      .catch((err) => {
        this.dispatchEvent(
          utils.toastMessage("Something Went Wrong", "warning")
        );
      });
    this.deleteModalOpen = false;
  };

  closeDeleteModal = () => {
    this.deleteModalOpen = false;
  };

  // onchangeHandler functionality start
  onchangeHandler = (event) => {
    if (event.target.name === "ReconsiderationDate") {
      this.reconsiderationDate = event.target.value;
    } else if (event.target.name === "ReconsiderationType") {
      this.reconsiderationType = event.target.value;
    } else {
      return;
    }
  };
  // onchangeHandler functionality end
  closeModalFormData = () => {
    this._reconsiderationModalEnable = false;
    this.closeDispatchModalHandler();
  };
  // closeModalFormData functional

  // saveModalFormData functionality start
  saveModalFormData = () => {
    let fields = {};

    fields[RECONSIDERATION_OBJECT_DATE.fieldApiName] = this.reconsiderationDate;
    fields[RECONSIDERATION_OBJECT_PROVIDER_ID.fieldApiName] = this.providerId;
    fields[RECONSIDERATION_OBJECT_TYPE.fieldApiName] = this.reconsiderationType;
    fields[RECONSIDERATION_OBJECT_STATUS.fieldApiName] = "In progress";

    const recordInput = {
      apiName: RECONSIDERATION_OBJECT.objectApiName,
      fields
    };

    createRecord(recordInput)
      .then((data) => {
        this.dispatchEvent(
          utils.toastMessage("Reconsideration Created Successfully", "success")
        );
        this._reconsiderationModalEnable = false;
        this.closeDispatchModalHandler();
        return refreshApex(this.refreshReconsiderationData);
      })
      .catch((err) => {
        this.dispatchEvent(
          utils.toastMessage("Something Went Wrong", "warning")
        );
        this.closeDispatchModalHandler();
      });
  };
  // saveModalFormData functionality end

  closeDispatchModalHandler = () => {
    const modalClose = new CustomEvent("addmodalclose", {
      detail: {
        modalClose: false
      }
    });
    this.dispatchEvent(modalClose);
  };
}
