/**
 * @Author        : Tamilarasan G
 * @CreatedOn     : June 1, 2020
 * @Purpose       : Public Provider Application Household Member checklist
 **/
import { LightningElement, track, wire } from 'lwc';
import getContactHouseHoldMembersDetails from "@salesforce/apex/PublicAppHouseholdMemberChecklist.getContactHouseHoldMembersDetails";
import getQuestionsByType from '@salesforce/apex/PublicAppHouseholdMemberChecklist.getQuestionsByType';
import getRecordQuestionId from '@salesforce/apex/PublicAppHouseholdMemberChecklist.getRecordQuestionId';
import getProviderAnswer from '@salesforce/apex/PublicAppHouseholdMemberChecklist.getProviderAnswer';

import getContactHouseHoldMemberAddress from '@salesforce/apex/PublicAppHouseholdMemberChecklist.getContactHouseHoldMemberAddress';
import { CJAMS_CONSTANTS } from "c/constants";

import getUploadedDocuments from '@salesforce/apex/PublicAppHouseholdMemberChecklist.getUploadedDocuments';
import getRecordType from '@salesforce/apex/PublicAppHouseholdMemberChecklist.getRecordType';
import bulkAddRecordAns from "@salesforce/apex/PublicAppHouseholdMemberChecklist.bulkAddRecordAns";


import ApplicationIdFromProRecQues from "@salesforce/schema/ProviderRecordQuestion__c.Application__c";
import ActorIdFieldFromProRecQues from "@salesforce/schema/ProviderRecordQuestion__c.Actor__c";
import RecordTypeIdFromProRecQues from "@salesforce/schema/ProviderRecordQuestion__c.RecordTypeId";

import PROVIDER_RECORD_QUESTION_API from "@salesforce/schema/ProviderRecordQuestion__c";
import {
  createRecord,
  deleteRecord,
  updateRecord
} from "lightning/uiRecordApi";

import images from "@salesforce/resourceUrl/images";

import { utils } from "c/utils";
import * as sharedData from 'c/sharedData';

import APPLICATIONSTATUS_FIELD from '@salesforce/schema/Application__c.Status__c';

import { getRecord } from 'lightning/uiRecordApi';


export default class PublicApplicationHouseholdMemberChecklist extends LightningElement {

  @track toggledivForHouseholdMember = false;
  @track dropdownSelectedValue = '';
  @track dropdownSelectedValueStatus = '';
  @track dropdownSelectedValueStatusColor = '';
  @track selectHouseholdMemberDetails;
  @track selectHouseholdMemberOptions = [];
  @track selectedId;
  @track houseHoldType;
  @track houseHoldCJAMSPID;
  @track houseHoldDOB;
  @track houseHoldAge;
  @track houseHoldGender;
  @track houseHoldAddress;
  @track HouseholdProfilePhoto;
  @track ContactID;
  @track RecordTypeID;
  @track ActorID;
  @track providerQuestionId;
  @track answerUpdate = false;
  @track getProviderQuestionIdfromBegining;
  @track allHouseholdMembersChecklistAnswers;
  @track providerHomeInfoAddress;
  // @track type = 'Household Member';
  @track receivedDate = utils.formatDateYYYYMMDD(new Date());
  @track downloadImage = '/sfc/servlet.shepherd/version/download/0680w000000B6GkAAK';
  @track SuperVisorFlowBtnDisable = false;
  @track ApplicationStatus;
  @track Spinner = false;



  @track questions = [];

  @track noimages = images + "/user.svg";



  get applicationId() {
    return sharedData.getApplicationId();
  }

  //This function used to get household member from Provider Id
  get getProviderId() {
    return sharedData.getProviderId();
  }

  get getApplicationStatus() {
    return sharedData.getApplicationStatus();
  }

  get getUserProfileName() {
    return sharedData.getUserProfileName();
  }
  connectedCallback() {
    getContactHouseHoldMemberAddress({
      providerId: this.getProviderId
    }).then((result) => {
      if (result.length > 0) {
        this.providerHomeInfoAddress = result[0].AddressLine1__c != undefined && result[0].AddressLine2__c != undefined ? result[0].AddressLine1__c + ', ' + result[0].AddressLine2__c : result[0].AddressLine1__c != undefined ? result[0].AddressLine1__c : '';
      }
    }).catch(errors => {
      let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
      return this.dispatchEvent(utils.handleError(errors, config));
    });
    this.getAllContactDetails();
  }

  //fetch data from contact obj and show in home.
  getAllContactDetails() {
    // if(!this.getProviderId)
    //   return;

    getContactHouseHoldMembersDetails(
      {
        providerId: this.getProviderId
      }
    ).then((data) => {
      let contactID = [];
      if (data.length > 0) {
        var selectHouseholdOptions = [];
        data.forEach((row) => {
          contactID.push(row.Contact__r.Id);
          selectHouseholdOptions.push(
            {
              id: row.Id,
              contactID: row.Contact__r.Id,
              label: row.Contact__r.FirstName__c + ' ' + row.Contact__r.LastName__c,
              value: row.Contact__r.FirstName__c + ' ' + row.Contact__r.LastName__c,
              status: 'Pending',
              statusColor: 'house-hold-status-orangecolor',
              Role__c: row.Role__c,
              SSN__c: row.Contact__r.SSN__c,
              Gender__c: row.Contact__r.Gender__c,
              ApproximateAge__c: row.Contact__r.ApproximateAge__c,
              DOB__c: utils.formatDate(row.Contact__r.DOB__c),
              ContactNumber__c: row.Contact__r.ContactNumber__c
            });
        });
        this.selectHouseholdMemberOptions = selectHouseholdOptions;
        this.selectedId = this.selectHouseholdMemberOptions[0].id;
        this.ContactID = contactID.toString();
        this.getImagesforHouseholdMember();

      }
    }).catch(errors => {
      let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
      return this.dispatchEvent(utils.handleError(errors, config));
    });
  }

  getImagesforHouseholdMember() {
    getUploadedDocuments({ strDocumentID: this.ContactID }).then((result) => {

      var DataOfImages = result;

      //Images For House listed For Card function

      this.selectHouseholdMemberOptions.forEach((element) => {
        DataOfImages.forEach((Key) => {
          if (element.contactID == Key.LinkedEntityId) {
            element.HouseholdProfilePhoto = '/sfc/servlet.shepherd/version/download/' + Key.ContentDocument.LatestPublishedVersionId;
          }
        });
        if (!element.HouseholdProfilePhoto) {
          element.HouseholdProfilePhoto = this.noimages;
        }
      });

    }).catch(errors => {
      let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
      return this.dispatchEvent(utils.handleError(errors, config));
    });
  }

  getProviderQuestionID() {
    getRecordQuestionId({ appId: this.applicationId, recordTypeId: this.RecordTypeID }).then((result) => {
      var arraytoString = [];
      result.forEach((item) => {
        arraytoString.push(item.Id);
      });
      if (arraytoString.length > 0) {
        this.getProviderQuestionIdfromBegining = arraytoString.toString();
        this.getProviderAnswerDetails();
      }
      else {
        this.selectHouseholdMember(this.selectedId);
      }
    }).catch(errors => {
      let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
      return this.dispatchEvent(utils.handleError(errors, config));
    });

  }

  getProviderAnswerDetails() {
    getProviderAnswer({ getAnswers: this.getProviderQuestionIdfromBegining }).then((result) => {
      this.allHouseholdMembersChecklistAnswers = result;
      this.selectHouseholdMember(this.selectedId);
      this.dropDownValueColorChange();
    }).catch(errors => {
      let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
      return this.dispatchEvent(utils.handleError(errors, config));
    });
    this.selectHouseholdMember(this.selectedId);
  }

  handleHouseholdMember() {
    this.toggledivForHouseholdMember = true;
  }

  handleMouseOutButtonHouseholdMember() {
    this.toggledivForHouseholdMember = false;
  }
  selectHouseholdMember(event) {
    this.selectedId = event.target != undefined ? event.target.id : this.selectedId;

    this.selectHouseholdMemberOptions.forEach((item) => {
      if (this.selectedId.includes(item.id)) {
        this.ActorID = item.id;
        this.dropdownSelectedValue = item.value;
        this.houseHoldType = item.Role__c;
        this.houseHoldCJAMSPID = item.ContactNumber__c;
        this.houseHoldDOB = item.DOB__c;
        this.houseHoldAge = item.ApproximateAge__c;
        this.houseHoldGender = item.Gender__c;
        this.houseHoldAddress = this.providerHomeInfoAddress;
        this.HouseholdProfilePhoto = item.HouseholdProfilePhoto;
        this.dropDownValueColorChange();
        this.dropdownSelectedValueStatus = item.status;
        this.dropdownSelectedValueStatusColor = item.status == 'Pending' ? 'house-hold-status-orangecolor' : 'house-hold-status-greencolor';

      }
    });
    this.dropDownValueChangeandAnswerUpdateIDset();
    this.toggledivForHouseholdMember = false;
  }

  questionAnswerRefresh() {
    this.questions.forEach((item) => {
      item.AnsComments = '';
      item.AnsStatus = '';
      item.AnsDate = utils.formatDateYYYYMMDD(new Date());
      item.Comments__c = '';
      item.HouseholdStatus__c = '';
      item.Date__c = utils.formatDateYYYYMMDD(new Date());
      item.AnswerId = undefined;
    });
  }

  dropDownValueChangeandAnswerUpdateIDset() {
    if (this.allHouseholdMembersChecklistAnswers != undefined) {

      let allHouseholdMembersChecklistAnswersFilteredArray = this.allHouseholdMembersChecklistAnswers.filter((item) => {
        return this.selectedId.includes(item.Actor__c);
      });


      if (allHouseholdMembersChecklistAnswersFilteredArray.length > 0) {
        allHouseholdMembersChecklistAnswersFilteredArray.forEach((item, index) => {
          if (this.selectedId.includes(item.Actor__c)) {
            this.questions[index].AnsComments = item.Comments__c != undefined ? item.Comments__c : '';
            this.questions[index].AnsStatus = item.HouseholdStatus__c != undefined ? item.HouseholdStatus__c : '';
            this.questions[index].AnsDate = item.HouseholdStatus__c != undefined ? item.Date__c : utils.formatDateYYYYMMDD(new Date());
            this.questions[index].AnswerId = item.Id;
            this.questions[index].Comar__c = item.Comar__c;
            this.questions[index].Date__c = item.Date__c != undefined ? item.Date__c : utils.formatDateYYYYMMDD(new Date());
            this.questions[index].Comments__c = item.Comments__c;
            this.questions[index].HouseholdStatus__c = item.HouseholdStatus__c;

            this.providerQuestionId = item.ProviderRecordQuestion__c;
          }
        });
        this.answerUpdate = true;
      }
      else {
        this.questionAnswerRefresh();
        this.answerUpdate = false;

      }
    }
    else {
      this.questionAnswerRefresh();
      this.answerUpdate = false;
    }
  }

  dropDownValueColorChange() {

    if (this.allHouseholdMembersChecklistAnswers != undefined) {

      const answerArray = Object.values(
        this.allHouseholdMembersChecklistAnswers.reduce((a, c) => {
          a[c.Actor__c] = c;
          return a
        }, {}))

      this.selectHouseholdMemberOptions.forEach((item) => {
        let flag = false;
        answerArray.forEach((key) => {
          if (key.Actor__c == item.id) {
            flag = true;
          }
        });

        if (flag) {
          item.statusColor = 'house-hold-status-greencolor';
          item.status = 'Completed';
        }
        else {
          item.statusColor = 'house-hold-status-orangecolor';
          item.status = 'Pending';
        }


      });
    }
    else {
      this.selectHouseholdMemberOptions.forEach((item) => {
        this.dropdownSelectedValueStatus = 'Pending';
        this.dropdownSelectedValueStatusColor = 'house-hold-status-orangecolor';
        item.statusColor = 'house-hold-status-orangecolor';
        item.status = 'Pending';
      });
    }
  }

  get options() {
    return [
      { label: 'Yes', value: 'Yes' },
      { label: 'No', value: 'No' },
    ];
  }

  statusChange(event) {
    if (event.target != undefined && event.target.name != undefined && event.target.name != null) {
      this.questions[event.target.name].HouseholdStatus__c = event.target.value;
    }
  }
  receivedDateChange(event) {
    if (event.target != undefined && event.target.name != undefined && event.target.name != null) {
      this.questions[event.target.name].Date__c = event.target.value;
    }
  }
  commentsChange(event) {
    if (event.target != undefined && event.target.name != undefined && event.target.name != null) {
      this.questions[event.target.name].Comments__c = event.target.value;
    }
  }
  handleSubmit() {
    if (this.answerUpdate) {
      this.answerRecordSave();
    }
    else {
      this.questionRecordSave();
    }
  }

  questionRecordSave() {

    let fields = {};
    fields[ApplicationIdFromProRecQues.fieldApiName] = this.applicationId;
    fields[ActorIdFieldFromProRecQues.fieldApiName] = this.ActorID;
    fields[RecordTypeIdFromProRecQues.fieldApiName] = this.RecordTypeID;

    const recordInput = {
      apiName: PROVIDER_RECORD_QUESTION_API.objectApiName,
      fields
    };

    createRecord(recordInput)
      .then((response) => {
        if (response) {
          this.providerQuestionId = response.id;
          this.answerRecordSave();
        }

      })
      .catch(errors => {
        let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
        return this.dispatchEvent(utils.handleError(errors, config));
      });

  }
  answerRecordSave() {
    this.Spinner = true;

    var questionanswerarray = [];

    this.questions.forEach((item) => {
      questionanswerarray.push(
        {
          'id': item.AnswerId,
          'Comar__c': item.Comar__c,
          'Date__c': item.Date__c,
          'Actor__c': this.ActorID,
          'ProviderRecordQuestion__c': this.providerQuestionId,
          'HouseholdStatus__c': item.HouseholdStatus__c,
          'Comments__c': item.Comments__c
        }
      );
    });
    let questionstosave = JSON.stringify(questionanswerarray);

    bulkAddRecordAns({
      datas: questionstosave
    })
      .then(result => {
        this.getProviderQuestionID();
        this.Spinner = false;
        this.dispatchEvent(utils.toastMessage("Household member checklist saved successfully", "success"));
      }).catch(errors => {
        this.Spinner = false;
        let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
        return this.dispatchEvent(utils.handleError(errors, config));
      });


  }

  @wire(getQuestionsByType)
  wiredquestions(data) {
    if (data.data != null && data.data != '') {
      let currentData = [];
      let id = 0;
      data.data.forEach((row) => {
        let rowData = {};
        rowData.rowId = id;
        rowData.Comar__c = row.Id;
        rowData.Date__c = utils.formatDateYYYYMMDD(new Date());
        rowData.Question__c = row.Question__c;
        currentData.push(rowData);
        id++;
      });
      this.questions = currentData;
    }
  }

  @wire(getRecordType, { recordType: "Household Member" })
  recordTypeDetails(result) {
    try {
      if (result.data) {
        this.RecordTypeID = result && result.data;
      }
    } catch (err) {
      this.dispatchEvent(utils.handleError(err));
    }
  }  

  @wire(getRecord, {
    recordId: "$applicationId",
    fields: [APPLICATIONSTATUS_FIELD]
  })
  wireuser({
    error,
    data
  }) {
    if (error) {
      this.error = error;
    } else if (data) {
      this.ApplicationStatus = data.fields.Status__c.value;
      if (this.ApplicationStatus) {
        if (this.getUserProfileName != 'Supervisor') {
          if (['Caseworker Submitted', 'Approved', 'Rejected', 'Supervisor Rejected'].includes(this.ApplicationStatus)) {
            this.SuperVisorFlowBtnDisable = true;
            // this.template.querySelector(".supervisor-disable").classList.add("disabledbutton");
          }
          else {
            this.SuperVisorFlowBtnDisable = false;
          }
        }
        if (this.getUserProfileName == 'Supervisor') {
          this.SuperVisorFlowBtnDisable = true;
          // this.template.querySelector(".supervisor-disable").classList.add("disabledbutton");
        }
      }
    }
  }

}