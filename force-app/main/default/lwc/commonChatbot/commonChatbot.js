import {
    LightningElement,
    track
} from 'lwc';
import postChatText from "@salesforce/apex/ChatBotHandler.postChatText";

export default class CommonChatbot extends LightningElement {

    @track chatList = [];
    @track userChat;
    @track showLoader = false;

    connectedCallback() {
        this.postToChatAndGetResponse('Hi');
    }

    postUserChat(evt) {
        this.userChat = evt.detail.value;

    }
    keycheck(event) {
        if (event.which == 13) {
            this.buildChatList(this.userChat, 'ME: ', 'slds-float_left', 'blockleft-txt');
            this.postToChatAndGetResponse(this.userChat);
            this.userChat = null;
            this.showLoader = true;
        }
    }
    scrollToText() {
        let scrollToMessages = this.template.querySelector(".myscroll");
        setTimeout(() => {
            scrollToMessages.scrollTop = scrollToMessages.scrollHeight;
        }, 10);
    }
    buildChatList(dataToPush, typeOfUser, classType, classType1) {
        this.chatList.push({
            "chatText": dataToPush,
            "userName": typeOfUser,
            "className1": 'slds-badge ' + classType,
            "className2": classType1,

        });
        this.scrollToText();
    }
    postToChatAndGetResponse(textToPost) {
        postChatText({
                schatText: textToPost
            })
            .then(result => {
                this.buildChatList(result, 'Red Bird Says: ', 'slds-float_right slds-theme_warning bgcolor', 'blockright-txt');
                this.userChat = null;
                this.showLoader = false;
              
            })
    }

}