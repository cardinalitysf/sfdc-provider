/**
 * @Author        : Jayachandran
 * @CreatedOn     : August 08,2020
 * @Purpose       : Incident Decision With Card Approval process For Incident Reporting(CaseOBJ)
 * @updatedBy     :
 * @updatedOn     :
 **/
import { LightningElement, wire, api } from 'lwc';
import fetchPickListValue from '@salesforce/apex/IncidentDecision.fetchPickListValue';
import images from '@salesforce/resourceUrl/images';
import saveSign from '@salesforce/apex/IncidentDecision.saveSign';
import updateIncidentdetailsDraft from '@salesforce/apex/IncidentDecision.updateIncidentdetailsDraft';
import updateIncidentcasedetails from '@salesforce/apex/IncidentDecision.updatecasedetails';
import getDatatableDetails from '@salesforce/apex/IncidentDecision.getDatatableDetails';
import getIncidentPersonChildDetails from '@salesforce/apex/IncidentDecision.getIncidentPersonChildDetails';
import getWitnessDetails from '@salesforce/apex/IncidentDecision.getWitnessDetails';
import getActorDetails from '@salesforce/apex/IncidentDecision.getActorDetails';
import getReferralData from '@salesforce/apex/IncidentDecision.getReferralData';
import getReviewDetails from "@salesforce/apex/IncidentDecision.getReviewDetails";

import * as shareddata from 'c/sharedData';
import { USER_PROFILE_NAME, CJAMS_CONSTANTS } from 'c/constants';
import { utils } from "c/utils";

// declaration of variables
// for calculations
let isDownFlag,
    isDotFlag = false,
    prevX = 0,
    currX = 0,
    prevY = 0,
    currY = 0;

let x = "#0000A0"; //blue color
let y = 1.5; //weight of line width and dot.       

let canvasElement, ctx; //storing canvas context
let attachment; //holds attachment information after saving the sigture on canvas
let dataURL, convertedDataURI; //holds image data

const dataTableColumns = [{
    label: 'Date',
    fieldName: 'SubmittedDate__c'
},
{
    label: 'Author',
    fieldName: 'authorName'
},
{
    label: 'Designation',
    fieldName: 'authorDesignation'
},
{
    label: 'Status',
    fieldName: 'status'
},
{
    label: 'Assigned To',
    fieldName: 'assignedToName'
},
{
    label: 'Designation',
    fieldName: 'supervisorDesignation'
},
{
    label: 'Action',
    fieldName: 'id',
    type: "button",
    typeAttributes: {
        iconName: 'utility:preview',
        name: 'View',
        title: 'Click to Preview',
        variant: 'border-filled',
        class: 'view-red',
        disabled: false,
        iconPosition: 'left',
        target: '_self'
    }
},
]

export default class IncidentDecision extends LightningElement {
    statusOptions;
    ApprovalStatus__c;
    ApprovalCommentsvalue;
    SignatureValidationView;
    addsign = false;
    Status;

    //flags
    bShowsignatureModal = false;
    openmodel = false;
    isOpenModal = false;
    actionShowModal = false;
    SignatureShowHide = false;
    @api personkidDecisonFlag = false;
    @api wittnessDecisonFlag;
    @api staffDecisionFlag;
    isSaveAsDraftDisable = false;

    refreshIcon = images + '/refresh-icon.svg';
    decisionIcon = images + '/decision-icon.svg';
    dataTableColumns = dataTableColumns;
    HideDatatable = false;
    currenttablepage = [];
    isSubmitBtnDisable = false;

    get caseId() {
        return shareddata.getCaseId();
    }

    get CaseworkerId() {
        return shareddata.getCaseworkerId();
    }

    get incidentStatus() {
        return shareddata.getIncidentStatus();
    }

    get profileName() {
        return shareddata.getUserProfileName();
    }
    connectedCallback() {
        if (this.profileName == USER_PROFILE_NAME.USER_PROVIDER_COMMUNITY || this.profileName == USER_PROFILE_NAME.USER_INTAKE_WRKR || this.profileName == USER_PROFILE_NAME.USER_SYS_ADMIN) {
            this.HideDatatable = false;
            let arr = ['Submitted','Submitted to Caseworker','Rejected','Acknowledged'];
            if (arr.includes(this.incidentStatus !== undefined ? this.incidentStatus : "Draft")) {
                this.isDisable = true;
                this.isDisablesignature = true;
                this.isSaveAsDraftDisable = true;
                this.isSubmitBtnDisable = true;
                this.HideDatatable = true;
            }
        }

        else if (this.profileName == USER_PROFILE_NAME.USER_CASE_WRKR) {
            this.HideDatatable = true;
            let arr = ['Acknowledged', 'Rejected'];
            if (arr.includes(this.incidentStatus)) {
                this.isDisable = true;
                this.isDisablesignature = true;
                this.isSaveAsDraftDisable = true;
                this.isSubmitBtnDisable = true;
            }
        } 
        this.getDatatablevalues();
        this.getPersondata();
        this.getWittness();
        this.getActorDetails();
        this.getReferralData();
        this.getReviewDetailsInitialLoad();
    }

    
    getReviewDetailsInitialLoad() {
        getReviewDetails({
            caseid: this.caseId
        })
            .then(result => {
                if (result.length > 0) {
                    if (this.profileName == USER_PROFILE_NAME.USER_CASE_WRKR) {
                        this.ApprovalStatus__c = result[0].CaseworkerDecisionStatus__c;
                        this.ApprovalCommentsvalue = result[0].CaseworkerComments__c;
                    } else if (this.profileName == USER_PROFILE_NAME.USER_INTAKE_WRKR || this.profileName == USER_PROFILE_NAME.USER_SYS_ADMIN || this.profileName == USER_PROFILE_NAME.USER_PROVIDER_COMMUNITY) {
                        this.ApprovalStatus__c = result[0].IntakeDecisionStatus__c;
                        this.ApprovalCommentsvalue = result[0].IntakeComments__c;
                    } 
                    this.Spinner = false;
                }
            }).catch(errors => {
                return this.dispatchEvent(utils.handleError(errors));
            })
    }

    @wire(fetchPickListValue, {
        objInfo: {
            sobjectType: "Case"
        },
        pickListFieldApi: "CommonStatus__c"
    })
    wiredProfitDetails({
        error,
        data
    }) {
        if (data) {
            let statusType = [];
            if (this.profileName == 'Caseworker') {
                statusType = data.filter(item => item.label != 'Approved' && item.label != 'Submit for Acknowledgement' && item.label != 'Submit for Review');
                this.statusOptions = statusType;
            } else {
                statusType = data.filter(item => item.label != 'Approved' && item.label != 'Submit for Acknowledgement' && item.label != 'Acknowledge');
                this.statusOptions = statusType;
            }
        } else if (error) {

        }
    }

 
    //handle on change
    changeStatusHandler(event) {
        this.ApprovalStatus__c = event.detail.value;
    }
    commentschangeHandler(event) {
        this.ApprovalCommentsvalue = event.detail.value;
    }
    signaturemodal() {
        // to open modal window set 'bShowsignatureModal' track value as true
        this.bShowsignatureModal = true;
    }
    signatureCloseModal() {
        this.bShowsignatureModal = false; // to close modal window set 'bShowsignatureModal' tarck value as false
    }

    DoneModal() {
        this.openmodel = false;
    }
    handleCloseModal() {
        this.isOpenModal = false;
    }
    actionCloseModal() {
        this.actionShowModal = false;
    }

    //Vaidation Data 

    getPersondata() {
        getIncidentPersonChildDetails({
            referID: this.caseId,
        })
            .then((result) => {
                if (result.length > 0) {
                    this.directPersonKidFlag = true;
                } else {
                    this.directPersonKidFlag = false;
                }
            }).catch(errors => {
                return this.dispatchEvent(utils.handleError(errors));
            })

    };

    getWittness() {
        getWitnessDetails({
            IncidentId: this.caseId,
        })
            .then((result) => {
                if (result.length > 0) {
                    this.directWitnessFlag = true;
                } else {
                    this.directWitnessFlag = false;
                }
            }).catch(errors => {
                return this.dispatchEvent(utils.handleError(errors));
            })
    }

    getActorDetails() {
        getActorDetails({
            caseId: this.caseId,
        })
            .then((result) => {
                if (result.length > 0) {
                    this.directStaffFlag = true;
                } else {
                    this.directStaffFlag = false;
                }
            }).catch(errors => {
                return this.dispatchEvent(utils.handleError(errors));
            })
    }

    getReferralData(){
        getReferralData({
            referralId: this.caseId,
        })
            .then((result) => {
                if (result.length > 0) {
                    if(result[0].IncidentType__c !== undefined){
                        this.directIncidentFlag = false;
                    }else {
                        this.directIncidentFlag = true;
                    }
                } 
                
               
            }).catch(errors => {
                return this.dispatchEvent(utils.handleError(errors));
            })
    }

    // Save As Draft 
    saveAsDraftMethod() {
        if (!this.ApprovalStatus__c)
            return this.dispatchEvent(utils.toastMessage("Please Select Status", "Warning"));

        let myObj = {
            'sobjectType': 'Case'
        };

        if (this.profileName == USER_PROFILE_NAME.USER_PROVIDER_COMMUNITY || this.profileName == USER_PROFILE_NAME.USER_SYS_ADMIN) {
            myObj.IntakeDecisionStatus__c = this.ApprovalStatus__c;
            myObj.IntakeComments__c = this.ApprovalCommentsvalue;
        } else if (this.profileName == USER_PROFILE_NAME.USER_CASE_WRKR) {
            myObj.CaseworkerDecisionStatus__c = this.ApprovalStatus__c;
            myObj.CaseworkerComments__c = this.ApprovalCommentsvalue;
        }

        if (this.caseId)
            myObj.Id = this.caseId;
        
        updateIncidentdetailsDraft({
            objSobjecttoUpdateOrInsert: myObj
        })
            .then(result => {
                this.actionShowModal = true;
                this.dispatchEvent(utils.toastMessage("Incident has been saved successfully !", "success"));
                window.location.reload();
            })
            .catch(error => {
                this.actionShowModal = false;
                this.error = error.message;
            });
    }

    submitMethod() {
        if (!this.ApprovalStatus__c)
            return this.dispatchEvent(utils.toastMessage("Please Select Status", "Warning"));
        if (this.addsign == false)
            return this.dispatchEvent(utils.toastMessage("Signature Is Mandatory", "Warning"));

        if (this.profileName == USER_PROFILE_NAME.USER_PROVIDER_COMMUNITY || this.profileName == USER_PROFILE_NAME.USER_INTAKE_WRKR || USER_PROFILE_NAME.USER_SYS_ADMIN) {
            if (this.directIncidentFlag)
                return this.dispatchEvent(utils.toastMessageWithTitle('Decision', 'Incident Not Found', 'Warning'));
                let x= (this.directPersonKidFlag) ? false : (this.personkidDecisonFlag) ? false : true;
            if (x)
                return this.dispatchEvent(utils.toastMessageWithTitle('Decision', 'Person/Kid/Child Information Not Found', 'Warning'));
                let y = (this.directWitnessFlag) ? false : (this.wittnessDecisonFlag) ? false : true;
            if (y)
                return this.dispatchEvent(utils.toastMessageWithTitle('Decision', 'Witness Incident Not Found', 'Warning'));
            let z =(this.directStaffFlag) ? false : (this.staffDecisionFlag) ? false : true;
                if (z)
                return this.dispatchEvent(utils.toastMessageWithTitle('Decision', 'Staff Details Not Found', 'Warning'));
        }

        this.submitMethod1();
    }

    submitMethod1() {
        let myObj = {
            'sobjectType': 'Case'
        };
        if (this.profileName == USER_PROFILE_NAME.USER_PROVIDER_COMMUNITY || this.profileName == USER_PROFILE_NAME.USER_INTAKE_WRKR || this.profileName == USER_PROFILE_NAME.USER_SYS_ADMIN) {
            myObj.IntakeDecisionStatus__c = this.ApprovalStatus__c;
            myObj.IntakeComments__c = this.ApprovalCommentsvalue;
            this.Status = (this.ApprovalStatus__c == 'Submit for Review') ? 'Submitted to Caseworker' : this.ApprovalStatus__c;
        } else if (this.profileName == USER_PROFILE_NAME.USER_CASE_WRKR) {
            myObj.CaseworkerDecisionStatus__c = this.ApprovalStatus__c;
            myObj.CaseworkerComments__c = this.ApprovalCommentsvalue;
            this.Status = (this.ApprovalStatus__c == 'Acknowledge') ? 'Approve' : 'Reject';
        }
        var today = new Date();
        var monthDigit = today.getMonth() + 1;
        if (monthDigit <= 9) {
            monthDigit = '0' + monthDigit;
        }
        var dayDigit = today.getDate();
        if (dayDigit <= 9) {
            dayDigit = '0' + dayDigit;
        }
        var date = today.getFullYear() + "-" + monthDigit + "-" + dayDigit;
        myObj.SubmittedDate__c = date;
        if (this.caseId)
            myObj.Id = this.caseId;
        let workItemId = this.workItemId === '' ? '' : this.workItemId;
        
        
        updateIncidentcasedetails({
            objSobjecttoUpdateOrInsert: myObj,
            nextApproverId: this.CaseworkerId,
            workItemId: workItemId,
            status: this.Status
        })
            .then(result => {
                this.actionShowModal = true;
                this.dispatchEvent(utils.toastMessage("Incident has been saved successfully !", "success"));
                window.location.reload();
            })
            .catch(error => {
                
                this.actionShowModal = false;
                this.error = error.message;
            });
    }

    dataAction(event) {
        let actiondt = event.detail.row;
        this.AuthorValue = event.detail.row.authorName;
        this.DateValue = event.detail.row.SubmittedDate__c;
        this.StatusValue = event.detail.row.status;
        this.ApprovalCommentsvalueview = event.detail.row.ApprovalCommentsvalue;
        this.SignatureValue = '/sfc/servlet.shepherd/version/download/' + event.detail.row.SignatureValue;
        this.openmodel = true;
    }

    getDatatablevalues() {
        getDatatableDetails({
            caseid: this.caseId
        })
            .then(result => {
                
                let resultArray = [];
                result = JSON.parse(result);
                if (result.length > 0) {
                    
                    this.SignatureValidation = result[0].signatureUrl;
                    this.workItemId = result[0].cases != undefined ? (result[0].cases.Workitems != undefined ? result[0].cases.Workitems.records[0].Id : '') : '';
                    for (var i = 0; i < result.length; i++) {
                        if (result[i].cases.StepsAndWorkitems !== undefined) {
                            let datas = result[i].cases.StepsAndWorkitems.records;
                            let customStatus = '';
                            for (let data in datas) {
                                let signUrlIndex = result[i].signatureUrl.findIndex(sign => sign.OwnerId === datas[data].ActorId);
                                let signUrl = signUrlIndex >= 0 ? result[i].signatureUrl[signUrlIndex].Id : '';
                                resultArray.push({
                                    authorName: datas[data].Actor.Name,
                                    status: (datas[data].ProcessNode == undefined) ? CJAMS_CONSTANTS.COMPLAINT_STATUS_SUBMITTOCW :
                                        (datas[data].StepStatus == 'Provider Submitted') ? CJAMS_CONSTANTS.CONTRACT_STATUS_INPROCESS :
                                            // (datas[data].ProcessNode.Name == '1st Approval') ? CJAMS_CONSTANTS.APPLICATION_SUBMITTED_TO_SUPERVISOR :
                                            //(datas[data].StepStatus == CJAMS_CONSTANTS.COMPLAINT_DROPDOWN_REMOVED) ? CJAMS_CONSTANTS.CONTRACT_STATUS_RETURNED 
                                            //:
                                            datas[data].StepStatus,
                                    assignedToName: datas[parseInt(data) - 1] !== undefined ? datas[parseInt(data) - 1].Actor.Name : ' - ',
                                    ApprovalCommentsvalue: datas[data].Comments,
                                    authorDesignation: (datas[data].Actor.Profile.Name == USER_PROFILE_NAME.USER_SYS_ADMIN) ? USER_PROFILE_NAME.USER_INTAKE_WRKR : datas[data].Actor.Profile.Name,
                                    supervisorDesignation: datas[parseInt(data) - 1] !== undefined ? (datas[data - 1].Actor.Profile.Name == USER_PROFILE_NAME.USER_SYS_ADMIN) ? USER_PROFILE_NAME.USER_INTAKE_WRKR : datas[data - 1].Actor.Profile.Name : ' - ',
                                    SubmittedDate__c: utils.formatDate(datas[data].CreatedDate),
                                    SignatureValue: signUrl
                                });
                            }
                        }
                    }
                }
                this.currenttablepage = resultArray;
                if (resultArray.length > 0) {
                    this.SignatureShowHide = true;
                    if (resultArray.length > 2) {
                        if (this.profileName == USER_PROFILE_NAME.USER_CASE_WRKR) {
                            this.addsign = true;
                            this.SignatureValidationView = '/sfc/servlet.shepherd/version/download/' + resultArray[resultArray.length - 2].SignatureValue;
                        } else {
                            this.addsign = true;
                            this.SignatureValidationView = '/sfc/servlet.shepherd/version/download/' + resultArray[resultArray.length - 1].SignatureValue;
                        }
                    } else {
                        if (this.profileName == USER_PROFILE_NAME.USER_CASE_WRKR) {
                            this.SignatureShowHide = false;
                        } else {
                            this.addsign = true;
                            this.SignatureValidationView = '/sfc/servlet.shepherd/version/download/' + resultArray[resultArray.length - 1].SignatureValue;
                        }
                    }
                } else {
                    this.SignatureShowHide = false;
                }
            }).catch(errors => {
                return this.dispatchEvent(utils.handleError(errors));
            })
    }

    closeModalAction() {
        this.openmodel = false;
    }

    ////////////////////////////////////////signature//////////////////////////////////////////////

    @api recordId;

    //event listeners added for drawing the signature within shadow boundary
    constructor() {
        super();
        this.template.addEventListener('mousemove', this.handleMouseMove.bind(this));
        this.template.addEventListener('mousedown', this.handleMouseDown.bind(this));
        this.template.addEventListener('mouseup', this.handleMouseUp.bind(this));
        this.template.addEventListener('mouseout', this.handleMouseOut.bind(this));
    }

    //retrieve canvase and context
    renderedCallback() {
        canvasElement = this.template.querySelector('canvas');
        setTimeout(function () {
            ctx = canvasElement.getContext("2d");
        }, 500);
    }

    //handler for mouse move operation
    handleMouseMove(event) {
        this.searchCoordinatesForEvent('move', event);
    }

    //handler for mouse down operation
    handleMouseDown(event) {
        this.searchCoordinatesForEvent('down', event);
    }

    //handler for mouse up operation
    handleMouseUp(event) {
        this.searchCoordinatesForEvent('up', event);
    }

    //handler for mouse out operation
    handleMouseOut(event) {
        this.searchCoordinatesForEvent('out', event);
    }

    //clear the signature from canvas
    handleClearClick() {
        this.SignatureShowHide = false;
        ctx.clearRect(0, 0, canvasElement.width, canvasElement.height);
    }

    searchCoordinatesForEvent(requestedEvent, event) {
        // event.preventDefault();
        if (requestedEvent === 'down') {
            this.setupCoordinate(event);
            isDownFlag = true;
            isDotFlag = true;
            if (isDotFlag) {
                this.drawDot();
                isDotFlag = false;
            }
        }
        if (requestedEvent === 'up' || requestedEvent === "out") {
            isDownFlag = false;
        }
        if (requestedEvent === 'move') {
            if (isDownFlag) {
                this.setupCoordinate(event);
                this.redraw();
            }
        }
    }

    //This method is primary called from mouse down & move to setup cordinates.
    setupCoordinate(eventParam) {
        //get size of an element and its position relative to the viewport 
        //using getBoundingClientRect which returns left, top, right, bottom, x, y, width, height.
        const clientRect = canvasElement.getBoundingClientRect();
        prevX = currX;
        prevY = currY;
        currX = eventParam.clientX - clientRect.left;
        currY = eventParam.clientY - clientRect.top;
    }

    //For every mouse move based on the coordinates line to redrawn
    redraw() {
        ctx.beginPath();
        ctx.moveTo(prevX, prevY);
        ctx.lineTo(currX, currY);
        ctx.strokeStyle = x; //sets the color, gradient and pattern of stroke
        ctx.lineWidth = y;
        ctx.closePath(); //create a path from current point to starting point
        ctx.stroke(); //draws the path
    }

    //this draws the dot
    drawDot() {
        ctx.beginPath();
        ctx.fillStyle = x; //blue color
        ctx.fillRect(currX, currY, y, y); //fill rectrangle with coordinates
        ctx.closePath();
    }
    handleFilesChange(event) {
        let strFileNames = '';
        // Get the list of uploaded files
        const uploadedFiles = event.detail.files;

        for (let i = 0; i < uploadedFiles.length; i++) {
            strFileNames += uploadedFiles[i].name + ', ';

        }
        this.dispatchEvent(utils.toastMessage(strFileNames + " Signature uploaded Successfully", "success"));
    }
    /*
        handler to perform save operation.
        save signature as attachment.
        after saving shows success or failure message as toast
    */
    handleSaveClick() {
        //
        //set to draw behind current content
        ctx.globalCompositeOperation = "destination-over";
        ctx.fillStyle = "#FFF"; //white
        ctx.fillRect(0, 0, canvasElement.width, canvasElement.height);

        //convert to png image as dataURL
        dataURL = canvasElement.toDataURL("image/png");
        //convert that as base64 encoding
        convertedDataURI = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");

        //call Apex method imperatively and use promise for handling sucess & failure
        saveSign({
            strSignElement: convertedDataURI,
            recId: this.caseId
        })
            .then(result => {
                this.addsign = true;
                this.signatureCloseModal();
                this.dispatchEvent(utils.toastMessage("Signature saved Successfully", "success"));
            }).catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });

    }
    //Signature Code End
}