import { LightningElement, track } from 'lwc';

//Toast Message
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

//Importing Field
import NARRATIVE from '@salesforce/schema/Case.Description';

//Importing Apex Methods
import saveCaseRecord from '@salesforce/apex/DMLOperationsHandler.updateOrInsertSOQL';

export default class NarrativeDetails extends LightningElement {
    @track error;
    @track result;

    // this object have record information
    casrecord = {
        Description: NARRATIVE

    }
    handlechange(event) {
        this.casrecord.Description = event.target.value;
    }

    //Save Functionality
    handleSave() {
        let myobjcase = { 'sobjectType': 'Case' };
        myobjcase.Description = this.casrecord.Description;
        saveCaseRecord({ objSobjecttoUpdateOrInsert: myobjcase })
            .then(result => {
                this.casrecord = {};
                this.dispatchEvent(new ShowToastEvent({
                    title: 'Success!!',
                    message: ' Created Successfully!!',
                    variant: 'success'
                }), );
            })
            .catch(error => {
                this.error = error.message;
            });
    }
}