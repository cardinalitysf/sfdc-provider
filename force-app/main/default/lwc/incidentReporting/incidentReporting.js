/**
 * @Author        : Saranraj
 * @CreatedOn     : Aug 03 ,2020
 * @Purpose       : Incident Reporting
 * @updatedBy     :
 * @updatedOn     :
 **/

import { LightningElement, wire } from "lwc";
import insertAnswerToProviderAnswer from "@salesforce/apex/IncidentFosterParentLawEnforcement.insertAnswerToProviderAnswer";
import getAddressDetailFromApex from "@salesforce/apex/IncidentFosterParentLawEnforcement.getAddressDetail";
import getIncidentRecordTypeValue from "@salesforce/apex/IncidentFosterParentLawEnforcement.getIncidentRecordTypeValue";
import incidentPickListValueFromReferenceValue from "@salesforce/apex/IncidentFosterParentLawEnforcement.incidentPickListValueFromReferenceValue";
import getPickListValues from "@salesforce/apex/CommonUtils.getPickListValues";
import getNarrativeQuestionsFromReference from "@salesforce/apex/IncidentFosterParentLawEnforcement.getNarrativeQuestionsFromReference";
import getReferralData from "@salesforce/apex/IncidentFosterParentLawEnforcement.getReferralData";
import getAPIStreetAddress from "@salesforce/apex/GetStreetAddressFromGoogleAPI.getSuggestions";
import getFullDetails from "@salesforce/apex/GetStreetAddressFromGoogleAPI.getPlaceDetails";
import getNarrativeAnswerForEdit from "@salesforce/apex/IncidentFosterParentLawEnforcement.getNarrativeAnswerForEdit";
import bulkUpdateRecordAns from "@salesforce/apex/monitoringBoardinterview.bulkUpdateRecordAns";
import * as sharedData from "c/sharedData";
import { utils } from "c/utils";
import {
  createRecord,
  updateRecord,
  deleteRecord,
  getRecord
} from "lightning/uiRecordApi";
import USER_ID from "@salesforce/user/Id";
import PROFILE_FIELD from "@salesforce/schema/User.Profile.Name";
import { getPicklistValuesByRecordType } from "lightning/uiObjectInfoApi";
import { getObjectInfo } from "lightning/uiObjectInfoApi";

// case objeect and fileds
import CASE_OBJECT from "@salesforce/schema/Case";
import ID_FIELD from "@salesforce/schema/Case.Id";
import AREA_FIELD from "@salesforce/schema/Case.Area__c";
import INCIDENT_FIELD from "@salesforce/schema/Case.Incident__c";
import INCIDENT_TYPE_FIELD from "@salesforce/schema/Case.IncidentType__c";
import INCIDENT_DATE_FIELD from "@salesforce/schema/Case.IncidentDate__c";
import INCIDENT_TIME_FIELD from "@salesforce/schema/Case.IncidentTime__c";
import RECEIVED_DATE_FIELD from "@salesforce/schema/Case.ComplaintReceivedDate__c";
import RECEIVED_TIME_FIELD from "@salesforce/schema/Case.ComplaintReceivedTime__c";
import LEVEL_OF_SUPERVISOR_FIELD from "@salesforce/schema/Case.LevelofSupervision__c";
import LOCATION_OF_THE_INCIDENT_FIELD from "@salesforce/schema/Case.LocationoftheIncident__c";

//  Address object and fields
import ADDRESS_OBJECT from "@salesforce/schema/Address__c";
import ID_FIELD_FROM_ADDRESS from "@salesforce/schema/Address__c.Id";
import ADDRESS_TYPE from "@salesforce/schema/Address__c.AddressType__c";
import ADDRESS_LINE1_FIELD from "@salesforce/schema/Address__c.AddressLine1__c";
import ADDRESS_LINE2_FIELD from "@salesforce/schema/Address__c.AddressLine2__c";
import CITY_FIELD from "@salesforce/schema/Address__c.City__c";
import COUNTY_FIELD from "@salesforce/schema/Address__c.County__c";
import STATE_FIELD from "@salesforce/schema/Address__c.State__c";
import ZIPCODE_FIELD from "@salesforce/schema/Address__c.ZipCode__c";
import INCIDENT_FIELD_FROM_ADDRESS from "@salesforce/schema/Address__c.Incident__c";

// Address object

// for providerQuestion

import PROV_REC_QUES_INCIDENT_FIELD from "@salesforce/schema/ProviderRecordQuestion__c.Incident__c";
import PROV_REC_QUES_RECORD_TYPE_ID_FIELD from "@salesforce/schema/ProviderRecordQuestion__c.RecordTypeId";
import PROVIDER_RECORD_QUESTION_OBJECT from "@salesforce/schema/ProviderRecordQuestion__c";

import COMAR_FIELD_FROM_PRO_ANS from "@salesforce/schema/ProviderRecordAnswer__c.Comar__c";
import ID_FIELD_FROM_PRO_ANS from "@salesforce/schema/ProviderRecordAnswer__c.Id";
import COMMENTS_FIELD_FROM_PRO_ANS from "@salesforce/schema/ProviderRecordAnswer__c.Comments__c";
import PRO_REC_QUEST_FIELD_FROM_PRO_ANS from "@salesforce/schema/ProviderRecordAnswer__c.ProviderRecordQuestion__c";

export default class IncidentReporting extends LightningElement {
  incidentPickList;
  incidentPickListValue;
  selectedIncidentType;
  selectedIncident;
  selectedIncidentDate;
  selectedIncidentTime;
  selectedDiscoveredDate;
  selectedDiscoveredTime;
  narrativeQuestion;
  selectedLevelOfSupervisor;
  selectedLocationOfTheIncident;
  selectedArea;
  selectedOffSite = false;
  showAddressFields = false;
  filterData = [];
  narrativeQuestionAnswer = [];
  incidentDataTableData = [];
  openIncidentModal = false;
  hiddenAddIncidentBtn = true;
  showDataTablePage = false;
  showUpdateBtn = false;
  addressId;
  availableAddressFields = false;
  providerAnswerId;
  incidentRecordTypeId;

  // for field dependency
  areaControllerValue;
  allAreaDropdownValue;
  areaDropDown = [];

  // for field dependency end

  // off site Address data
  offSiteAddressLine1;
  offSiteAddressLine2;
  offSiteState;
  offSiteCity;
  offSiteCounty;
  offSiteZipCode;

  // off site Address data end

  referralId = "";

  googleDropdown = false;
  dataFromGoogleAPI = [];
  userProfileName
  disableInput = false
  showSaveBtn = true

  get incidentColumn() {
    if(this.userProfileName !== "Caseworker") {
      return [
        {
          label: "Incident Type",
          fieldName: "IncidentType__c",
          cellAttributes: {
            class: "test"
          }
        },
        {
          label: "Level of Supervisor",
          fieldName: "LevelOfSupervision__c",
          cellAttributes: {
            class: "test"
          }
        },
        {
          label: "Location",
          fieldName: "LocationOfTheIncident__c",
          cellAttributes: {
            class: "test"
          }
        },
        {
          label: "Area",
          fieldName: "Area__c",
          cellAttributes: {
            class: "test"
          }
        },
        {
          label: "Incident",
          fieldName: "Incident__c",
          cellAttributes: {
            class: "test"
          }
        },
        {
          label: "Action",
          type: "button",
          initialWidth: 100,
          fieldName: "id",
          typeAttributes: {
            iconName: "utility:edit",
            name: "Edit",
            title: "Edit",
            disabled: false,
            target: "_self",
            initialWidth: 20
          }
        }
      ];
    } else {

      return [
        {
          label: "Incident Type",
          fieldName: "IncidentType__c",
          cellAttributes: {
            class: "test"
          }
        },
        {
          label: "Level of Supervisor",
          fieldName: "LevelOfSupervision__c",
          cellAttributes: {
            class: "test"
          }
        },
        {
          label: "Location",
          fieldName: "LocationOfTheIncident__c",
          cellAttributes: {
            class: "test"
          }
        },
        {
          label: "Area",
          fieldName: "Area__c",
          cellAttributes: {
            class: "test"
          }
        },
        {
          label: "Incident",
          fieldName: "Incident__c",
          cellAttributes: {
            class: "test"
          }
        },
        {
          label: "Action",
          fieldName: "id",
          type: "button",
          initialWidth: 90,
          typeAttributes: {
            iconName: "utility:preview",
            name: "Preview",
            title: "Preview",
            initialWidth: 20,
            disabled: false,
            iconPosition: "left",
            target: "_self"
          }
        }
      ];
    }
    
  }

  get levelOfSupervisorOptions() {
    return [
      { label: "Supervised by Staff", value: "Supervised by Staff" },
      {
        label: "Not Supervised by Staff",
        value: "Not Supervised by Staff"
      }
    ];
  }

  get locationOfIncidentOptions() {
    return [
      { label: "On-Site", value: "On-Site" },
      { label: "Off-Site", value: "Off-Site" }
    ];
  }

  connectedCallback() {
    this.getCaseId();
    this.getReferralDataForTable();
    this.getIncidentPickListValues();
    this.getAddressDetailForEditModal();
  }

  @wire(getRecord, {
    recordId: USER_ID,
    fields: [PROFILE_FIELD]
  })
  wireuser({ error, data }) {
    if (error) {
      this.error = error;
    } else if (data) {
      this.userProfileName = data.fields.Profile.displayValue;
    }
  }

  getCaseId() {
    this.referralId = sharedData.getCaseId();
    return sharedData.getCaseId();
  }

  @wire(getObjectInfo, { objectApiName: CASE_OBJECT })
  objectInfo;

  @wire(getPicklistValuesByRecordType, {
    objectApiName: CASE_OBJECT,
    recordTypeId: "$objectInfo.data.defaultRecordTypeId"
  })
  getAllPicklistValues(result) {
    try {
      if (result.data) {
        this.areaControllerValue =
          result.data.picklistFieldValues.Area__c.controllerValues;
        this.allAreaDropdownValue =
          result.data.picklistFieldValues.Area__c.values;
      }
    } catch (err) {
      this.dispatchEvent(utils.handleError(err));
    }
  }

  getIncidentPickListValues = async () => {
    try {
      let incidentPickLisResponse = await getPickListValues({
        objInfo: {
          sobjectType: "Case"
        },
        picklistFieldApi: "IncidentType__c"
      });
      this.incidentPickList = incidentPickLisResponse;
    } catch (err) {
      this.dispatchEvent(utils.toastMessage("Something went wrong", "info"));
    }
  };

  // get incident record type
  @wire(getIncidentRecordTypeValue, { recordType: "Incident" })
  getIncidentRecordTypeValueFromApex(result) {
    try {
      if (result.data) {
        this.incidentRecordTypeId = result && result.data
        // result.data.map((item) => {
        //   if (item.Name === "Incident") {
        //     this.incidentRecordTypeId = item.Id;
        //     // this.incidentRecordTypeName = item.Name;
        //   }
        // });
      }
    } catch (err) {
      this.dispatchEvent(utils.handleError(err));
    }
  }

  // get incident record type end

  @wire(getNarrativeQuestionsFromReference)
  getNarrativeQuestionsFromReferenceFromApex(result) {
    try {
      this.narrativeQuestion =
        result &&
        result.data.map((item, index) => {
          return { ...item, index: index + 1 };
        });
    } catch (err) {
      this.dispatchEvent(utils.handleError(err));
    }
  }

  getNarrativeQuestionsForEditModal = async () => {
    let getNarrativeAnswerForEditResponse = await getNarrativeAnswerForEdit({
      providerQuestionId: this.providerAnswerId
    });

    let value = [];
    this.narrativeQuestion.map((item) => {
      getNarrativeAnswerForEditResponse.map((data) => {
        if (item.Id === data.Comar__c) {
          value.push({
            ...item,
            answerId: data.Id,
            comar: data.Comar__c,
            comment: data.Comments__c ? data.Comments__c : ""
          });
        }
      });
    });

    if (value.length > 0) {
      this.narrativeQuestion = value;
    }
  };

  handleSelectedAddress = (event) => {
    this.offSiteAddressLine1 = event.target.dataset.description;

    getFullDetails({
      placeId: event.target.dataset.id
    }).then((result) => {
      const main = JSON.parse(result);
      this.offSiteAddressLine1 =
        main.result.address_components[0].long_name +
        " " +
        main.result.address_components[1].long_name;
      this.offSiteCity = main.result.address_components[2].long_name;
      this.offSiteCounty = main.result.address_components[4].long_name;
      this.offSiteState = main.result.address_components[5].long_name;
      this.offSiteZipCode = main.result.address_components[7].long_name;
      this.googleDropdown = false;
    });
  };

  getReferralDataForTable = async () => {
    try {
      let response = await getReferralData({ referralId: this.referralId });
      let temp = [];

      response.forEach((item) => {
        if (this.checkDataLength(item.Area__c)) {
          temp.push({ Area__c: item.Area__c, Id: item.Id });
        }

        if (this.checkDataLength(item.IncidentType__c)) {
          temp.push({ IncidentType__c: item.IncidentType__c });
        }

        if (this.checkDataLength(item.Incident__c)) {
          temp.push({ Incident__c: item.Incident__c });
        }

        if (this.checkDataLength(item.LevelofSupervision__c)) {
          temp.push({ LevelOfSupervision__c: item.LevelofSupervision__c });
        }

        if (this.checkDataLength(item.LocationoftheIncident__c)) {
          temp.push({
            LocationOfTheIncident__c: item.LocationoftheIncident__c
          });
        }

        if (this.checkDataLength(item.ComplaintReceivedTime__c)) {
          temp.push({
            ComplaintReceivedTime__c: item.ComplaintReceivedTime__c
          });
        }

        if (this.checkDataLength(item.IncidentDate__c)) {
          temp.push({ IncidentDate__c: item.IncidentDate__c });
        }

        if (this.checkDataLength(item.ComplaintReceivedDate__c)) {
          temp.push({
            ComplaintReceivedDate__c: item.ComplaintReceivedDate__c
          });
        }

        if (this.checkDataLength(item.IncidentTime__c)) {
          temp.push({ IncidentTime__c: item.IncidentTime__c });
        }
      });

      this.incidentDataTableData = Object.values(
        temp.reduce((cul, value) => {
          cul[value.entity] = Object.assign(cul[value.entity] || {}, value);
          return cul;
        }, {})
      );

      if (this.incidentDataTableData.length > 0) {
        this.showDataTablePage = true;
        const incidentDataTableFlagDispatch = new CustomEvent(
          "incidentdatatableflag",
          {
            detail: {
              incidentDataTableFlag: false
            }
          }
        );
        this.dispatchEvent(incidentDataTableFlagDispatch);
      }
    } catch (err) {
      this.dispatchEvent(utils.handleError(err));
    }
  };

  checkDataLength = (item) => item !== undefined && item.length > 0;

  handleOpenIncidentModal = () => {
    this.openIncidentModal = true;
    this.hiddenAddIncidentBtn = false;
    this.dispatchEvent(
      new CustomEvent("hiddenothermodal", {
        detail: {
          incident: true
        }
      })
    );
  };

  dispatchEventToParent = () => {
    this.dispatchEvent(
      new CustomEvent("showothermodal", {
        detail: {
          incident: false
        }
      })
    );
  }

  handleIncidentCloseModal = () => {
    this.openIncidentModal = false;
    this.hiddenAddIncidentBtn = true;
    this.getReferralDataForTable();
    this.dispatchEventToParent()
  };

  handleRowAction = (event) => {
    const actionName = event.detail.action.name;
    const row = event.detail.row;
    switch (actionName) {
      case "Edit":
        this.showUpdateBtn = true;
        this.showSaveBtn = false
        this.showEditModalHandler(row);
        this.handleOpenIncidentModal()
        break;
      case "Preview":
        this.showUpdateBtn = false
        this.showSaveBtn = false
        this.showEditModalHandler(row,  "Preview")
        this.handleOpenIncidentModal()
        break;
      default:
    }
  };

  showEditModalHandler = (row, preview) => {
    this.showDataTablePage = false;
    this.openIncidentModal = true;
    this.hiddenAddIncidentBtn = false;
    this.disableInput = preview ? true : false

    this.selectedArea = row.Area__c;
    this.selectedIncident = row.Incident__c;
    this.selectedIncidentDate = row.IncidentDate__c;
    this.selectedIncidentTime = row.IncidentTime__c;
    this.selectedLevelOfSupervisor = row.LevelOfSupervision__c;
    this.selectedDiscoveredDate = row.ComplaintReceivedDate__c;
    this.selectedDiscoveredTime = row.ComplaintReceivedTime__c;

    this.SelectedLocationOfTheIncident = row.LocationOfTheIncident__c;
    this.selectedIncidentType = row.IncidentType__c;

    if (row.LocationOfTheIncident__c === "Off-Site") {
      this.showAddressFields = true;
      this.getAddressDetailForEditModal();
    }
    this.getNarrativeQuestionsForEditModal();
  };

  getAddressDetailForEditModal = async () => {
    let response = await getAddressDetailFromApex({
      addressType: "Incident",
      referralId: this.referralId
    });

    this.availableAddressFields = response.length > 0;
    response.map((item) => {
      this.addressId = item.Id;
      this.offSiteAddressLine1 = item.AddressLine1__c && item.AddressLine1__c;
      this.offSiteAddressLine2 = item.AddressLine2__c && item.AddressLine2__c;
      this.offSiteCity = item.City__c && item.City__c;
      this.offSiteCounty = item.County__c && item.County__c;
      this.offSiteState = item.State__c && item.State__c;
      this.offSiteZipCode = item.ZipCode__c && item.ZipCode__c;
    });
  };

  incidentPickListFromReference = async () => {
    try {
      let response = await incidentPickListValueFromReferenceValue({
        selectedIncidentType: this.selectedIncidentType
      });

      this.incidentPickListValue =
        response &&
        response.map((item) => {
          return { ...item, label: item.RefValue__c, value: item.RefValue__c };
        });
    } catch (err) {
      this.dispatchEvent(utils.handleError(err));
    }
  };

  setAreaDropdownValues = () => {
    let temp = [];
    this.allAreaDropdownValue.map((value) => {
      if (
        value.validFor[0] ===
        this.areaControllerValue[this.selectedLocationOfTheIncident]
      ) {
        temp.push({
          label: value.value && value.value,
          value: value.value && value.value
        });
      }
    });
    this.areaDropDown = temp;
  };

  handleIncidentChange = (event) => {
    let targetName = event.target.name;
    let targetValue = event.target.value;

    if (targetName === "Incident Type") {
      this.selectedIncidentType = targetValue;
      this.incidentPickListFromReference();
    } else if (targetName === "incident") {
      this.selectedIncident = targetValue;
    } else if (targetName === "Incident Date") {
      this.selectedIncidentDate = targetValue;
    } else if (targetName === "Incident Time") {
      this.selectedIncidentTime = targetValue;
    } else if (targetName === "Discovered Date") {
      this.selectedDiscoveredDate = targetValue;
    } else if (targetName === "Discovered Time") {
      this.selectedDiscoveredTime = targetValue;
    } else if (targetName === "Level of Supervisor") {
      this.selectedLevelOfSupervisor = targetValue;
    } else if (targetName === "Location of the Incident") {
      this.selectedLocationOfTheIncident = targetValue;
      this.setAreaDropdownValues();
      if (targetValue === "Off-Site") {
        this.selectedOffSite = true;
      } else if (targetValue === "On-Site") {
        this.selectedOffSite = false;
        this.showAddressFields = false;
      } else {
        return;
      }
    } else if (targetName === "Area") {
      this.selectedArea = targetValue;
      if (this.selectedOffSite) {
        this.showAddressFields = true;
      }
    } else {
      return;
    }
  };

  // offSiteAddressChangeHandler start
  offSiteAddressChangeHandler = (event) => {
    let targetValue = event.target.value;
    let targetName = event.target.name;

    if (targetName === "Address Line1") {
      this.offSiteAddressLine1 = targetValue;
      getAPIStreetAddress({
        input: targetValue
      })
        .then((response) => {
          this.dataFromGoogleAPI = JSON.parse(response);
          this.googleDropdown = this.dataFromGoogleAPI.predictions.length > 0;
        })
        .catch((err) => {
          this.dispatchEvent(utils.handleError(err));
        });
    } else if (targetName === "Address Line2") {
      this.offSiteAddressLine2 = targetValue;
    } else if (targetName === "State") {
      this.offSiteState = targetValue;
    } else if (targetName === "County") {
      this.offSiteCounty = targetValue;
    } else if (targetName === "Zip Code") {
      this.offSiteZipCode = targetValue;
    } else if (targetName === "City") {
      this.offSiteCity = targetValue;
    } else {
      return "";
    }
  };
  // offSiteAddressChangeHandler end

  //   narrativeAnswerHandler start
  narrativeAnswerHandler = (event) => {
    let answer = event.target.value;
    let question = event.target.dataset.question;

    // let filterData = [];
    let data = [];
    this.narrativeQuestion.forEach((questionItem) => {
      if (question === questionItem.Question__c) {
        if (
          this.filterData.filter((item) => item.id === questionItem.Id).length >
          0
        ) {
          this.filterData.find(
            (data) => data.id === questionItem.Id
          ).answer = answer;
        } else {
          // this.filterData.push(filterData);
          this.filterData.push({
            question: questionItem.Question__c,
            answer: answer,
            id: questionItem.Id,
            RecordTypeId: questionItem.RecordTypeId
          });
        }
      } else {
        if (
          this.filterData.filter((item) => item.id === questionItem.Id).length >
          0
        ) {
          return
          // this.filterData.find((data) => data.id === questionItem.Id).answer = "";
        } else {
          this.filterData.push({
            question: questionItem.Question__c,
            answer: "",
            id: questionItem.Id,
            RecordTypeId: questionItem.RecordTypeId
          });
        }
      }
    });
  };

  //   narrativeAnswerHandler end

  // save Incident start
  saveIncident = (event) => {
    let btnTitle = event.target.title;

    if (
      this.selectedIncident === "undefined" ||
      this.selectedIncidentType === "undefined" ||
      this.selectedArea === "undefined" ||
      this.selectedLevelOfSupervisor === "undefined" ||
      this.selectedLocationOfTheIncident === "undefined"
    ) {
      this.dispatchEvent(
        utils.toastMessage("Please fill All the required fields", "warning")
      );
    } else {
      let fields = {};
      fields[ID_FIELD.fieldApiName] = this.referralId;
      fields[INCIDENT_FIELD.fieldApiName] = this.selectedIncident;
      fields[INCIDENT_DATE_FIELD.fieldApiName] = this.selectedIncidentDate;
      fields[INCIDENT_TIME_FIELD.fieldApiName] = this.selectedIncidentTime;
      fields[INCIDENT_TYPE_FIELD.fieldApiName] = this.selectedIncidentType;
      fields[RECEIVED_DATE_FIELD.fieldApiName] = this.selectedDiscoveredDate;
      fields[RECEIVED_TIME_FIELD.fieldApiName] = this.selectedDiscoveredTime;

      fields[
        LOCATION_OF_THE_INCIDENT_FIELD.fieldApiName
      ] = this.selectedLocationOfTheIncident;
      fields[
        LEVEL_OF_SUPERVISOR_FIELD.fieldApiName
      ] = this.selectedLevelOfSupervisor;
      fields[AREA_FIELD.fieldApiName] = this.selectedArea;

      let recordData = {
        fields
      };

      updateRecord(recordData)
        .then((response) => {
          if (response) {
            if (this.selectedLocationOfTheIncident === "Off-Site") {
              this.OffSiteAddressSaveHandler(btnTitle);
            } 
            this.narrativeQuestionAnswerSaveHandler(btnTitle);
          }
        })
        .catch((err) => {
          this.dispatchEvent(utils.handleError(err));
        });
    }
  };

  // OffSiteAddressSaveHandler start
  OffSiteAddressSaveHandler = (title) => {
    let fields = {};

    title === "Update"
      ? (fields[ID_FIELD_FROM_ADDRESS.fieldApiName] = this.addressId)
      : null;
    fields[ADDRESS_TYPE.fieldApiName] = "Incident";
    fields[ADDRESS_LINE1_FIELD.fieldApiName] = this.offSiteAddressLine1;
    fields[ADDRESS_LINE2_FIELD.fieldApiName] = this.offSiteAddressLine2;
    fields[STATE_FIELD.fieldApiName] = this.offSiteState;
    fields[CITY_FIELD.fieldApiName] = this.offSiteCity;
    fields[COUNTY_FIELD.fieldApiName] = this.offSiteCounty;
    fields[ZIPCODE_FIELD.fieldApiName] = this.offSiteZipCode;
    fields[INCIDENT_FIELD_FROM_ADDRESS.fieldApiName] = this.referralId;

    let recordData = {
      apiName: ADDRESS_OBJECT.objectApiName,
      fields
    };

    let updateRecordData = {
      fields
    };

    if (title === "Update") {
      if (this.availableAddressFields) {
        updateRecord(updateRecordData)
          .then((response) => {
            this.openIncidentModal = false;
            this.getReferralDataForTable();
            this.dispatchEventToParent()
            this.getAddressDetailForEditModal();
            this.dispatchEvent(
              utils.toastMessage("Updated Successfully", "success")
            );
          })
          .catch((err) => {
            this.dispatchEvent(utils.handleError(err));
          });
      } else {
        createRecord(recordData)
          .then((response) => {
            this.openIncidentModal = false;
            this.getReferralDataForTable();
            this.dispatchEventToParent()
            this.getAddressDetailForEditModal();
            this.dispatchEvent(
              utils.toastMessage("Created Successfully", "success")
            );
          })
          .catch((err) => {
            this.dispatchEvent(utils.handleError(err));
          });
      }
    } else {
      createRecord(recordData)
        .then((response) => {
          this.openIncidentModal = false;
          this.getReferralDataForTable();
          this.getAddressDetailForEditModal();
          this.dispatchEventToParent()
          this.dispatchEvent(
            utils.toastMessage("Created Successfully", "success")
          );
        })
        .catch((err) => {
          this.dispatchEvent(utils.handleError(err));
        });
    }
  };

  // narrativeQuestionAnswerSaveHandler start
  narrativeQuestionAnswerSaveHandler = (btnTitle) => {
    if (btnTitle !== "Update") {
      let fields = {};

      fields[PROV_REC_QUES_INCIDENT_FIELD.fieldApiName] = this.referralId;
      fields[
        PROV_REC_QUES_RECORD_TYPE_ID_FIELD.fieldApiName
      ] = this.incidentRecordTypeId;

      const recordData = {
        apiName: PROVIDER_RECORD_QUESTION_OBJECT.objectApiName,
        fields
      };

      createRecord(recordData)
        .then((response) => {
          this.providerAnswerId = response.id;
          this.handleProviderAnswer(response.id);
        })
        .catch((err) => {
          this.dispatchEvent(utils.handleError(err));
        });
    } else {
      this.updatenarrativeQuestionAnswerHandler();
    }
  };

  handleProviderAnswer = (id) => {
    let data = [];
    this.filterData.map((item) => {
      let fields = {};

      fields[COMAR_FIELD_FROM_PRO_ANS.fieldApiName] = item.id;
      fields[PRO_REC_QUEST_FIELD_FROM_PRO_ANS.fieldApiName] = id;
      fields[COMMENTS_FIELD_FROM_PRO_ANS.fieldApiName] = item.answer;
      data = [...data, fields];
    });

    insertAnswerToProviderAnswer({
      data: JSON.stringify(data)
    })
      .then((response) => {
        this.openIncidentModal = false;
        this.getReferralDataForTable();
        this.getAddressDetailForEditModal();
        this.dispatchEventToParent()
        this.dispatchEvent(
          utils.toastMessage("Incident Record Created Successfully", "success")
        );
      })
      .catch((err) => {
        this.dispatchEvent(utils.handleError(err));
      });
  };

  updatenarrativeQuestionAnswerHandler = () => {
    let temp = [];
    this.filterData.forEach((item) => {
      this.narrativeQuestion.forEach((data) => {
        if (item.id === data.Id) {
          let fieldSet = {};
          fieldSet[ID_FIELD_FROM_PRO_ANS.fieldApiName] = data.answerId;
          fieldSet[COMMENTS_FIELD_FROM_PRO_ANS.fieldApiName] = item.answer
            ? item.answer
            : "";
          fieldSet[
            PRO_REC_QUEST_FIELD_FROM_PRO_ANS.fieldApiName
          ] = this.providerAnswerId;
          temp.push(fieldSet);
        }
      });
    });

    let convertedData = JSON.stringify(temp);

    bulkUpdateRecordAns({
      datas: convertedData
    })
      .then((response) => {
        this.openIncidentModal = false;
        this.getReferralDataForTable();
        this.getAddressDetailForEditModal();
        this.dispatchEventToParent()
        this.dispatchEvent(
          utils.toastMessage("Incident Record Updated Successfully", "success")
        );
      })
      .catch((err) => {
        this.dispatchEvent(utils.handleError(err));
      });
  };

  // save Incident end
}
