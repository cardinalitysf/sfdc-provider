/**
 * @Author        : Murugan M
 * @CreatedOn     : July 31, 2020
 * @Purpose       : This component contains CPA details dashboard
 * @Updated       : Tamilarasan G (for update in try/catch)
 **/
import {
    LightningElement,
    track,
    wire,
    api
} from 'lwc';
import getProgramTypeList from "@salesforce/apex/cpaHomeDashboard.getProgramTypeList";
import getCPAHomeDetails from "@salesforce/apex/cpaHomeDashboard.getCPAHomeDetails";
import UpdateCPAHome from "@salesforce/apex/cpaHomeDashboard.UpdateCPAHome";

import * as shareddata from "c/sharedData";
import {
    createRecord,
    updateRecord,
    deleteRecord
} from "lightning/uiRecordApi";
import {
    utils
} from "c/utils";
import { CJAMS_CONSTANTS } from "c/constants";

import images from "@salesforce/resourceUrl/images";

const actions = [{
    label: 'Edit',
    name: 'Edit',
    iconName: 'utility:edit',
    target: '_self'
},
{
    label: 'View',
    name: 'View',
    iconName: 'utility:preview',
    target: '_self'
},
{
    label: 'Delete',
    name: 'Delete',
    iconName: 'utility:delete',
    target: '_self'
},
{
    label: 'Suspend',
    name: 'Suspend',
    iconName: 'utility:Suspend',
    target: '_self'
}
];


export default class CpaHomeDashboard extends LightningElement {
    openSuspendModel = false;
    programTypeOptions;
    programTypeModel;
    openaddCPAModel = false;
    selectedcpatype;
    currentPageCPAData;
    paginationShow = true;
    showNoDataImage = true;
    actionName;
    rowActionValue;
    openmodelDel = false;
    rowId;
    suspendRecord = {};
    dateValue;
    currentdate;
    letterSentDate;
    startDate;
    endDate;
    comments;
    suspendFieldDisable = false;
    isDisableAddCPAHomeBtn = true;
    dropDownDefaultValue = 'All';

    //Pagination Tracks
    page = 1;
    perpage = 5;
    setPagination = 5;
    totalRecordsCount = 0;
    totalRecords;


    attachmentIcon = images + "/deficiencyIcon.svg";

    get columns() {
        if(this.selectedcpatype =='All'){

    return [{
        label: 'CPA Home Name',
        fieldName: 'CPAName',
        type: 'text',
        initialWidth: 170
    },
    {
        label: 'Program Type',
        fieldName: 'CPAprogramType',
        type: 'text',
        initialWidth: 200
    },
    {
        label: 'Address',
        fieldName: 'CPAAddres',
        type: 'text'
    },
    {
        label: 'Number of HouseHold Member',
        fieldName: 'CPAMembers',
        type: 'text',
        initialWidth: 200
    },
    {
        label: 'Start Date',
        fieldName: 'CPAStartDate',
        type: 'date',
        typeAttributes: {
            month: '2-digit',
            day: '2-digit',
            year: 'numeric'
        },
        initialWidth: 170
    },
    {
        label: 'End Date',
        fieldName: 'CPAEndDate',
        type: 'date',
        typeAttributes: {
            month: '2-digit',
            day: '2-digit',
            year: 'numeric'
        }
    },
    {
        label: 'Clearance/Medical Status',
        type: 'button',
        typeAttributes: {
            label: {
                fieldName: 'CPAClearanceStatus'
            },
            name: "Clearance",
            target: "_self",
        },
        cellAttributes: {
            iconName: 'utility:description',
            iconAlternativeText: 'Description',
            class: {
                fieldName: 'descriptionClass'
            }
        }
    },
    {
        label: 'Home Status',
        fieldName: 'CPAStatus',
        type: 'text',
        cellAttributes: {
            class: {
                fieldName: "statusClass"
            }
        }
    },
    {
        type: 'action',
        typeAttributes: {
            rowActions: actions
        },
        cellAttributes: {
            iconName: 'utility:threedots_vertical',
            iconAlternativeText: 'Actions',
            class: "tripledots"
        },
    }
    ];
}
else{
    return [{
        label: 'CPA Home Name',
        fieldName: 'CPAName',
        type: 'text',
        initialWidth: 170
    },
    {
        label: 'Address',
        fieldName: 'CPAAddres',
        type: 'text'
    },
    {
        label: 'Number of HouseHold Member',
        fieldName: 'CPAMembers',
        type: 'text'
    },
    {
        label: 'Start Date',
        fieldName: 'CPAStartDate',
        type: 'date',
        typeAttributes: {
            month: '2-digit',
            day: '2-digit',
            year: 'numeric'
        },
        initialWidth: 170
    },
    {
        label: 'End Date',
        fieldName: 'CPAEndDate',
        type: 'date',
        typeAttributes: {
            month: '2-digit',
            day: '2-digit',
            year: 'numeric'
        }
    },
    {
        label: 'Clearance/Medical Status',
        type: 'button',
        typeAttributes: {
            label: {
                fieldName: 'CPAClearanceStatus'
            },
            name: "Clearance",
            target: "_self",
        },
        cellAttributes: {
            iconName: 'utility:description',
            iconAlternativeText: 'Description',
            class: {
                fieldName: 'descriptionClass'
            }
        }
    },
    {
        label: 'Home Status',
        fieldName: 'CPAStatus',
        type: 'text',
        cellAttributes: {
            class: {
                fieldName: "statusClass"
            }
        }
    },
    {
        type: 'action',
        typeAttributes: {
            rowActions: actions
        },
        cellAttributes: {
            iconName: 'utility:threedots_vertical',
            iconAlternativeText: 'Actions',
            class: "tripledots"
        },
    }
    ];
}

}


    connectedCallback() {
        this.getProgramType();
        this.futuredatedisable();
    }

    //Function used to disable the future date in received date field
    futuredatedisable() {
        var today = new Date();
        var monthDigit = today.getMonth() + 1;
        if (monthDigit <= 9) {
            monthDigit = '0' + monthDigit;
        }
        var dayDigit = today.getDate();
        if (dayDigit <= 9) {
            dayDigit = '0' + dayDigit;
        }
        var date = today.getFullYear() + "-" + monthDigit + "-" + dayDigit;
        this.currentdate = date;
    }

    //Function used to get program type value based on the Provider
    getProgramType() {
        getProgramTypeList({
            ProviderId: this.CPAHome
        }).then(result => {
            if (result.length > 0) {

                let prgType = result; 
                let programTypeOptions = [];
                prgType.forEach(key => {
                    programTypeOptions.push({
                        label: key.ProgramType__c,
                        value: key.ProgramType__c,
                        imagesrc: key.ProgramType__c == "Independent Living Program" ? "ILP_gray" : key.ProgramType__c == "Independent Living Program Teen Parent" ? "ILPTP_gray" : key.ProgramType__c == "Traditional Foster Care" ? "Traditional_gray" : key.ProgramType__c == "Treatment Foster Care" ? "Treatment_gray" : key.ProgramType__c == "Medically fragile Foster Care" ? "MFFC_gray" : key.ProgramType__c == "Teen Parent Foster Care" ? "TPFC_gray" : key.ProgramType__c == "Adoption" ? "Adoption_gray" : '',
                    })
                });
                this.programTypeOptions = [...programTypeOptions];
                this.programTypeModel = [...programTypeOptions];
            }
            if(result.length >1){
                let addAll;
                addAll = {label:'All',value:'All'};
                this.programTypeOptions.unshift(addAll);
                this.dropDownDefaultValue = 'All';
                this.selectedcpatype = 'All';
                this.fetchCPAHomeDetail();
            }
            else if(result.length == 1){
                this.dropDownDefaultValue = this.programTypeOptions[0].value;
                this.selectedcpatype = this.programTypeOptions[0].value;
                this.fetchCPAHomeDetail();
            }
        }).catch(errors => {
            let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
            return this.dispatchEvent(utils.handleError(errors, config));
        });
    }

    //Function used to redirect from CPAHome Dashboard to CPAHome Page
    redirecttoCPAHomeType() {
        const redirecttoCPAhomepage = new CustomEvent('redirecttocpahomepage', {
            detail: {
                isView: true,
                providertypevalue: this.selectedcpatype,
                actionName: this.actionName,
                rowActionValue: this.rowActionValue
            }
        });
        this.dispatchEvent(redirecttoCPAhomepage);
    }

    get CPAHome() {
        return shareddata.getProviderId();
    }

    //Function used to open the model popup and listed the program type as grid view
    openCPAModal() {
        this.isDisableAddCPAHomeBtn = true;
        this.actionName = "Add";
        if (this.programTypeModel.length > 1) {
            this.openaddCPAModel = true;
        } else {
            this.openaddCPAModel = false;
            this.selectedcpatype = this.programTypeModel[0].value;
            this.redirecttoCPAHomeType();
        }
    }

    //Function used to handle the onchange action in the dropdown
    programSelectOnChange(event) {
        this.selectedcpatype = event.target.value;
        this.fetchCPAHomeDetail();
    }

    //Function used to fetch the data table from the CPAHome object
    fetchCPAHomeDetail() {
        this.currentPageCPAData = [];
        this.page = 1;
        getCPAHomeDetails({
            ProviderId: this.CPAHome,
            ProgramType: this.selectedcpatype
        }).then(result => {
            if (result.length > 0) {
                let currentData = [];
                result.forEach((item) => {
                    let rowData = {};
                    rowData.CPAId = item.Id;
                    rowData.CPAName = item.Name;
                    rowData.CPAAddres = item.Address__r != undefined ? item.Address__r.AddressLine1__c != undefined ? item.Address__r.AddressLine1__c : '' : '';
                    rowData.CPAStartDate = item.StartDate__c != undefined ? item.StartDate__c : '';
                    rowData.CPAEndDate = item.EndDate__c != undefined ? item.EndDate__c : '';
                    rowData.CPAStatus = item.Status__c != undefined ? item.Status__c : '';
                    rowData.CPAClearanceStatus = item.ClearanceMedicalStatus__c != undefined ? item.ClearanceMedicalStatus__c : '';
                    rowData.supensionSentDate = item.Dateofsupensionlettersent__c != undefined ? item.Dateofsupensionlettersent__c : this.currentdate;
                    rowData.supensionStartDate = item.SuspenstionStartDate__c != undefined ? item.SuspenstionStartDate__c : '';
                    rowData.supensionEndDate = item.SuspensionEndDate__c != undefined ? item.SuspensionEndDate__c : '';
                    rowData.supensionComments = item.SuspensionComments__c != undefined ? item.SuspensionComments__c : '';
                    rowData.statusClass = item.Status__c;
                    rowData.CPAprogramType = item.ProgramType__c != undefined ? item.ProgramType__c : '';
                    rowData.CPAMembers = item.Actors__r != undefined ? item.Actors__r[0].Contact__r != undefined ? this.cpaMembeCount(item.Actors__r) : '' : '';
                    rowData.descriptionClass = rowData.CPAClearanceStatus ? 'btnbdrIpad blue' : 'svgRemoveicon';
                    currentData.push(rowData);
                });
                this.totalRecords = currentData;
                this.totalRecordsCount = this.totalRecords.length;
                this.pageData();

                this.showNoDataImage = false;
            } else {
                this.showNoDataImage = true;
            }

        }).catch(errors => {
            let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
            return this.dispatchEvent(utils.handleError(errors, config));
        })
    }

    pageData() {
        let page = this.page;
        let perpage = this.perpage;
        let startIndex = page * perpage - perpage;
        let endIndex = page * perpage;
        this.currentPageCPAData = this.totalRecords.slice(startIndex, endIndex);
    }
    //For Pagination Child Bind
    hanldeProgressValueChange(event) {
        this.page = event.detail;
        this.pageData();
    }


    //Function used to get the count of houseHold member
    cpaMembeCount(event) {
        if (event.length > 1) {
            let contactName;
            let count = event.length - 1;
            event.forEach(key => {
                if (key.Role__c == "Applicant") {
                    contactName = key.Contact__r.LastName;
                }
            });
            return contactName + ' (' + count + ')';
        } else {
            return event[0].Contact__r.LastName;
        }
    }

    //Function used action of the data table like -- Edit/View/Delete/Suspend
    handleRowAction(event) {
        this.rowId = event.detail.row.CPAId;
        this.rowActionValue = event.detail.row;
        if (event.detail.action.name == "Edit") {
            this.selectedcpatype = event.detail.row.CPAprogramType;
            this.actionName = "Edit";
            this.redirecttoCPAHomeType();
        } else if (event.detail.action.name == "View") {
            this.selectedcpatype = event.detail.row.CPAprogramType;
            this.actionName = "View";
            this.redirecttoCPAHomeType();
        } else if (event.detail.action.name == "Delete") {
            this.openmodelDel = true;
        } else if (event.detail.action.name == "Suspend") {
            this.openSuspendModel = true;
            this.letterSentDate = event.detail.row.supensionSentDate;
            this.startDate = event.detail.row.supensionStartDate;
            this.endDate = event.detail.row.supensionEndDate;
            this.comments = event.detail.row.supensionComments;

            if (event.detail.row.CPAStatus == "Suspended" || event.detail.row.CPAStatus == "Active") {
                this.suspendFieldDisable = true;
            } else {
                this.suspendFieldDisable = false;
            }

        } else if (event.detail.action.name == "Clearance") {
            this.redirecttoClearance();
        }
    }

    //Function used from redirect to clearance page
    redirecttoClearance() {
        const redirecttoclearance = new CustomEvent('redirecttoclearance', {
            detail: {
                isView: true,
                providertypevalue: this.selectedcpatype,
                rowActionValue: this.rowActionValue
            }
        });
        this.dispatchEvent(redirecttoclearance);
    }

    //Function used to change the style as per selected program type
    onSelectProgramType(event) {
        this.isDisableAddCPAHomeBtn = false;
        this.selectedcpatype = event.currentTarget.name;
        if (event.currentTarget.name == "Independent Living Program") {
            this.template.querySelector(".ILP_gray").classList.add("activecss");
            this.template.querySelector(".ILPTP_gray").classList.remove("activecss");
            this.template.querySelector(".Traditional_gray").classList.remove("activecss");
            this.template.querySelector(".Treatment_gray").classList.remove("activecss");
            this.template.querySelector(".MFFC_gray").classList.remove("activecss");
            this.template.querySelector(".TPFC_gray").classList.remove("activecss");
            this.template.querySelector(".Adoption_gray").classList.remove("activecss");
        } else if (event.currentTarget.name == "Independent Living Program Teen Parent") {
            this.template.querySelector(".ILP_gray").classList.remove("activecss");
            this.template.querySelector(".ILPTP_gray").classList.add("activecss");
            this.template.querySelector(".Traditional_gray").classList.remove("activecss");
            this.template.querySelector(".Treatment_gray").classList.remove("activecss");
            this.template.querySelector(".MFFC_gray").classList.remove("activecss");
            this.template.querySelector(".TPFC_gray").classList.remove("activecss");
            this.template.querySelector(".Adoption_gray").classList.remove("activecss");
        } else if (event.currentTarget.name == "Traditional Foster Care") {
            this.template.querySelector(".ILP_gray").classList.remove("activecss");
            this.template.querySelector(".ILPTP_gray").classList.remove("activecss");
            this.template.querySelector(".Traditional_gray").classList.add("activecss");
            this.template.querySelector(".Treatment_gray").classList.remove("activecss");
            this.template.querySelector(".MFFC_gray").classList.remove("activecss");
            this.template.querySelector(".TPFC_gray").classList.remove("activecss");
            this.template.querySelector(".Adoption_gray").classList.remove("activecss");
        } else if (event.currentTarget.name == "Treatment Foster Care") {
            this.template.querySelector(".ILP_gray").classList.remove("activecss");
            this.template.querySelector(".ILPTP_gray").classList.remove("activecss");
            this.template.querySelector(".Traditional_gray").classList.remove("activecss");
            this.template.querySelector(".Treatment_gray").classList.add("activecss");
            this.template.querySelector(".MFFC_gray").classList.remove("activecss");
            this.template.querySelector(".TPFC_gray").classList.remove("activecss");
            this.template.querySelector(".Adoption_gray").classList.remove("activecss");
        } else if (event.currentTarget.name == "Medically fragile Foster Care") {
            this.template.querySelector(".ILP_gray").classList.remove("activecss");
            this.template.querySelector(".ILPTP_gray").classList.remove("activecss");
            this.template.querySelector(".Traditional_gray").classList.remove("activecss");
            this.template.querySelector(".Treatment_gray").classList.remove("activecss");
            this.template.querySelector(".MFFC_gray").classList.add("activecss");
            this.template.querySelector(".TPFC_gray").classList.remove("activecss");
            this.template.querySelector(".Adoption_gray").classList.remove("activecss");
        } else if (event.currentTarget.name == "Teen Parent Foster Care") {
            this.template.querySelector(".ILP_gray").classList.remove("activecss");
            this.template.querySelector(".ILPTP_gray").classList.remove("activecss");
            this.template.querySelector(".Traditional_gray").classList.remove("activecss");
            this.template.querySelector(".Treatment_gray").classList.remove("activecss");
            this.template.querySelector(".MFFC_gray").classList.remove("activecss");
            this.template.querySelector(".TPFC_gray").classList.add("activecss");
            this.template.querySelector(".Adoption_gray").classList.remove("activecss");
        } else if (event.currentTarget.name == "Adoption") {
            this.template.querySelector(".ILP_gray").classList.remove("activecss");
            this.template.querySelector(".ILPTP_gray").classList.remove("activecss");
            this.template.querySelector(".Traditional_gray").classList.remove("activecss");
            this.template.querySelector(".Treatment_gray").classList.remove("activecss");
            this.template.querySelector(".MFFC_gray").classList.remove("activecss");
            this.template.querySelector(".TPFC_gray").classList.remove("activecss");
            this.template.querySelector(".Adoption_gray").classList.add("activecss");
        }

    }


    //Function used to delete the records 
    handleRowdelete() {
        deleteRecord(this.rowId)
            .then(() => {
                this.openmodelDel = false;
                this.dispatchEvent(utils.toastMessage('Record deleted successfully', "success"));
                this.fetchCPAHomeDetail();
            })
            .catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DELETE_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
    }

    //Function used get the field onchange value in suspend model popup 
    handleFieldChange(event) {
        if (event.target.name == "DateSuspensionLetterSent") {
            this.letterSentDate = event.target.value;
        } else if (event.target.name == "SuspensionStartDate") {
            this.startDate = event.target.value;
        } else if (event.target.name == "SuspensionEndDate") {
            this.endDate = event.target.value;
        } else if (event.target.name == "SuspensionComments") {
            this.comments = event.target.value;
        }
    }

    //Function used save the suspend field value to CPAHome object 
    saveSuspendValue() {
        if (!this.letterSentDate) {
            return this.dispatchEvent(utils.toastMessage('Ender the Date ', "error"));
        } else if (this.startDate > this.endDate) {
            return this.dispatchEvent(utils.toastMessage('End Date Should be After Start date ', "error"));
        }
        this.fetchSuspendValue();
    }

    //Function used get the suspend field value from CPAHome object 
    fetchSuspendValue() {
        let myObj = {
            'sobjectType': 'CPAHome__c'
        };
        myObj.Dateofsupensionlettersent__c = this.letterSentDate;
        myObj.SuspenstionStartDate__c = this.startDate;
        myObj.SuspensionEndDate__c = this.endDate;
        myObj.SuspensionComments__c = this.comments;
        myObj.Status__c = "Suspended";
        myObj.Id = this.rowId;

        UpdateCPAHome({
            objSobjecttoUpdateOrInsert: myObj
        })
            .then(result => {
                this.dispatchEvent(utils.toastMessage('Record Updated successfully', "success"));
                this.fetchCPAHomeDetail();
                this.openSuspendModel = false;
            })
            .catch(errors => {
                this.openSuspendModel = false;
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            })
    }

    //Function used to close model popup
    closeModel() {
        this.openSuspendModel = false;
        this.openaddCPAModel = false;
        this.openmodelDel = false;
    }

}