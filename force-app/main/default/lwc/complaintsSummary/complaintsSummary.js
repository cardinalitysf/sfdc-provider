/** 
*    Author : Balamurugan B
*   Modified On : July 02, 2020
**/

import {
    LightningElement,
    track,
    api,
    wire
} from 'lwc';

import {
    utils
} from 'c/utils';
import * as sharedData from "c/sharedData";
import getNarrativeDetails from '@salesforce/apex/ComplaintsSummary.getNarrativeDetails';
import updateNarrativeDetails from '@salesforce/apex/ComplaintsSummary.updateNarrativeDetails';
import getHistoryOfNarrativeDetails from '@salesforce/apex/ComplaintsSummary.getHistoryOfNarrativeDetails';
import getSelectedHistoryRec from '@salesforce/apex/ComplaintsSummary.getSelectedHistoryRec';
import images from '@salesforce/resourceUrl/images';
import getPublicProviderStatus from "@salesforce/apex/ComplaintsSummary.getPublicProviderStatus";
import getRecordType from "@salesforce/apex/ComplaintsSummary.getRecordType";
import {
    CJAMS_CONSTANTS
} from 'c/constants';








import {
    refreshApex
} from "@salesforce/apex";

export default class ComplaintsSummary extends LightningElement {
    @track result;
    @track data = [];
    @track narrativeValue;
    @track visibleHistoryData = false;
    @track openNewModal = false;
    @track richTextDisable = false;
    @track btnDisable = false;
    @track openNarrative = true;
    @track disableAdd = false;
    @track btnspeechDisable = false;
    @track btnShowHide = true;


    freezeNarrativeScreen = false;
    finalStatus;
    attachmentIcon = images + '/document-newIcon.svg';
    recordId;
    wireNarrativeHistoryDetails;

    //Fetch Case id
    get caseid() {
        return sharedData.getCaseId();
    }


    get recordType() {
        return 'Complaints';
    }


    get isSuperUserFlow() {
        let userProfile = sharedData.getUserProfileName();
        if (userProfile === 'Supervisor') {
            this.btnDisable = true;
            return true;
        }
        else {
            return false;
        }
    }

    connectedCallback() {
        this.isSuperUserFlow;
        if (this.recordType != undefined) {
            getRecordType({
                name: this.recordType
            })
                .then((result) => {
                    this.RecordTypeId = result;
                }).catch((errors) => {
                    return this.dispatchEvent(utils.handleError(errors));
                });
        }
    }

    openNarrativeModel() {

        if (this.freezeNarrativeScreen)
            return this.dispatchEvent(
                utils.toastMessage(
                    `Cannot add narrative for ${this.finalStatus} complaints`,
                    "warning"
                )
            );
        this.openNarrative = true;
        this.btnDisable = false;
        this.richTextDisable = false;
        this.btnspeechDisable = false;
        this.narrativeValue = '';
    }



    @wire(getNarrativeDetails, {
        applicationId: '$caseid'
    })
    wiredNarratvieDetails({
        error,
        data
    }) {
        if (data) {
            try {
                if (data.length > 0) {
                    this.narrativeValue = data[0].Summary__c;
                }
            } catch (error) {
                if (error) {
                    let error = JSON.parse(error.body.message);
                    const { title, message, errorType } = error;
                    this.dispatchEvent(
                        utils.toastMessageWithTitle(title, message, errorType)
                    )
                }
                else {
                    this.dispatchEvent(
                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                    );
                }
            }

        }
    }

    @wire(getHistoryOfNarrativeDetails, {
        applicationId: '$caseid'
    })
    wiredNarrativeHistoryDetails(result) {
        this.wireNarrativeHistoryDetails = result;

        if (result.data) {
            try {
                if (result.data.length > 0) {
                    this.visibleHistoryData = true;

                    this.data = result.data.map((row, index) => {
                        return {
                            Id: row.Id,
                            author: row.LastModifiedBy.Name,
                            lastModifiedDate: row.LastModifiedDate,
                            dataIndex: index
                        }
                    });
                } else {
                    this.visibleHistoryData = false;
                }
            } catch (errors) {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            }
        }
    }

    @wire(getPublicProviderStatus, {
        applicationId: '$caseid'
    })
    wiredApplicationDetails({
        error,
        data
    }) {
        if (data) {
            try {
                if (
                    data.length > 0 &&
                    CJAMS_CONSTANTS.NARRATIVE_STATUS.includes(data[0].Status)
                ) {
                    this.btnDisable = true;
                    this.richTextDisable = true;
                    this.disableAdd = true;
                    this.btnspeechDisable = true;
                    this.btnShowHide = false;
                    this.freezeNarrativeScreen = true;
                    this.finalStatus = data[0].Status;
                    this.narrativeValue = data[0].Summary__c;

                } else {
                    this.btnDisable = false;
                    this.richTextDisable = false;
                    this.disableAdd = false;
                    this.btnspeechDisable = false;
                    this.btnShowHide = true;
                    this.freezeNarrativeScreen = false;
                    this.finalStatus = null;
                    this.narrativeValue = data[0].Summary__c;
                }
            } catch (errors) {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            }
        }
    }


    handleSpeechToText(event) {

        this.narrativeValue = event.detail.speechValue;
    }
    //Narrative On Change Functionality
    narrativeOnChange(event) {
        this.narrativeValue = event.target.value;
    }

    cancelHandler() {
        this.narrativeValue = '';
    }

    //Save Functionality
    saveNarrativeDetails() {
        this.template.querySelector('c-common-speech-to-text').stopSpeech();
        let myObj = {
            'sobjectType': 'Case'
        };

        myObj.Summary__c = this.narrativeValue;
        myObj.RecordTypeId = this.RecordTypeId;
        if (this.caseid)
            myObj.Id = this.caseid;
        this.btnDisable = true;
        updateNarrativeDetails({
            objSobjecttoUpdateOrInsert: myObj
        })
            .then(result => {

                this.narrativeValue = '';
                this.btnDisable = false;
                this.dispatchEvent(utils.toastMessage("Summary Saved Successfully", "success"));

                return refreshApex(this.wireNarrativeHistoryDetails);
            }).catch((errors) => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
    }

    //Get respective history record
    narrativeHistoryOnClick(event) {
        this.recordId = event.currentTarget.dataset.id;

        let dataIndex = event.currentTarget.dataset.value;
        let datas = this.template.querySelectorAll('.divHighLight');

        for (let i = 0; i < datas.length; i++) {
            if (i == dataIndex)
                datas[i].className = "slds-nav-vertical__item divHighLight slds-is-active";
            else
                datas[i].className = "slds-nav-vertical__item divHighLight";
        }

        getSelectedHistoryRec({
            recId: this.recordId
        })
            .then(result => {
                if (result.length > 0 && result[0].RichNewValue__c) {
                    this.narrativeValue = result[0].RichNewValue__c;
                    this.btnDisable = true;
                    this.richTextDisable = true;
                    this.btnspeechDisable = true;
                } else {
                    this.narrativeValue = null;
                }
            }).catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });

    }
}