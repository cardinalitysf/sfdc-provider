import { LightningElement, api } from 'lwc';
import { reduceErrors } from 'c/utils';
import { CJAMS_CONSTANTS } from 'c/constants';

export default class ErrorPanel extends LightningElement {
    /** Generic / user-friendly message */
    @api friendlyMessage = CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE;

    viewDetails = false;

    /** Single or array of LDS errors */
    @api errors;

    get errorMessages() {
        return reduceErrors(this.errors);
    }

    handleCheckboxChange(event) {
        this.viewDetails = event.target.checked;
    }
}