import { LightningElement, track } from 'lwc';
import images from "@salesforce/resourceUrl/images";

import myResource from '@salesforce/resourceUrl/styleSheet';
import { loadStyle } from 'lightning/platformResourceLoader';

export default class ReferralTypes extends LightningElement {

    @track openModelwindow = true;

    privateIcon = images + "/office-building.svg";
    existingIcon = images + "/clipboard.svg";
    plusIcon = images + "/plus-new-referal.svg";
    publicIcon = images + "/houseIcon.svg";

    providerTypeName = '';
    btnDisabled = true;
    providerTypeViewOrExisting = '';

    renderedCallback() {
        Promise.all([
            loadStyle(this, myResource + '/styleSheet.css'),
        ])
    }

    openModel() {
        this.openModelwindow = true;
    }
    closeModel() {
        this.openModelwindow = false;
    }

    handleNext() {
        //event.preventDefault();
        const togglecmp = new CustomEvent('componentstoggletypeandprovider', {
            detail: {
                first: false,
                second: this.providerTypeName === 'private' ? true : false,
                third: this.providerTypeName === 'exist' ? true : false
            }
        });
        this.dispatchEvent(togglecmp);
    }

    privateProvider() {
        this.providerTypeName = 'private';
        let cardDatas = this.template.querySelectorAll('.box-col1');
        if (cardDatas.length > 0) {
            cardDatas.forEach(card => {
                card.className = card.className.includes(' bluebdr') == true ? card.className.replace(' bluebdr', '') : card.className;
            });
            cardDatas[0].className = cardDatas[0].className + ' bluebdr';
        }
    }

    existingProvider() {
        this.providerTypeName = 'exist';
        let cardDatas = this.template.querySelectorAll('.box-col1');
        if (cardDatas.length > 0) {
            cardDatas.forEach(card => {
                card.className = card.className.includes(' bluebdr') == true ? card.className.replace(' bluebdr', '') : card.className;
            });
            cardDatas[1].className = cardDatas[0].className + ' bluebdr';
        }
    }

    publicProvider() {
        this.providerTypeName = 'public';
        let cardDatas = this.template.querySelectorAll('.box-col1');
        if (cardDatas.length > 0) {
            cardDatas.forEach(card => {
                card.className = card.className.includes(' bluebdr') == true ? card.className.replace(' bluebdr', '') : card.className;
            });
            cardDatas[2].className = cardDatas[0].className + ' bluebdr';
        }
    }

}