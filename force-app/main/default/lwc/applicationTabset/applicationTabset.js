/**
 * @Author        : Janaswini S
 * @CreatedOn     : April 1, 2020
 * @Purpose       : This component contains Application Tabs
 **/

import {
  LightningElement,
  track,
  api
} from "lwc";
import * as sharedData from "c/sharedData";
import fetchDataForTabset from "@salesforce/apex/ApplicationTabset.fetchDataForTabset";
import { CJAMS_CONSTANTS } from "c/constants";
import { utils } from "c/utils";

export default class ApplicationTabset extends LightningElement {
  @track applicationName;
  @api activeTab = "general";

  get getapplicationid() {
    return sharedData.getApplicationId();
  }

  connectedCallback() {
    this.fetchDatasFromApplication();
  }

  fetchDatasFromApplication() {
    if (this.getapplicationid != undefined) {
      fetchDataForTabset({
        fetchdataname: this.getapplicationid
      }).then(result => {
        this.applicationName = result[0].Name ? result[0].Name : " - ";
      })
        .catch(errors => {
          if (errors) {
            let error = JSON.parse(errors.body.message);
            const { title, message, errorType } = error;
            this.dispatchEvent(
              utils.toastMessageWithTitle(title, message, errorType)
            );
          } else {
            this.dispatchEvent(
              utils.toastMessage(
                CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
                CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
              )
            );
          }
        });
    }
  }

  handleredirectMoniteringTabs() {
    const oncaseid = new CustomEvent('redirecteditmonitering', {
      detail: {
        first: false
      }
    });
    this.dispatchEvent(oncaseid);
  }
  @track licenseDateFlag;
  handleLicenceDate(event) {
    this.licenseDateFlag = event.detail.first;
  }
  PageRedirecttoDashBoard() {
    window.location.reload();
  }
}