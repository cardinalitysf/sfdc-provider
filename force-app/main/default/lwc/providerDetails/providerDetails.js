import {
  LightningElement,
  track,
  wire
} from 'lwc';
import {
  ShowToastEvent
} from 'lightning/platformShowToastEvent';
// import fetchPickListValue from "@salesforce/apex/DMLOperationsHandler.fetchPickListValue";
import updateprovidewrandReferral from "@salesforce/apex/DMLOperationsHandler.updateOrInsertSOQL";
import getStreetAddress from "@salesforce/apex/GetStreetAddressFromGoogleAPI.getSuggestions";
import getFullDetails from "@salesforce/apex/GetStreetAddressFromGoogleAPI.getPlaceDetails";

import getReferenceCountry from '@salesforce/apex/ProviderDetailController.getReferenceCountry';
import getReferenceCity from '@salesforce/apex/ProviderDetailController.getReferenceCity';
import getReferenceState from '@salesforce/apex/ProviderDetailController.getReferenceState';

export default class ProviderDetails extends LightningElement {
  @track valueProfit = "";
  //   @track valueCheck = " ";
  active = false;
  selectedVals = [];
  @track result;
  @track strStreetAddress;
  @track strStreetAddresPrediction = [];


  @track stageNameValues;
  @track options_SelectCountry;
  @track options_SelectCity;
  @track options_SelectState;

  @track sf_SelectState;
  @track sf_SelectCity;
  @track sf_SelectCountry;



  async connectedCallback() {

    this.getCountryDropdownOnLoad(); //get Countries
    this.getCityDropdownOnLoad(); //get Cities
    this.getStateDropdownOnLoad(); //get States
  }

  getCountryDropdownOnLoad() {
    getReferenceCountry({})
      .then(result => {

        if (result == null) {
          //do nothing
        } else {
          var RetVal = JSON.parse(result);
          var RetValOptions = [];
          if (RetVal.length > 0) {
            for (var i = 0; i < RetVal.length; i++) {
              var cell = {
                value: RetVal[i].Id,
                label: RetVal[i].RefValue__c
              };
              RetValOptions.push(cell);
            }
          }
          this.options_SelectCountry = RetValOptions;
        }
      })
      .catch(error => {
        this.error = error;
      })

  }
  getCityDropdownOnLoad() {
    getReferenceCity({})
      .then(result => {
        if (result == null) {
          //do nothing
        } else {
          var RetVal = JSON.parse(result);
          var RetValOptions = [];
          if (RetVal.length > 0) {
            for (var i = 0; i < RetVal.length; i++) {
              var cell = {
                value: RetVal[i].Id,
                label: RetVal[i].RefValue__c
              };
              RetValOptions.push(cell);
            }
          }
          this.options_SelectCity = RetValOptions;

        }
      })
      .catch(error => {
        this.error = error;
      })
  }
  getStateDropdownOnLoad() {
    getReferenceState({})
      .then(result => {
        if (result == null) {
          //do nothing
        } else {
          var RetVal = JSON.parse(result);
          var RetValOptions = [];
          if (RetVal.length > 0) {
            for (var i = 0; i < RetVal.length; i++) {
              var cell = {
                value: RetVal[i].Id,
                label: RetVal[i].RefValue__c
              };
              RetValOptions.push(cell);
            }
          }
          this.options_SelectState = RetValOptions;
        }
      })
      .catch(error => {
        this.error = error;
      })
  }


  get streetAddressLength() {
    return this.strStreetAddresPrediction != '';
  }
  get strStreetAddresPredictionUI() {
    return this.strStreetAddresPrediction;
  }
  get tabClass() {
    return this.active ? 'slds-dropdown slds-dropdown_length-5 slds-dropdown_fluid slds-show' : 'slds-dropdown slds-dropdown_length-5 slds-dropdown_fluid slds-hide';
  }
  @wire(getStreetAddress, {
    input: '$strStreetAddress'
  })
  wiregetStreetAddress({
    error,
    data
  }) {
    if (data) {
      //this.strStreetAddresPrediction='';

      this.strStreetAddresPrediction = JSON.parse(data);

      this.active = this.strStreetAddresPrediction.predictions.length > 0 ? true : false;

    } else if (error) {

    }
  }
  selectStreetAddress(event) {
    this.strStreetAddress = event.target.dataset.description;

    getFullDetails({
        placeId: event.target.dataset.id
      })
      .then(result => {
        var main = JSON.parse(result);

      })
      .error(error => {

      })

  }
  getStreetAddress(event) {

    this.strStreetAddress = event.target.value;

  }

  get optionsRadio() {
    return [{
        label: "Yes",
        value: "Yes"
      },
      {
        label: "No",
        value: "No"
      },
      {
        label: "N/A",
        value: "N/A"
      }
    ];
  }
  get optionsRadioOne() {
    return [{
        label: "Yes",
        value: "Yes"
      },
      {
        label: "No",
        value: "No"
      },
      {
        label: "N/A",
        value: "N/A"
      }
    ];
  }
  get optionsRadioTwo() {
    return [{
        label: "Yes",
        value: "Yes"
      },
      {
        label: "No",
        value: "No"
      },
      {
        label: "N/A",
        value: "N/A"
      }
    ];
  }

  get options() {
    return [{
        label: "Java",
        value: "Java"
      },
      {
        label: "Javascript",
        value: "Javascript"
      },
      {
        label: "Cobol",
        value: "Cobol"
      },
      {
        label: "Angular",
        value: "Angular"
      },
      {
        label: "React",
        value: "React"
      },
      {
        label: "Vue",
        value: "Vue"
      }
    ];
  }

  get optionsProfit() {
    return [{
        label: "Profit",
        value: "profit"
      },
      {
        label: "Non Profit",
        value: "nonprofit"
      }
    ];
  }

  handleChangeRadioButton(event) {
    const selectedOption = event.detail.value;

  }

  handleClick(evt) {

    const allValid = [
      ...this.template.querySelectorAll("lightning-input"),
      ...this.template.querySelectorAll("lightning-combobox")
    ].reduce((validSoFar, inputCmp) => {
      inputCmp.reportValidity();
      return validSoFar && inputCmp.checkValidity();
    }, true);
    if (allValid) {
      let button = this.template.querySelector("lightning-button");
      button.disabled = false;

      // alert('All form entries look valid. Ready to submit!');
    } else {
      let button = this.template.querySelector("lightning-button");
      button.disabled = true;
      // alert('Please update the invalid form entries and try again.');
    }
  }

  saveForm(event) {

    var inp = [
      ...this.template.querySelectorAll("lightning-input"),
      ...this.template.querySelectorAll("lightning-combobox"),
      ...this.template.querySelectorAll("lightning-radio-group")
    ];

    let objReferral = {
      'sObjectType': "Case"
    };
    let data = {};
    let objProvider = {
      'sobjectType': 'Account'
    };
    objProvider.Name = 'Please work for janaswini';
    inp.forEach(element => {
      switch (element.name) {
        case "ParentCorporation":
          objProvider.ParentCorporation__c = element.value;
          break;
        case "Providertype":
          objProvider.ProviderType__c = element.value;
          break;
        case "Program":
          objReferral.Program__c = element.value;
          break;
        case "Corporation":
          objProvider.Corporation__c = element.value;
          break;
        case "ProgramType":
          objReferral.ProgramType__c = element.value;
          break;
        case "TaxId":
          objProvider.TaxId__c = element.value;
          break;
        case "proposal":
          objProvider.RFP__c = element.value;
          break;
        case "FEIN":
          objProvider.FEINTaxID__c = element.value;
          break;
        case "FirstName":
          objProvider.FirstName__c = element.value;
          break;
        case "SON":
          objProvider.SON__c = element.value;
          break;
        case "LastName":
          objProvider.LastName__c = element.value;
          break;
        case "AddressLine1":
          objProvider.BillingAddress = element.value;
          break;
        case "Email":
          objProvider.Email__c = element.value;
          break;
        case "AddressLine2":
          objProvider.ShippingAddress = element.value;
          break;
        case "PhoneNumber":
          objProvider.Phone = element.value;
          break;
        case "State":
          data.State = element.value;
          break;
        case "CellNumber":
          objProvider.CellNumber__c = element.value;
          break;
        case "City":
          data.City = element.value;
          break;
        case "FaxNumber":
          objProvider.Fax = element.value;
          break;
        case "Country":
          data.Country = element.value;
          break;
        case "Accredition":
          objProvider.Accrediation__c = element.value;
          break;
        case "ZipCode":
          data.ZipCode = element.value;
          break;
        case "valueProfit":
          objProvider.Profit__c = element.value;
          break;
      }
    }) // End for each 



    updateprovidewrandReferral({
        objSobjecttoUpdateOrInsert: objProvider
      })
      .then(result => {

      })
      .catch(error => {

      });


  }

  handleChangePickList(event) {
    this.selectedVals.push(event.detail.value);
    this.value = this.selectedVals.join(", ");
  }

  handleSelectState(event) {

  }
  handleSelectCity(event) {

  }
  handleSelectCountry(event) {

  }
}