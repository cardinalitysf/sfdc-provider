/**
 * @Author        : Sundar Karuppalagu
 * @CreatedOn     : APRIL 25 ,2020
 * @Purpose       : Contract Review Process
 * @UpdatedBy     : Sundar K
 * @UpdatedOn     : MAY 04, 2020
 **/

import { LightningElement, wire, track } from 'lwc';

import getContractReviewDetails from '@salesforce/apex/ProvidersContractReview.getContractReviewDetails';
import getAssignedSuperUserTableDetails from '@salesforce/apex/ProvidersContractReview.getAssignedSuperUserTableDetails';
import getAllSuperUsersList from '@salesforce/apex/ProvidersContractReview.getAllSuperUsersList';
import saveSign from '@salesforce/apex/ProvidersContractReview.saveSign';
import getBase64Image from '@salesforce/apex/applicationLicenseInfo.getSignatureAsBas64Image';

import { getPicklistValuesByRecordType, getObjectInfo } from 'lightning/uiObjectInfoApi';
import { updateRecord } from 'lightning/uiRecordApi';

import CONTRACT_OBJECT from '@salesforce/schema/Contract__c';
import ID_FIELD from '@salesforce/schema/Contract__c.Id';
import REVIEWSTATUS_FIELD from '@salesforce/schema/Contract__c.ReviewStatus__c';
import COMMENTS_FIELD from '@salesforce/schema/Contract__c.ReviewComments__c';
import SUPERVISORCOMMENTS_FIELD from '@salesforce/schema/Contract__c.SupervisorComments__c';
import CONTRACTSTATUS_FIELD from '@salesforce/schema/Contract__c.ContractStatus__c';
import SUPERVISOR_FIELD from '@salesforce/schema/Contract__c.Supervisor__c';
import CASEWORKER_FIELD from '@salesforce/schema/Contract__c.Caseworker__c';
import SUBMITTEDDATE_FIELD from '@salesforce/schema/Contract__c.SubmittedDate__c';
import SIGNATURE_FIELD from '@salesforce/schema/Contract__c.IsSignature__c';
import userId from '@salesforce/user/Id';

import { CJAMS_CONSTANTS } from 'c/constants';
import { utils } from 'c/utils';
import * as sharedData from "c/sharedData";
import images from '@salesforce/resourceUrl/images';

//Updated By Sundar K for Supervisor Flow Check
import updateapplicationdetails from '@salesforce/apex/ProvidersContractReview.updateapplicationdetails';

//Assinged Super User Data Table Header Columns Declaration
const assignedSuperUserTableHeader = [{
        label: 'Assigned Date',
        fieldName: 'SubmittedDate__c',
        type: 'text'
    }, {
        label: 'Author',
        fieldName: 'caseWorkerName',
        type: 'text'
    }, {
        label: 'Designation',
        fieldName: 'caseWorkerDesignation',
        type: 'text'
    }, {
        label: 'Status',
        fieldName: 'assignedStatus',
        type: 'text'
    }, {
        label: 'Assigned To',
        fieldName: 'superVisorName',
        type: 'text'
    }, {
        label: 'Designation',
        fieldName: 'superVisorDesignation',
        type: 'text'
    }, {
        label: 'Action',
        fieldName: 'Id',
        type: "button",
        typeAttributes: {
            iconName: 'utility:preview',
            name: 'View',
            title: 'Click to Preview',
            variant: 'border-filled',
            class: 'view-red',
            disabled: false,
            iconPosition: 'left',
            target: '_self'
        }
    }
]

//Tier Approval Modal for Supervisor Header Columns Declaration
const superUserTableColumns = [{
        label: 'Name',
        fieldName: 'Name',
        type: 'text'
    }, {
        label: 'CaseLoad',
        fieldName: 'CaseLoad__c',
        type: 'number'
    }, {
        label: 'Availability',
        fieldName: 'Availability__c',
        type: 'text'
    }, {
        label: 'Zip Code',
        fieldName: '',
        type: 'text'
    }, {
        label: 'Type Of Worker',
        fieldName: 'ProfileName',
        type: 'text'
    }, {
        label: 'Unit',
        fieldName: 'Unit__c',
        type: 'text'
    }
];

//Signature model declaration
let isDownFlag,
    isDotFlag = false,
    prevX = 0,
    currX = 0,
    prevY = 0,
    currY = 0;

let x = "#0000A0"; //blue color
let y = 1.5; //weight of line width and dot.       

let canvasElement, ctx; //storing canvas context
let attachment; //holds attachment information after saving the sigture on canvas
let dataURL, convertedDataURI; //holds image data

//Class Name declaration in LWC
export default class ProvidersContractReview extends LightningElement {
    //Fields declaration
    @track reviewDetails = {};
    @track statusOptions = [];

    //Disable fields using flags
    @track isStatusDisable = false;
    @track isCommentsDisable = false;
    @track isSignatureDisable = false;
    @track isTierApprovalBtnDisable = false;
    @track isDraftBtnDisable = false;
    @track isSubmitBtnDisable = false;

    //Refresh Icon Image
    refreshIcon = images + '/refresh-icon.svg';

    //Super User Modal Table declaration
    @track superUserTableColumns = superUserTableColumns;
    @track superUsersTableData = [];
    @track openSuperUserModel = false;
    @track assignBtnDisable = false;

    //Fetch Assigned Super User Table Data Declaration
    @track isSuperUserAssigned = false;
    @track assignedSuperUserTableHeader = assignedSuperUserTableHeader;
    @track assignedSuperUserTableData = [];
    @track openPreviewModel = false;
    @track previewDetails = {};
    isSuperUserSelected = false;
    Supervisor__c;
    Caseworker__c;
    SubmittedDate__c;

    //Signature fields declaration
    @track openSignatureModel = false;
    @track getContext;
    SignatureValidation;

    //Super User Table Selected Rows Initialization
    @track selectedRows = [];

    //Image Base64 format initialization
    imageBase64Format;
    isSignatureRemoved = false;

    //Supervisor Flow Declarations
    @track workItemId;
    superUserSelectedReviewStatus;

    //Get Contract ID
    get contractId() {
        return sharedData.getContractId();
    }

    get isSuperUserFlow() {
        let userProfile = sharedData.getUserProfileName();

        if (userProfile === 'Supervisor')
            return true;
        else
            return false;
    }

    connectedCallback() {
        getContractReviewDetails({
            contractId: this.contractId
        })
        .then(result => {
            if (result.length > 0 && this.isSuperUserFlow === false) {
                this.superUserSelectedReviewStatus = ['Approve', 'Return to Caseworker'].includes(result[0].ReviewStatus__c) ? result[0].ReviewStatus__c : "";
                this.reviewDetails.ReviewStatus__c = result[0].ReviewStatus__c == CJAMS_CONSTANTS.REVIEW_STATUS_REWORK ? CJAMS_CONSTANTS.REVIEW_STATUS_NONE : result[0].ReviewStatus__c;                    
                this.reviewDetails.ReviewComments__c = result[0].ReviewComments__c;

                if (result[0].ContractStatus__c == CJAMS_CONSTANTS.CONTRACT_STATUS_SUBMITTED || result[0].ContractStatus__c == CJAMS_CONSTANTS.CONTRACT_STATUS_ACTIVE || result[0].ContractStatus__c == CJAMS_CONSTANTS.CONTRACT_STATUS_INACTIVE || result[0].ContractStatus__c == CJAMS_CONSTANTS.CONTRACT_STATUS_EXPIRED) {
                    this.isStatusDisable = true;
                    this.isCommentsDisable = true;
                    this.isSignatureDisable = true;
                    this.isTierApprovalBtnDisable = true;
                    this.isDraftBtnDisable = true;
                    this.isSubmitBtnDisable = true;
                    this.isSuperUserAssigned = true;
                } else if (!result[0].ReviewStatus__c) {
                    this.isTierApprovalBtnDisable = true;
                    this.isSubmitBtnDisable = true;
                } else if (result[0].ReviewStatus__c == CJAMS_CONSTANTS.REVIEW_STATUS_NONE || this.reviewDetails.ReviewStatus__c == CJAMS_CONSTANTS.REVIEW_STATUS_NONE) {
                    this.isTierApprovalBtnDisable = true;
                    this.isSubmitBtnDisable = true;
                    this.isSuperUserAssigned = false;
                } else if (result[0].ReviewStatus__c == CJAMS_CONSTANTS.REVIEW_STATUS_REJECT) {
                    //Sundar Do we need this if condition?
                    this.isTierApprovalBtnDisable = true;
                    this.isSuperUserAssigned = false;
                } else {
                    this.isStatusDisable = false;
                    this.isCommentsDisable = false;
                    this.isSignatureDisable = false;
                    this.isTierApprovalBtnDisable = false;
                    this.isDraftBtnDisable = false;
                    this.isSubmitBtnDisable = false;
                }

                this.getAssignedSuperUserTableValues();
            }

            if (this.isSuperUserFlow === true && result.length > 0) {
                this.reviewDetails.ReviewStatus__c = result[0].ReviewStatus__c == CJAMS_CONSTANTS.REVIEW_STATUS_SUBMITFORREVIEW ? "" : result[0].ReviewStatus__c;
                this.reviewDetails.SupervisorComments__c = result[0].SupervisorComments__c;
                this.superUserSelectedReviewStatus = result[0].ReviewStatus__c;

                if (result[0].ContractStatus__c == CJAMS_CONSTANTS.CONTRACT_STATUS_ACTIVE) {
                    this.isStatusDisable = true;
                    this.isCommentsDisable = true;
                    this.isSignatureDisable = true;
                    this.isSubmitBtnDisable = true;
                    this.isSuperUserAssigned = true;
                } else if (result[0].ContractStatus__c == CJAMS_CONSTANTS.CONTRACT_STATUS_SUBMITTED) {
                    this.isSuperUserAssigned = true;
                } else {
                    this.isSuperUserAssigned = false;
                }

                this.getAssignedSuperUserTableValues();
            }
        })
    }

    getAssignedSuperUserTableValues() {
        getAssignedSuperUserTableDetails({
            contractId: this.contractId
        })
        .then(result => {
            if (result.length == 0)
                return this.isSuperUserAssigned = false;

            result = JSON.parse(result);

            this.SignatureValidation = result[0].signatureUrl;
            let resultArray = [];

            if (this.isSuperUserFlow === false && result[0].contractRecord.Supervisor__c !== undefined) {
                this.isSuperUserAssigned = true;

                resultArray.push({
                    SubmittedDate__c: utils.formatDate(result[0].contractRecord.SubmittedDate__c),
                    caseWorkerName: result[0].contractRecord.Caseworker__r.Name,
                    caseWorkerDesignation: result[0].contractRecord.Caseworker__r.Unit__c,
                    assignedStatus: result[0].contractRecord.ReviewStatus__c,
                    superVisorName: result[0].contractRecord.Supervisor__r.Name,
                    comments: result[0].contractRecord.ReviewComments__c,
                    superVisorDesignation: 'Supervisor',
                    SignatureValue: result[0].signatureUrl,
                    supervisorID: result[0].contractRecord.Supervisor__c
                });
                this.assignedSuperUserTableData = [...resultArray];
                this.selectedRows.splice(0, this.selectedRows.length);
                this.selectedRows.push(this.assignedSuperUserTableData[0].supervisorID);
            }

            for (var i=0; i<result.length; i++) {
                if (result[i].contractRecord.Steps !== undefined) {
                    if (result[i].contractRecord.Steps.totalSize === 1) {
                        let signUrlIndex = result[i].signatureUrl.findIndex(sign => sign.OwnerId === result[i].contractRecord.Steps.records[0].ActorId);
                        let signUrl = signUrlIndex >= 0 ? result[i].signatureUrl[signUrlIndex].Id : '';
                        resultArray.push({
                            caseWorkerName: result[i].contractRecord.Steps.records[0].Actor.Name,
                            assignedStatus: result[i].contractRecord.Steps.records[0].StepStatus == 'Started' ? 'Submitted' : (result[i].contract.StepStatus == 'Removed' ? 'Returned' : result[0].contract.StepStatus),
                            superVisorName: result[i].contractRecord.Workitems.records[0].Actor.Name,
                            comments: result[i].contractRecord.Steps.records[0].Comments,
                            caseWorkerDesignation: result[i].contractRecord.Steps.records[0].Actor.Profile.Name == 'Supervisor' ? 'Supervisor' : 'OLM',
                            superVisorDesignation: result[i].contractRecord.Workitems.records[0].Actor.Profile.Name,
                            SubmittedDate__c: utils.formatDate(result[i].contractRecord.Steps.records[0].CreatedDate),
                            SignatureValue: signUrl
                        });
                    } else if (result[i].contractRecord.Steps.totalSize > 1) {
                        let datas = result[i].contractRecord.Steps.records;
                        for (let data in datas) {
                            let signUrlIndex = result[i].signatureUrl.findIndex(sign => sign.OwnerId === result[i].contractRecord.Steps.records[i].ActorId);
                            let signUrl = signUrlIndex >= 0 ? result[i].signatureUrl[signUrlIndex].Id : '';
                            resultArray.push({
                                caseWorkerName: datas[data].Actor.Name,
                                assignedStatus: datas[data].StepStatus == 'Started' ? 'Submitted' :  (datas[data].StepStatus == 'Removed' ? 'Returned' : datas[data].StepStatus),
                                superVisorName: datas[parseInt(data) - 1] !== undefined ? datas[parseInt(data) - 1].Actor.Name : ' - ',
                                comments: datas[data].Comments,
                                caseWorkerDesignation: datas[data].Actor.Profile.Name == 'Supervisor' ? 'Supervisor' : 'OLM',
                                superVisorDesignation: datas[parseInt(data) - 1] !== undefined ? datas[data - 1].Actor.Profile.Name : ' - ',
                                SubmittedDate__c: utils.formatDate(datas[data].CreatedDate),
                                SignatureValue: signUrl
                            });

                            if(datas[data].StepStatus == "Removed" && this.isSuperUserFlow === false){
                                this.selectedRows.push(datas[data].Actor.Id);
                            }
                        }
                    }
    
                    if (result[i].contractRecord.Steps.totalSize === 1)
                        this.workItemId = result[i].contractRecord.Workitems.records[0].Id;
    
                    this.isSuperUserAssigned = true;
                }
            }
            this.assignedSuperUserTableData = [...resultArray];
        })
    }

    //Get Object Info
    @wire(getObjectInfo, {
        objectApiName: CONTRACT_OBJECT
    })
    objectInfo;

    //Get PickList Values from Contract Object
    @wire(getPicklistValuesByRecordType, {
        objectApiName: CONTRACT_OBJECT,
        recordTypeId: '$objectInfo.data.defaultRecordTypeId'
    })
    wiredPicklistValues({ error, data }) {
        if (data) {
            let picklistArray = [];

            if (this.isSuperUserFlow) {
                data.picklistFieldValues.ReviewStatus__c.values.forEach(key => {
                    if (['Approve', 'Return to Caseworker'].includes(key.label)) {
                        picklistArray.push({
                            label: key.label,
                            value: key.value
                        });
                    }
                })
            } else {
                if (!this.superUserSelectedReviewStatus) {
                    data.picklistFieldValues.ReviewStatus__c.values.forEach(key => {
                        if (['None', 'Submit for Review', 'Reject'].includes(key.label)) {
                            picklistArray.push({
                                label: key.label,
                                value: key.value
                            });
                        }
                    })
                } else {
                    data.picklistFieldValues.ReviewStatus__c.values.forEach(key => {
                        picklistArray.push({
                            label: key.label,
                            value: key.value
                        })
                    })
                }
            }
            this.statusOptions = [...picklistArray];

        } else if (error) {
            this.dispatchEvent(utils.toastMessage("Error in fetching Status Picklist Values", "warning"));
        }
    }

    //Fields On Change method
    reviewOnChange(event) {
        if (event.target.name == 'Status') {
            if (this.isSuperUserFlow === true) {
                this.isSubmitBtnDisable = ['Approve', 'Return to Caseworker'].includes(event.detail.value) == -1 ? true : false;
                this.isTierApprovalBtnDisable = true;
                this.isSignatureDisable = false;
                this.reviewDetails.ReviewStatus__c = event.detail.value;
            } else {
                this.isSubmitBtnDisable = ['', 'None', 'Approve', 'Return to Caseworker'].includes(event.detail.value) ? true : false;
                this.isTierApprovalBtnDisable = ['', 'None', 'Reject', 'Approval', 'Return to Caseworker'].includes(event.detail.value) ? true : false;
                this.isSignatureDisable = false;
                this.isSuperUserAssigned = ['', 'None', 'Reject'].includes(event.detail.value) ? false : (this.assignedSuperUserTableData.length > 0 ? true : false);

                this.reviewDetails.ReviewStatus__c = event.detail.value;
            }
        } else if (event.target.name == 'Comments') {
            if (this.isSuperUserFlow === true) {
                this.reviewDetails.SupervisorComments__c = event.target.value;
            } else {
                this.reviewDetails.ReviewComments__c = event.target.value;
            }
        }
    }

    //Get Supervisor Users List to assign
    @wire(getAllSuperUsersList)
    wiredSuperUserDetails({ error, data }) {
        if (data) {
            if (data.length > 0) {
                this.superUsersTableData = data.filter(item => item.Profile.Name == 'Supervisor');
            } else {
                this.dispatchEvent(utils.toastMessage("Supervisor users not found. please check.", "warning"));
            }
        } else if (error) {
            this.dispatchEvent(utils.toastMessage("Error in fetching Supervisor users. please check", "warning"));
        }
    }

    //On Click event for Tier Approval Model
    handleSuperUserModel() {
        if (this.reviewDetails && !this.reviewDetails.ReviewStatus__c)
            return this.dispatchEvent(utils.toastMessage("Please select status", "warning"));

        if (this.isSuperUserFlow === true)
            return this.dispatchEvent(utils.toastMessage("Sorry, you do not have access to add superusers.", "warning"));

        this.openSuperUserModel = true;
        this.assignBtnDisable = true;
        let localvar = [...this.superUsersTableData];
        this.superUsersTableData = localvar.map((row) => {
            return Object.assign({
                ZipCode: '',
                ProfileName: row.Profile.Name
            }, row)
        });
    }

    //Row Selection in Super User Model
    handleSuperUserRowSelection(event) {
        this.isSuperUserSelected = true;
        this.Supervisor__c = event.detail.selectedRows[0].Id
        this.Caseworker__c = userId;
        this.assignBtnDisable = false;
    }

    //Assigning Super User 
    handleAssignMethod() {
        if (!this.Supervisor__c)
            return this.dispatchEvent(utils.toastMessage("Atleast one supervisor selection is mandatory", "warning"));

        this.assignBtnDisable = true;

        let currentDate = new Date().getDate() < 10 ? '0' + new Date().getDate() : new Date().getDate();
        let currentMonth = ((new Date().getMonth()) + 1) < 10 ? '0' + ((new Date().getMonth()) + 1) : ((new Date().getMonth()) + 1);

        this.SubmittedDate__c = new Date().getFullYear() + '-' + currentMonth + '-' + currentDate;

        const fields = {};

        fields[ID_FIELD.fieldApiName] = this.contractId;
        fields[REVIEWSTATUS_FIELD.fieldApiName] = this.reviewDetails.ReviewStatus__c;
        fields[COMMENTS_FIELD.fieldApiName] = this.reviewDetails.ReviewComments__c;
        fields[SUPERVISOR_FIELD.fieldApiName] = this.Supervisor__c;
        fields[CASEWORKER_FIELD.fieldApiName] = this.Caseworker__c;
        fields[SUBMITTEDDATE_FIELD.fieldApiName] = this.SubmittedDate__c;

        const recordInput = {
            fields
        };

        updateRecord(recordInput)
            .then(() => {
                this.assignBtnDisable = false;
                this.getAssignedSuperUserTableValues();
                this.openSuperUserModel = false;

                this.dispatchEvent(utils.toastMessage("Supervisor has been assigned successfully", "success"));
            })
            .catch(error => {
                this.assignBtnDisable = false;
                this.openSuperUserModel = false;
            })
    }

    //Close Super User Model
    superUserCloseModel() {
        this.openSuperUserModel = false;
        this.assignBtnDisable = false;
    }

    //Preview Model Action method
    handleRowAction(event) {
        this.openPreviewModel = true;

        this.previewDetails = {
            authorName: event.detail.row.caseWorkerName,
            assignedDate: event.detail.row.SubmittedDate__c,
            assignedStatus: event.detail.row.assignedStatus,
            comments: event.detail.row.comments,
            SignatureValue: '/sfc/servlet.shepherd/version/download/' + event.detail.row.SignatureValue
        }
    }

    //This method used to Close the Preview Model when user has clicked DONE/Close Button
    previewCloseModel() {
        this.openPreviewModel = false;
    }

    /**********Canvas Signature******/

    //Open Signature Model
    handleSignatureModel() {
        this.openSignatureModel = true;
        canvasElement = this.template.querySelector('.sign-box');
        ctx = canvasElement.getContext("2d");
        this.drawCanvasImage();
    }

    //Upload Signature from Local System
    handleFilesChange(event) {
        let strFileNames = '';

        const uploadedFiles = event.detail.files;

        for (let i = 0; i < uploadedFiles.length; i++) {
            strFileNames += uploadedFiles[i].name + ', ';
        }

        this.dispatchEvent(utils.toastMessage(`${strFileNames} files uploaded successfully`, "success"));
    }

    //clear the signature from canvas
    handleClearClick() {
        ctx.clearRect(0, 0, canvasElement.width, canvasElement.height);
        this.isSignatureRemoved = true;
        dataURL = "";
    }

    //event listeners added for drawing the signature within shadow boundary
    constructor() {
        super();
        this.template.addEventListener('mousemove', this.handleMouseMove.bind(this));
        this.template.addEventListener('mousedown', this.handleMouseDown.bind(this));
        this.template.addEventListener('mouseup', this.handleMouseUp.bind(this));
        this.template.addEventListener('mouseout', this.handleMouseOut.bind(this));
    }

    //handler for mouse move operation
    handleMouseMove(event) {
        this.searchCoordinatesForEvent('move', event);
    }

    //handler for mouse down operation
    handleMouseDown(event) {
        this.searchCoordinatesForEvent('down', event);
    }

    //handler for mouse up operation
    handleMouseUp(event) {
        this.searchCoordinatesForEvent('up', event);
    }

    //handler for mouse out operation
    handleMouseOut(event) {
        this.searchCoordinatesForEvent('out', event);
    }

    handleSaveClick() {
        //set to draw behind current content
        ctx.globalCompositeOperation = "destination-over";
        ctx.fillStyle = "#FFF"; //white
        ctx.fillRect(0, 0, canvasElement.width, canvasElement.height);

        //convert to png image as dataURL
        dataURL = canvasElement.toDataURL("image/png");

        //convert that as base64 encoding
        convertedDataURI = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");

        //call Apex method imperatively and use promise for handling sucess & failure
        saveSign({
                strSignElement: convertedDataURI,
                recId: this.contractId
            })
            .then(() => {
                this.getAssignedSuperUserTableValues();
                this.openSignatureModel = false;

                this.dispatchEvent(utils.toastMessage("Signature has been saved successfully", "success"));
            })
            .catch(error => {
                this.dispatchEvent(utils.toastMessage("Error in creating signature. please check", "error"));
            })
    }

    searchCoordinatesForEvent(requestedEvent, event) {
        if (requestedEvent === 'down') {
            this.setupCoordinate(event);
            isDownFlag = true;
            isDotFlag = true;
            if (isDotFlag) {
                this.drawDot();
                isDotFlag = false;
            }
        }

        if (requestedEvent === 'up' || requestedEvent === "out") {
            isDownFlag = false;
        }

        if (requestedEvent === 'move') {
            if (isDownFlag) {
                this.setupCoordinate(event);
                this.redraw();
            }
        }
    }

    //This method is primary called from mouse down & move to setup cordinates.
    setupCoordinate(eventParam) {
        //get size of an element and its position relative to the viewport 
        //using getBoundingClientRect which returns left, top, right, bottom, x, y, width, height.
        const clientRect = canvasElement.getBoundingClientRect();
        prevX = currX;
        prevY = currY;
        currX = eventParam.clientX - clientRect.left;
        currY = eventParam.clientY - clientRect.top;
    }

    //For every mouse move based on the coordinates line to redrawn
    redraw() {
        ctx.beginPath();
        ctx.moveTo(prevX, prevY);
        ctx.lineTo(currX, currY);
        ctx.strokeStyle = x; //sets the color, gradient and pattern of stroke
        ctx.lineWidth = y;
        ctx.closePath(); //create a path from current point to starting point
        ctx.stroke(); //draws the path
    }

    //this draws the dot
    drawDot() {
        ctx.beginPath();
        ctx.fillStyle = x; //blue color
        ctx.fillRect(currX, currY, y, y); //fill rectrangle with coordinates
        ctx.closePath();
    }

    //retrieve canvase and context
    renderedCallback() {

        canvasElement = this.template.querySelector('canvas');
        ctx = canvasElement.getContext("2d");

        //This method used to get the Uploaded Image and convert into Canvas Image Base64 Format
        getBase64Image({
                appID: this.contractId
            })
            .then(result => {
                this.imageBase64Format = "data:image/png;base64," + result;
                this.drawCanvasImage();
            })
    }

    //This method used to set Image format as Canvas Image format.
    drawCanvasImage() {
        let signatureImage = new Image();
        signatureImage.onload = function () {
            ctx.drawImage(this, 0, 0);
        }
        signatureImage.setAttribute("src", this.imageBase64Format);
    }

    //Close Signature Model
    signatureCloseModel() {
        this.openSignatureModel = false;
    }
    /* Signature Model End */

    // Draft Method On Click
    handleDraftMethod() {
        if (this.reviewDetails && !this.reviewDetails.ReviewStatus__c)
            return this.dispatchEvent(utils.toastMessage("Please Select Status", "warning"));

        this.isDraftBtnDisable = true;

        let currentDate = new Date().getDate() < 10 ? '0' + new Date().getDate() : new Date().getDate();
        let currentMonth = ((new Date().getMonth()) + 1) < 10 ? '0' + ((new Date().getMonth()) + 1) : ((new Date().getMonth()) + 1);

        const fields = {};

        fields[ID_FIELD.fieldApiName] = this.contractId;
        fields[REVIEWSTATUS_FIELD.fieldApiName] = this.reviewDetails.ReviewStatus__c;
        fields[COMMENTS_FIELD.fieldApiName] = this.reviewDetails.ReviewComments__c;

        if (this.isSuperUserFlow === true)
            fields[CONTRACTSTATUS_FIELD.fieldApiName] = CJAMS_CONSTANTS.CONTRACT_STATUS_SUBMITTED;
        else
            fields[CONTRACTSTATUS_FIELD.fieldApiName] = CJAMS_CONSTANTS.CONTRACT_STATUS_INPROCESS;

        if (this.SignatureValidation)
            fields[SIGNATURE_FIELD.fieldApiName] = true;

        if (this.isSuperUserSelected === true)
            fields[SUBMITTEDDATE_FIELD.fieldApiName] = new Date().getFullYear() + '-' + currentMonth + '-' + currentDate;

        const recordInput = {
            fields
        };

        updateRecord(recordInput)
            .then(() => {
                this.isDraftBtnDisable = false;
                this.dispatchEvent(utils.toastMessage("Contract has been saved successfully", "success"));
                window.location.reload();
            })
            .catch(error => {
                this.isDraftBtnDisable = false;
                this.dispatchEvent(utils.toastMessage("Error in saving contract details. please check", "error"));
            })
    }

    /*isEmptyCanvas() {
        var emptyCanvas = document.createElement("canvas");
        return (emptyCanvas.toDataURL() === canvasElement.toDataURL());
    }*/

    //Submit Method
    handleSubmitMethod() {
        if (this.reviewDetails && !this.reviewDetails.ReviewStatus__c)
            return this.dispatchEvent(utils.toastMessage("Please Select Status", "warning"));

        if (canvasElement.toDataURL() === 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAACWCAYAAABkW7XSAAAEaElEQVR4Xu3UgQkAMAwCwXb/oS10i4fLBHIG77YdR4AAgYDANViBlkQkQOALGCyPQIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiDwAIGUVl1ZhH69AAAAAElFTkSuQmCC') {
            return this.dispatchEvent(utils.toastMessage("Signature Is Mandatory", "warning"));
        }

        if (this.assignedSuperUserTableData.length == 0 && (this.reviewDetails && this.reviewDetails.ReviewStatus__c == CJAMS_CONSTANTS.REVIEW_STATUS_SUBMITFORREVIEW))
            return this.dispatchEvent(utils.toastMessage("Tier of Approval is mandatory", "warning"));

        this.isSubmitBtnDisable = true;

        let currentDate = new Date().getDate() < 10 ? '0' + new Date().getDate() : new Date().getDate();
        let currentMonth = ((new Date().getMonth()) + 1) < 10 ? '0' + ((new Date().getMonth()) + 1) : ((new Date().getMonth()) + 1);

        const fields = {};

        fields[ID_FIELD.fieldApiName] = this.contractId;
        fields[REVIEWSTATUS_FIELD.fieldApiName] = this.reviewDetails.ReviewStatus__c;
        fields[SIGNATURE_FIELD.fieldApiName] = true;

        if (this.isSuperUserFlow === true)
            fields[SUPERVISORCOMMENTS_FIELD.fieldApiName] = this.reviewDetails.SupervisorComments__c;
        else
            fields[COMMENTS_FIELD.fieldApiName] = this.reviewDetails.ReviewComments__c;

        if (this.reviewDetails.ReviewStatus__c == CJAMS_CONSTANTS.REVIEW_STATUS_SUBMITFORREVIEW) {
            fields[CONTRACTSTATUS_FIELD.fieldApiName] = CJAMS_CONSTANTS.CONTRACT_STATUS_SUBMITTED;
            fields[SUBMITTEDDATE_FIELD.fieldApiName] = new Date().getFullYear() + '-' + currentMonth + '-' + currentDate;
        } else if (this.reviewDetails.ReviewStatus__c == CJAMS_CONSTANTS.REVIEW_STATUS_REJECT) {
            fields[CONTRACTSTATUS_FIELD.fieldApiName] = CJAMS_CONSTANTS.CONTRACT_STATUS_INACTIVE;
            fields[SUPERVISOR_FIELD.fieldApiName] = null;
            fields[CASEWORKER_FIELD.fieldApiName] = null;
            fields[SUBMITTEDDATE_FIELD.fieldApiName] = null;
        } else if (this.reviewDetails.ReviewStatus__c == CJAMS_CONSTANTS.REVIEW_STATUS_APPROVED) {
            fields[CONTRACTSTATUS_FIELD.fieldApiName] = CJAMS_CONSTANTS.CONTRACT_STATUS_ACTIVE;
        } else if (this.reviewDetails.ReviewStatus__c == CJAMS_CONSTANTS.REVIEW_STATUS_REWORK) {
            fields[CONTRACTSTATUS_FIELD.fieldApiName] = CJAMS_CONSTANTS.CONTRACT_STATUS_RETURNED;
        } else {
            fields[CONTRACTSTATUS_FIELD.fieldApiName] = CJAMS_CONSTANTS.CONTRACT_STATUS_INPROCESS;
        }

        let selectedRows = this.selectedRows[0] == undefined ? '' : this.selectedRows[0];
        let workItemId = this.workItemId === '' ? '' : this.workItemId;

        const recordInput = {
            fields
        };

        updateRecord(recordInput)
        .then((result) => {
            this.isSubmitBtnDisable = false;
            if (this.reviewDetails && (['Submit for Review', 'Approve', 'Return to Caseworker'].includes(this.reviewDetails.ReviewStatus__c))) {
                let resultObj = {
                    Id: result.id,
                    ReviewComments__c: result.fields.ReviewComments__c.value,
                    SupervisorComments__c : result.fields.SupervisorComments__c.value,
                    ReviewStatus__c: result.fields.ReviewStatus__c.value,
                    Caseworker__c: result.fields.Caseworker__c.value
                };

                let finalStatus = this.reviewDetails.ReviewStatus__c == 'Submit for Review' ? 'Submitted' : this.reviewDetails.ReviewStatus__c == 'Approve' ? 'Approved' : this.reviewDetails.ReviewStatus__c == 'Return to Caseworker' ? 'Reassigned' : '';

                updateapplicationdetails({
                        resultObj: resultObj,
                        supervisorId: selectedRows,
                        workItemId: workItemId
                    })
                    .then(() => {
                        this.dispatchEvent(utils.toastMessage(`Contract has been ${finalStatus} successfully`, "success"));
                        this.RedirecttoContractDashboard();
                    })
                    .catch(error => {
                        this.dispatchEvent(utils.toastMessage("Error in submitting contract details. please check", "error"));
                    })
            } else {
                this.dispatchEvent(utils.toastMessage("Contract has been rejected successfully", "success"));
                this.RedirecttoContractDashboard();
            }
        })
        .catch(error => {
            this.isSubmitBtnDisable = false;
            this.dispatchEvent(utils.toastMessage("Error in submitting contract details. please check", "error"));
        })
    }

    RedirecttoContractDashboard(){
        const onredirecttocontract = new CustomEvent('redirecttocontractdashboard', {
            detail: {
                first: true
            }
        });
        this.dispatchEvent(onredirecttocontract);
    }
}