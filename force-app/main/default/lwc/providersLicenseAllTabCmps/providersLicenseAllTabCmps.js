/**
 * @Author        : Preethi Bojarajan
 * @CreatedOn     : Apr 21,2020
 * @Purpose       : Providers License All Tab Combine
 * @updatedBy     : 
 * @updatedOn     : 
 **/

import { LightningElement, track } from 'lwc';

export default class ProvidersLicenseAllTabCmps extends LightningElement {
    @track showDashboard = true;
    @track showProviderInfo = false;
    @track showProviderMonitering = false;
    @track activeTab = 'providerInfo';
    @track showContractInfo = false;
    @track showContractAdd = false;
    @track showStaffInfo = false;
    @track providerStaffId = null;
    @track showApplicationTabs = false;

    handleDashboardShowHide(event) {
        this.showDashboard = event.detail.first;
        this.showProviderInfo = true;
    }

    handleProviderMoniteringTabs(event) {
        this.showProviderInfo = event.detail.first;
        this.showDashboard = event.detail.first;
        this.showProviderMonitering = true;
    }

    handleredirectToMoniter(event) {
        this.showProviderMonitering = false;
        this.showProviderInfo = event.detail.first;
        this.activeTab = 'monitoring';
    }

    handleContractManage(event) {
        this.showProviderInfo = event.detail.first;
        this.showDashboard = event.detail.first;
        this.showContractInfo = true;
        this.showContractAdd = false;
    }

    handleContractAdd(event) {
        this.showProviderInfo = event.detail.first;
        this.showContractAdd = true;
        this.showContractInfo = false;
    }

    handleContractDashboard(event) {
        this.showProviderInfo = event.detail.first;
        this.showContractAdd = false;
        this.showContractInfo = false;
        this.activeTab = 'contract';
    }

    handleredirectToStaffInfo(event) {
        if (event.detail.first === false) {
            this.showProviderInfo = event.detail.first;
            this.showStaffInfo = true;
            this.providerStaffId = event.detail.staffId;
        } else {
            this.showProviderInfo = event.detail.first;
            this.showStaffInfo = false;
            this.activeTab = 'staff';
            this.providerStaffId = null;
        }
    }

    handletoApplication(event) {
        this.showProviderInfo = event.detail.first;
        this.showApplicationTabs = true;
    }
}