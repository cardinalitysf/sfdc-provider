/**
 * @Author        : Praveen
 * @CreatedOn     : Apr 15,2020
 * @Purpose       : Attachments based on myProfile Documents
 **/

    import {
        LightningElement,
        track
    
    } from 'lwc';
    import * as referralsharedDatas from "c/sharedData";
    import {
        CJAMS_CONSTANTS
    } from 'c/constants';
    
    
    export default class ProviderMyProfileDocuments extends LightningElement {
        get providerId() {
            return referralsharedDatas.getProviderId();
        }
        get sobjectNameToFetch() {
            return CJAMS_CONSTANTS.PROVIDER_OBJECT_NAME;
        }
    
    }