/**
 * @Author        : Preethi Bojarajan
 * @CreatedOn     : Mar 31,2020
 * @Purpose       : Monitering Records for Staff / Plant and Office Inspection
 * @updatedBy     : Preethi Bojarajan
 * @updatedOn     : Jul 30 ,2020
 **/
import { LightningElement, track, wire, api } from 'lwc';

// Utils
import { utils } from 'c/utils';
import * as sharedData from "c/sharedData";
import { constPopupVariables,CJAMS_CONSTANTS } from 'c/constants';

//For Create, Refresh Records in Table utils
import { refreshApex } from "@salesforce/apex";
import { createRecord, updateRecord } from "lightning/uiRecordApi";
import { deleteRecord } from "lightning/uiRecordApi";
import { CurrentPageReference } from 'lightning/navigation';
import { fireEvent } from 'c/pubsub';

import REFVALUE_OBJECT from '@salesforce/schema/ReferenceValue__c';
import REGULATIONS_FIELD from '@salesforce/schema/ReferenceValue__c.Regulations__c';
import QUESTION_FIELD from '@salesforce/schema/ReferenceValue__c.Question__c';
import TYPE_FIELD from '@salesforce/schema/ReferenceValue__c.Type__c';
import RECORDTYPEID_FIELD from '@salesforce/schema/ReferenceValue__c.RecordTypeId';

//Record Fields
import PROVIDERRECANS_OBJECT from '@salesforce/schema/ProviderRecordAnswer__c';
import ID_FIELD from '@salesforce/schema/ProviderRecordAnswer__c.Id';
import COMAR_FIELD from '@salesforce/schema/ProviderRecordAnswer__c.Comar__c';
import COMPLIANCE_FIELD from '@salesforce/schema/ProviderRecordAnswer__c.Compliance__c';
import DATE_FIELD from '@salesforce/schema/ProviderRecordAnswer__c.Date__c';
import FINDINGS_FIELD from '@salesforce/schema/ProviderRecordAnswer__c.Findings__c';
import FREQUENCYOFDEF_FIELD from '@salesforce/schema/ProviderRecordAnswer__c.FrequencyofDeficiency__c';
import IMPACTOFDEF_FIELD from '@salesforce/schema/ProviderRecordAnswer__c.ImpactofDeficiency__c';
import SCOPEOFDEF_FIELD from '@salesforce/schema/ProviderRecordAnswer__c.Scope_of_Deficiency__c';
import PROVIDERRECQUES_FIELD from '@salesforce/schema/ProviderRecordAnswer__c.ProviderRecordQuestion__c';
import PROVIDERRECQUES_OBJECT from '@salesforce/schema/ProviderRecordQuestion__c';
import MONITERIG_FIELD from '@salesforce/schema/ProviderRecordQuestion__c.Monitoring__c';
import STAFFQUES_FIELD from '@salesforce/schema/ProviderRecordQuestion__c.Name__c';
import STATUS_FIELD from '@salesforce/schema/ProviderRecordQuestion__c.Status__c';
import SITE_FIELD from '@salesforce/schema/ProviderRecordQuestion__c.Site__c';
import RECTYPEQUES_FIELD from '@salesforce/schema/ProviderRecordQuestion__c.RecordTypeId';
import DATEQUES_FIELD from '@salesforce/schema/ProviderRecordQuestion__c.Date__c';
import NCQUES_FIELD from '@salesforce/schema/ProviderRecordQuestion__c.ANYNC__c';
import PENDGQUES_FIELD from '@salesforce/schema/ProviderRecordQuestion__c.ANYPending__c';
import IDQUES_FIELD from '@salesforce/schema/ProviderRecordQuestion__c.Id';

//Apex Classes
import getUserEmail from '@salesforce/apex/providerApplication.getUserEmail';
import getRecordDetails from '@salesforce/apex/moniteringComarRecords.getRecordDetails';
import getQuestionsByType from '@salesforce/apex/moniteringComarRecords.getQuestionsByType';
import applicationStaff from "@salesforce/apex/moniteringComarRecords.applicationStaff";
import getStaffFromApplicationId from "@salesforce/apex/moniteringComarRecords.getStaffFromApplicationId";
import bulkAddRecordAns from "@salesforce/apex/moniteringComarRecords.bulkAddRecordAns";
import getSiteId from "@salesforce/apex/moniteringComarRecords.getSiteId";
import editRecordDet from "@salesforce/apex/moniteringComarRecords.editRecordDet";
import getRecordType from "@salesforce/apex/moniteringComarRecords.getRecordType";
import bulkUpdateRecordAns from "@salesforce/apex/moniteringComarRecords.bulkUpdateRecordAns";
import images from '@salesforce/resourceUrl/images';

export default class PhysicalPlantInspection extends LightningElement {
    //Get Active Tab Record
    @api getActiveTab = null;
    @track hasStaffEnable = true;
    @track hasStaffCreate = false;
    @track staffLists = [];
    @track staffId = null;
    @track disableFields = false;
    @track Spinner = true;
    //DataTable Data
    @track dataTableShow = true;
    @track columns = [];
    @track tabContent = '';
    @track wiredRecordDetails = [];
    wiredEditReg;
    @track totalRecords = [];
    @track datas = [];
    @api siteId = null;
    @track programSiteName = null;
    @api recordTypeId = null;
    @track deleteLoader = false;

    //Label for New Record Creation
    @track newRecordCreate = null;
    @track openModel = false;
    @track createComar = {};
    @track createNewRecords = [];
    @api type = null;
    @track recordNewModel;
    @track questions = [];
    @track editedQuestions = [];
    questionsRegulations;
    @api updateMode = false;
    @track deleteRowId = null;
    @track openmodelDel = false;
    @api editRecordId = null;
    @track btnEnable = true;
    @track hideAddButton = false;
    attachmentIcon = images + '/application-licenseinfo.svg';

    //Pagination Tracks    
    @track page = 1;
    perpage = 10;
    setPagination = 5;
    @track totalRecordsCount = 0;

    //DataTable RowAction Method Start
    constructor() {
        super();
        this.columns = [{
            label: 'Any NC',
            fieldName: 'ANYNC__c',
            sortable: true
        },
        {
            label: 'Any Pending',
            fieldName: 'ANYPending__c',
        },
        {
            label: 'Update Date',
            fieldName: 'Date__c'
        },
        {
            label: 'Updated By',
            fieldName: 'LastModifiedBy',
            initialWidth: 220
        },
        {
            label: 'Status',
            fieldName: 'Status__c',
            type: 'text',
            cellAttributes: {
                class: {
                    fieldName: 'statusClass'
                }
            }
        },
        {
            label: 'Action',
            type: 'button',
            initialWidth: 100,
            fieldName: 'id',
            typeAttributes: {
                iconName: 'utility:preview',
                name: 'viewRecord',
                title: 'view',
                disabled: false,
                initialWidth: 20,
                target: '_self'
            }
        }, {
            type: 'button',
            initialWidth: 40,
            fieldName: 'id',
            typeAttributes: {
                iconName: 'utility:edit',
                name: 'editRecord',
                title: 'Edit',
                disabled: false,
                initialWidth: 20,
                class: "action-position"
            }
        }, {
            type: 'button',
            fieldName: 'id',
            initialWidth: 40,
            typeAttributes: {
                iconName: 'utility:delete',
                name: 'deleteRecordRow',
                title: 'Delete',
                disabled: false,
                initialWidth: 20,
                class: "del-red action-positiontrd",
            }
        }
        ]
    }
    @wire(CurrentPageReference) pageRef;
    // Sorting the Data Table Column Function End
    sortBy(field, reverse, primer) {
        const key = primer ?
            function (x) {
                return primer(x[field]);
            } :
            function (x) {
                return x[field];
            };

        return function (a, b) {
            a = key(a);
            b = key(b);
            if (a === undefined) a = '';
            if (b === undefined) b = '';
            a = typeof (a) === 'number' ? a : a.toLowerCase();
            b = typeof (b) === 'number' ? b : b.toLowerCase();

            return reverse * ((a > b) - (b > a));
        };
    }

    onHandleSort(event) {
        const {
            fieldName: sortedBy,
            sortDirection: sortDirection
        } = event.detail;
        const cloneData = [...this.datas];

        cloneData.sort(this.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1));
        this.datas = cloneData;
        this.sortDirection = sortDirection;
        this.sortedBy = sortedBy;
    }
    // Sorting the Data Table Column Function End
    get moniteringId() {
        return sharedData.getMonitoringId();
    }
    get applicationId() {
        return sharedData.getApplicationId();
    }

    get monitoringStatus() {
        return sharedData.getMonitoringStatus();
    }
    //Depending the Logged In User Getting the Monitering Id of the User Function End

    connectedCallback() {
        if (this.userProfileName == 'Supervisor') {
            this.hideAddButton = true;

        } else {
            this.hideAddButton = false;
        }

        if (this.getActiveTab == 'officeTab') {
            this.newRecordCreate = 'Add Office Record';
            this.type = 'Office';
            this.popUpForSuccess = constPopupVariables[this.getActiveTab];
            this.popUpForUpdate = constPopupVariables[this.getActiveTab + 'Update'];
            this.hasStaffEnable = false;
            this.columns.unshift({
                label: 'Site',
                type: 'button',
                sortable: true,
                typeAttributes: {
                    name: 'dontRedirect',
                    label: {
                        fieldName: 'siteAddress'
                    }
                },
                cellAttributes: {
                    class: "title"
                }
            })
        } else if (this.getActiveTab == 'plantTab') {
            this.newRecordCreate = 'Add Site Record';
            this.type = 'Physical Plant';
            this.popUpForSuccess = constPopupVariables[this.getActiveTab];
            this.popUpForUpdate = constPopupVariables[this.getActiveTab + 'Update'];
            this.hasStaffEnable = false;
            this.columns.unshift({
                label: 'Site',
                type: 'button',
                sortable: true,
                typeAttributes: {
                    name: 'dontRedirect',
                    label: {
                        fieldName: 'siteAddress'
                    }
                },
                cellAttributes: {
                    class: "title"
                }
            })
        } else {
            this.newRecordCreate = 'Add New Staff Record';
            this.type = 'Staff Record';
            this.popUpForSuccess = constPopupVariables.staffTab;
            this.popUpForUpdate = constPopupVariables.staffTabUpdate;
            this.hasStaffEnable = true;
            this.columns.unshift({
                label: 'Name',
                fieldName: 'FirstName__c',
                sortable: true
            })
        }
        if (this.monitoringStatus !== 'Draft')
            this.disableFields = true
    }

    pageData() {
        let page = this.page;
        let perpage = this.perpage;
        let startIndex = page * perpage - perpage;
        let endIndex = page * perpage;
        this.datas = [...this.totalRecords.slice(startIndex, endIndex)];
    }

    //Get Record Detais of Staff / Physical Plant / Office Function Start
    get siteId() {
        return sharedData.getMonitoringAddressId();
    }

    @wire(getSiteId, {
        siteId: '$siteId'
    })
    siteDetails(data) {
        if (data.data != undefined && data.data.length > 0) {
            this.programSiteName = data.data[0].AddressLine1__c;
        }
    }

    @wire(getRecordType, {
        name: '$type'
    })
    recordTypeDetails(data) {
        if (data.data) 
            this.recordTypeId = data.data;
    }

    @wire(getRecordDetails, {
        moniteringId: '$moniteringId',
        siteId: '$siteId',
        recordType: '$recordTypeId'
    })
    recordDetails(data) {
        this.wiredRecordDetails = data;
        if (data.data != null && data.data != '') {
            this.dataTableShow = true;
            let currentData = [];
            //this.programSiteName = data.data[0].Site__r.AddressLine1__c;
            data.data.forEach((row) => {
                let rowData = {};
                rowData.id = row.Id;
                rowData.Monitoring__c = row.Monitoring__c;
                rowData.siteAddress = row.Site__r.AddressLine1__c;
                rowData.ANYNC__c = row.ANYNC__c;
                rowData.ANYPending__c = row.ANYPending__c;
                rowData.Status__c = row.Status__c;
                rowData.LastModifiedBy = row.LastModifiedBy.Name;
                rowData.FirstName__c = row.Name__r == undefined ? " " : row.Name__r.FirstName__c;
                rowData.Name__c = row.Name__c == undefined ? " " : row.Name__c;
                rowData.Date__c = utils.formatDate(row.Date__c);
                rowData.statusClass = row.Status__c === 'Completed' ? 'Approved' : row.Status__c;
                currentData.push(rowData);
            });
            this.totalRecords = currentData;
            this.totalRecordsCount = this.totalRecords.length;
            this.Spinner = false;
            this.pageData();
        } else {
            this.dataTableShow = false;
        }
    }
    //Get Record Detais of Staff / Physical Plant / Office Function End

    //New Record Create for Staff / Physical Plant / Office Function Start

    /* New Record Creation */
    createNewRecordModelPop(event) {

        if (this.getActiveTab == 'officeTab' || this.getActiveTab == 'plantTab' || this.hasStaffCreate == true) {
            if (event.target.name == 'staff')
                this.staffId = event.target.value;
            if (this.getActiveTab == 'officeTab')
                this.type = 'Office';
            else if (this.getActiveTab == 'plantTab')
                this.type = 'Physical Plant';
            else this.type = 'Staff Record';
            if (this.type == 'Staff Record') {
                if (this.staffId == null)
                    return;
            }
            this.recordNewModel = true;
            this.hasStaffCreate = false;
            this.btnEnable = true;
            refreshApex(this.questionsRegulations);
        } else {
            //this.staffListLoad();
            this.hasStaffCreate = true;
        }
        this.Spinner = false;
    }

    closeNewRecord() {
        this.recordNewModel = false;
        this.btnEnable = true;
        this.updateMode = false;
        this.staffId = null;
        this.type = '';
    }


    @wire(getQuestionsByType, {
        type: '$type'
    })
    wiredquestions(data) {
        this.questionsRegulations = data;
        if (data.data != null && data.data != '') {
            let currentData = [];
            let id = 1;
            this.questions = [];
            data.data.forEach((row) => {
                let rowData = {};
                rowData.rowId = id;
                rowData.RegulationId = row.Id;
                rowData.Regulations__c = row.Regulations__c;
                rowData.Question__c = row.Question__c;
                rowData.Compliance__c = 'Pending';
                rowData.Date__c = utils.formatDateYYYYMMDD(new Date());
                rowData.Findings__c = null;
                rowData.FrequencyofDeficiency__c = null;
                rowData.ImpactofDeficiency__c = null;
                rowData.Scope_of_Deficiency__c = null;
                rowData.ProviderRecordQuestion__c = null;
                currentData.push(rowData);
                id++;
            });
            this.questions = currentData;
            this.Spinner = false;
        }
    }
    //New Record Create for Staff / Physical Plant / Office Function End    

    /* On-Click Button Change Functionlity on New Record Creation Start */
    openNewRow(event) {
        if (event.target != undefined && event.target.name != undefined && event.target.name != null) {
            let allD = this.template.querySelectorAll('tbody');
            allD[event.target.name - 1].rows[1].className = 'displayContent';
            allD[event.target.name - 1].rows[0].cells[1].childNodes.forEach((but) => {
                but.className = but.className.includes('backgroComboColor') == true ? but.className.replace(' backgroComboColor', '') : but.className;
                but.className = 2 === parseInt(but.dataset.id, 10) ? but.className + ' backgroComboColor' : but.className;
            });
            this.questions[event.target.name - 1].Compliance__c = 'Non-Compliance';
        } else {

            let index = parseInt(event);
            let allD = this.template.querySelectorAll('tbody');
            allD[index].rows[1].className = 'displayContent';
            allD[index].rows[0].cells[1].childNodes.forEach((but) => {
                but.className = but.className.includes('backgroComboColor') == true ? but.className.replace(' backgroComboColor', '') : but.className;
                but.className = 2 === parseInt(but.dataset.id, 10) ? but.className + ' backgroComboColor' : but.className;
            });
        }
    }

    selectedCompliance(event) {
        if (event.target != undefined && event.target.name != undefined && event.target.name != null) {
            let allD = this.template.querySelectorAll('tbody');
            allD[event.target.name - 1].rows[1].className = 'displayNone';
            allD[event.target.name - 1].rows[0].cells[1].childNodes.forEach((but) => {
                but.className = but.className.includes('backgroComboColor') == true ? but.className.replace(' backgroComboColor', '') : but.className;
                but.className = 1 === parseInt(but.dataset.id, 10) ? but.className + ' backgroComboColor' : but.className;
            });
            this.questions[event.target.name - 1].Compliance__c = 'Compliance';
        } else {

            let index = parseInt(event);
            let allD = this.template.querySelectorAll('tbody');
            allD[index].rows[1].className = 'displayNone';
            allD[index].rows[0].cells[1].childNodes.forEach((but) => {
                but.className = but.className.includes('backgroComboColor') == true ? but.className.replace(' backgroComboColor', '') : but.className;
                but.className = 1 === parseInt(but.dataset.id, 10) ? but.className + ' backgroComboColor' : but.className;
            });
        }
    }

    selectedNonApplicable(event) {
        if (event.target != undefined && event.target.name != undefined && event.target.name != null) {
            let allD = this.template.querySelectorAll('tbody');
            allD[event.target.name - 1].rows[1].className = 'displayNone';
            allD[event.target.name - 1].rows[0].cells[1].childNodes.forEach((but) => {
                but.className = but.className.includes('backgroComboColor') == true ? but.className.replace(' backgroComboColor', '') : but.className;
                but.className = 3 === parseInt(but.dataset.id, 10) ? but.className + ' backgroComboColor' : but.className;
            });
            this.questions[event.target.name - 1].Compliance__c = 'Not Applicable';
        } else {
            let index = parseInt(event);
            let allD = this.template.querySelectorAll('tbody');
            allD[index].rows[1].className = 'displayNone';
            allD[index].rows[0].cells[1].childNodes.forEach((but) => {
                but.className = but.className.includes('backgroComboColor') == true ? but.className.replace(' backgroComboColor', '') : but.className;
                but.className = 3 === parseInt(but.dataset.id, 10) ? but.className + ' backgroComboColor' : but.className;
            });
        }
    }

    selectedPending(event) {
        if (event.target != undefined && event.target.name != undefined && event.target.name != null) {
            let allD = this.template.querySelectorAll('tbody');
            allD[event.target.name - 1].rows[1].className = 'displayNone';
            allD[event.target.name - 1].rows[0].cells[1].childNodes.forEach((but) => {
                but.className = but.className.includes('backgroComboColor') == true ? but.className.replace(' backgroComboColor', '') : but.className;
                but.className = 4 === parseInt(but.dataset.id, 10) ? but.className + ' backgroComboColor' : but.className;
            });
            this.questions[event.target.name - 1].Compliance__c = 'Pending';
        } else {
            let index = parseInt(event);
            let allD = this.template.querySelectorAll('tbody');
            allD[index].rows[1].className = 'displayNone';
            allD[index].rows[0].cells[1].childNodes.forEach((but) => {
                but.className = but.className.includes('backgroComboColor') == true ? but.className.replace(' backgroComboColor', '') : but.className;
                but.className = 4 === parseInt(but.dataset.id, 10) ? but.className + ' backgroComboColor' : but.className;
            });
        }
    }
    /* On-Click Button Change Functionlity on New Record Creation End */

    /* Non - Comliance Row Functions Start */

    get frequencyDefis() {
        return [{
            label: 'Initial',
            value: 'Initial'
        },
        {
            label: 'Repeat',
            value: 'Repeat'
        },
        {
            label: 'Chronic',
            value: 'Chronic'
        },
        ];
    }

    get impactDefis() {
        return [{
            label: 'Minor',
            value: 'Minor'
        },
        {
            label: 'Major',
            value: 'Major'
        },
        {
            label: 'Serious',
            value: 'Serious'
        }
        ]
    }

    get scopeDefis() {
        return [{
            label: 'Isolated',
            value: 'Isolated'
        },
        {
            label: 'Narrow',
            value: 'Narrow'
        },
        {
            label: 'Moderate',
            value: 'Moderate'
        },
        {
            label: 'Broad',
            value: 'Broad'
        }
        ]
    }

    /* Non - Comliance Row Functions End */

    /* New Comar Add POP-UP Function Start */

    addRegulationPopup() {
        this.openModel = true;
    }

    closeModal() {
        this.openModel = false;
        this.createComar = {};
        this.openmodelDel = false;
    }

    comarOnChange(event) {
        if (event.target.name == 'regulations') {
            this.createComar.regulations = event.detail.value;
        } else if (event.target.name == 'question') {
            this.createComar.question = event.detail.value;
        }
    }

    createNewComar() {
        const allValid = [
            ...this.template.querySelectorAll('section lightning-input')
        ]
            .reduce((validSoFar, inputFields) => {
                inputFields.reportValidity();
                return validSoFar && inputFields.checkValidity();
            }, true);
        this.createComar.type = this.type;
        if (allValid) {
            let fields = {};

            fields[REGULATIONS_FIELD.fieldApiName] = this.createComar.regulations;
            fields[QUESTION_FIELD.fieldApiName] = this.createComar.question;
            fields[TYPE_FIELD.fieldApiName] = this.createComar.type;
            fields[RECORDTYPEID_FIELD.fieldApiName] = this.recordTypeId;
            const recordInput = {
                apiName: REFVALUE_OBJECT.objectApiName,
                fields
            };

            createRecord(recordInput)
                .then(result => {
                    this.openModel = false;

                    this.createComar = {};

                    fireEvent(this.pageRef, "RefreshActivityPage", true);
                    this.dispatchEvent(utils.toastMessage(constPopupVariables.comarSave, "success"));
                    return refreshApex(this.questionsRegulations);
                    //this.fetchQuestions();
                }).catch(errors => {
                    let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                    return this.dispatchEvent(utils.handleError(errors, config));
                })
        }
    }
    /* New Comar Add POP-UP Function End */

    //Edit Record Details Function Datatable Start
    @wire(editRecordDet, {
        questionId: '$editRecordId'
    })
    wiredEditRecords(result) {
        this.wiredEditReg = result;
        if (result.data != undefined && result.data.length > 0) {
            this.recordNewModel = true;
            let currentData = [];

            for (let i = 0; i < result.data.length; i++) {
                currentData.push({
                    rowId: i + 1,
                    Id: result.data[i].Id,
                    RegulationId: result.data[i].Comar__c,
                    Regulations__c: result.data[i].Comar__r != undefined ? result.data[i].Comar__r.Regulations__c : '',
                    Question__c: result.data[i].Comar__r != undefined ? result.data[i].Comar__r.Question__c : '',
                    Compliance__c: result.data[i].Compliance__c,
                    Date__c: result.data[i].Date__c,
                    Findings__c: result.data[i].Findings__c,
                    FrequencyofDeficiency__c: result.data[i].FrequencyofDeficiency__c,
                    ImpactofDeficiency__c: result.data[i].ImpactofDeficiency__c,
                    Scope_of_Deficiency__c: result.data[i].Scope_of_Deficiency__c,
                    ProviderRecordQuestion__c: result.data[i].ProviderRecordQuestion__c
                });
            }

            this.questions = currentData;
            this.editedQuestions = [...this.questions];
            setTimeout(() => {
                this.rowBtnChangeFn();
            }, 0);

        }
    }

    handleRowAction(event) {
        if (event.detail.action.name === 'dontRedirect') return;
        const action = event.detail.action;
        const row = event.detail.row;
        switch (action.name) {
            case 'editRecord':
                if (this.disableFields == true)
                    return this.dispatchEvent(utils.toastMessage(constPopupVariables.UNEDIT, "warning"));
                let doAny = 'do';
                if (this.editRecordId != null && row.id == this.editRecordId)
                    doAny = 'dont';
                this.editRecordId = row.id;
                let data = this.datas.filter(q => q.id == row.id);

                this.btnEnable = data[0].Status__c == 'Completed' ? false : true;
                this.updateMode = true;
                this.recordNewModel = true;
                if (doAny == 'dont') {
                    this.questions = this.editedQuestions;
                    setTimeout(() => {
                        this.rowBtnChangeFn();
                    }, 0);
                    break;
                } else {
                    refreshApex(this.wiredEditReg);
                    break;
                }
            case 'deleteRecordRow':
                if (this.disableFields == true)
                    return this.dispatchEvent(utils.toastMessage(constPopupVariables.UNDELETE, "warning"));
                this.deleteRowId = row.id;
                this.openmodelDel = true;
                break;
            case 'viewRecord':
                this.disableFields = true;
                let doAnyA = 'do';
                if (this.editRecordId != null && row.id == this.editRecordId)
                    doAnyA = 'dont';
                this.editRecordId = row.id;
                this.btnEnable = false;
                this.updateMode = true;
                this.recordNewModel = true;
                if (doAnyA == 'dont') {
                    this.questions = this.editedQuestions;
                    setTimeout(() => {
                        this.rowBtnChangeFn();
                    }, 0);
                    break;
                } else {
                    refreshApex(this.wiredEditReg);
                    break;
                }
        }
    }

    btnChangeFn(id, compliance) {
        switch (compliance) {
            case 'Compliance':
                this.selectedCompliance(id);
                break;
            case 'Non-Compliance':
                this.openNewRow(id);
                break;
            case 'Not Applicable':
                this.selectedNonApplicable(id);
                break;
            case 'Pending':
                this.selectedPending(id);
                break;
        }
    }

    rowBtnChangeFn() {
        for (let ques in this.questions) {
            this.btnChangeFn(ques, this.questions[ques].Compliance__c);
        }
    }

    deleteFrSure() {
        this.deleteLoader = true;
        deleteRecord(this.deleteRowId)
            .then(() => {
                this.closeModal();
                this.dispatchEvent(utils.toastMessage(constPopupVariables.DELETESUCCESS, "success"));
                this.deleteLoader = false;
                return refreshApex(this.wiredRecordDetails);

            }).catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DELETE_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
        this.closeModal();

        refreshApex(this.wiredRecordDetails);
    }

    //Edit Record Details Function Datatable End

    //For Pagination Child Bind
    hanldeProgressValueChange(event) {
        this.page = event.detail;

        this.pageData();
    }

    /* Staff List Load In Staff Record Page Function Start */
    @wire(getStaffFromApplicationId, {
        contactfromapp: '$applicationId'
    })
    wiredStaffDetails(result) {
        if (result.data) {
            let tmperrarray = [];
            result.data.forEach((staff) => {
                let obj = {};
                if (staff.Staff__r !== undefined && staff.Staff__r.FirstName__c != undefined) {
                    obj.label = staff.Staff__r.FirstName__c;
                    obj.value = staff.Staff__r.Id;
                    tmperrarray.push(obj);
                }
            });
            this.staffLists = tmperrarray;
        }
    }
    /* Staff List Load In Staff Record Page Function End */

    /* Create New Record Function Start */
    handleDate(event) {
        this.questions[event.target.name - 1].Date__c = event.detail.value;
    }
    handleFreq(event) {
        this.questions[event.target.name - 1].FrequencyofDeficiency__c = event.detail.value;
    }
    handleImpact(event) {
        this.questions[event.target.name - 1].ImpactofDeficiency__c = event.detail.value;
    }
    handleScope(event) {
        this.questions[event.target.name - 1].Scope_of_Deficiency__c = event.detail.value;
    }
    handlefind(event) {
        this.questions[event.target.name - 1].Findings__c = event.detail.value;
    }

    createNewRecord(event) {
        this.Spinner = true;
        this.disableFields = true;
        let nc = this.questions.filter(q => q.Compliance__c == 'Non-Compliance');
        let pending = this.questions.filter(q => q.Compliance__c == 'Pending');
        let status = event.target.name == 'draft' ? 'Draft' : 'Completed';
        if (this.updateMode == false) {
            let fields = {};
            if (this.type == 'Staff Record')
                fields[STAFFQUES_FIELD.fieldApiName] = this.staffId;
            fields[MONITERIG_FIELD.fieldApiName] = this.moniteringId;
            fields[STATUS_FIELD.fieldApiName] = status;
            fields[SITE_FIELD.fieldApiName] = this.siteId;
            fields[RECTYPEQUES_FIELD.fieldApiName] = this.recordTypeId;
            fields[DATEQUES_FIELD.fieldApiName] = new Date();
            fields[NCQUES_FIELD.fieldApiName] = nc.length > 0 ? 'Yes' : 'No';
            fields[PENDGQUES_FIELD.fieldApiName] = pending.length > 0 ? 'Yes' : 'No';

            const recordInput = {
                apiName: PROVIDERRECQUES_OBJECT.objectApiName,
                fields
            };
            createRecord(recordInput)
                .then(result => {
                    if (result.id) {
                        let datasTo = [];
                        this.questions.forEach(data => {
                            let fieldst = {};

                            fieldst[DATE_FIELD.fieldApiName] = data.Date__c,
                                fieldst[COMPLIANCE_FIELD.fieldApiName] = data.Compliance__c,
                                fieldst[FREQUENCYOFDEF_FIELD.fieldApiName] = data.FrequencyofDeficiency__c,
                                fieldst[IMPACTOFDEF_FIELD.fieldApiName] = data.ImpactofDeficiency__c,
                                fieldst[COMAR_FIELD.fieldApiName] = data.RegulationId,
                                fieldst[FINDINGS_FIELD.fieldApiName] = data.Findings__c,
                                fieldst[SCOPEOFDEF_FIELD.fieldApiName] = data.Scope_of_Deficiency__c,
                                fieldst[PROVIDERRECQUES_FIELD.fieldApiName] = result.id
                            datasTo.push(fieldst);
                        });
                        let datasString = JSON.stringify(datasTo);
                        bulkAddRecordAns({
                            datas: datasString
                        })
                            .then(result => {
                                this.Spinner = false;
                                fireEvent(this.pageRef, "RefreshActivityPage", true);
                                this.dispatchEvent(utils.toastMessage(this.popUpForSuccess, "success"));
                                this.closeNewRecord();
                                this.disableFields = false;
                                return refreshApex(this.wiredRecordDetails);
                            }).catch(errors => {
                                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                                return this.dispatchEvent(utils.handleError(errors, config));
                            });
                    }
                }).catch(errors => {
                    let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                    return this.dispatchEvent(utils.handleError(errors, config));
                });
        } else {
            let fields = {};
            fields[IDQUES_FIELD.fieldApiName] = this.editRecordId;
            fields[STATUS_FIELD.fieldApiName] = status;
            fields[DATEQUES_FIELD.fieldApiName] = new Date();
            fields[NCQUES_FIELD.fieldApiName] = nc.length > 0 ? 'Yes' : 'No';
            fields[PENDGQUES_FIELD.fieldApiName] = pending.length > 0 ? 'Yes' : 'No';

            const recordInput = {
                fields
            };
            updateRecord(recordInput)
                .then(result => {
                    let datasTo = [];
                    this.questions.forEach(dat => {
                        let fieldst = {};

                        fieldst[ID_FIELD.fieldApiName] = dat.Id,
                            fieldst[DATE_FIELD.fieldApiName] = dat.Date__c,
                            fieldst[COMPLIANCE_FIELD.fieldApiName] = dat.Compliance__c,
                            fieldst[FREQUENCYOFDEF_FIELD.fieldApiName] = dat.FrequencyofDeficiency__c,
                            fieldst[IMPACTOFDEF_FIELD.fieldApiName] = dat.ImpactofDeficiency__c,
                            fieldst[COMAR_FIELD.fieldApiName] = dat.RegulationId,
                            fieldst[FINDINGS_FIELD.fieldApiName] = dat.Findings__c,
                            fieldst[SCOPEOFDEF_FIELD.fieldApiName] = dat.Scope_of_Deficiency__c,
                            fieldst[PROVIDERRECQUES_FIELD.fieldApiName] = dat.ProviderRecordQuestion__c
                        datasTo.push(fieldst);
                    });
                    let datasString = JSON.stringify(datasTo);
                    bulkUpdateRecordAns({
                        datas: datasString
                    })
                        .then(result => {
                            fireEvent(this.pageRef, "RefreshActivityPage", true);
                            this.dispatchEvent(utils.toastMessage("Records Has Been Updated Successfully!..", "success"));
                            this.closeNewRecord();
                            this.closeModal();
                            this.questions = [];
                            this.disableFields = false;
                            return refreshApex(this.wiredRecordDetails);
                        }).catch(errors => {
                            let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                            return this.dispatchEvent(utils.handleError(errors, config));

                        });
                }).catch(errors => {
                    let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                    return this.dispatchEvent(utils.handleError(errors, config));

                });
        }
        /* Create New Record Function End */
    }

    /*User profile based button hide */
    get userProfileName() {
        return sharedData.getUserProfileName();
    }
}