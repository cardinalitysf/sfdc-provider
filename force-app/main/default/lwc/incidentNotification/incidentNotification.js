/**
 * @Author        : Naveen S
 * @CreatedOn     : Aug 1 ,2020
 * @Purpose       : Incident Notifications Added.
 * @updatedBy     
 * @updatedOn    
 **/
import { LightningElement, track, wire, api } from 'lwc';
import { refreshApex } from "@salesforce/apex";
import { createRecord, updateRecord, deleteRecord } from "lightning/uiRecordApi";
// Utils
import { utils } from 'c/utils';
import * as sharedData from "c/sharedData";
import { CJAMS_CONSTANTS, USER_PROFILE_NAME } from 'c/constants';

import getQuestionsByType from '@salesforce/apex/IncidentNotification.getQuestionsByType';
import getProviderAnswer from '@salesforce/apex/IncidentNotification.getProviderAnswer';
import getRecordQuestionId from '@salesforce/apex/IncidentNotification.getRecordQuestionId';
import getRecordType from '@salesforce/apex/IncidentNotification.getRecordType';
import { getObjectInfo } from "lightning/uiObjectInfoApi";
import recordAnswer_OBJECT from "@salesforce/schema/ProviderRecordAnswer__c";
import { getPicklistValuesByRecordType } from "lightning/uiObjectInfoApi";

import PROVIDERRECQUES_OBJECT from '@salesforce/schema/ProviderRecordQuestion__c';
import INCIDENT_FIELD from '@salesforce/schema/ProviderRecordQuestion__c.Incident__c';
import STATUS_FIELD from '@salesforce/schema/ProviderRecordQuestion__c.Status__c';
import RECTYPEQUES_FIELD from '@salesforce/schema/ProviderRecordQuestion__c.RecordTypeId';
import bulkAddRecordAns from "@salesforce/apex/IncidentNotification.bulkAddRecordAns";
import bulkUpdateRecordAns from "@salesforce/apex/IncidentNotification.bulkUpdateRecordAns";
import IDQUES_FIELD from '@salesforce/schema/ProviderRecordQuestion__c.Id';


export default class IncidentNotification extends LightningElement {
    @track hastypeselected = 'Notification';
    @track type = 'Notification';
    @track questions;
    @track methodValues;
    @track isBiologicalRecevied = true;
    @track isBiological = false;
    @track isOtherBiological = true;
    @track isStatment = false;
    @track addBilogicalparents = false;
    @track recordTypeId;
    @track getProviderQuesIdfromBegining;
    @track allNotificationAnswers;
    @track providerQuestionId;
    @track isAnswerUpdate = false;
    @track isExplianVisble = false;
    @track sentNotify = true;
    @track sentnotifystatus;
    @track sendEnable = false;
    @track saveEnable = false;
    @track comarUniqueId;
    @track biologicalLastarray = {};
    @track newBiologicalName = null;
    @track newBiologicalDate = null;
    @track newBiologicalRecevied = null;
    @track newBiologicalPhone = null;
    @track newBiologicalEmail = null;
    @track updatenewBiologicalAnswerId;
    @track UserProfileDisable;
    @track Spinner = false;


    QuestionBiologicalParents = {
        "rowId": 8,
        "Id": null,
        "Comar__c": null,
        "Name__c": null,
        "Date__c": utils.formatDateYYYYMMDD(new Date()),
        "Received__c": null,
        "Phone__c": null,
        "Email__c": null,
        "ProviderRecordQuestion__c": null,
        "RefKey__c": "Biological Parents",
        "Findings__c": "BiologicalParent"
    };

    get IncidentId() {
        return sharedData.getCaseId();
        //return '5000p0000035Ok1AAE';
    }

    get userProfileName() {
        return sharedData.getUserProfileName();
        //return 'Caseworker';
    }
    get incidentStatus() {
        return sharedData.getIncidentStatus();
    }

    get options() {
        return [
            {
                label: "Yes",
                value: "Yes"
            },
            {
                label: "No",
                value: "No"
            }
        ];
    }

    //Declare Answer object into one variable
    @wire(getObjectInfo, {
        objectApiName: recordAnswer_OBJECT
    })
    objectInfo;

    //Function used to get all picklist values from Answer object
    @wire(getPicklistValuesByRecordType, {
        objectApiName: recordAnswer_OBJECT,
        recordTypeId: "$objectInfo.data.defaultRecordTypeId"
    })
    getAllPicklistValues({ error, data }) {
        if (data) {
            // methodDropDown
            let methodOptions = [];
            data.picklistFieldValues.Method__c.values.forEach((key) => {
                methodOptions.push({
                    label: key.label,
                    value: key.value
                });
            });
            this.methodValues = methodOptions;
        } else if (error) {
            this.dispatchEvent(
                utils.toastMessage("Error in fetching record", "error")
            );
            this.error = JSON.stringify(error);
        }
    }

    connectedCallback() {
        if (this.userProfileName === USER_PROFILE_NAME.USER_CASE_WRKR || (this.incidentStatus !== undefined && (this.incidentStatus !== 'Pending'))) {
            this.UserProfileDisable = true;
            this.saveEnable = true;
            this.sendEnable = true;

        }
        this.getReferrenceValueQuestion();
        this.getRecordTypesId();
        setTimeout(() => {
            this.getProviderQuestionID();
        }, 9000);
    }

    getReferrenceValueQuestion() {
        getQuestionsByType({
            type: this.hastypeselected
        }).then((result) => {
            if (result != null && result != '') {
                let currentData = [];
                let id = 0;
                this.questions = []
                result.forEach((row) => {
                    let rowData = {};
                    rowData.rowId = id;
                    rowData.Question__c = row.Question__c;
                    rowData.Comar__c = row.Id;
                    rowData.Name__c = null;
                    rowData.Date__c = utils.formatDateYYYYMMDD(new Date());
                    rowData.Method__c = null;
                    rowData.Received__c = null;
                    rowData.Phone__c = null;
                    rowData.StaffMembercompletingReport__c = null;
                    rowData.Comments__c = null;
                    rowData.Email__c = null;
                    rowData.Findings__c = null;
                    if (row.QuestionOrder__c == "1") { rowData.RefKey__c = row.RefKey__c; }
                    if (row.RefKey__c == "Biological Parents") {
                        rowData.isOtherBiological = false;
                        rowData.isStatment = false;
                        rowData.isBiological = true;
                    }
                    else if (row.RefKey__c == "Statement") {
                        rowData.isOtherBiological = false;
                        rowData.isStatment = true;
                        rowData.isBiological = false;
                    }
                    else {
                        rowData.isOtherBiological = true;
                        rowData.isStatment = false;;
                        rowData.isBiological = false;
                    }
                    currentData.push(rowData);
                    id++;
                });
                this.questions = currentData;
            }
        })
            .catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
    }
    getRecordTypesId() {
        getRecordType({ name: this.type }).then((result) => {
            if (result.length > 0) {
                this.recordTypeId = result;
            }
        })
            .catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
    }
    getProviderQuestionID() {
        getRecordQuestionId({ incidentId: this.IncidentId, recordTypeId: this.recordTypeId }).then((result) => {
            var arrayRecordQuestion = [];
            var arraynotifystatus = [];
            result.forEach((item) => {
                arrayRecordQuestion.push(item.Id);
            });
            this.getProviderQuesIdfromBegining = arrayRecordQuestion.toString();
            result.forEach((item) => {
                arraynotifystatus.push(item.Status__c);
            });
            this.sentnotifystatus = arraynotifystatus.toString();
            if (this.sentnotifystatus == 'Completed') {
                this.sendEnable = true;
                this.sentNotify = false;
                this.saveEnable = false;
            }
            else {
                this.saveEnable = true;
                this.sentNotify = true;
                this.sendEnable = false;
            }
            if (result.length > 0) {
                this.getProviderAnswerDetails();
            }
            else {
                this.getReferrenceValueQuestion();
                this.isAnswerUpdate = false;
            }
        })
            .catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
    }

    getProviderAnswerDetails() {
        getProviderAnswer({ getAnswers: this.getProviderQuesIdfromBegining }).then((result) => {
            this.allNotificationAnswers = result;
            if (result.length >= 8) {
                this.biologicalLastarray = result[result.length - 1]
                this.newBiologicalName = result[result.length - 1].Name__c != undefined ? result[result.length - 1].Name__c : '';
                this.newBiologicalDate = result[result.length - 1].Date__c != undefined ? result[result.length - 1].Date__c : '';
                this.newBiologicalRecevied = result[result.length - 1].Received__c != undefined ? result[result.length - 1].Received__c : '';
                this.newBiologicalPhone = result[result.length - 1].Phone__c != undefined ? result[result.length - 1].Phone__c : '';
                this.newBiologicalEmail = result[result.length - 1].Email__c != undefined ? result[result.length - 1].Email__c : '';
                this.updatenewBiologicalAnswerId = result[result.length - 1].Id;
                this.allNotificationAnswers.pop();

                if (this.newBiologicalName != '' || this.newBiologicalRecevied != '' || this.newBiologicalPhone != '' || this.newBiologicalEmail != '') {
                    this.addBilogicalparents = true;
                }
            }
            else {
                this.addBilogicalparents = false;
            }
            if (this.allNotificationAnswers.length > 0) {
                this.allNotificationAnswers.forEach((item, index) => {
                    this.questions[index].Name__c = item.Name__c != undefined ? item.Name__c : '';
                    this.questions[index].Date__c = item.Date__c ? item.Date__c : '';
                    this.questions[index].Comments__c = item.Comments__c ? item.Comments__c : '';
                    this.questions[index].Id = item.Id ? item.Id : '';
                    this.questions[index].Received__c = item.Received__c ? item.Received__c : '';
                    this.questions[index].Method__c = item.Method__c ? item.Method__c : '';
                    this.questions[index].QuestionOrder__c = item.Comar__r.QuestionOrder__c ? item.Comar__r.QuestionOrder__c : '';
                    this.questions[index].RefKey__c = item.Comar__r.RefKey__c ? item.Comar__r.RefKey__c : '';
                    this.questions[index].Question__c = item.Comar__r.Question__c ? item.Comar__r.Question__c : '';
                    this.questions[index].Phone__c = item.Phone__c ? item.Phone__c : '';
                    this.questions[index].Email__c = item.Email__c ? item.Email__c : '';
                    this.questions[index].StaffMembercompletingReport__c = item.StaffMembercompletingReport__c ? item.StaffMembercompletingReport__c : '';
                    this.questions[index].ProviderRecordQuestion__c = item.ProviderRecordQuestion__c ? item.ProviderRecordQuestion__c : '';
                    this.providerQuestionId = item.ProviderRecordQuestion__c;
                    this.comarUniqueId = item.Comar__c;
                    this.sendEnable = true;
                    this.saveEnable = false;
                    if (item.Comar__r.QuestionOrder__c == "1") { this.questions[index].RefKey__c = item.Comar__r.RefKey__c; }
                    else {
                        this.questions[index].RefKey__c = null;
                    }
                    if (item.Comar__r.RefKey__c == "Biological Parents") {
                        item.isOtherBiological = false;
                        item.isStatment = false;
                        item.isBiological = true;
                    }

                    else if (item.Comar__r.RefKey__c == "Statement") {
                        item.isOtherBiological = false;
                        item.isStatment = true;
                        item.isBiological = false;
                        if (item.Received__c == 'Yes') {
                            this.isExplianVisble = true;
                        }
                        else {
                            this.isExplianVisble = false;
                        }
                    }
                    else {
                        item.isOtherBiological = true;
                        item.isStatment = false;;
                        item.isBiological = false;
                    }
                });
                this.isAnswerUpdate = true;
                refreshApex(this.questions);
            } else {
                this.getReferrenceValueQuestion();
                this.isAnswerUpdate = false;
            }
        })
            .catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
    }
    addBiologicalParents(event) {
        this.addBilogicalparents = true;
        this.newBiologicalDate = utils.formatDateYYYYMMDD(new Date());
    }

    addBiologicalDelete(event) {
        this.addBilogicalparents = false;
    }

    namechange(event) {
        if (event.target != undefined && event.target.name != undefined && event.target.name != null) {
            this.questions[event.target.name].Name__c = event.target.value;
        }
    }
    datetimeChange(event) {
        if (event.target != undefined && event.target.name != undefined && event.target.name != null) {
            this.questions[event.target.name].Date__c = event.target.value;
        }
    }
    otherBiologicalMethodChange(event) {
        if (event.target != undefined && event.target.name != undefined && event.target.name != null) {
            this.questions[event.target.name].Method__c = event.target.value;
        }
    }
    otherComments(event) {
        if (event.target != undefined && event.target.name != undefined && event.target.name != null) {
            this.questions[event.target.name].Comments__c = event.target.value;
        }
    }

    receviedChange(event) {

        if (event.target != undefined && event.target.name != undefined && event.target.name != null) {
            this.questions[event.target.name].Received__c = event.target.value;
        }
    }
    statementReceviedChange(event) {

        if (event.target != undefined && event.target.name != undefined && event.target.name != null) {
            this.questions[event.target.name].Received__c = event.target.value;
            if (event.target.value == 'Yes') {
                this.isExplianVisble = true;
            }
            else {
                this.isExplianVisble = false;
            }
        }
    }
    statementNameChange(event) {
        if (event.target != undefined && event.target.name != undefined && event.target.name != null) {
            this.questions[event.target.name].Name__c = event.target.value;
        }
    }
    dateTimestatementChange(event) {
        if (event.target != undefined && event.target.name != undefined && event.target.name != null) {
            this.questions[event.target.name].Date__c = event.target.value;
        }
    }
    receviedOtherBiologicalChange(event) {
        if (event.target != undefined && event.target.name != undefined && event.target.name != null) {
            this.questions[event.target.name].Received__c = event.target.value;
        }
    }
    phoneNumberChange(event) {
        if (event.target != undefined && event.target.name != undefined && event.target.name != null) {
            event.target.value = event.target.value.replace(/(\D+)/g, "");
            this.questions[event.target.name].Phone__c = utils.formattedPhoneNumber(event.target.value);

        }
    }
    staffMemberChange(event) {
        if (event.target != undefined && event.target.name != undefined && event.target.name != null) {
            this.questions[event.target.name].StaffMembercompletingReport__c = event.target.value;
        }
    }
    onchangeEmail(event) {
        if (event.target != undefined && event.target.name != undefined && event.target.name != null) {
            this.questions[event.target.name].Email__c = event.target.value;
        }
    }

    saveRecord(event) {

        this.answerUpdate();
    }

    answerUpdate() {
        this.Spinner = true;
        this.QuestionBiologicalParents.ProviderRecordQuestion__c = this.providerQuestionId
        this.QuestionBiologicalParents.Id = this.updatenewBiologicalAnswerId;
        this.questions.push(this.QuestionBiologicalParents);
        let questionstosave = JSON.stringify(this.questions);
        bulkUpdateRecordAns({
            datas: questionstosave
        })
            .then(result => {
                this.getReferrenceValueQuestion();
                this.getProviderQuestionID();
                this.Spinner = false;
                this.dispatchEvent(utils.toastMessage("Records Has Been Updated Successfully!..", "success"));
            })
            .catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
    }

    clickSendNotifcation(event) {
        this.Spinner = true;
        let status = 'Completed';
        let fields = {};
        fields[INCIDENT_FIELD.fieldApiName] = this.IncidentId;
        fields[STATUS_FIELD.fieldApiName] = status;
        fields[RECTYPEQUES_FIELD.fieldApiName] = this.recordTypeId;
        const recordInput = {
            apiName: PROVIDERRECQUES_OBJECT.objectApiName,
            fields
        };
        createRecord(recordInput)
            .then(result => {
                this.questions.push(this.QuestionBiologicalParents);
                if (result.id) {

                    bulkAddRecordAns({
                        datas: JSON.stringify(this.questions),
                        questionId: result.id
                    })
                        .then(result => {
                            this.getReferrenceValueQuestion();
                            this.getProviderQuestionID();
                            this.Spinner = false;
                            this.dispatchEvent(utils.toastMessage("Incident Notification Sent Successfully!..", "success"));
                        })
                }
            })


    }
    namechangeBiological(event) {
        if (event.target.name == "AddBilogicalname") {
            this.QuestionBiologicalParents.Name__c = event.target.value;
        }
        if (event.target.name == "addBiologicalDate") {
            this.QuestionBiologicalParents.Date__c = event.target.value;
        }
        if (event.target.name == "addBiologicalRecevied") {
            this.QuestionBiologicalParents.Received__c = event.target.value;
        }
        if (event.target.name == "addBiologicalPhone") {
            event.target.value = event.target.value.replace(/(\D+)/g, "");
            this.QuestionBiologicalParents.Phone__c = utils.formattedPhoneNumber(event.target.value);
            this.newBiologicalPhone = utils.formattedPhoneNumber(event.target.value);

        }
        if (event.target.name == "addBiologicalEmail") {
            this.QuestionBiologicalParents.Email__c = event.target.value;
        }
        this.QuestionBiologicalParents.Comar__c = this.comarUniqueId;
    }
}