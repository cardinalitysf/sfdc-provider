import { LightningElement, wire , track } from 'lwc';
import Contact_OBJECT from "@salesforce/schema/Contact";
import Actor_OBJECT from "@salesforce/schema/Actor__c";
import { getPicklistValuesByRecordType } from "lightning/uiObjectInfoApi";
import { getObjectInfo } from "lightning/uiObjectInfoApi";
import getRecordType from "@salesforce/apex/IncidentPersonKidChildInfo.getRecordType";
import EditIncidentPersonChildDetails from "@salesforce/apex/IncidentPersonKidChildInfo.EditIncidentPersonChildDetails";
import getStaffMembers from '@salesforce/apex/IncidentPersonKidChildInfo.getStaffMembers';

import InsertUpdateIncident from "@salesforce/apex/IncidentPersonKidChildInfo.InsertUpdateIncident";
import { utils } from "c/utils";
import * as sharedData from "c/sharedData";
import { CJAMS_CONSTANTS } from 'c/constants';

//Google Api Apex
import getFullDetails from "@salesforce/apex/GetStreetAddressFromGoogleAPI.getPlaceDetails";
import getAPIStreetAddress from "@salesforce/apex/GetStreetAddressFromGoogleAPI.getSuggestions";
import getAllStates from "@salesforce/apex/ComplaintsReferral.getStateDetails";

export default class IncidentPersonKidChildInfo extends LightningElement {

    //picklist options
    placementoptions;
    roleIncidentOptions;
    PhysicalRestrainedOptions;
    DurationPhysicalRestrainedOptions;
    DurationMechanicalRestraintOptions;
    StaffInvolvedOptions;
    DurationofSeclusion;
    DeEscalationOptions;
    InjurySustainedOptions;
    injuirylevel;
    InjuryResultingfromRestraintOptions;
    SeclusionOptions;
    SeenByMedicalOptions;
    DurationofSeclusionOptions;
    PreventionOptions;
    isChecked = false;
    RecordTypeId;
    title = 'Add person/kid/child';
    SaveUpdateButton = 'SAVE';
    SaveUpdateShowHide = true;

    recordType = 'Person/Kid';
    isDisabled = false;
    currentDate = utils.formatDateYYYYMMDD(new Date());

    //objs
    addPersonkidchildContact = {};
    incidentInvloved = {};
    addressOfReporter = {};

    //googleApi Tracks
        @track modes = [];


    connectedCallback() {
        if (this.recordType != undefined) {
            getRecordType({
                name: this.recordType
            })
                .then((result) => {
                    this.RecordTypeId = result;
                }).catch((errors) => {
                    return this.dispatchEvent(utils.handleError(errors));
                });
        }

        this.loadEditRowList();
        this.getStaffMembersList();
    }
    get getActionPersonKid() {
        return sharedData.getActionPersonKid();
    }

    get personKidRowId() {
        return sharedData.getPersonKidIncidentId();
    }
    get personKidContactId() {
        return sharedData.getContactId();
    }
    get personKidAddressId() {
        return sharedData.getAddressId();
    }
    get applicationId() {
        return sharedData.getApplicationId();
    }

    //Incident List row Loading According to edit and view.
    loadEditRowList() {
        if (this.personKidRowId != undefined) {
            if (this.getActionPersonKid == 'View') {
                this.title = 'View person/kid/child'
                this.isDisabled = true;
                this.SaveUpdateShowHide = false;
            }
            else {
                this.isDisabled = false;
                this.title = 'Edit person/kid/child'
                this.SaveUpdateButton = 'UPDATE';
                this.SaveUpdateShowHide = true;
            }
            EditIncidentPersonChildDetails({
                ID: this.personKidRowId
            })
                .then((data) => {
                    if (data) {
                        if(data[0].Contact__r != undefined && data[0].Address__r != undefined){
                        this.addPersonkidchildContact.FirstName__c = data[0].Contact__r.FirstName__c != undefined ? data[0].Contact__r.FirstName__c : '';
                        this.addPersonkidchildContact.LastName__c = data[0].Contact__r.LastName__c != undefined ? data[0].Contact__r.LastName__c : '';
                        this.addPersonkidchildContact.DOB__c = data[0].Contact__r.DOB__c != undefined ? data[0].Contact__r.DOB__c : '';
                        this.addPersonkidchildContact.IdentifierNumber__c = data[0].Contact__r.IdentifierNumber__c != undefined ? data[0].Contact__r.IdentifierNumber__c : '';
                        this.addPersonkidchildContact.AdmittingCharge__c = data[0].Contact__r.AdmittingCharge__c != undefined ? data[0].Contact__r.AdmittingCharge__c : '';
                        this.addPersonkidchildContact.PlacingAgency__c = data[0].Contact__r.PlacingAgency__c != undefined ? data[0].Contact__r.PlacingAgency__c : '';
                        this.incidentInvloved.RoleinIncident__c = data[0].RoleinIncident__c != undefined ? data[0].RoleinIncident__c : '';
                        this.incidentInvloved.PhysicalRestrainedType__c = data[0].PhysicalRestrainedType__c != undefined ? data[0].PhysicalRestrainedType__c : '';
                        this.isChecked = data[0].Assign__c != undefined ? data[0].Assign__c : '';
                        this.incidentInvloved.MechanicalRestrainedType__c = data[0].MechanicalRestrainedType__c != undefined ? data[0].MechanicalRestrainedType__c : '';
                        this.incidentInvloved.DurationofPhysicalRestraint__c = data[0].DurationofPhysicalRestraint__c != undefined ? data[0].DurationofPhysicalRestraint__c : '';
                        this.incidentInvloved.DurationofMechanicalRestraint__c = data[0].DurationofMechanicalRestraint__c != undefined ? data[0].DurationofMechanicalRestraint__c : '';
                        this.incidentInvloved.StaffInvolved__c = data[0].StaffInvolved__c != undefined ? data[0].StaffInvolved__c : '';
                        this.incidentInvloved.DeescalationEffortMade__c = data[0].DeescalationEffortMade__c != undefined ? data[0].DeescalationEffortMade__c : '';
                        this.incidentInvloved.InjurySustained__c = data[0].InjurySustained__c != undefined ? data[0].InjurySustained__c : '';
                        this.incidentInvloved.InjurySeverityLevel__c = data[0].InjurySeverityLevel__c != undefined ? data[0].InjurySeverityLevel__c : '';
                        this.incidentInvloved.InjuryResultingfromRestraint__c = data[0].InjuryResultingfromRestraint__c != undefined ? data[0].InjuryResultingfromRestraint__c : '';
                        this.incidentInvloved.SeenByMedical__c = data[0].SeenByMedical__c != undefined ? data[0].SeenByMedical__c : '';
                        this.incidentInvloved.Seclusion__c = data[0].Seclusion__c != undefined ? data[0].Seclusion__c : '';
                        this.incidentInvloved.DurationofSeclusion__c = data[0].DurationofSeclusion__c != undefined ? data[0].DurationofSeclusion__c : '';
                        this.modes = data[0].PhysicalRestraintReason__c != undefined ? data[0].PhysicalRestraintReason__c : '';
                        this.reason.push(data[0].PhysicalRestraintReason__c);
                        this.addressOfReporter.AddressLine1__c = data[0].Address__r.AddressLine1__c != undefined ? data[0].Address__r.AddressLine1__c : '';
                        this.addressOfReporter.AddressLine2__c = data[0].Address__r.AddressLine2__c != undefined ? data[0].Address__r.AddressLine2__c : '';
                        this.addressOfReporter.State__c = data[0].Address__r.State__c != undefined ? data[0].Address__r.State__c : '';
                        this.addressOfReporter.City__c = data[0].Address__r.City__c != undefined ? data[0].Address__r.City__c : '';
                        this.addressOfReporter.County__c = data[0].Address__r.County__c != undefined ? data[0].Address__r.County__c : '';
                        this.addressOfReporter.ZipCode__c = data[0].Address__r.ZipCode__c != undefined ? data[0].Address__r.ZipCode__c : '';
                      } 
                      this.mechanicalCheckBoxSelectUnselectDuringPageLoad();  
                    }
                }).catch(errors => {
                    let config = {
                        friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
                        errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
                    }
                    return this.dispatchEvent(utils.handleError(errors, config));
                });
        }
    }

    getStaffMembersList() {
        getStaffMembers({
            applicationId: this.applicationId
        }).then((result) => {
            try {
                if (result.length > 0) {
                    let staff = result.map(item => {
                        let a = Object.assign({}, item);
                        a.label = (item.Staff__r !=undefined) ? (item.Staff__r.LastName !=undefined) ? item.Staff__r.LastName:'':'';
                        a.value = (item.Staff__r !=undefined) ? (item.Staff__r.LastName !=undefined) ? item.Staff__r.LastName:'':'';
                        return a;
                    });
                    this.StaffInvolvedOptions = staff;
                }
                this.Spinner = false;
            } catch (error) {
                throw error;
            }
        }).catch((errors) => {
            if (errors) {
                let error = JSON.parse(errors.body.message);
                const {
                    title,
                    message,
                    errorType
                } = error;
                this.dispatchEvent(utils.toastMessageWithTitle(title, message, errorType));
            } else {
                this.dispatchEvent(utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE));
            }
        });
    }

    //Declare contact object into one variable
    @wire(getObjectInfo, {
        objectApiName: Contact_OBJECT
    })
    objectInfo;

    //Function used to get all picklist values from Contact object
    @wire(getPicklistValuesByRecordType, {
        objectApiName: Contact_OBJECT,
        recordTypeId: "$objectInfo.data.defaultRecordTypeId"
    })
    getAllPicklistValues({
        error,
        data
    }) {
        if (data) {
            // placement Agency Field Picklist values
            let placementAgencyOptions = [];
            data.picklistFieldValues.PlacingAgency__c.values.forEach((key) => {
                placementAgencyOptions.push({
                    label: key.label,
                    value: key.value
                });
            });
            this.placementoptions = placementAgencyOptions;
        }
    }

    //Declare Actor object into one variable
    @wire(getObjectInfo, {
        objectApiName: Actor_OBJECT
    })
    objectInfo;

    //Function used to get all picklist values from Actor object

    @wire(getPicklistValuesByRecordType, {
        objectApiName: Actor_OBJECT,
        recordTypeId: "$objectInfo.data.defaultRecordTypeId"
    })
    getAllPicklistValues1({
        error,
        data
    }) {
        if (data) {
            // Role in Incident Field Picklist values
            let roleIncidentOptions = [];
            data.picklistFieldValues.RoleinIncident__c.values.forEach((key) => {
                roleIncidentOptions.push({
                    label: key.label,
                    value: key.value
                });
            });
            this.roleIncidentOptions = roleIncidentOptions;

            // Physical Restrained Type Picklist values
            this.PhysicalRestrainedOptions = data.picklistFieldValues.PhysicalRestrainedType__c.values.map((key) => {
                return {
                    label: key.label,
                    value: key.value
                };
            })

            // Duration of Physical Restrained Picklist values
            this.DurationPhysicalRestrainedOptions = data.picklistFieldValues.DurationofPhysicalRestraint__c.values.map((key) => {
                return {
                    label: key.label,
                    value: key.value
                };
            })

            // Mechanical Restraint Type Picklist values
            this.MechanicalRestraintOptions = data.picklistFieldValues.MechanicalRestrainedType__c.values.map((key) => {
                return {
                    label: key.label,
                    value: key.value
                };
            })

            // Duration of Mechanical Restraint Type Picklist values
            this.DurationMechanicalRestraintOptions = data.picklistFieldValues.DurationofMechanicalRestraint__c.values.map((key) => {
                return {
                    label: key.label,
                    value: key.value
                };
            })

            // De-escalation Effort Made Picklist values
            this.DeEscalationOptions = data.picklistFieldValues.DeescalationEffortMade__c.values.map((key) => {
                return {
                    label: key.label,
                    value: key.value
                };
            })

            this.InjurySustainedOptions = data.picklistFieldValues.InjurySustained__c.values.map((key) => {
                return {
                    label: key.label,
                    value: key.value
                };
            })

            this.injuirylevel = data.picklistFieldValues.InjurySeverityLevel__c.values.map((key) => {
                return {
                    label: key.label,
                    value: key.value
                };
            })

            this.InjuryResultingfromRestraintOptions = data.picklistFieldValues.InjuryResultingfromRestraint__c.values.map((key) => {
                return {
                    label: key.label,
                    value: key.value
                };
            })

            this.SeenByMedicalOptions = data.picklistFieldValues.SeenByMedical__c.values.map((key) => {
                return {
                    label: key.label,
                    value: key.value
                };
            })

            this.SeclusionOptions = data.picklistFieldValues.Seclusion__c.values.map((key) => {
                return {
                    label: key.label,
                    value: key.value
                };
            })

            this.DurationofSeclusionOptions = data.picklistFieldValues.DurationofSeclusion__c.values.map((key) => {
                return {
                    label: key.label,
                    value: key.value
                };
            })

            this.PreventionOptions = data.picklistFieldValues.PhysicalRestraintReason__c.values.map((key) => {
                return {
                    label: key.label,
                    value: key.value
                };
            })

        }
    }

    handlechange(event) {
        switch (event.target.name) {
            case 'firstName':
                this.addPersonkidchildContact.FirstName__c = event.target.value;
                break;
            case 'lastName':
                this.addPersonkidchildContact.LastName__c = event.target.value;
                break;
            case 'dob':
                this.addPersonkidchildContact.DOB__c = event.target.value;
                break;
            case 'identifyNumber':
                this.addPersonkidchildContact.IdentifierNumber__c = event.target.value;
                break;
            case 'admittingChange':
                this.addPersonkidchildContact.AdmittingCharge__c = event.target.value;
                break;
            case 'placingAgency':
                this.addPersonkidchildContact.PlacingAgency__c = event.target.value;
                break;
        }
    }

    backtoDashBoard(){
        const redirectdash = new CustomEvent("redirectbacktodashboard", {
            detail: { first: false }
        });
        this.dispatchEvent(redirectdash);
    }

    backtoListPage() {
        const redirect = new CustomEvent("redirectbacktolistofperson", {
            detail: { first: false }
        });
        this.dispatchEvent(redirect);
    }

 

    handlechangeincident(event) {
        switch (event.target.name) {
            case 'assign':
                this.isChecked = event.target.checked;
                break;
            case 'roleincident':
                this.incidentInvloved.RoleinIncident__c = event.target.value;
                break;
            case 'physicalrestraint':
                this.incidentInvloved.PhysicalRestrainedType__c = event.target.value;
                break;
            case 'durationrestraint':
                this.incidentInvloved.DurationofPhysicalRestraint__c = event.target.value;
                break;
            case 'durationmechanical':
                this.incidentInvloved.DurationofMechanicalRestraint__c = event.target.value;
                break;
            case 'staffinvolved':
                this.incidentInvloved.StaffInvolved__c = event.target.value;
                break;
            case 'deescalation':
                this.incidentInvloved.DeescalationEffortMade__c = event.target.value;
                break;
            case 'injuiry':
                this.incidentInvloved.InjurySustained__c = event.target.value;
                break;
            case 'injuirylevel':
                this.incidentInvloved.InjurySeverityLevel__c = event.target.value;
                break;
            case 'injuiryrestraint':
                this.incidentInvloved.InjuryResultingfromRestraint__c = event.target.value;
                break;
            case 'seenmedical':
                this.incidentInvloved.SeenByMedical__c = event.target.value;
                break;
            case 'seclusion':
                this.incidentInvloved.Seclusion__c = event.target.value;
                break;
            case 'durationSeclusion':
                this.incidentInvloved.DurationofSeclusion__c = event.target.value;
                break;
        }
    }
    reason = [];

    get selectedValues() {
        return this.reason.join(",");
    }

    handlechangereason(event) {
        this.reason = event.target.value;
    }
  

    get caseId() {
        return sharedData.getCaseId();
    }

    saveIncidentDetails() {
        if(this.addPersonkidchildContact.DOB__c > this.currentDate){
            return this.dispatchEvent(utils.toastMessage(`Date of Birth Should not be Greater than Current Date`, "warning"))
        }
        if (!this.addPersonkidchildContact.FirstName__c) {
            return this.dispatchEvent(utils.toastMessage(`Please Enter First Name `, "warning"))
        }
        if (!this.addPersonkidchildContact.LastName__c) {
            return this.dispatchEvent(utils.toastMessage(`Please Enter Last Name `, "warning"))
        }
        if (!this.addPersonkidchildContact.DOB__c) {
            return this.dispatchEvent(utils.toastMessage(`Please Enter Date of Birth `, "warning"))
        }
        if (!this.addPersonkidchildContact.IdentifierNumber__c) {
            return this.dispatchEvent(utils.toastMessage(`Please Enter Identify Number `, "warning"))
        }
        if (!this.addPersonkidchildContact.AdmittingCharge__c) {
            return this.dispatchEvent(utils.toastMessage(`Please Enter Admitting Change `, "warning"))
        }
        if (!this.addPersonkidchildContact.PlacingAgency__c) {
            return this.dispatchEvent(utils.toastMessage(`Please Enter Placing Agency `, "warning"))
        }
        if (!this.addressOfReporter.AddressLine1__c) {
            return this.dispatchEvent(utils.toastMessage(`Please Enter Address Line1 `, "warning"))
        }
        if (!this.addressOfReporter.State__c) {
            return this.dispatchEvent(utils.toastMessage(`Please Enter the State `, "warning"))
        }
        if (!this.addressOfReporter.City__c) {
            return this.dispatchEvent(utils.toastMessage(`Please Enter the City `, "warning"))
        }
        if (!this.addressOfReporter.County__c) {
            return this.dispatchEvent(utils.toastMessage(`Please Enter the County `, "warning"))
        }
        if (!this.addressOfReporter.ZipCode__c) {
            return this.dispatchEvent(utils.toastMessage(`Please Enter the zipcode `, "warning"))
        }

        if (!this.incidentInvloved.RoleinIncident__c) {
            return this.dispatchEvent(utils.toastMessage(`Please choose role in incident `, "warning"))
        }
        this.addPersonkidchildContact.sobjectType = 'Contact';
        this.addPersonkidchildContact.LastName = this.addPersonkidchildContact.FirstName__c +' '+ this.addPersonkidchildContact.LastName__c;
        this.addPersonkidchildContact.RecordTypeId = this.RecordTypeId;
        this.addPersonkidchildContact.Id = this.personKidContactId;
        InsertUpdateIncident({
            saveObjInsert: this.addPersonkidchildContact
        }).then((result1) => {
            this.addressOfReporter.sobjectType = 'Address__c';
            this.addressOfReporter.Id = this.personKidAddressId;
            this.addressOfReporter.AddressType__c = 'Person/Kid';
            InsertUpdateIncident({
                saveObjInsert: this.addressOfReporter
            }).then((result2) => {
                this.incidentInvloved.sobjectType = "Actor__c";
                this.incidentInvloved.Contact__c = result1;
                this.incidentInvloved.Address__c = result2;
                this.incidentInvloved.Assign__c = this.isChecked;
                this.incidentInvloved.MechanicalRestrainedType__c = this.getMechanicalValues();
                this.incidentInvloved.PhysicalRestraintReason__c = this.selectedValues;
                this.incidentInvloved.Referral__c = this.caseId;
                this.incidentInvloved.Role__c = 'Person/Kid';
                this.incidentInvloved.Id = this.personKidRowId;
                InsertUpdateIncident({
                    saveObjInsert: this.incidentInvloved
                }).then((result3) => {
                    this.dispatchEvent(utils.toastMessage("Person/Kid/Child Details Updated Successfully", "success"));
                    this.backtoListPage();
                }).catch(error => {
                    this.dispatchEvent(utils.toastMessage("Error in saving record", "Error"));      
                  });
            }).catch((error) => {
                this.dispatchEvent(utils.toastMessage("Error in saving record", "Error"));
            })
        }).catch((error) => {
            this.dispatchEvent(utils.toastMessage("Error in saving record", "Error"));
        })
    }
    cancelIncidentPersonKidDetails() {
        this.backtoListPage();
    }

//     //Google api code starts from here
// offSiteAddressChangeHandler start
googleDropdown = false;
dataFromGoogleAPI = [];

offSiteAddressChangeHandler = (event) => {
  let targetValue = event.target.value;
  let targetName = event.target.name;

  if (targetName === "Address Line1") {
      this.addressOfReporter.AddressLine1__c = targetValue;
    getAPIStreetAddress({
      input: targetValue
    })
      .then((response) => {
        this.dataFromGoogleAPI = JSON.parse(response);
        this.googleDropdown = this.dataFromGoogleAPI.predictions.length > 0;
      })
      .catch((err) => {
        this.dispatchEvent(utils.handleError(err));
      });
  } else if (targetName === "Address Line2") {
      this.addressOfReporter.AddressLine2__c = targetValue;
  } else if (targetName === "State") {
      this.addressOfReporter.State__c = targetValue;
  } else if (targetName === "County") {
      this.addressOfReporter.County__c= targetValue;
  } else if (targetName === "Zip Code") {
      this.addressOfReporter.ZipCode__c = targetValue;
  } else if (targetName === "City") {
      this.addressOfReporter.City__c = targetValue;
  } else {
    return "";
  }
};

// offSiteAddressChangeHandler end

handleSelectedAddress = (event) => {
  this.addressOfReporter.AddressLine1__c = event.target.dataset.description;

  getFullDetails({
    placeId: event.target.dataset.id
  }).then((result) => {
    const main = JSON.parse(result);
    this.addressOfReporter.AddressLine1__c =
      main.result.address_components[0].long_name +
      " " +
      main.result.address_components[1].long_name;
    this.addressOfReporter.City__c = main.result.address_components[2].long_name && main.result.address_components[2].long_name  || '';
    this.addressOfReporter.County__c =main.result.address_components[4].long_name && main.result.address_components[4].long_name  || '';
    this.addressOfReporter.State__c =main.result.address_components[5] && main.result.address_components[5].long_name || '';
    this.addressOfReporter.ZipCode__c =main.result.address_components[7] && main.result.address_components[7].long_name || '';
    this.googleDropdown = false;
  });
};

////Google api Ends here//////////

//Pill Multiple checkbox Dropdown Starts for MechanicalRestraint.
    get mechanicalOptionsForDisplay() {
        return this.MechanicalRestraintOptions;
      }

    activeForPType = false;
    pillValueForPtype = [];

    get ForPTypeClass() {
      return this.activeForPType
        ? "slds-popover slds-popover_full-width slds-popover_show"
        : "slds-popover slds-popover_full-width slds-popover_hide";
    }
    handlePType(evt) {
      this.activeForPType = this.activeForPType ? false : true;
    }
    get toggledivForPtype() {
      return this.pillValueForPtype.length == 0 ? false : true;
    }
    handleMouseOutButton(evr) {
      this.activeForPType = false;
    }
    handleclickofPTypeCheckBox(evt) {
      if (evt.target.checked) {
        this.pillValueForPtype.push({
          value: evt.target.value
        });
      } else {
        this.pillValueForPtype = this.remove(
          this.pillValueForPtype,
          "value",
          evt.target.value
        );
      }
      evt.target.checked != evt.target.checked;
    }
    remove(array, key, value) {
      const index = array.findIndex(obj => obj[key] === value);
      return index >= 0
        ? [...array.slice(0, index), ...array.slice(index + 1)]
        : array;
    }
    handleRemove(evt) {
      this.pillValueForPtype = this.remove(
        this.pillValueForPtype,
        "value",
        evt.target.label
      );
      let i;
      let checkboxes = this.template.querySelectorAll("[data-id=checkbox]");
      checkboxes.forEach(element => {
        if (element.value == evt.target.label) {
          if (element.checked) {
            element.checked = false;
          }
        }
      });
      this.activeForPType = true;
    }
  
    get pillvalueForPType() {
      return this.pillValueForPtype;
    }

    mechanicalCheckBoxSelectUnselectDuringPageLoad() {
        let localObject = [];
        this.incidentInvloved.MechanicalRestrainedType__c.split(/\s*,\s*/).forEach(function (myString) {
            localObject.push({
              value: myString
            });
          });
    
          var inp = [...this.template.querySelectorAll("[data-id=checkbox]")];
          inp.forEach(element => {
            localObject.forEach(function (item) {
              if (item.value == element.value) element.checked = true;
            });
          });
          this.pillValueForPtype = localObject;
      }

      getMechanicalValues() {
        var sArray = [];
        this.pillValueForPtype.forEach(function (item) {
          sArray.push(item.value);
        });
        return sArray.join(",");
      }

// Pill Multiple checkbox Dropdown Starts ends

}