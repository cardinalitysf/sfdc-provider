/**
    * @Author        : Sundar Karuppalagu
    * @CreatedOn     : JULY 05 ,2020
    * @Purpose       : Sanctions Revocation Screen
    * @UpdatedBy     : Sundar K -> JULY 24, 2020 -> Code Format handled and try catch method used and sanction id stored in Complaint Object
 **/

import { LightningElement, track, wire, api } from 'lwc';

import getRecordType from "@salesforce/apex/ComplaintsSanctionRevocation.getRecordType";
import getSelectedSanctionData from "@salesforce/apex/ComplaintsSanctionRevocation.getSelectedSanctionData";

import * as sharedData from "c/sharedData";
import { utils } from "c/utils";
import { CJAMS_CONSTANTS, BUTTON_LABELS, TOAST_HEADER_LABELS, DATATABLE_BUTTON_LABELS } from "c/constants";

import { createRecord, updateRecord } from "lightning/uiRecordApi";
import { getObjectInfo, getPicklistValuesByRecordType } from "lightning/uiObjectInfoApi";

//Sanction Object fields declaration
import SANCTIONS_OBJECT from "@salesforce/schema/Sanctions__c";
import ID_FIELD from "@salesforce/schema/Sanctions__c.Id";
import STARTDATE_FIELD from "@salesforce/schema/Sanctions__c.StartDate__c";
import DOCUMENT_FIELD from "@salesforce/schema/Sanctions__c.Document__c";
import DELIVERY_FIELD from "@salesforce/schema/Sanctions__c.Delivery__c";
import REVOCATION_FIELD from "@salesforce/schema/Sanctions__c.Revocation__c";
import COMPLAINT_FIELD from "@salesforce/schema/Sanctions__c.Complaint__c";
import PROVIDER_FIELD from "@salesforce/schema/Sanctions__c.Provider__c";
import RECORDTYPE_FIELD from "@salesforce/schema/Sanctions__c.RecordTypeId";

//Complaint Object Fields declaration
import SANCTION_FIELD from "@salesforce/schema/Case.Sanctions__c";
import COMPLAINTID_FIELD from "@salesforce/schema/Case.Id";

//Class name declaration in lwc
export default class ComplaintsSanctionRevocation extends LightningElement {
    //Fields declaration
    @track revocationDetails = {};
    @track btnLabel = '';
    @track showDelivery = false;
    @track Spinner = true;

    //Disable fields using flags
    @track isRevocationDateDisable = false;
    @track isDocumentDisable = false;
    @track isDeliveryDisable = false;
    @track isSaveBtnDisable = false;    

    //Picklist value Initialization
    @track documentOptions = [];
    @track deliveryOptions = [];

    //Local value declaration
    _recordTypeId;
    _actionName;
    _actionErrorName;
    error;

    //Global value declaration
    @api selectedSanctionId;
    @api sanctionActionName;
    @api sanctionPageType;

    //get Complaint ID
    get complaintId() {
        return sharedData.getCaseId();
    }

    //get Provider ID
    get providerId() {
        return sharedData.getProviderId();
    }

    //Declare sanction object into one variable - Wire to a property
    @wire(getObjectInfo, { objectApiName: SANCTIONS_OBJECT })
    objectInfo;

    //Function used to get all picklist values from sanction object - Wire to a function
    @wire(getPicklistValuesByRecordType, {
        objectApiName: SANCTIONS_OBJECT,
        recordTypeId: '$objectInfo.data.defaultRecordTypeId'
    })
    getAllPicklistValues({ error, data }) {
        if (data) {
            this.error = null;

            // Document Field Picklist values
            this.documentOptions = data.picklistFieldValues.Document__c.values.map(key => {
                return {
                    label: key.label,
                    value: key.value
                }
            });

            // Delivery Field Picklist values
            this.deliveryOptions = data.picklistFieldValues.Delivery__c.values.map(key => {
                return {
                    label: key.label,
                    value: key.value
                }
            });
        } else if (error) {
            this.error = JSON.stringify(error);
            this.dispatchEvent(utils.toastMessage("Picklist values are failed to load. please check", TOAST_HEADER_LABELS.TOAST_WARNING));
        }
    }

    //Connected Call Back method
    connectedCallback() {
        this.btnLabel = !this.selectedSanctionId ? BUTTON_LABELS.BTN_SAVE : BUTTON_LABELS.BTN_UPDATE;

        this.handleRecordType();
    }

    //Handle Record Type method
    handleRecordType() {
        //Fetch Record Type for Sanction Object
        getRecordType({
            name: this.sanctionPageType
        })
        .then(result => {
            if (!result) {
                this.Spinner = false;
                return this.dispatchEvent(utils.toastMessage("Sanction Record Type failed to load. please check", TOAST_HEADER_LABELS.TOAST_WARNING));
            }

            this._recordTypeId = result;

            if (!this.selectedSanctionId)
                this.Spinner = false;
            else
                this.handleRevocationData();
        })
        .catch(errors => {
            this.Spinner = false;
            return this.dispatchEvent(utils.handleError(errors));
        });
    }

    //Handle Revocation Data after ID generated
    handleRevocationData() {
        if ([DATATABLE_BUTTON_LABELS.BTN_EDIT, DATATABLE_BUTTON_LABELS.BTN_VIEW].includes(this.sanctionActionName)) {
            //Fetch selected sanction id data
            getSelectedSanctionData({
                sanctionId: this.selectedSanctionId
            })
            .then(result => {
                if (result.length == 0) {
                    this.Spinner = false;
                    return this.dispatchEvent(utils.toastMessage("Error in fetching revocation data. please check", TOAST_HEADER_LABELS.TOAST_WARNING));
                }

                try {
                    this.revocationDetails.StartDate__c = result[0].StartDate__c;
                    this.revocationDetails.Document__c = result[0].Document__c;
                    this.revocationDetails.Delivery__c = result[0].Delivery__c;
    
                    this.showDelivery = ['RCC Moratorium', 'RCC Notice Intent Revoke'].includes(result[0].Document__c) ? true : false;

                    this.handleFieldsDisable();
                } catch (error) {
                    this.error = JSON.stringify(error);
                    this.Spinner = false;
                }
            })
            .catch(errors => {
                this.Spinner = false;
                return this.dispatchEvent(utils.handleError(errors));
            });
        } else
            this.dispatchEvent(utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, TOAST_HEADER_LABELS.TOAST_WARNING));
    }

    //Handle fields disable method
    handleFieldsDisable() {
        try {
            if (this.sanctionActionName == DATATABLE_BUTTON_LABELS.BTN_VIEW) {
                this.isRevocationDateDisable = true;
                this.isDocumentDisable = true;
                this.isDeliveryDisable = true;
                this.isSaveBtnDisable = true;
            } else {
                this.isRevocationDateDisable = false;
                this.isDocumentDisable = false;
                this.isDeliveryDisable = false;
                this.isSaveBtnDisable = false;
            }
            this.Spinner = false;
        } catch(error) {
            this.error = JSON.stringify(error);
            this.Spinner = false;
        }
    }

    //Fields On Change method
    revocationOnChange(event) {
        switch(event.target.name) {
            case 'RevocationDate': 
                this.revocationDetails.StartDate__c = event.target.value;
                break;
            case 'Document' : 
                this.revocationDetails.Document__c = event.detail.value;
                this.revocationDetails.Delivery__c = null;
                this.showDelivery = ['RCC Moratorium', 'RCC Notice Intent Revoke'].includes(event.detail.value) ? true : false;
                break;
            case this.showDelivery === true && 'Delivery' : 
                this.revocationDetails.Delivery__c = event.detail.value;
                break;
        }
    }

    //Handle Common Method for Save/Update sanction
    handleSaveMethod() {
        if (!this._recordTypeId)
            return this.dispatchEvent(utils.toastMessage("Sanction Record Type failed to load. please check", TOAST_HEADER_LABELS.TOAST_WARNING));

        if (!this.revocationDetails.StartDate__c)
            return this.dispatchEvent(utils.toastMessage("Please Select Revocation Date", TOAST_HEADER_LABELS.TOAST_WARNING));

        if (!this.revocationDetails.Document__c)
            return this.dispatchEvent(utils.toastMessage("Please Select Document", TOAST_HEADER_LABELS.TOAST_WARNING));

        if (this.showDelivery === true && !this.revocationDetails.Delivery__c)
            return this.dispatchEvent(utils.toastMessage("Please Select Delivery", TOAST_HEADER_LABELS.TOAST_WARNING));

        this.isSaveBtnDisable = true;
        this.Spinner = true;

        const fields = {};

        fields[STARTDATE_FIELD.fieldApiName] = this.revocationDetails.StartDate__c;
        fields[DOCUMENT_FIELD.fieldApiName] = this.revocationDetails.Document__c;
        fields[DELIVERY_FIELD.fieldApiName] = this.revocationDetails.Delivery__c;

        this._actionName = this.btnLabel == BUTTON_LABELS.BTN_SAVE ? 'saved' : 'updated';
        this._actionErrorName = this.btnLabel == BUTTON_LABELS.BTN_SAVE ? 'saving' : 'updating';

        if (!this.selectedSanctionId) {
            fields[COMPLAINT_FIELD.fieldApiName] = this.complaintId;
            fields[PROVIDER_FIELD.fieldApiName] = this.providerId;
            fields[RECORDTYPE_FIELD.fieldApiName] = this._recordTypeId;
            fields[REVOCATION_FIELD.fieldApiName] = true;

            const recordInput = {
                apiName: SANCTIONS_OBJECT.objectApiName,
                fields
            };

            createRecord(recordInput)
            .then((result) => {
                this.selectedSanctionId = result.id;
                this.updateComplaintRecord();
            })
            .catch(error => {
                this.error = JSON.stringify(error);
                this.Spinner = false;
                this.isSaveBtnDisable = false;
                this.dispatchEvent(utils.toastMessage(`Error in ${this._actionErrorName} revocation sanction details. please check`, TOAST_HEADER_LABELS.TOAST_WARNING));
            })
        } else {
            fields[ID_FIELD.fieldApiName] = this.selectedSanctionId;

            const recordInput = {
                fields
            };

            updateRecord(recordInput)
            .then(() => {
                this.dispatchEvent(utils.toastMessage(`Revocation sanction has been ${this._actionName} successfully`, TOAST_HEADER_LABELS.TOAST_SUCCESS));
                this.handleRedirectionMethod();
            })
            .catch(error => {
                this.error = JSON.stringify(error);
                this.Spinner = false;
                this.isSaveBtnDisable = false;
                this.dispatchEvent(utils.toastMessage(`Error in ${this._actionErrorName} revocation sanction details. please check`, TOAST_HEADER_LABELS.TOAST_WARNING));
            })
        }
    }

    //Update Complaint record - sanction ID
    updateComplaintRecord() {
        const fields = {};
        fields[SANCTION_FIELD.fieldApiName] = this.selectedSanctionId;
        fields[COMPLAINTID_FIELD.fieldApiName] = this.complaintId;

        const complaintRecordInput = {
            fields
        };

        updateRecord(complaintRecordInput)
        .then(() => {
            this.dispatchEvent(utils.toastMessage(`Revocation sanction has been ${this._actionName} successfully`, TOAST_HEADER_LABELS.TOAST_SUCCESS));
            this.handleRedirectionMethod();
        })
        .catch(error => {
            this.error = JSON.stringify(error);
            this.Spinner = false;
            this.isSaveBtnDisable = false;
            this.dispatchEvent(utils.toastMessage(`Error in ${this._actionErrorName} revocation sanction details. please check`, TOAST_HEADER_LABELS.TOAST_WARNING));
        })
    }

    //Handle Cancel method
    handleCancelMethod() {
        this.Spinner = true;

        //1 Second Interval
        setTimeout(() => {
            this.handleRedirectionMethod();
        }, 1000); 
    }

    //Handle Sanction Redirection method
    handleRedirectionMethod() {
        this.Spinner = false;
        this.revocationDetails = {};
        this.isSaveBtnDisable = false;

        const oncomplaintid = new CustomEvent('redirecttocomplaintsanctions', {
            detail: {
                first: true
            }
        });
        this.dispatchEvent(oncomplaintid);
    }
}