/**
 * @Author        : Naveen S
 * @CreatedOn     : April 16 ,2020
 * @Purpose       : Provider Licenses Information 
 * @updatedBy     :
 * @updatedOn     :
 **/
import {
    LightningElement,
    track
} from "lwc";
import myResource from '@salesforce/resourceUrl/styleSheet';
import {
    loadStyle
} from 'lightning/platformResourceLoader';
import images from '@salesforce/resourceUrl/images';
import {
    utils
} from "c/utils";
import getApplicationId from "@salesforce/apex/providerLicensesInfo.getApplicationId";
import getLicensesId from "@salesforce/apex/providerLicensesInfo.getLicensesId";
import getLicenseInformation from "@salesforce/apex/providerLicensesInfo.getLicenseInformation";
import getProviderDetails from "@salesforce/apex/providerLicensesInfo.getProviderDetails";
import getaddressdetails from "@salesforce/apex/providerLicensesInfo.getaddressdetails";
import getMonitoringDetails from '@salesforce/apex/providerLicensesInfo.getMonitoringDetails';
import getIRCRatesCapacity from '@salesforce/apex/providerLicensesInfo.getIRCRatesCapacity';
import getContractCapacity from '@salesforce/apex/providerLicensesInfo.getContractCapacity';
import * as sharedData from "c/sharedData";
import { CJAMS_CONSTANTS } from 'c/constants';
export default class ProviderLicensesInfo extends LightningElement {
    @track LicenseNumber;
    @track Program;
    @track ProgramType;
    @track ProviderName;
    @track LicenseStatus;
    @track IRCCapacity;
    @track LicenseBeds;
    @track ContractedBeds;
    @track SiteID = null;
    @track SiteAddress = null;
    @track ApplicationId;
    @track Monitoring;
    @track MobileNumber = null;
    @track EmailAddress = null;
    @track Fax;
    @track Gender;
    @track licensesIdList = [];
    @track issuanceDate;
    @track effectiveDate;
    @track expirationDate;
    @track MinAge;
    @track MaxAge;
    @track MinIQ;
    @track MaxIQ;
    @track ApplicationPrimaryID;
    @track MonitoringExpriyDate;
    @track licenseInfoVisible = true;
    @track OpenLicenseCard = true;
    @track NoLicenseCard = false;
    @track MonitoringPrimaryId = null;
    attachmentIcon = images + '/application-monitoring.svg';
    @track LicenseApproveImg = true;
    @track LiciensePrimaryId;
    @track Spinner = true;
    siteicon = images + '/licenseInfo-icon.svg';
    addressIcon = images + '/site-address.svg';
    greenTick = images + '/green-tick.svg';
    fax = images + '/fax.svg';
    email = images + '/email.svg';
    mobile = images + '/mobile.svg';
    email2 = images + '/email2.svg';
    paymentaddress = images + '/payment-address.svg';
    providernameicon = images + '/provider-nameicon.svg';
    ircIcon = images + '/ircIcon.svg';
    licenseBeds = images + '/license-bedIcon.svg';
    contractedBeds = images + '/contracted-bedIcon.svg';
    ageRange = images + '/agerangeIcon.svg';
    iQRange = images + '/iQ-RangeIcon.svg';
    genderIcon = images + '/genderIcon.svg';
    get getProviderId() {
        return sharedData.getProviderId();
    }
    renderedCallback() {
        Promise.all([
            loadStyle(this, myResource + '/styleSheet.css')
        ])
    }
    connectedCallback() {
        this.getlicensesInformation();
    }
    getlicensesInformation() {
        getApplicationId({
            providerId: this.getProviderId
        })
            .then((data) => {
                let currentData = [];
                data.forEach((row) => {
                    let rowData = {};
                    rowData.Id = row.Id;
                    currentData.push(rowData);
                });
                this.objapplicationid = currentData;
                this.getLicensesFromApplication();
            })
            .catch(errors => {
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const { title, message, errorType } = error;
                    this.dispatchEvent(
                        utils.toastMessageWithTitle(title, message, errorType)
                    );
                } else {
                    this.dispatchEvent(
                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                    );
                }
            });
    }
    getLicensesFromApplication() {
        let applicationStringId = this.objapplicationid.map(e => e.Id).join(",");
        getLicensesId({
            applicationID: applicationStringId
        })
            .then((data) => {
                this.licensesIdList = data.map((row, index) => {
                    return {
                        Id: row.Id,
                        Name: row.Name,
                        dataIndex: index
                    }
                });
                if (this.licensesIdList.length > 0) {
                    this.licenseInfoVisible = true;
                    this.NoLicenseCard = false;
                    this.OpenLicenseCard = true;
                    this.getLicenseInformation(this.licensesIdList[0].Id);
                } else {
                    this.Spinner = false;
                    this.licenseInfoVisible = false;
                    this.NoLicenseCard = true;
                    this.OpenLicenseCard = false;
                }
            })
            .catch(errors => {
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const { title, message, errorType } = error;
                    this.dispatchEvent(
                        utils.toastMessageWithTitle(title, message, errorType)
                    );
                } else {
                    this.dispatchEvent(
                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                    );
                }
            });
    }
    licenseInformation(event) {
        let dataIndex = event.currentTarget.dataset.value;
        let datas = this.template.querySelectorAll('.divHighLight');
        for (let i = 0; i < datas.length; i++) {
            if (i == dataIndex)
                datas[i].className = "slds-nav-vertical__item divHighLight slds-is-active";
            else
                datas[i].className = "slds-nav-vertical__item divHighLight";
        }
        this.getLicenseInformation(event.currentTarget.dataset.id);
    }
    getLicenseInformation(recordId) {
        this.LiciensePrimaryId = recordId;
        sharedData.setLicenseId(this.LiciensePrimaryId);
        getLicenseInformation({
            licenseId: recordId
        })
            .then((result) => {
                if (result.length > 0 && result[0].Name) {
                    this.LicenseNumber = result[0].Name;
                    this.Program = result[0].ProgramName__c ? result[0].ProgramName__c : ' - ';
                    this.ProgramType = result[0].ProgramType__c ? result[0].ProgramType__c : ' - ';
                    this.issuanceDate = utils.formatDate(result[0].CreatedDate);
                    this.effectiveDate = utils.formatDate(result[0].StartDate__c);
                    this.expirationDate = utils.formatDate(result[0].EndDate__c);
                    this.LicenseStatus = result[0].Status__c ? result[0].Status__c : ' - ';
                    this.ApplicationId = result[0].Application__r.Name ? result[0].Application__r.Name : ' - ';
                    this.Gender = result[0].Application__r.Gender__c ? result[0].Application__r.Gender__c : ' - ';
                    this.LicenseBeds = result[0].Application__r.Capacity__c ? result[0].Application__r.Capacity__c : ' - ';
                    this.MinAge = result[0].Application__r.MinAge__c ? result[0].Application__r.MinAge__c : ' 0 ';
                    this.MaxAge = result[0].Application__r.MaxAge__c ? result[0].Application__r.MaxAge__c : ' - ';
                    this.MinIQ = result[0].Application__r.MinIQ__c ? result[0].Application__r.MinIQ__c : ' 0 ';
                    this.MaxIQ = result[0].Application__r.MaxIQ__c ? result[0].Application__r.MaxIQ__c : ' - ';
                    this.ApplicationPrimaryID = result[0].Application__r.Id;
                    if (this.LicenseStatus == ' - ') {
                        this.LicenseApproveImg = false;
                    } else {
                        this.LicenseApproveImg = true;
                    }
                    if (this.ApplicationPrimaryID != undefined) {
                        getaddressdetails({
                            getaddressdetails: this.ApplicationPrimaryID
                        })
                            .then((result) => {
                                if (result.length > 0) {
                                    this.SiteAddress = result[0].AddressLine1__c ? result[0].AddressLine1__c : ' - ';
                                    this.SiteID = result[0].Name ? result[0].Name : ' - ';
                                    this.EmailAddress = result[0].Email__c ? result[0].Email__c : ' - ';
                                    this.MobileNumber = result[0].Phone__c;
                                } else {
                                    this.SiteAddress = '-';
                                    this.SiteID = '-';;
                                    this.EmailAddress = '-';;
                                    this.MobileNumber = '-';;
                                }
                            })
                            .catch(errors => {
                                if (errors) {
                                    let error = JSON.parse(errors.body.message);
                                    const { title, message, errorType } = error;
                                    this.dispatchEvent(
                                        utils.toastMessageWithTitle(title, message, errorType)
                                    );
                                } else {
                                    this.dispatchEvent(
                                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                                    );
                                }
                            });
                        getMonitoringDetails({
                            applicationid: this.ApplicationPrimaryID
                        })
                            .then((result) => {
                                if (result.length > 0) {
                                    this.MonitoringPrimaryId = result[0].Id;
                                    this.Monitoring = result[0].Name;
                                } else {
                                    this.Monitoring = '-';
                                }
                            })
                            .catch(errors => {
                                if (errors) {
                                    let error = JSON.parse(errors.body.message);
                                    const { title, message, errorType } = error;
                                    this.dispatchEvent(
                                        utils.toastMessageWithTitle(title, message, errorType)
                                    );
                                } else {
                                    this.dispatchEvent(
                                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                                    );
                                }
                            });
                    }
                    if (this.LiciensePrimaryId != undefined) {
                        getIRCRatesCapacity({
                            licenseId: this.LiciensePrimaryId
                        })
                            .then((result) => {
                                if (result.length > 0) {
                                    this.IRCCapacity = result[0].IRCBedCapacity__c ? result[0].IRCBedCapacity__c : ' - ';
                                } else {
                                    this.IRCCapacity = '-';
                                }
                            })
                            .catch(errors => {
                                if (errors) {
                                    let error = JSON.parse(errors.body.message);
                                    const { title, message, errorType } = error;
                                    this.dispatchEvent(
                                        utils.toastMessageWithTitle(title, message, errorType)
                                    );
                                } else {
                                    this.dispatchEvent(
                                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                                    );
                                }
                            });
                        getContractCapacity({
                            licenseId: this.LiciensePrimaryId
                        })
                            .then((result) => {
                                if (result.length > 0) {
                                    this.ContractedBeds = result[0].TotalContractBed__c ? result[0].TotalContractBed__c : ' - ';
                                } else {
                                    this.ContractedBeds = '-';
                                }
                            })
                            .catch(errors => {
                                if (errors) {
                                    let error = JSON.parse(errors.body.message);
                                    const { title, message, errorType } = error;
                                    this.dispatchEvent(
                                        utils.toastMessageWithTitle(title, message, errorType)
                                    );
                                } else {
                                    this.dispatchEvent(
                                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                                    );
                                }
                            });
                    }
                    this.getProviderNameDeatils();
                }
            })
            .catch(errors => {
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const { title, message, errorType } = error;
                    this.dispatchEvent(
                        utils.toastMessageWithTitle(title, message, errorType)
                    );
                } else {
                    this.dispatchEvent(
                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                    );
                }
            });
    }
    getProviderNameDeatils() {
        getProviderDetails({
            providerId: this.getProviderId
        })
            .then((result) => {
                if (result.length > 0 && result[0].Name) {
                    this.ProviderName = result[0].Name ? result[0].Name : ' - ';
                    this.Fax = result[0].Fax ? result[0].Fax : ' - ';
                }
                this.Spinner = false;
            })
            .catch(errors => {
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const { title, message, errorType } = error;
                    this.dispatchEvent(
                        utils.toastMessageWithTitle(title, message, errorType)
                    );
                } else {
                    this.dispatchEvent(
                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                    );
                }
            });
    }
    redirectToMonitering() {
        sharedData.setMonitoringId(this.MonitoringPrimaryId);
        const oncaseid = new CustomEvent('redirecteditmonitering', {
            detail: {
                first: true
            }
        });
        this.dispatchEvent(oncaseid);
    }
    redirectToApplication() {
        sharedData.setApplicationId(this.ApplicationPrimaryID);
        const oncaseid = new CustomEvent('redirecteditapplication', {
            detail: {
                first: false
            }
        });
        this.dispatchEvent(oncaseid);
    }
}