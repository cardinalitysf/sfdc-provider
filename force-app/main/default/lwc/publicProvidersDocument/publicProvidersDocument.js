import {
    LightningElement,
    track

} from 'lwc';
import * as referralsharedDatas from "c/sharedData";
import {
    CJAMS_CONSTANTS
} from 'c/constants';


export default class PublicProvidersDocument extends LightningElement {
    get providerId() {
   return referralsharedDatas.getProviderId();
   //  return '0010w00000B1ckwAAB';
    }
    get sobjectNameToFetch() {
        return CJAMS_CONSTANTS.PROVIDER_OBJECT_NAME;
    }

}