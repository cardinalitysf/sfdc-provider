/**
 * @Author        : Preethi Bojarajan
 * @CreatedOn     : March 21 ,2020
 * @Purpose       : In Edit Mode Of Application Header Component
 * @updatedBy     : Preethi Bojarajan
 * @updatedOn     : Aug 21, 2020
 **/

import { LightningElement, track, wire } from 'lwc';
import * as sharedData from "c/sharedData";
import getApplicationId from '@salesforce/apex/providerApplicationHeader.getApplicationId';

export default class ProviderApplicationHeader extends LightningElement {
    @track applicationId = null;

    get Id() {
        return sharedData.getApplicationId();
    }

    @wire(getApplicationId, {
        Id: '$Id'
    })
    applicationIdOf({
        error,
        data
    }) {
        if (data) {
            this.applicationId = data[0].Name;
        } else if (error) {
            this.error = error;
        }
    }

    redirectToProvidersAppDashboard(){
        const onRedirection = new CustomEvent('redirecttoproviderappdashboard', {
            detail: {
                first: true
            }
        });
        this.dispatchEvent(onRedirection);
    }
}