/**
 * @Author        : Sindhu Venkateswarlu
 * @CreatedOn     : MAY 14 ,2020
 * @Purpose       : This component represents to create/update Training details by Trainer.
 * @UpdatedBy     : Sundar K
 * @UpdatedOn     : MAY 22, 2020
 * @UpdatedBy     : Sundar K -> JULY 27, 2020 -> Training Module Edit/View Functionality added
 * @UpdatedBy     : Sundar K -> AUGUST 9, 2020 -> Training Module ADD/Edit/View Functionality modified and commented lines are removed.
 **/

import {
    LightningElement,
    track,
    wire,
    api
} from "lwc";

import getRecordType from "@salesforce/apex/PublicProviderAddNewTraining.getRecordType";
import getInstructorDetails from "@salesforce/apex/PublicProviderAddNewTraining.getInstructorDetails";
import editOrViewTrainingDetails from '@salesforce/apex/PublicProviderAddNewTraining.editOrViewTrainingDetails';
import getMeetingInfoDetails from '@salesforce/apex/PublicProviderAddNewTraining.getMeetingInfoDetails';
import getAPIStreetAddress from "@salesforce/apex/GetStreetAddressFromGoogleAPI.getSuggestions";
import getFullDetails from "@salesforce/apex/GetStreetAddressFromGoogleAPI.getPlaceDetails";
import getAllStates from "@salesforce/apex/PublicProviderAddNewTraining.getStateDetails";

import insertOrUpdateMeetingInfoDetails from "@salesforce/apex/PublicProviderAddNewTraining.insertOrUpdateMeetingInfoDetails";

import images from "@salesforce/resourceUrl/images";
import {
    fireEvent
} from "c/pubsub";
import {
    utils
} from "c/utils";
import {
    CJAMS_CONSTANTS_COUNTYARR,
    TOAST_HEADER_LABELS,
    BUTTON_LABELS,
    DATATABLE_BUTTON_LABELS
} from "c/constants";

import {
    CurrentPageReference
} from "lightning/navigation";
import {
    getPicklistValuesByRecordType,
    getObjectInfo
} from "lightning/uiObjectInfoApi";
import {
    createRecord,
    updateRecord
} from "lightning/uiRecordApi";

//Training Object fields declaration
import TRAINING_OBJECT from "@salesforce/schema/Training__c";
import ID_FIELD from "@salesforce/schema/Training__c.Id";
import RECORDTYPEID_FIELD from "@salesforce/schema/Training__c.RecordTypeId";
import TYPE_FIELD from "@salesforce/schema/Training__c.Type__c";
import SESSIONTYPE_FIELD from "@salesforce/schema/Training__c.SessionType__c";
import MEDIUM_FIELD from "@salesforce/schema/Training__c.Medium__c";
import URL_FIELD from "@salesforce/schema/Training__c.URL__c";
import ROOMNO_FIELD from "@salesforce/schema/Training__c.RoomNo__c";
import ADDRESSLINE1_FIELD from "@salesforce/schema/Training__c.AddressLine1__c";
import STATE_FIELD from "@salesforce/schema/Training__c.State__c";
import CITY_FIELD from "@salesforce/schema/Training__c.City__c";
import COUNTY_FIELD from "@salesforce/schema/Training__c.County__c";
import ZIPCODE_FIELD from "@salesforce/schema/Training__c.ZipCode__c";
import INSTRUCTOR1_FIELD from "@salesforce/schema/Training__c.Instructor1__c";
import INSTRUCTOR2_FIELD from "@salesforce/schema/Training__c.Instructor2__c";
import JURISDICTION_FIELD from "@salesforce/schema/Training__c.Jurisdiction__c";
import SESSIONNUMBER_FIELD from "@salesforce/schema/Training__c.SessionNumber__c";
import DATE_FIELD from "@salesforce/schema/Training__c.Date__c";
import STARTTIME_FIELD from "@salesforce/schema/Training__c.StartTime__c";
import ENDTIME_FIELD from "@salesforce/schema/Training__c.EndTime__c";
import DURATION_FIELD from "@salesforce/schema/Training__c.Duration__c";
import TOPICDESCRIPTION_FIELD from "@salesforce/schema/Training__c.Description__c";
import STATUS_FIELD from "@salesforce/schema/Training__c.Status__c";

//Meeting Info Session details table header
const columns = [{
    label: 'Sessions',
    fieldName: 'SessionNumber'
}, {
    label: 'Date',
    fieldName: 'Date'
}, {
    label: 'Start Time',
    fieldName: 'StartTime',
    sortable: true
}, {
    label: 'EndTime',
    fieldName: 'EndTime'
}, {
    label: 'Duration',
    fieldName: 'Duration'
}]

//Class Name declaration
export default class PublicProviderAddNewTraining extends LightningElement {
    //Variable Initialization
    @track trainingDetails = {};
    @track title;
    @track btnLabel;
    @track sessionBtnLabel;
    @track columns = columns;
    @track meetingInfoSessionDetailsArr = [];
    @track minTime;
    @track minDate;
    @track Spinner = true;
    @track today = new Date().setHours(0, 0, 0, 0);
    @track totalHrsInMinutes = 0;

    //Address Fields Initialization
    @track strStreetAddresPrediction = [];
    @track selectedState;
    @track selectedCity;
    @track selectedCounty;
    @track selectedZipCode;
    @track stateSearchTxt;
    @track countySearchTxt;
    @track stateArr = [];
    @track countyArr = CJAMS_CONSTANTS_COUNTYARR;
    @track filteredStateArr = [];
    @track filteredCountyArr = [];
    @track showStateBox = false;
    @track showCountyBox = false;
    active = false;

    //Local value declaration
    selectSessionArr = [];
    sessionTypeOnLoadArray = [];
    number = 0;
    _recordTypeId;
    _recordTypeName;
    sessionNumberStr;
    finalStatus;
    error;
    durationIcon = images + '/icon-duration.png';

    //Disable fields Initialization
    @track isDisableType = false;
    @track isDisableSessionType = false;
    @track isDisableMedium = false;
    @track isDisableEndTime = false;
    @track isDisableDuration = true;
    @track isDisableActionBtn = false;
    @track isDisableSessionBtn = false;
    @track isSessionAdded = false;

    //Field Visibility Initialization
    @track showURL1 = false;
    @track showURL2 = false;
    @track showAddressFields = false;
    @track showSessionNo = false;
    @track showSessionBtn = false;
    @track showMeetingInfoTable = false;
    @track showDescription = false;

    //Drop down setter and getters
    _typeOptions;
    _sessionTypeOptions;
    _jurisdictionOptions;
    _mediumOptions;
    _instructor1Options;
    _instructor2Options;
    _selectSessionOptions = [{
        label: 'New',
        value: 'New'
    }];

    @track isSessionModified = false;
    decideClose = false;
    viewTrainingModel = false;
    selectedTrainingId;

    get typeOptions() {
        return this._typeOptions;
    }

    get sessionTypeOptions() {
        return this._sessionTypeOptions;
    }

    get jurisdictionOptions() {
        return this._jurisdictionOptions;
    }

    get mediumOptions() {
        return this._mediumOptions;
    }

    get instructor1Options() {
        return this._instructor1Options;
    }

    get instructor2Options() {
        return this._instructor2Options;
    }

    get selectSessionOptions() {
        return [...this._selectSessionOptions];
    }

    get strStreetAddresPredictionLength() {
        return this.strStreetAddresPrediction.predictions ? true : false;
    }

    get tabClass() {
        return this.active ?
            "slds-dropdown slds-dropdown_length-5 slds-dropdown_fluid slds-show" :
            "slds-dropdown slds-dropdown_length-5 slds-dropdown_fluid slds-hide"
    }

    actionOnModal;
    @api get loadTrainingModal() {
        return this.actionOnModal;
    }

    set loadTrainingModal(value) {
        this.setAttribute('loadTrainingModal', value);
        this.actionOnModal = value;
        this.handleValueChange(value);
    }

    @wire(CurrentPageReference) pageRef;

    //Function used to fetch all Instructor names from Contact Object when Modal opens and where Record Type = 'Instructor'
    @wire(getInstructorDetails)
    wireInstructorDetails({
        error,
        data
    }) {
        if (data) {
            this.error = null;

            if (data.length == 0)
                return this.dispatchEvent(utils.toastMessage("Instructor details are not found. please check", TOAST_HEADER_LABELS.TOAST_WARNING));

            let contactArr = data.map(row => {
                return {
                    label: row.Name,
                    value: row.Id
                }
            });

            this._instructor1Options = contactArr;
            this._instructor2Options = contactArr;
        } else if (error) {
            this.error = JSON.stringify(error);
            this.dispatchEvent(utils.toastMessage("Error in fetching Instructor details. please check", TOAST_HEADER_LABELS.TOAST_WARNING));
        }
    }

    //Declare Training object into one variable - Wire to a property
    @wire(getObjectInfo, {
        objectApiName: TRAINING_OBJECT
    })
    objectInfo;

    //Function used to get all picklist values from Training Object - Wire to a function
    @wire(getPicklistValuesByRecordType, {
        objectApiName: TRAINING_OBJECT,
        recordTypeId: '$objectInfo.data.defaultRecordTypeId'
    })
    getAllPicklistValues({
        error,
        data
    }) {
        if (data) {
            this.error = null;

            // Type Field Picklist values
            this._typeOptions = data.picklistFieldValues.Type__c.values.map(key => {
                return {
                    label: key.label,
                    value: key.value
                }
            });

            // Session Type Field Picklist values
            this._sessionTypeOptions = data.picklistFieldValues.SessionType__c.values.map(key => {
                return {
                    label: key.label,
                    value: key.value
                }
            });
            this.sessionTypeOnLoadArray = this._sessionTypeOptions;

            // Jurisdiction Type Field Picklist values
            this._jurisdictionOptions = data.picklistFieldValues.Jurisdiction__c.values.map(key => {
                return {
                    label: key.label,
                    value: key.value
                }
            });

            //  Medium Field Picklist values
            this._mediumOptions = data.picklistFieldValues.Medium__c.values.map(key => {
                return {
                    label: key.label,
                    value: key.value
                }
            });
        } else if (error) {
            this.error = JSON.stringify(error);
            this.dispatchEvent(utils.toastMessage("Error in fetching Picklist values. please check.", TOAST_HEADER_LABELS.TOAST_WARNING));
        }
    }

    //Handle Value change method
    handleValueChange(value) {        
        const {
            Component,
            id
        } = value;

        switch (Component) {
            case DATATABLE_BUTTON_LABELS.BTN_EDIT:
                this.selectedTrainingId = id;
                this.title = 'EDIT TRAINING SESSION';
                this.btnLabel = BUTTON_LABELS.BTN_UPDATE;
                this.Spinner = true;
                this.handleEditOrViewTrainingModel();
                setTimeout(() => {
                    this.viewTrainingModel = false;
                    this.decideClose = true;
                }, 1000);
                break;
            case DATATABLE_BUTTON_LABELS.BTN_VIEW:
                this.selectedTrainingId = id;
                this.title = 'VIEW TRAINING SESSION';
                this.btnLabel = BUTTON_LABELS.BTN_UPDATE;
                this.Spinner = true;
                this.handleEditOrViewTrainingModel();
                setTimeout(() => {
                    this.decideClose = false;
                    this.viewTrainingModel = true;
                }, 1000);
                break;
            case "AddTraining":
                this._recordTypeName = 'Public';
                this.title = 'ADD TRAINING SESSION';
                this.btnLabel = BUTTON_LABELS.BTN_SAVE;
                this.Spinner = true;
                this.handleRecordType();
                setTimeout(() => {
                    this.viewTrainingModel = false;
                    this.decideClose = true;
                }, 1000);
                break;
        }
    }

    //Handle Record Type method for Open Training Model
    handleRecordType() {
        //Fetch Record Type for Sanction Object
        getRecordType({
                name: this._recordTypeName
            })
            .then(result => {
                if (!result) {
                    this.Spinner = false;
                    return this.dispatchEvent(utils.toastMessage("Training Record Type failed to load. please check", TOAST_HEADER_LABELS.TOAST_WARNING));
                }
                this._recordTypeId = result;
                this.handleStateInitialLoad();
            })
            .catch(errors => {
                this.Spinner = false;
                this.dispatchEvent(utils.handleError(errors));
            });
    }

    //Function used to fetch all states from Address Object when Modal Opens
    handleStateInitialLoad() {
        getAllStates()
            .then(result => {
                if (result.length == 0) {
                    this.Spinner = false;
                    return this.dispatchEvent(utils.toastMessage("No states found. please check", TOAST_HEADER_LABELS.TOAST_WARNING));
                }

                this.stateArr = result.map(row => {
                    return {
                        Id: row.Id,
                        Value: row.RefValue__c
                    }
                });

                if (this.stateArr.length > 0)
                    this.handleTrainingModal();
            })
            .catch(errors => {
                this.Spinner = false;
                this.dispatchEvent(utils.handleError(errors));
            })
    }

    //Handle Training Model method
    handleTrainingModal() {
        let currentDate = new Date();
        let currentMinute = new Date().getMinutes() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes();
        let currentHour = new Date().getHours() < 10 ? '0' + new Date().getHours() : new Date().getHours();

        this.minDate = utils.formatDateYYYYMMDD(currentDate);
        this.trainingDetails.Date__c = utils.formatDateYYYYMMDD(currentDate);
        this.trainingDetails.StartTime__c = currentHour + ':' + currentMinute + ':00.000Z';
        this.minTime = this.trainingDetails.StartTime__c;
        this.trainingDetails.Duration__c = '00:00';
        this.isDisableMedium = true;
        this.isDisableSessionType = true;
        this.showDescription = true;
        this.isDisableActionBtn = true;
        this._number = 0;
        this.Spinner = false;
    }

    handleEditOrViewTrainingModel() {
        editOrViewTrainingDetails({
                selectedTrainingId: this.selectedTrainingId
            })
            .then(result => {
                if (result.length == 0) {
                    this.Spinner = false;
                    return this.dispatchEvent(utils.toastMessage("No Training Details found. please check", TOAST_HEADER_LABELS.TOAST_WARNING));
                }

                this.showURL2 = (result[0].SessionType__c == 'PRIDE' && result[0].Medium__c == 'Online') ? true : false;
                this.showURL1 = (result[0].SessionType__c != 'PRIDE' && result[0].Medium__c == 'Online') ? true : false;
                this.showAddressFields = result[0].Medium__c == 'In-Person' ? true : false;
                this.showSessionNo = result[0].SessionType__c == 'PRIDE' ? true : false;
                this.showSessionBtn = result[0].SessionType__c == 'PRIDE' ? true : false;
                this.sessionBtnLabel = this.showSessionBtn === true ? 'UPDATE SESSION' : '';
                this.showMeetingInfoTable = result[0].SessionType__c == 'PRIDE' ? true : false;
                this.showDescription = this.showURL2 === false ? true : false;
                this.isDisableSessionType = result[0].SessionType__c == 'PRIDE' ? true : false;
                this.isDisableType = true;

                this.loadSessionType(result[0].Type__c);

                this.trainingDetails.Type__c = result[0].Type__c;
                this.trainingDetails.SessionType__c = result[0].SessionType__c;
                this.trainingDetails.Jurisdiction__c = result[0].Jurisdiction__c;
                this.trainingDetails.Medium__c = result[0].Medium__c;
                this.trainingDetails.Instructor1__c = result[0].Instructor1__c;
                this.trainingDetails.Instructor2__c = result[0].Instructor2__c;
                this.trainingDetails.URL__c = result[0].URL__c;
                this.trainingDetails.RoomNo__c = result[0].RoomNo__c;
                this.trainingDetails.AddressLine1__c = result[0].AddressLine1__c;
                this.trainingDetails.AddressLine2__c = result[0].AddressLine2__c;
                this.selectedState = result[0].State__c;
                this.selectedCity = result[0].City__c;
                this.selectedCounty = result[0].County__c;
                this.selectedZipCode = JSON.stringify(result[0].ZipCode__c);
                this.trainingDetails.Description__c = result[0].Description__c;
                this.trainingDetails.Date__c = result[0].Date__c;
                this.trainingDetails.StartTime__c = utils.formatTime24Hr(result[0].StartTime__c) + 'Z';
                this.trainingDetails.EndTime__c = utils.formatTime24Hr(result[0].EndTime__c) + 'Z';
                this.trainingDetails.Duration__c = result[0].Duration__c;
                this.trainingDetails.SessionNumber__c = result[0].SessionNumber__c;
                this.minTime = utils.formatTime24Hr(result[0].StartTime__c) + 'Z';

                if (result[0].SessionType__c == 'PRIDE')
                    this.handleMeetingInfoDetails();
                else
                    this.Spinner = false;
            })
            .catch(errors => {
                this.Spinner = false;
                this.dispatchEvent(utils.handleError(errors));
            })
    }

    handleMeetingInfoDetails() {
        getMeetingInfoDetails({
                selectedTrainingId: this.selectedTrainingId
            })
            .then(result => {
                if (result.length == 0) {
                    this.Spinner = false;
                    return this.dispatchEvent(utils.toastMessage("No PRIDE Training Details found. please check", TOAST_HEADER_LABELS.TOAST_WARNING));
                }
                let durationCalculation = 0;

                this.meetingInfoSessionDetailsArr = result.map(row => {
                    return {
                        SessionNumber__c: row.SessionNumber__c,
                        Date__c: utils.formatDate(row.Date__c),
                        StartTime__c: utils.formatTime12Hr(row.StartTime__c),
                        EndTime__c: utils.formatTime12Hr(row.EndTime__c),
                        dummyStartTime: utils.formatTime24Hr(row.StartTime__c) + 'Z',
                        dummyEndTime: utils.formatTime24Hr(row.EndTime__c) + 'Z',
                        Duration__c: row.Duration__c,
                        Id: row.Id
                    }
                });

                if (this.meetingInfoSessionDetailsArr.length > 0) {
                    this.showMeetingInfoTable = true;

                    this.meetingInfoSessionDetailsArr.forEach(row => {
                        this._selectSessionOptions.push({
                            label: row.SessionNumber__c,
                            value: row.SessionNumber__c
                        });

                        durationCalculation = parseInt(row.Duration__c.split(':')[0] * 60) + parseInt(row.Duration__c.split(':')[1]);
                        this.totalHrsInMinutes += durationCalculation;
                    });
                    this.Spinner = false;
                }
            })
            .catch(errors => {
                this.Spinner = false;
                this.dispatchEvent(utils.handleError(errors));
            })
    }

    //On Change method handled for Training Fields
    trainingOnChange(event) {
        this.isDisableActionBtn = false;

        if (event.target.name == 'Type') {
            this.loadSessionType(event.detail.value);
            if (event.detail.value == 'Pre-Service Training') {
                this.trainingDetails.SessionType__c = 'PRIDE';
                this.isDisableSessionType = true;
                this.showSessionNo = true;
                this.showSessionBtn = true;
                this.sessionBtnLabel = this.selectedTrainingId ? 'UPDATE SESSION' : '+ ADD SESSION';
                this.trainingDetails.SessionNumber__c = !this.selectedTrainingId ? 'New' : this.trainingDetails.SessionNumber__c;
                this.isDisableMedium = false;
                this.showMeetingInfoTable = this.meetingInfoSessionDetailsArr.length > 0 ? true : false;
                this.urlViewCondition();
            } else {
                this.trainingDetails.SessionType__c = null;
                this.isDisableSessionType = false;
                this.showSessionNo = false;
                this.showSessionBtn = false;
                this.sessionBtnLabel = null;
                this.trainingDetails.SessionNumber__c = null;
                this.isDisableMedium = true;
                this.trainingDetails.Medium__c = null;
                this.showURL1 = false;
                this.showURL2 = false;
                this.showMeetingInfoTable = false;
                this.showAddressFields = false;
                this.trainingDetails.RoomNo__c = null;
                this.trainingDetails.AddressLine1__c = null;
                this.selectedState = null;
                this.selectedCity = null;
                this.selectedCounty = null;
                this.selectedZipCode = null;
                this.showDescription = true;
            }
            this.trainingDetails.Type__c = event.detail.value;
        } else if (event.target.name == 'SessionType' && !this.isDisableSessionType) {
            this.trainingDetails.SessionType__c = event.detail.value;
            this.isDisableMedium = false;
            this.urlViewCondition();
        } else if (event.target.name == 'Jurisdiction') {
            this.trainingDetails.Jurisdiction__c = event.detail.value;
        } else if (event.target.name == 'Medium') {
            this.trainingDetails.Medium__c = event.detail.value;
            if (event.detail.value == 'Online') {
                this.urlViewCondition();
                this.showAddressFields = false;
                this.trainingDetails.RoomNo__c = null;
                this.trainingDetails.AddressLine1__c = null;
                this.selectedState = null;
                this.selectedCity = null;
                this.selectedCounty = null;
                this.selectedZipCode = null;
            } else {
                this.trainingDetails.URL__c = null;
                this.showAddressFields = true;
                this.showURL1 = false;
                this.showURL2 = false;
                this.showDescription = true;
            }
        } else if (event.target.name == 'URL1' || event.target.name == 'URL2') {
            this.trainingDetails.URL__c = event.target.value;
        } else if (event.target.name == 'RoomNo') {
            this.trainingDetails.RoomNo__c = event.target.value;
        } else if (event.target.name == 'State') {
            this.selectedState = event.target.value;
        } else if (event.target.name == 'City') {
            this.selectedCity = event.target.value;
        } else if (event.target.name == 'County') {
            this.selectedCounty = event.target.value;
        } else if (event.target.name == 'ZipCode') {
            this.selectedZipCode = event.target.value.replace(/(\D+)/g, "");
        } else if (event.target.name == 'Instructor1') {
            this.trainingDetails.Instructor1__c = event.target.value;
        } else if (event.target.name == 'Instructor2') {
            this.trainingDetails.Instructor2__c = event.target.value;
        } else if (event.target.name == 'SelectSession') {
            this.isDisableSessionBtn = false;
            this.trainingDetails.SessionNumber__c = event.detail.value;
            this.sessionBtnLabel = event.detail.value != 'New' ? 'UPDATE SESSION' : '+ ADD SESSION';
            this.isSessionModified = true;
            if (event.detail.value == 'New') {
                this.trainingDetails.Date__c = null;
                this.trainingDetails.StartTime__c = null;
                this.trainingDetails.EndTime__c = null;
                this.trainingDetails.Duration__c = null;
                this.isSessionAdded = false;
            } else {
                this.loadMeetingInfoDetails(event.detail.value);
            }
        } else if (event.target.name == 'Date') {
            this.trainingDetails.Date__c = event.target.value;
            if (this.isSessionAdded === true) {
                this.isDisableSessionBtn = false;
                this.isSessionModified = true;
                this.sessionBtnLabel = this.trainingDetails.SessionNumber__c != 'New' ? 'UPDATE SESSION' : '+ ADD SESSION';
            }
        } else if (event.target.name == 'StartTime') {
            this.trainingDetails.StartTime__c = event.target.value + 'Z';
            this.isDisableEndTime = false;
            this.minTime = event.target.value;
            this.trainingDetails.EndTime__c = null;
            this.trainingDetails.Duration__c = null;

            if (this.isSessionAdded === true) {
                this.isDisableSessionBtn = false;
                this.isSessionModified = true;
                this.sessionBtnLabel = this.trainingDetails.SessionNumber__c != 'New' ? 'UPDATE SESSION' : '+ ADD SESSION';
            }
        } else if (event.target.name == 'EndTime') {
            this.trainingDetails.EndTime__c = event.target.value + 'Z';

            if (this.trainingDetails.StartTime__c != null && this.trainingDetails.EndTime__c != null) {
                this.trainingDetails.Duration__c = utils.calTimeDifference(this.trainingDetails.StartTime__c, this.trainingDetails.EndTime__c);
            }

            if (this.isSessionAdded === true) {
                this.isDisableSessionBtn = false;
                this.isSessionModified = true;
                this.sessionBtnLabel = this.trainingDetails.SessionNumber__c != 'New' ? 'UPDATE SESSION' : '+ ADD SESSION';
            }
        } else if (event.target.name == 'Description1' || event.target.name == 'Description2') {
            this.trainingDetails.Description__c = event.target.value;
        }
    }

    //Loading Session Type based on Type Selection
    loadSessionType(type) {
        let typeArray = [];

        if (type != 'Pre-Service Training') {
            this.sessionTypeOnLoadArray.forEach(row => {
                if (row.value != 'PRIDE') {
                    typeArray.push({
                        label: row.label,
                        value: row.value
                    });
                }
            });
        } else {
            this.sessionTypeOnLoadArray.forEach(row => {
                typeArray.push({
                    label: row.label,
                    value: row.value
                });
            });
        }
        this._sessionTypeOptions = typeArray;
    }

    //Function used to display URL column when condition met
    urlViewCondition() {
        if (this.trainingDetails && this.trainingDetails.Medium__c == 'Online') {
            this.showURL1 = this.trainingDetails.SessionType__c != 'PRIDE' ? true : false;
            this.showURL2 = this.trainingDetails.SessionType__c == 'PRIDE' ? true : false;
            this.showMeetingInfoTable = this.trainingDetails.SessionType__c == 'PRIDE' && this.meetingInfoSessionDetailsArr.length > 0 ? true : false;
            this.showDescription = this.trainingDetails.SessionType__c == 'PRIDE' ? false : true;
        } else {
            this.showURL1 = false;
            this.showURL2 = false;
            this.showMeetingInfoTable = false;
            this.showDescription = true;
        }
    }

    //Session On Change - Load existing updated Session Details
    loadMeetingInfoDetails(value) {
        this.meetingInfoSessionDetailsArr.forEach(row => {
            if (row.SessionNumber__c == value) {
                this.trainingDetails.SessionNumber__c = row.SessionNumber__c;
                this.trainingDetails.Date__c = utils.formatDateYYYYMMDD(row.Date__c);
                this.trainingDetails.StartTime__c = row.dummyStartTime;
                this.trainingDetails.EndTime__c = row.dummyEndTime;
                this.trainingDetails.Duration__c = row.Duration__c;
            }
        });
    }

    //getStreetAddress from Google API when user manually input
    getStreetAddress(event) {
        this.trainingDetails.AddressLine1__c = event.target.value;
        let streetSearchText = event.target.value;

        if (!event.target.value) {
            this.selectedState = "";
            this.selectedCity = "";
            this.selectedCounty = "";
            this.selectedZipCode = "";
        } else {
            getAPIStreetAddress({
                    input: streetSearchText
                })
                .then(result => {
                    this.strStreetAddresPrediction = JSON.parse(result);
                    this.active = this.strStreetAddresPrediction.predictions.length > 0 ? true : false;
                })
                .catch(error => {
                    this.dispatchEvent(utils.toastMessage("Error in Fetching Google API Street Address", TOAST_HEADER_LABELS.TOAST_WARNING));
                })
        }
    }

    //Function used to select Google API Address
    selectStreetAddress(event) {
        this.trainingDetails.AddressLine1__c = event.target.dataset.description;

        getFullDetails({
                placeId: event.target.dataset.id
            })
            .then(result => {
                var main = JSON.parse(result);

                //Sundar Will this work ?
                try {
                    var cityTextbox = this.template.querySelector(".dummyCity");
                    this.selectedCity = main.result.address_components[2].long_name;

                    var countyComboBox = this.template.querySelector(".dummyCounty");
                    this.selectedCounty = main.result.address_components[4].long_name;

                    var stateComboBox = this.template.querySelector(".dummyState");
                    this.selectedState = main.result.address_components[5].long_name;

                    var ZipCodeTextbox = this.template.querySelector(".dummyZipCode");
                    this.selectedZipCode = main.result.address_components[7].long_name;
                } catch (ex) {}

                this.active = false;
            })
            .catch(error => {
                this.dispatchEvent(utils.toastMessage("Error in Selected Address. please check", TOAST_HEADER_LABELS.TOAST_WARNING));
            });
    }

    //Function used to capture on change states in an array
    stateOnChange(event) {
        let stateSearchTxt = event.target.value;

        if (!stateSearchTxt)
            this.showStateBox = false;
        else {
            this.filteredStateArr = this.stateArr.filter(val => val.Value.indexOf(stateSearchTxt) > -1).map(sresult => {
                return sresult;
            });

            this.showStateBox = this.filteredStateArr.length > 0 ? true : false;
        }
    }

    //Function used to set selected state
    setSelectedState(event) {
        this.selectedState = event.currentTarget.dataset.value;
        this.showStateBox = false;
    }

    //Function used to select the county from CountyArr
    countyOnChange(event) {
        let countySearchTxt = event.target.value;

        if (!countySearchTxt)
            this.showCountyBox = false;
        else {
            this.filteredCountyArr = this.countyArr.filter(val => val.value.indexOf(countySearchTxt) > -1).map(cresult => {
                return cresult;
            });
            this.showCountyBox = this.filteredCountyArr.length > 0 ? true : false;
        }
    }

    //Function used to set selected city
    setSelectedCounty(event) {
        this.selectedCounty = event.currentTarget.dataset.value;
        this.showCountyBox = false;
    }

    //Session On Click method
    handleSession() {
        let durationCalculation = 0;
        this.isSessionModified = false;

        if (this.sessionBtnLabel == '+ ADD SESSION' && this._selectSessionOptions[0].label == 'New') {
            if (this.trainingDetails && !this.trainingDetails.Date__c)
                return this.dispatchEvent(utils.toastMessage('Date is mandatory', TOAST_HEADER_LABELS.TOAST_WARNING));
            if (!this.selectedTrainingId && this.trainingDetails && this.trainingDetails.Date__c && (new Date(this.trainingDetails.Date__c).setHours(0, 0, 0, 0) < this.today))
                return this.dispatchEvent(utils.toastMessage('Date must be more than current date', TOAST_HEADER_LABELS.TOAST_WARNING));
            if (this.trainingDetails && !this.trainingDetails.StartTime__c)
                return this.dispatchEvent(utils.toastMessage('Start Time is mandatory', TOAST_HEADER_LABELS.TOAST_WARNING));
            if (this.trainingDetails && !this.trainingDetails.EndTime__c)
                return this.dispatchEvent(utils.toastMessage('End Time is mandatory', TOAST_HEADER_LABELS.TOAST_WARNING));
            if (this.trainingDetails && (this.trainingDetails.StartTime__c == this.trainingDetails.EndTime__c))
                return this.dispatchEvent(utils.toastMessage('Session Start Time and End Time should not be same', TOAST_HEADER_LABELS.TOAST_WARNING));
            if (this.trainingDetails && !this.trainingDetails.Duration__c)
                return this.dispatchEvent(utils.toastMessage('Duration is mandatory', TOAST_HEADER_LABELS.TOAST_WARNING));

            this.isDisableSessionBtn = true;
            this.isSessionAdded = true;
            if (this.selectedTrainingId) {
                let sessionNoArray = [];
                this.meetingInfoSessionDetailsArr.forEach(row => {
                    sessionNoArray.push(row.SessionNumber__c);
                });
                this._number = Number(sessionNoArray[sessionNoArray.length - 1].split('Session ')[1]) + 1;
            } else {
                this._number = this._number + 1;
            }

            this.sessionNumberStr = 'Session ' + this._number.toString();
            if (this.trainingDetails.Duration__c != null) {
                durationCalculation = parseInt(this.trainingDetails.Duration__c.split(':')[0] * 60) + parseInt(this.trainingDetails.Duration__c.split(':')[1]);
                this.totalHrsInMinutes += durationCalculation;
            }

            this.meetingInfoSessionDetailsArr.push({
                SessionNumber__c: this.sessionNumberStr,
                Date__c: utils.formatDate(this.trainingDetails.Date__c),
                StartTime__c: utils.sessionTableTimeFormat(this.trainingDetails.StartTime__c),
                EndTime__c: utils.sessionTableTimeFormat(this.trainingDetails.EndTime__c),
                Duration__c: this.trainingDetails.Duration__c,
                dummyStartTime: this.trainingDetails.StartTime__c,
                dummyEndTime: this.trainingDetails.EndTime__c,
                totalHrs: durationCalculation
            });

            if (this.meetingInfoSessionDetailsArr.length > 0) {
                this.showMeetingInfoTable = true;
                this.trainingDetails.SessionNumber__c = this.sessionNumberStr;

                this._selectSessionOptions.push({
                    label: this.sessionNumberStr,
                    value: this.sessionNumberStr
                });

                this.dispatchEvent(utils.toastMessage(`${this.trainingDetails.SessionNumber__c} has been added successfully`, TOAST_HEADER_LABELS.TOAST_SUCCESS));
            }
        } else {
            if (this.trainingDetails && !this.trainingDetails.Date__c)
                return this.dispatchEvent(utils.toastMessage('Date is mandatory', TOAST_HEADER_LABELS.TOAST_WARNING));
            if (!this.selectedTrainingId && this.trainingDetails && this.trainingDetails.Date__c && (new Date(this.trainingDetails.Date__c).setHours(0, 0, 0, 0) < this.today))
                return this.dispatchEvent(utils.toastMessage('Date must be more than current date', TOAST_HEADER_LABELS.TOAST_WARNING));
            if (this.trainingDetails && !this.trainingDetails.StartTime__c)
                return this.dispatchEvent(utils.toastMessage('Start Time is mandatory', TOAST_HEADER_LABELS.TOAST_WARNING));
            if (this.trainingDetails && !this.trainingDetails.EndTime__c)
                return this.dispatchEvent(utils.toastMessage('End Time is mandatory', TOAST_HEADER_LABELS.TOAST_WARNING));
            if (this.trainingDetails && (this.trainingDetails.StartTime__c == this.trainingDetails.EndTime__c))
                return this.dispatchEvent(utils.toastMessage('Session Start Time and End Time should not be same', TOAST_HEADER_LABELS.TOAST_WARNING));
            if (this.trainingDetails && !this.trainingDetails.Duration__c)
                return this.dispatchEvent(utils.toastMessage('Duration is mandatory', TOAST_HEADER_LABELS.TOAST_WARNING));

            this.isSessionAdded = false;
            this.isDisableSessionBtn = false;

            let itemFound = false;

            for (var i = 0; i < this.meetingInfoSessionDetailsArr.length; i++) {
                if (this.meetingInfoSessionDetailsArr[i].SessionNumber__c == this.trainingDetails.SessionNumber__c) {
                    this.totalHrsInMinutes = this.meetingInfoSessionDetailsArr[i].Duration__c != this.trainingDetails.Duration__c ? ((parseInt(this.totalHrsInMinutes) - parseInt(this.meetingInfoSessionDetailsArr[i].totalHrs)) + (parseInt(this.trainingDetails.Duration__c.split(':')[0] * 60) + parseInt(this.trainingDetails.Duration__c.split(':')[1]))) : this.totalHrsInMinutes;
                    this.meetingInfoSessionDetailsArr[i].Date__c = utils.formatDate(this.trainingDetails.Date__c);
                    this.meetingInfoSessionDetailsArr[i].StartTime__c = utils.sessionTableTimeFormat(this.trainingDetails.StartTime__c);
                    this.meetingInfoSessionDetailsArr[i].EndTime__c = utils.sessionTableTimeFormat(this.trainingDetails.EndTime__c);
                    this.meetingInfoSessionDetailsArr[i].Duration__c = this.trainingDetails.Duration__c;
                    this.meetingInfoSessionDetailsArr[i].dummyStartTime = this.trainingDetails.StartTime__c;
                    this.meetingInfoSessionDetailsArr[i].dummyEndTime = this.trainingDetails.EndTime__c;
                    itemFound = true;
                    break;
                }
            }

            if (itemFound)
                this.dispatchEvent(utils.toastMessage(`${this.trainingDetails.SessionNumber__c} has been updated successfully`, TOAST_HEADER_LABELS.TOAST_SUCCESS));
        }

        this.showMeetingInfoTable = this.meetingInfoSessionDetailsArr.length > 0 ? true : false;
    }

    //Handle Training Save method
    saveTrainingDetails() {
        if (!this.selectedTrainingId && this.trainingDetails && (new Date(this.trainingDetails.Date__c).setHours(0, 0, 0, 0) < this.today))
            return this.dispatchEvent(utils.toastMessage("Date must be more than current date", TOAST_HEADER_LABELS.TOAST_WARNING));

        const allValid = [
                ...this.template.querySelectorAll('lightning-input'),
                ...this.template.querySelectorAll('lightning-combobox')
            ]
            .reduce((validSoFar, inputFields) => {
                inputFields.reportValidity();
                return validSoFar && inputFields.checkValidity();
            }, true);

        if (allValid) {
            if (!this.selectedTrainingId && !this._recordTypeId)
                return this.dispatchEvent(utils.toastMessage("Training Record Type failed to load. please check", TOAST_HEADER_LABELS.TOAST_WARNING));

            if (this.trainingDetails && this.trainingDetails.Instructor2__c && (this.trainingDetails.Instructor1__c == this.trainingDetails.Instructor2__c))
                return this.dispatchEvent(utils.toastMessage("Instructor1 and Instructor2 should not be same", TOAST_HEADER_LABELS.TOAST_WARNING));

            if (this.selectedZipCode != null && (((/^\d{1,5}$/).test(this.selectedZipCode) === false) || this.selectedZipCode.length != 5))
                return this.dispatchEvent(utils.toastMessage("Invalid ZipCode", TOAST_HEADER_LABELS.TOAST_WARNING));

            if (this.trainingDetails && this.trainingDetails.StartTime__c == this.trainingDetails.EndTime__c)
                return this.dispatchEvent(utils.toastMessage("Start Time and End Time should not be same", TOAST_HEADER_LABELS.TOAST_WARNING));

            if (this.trainingDetails && !this.trainingDetails.Duration__c)
                return this.dispatchEvent(utils.toastMessage("Duration is mandatory", TOAST_HEADER_LABELS.TOAST_WARNING));

            if (this.meetingInfoSessionDetailsArr.length == 0 && this.trainingDetails.SessionType__c == 'PRIDE')
                return this.dispatchEvent(utils.toastMessage("Sessions must be added to PRIDE session type.", TOAST_HEADER_LABELS.TOAST_WARNING));

            if (this.trainingDetails.SessionType__c == 'PRIDE' && Number(this.totalHrsInMinutes) < 1620)
                return this.dispatchEvent(utils.toastMessage("PRIDE Sessions must be added to 27 hrs", TOAST_HEADER_LABELS.TOAST_WARNING));

            if (this.isSessionModified === true)
                return this.dispatchEvent(utils.toastMessage("Modified Session is not updated. please check", TOAST_HEADER_LABELS.TOAST_WARNING));

            this.isDisableActionBtn = true;
            this.Spinner = true;

            const fields = {};

            fields[RECORDTYPEID_FIELD.fieldApiName] = this._recordTypeId;
            fields[TYPE_FIELD.fieldApiName] = this.trainingDetails.Type__c;
            fields[SESSIONTYPE_FIELD.fieldApiName] = this.trainingDetails.SessionType__c;
            fields[JURISDICTION_FIELD.fieldApiName] = this.trainingDetails.Jurisdiction__c;
            fields[MEDIUM_FIELD.fieldApiName] = this.trainingDetails.Medium__c;
            fields[URL_FIELD.fieldApiName] = this.trainingDetails.URL__c;
            fields[ROOMNO_FIELD.fieldApiName] = this.trainingDetails.RoomNo__c;
            fields[ADDRESSLINE1_FIELD.fieldApiName] = this.trainingDetails.AddressLine1__c;
            fields[STATE_FIELD.fieldApiName] = this.selectedState;
            fields[CITY_FIELD.fieldApiName] = this.selectedCity;
            fields[COUNTY_FIELD.fieldApiName] = this.selectedCounty;
            fields[ZIPCODE_FIELD.fieldApiName] = this.selectedZipCode;
            fields[INSTRUCTOR1_FIELD.fieldApiName] = this.trainingDetails.Instructor1__c;
            fields[INSTRUCTOR2_FIELD.fieldApiName] = this.trainingDetails.Instructor2__c;
            fields[SESSIONNUMBER_FIELD.fieldApiName] = this.trainingDetails.SessionNumber__c;
            fields[DATE_FIELD.fieldApiName] = this.trainingDetails.Date__c;
            fields[STARTTIME_FIELD.fieldApiName] = this.trainingDetails.StartTime__c;
            fields[ENDTIME_FIELD.fieldApiName] = this.trainingDetails.EndTime__c;
            fields[DURATION_FIELD.fieldApiName] = this.trainingDetails.Duration__c;
            fields[TOPICDESCRIPTION_FIELD.fieldApiName] = this.trainingDetails.Description__c;

            this.finalStatus = this.selectedTrainingId ? 'updated' : 'added';

            if (!this.selectedTrainingId) {
                fields[STATUS_FIELD.fieldApiName] = 'Scheduled';

                const recordInput = {
                    apiName: TRAINING_OBJECT.objectApiName,
                    fields
                };

                createRecord(recordInput)
                    .then(result => {
                        if (this.showMeetingInfoTable)
                            setTimeout(() => {
                                this.handleMeetingInfoSessions(this.meetingInfoSessionDetailsArr, result.id)
                            }, 1000);
                        else {
                            setTimeout(() => {
                                this.closeTrainingModal()
                            }, 1000);

                            this.dispatchEvent(utils.toastMessage("Training details has been added successfully", TOAST_HEADER_LABELS.TOAST_SUCCESS));
                        }
                    })
                    .catch(error => {
                        this.error = error;
                        this.isDisableActionBtn = false;
                        this.Spinner = false;
                        this.dispatchEvent(utils.toastMessage("Error in adding Training details. Please check.", TOAST_HEADER_LABELS.TOAST_WARNING));
                    })
            } else {
                fields[ID_FIELD.fieldApiName] = this.selectedTrainingId;

                const recordInput = {
                    fields
                };

                updateRecord(recordInput)
                    .then(() => {
                        if (this.showMeetingInfoTable)
                            setTimeout(() => {
                                this.handleMeetingInfoSessions(this.meetingInfoSessionDetailsArr, this.selectedTrainingId)
                            }, 1000);
                        else {
                            setTimeout(() => {
                                this.closeTrainingModal()
                            }, 1000);

                            this.dispatchEvent(utils.toastMessage("Training details has been updated successfully", TOAST_HEADER_LABELS.TOAST_SUCCESS));
                        }
                    })
                    .catch(error => {
                        this.error = error;
                        this.Spinner = false;
                        this.isDisableActionBtn = false;

                        this.dispatchEvent(utils.toastMessage("Error in updating Training Details. Please check.", TOAST_HEADER_LABELS.TOAST_WARNING));
                    })
            }
        } else {
            this.isDisableActionBtn = false;
            this.dispatchEvent(utils.toastMessage("Please fill mandatory fields", TOAST_HEADER_LABELS.TOAST_WARNING));
        }
    }

    //Handle Meeting Info method for PRIDE session
    handleMeetingInfoSessions(meetingInfoDetails, trainingId) {
        let finalArray = [];

        meetingInfoDetails.forEach(row => {
            finalArray.push({
                SessionNumber__c: row.SessionNumber__c,
                Date__c: utils.formatDateYYYYMMDD(row.Date__c),
                StartTime__c: row.dummyStartTime,
                EndTime__c: row.dummyEndTime,
                Duration__c: row.Duration__c,
                MeetingStatus__c: 'Scheduled',
                Id: row.Id
            });
        });

        insertOrUpdateMeetingInfoDetails({
                meetingInfoDetails: JSON.stringify(finalArray),
                trainingId: trainingId
            })
            .then(result => {
                this.Spinner = false;

                setTimeout(() => {
                    this.closeTrainingModal()
                }, 1000);

                this.dispatchEvent(utils.toastMessage(`Training details has been ${this.finalStatus} successfully`, TOAST_HEADER_LABELS.TOAST_SUCCESS));
            })
            .catch(errors => {
                this.Spinner = false;
                this.isDisableActionBtn = false;
                this.dispatchEvent(utils.toastMessage("Error in inserting Meeting Info Sessions. Please check.", TOAST_HEADER_LABELS.TOAST_WARNING));
            })
    }

    //Close Training Model Method
    closeTrainingModal() {
        this.Spinner = true;
        this.decideClose = false;
        this.viewTrainingModel = false;
        this.trainingDetails = {};
        this.isDisableType = false;
        this.isDisableSessionType = false;
        this.isDisableActionBtn = false;
        this.showAddressFields = false;
        this.showSessionNo = false;
        this.showSessionBtn = false;
        this.showURL1 = false;
        this.showURL2 = false;
        this.showMeetingInfoTable = false;
        this.meetingInfoSessionDetailsArr = [];
        this.showDescription = true;
        this.selectedTrainingId = null;
        this._selectSessionOptions = [{
            label: 'New',
            value: 'New'
        }];

        this.minDate = '';
        this.totalHrsInMinutes = 0;
        this.dispatchCustomEventToHandleAddTraingPopUp();
    }

    //Custom Event calling for closing the Training Modal
    dispatchCustomEventToHandleAddTraingPopUp() {
        setTimeout(() => {
            const tooglecmps = new CustomEvent('togglecomponents', {
                detail: {
                    Component: 'CloseTraining'
                }
            });
            this.dispatchEvent(tooglecmps);
            fireEvent(this.pageRef, "RefreshPublicProviderTrainingDashBoard", true);
            this.Spinner = false;
        }, 1000);
    }
}