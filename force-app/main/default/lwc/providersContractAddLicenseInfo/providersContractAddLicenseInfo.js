/**
 * @Author        : G.Tamilarasan
 * @CreatedOn     : April 16, 2020
 * @Purpose       : This componentes contains New Contract creation
 **/

import {
    LightningElement,
    track,
    wire
} from 'lwc';
import getContractDetails from '@salesforce/apex/ProvidersContractAddLicenseInfo.getContractDetails';
import getContractBedDetails from '@salesforce/apex/ProvidersContractAddLicenseInfo.getContractBedDetails';
import InsertUpdateContract from '@salesforce/apex/ProvidersContractAddLicenseInfo.InsertUpdateContract';
import { CJAMS_CONSTANTS } from "c/constants";
import * as shareddata from 'c/sharedData';
import {
    utils
} from "c/utils";
import Contract__c_OBJECT from '@salesforce/schema/Contract__c';
import {
    getPicklistValuesByRecordType
} from 'lightning/uiObjectInfoApi';
import {
    getObjectInfo
} from 'lightning/uiObjectInfoApi';

export default class ProvidersContractAddLicenseInfo extends LightningElement {

    @track addoreditContract = {
        CreatedDate: '',
        ReceivedDate__c: '',
        Name: '',
        LicensingAgency__c: '',
        ContractingAgency__c: '',
        SolicitationType__c: '',
        ContractStatus__c: '',
        ContractType__c: '',
        TotalContractBed__c: '',
        ContractTermType__c: '',
        ContractStartDate__c: '',
        ContractEndDate__c: '',
        TotalContractAmount__c: '',
        BudgetCodeType__c: '',
        BudgetCode__c: '',
        Notes__c: '',
        FiscalAmount__c: ''
    };
    @track SolicitationTypeOptions;
    @track ContractStatusOptions;
    @track ContractTypeOptions;
    @track ContractTermTypeOptions;
    @track BudgetCodeTypeOptions;
    @track BudgetCodeOptions;
    @track baseListShow = false;
    @track fiscalYearArray = [];
    @track fiscalYear;
    @track fiscalYearStartDate;
    @track fiscalYearEndDate;
    @track licenceStartDate;
    @track licenceEndDate;
    @track mindatecontract;
    @track maxdatecontract;
    @track ContractId;
    @track bedCapacityHint;
    @track bedCapacityCount;
    @track isDisabledEdit = false;
    @track LicenseID;
    @track contractLabel = "Add Contract";
    @track maxdate;

    //This function used to get the Provider ID dynamically
    get providerId() {
        let data = shareddata.getProviderId();
        return data;
    }

    //This function used to get the action of the contraction whether it is Edit or View
    get contractAction() {
        let data = shareddata.getContractAction();
        return data;
    }

    //Function used to disable the future date in received date field
    futuredatedisable() {
        var today = new Date();
        var monthDigit = today.getMonth() + 1;
        if (monthDigit <= 9) {
            monthDigit = '0' + monthDigit;
        }
        var dayDigit = today.getDate();
        if (dayDigit <= 9) {
            dayDigit = '0' + dayDigit;
        }

        var date = today.getFullYear() + "-" + monthDigit + "-" + dayDigit;
        var finaldate = date;
        this.maxdate = finaldate;
    }


    //This function used to show the records while DOM load
    connectedCallback() {
        if (this.contractAction == 'editRecordRow' || this.contractAction == 'viewRecordRow') {
            this.ContractId = shareddata.getContractId();
            this.LicenseID = shareddata.getLicenseId();
            this.contractDetailsLoad();
            this.BedCountLoad();
            this.contractLabel = "Edit Contract";
            if (this.contractAction == 'viewRecordRow') {
                this.isDisabledEdit = true;
                this.contractLabel = "View Contract";
            } else {
                this.isDisabledEdit = false;
            }
        } else if (this.contractAction == undefined) {
            this.isDisabledEdit = true;
        }
        this.futuredatedisable();
    }

    //This function used to get the contract details from Contract object
    contractDetailsLoad() {
        getContractDetails({
            contractdetails: this.ContractId
        }).then(result => {
            this.addoreditContract = result[0];
            if (!this.addoreditContract.ReceivedDate__c) {
                if (this.addoreditContract.CreatedDate != undefined) {
                    const target = {
                        "ReceivedDate__c": this.addoreditContract.CreatedDate
                    };
                    const returnedTarget = Object.assign(target, this.addoreditContract);
                    this.addoreditContract = returnedTarget;
                }
            }
            if (!this.addoreditContract.LicensingAgency__c) {
                this.addoreditContract.LicensingAgency__c = 'OLM';
            }
            if (!this.addoreditContract.ContractingAgency__c) {
                this.addoreditContract.ContractingAgency__c = 'OLM';
            }
            if (this.addoreditContract.ContractStatus__c = "New") {
                this.addoreditContract.ContractStatus__c = "In process";
            }
            if (!this.addoreditContract.ContractStartDate__c) {
                this.addoreditContract.ContractStartDate__c = this.licenceStartDate;
            } else {
                this.mindatecontract = this.addoreditContract.ContractStartDate__c;
                this.maxdatecontract = this.addoreditContract.ContractEndDate__c;
            }
            if (!this.addoreditContract.ContractEndDate__c) {
                this.addoreditContract.ContractEndDate__c = this.licenceEndDate;
            } else {
                this.mindatecontract = this.addoreditContract.ContractStartDate__c;
                this.maxdatecontract = this.addoreditContract.ContractEndDate__c;
            }
            if (this.addoreditContract.ContractTermType__c != undefined) {
                if (this.addoreditContract.ContractTermType__c == "Base") {
                    this.baseListShow = true;
                    this.fiscalYearStartDate = this.addoreditContract.ContractStartDate__c;
                    this.fiscalYearEndDate = this.addoreditContract.ContractEndDate__c;
                    this.fiscalYearCalculation();
                } else {
                    this.baseListShow = false;
                }
            }
        }).catch(errors => {
            let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
            return this.dispatchEvent(utils.handleError(errors, config));
        });
    }

    //This function used to get the input values while change action and store it to single variable
    handleChange(event) {
        if (event.target.name == "EntryDate") {
            this.addoreditContract.CreatedDate = event.target.value;
        } else if (event.target.name == "ReceivedDate__c") {
            this.addoreditContract.ReceivedDate__c = event.target.value;
        } else if (event.target.name == "Name") {
            this.addoreditContract.Name = event.target.value;
        } else if (event.target.name == "LicensingAgency__c") {
            this.addoreditContract.LicensingAgency__c = event.target.value;
        } else if (event.target.name == "ContractingAgency__c") {
            this.addoreditContract.ContractingAgency__c = event.target.value;
        } else if (event.target.name == "SolicitationType__c") {
            this.addoreditContract.SolicitationType__c = event.target.value;
        } else if (event.target.name == "ContractStatus__c") {
            this.addoreditContract.ContractStatus__c = event.target.value;
        } else if (event.target.name == "ContractType__c") {
            this.addoreditContract.ContractType__c = event.target.value;
        } else if (event.target.name == "TotalContractBed__c") {
            this.addoreditContract.TotalContractBed__c = event.target.value;
        } else if (event.target.name == "ContractTermType__c") {
            this.addoreditContract.ContractTermType__c = event.target.value;
            if (event.target.value == "Base") {
                this.fiscalYearStartDate = this.addoreditContract.ContractStartDate__c;
                this.fiscalYearEndDate = this.addoreditContract.ContractEndDate__c;
                this.fiscalYearCalculation();
                this.baseListShow = true;
            } else {
                this.baseListShow = false;
            }
        } else if (event.target.name == "ContractStartDate__c") {
            this.addoreditContract.ContractStartDate__c = event.target.value;
            this.fiscalYearStartDate = event.target.value;
            this.fiscalYearCalculation();
        } else if (event.target.name == "ContractEndDate__c") {
            this.addoreditContract.ContractEndDate__c = event.target.value;
            this.fiscalYearEndDate = event.target.value;
            this.fiscalYearCalculation();
        } else if (event.target.name == "TotalContractAmount__c") {
            this.addoreditContract.TotalContractAmount__c = Number(event.target.value);
        } else if (event.target.name == "BudgetCodeType__c") {
            this.addoreditContract.BudgetCodeType__c = event.target.value;
        } else if (event.target.name == "BudgetCode__c") {
            this.addoreditContract.BudgetCode__c = event.target.value;
        } else if (event.target.name == "Notes__c") {
            this.addoreditContract.Notes__c = event.target.value;
        }
    }

    //This function used to validate the required field and save the records to Contract Object
    saveMethod() {
        this.addoreditContract.FiscalAmount__c = "";
        let fiscalamount = this.template.querySelectorAll(".fiscalamountclass"); //get the Fiscal Amount input values
        for (let i = 0; i < fiscalamount.length; i++) {
            this.addoreditContract.FiscalAmount__c += fiscalamount[i].value + ";";
        }
        this.addoreditContract.FiscalAmount__c = this.addoreditContract.FiscalAmount__c.substr(0, this.addoreditContract.FiscalAmount__c.length - 1);

        if (!this.addoreditContract.Name) {
            return this.dispatchEvent(utils.toastMessage("Please enter a Contract Number", "warning"));
        }
        if (!this.addoreditContract.LicensingAgency__c) {
            return this.dispatchEvent(utils.toastMessage("Please enter a Licensing Agency", "warning"));
        }
        if (!this.addoreditContract.ContractingAgency__c) {
            return this.dispatchEvent(utils.toastMessage("Please enter a Contracting Agency", "warning"));
        }
        if (!this.addoreditContract.SolicitationType__c) {
            return this.dispatchEvent(utils.toastMessage("Please enter a Solicitation Type", "warning"));
        }
        if (!this.addoreditContract.ContractStatus__c) {
            return this.dispatchEvent(utils.toastMessage("Please enter a Contract Status", "warning"));
        }
        if (!this.addoreditContract.ContractType__c) {
            return this.dispatchEvent(utils.toastMessage("Please enter a Contract Type", "warning"));
        }
        if (!this.addoreditContract.TotalContractBed__c) {
            return this.dispatchEvent(utils.toastMessage("Please enter a Total Contract Beds", "warning"));
        }
        if (!this.addoreditContract.ContractStartDate__c) {
            return this.dispatchEvent(utils.toastMessage("Please enter a Contract Start Date", "warning"));
        }
        if (!this.addoreditContract.ContractEndDate__c) {
            return this.dispatchEvent(utils.toastMessage("Please enter a Contract End Date", "error"));
        }

        //Sundar Added this line of code
        if (!this.addoreditContract.TotalContractAmount__c)
            return this.dispatchEvent(utils.toastMessage("Total Contract Amount is mandatory", "warning"));

        if (this.addoreditContract.TotalContractBed__c != undefined) {
            if (this.bedCapacityCount != undefined) {
                if (this.addoreditContract.TotalContractBed__c > this.bedCapacityCount) {
                    return this.dispatchEvent(utils.toastMessage("Total Contract Beds Should not exceed IRC Bed Capacity = " + this.bedCapacityCount, "error"));
                } else {
                    this.addoreditContract.sobjectType = 'Contract__c';
                    this.addoreditContract.License__c = this.LicenseID;
                    this.addoreditContract.Provider__c = this.providerId;
                    this.addoreditContract.Id = this.ContractId;
                    //Save the records to Contract Object
                    InsertUpdateContract({
                        objSobjecttoUpdateOrInsert: this.addoreditContract
                    }).then(result => {
                        this.dispatchEvent(utils.toastMessage("Record Saved Successfully", "Success"));
                        this.RedirecttoContractDashboard();
                    }).catch(errors => {
                        let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                        return this.dispatchEvent(utils.handleError(errors, config));
                    });
                }
            } else {
                return this.dispatchEvent(utils.toastMessage("Please provide IRC Bed Capacity", "error"));
            }
        }

    }

    //This function used to calculate the Financial Year (1st July to 30th June)
    fiscalYearCalculation() {
        let fiscalYearText = "FY-";
        let fiscalStartDateText = "-07-01";
        let fiscalEndEndText = "-06-30";

        let startdatesplit = this.fiscalYearStartDate.split("-");
        let startdatesplitobject = {
            startYear: startdatesplit[0],
            startMonth: startdatesplit[1],
            startDate: startdatesplit[2]
        };
        let enddatesplit = this.fiscalYearEndDate.split("-");
        let enddatesplitobject = {
            endYear: enddatesplit[0],
            endMonth: enddatesplit[1],
            endDate: enddatesplit[2]
        };

        let tmparray = [];
        if (startdatesplitobject.startMonth <= 6) {
            tmparray.push({
                fiscalYear: fiscalYearText + startdatesplitobject.startYear,
                fiscalStartDate: this.fiscalYearStartDate,
                fiscalEndDate: startdatesplitobject.startYear + fiscalEndEndText,
                fiscalAmount: ""
            });
            for (var i = startdatesplitobject.startYear; i < enddatesplitobject.endYear; i++) {
                let val = Number(i) + 1;
                tmparray.push({
                    fiscalYear: fiscalYearText + i,
                    fiscalStartDate: i + fiscalStartDateText,
                    fiscalEndDate: val + fiscalEndEndText,
                    fiscalAmount: ""
                })
            }
        } else {
            let value = Number(this.fiscalYearStartDate) + 1;
            tmparray.push({
                fiscalYear: fiscalYearText + startdatesplitobject.startYear,
                fiscalStartDate: this.fiscalYearStartDate,
                fiscalEndDate: value + fiscalEndEndText,
                fiscalAmount: ""
            });
            for (var i = value; i < enddatesplitobject.endYear; i++) {
                let val = Number(i) + 1;
                tmparray.push({
                    fiscalYear: fiscalYearText + i,
                    fiscalStartDate: i + fiscalStartDateText,
                    fiscalEndDate: val + fiscalEndEndText,
                    fiscalAmount: ""
                })
            }
        }
        if (enddatesplitobject.endMonth <= 6) {
            tmparray[tmparray.length - 1].fiscalEndDate = this.fiscalYearEndDate;
        } else {
            tmparray.push({
                fiscalYear: fiscalYearText + enddatesplitobject.endYear,
                fiscalStartDate: enddatesplitobject.endYear + fiscalStartDateText,
                fiscalEndDate: this.fiscalYearEndDate,
                fiscalAmount: ""
            });
        }

        if (this.addoreditContract.FiscalAmount__c != undefined) {
            let fiscalamountsplitfromreceived = this.addoreditContract.FiscalAmount__c;
            fiscalamountsplitfromreceived = fiscalamountsplitfromreceived.split(";");
            for (let i = 0; i < tmparray.length; i++) {
                tmparray[i].fiscalAmount = fiscalamountsplitfromreceived[i];
            }
        }
        this.fiscalYearArray = tmparray;
    }

    //This function used to get the IRC beds count
    BedCountLoad() {
        getContractBedDetails({
            licenseID: this.LicenseID
        }).then(result => {
            if (result.length != 0) {
                this.bedCapacityCount = result[0].IRCBedCapacity__c;
                this.bedCapacityHint = "Should not exceed IRC Bed Capacity = " + result[0].IRCBedCapacity__c;
            } else {
                return this.dispatchEvent(utils.toastMessage("Please provide IRC Bed Capacity", "error"));
            }
        }).catch(errors => {
            let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
            return this.dispatchEvent(utils.handleError(errors, config));
        });
    }

    //This function used to redirect back to contract dashboard
    RedirecttoContractDashboard() {
        const onredirecttocontract = new CustomEvent('redirecttocontractdashboard', {
            detail: {
                first: true
            }
        });
        this.dispatchEvent(onredirecttocontract);
    }

    //This function used to get the value from Child Component
    hanldeProgressValueChange(event) {
        this.licenceStartDate = event.detail.LicenseStartDate;
        this.licenceEndDate = event.detail.LicenseEndDate;
        this.fiscalYearStartDate = event.detail.LicenseStartDate;
        this.fiscalYearEndDate = event.detail.LicenseEndDate;
        this.mindatecontract = event.detail.LicenseStartDate;
        this.maxdatecontract = event.detail.LicenseEndDate;
        this.ContractId = event.detail.ContractID;
        this.LicenseID = event.detail.LicenseID;
        this.isDisabledEdit = event.detail.isDisable;
        this.contractDetailsLoad();
        this.fiscalYearCalculation();
        this.BedCountLoad();
    }

    //Declare contract object into one variable
    @wire(getObjectInfo, {
        objectApiName: Contract__c_OBJECT
    })
    objectInfo;

    //Function used to get all picklist values from contract object
    @wire(getPicklistValuesByRecordType, {
        objectApiName: Contract__c_OBJECT,
        recordTypeId: '$objectInfo.data.defaultRecordTypeId'
    })
    getAllPicklistValues({
        error,
        data
    }) {
        if (data) {
            this.error = null;
            // Solicitation Type Field Picklist values
            let SolicitationType = [];
            data.picklistFieldValues.SolicitationType__c.values.forEach(key => {
                SolicitationType.push({
                    label: key.label,
                    value: key.value
                })
            });
            this.SolicitationTypeOptions = SolicitationType;

            // Contract Status Field Picklist values
            let ContractStatus = [];
            data.picklistFieldValues.ContractStatus__c.values.forEach(key => {
                ContractStatus.push({
                    label: key.label,
                    value: key.value
                })
            });
            this.ContractStatusOptions = ContractStatus;

            // Contract Type Field Picklist values
            let ContractType = [];
            data.picklistFieldValues.ContractType__c.values.forEach(key => {
                ContractType.push({
                    label: key.label,
                    value: key.value
                })
            });
            this.ContractTypeOptions = ContractType;

            // Contract Term Type Field Picklist values
            let ContractTermType = [];
            data.picklistFieldValues.ContractTermType__c.values.forEach(key => {
                ContractTermType.push({
                    label: key.label,
                    value: key.value
                })
            });
            this.ContractTermTypeOptions = ContractTermType;

            // Budget Code Type Field Picklist values
            let BudgetCodeType = [];
            data.picklistFieldValues.BudgetCodeType__c.values.forEach(key => {
                BudgetCodeType.push({
                    label: key.label,
                    value: key.value
                })
            });
            this.BudgetCodeTypeOptions = BudgetCodeType;

            // Budget Code Field Picklist values
            let BudgetCode = [];
            data.picklistFieldValues.BudgetCode__c.values.forEach(key => {
                BudgetCode.push({
                    label: key.label,
                    value: key.value
                })
            });
            this.BudgetCodeOptions = BudgetCode;
        } else if (error) {
            this.error = JSON.stringify(error);
        }
    }
}