import {
  LightningElement,
  api,
  track
} from 'lwc';

export default class Cards extends LightningElement {
  @api _status;
  @api _heading;
  @track color;

  @api get heading() {
    return this._heading;
  }

  set heading(value) {
    this.setAttribute("heading", value);
    this._heading = value;
  }

  @api
  get status() {
    return this._status;
  }
  set status(value) {
    this.setAttribute("status", value);
    this._status = value ? value : 0;
  }
}