import {
  LightningElement,
  wire,
  api,
  track
} from 'lwc';

import images from '@salesforce/resourceUrl/images';

export default class PublicProviderTrainingDatatable extends LightningElement {
  @track _data
  @track _options
  @track _hidePagination
  descriptionIcon = images + '/description-icon.svg';


  @api get option() {
    return this_options
  }

  set option(value) {
    try {
      this.setAttribute("option", value)
      this._options = value
    } catch (err) {
      this.dispatchEvent(utils.toastMessage("Something Went Wrong", "error"))
    }
  }

  @api get tableData() {
    return this._data;
  }

  set tableData(value) {
    try {
      this.setAttribute("totalTrainingData", value);
      this._data = value;
      this.setPages(this._data);
      this.totalRecordsCount = value.length;
    } catch (e) {
      this.dispatchEvent(utils.toastMessage("Something Went Wrong", "error"))
    }
  }

  @api
  get hidePagination() {
    if (this._data) {
      this._hidePagination = this._data.length <= 0;
    } else {
      this._hidePagination = true;
    }
    return this._hidePagination
  }

  set hidePagination(value) {
    this.setAttribute("hidePagination", value);
    this._hidePagination = this._data.length > 0;
  }

  handleRowAction = (event) => {

    const clickedId = new CustomEvent('rowclickedid', {
      detail: {
        uniqueId: event.detail.row.uniqueId,
        actionType: event.detail.action.name ? event.detail.action.name : 'ViewTraining'
      }
    });
    this.dispatchEvent(clickedId);

  }

  // new pagination

  @track page = 1;
  perPage = 10;
  @track pages = [];
  setSize = 5;
  @track pagePerTime;
  @track pagesLength;

  renderedCallback() {
    this.renderButtons();
  }

  renderButtons = () => {
    this.template.querySelectorAll('button').forEach((but) => {
      but.className = this.page === parseInt(but.dataset.id, 10) ? 'slds-button slds-button_neutral active' : 'slds-button slds-button_neutral';
    });
  }


  get pagesList() {
    let mid = Math.floor(this.setSize / 2) + 1;
    if (this.page > mid) {
      return this.pages.slice(this.page - mid, this.page + mid - 1);
      // let pagecount = this.pages.slice(0, this.setsize);
      // this.pagePerTime = pagecount[pagecount.length - 1];
      // return this.pages.slice(this.page - mid, this.page + mid - 1);
    } else
      // let pagecount = this.pages.slice(0, this.setsize);
      // this.pagePerTime = pagecount[pagecount.length - 1];
    return this.pages.slice(0, this.setSize);
  }

  setPages = (data) => {
    try {
      let numberOfPages = Math.ceil(JSON.stringify(data.length) / this.perPage);
      this.pagesLength = numberOfPages;
      this.pages = [];
      for (let i = 1; i <= numberOfPages; i++) {
        this.pages.push(i);
      }
    } catch (e) {}
  }

  get hasPrev() {
    return this.page > 1;
  }

  get hasNext() {
    return this.page < this.pages.length;
  }

  onNext = () => {
    ++this.page;
  }

  onPrev = () => {
    --this.page;
  }

  onPageClick(event) {
    this.page = parseInt(event.target.dataset.id, 10);
  }

  get currentPageData() {
    return this.pageData();
  }

  pageData() {
    if (this._data) {
      if (this._data.length > 0) {
        let startIndex = (this.page * this.perPage) - this.perPage;
        let endIndex = (this.page * this.perPage);
        return this._data.slice(startIndex, endIndex);
      }
    }
  }
}