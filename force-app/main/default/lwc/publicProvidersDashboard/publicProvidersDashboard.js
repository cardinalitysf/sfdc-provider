/**
 * @Author        : Preethi Bojarajan
 * @CreatedOn     : Jun 11,2020
 * @Purpose       : Providers Dashboard for Private / Public Providers
 * @updatedBy     :
 * @updatedOn     :
 **/
import { LightningElement, track, wire, api } from 'lwc';
import { refreshApex } from "@salesforce/apex";

//Import Apex
import getProvidersCount from "@salesforce/apex/PublicProvidersDashboard.getProvidersCount";
import getProvidersList from "@salesforce/apex/PublicProvidersDashboard.getProvidersList";
import getMonitoringList from "@salesforce/apex/PublicProvidersDashboard.getMonitoringList";
import getContractList from "@salesforce/apex/PublicProvidersDashboard.getContractList";
import getProvidersSupCount from "@salesforce/apex/PublicProvidersDashboard.getProvidersSupCount";

//Utils 
import * as sharedData from "c/sharedData";
import { DASHBORD_LABELS, USER_PROFILE_NAME, CJAMS_CONSTANTS } from "c/constants";
import { getRecord } from "lightning/uiRecordApi";
import USER_ID from "@salesforce/user/Id";
import PROFILE_FIELD from "@salesforce/schema/User.Profile.Name";

const cardDatas = [{
    Id: '1',
    title: 'Inactive',
    key: 'inactive',
    count: 0
}, {
    Id: '2',
    title: 'Active',
    key: 'active',
    count: 0
}, {
    Id: '3',
    title: 'Total Providers',
    key: 'total',
    count: 0
}];

const suprCardDatas = [{
    Id: '1',
    title: 'Pending',
    key: 'Submitted',
    count: 0
}, {
    Id: '2',
    title: 'Approved',
    key: 'Approved',
    count: 0
}, {
    Id: '3',
    title: 'Rejected',
    key: 'Rejected',
    count: 0
}];

const pubColumns = [{
    label: "Provider Id",
    fieldName: "id",
    sortable: true,
    type: "button",
    typeAttributes: {
        label: {
            fieldName: "ProviderId"
        }
    }
},
{
    label: "Provider Category",
    fieldName: "ProviderType",
    sortable: true,
    cellAttributes: {
        class: "fontclrGrey"
    }
},
{
    label: "Applicant",
    fieldName: "County",
    sortable: true,
    cellAttributes: {
        class: "fontclrGrey"
    }
},
{
    label: "Co Applicant",
    fieldName: "TaxId",
    cellAttributes: {
        class: "fontclrGrey"
    }
},
{
    label: "SSN Number(Applicant)",
    fieldName: "Phone",
    cellAttributes: {
        class: "fontclrGrey"
    }
},
{
    label: "Status",
    fieldName: "Status",
    type: 'text',
    cellAttributes: {
        class: {
            fieldName: 'statusClass'
        }
    }
}
];

const priColumns = [{
    label: "Provider Id",
    fieldName: "id",
    sortable: true,
    type: "button",
    typeAttributes: {
        label: {
            fieldName: "ProviderId"
        }
    }
},
{
    label: "Provider Category",
    fieldName: "ProviderType",
    sortable: true,
    cellAttributes: {
        class: "fontclrGrey"
    }
},
{
    label: "Provider Name",
    fieldName: "Name",
    cellAttributes: {
        class: "fontclrGrey"
    }
},
{
    label: "Jurisdiction",
    fieldName: "County",
    sortable: true,
    cellAttributes: {
        class: "fontclrGrey"
    }
},
{
    label: "Tax",
    fieldName: "TaxId",
    cellAttributes: {
        class: "fontclrGrey"
    }
},
{
    label: "Phone",
    fieldName: "Phone",
    cellAttributes: {
        class: "fontclrGrey"
    }
},
{
    label: "Status",
    fieldName: "Status",
    type: 'text',
    cellAttributes: {
        class: {
            fieldName: 'statusClass'
        }
    }
}
];

const monitoringColumns = [{
    label: "Provider Id",
    fieldName: "id",
    sortable: true,
    type: "button",
    typeAttributes: {
        label: {
            fieldName: "ProviderId"
        }
    }
},
{
    label: "Monitoring Id",
    type: "button",
    typeAttributes: {
        label: {
            fieldName: "moniContId"
        },
        name: "monitoring"
    }
},
{
    label: "Provider Category",
    fieldName: "ProviderType",
    sortable: true,
    cellAttributes: {
        class: "fontclrGrey"
    }
},
{
    label: "Provider Name",
    fieldName: "Name",
    cellAttributes: {
        class: "fontclrGrey"
    }
},
{
    label: "Jurisdiction",
    fieldName: "County",
    sortable: true,
    cellAttributes: {
        class: "fontclrGrey"
    }
},
{
    label: "Tax",
    fieldName: "TaxId",
    cellAttributes: {
        class: "fontclrGrey"
    }
},
{
    label: "Phone",
    fieldName: "Phone",
    cellAttributes: {
        class: "fontclrGrey"
    }
},
{
    label: "Status",
    fieldName: "Status",
    type: 'text',
    cellAttributes: {
        class: {
            fieldName: 'statusClass'
        }
    }
}
];

const contractColumns = [{
    label: "Provider Id",
    fieldName: "id",
    sortable: true,
    type: "button",
    typeAttributes: {
        label: {
            fieldName: "ProviderId"
        }
    }
},
{
    label: "Contract Id",
    type: "button",
    typeAttributes: {
        label: {
            fieldName: "moniContId"
        },
        name: "contract"
    }
},
{
    label: "Provider Category",
    fieldName: "ProviderType",
    sortable: true,
    cellAttributes: {
        class: "fontclrGrey"
    }
},
{
    label: "Provider Name",
    fieldName: "Name",
    cellAttributes: {
        class: "fontclrGrey"
    }
},
{
    label: "Jurisdiction",
    fieldName: "County",
    sortable: true,
    cellAttributes: {
        class: "fontclrGrey"
    }
},
{
    label: "Tax",
    fieldName: "TaxId",
    cellAttributes: {
        class: "fontclrGrey"
    }
},
{
    label: "Phone",
    fieldName: "Phone",
    cellAttributes: {
        class: "fontclrGrey"
    }
},
{
    label: "Status",
    fieldName: "Status",
    type: 'text',
    cellAttributes: {
        class: {
            fieldName: 'statusClass'
        }
    }
}
];

export default class PublicProvidersDashboard extends LightningElement {
    @track cardDatas = cardDatas;
    @track suprCardDatas = suprCardDatas;
    @track columns = pubColumns;
    @track monitoringColumns = monitoringColumns;
    @track contractColumns = contractColumns;
    @track supervisorColumns = monitoringColumns;
    @track accountCount = [];
    @track accountSupCount = [];
    @track totalRecords = [];
    @track activePage = 0;
    @track initLoad = true;
    @track supervisorFlow = false;
    @track supervisorFlowPublic = true;
    @api modelName = 'Monitoring__c';
    @track contBtnDisable = false;
    @track moniBtnDisable = true;
    @track moniteringDatas = [];

    //Public / Private
    @track showPublicData = true;
    @track providerType = DASHBORD_LABELS.BTN_PUBLIC;

    //Search Function Start
    @api searchKey = "";
    @track searchType = 'ProviderId__c';

    @wire(getRecord, {
        recordId: USER_ID,
        fields: PROFILE_FIELD,
    })
    getUserProfileRecord(result) {
        if (result.data) {
            this.userProfileName = result.data.fields.Profile.displayValue;
            if (this.userProfileName === USER_PROFILE_NAME.USER_SUPERVISOR && this.providerType === DASHBORD_LABELS.BTN_PUBLIC)
                this.supervisorFlowPublic = true;
            else if (this.userProfileName === USER_PROFILE_NAME.USER_SUPERVISOR)
                this.supervisorFlow = true;
            sharedData.setUserProfileName(result.data.fields.Profile.displayValue);
            if (!this.initLoad) return;
            let init = 0;
            let search = this.searchKey;
            this.initLoad = false;
            this.getDataTableDatas(init, search, this.searchType);
        }
    }

    get cardClasses() {
        return [{
            title: 'rejected',
            card: 'rejected-cls',
            init: true
        }, {
            title: 'approved',
            card: 'approved-cls',
            init: false
        }, {
            title: 'total',
            card: 'total-cls',
            init: false
        }]
    }

    get supCardClasses() {
        return [{
            title: 'pending',
            card: 'pending-cls',
            init: true
        }, {
            title: 'approved',
            card: 'approved-cls',
            init: false
        }, {
            title: 'rejected',
            card: 'rejected-cls',
            init: false
        }]
    }

    //Common Data Formating Function Start
    formatData(data) {
        let currentData = [];
        if (this.supervisorFlow === false && this.providerType === DASHBORD_LABELS.BTN_PRIVATE)
            data.forEach(row => {
                let rowData = {};
                rowData.ProviderId = row.ProviderId__c;
                rowData.ProviderType = row.ProviderType__c;
                rowData.Status = row.Status__c;
                rowData.Id = row.Id;
                rowData.Name = row.Name !== undefined ? row.Name : '';
                rowData.County = row.BillingCountry != undefined ? row.BillingCountry.substring(0, 2) : '';
                rowData.TaxId = row.TaxId__c != undefined ? row.TaxId__c : '';
                rowData.Phone = row.Phone != undefined ? row.Phone : '';
                rowData.statusClass = row.Status__c;
                currentData.push(rowData);
            });
        else if (this.supervisorFlow === false && this.providerType === DASHBORD_LABELS.BTN_PUBLIC)
            data.forEach(row => {
                let rowData = {};
                rowData.ProviderId = row.ProviderId__c;
                rowData.ProviderType = row.ProviderType__c;
                rowData.Status = row.Status__c;
                rowData.Id = row.Id;
                if (row.Actors__r !== undefined && row.Actors__r.length > 0) {
                    rowData.Name = row.Actors__r[0].Application__r !== undefined ? row.Actors__r[0].Application__r.Program__c : '';
                    let applicant = row.Actors__r.filter(actor => actor.Role__c === 'Applicant');
                    let coApplicant = row.Actors__r.filter(actor => actor.Role__c === 'Co-Applicant');
                    rowData.County = applicant.length > 0 ? applicant[0].Contact__r.LastName : '';
                    rowData.TaxId = coApplicant.length > 0 ? coApplicant[0].Contact__r.LastName : '';
                    rowData.Phone = applicant[0].Contact__r.SSN__c !== undefined ? applicant[0].Contact__r.SSN__c : '';
                } else {
                    rowData.Name = '';
                    rowData.Applicant = '';
                    rowData.TaxId = '';
                    rowData.Phone = '';
                }
                rowData.statusClass = row.Status__c;
                currentData.push(rowData);
            });
        else
            data.forEach(row => {
                let preData = null;
                if (row.ApplicationLicenseId__r !== undefined)
                    preData = row.ApplicationLicenseId__r.Provider__r;
                else
                    preData = row.Provider__r;
                let rowData = {};
                rowData.moniContId = row.Name;
                rowData.ProviderId = preData.ProviderId__c != undefined ? preData.ProviderId__c : '';
                rowData.ProviderType = preData.ProviderType__c;
                rowData.Status = preData.Status__c;
                rowData.Id = row.Id;
                rowData.providerId = preData.Id;
                rowData.Name = preData.Name != undefined ? preData.Name : '';
                rowData.County = preData.BillingCountry != undefined ? preData.BillingCountry.substring(0, 2) : '';
                rowData.TaxId = preData.TaxId__c != undefined ? preData.TaxId__c : '';
                rowData.Phone = preData.Phone != undefined ? preData.Phone : '';
                rowData.statusClass = preData.Status__c;
                currentData.push(rowData);
            });
        return currentData;
    }
    // Common Data Formating Function End

    @wire(getProvidersCount, {
        searchKey: '$searchKey',
        searchType: '$searchType',
        providerType: '$providerType'
    })
    wiredProvidersResult(result) {
        this.accountCount = result;
        if (result.data) {
            let init = 0;
            let datas = result.data[init];
            for (let key of this.cardDatas) {
                key.count = datas[key.key];
            }
        } else if (result.error) {
            let errors = result.error;
            if (errors) {
                let error = JSON.parse(errors.body.message);
                const {
                    title,
                    message,
                    errorType
                } = error;
                this.dispatchEvent(utils.toastMessageWithTitle(title, message, errorType));
            } else {
                this.dispatchEvent(utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE));
            }
        }
    }

    @wire(getProvidersSupCount, {
        searchKey: '$searchKey',
        searchType: '$searchType',
        providerType: '$providerType',
        modelName: '$modelName'
    })
    wiredProvidersSupResult(result) {
        this.accountSupCount = result;
        if (result.data) {
            let init = 0;
            let datas = result.data[init];
            for (let key of this.suprCardDatas) {
                key.count = datas[key.key];
            }
        } else if (result.error) {
            let errors = result.error;
            if (errors) {
                let error = JSON.parse(errors.body.message);
                const {
                    title,
                    message,
                    errorType
                } = error;
                this.dispatchEvent(utils.toastMessageWithTitle(title, message, errorType));
            } else {
                this.dispatchEvent(utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE));
            }
        }
    }

    getDataTableDatas(id, search, searchType) {
        this.activePage = id;
        if (this.supervisorFlow === false) {
            getProvidersList({
                searchKey: search,
                type: this.cardDatas[id].key,
                searchType: searchType,
                providerType: this.providerType
            }).then(data => {
                if (data.length > 0) {
                    let currentData = this.formatData(data);
                    this.totalRecords = currentData;
                } else this.totalRecords = [];
            }).catch((errors) => {
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const {
                        title,
                        message,
                        errorType
                    } = error;
                    this.dispatchEvent(
                        utils.toastMessageWithTitle(title, message, errorType)
                    );
                } else {
                    this.dispatchEvent(
                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                    );
                }
            });
        } else {
            if (this.moniBtnDisable === true) {
                this.modelName = 'Monitoring__c';
                this.moniBtnDisable = true;
                this.contBtnDisable = false;
                getMonitoringList({
                    searchKey: search,
                    type: this.suprCardDatas[id].key,
                    searchType: searchType
                }).then(data => {
                    this.moniteringDatas = data;
                    let currentData = [];
                    if (this.moniteringDatas.length > 0)
                        currentData = this.formatData(data);
                    this.totalRecords = currentData;
                }).catch((errors) => {
                    if (errors) {
                        let error = JSON.parse(errors.body.message);
                        const {
                            title,
                            message,
                            errorType
                        } = error;
                        this.dispatchEvent(
                            utils.toastMessageWithTitle(title, message, errorType)
                        );
                    } else {
                        this.dispatchEvent(
                            utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                        );
                    }
                });
                this.supervisorColumns = this.monitoringColumns;
            } else {
                this.modelName = 'Contract__c';
                this.contBtnDisable = true;
                this.moniBtnDisable = false;
                let type = '';
                if (this.suprCardDatas[id].key === 'Approved') type = 'Active';
                else if (this.suprCardDatas[id].key === 'Rejected') type = 'Inactive';
                else type = 'Submitted';
                this.modelName = 'Contract__c';
                getContractList({
                    searchKey: search,
                    type: type,
                    searchType: searchType
                }).then(data => {
                    let currentData = [];
                    if (data.length > 0)
                        currentData = this.formatData(data);
                    this.totalRecords = currentData;
                }).catch((errors) => {
                    if (errors) {
                        let error = JSON.parse(errors.body.message);
                        const {
                            title,
                            message,
                            errorType
                        } = error;
                        this.dispatchEvent(
                            utils.toastMessageWithTitle(title, message, errorType)
                        );
                    } else {
                        this.dispatchEvent(
                            utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                        );
                    }
                });
                this.supervisorColumns = this.contractColumns;
            }
            refreshApex(this.accountSupCount);
        }
        //this.template.querySelector('c-common-dashboard').searchFn();
    }

    /* Datas to Dashboard DataTable Start */
    handleDataTableDatas(event) {
        let search = this.searchKey;
        this.getDataTableDatas(event.detail, search, this.searchType);
    }

    handleChange(event) {
        this.searchKey = event.target.value;
        this.getDataTableDatas(this.activePage, this.searchKey, this.searchType);
        if (this.supervisorFlow === false)
            refreshApex(this.accountCount);
        this.template.querySelector('c-common-dashboard').searchFn();
    }

    searchListType(event) {
        this.searchType = event.detail.value;
    }

    get searchLists() {
        return [{
            label: 'Provider Id',
            value: 'ProviderId__c'
        },
        {
            label: 'Provider Name',
            value: 'Name'
        },
        {
            label: 'Jurisdiction',
            value: 'BillingCountry'
        },
        ];
    }
    /* Datas to Dashboard DataTable Start */

    // Redirection Function
    handleRedirection(event) {
        if (event.detail.name !== undefined) {
            if (event.detail.name === 'monitoring') {
                sharedData.setProviderId(event.detail.providerId);
                sharedData.setMonitoringId(event.detail.redirectId);
                const onClickId = new CustomEvent('redirecteditmonitering', {
                    detail: {
                        first: event.detail.first
                    }
                });
                this.dispatchEvent(onClickId);
            } else {
                sharedData.setProviderId(event.detail.providerId);
                sharedData.setContractId(event.detail.redirectId);
                const onClickId = new CustomEvent('redirecttocontractdashboard', {
                    detail: {
                        first: event.detail.first
                    }
                });
                this.dispatchEvent(onClickId);
            }
        } else {
            sharedData.setApplicationId(undefined);
            if (this.providerType === DASHBORD_LABELS.BTN_PUBLIC) {
                sharedData.setProviderId(event.detail.redirectId);
                const onClickId = new CustomEvent('redirecttopublicproviderinfo', {
                    detail: {
                        first: event.detail.first,
                        providerName: event.detail.providerName
                    }
                });
                this.dispatchEvent(onClickId);
            } else {
                if (this.supervisorFlow === true)
                    sharedData.setProviderId(event.detail.providerId);
                else
                    sharedData.setProviderId(event.detail.redirectId);
                const onClickId = new CustomEvent('redirecttoprivateproviderinfo', {
                    detail: {
                        first: event.detail.first
                    }
                });
                this.dispatchEvent(onClickId);
            }
        }
    }
    /* Handle Between Monitoring And C  ontract tabs */
    handleClick(event) {
        let value = event.target.dataset.name;
        let id = this.activePage;
        let search = this.searchKey;
        let searchType = this.searchType;
        if (value === 'Monitoring__c') {
            this.moniBtnDisable = true;
            this.contBtnDisable = false;
            this.modelName = 'Monitoring__c';
            this.supervisorColumns = this.monitoringColumns;
            getMonitoringList({
                searchKey: search,
                type: this.suprCardDatas[id].key,
                searchType: searchType
            }).then(data => {
                let currentData = this.formatData(data);
                this.totalRecords = currentData;
            }).catch((errors) => {
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const {
                        title,
                        message,
                        errorType
                    } = error;
                    this.dispatchEvent(
                        utils.toastMessageWithTitle(title, message, errorType)
                    );
                } else {
                    this.dispatchEvent(
                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                    );
                }
            });
        } else {
            this.contBtnDisable = true;
            this.moniBtnDisable = false;
            this.supervisorColumns = this.contractColumns;
            let type = '';
            if (this.suprCardDatas[id].key === 'Approved') type = 'Active';
            else if (this.suprCardDatas[id].key === 'Rejected') type = 'Inactive';
            else type = 'Submitted';
            this.modelName = 'Contract__c';
            getContractList({
                searchKey: search,
                type: type,
                searchType: searchType
            }).then(data => {
                let currentData = this.formatData(data);
                this.totalRecords = currentData;
            }).catch((errors) => {
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const {
                        title,
                        message,
                        errorType
                    } = error;
                    this.dispatchEvent(
                        utils.toastMessageWithTitle(title, message, errorType)
                    );
                } else {
                    this.dispatchEvent(
                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                    );
                }
            });
        }
        refreshApex(this.accountSupCount);
        this.template.querySelector('c-common-dashboard').searchFn();
    }
    /* Handle Between Monitoring And Contract tabs End */

    handleDashboardDatas(event) {
        this.providerType = event.detail;
        if (this.providerType === DASHBORD_LABELS.BTN_PUBLIC) {
            this.supervisorFlowPublic = true;
            this.supervisorFlow = false;
            this.columns = pubColumns;
        } else if (this.providerType === DASHBORD_LABELS.BTN_PRIVATE && this.userProfileName === USER_PROFILE_NAME.USER_SUPERVISOR) {
            this.supervisorFlowPublic = false;
            this.supervisorFlow = true;
            this.columns = priColumns;
        } else if (this.providerType === DASHBORD_LABELS.BTN_PRIVATE) {
            this.supervisorFlowPublic = true;
            this.supervisorFlow = false;
            this.columns = priColumns;
        }
        let init = 0;
        let search = this.searchKey;
        this.getDataTableDatas(init, search, this.searchType);
        if (this.supervisorFlow === false)
            refreshApex(this.accountCount);
        else refreshApex(this.accountSupCount);
    }
}