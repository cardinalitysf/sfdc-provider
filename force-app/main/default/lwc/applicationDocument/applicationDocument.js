/**
 * @Author        : BalaMurugan
 * @CreatedOn     : Apr 12,2020
 * @Purpose       : Attachments based on Extension and Login of CaseWorker  viceversa Provider
 * @UpdatedBy     : Pratheeba
 * @CreatedOn     : Apr 29,2020
 * @Purpose       : supervisor flow
 **/

import {
    LightningElement,
    track

} from 'lwc';
import * as referralsharedDatas from "c/sharedData";
import {
    CJAMS_CONSTANTS
} from 'c/constants';


export default class ApplicationDocument extends LightningElement {
    get applicationID() {
        return referralsharedDatas.getApplicationId();
    }
    get sobjectNameToFetch() {
        return CJAMS_CONSTANTS.APPLICATION_OBJECT_NAME;
    }

    get getUserProfileName() {
        return referralsharedDatas.getUserProfileName();
    }

}