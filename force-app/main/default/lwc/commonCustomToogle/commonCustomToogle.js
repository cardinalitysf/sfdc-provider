import {
    LightningElement,
    api
} from 'lwc';

export default class CommonCustomToogle extends LightningElement {
    @api recordId;
    @api isChecked;
    @api isDisabled;
    handleToggleChange(evt) {
        this.dispatchEvent(new CustomEvent('togglechange', {
            composed: true,
            bubbles: true,
            cancelable: true,
            detail: {
                data: {
                    recordId: this.recordId,
                    isChecked: evt.target.checked
                }
            }
        }));
    }

}