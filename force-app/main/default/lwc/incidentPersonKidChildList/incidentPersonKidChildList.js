import { LightningElement, } from 'lwc';
import getIncidentPersonChildDetails from '@salesforce/apex/IncidentPersonKidChildInfo.getIncidentPersonChildDetails';
import { deleteRecord } from "lightning/uiRecordApi";
import { CJAMS_CONSTANTS, USER_PROFILE_NAME } from "c/constants";
import { utils } from "c/utils";
import * as sharedData from "c/sharedData";
import images from "@salesforce/resourceUrl/images";

const columns = [{
    label: "PERSON/KID/CHILD INVOLVED",
    fieldName: "personName",
    type: "text"
}, {
    label: "IDENTIFIER NUMBER",
    fieldName: "identityNumber",
    type: "text"
},
{
    label: "ADMITTING CHARGE",
    fieldName: "admittingchange",
    type: "number"
},
{
    label: "ROLE IN INCIDENT",
    fieldName: "roleincident",
    type: "text"
},
{
    label: "PHYSICAL RESTRAINT",
    fieldName: "physicalduration",
    type: "text"
},
{
    label: "STAFF NAME",
    fieldName: "StaffInvolved__c",
    type: "text"
},
{
    label: "STATUS",
    fieldName: "Assign",
    type: "text"
}
]
export default class IncidentPersonKidChildList extends LightningElement {

    columns = columns;
    deleteModel = false;
    disableDelClick = false;
    AssignbtnDisable = false;
    currentDataPersonKid;
    norecorddisplay = true;
    personKidIcon = images + "/friend.svg";

    connectedCallback() {
        this.getPersondata();
    }

    get caseid() {
         return sharedData.getCaseId();
    }

    get profileName() {
        return sharedData.getUserProfileName();
    }

    get incidentStatus() {
        return sharedData.getIncidentStatus();
    }
    //Intial Load Data
    getPersondata() {
        if (!this.caseid) {
            this.norecorddisplay = false;
            this.Spinner = false;
            return
          }
        getIncidentPersonChildDetails({
            referID: this.caseid,
        })
            .then((result) => {
                if (result.length > 0) {
                    let statusClass = "tripledots tripleDot_sec_del";
                      if (this.profileName === USER_PROFILE_NAME.USER_CASE_WRKR || (this.incidentStatus !== undefined && (this.incidentStatus !== "Pending" || this.incidentStatus !== "Draft"))) {
                        this.AssignbtnDisable = true;
                        statusClass = "tripledots";
                     }
                    this.norecorddisplay = true;
                    this.currentDataPersonKid = result.map(i => {
                        return {
                            Id: i.Id,
                            personName: i.Contact__r.LastName,
                            identityNumber: i.Contact__r.IdentifierNumber__c != undefined ? i.Contact__r.IdentifierNumber__c : '-',
                            admittingchange: i.Contact__r.AdmittingCharge__c != undefined ? i.Contact__r.AdmittingCharge__c : '-',
                            roleincident: i.RoleinIncident__c != undefined ? i.RoleinIncident__c : '-',
                            physicalduration: i.DurationofPhysicalRestraint__c != undefined ? i.DurationofPhysicalRestraint__c : '-',
                            Assign: i.Assign__c == true ? 'Assigned' : '-',
                            ContactId: i.Contact__r.Id,
                            AddressId: i.Address__r.Id,
                            StaffInvolved__c: i.StaffInvolved__c != undefined ? i.StaffInvolved__c : '-',
                            buttonDisabled: (this.AssignbtnDisable) ? true : false,
                            statusClass: statusClass
                        }
                    })
                    result = this.currentDataPersonKid;
                    const personkidDispatch = new CustomEvent('personkiddecisonflag', {
                        detail: {
                            PersonKidDataflag: true
                        }
                    });
                    this.dispatchEvent(personkidDispatch);
                }else
                  this.norecorddisplay = false;
            }).catch(errors => {
                let config = {
                    friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE,
                    errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE
                }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
    }

    handleRowAction(event) {
        this.recordEditId = event.detail.row.Id;
        this.contactId = event.detail.row.ContactId;
        this.addressId = event.detail.row.AddressId;
        sharedData.setPersonKidIncidentId(this.recordEditId);
        sharedData.setContactId(this.contactId);
        sharedData.setAddressId(this.addressId);
        sharedData.setActionPersonKid(event.detail.action.name);
        if (event.detail.action.name == "Edit") {
            this.redirectionEditView();
        } else if (event.detail.action.name == "View") {
            this.redirectionEditView();
        }
        else if (event.detail.action.name == "Delete") {
            this.deleteRecordForincident = event.detail.row.Id;
            this.deleteModel = true;
        }
    }

    redirectionEditView() {
        const onEditid = new CustomEvent("redirecttoeditpersonkid", {
            detail: {
                first: true
            }
        });
        this.dispatchEvent(onEditid);
    }

    //Delete incident Record
    handleDelete() {
        this.disableDelClick = true;
        deleteRecord(this.deleteRecordForincident)
            .then(() => {
                this.deleteModel = false;
                this.dispatchEvent(
                    utils.toastMessage("Person Details deleted successfully", "success")
                );
                this.disableDelClick = false;
                this.getPersondata();
            })
            .catch((error) => {
                this.disableDelClick = false;
                this.deleteModel = false;
                this.dispatchEvent(
                    utils.toastMessage("Warning in delete Person details", "Warning")
                );
            });
    }
    closeDeleteModal() {
        this.deleteModel = false;
    }
    //Show Hide Of View Only In threeDots...
    constructor() {
        super();
        let ViewIcon = true;
        for (let i = 0; i < this.columns.length; i++) {
            if (this.columns[i].type == 'action') {
                ViewIcon = false;
            }
        }
        if (ViewIcon) {
            this.columns.push(
                {
                    type: 'action', typeAttributes: { rowActions: this.getRowActions }, cellAttributes: {
                        iconName: 'utility:threedots_vertical',
                        iconAlternativeText: 'Actions',
                        class: { fieldName: "statusClass" }
                    }
                }
            )
        }
    }
    getRowActions(row, doneCallback) {
        const actions = [];
        if (row['buttonDisabled'] == true) {
            actions.push({
                'label': 'View',
                'name': 'View',
                'iconName': 'utility:preview',
                'target': '_self'
            });
        } else {
            actions.push({
                'label': 'View',
                'name': 'View',
                'iconName': 'utility:preview',
                'target': '_self'
            },{
                'label': 'Edit',
                'name': 'Edit',
                'iconName': 'utility:edit',
                'target': '_self'
            },{
                'label': 'Delete',
                'name': 'Delete',
                'iconName': 'utility:delete',
                'target': '_self'
                });
        }
        // simulate a trip to the server
        setTimeout(() => {
            doneCallback(actions);
        }, 20);
    }

}