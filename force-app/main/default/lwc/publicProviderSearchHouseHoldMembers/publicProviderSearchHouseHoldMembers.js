/**
 * @Author        : G.Tamilarasan
 * @CreatedOn     : May 13, 2020
 * @Purpose       : This component contains Search Household Members
 **/
import { LightningElement, track, api } from 'lwc';

import getSearchHoldMembers from '@salesforce/apex/PublicProviderSearchHouseHoldMembers.getSearchHoldMembers';
import { utils } from "c/utils";
import images from '@salesforce/resourceUrl/images';
import * as sharedData from 'c/sharedData';
import { CJAMS_CONSTANTS } from "c/constants";

//Declare column headers in Household search table
const columns = [
    { label: 'First Name', fieldName: 'FirstName__c', type: 'text' },
    { label: 'Last Name', fieldName: 'LastName__c', type: 'text' },
    { label: 'DOB', fieldName: 'DOB__c', type: 'date', typeAttributes: { month: '2-digit', day: '2-digit', year: 'numeric' } },
    { label: 'Gender', fieldName: 'Gender__c', type: 'text' },
    { label: 'SSN', fieldName: 'SSN__c', type: 'text' },
    { label: 'Cjams Id', fieldName: 'ContactNumber__c', type: 'text' },
    { label: 'Chessie Id', fieldName: 'CHESSIEID__c', type: 'text' },
    { label: 'Mom Id', fieldName: 'MDMID__c', type: 'text' },
    { label: 'History', fieldName: 'History__c', type: 'text' },
];

export default class PublicProviderSearchHouseHoldMembers extends LightningElement {

    @track columns = columns;

    @track openSearchHouseholdModel = true;
    @track searchdetailslist;
    @track advanceSearchLabel = false;
    @track searchHouseHoldTableView = false;
    @track currentPageHouseHoldData;
    @track searchHouseHoldMembersLists = { FirstName: '', LastName: '', Gender__c: '', DOB__c: '', ApproximateAge__c: '', SSN__c: '' };
    @track nonSearchHouseHoldMembersLists = { MaidenName: '', AliasName: '', StateID: '', PetitionId: '', FEIN: '', Complaint: '', Occupation: '', Email: '', Phone: '', OldNos: '', CJAMSPID: '', CHESSIEPID: '', SocialMediaName: '', Address1: '', Address2: '', City: '', State: '', Country: '', ZipCode: '' };
    @track disableSearchButton = true;
    @track Spinner = false;
    @track pillvalueForSearchHold = [];
    @track GenderOptions = [{ label: "Male", value: "Male" }, { label: "Female", value: "Female" }];
    @track norecorddisplay = true;
    @track isDisableNextButton = true;
    @track isCancelButtonRedirection = true;
    @track totalRecords;
    @track selectedContactId;
    @track totalRecordsCount = 0;
    @track page = 1;
    perpage = 10;
    setPagination = 5;
    @api showResult = false;
    @api searchHouseHoldMembersListsFromAddMember = { FirstName: '', LastName: '', Gender__c: '', DOB__c: '', ApproximateAge__c: '', SSN__c: '' };
    attachmentIcon = images + '/document-newIcon.svg';

    //Search Model popup open
    opensearchhouseholdmodel() {
        this.isCancelButtonRedirection = false;
        this.openSearchHouseholdModel = true;
    }
    //Search Model popup close
    searchHouseholdCloseModal() {
        if (this.isCancelButtonRedirection) {
            this.redirectHouseHoldMemberHome();
        }
        else {
            this.openSearchHouseholdModel = false;
            this.searchHouseHoldTableView = true;
            this.searchDataList();
        }
    }

    //This function used to clear the search input fields
    clearSearchHousehold() {
        this.searchHouseHoldMembersLists = { FirstName: '', LastName: '', Gender__c: '', DOB__c: '', ApproximateAge__c: '', SSN__c: '' };

    }

    //This function used to assign the values to input fields when return back to add household member
    connectedCallback() {
        if (this.showResult) {
            this.searchHouseHoldMembersLists.FirstName = this.searchHouseHoldMembersListsFromAddMember.FirstName;
            this.searchHouseHoldMembersLists.LastName = this.searchHouseHoldMembersListsFromAddMember.LastName;
            this.searchHouseHoldMembersLists.Gender__c = this.searchHouseHoldMembersListsFromAddMember.Gender__c;
            this.searchHouseHoldMembersLists.DOB__c = this.searchHouseHoldMembersListsFromAddMember.DOB__c;
            this.searchHouseHoldMembersLists.ApproximateAge__c = this.searchHouseHoldMembersListsFromAddMember.ApproximateAge__c;
            this.searchHouseHoldMembersLists.SSN__c = this.searchHouseHoldMembersListsFromAddMember.SSN__c;
            this.searchHouseHoldData();
        }

    }

    //This function used to reset the flag when disconnected
    disconnectedCallback() {
        this.showResult = false;
    }
    get searchDetailsList() {
        return this.searchdetailslist;
    }

    //This function used to show/hide for advanced search options
    advanceSearch() {
        this.advanceSearchLabel = !this.advanceSearchLabel;
    }

    //This function used to get the corresponding row Id to send the add household member 
    handleRowSelection = event => {
        this.HouseHoldRowID = [];
        this.selectedContactId = event.detail.selectedRows[0].Id;
        this.isDisableNextButton = false;
    }

    //This function used to make the query format 
    searchHouseHoldData() {
        this.searchHouseHoldTableView = true;
        this.pillvalueForSearchHold = [];
        let stringConversion = "";
        let stringConversionObject = {};

        if (!!this.searchHouseHoldMembersLists.FirstName) {
            this.pillvalueForSearchHold.push({ label: "FirstName", value: "First Name: " + this.searchHouseHoldMembersLists.FirstName });
            stringConversion += " AND FirstName__c = \'" + this.searchHouseHoldMembersLists.FirstName + "\'";
            stringConversionObject.FirstName = this.searchHouseHoldMembersLists.FirstName;
        }
        if (!!this.searchHouseHoldMembersLists.LastName) {
            this.pillvalueForSearchHold.push({ label: "LastName", value: "Last Name: " + this.searchHouseHoldMembersLists.LastName });
            stringConversion += " AND LastName__c = \'" + this.searchHouseHoldMembersLists.LastName + "\'";
            stringConversionObject.LastName = this.searchHouseHoldMembersLists.LastName;
        }
        if (!!this.searchHouseHoldMembersLists.Gender__c) {
            this.pillvalueForSearchHold.push({ label: "Gender__c", value: "Gender: " + this.searchHouseHoldMembersLists.Gender__c });
            stringConversion += " AND Gender__c = \'" + this.searchHouseHoldMembersLists.Gender__c + "\'";
            stringConversionObject.Gender__c = this.searchHouseHoldMembersLists.Gender__c;
        }
        if (!!this.searchHouseHoldMembersLists.DOB__c) {
            this.pillvalueForSearchHold.push({ label: "DOB__c", value: "DOB: " + this.searchHouseHoldMembersLists.DOB__c });
            stringConversion += " AND DOB__c = " + this.searchHouseHoldMembersLists.DOB__c;
            stringConversionObject.DOB__c = this.searchHouseHoldMembersLists.DOB__c;
        }
        if (!!this.searchHouseHoldMembersLists.ApproximateAge__c) {
            this.pillvalueForSearchHold.push({ label: "ApproximateAge__c", value: "Approx. Age: " + this.searchHouseHoldMembersLists.ApproximateAge__c });
            stringConversion += " AND ApproximateAge__c = " + this.searchHouseHoldMembersLists.ApproximateAge__c;
            stringConversionObject.ApproximateAge__c = this.searchHouseHoldMembersLists.ApproximateAge__c;
        }
        if (!!this.searchHouseHoldMembersLists.SSN__c) {
            this.pillvalueForSearchHold.push({ label: "SSN__c", value: "SSN: " + this.searchHouseHoldMembersLists.SSN__c });
            stringConversion += " AND SSN__c = \'" + this.searchHouseHoldMembersLists.SSN__c + "\'";
            stringConversionObject.SSN__c = this.searchHouseHoldMembersLists.SSN__c;
        }


        this.searchdetailslist = stringConversion;
        this.searchDataList();

        this.openSearchHouseholdModel = false;
    }

    //This function used to search the household member from contact object
    searchDataList() {

        this.Spinner = true;
        getSearchHoldMembers({ searchdetails: this.searchdetailslist }).then(result => {
            this.Spinner = false;
            this.totalRecords = result;
            this.totalRecordsCount = this.totalRecords.length;
            if (this.totalRecords.length == 0) {
                this.norecorddisplay = false;
            }
            else {
                this.norecorddisplay = true;
            }
            this.pageData();

        }).catch(errors => {
            this.Spinner = false;
            let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
            return this.dispatchEvent(utils.handleError(errors, config));
        });
    }

    //This function used to capture all the search input values
    handleChange(event) {
        this.disableSearchButton = false;
        if (event.target.name == "FirstName") {
            this.searchHouseHoldMembersLists.FirstName = event.target.value;
        }
        else if (event.target.name == "LastName") {
            this.searchHouseHoldMembersLists.LastName = event.target.value;
        }
        else if (event.target.name == "Gender__c") {
            this.searchHouseHoldMembersLists.Gender__c = event.target.value;
        }
        else if (event.target.name == "DOB__c") {
            this.searchHouseHoldMembersLists.DOB__c = event.target.value;
        }
        else if (event.target.name == "ApproximateAge__c") {
            this.searchHouseHoldMembersLists.ApproximateAge__c = event.target.value;
        }
        else if (event.target.name == "SSN__c") {
            this.searchHouseHoldMembersLists.SSN__c = event.target.value;
        }

    }

    //This function used to remove the searched values when clicking pill
    handleRemove(evt) {
        this.pillvalueForSearchHold = this.remove(
            this.pillvalueForSearchHold,
            "value",
            evt.target.label
        );
        if (evt.target.label == "First Name: " + this.searchHouseHoldMembersLists.FirstName)
            this.searchHouseHoldMembersLists.FirstName = "";
        if (evt.target.label == "Last Name: " + this.searchHouseHoldMembersLists.LastName)
            this.searchHouseHoldMembersLists.LastName = "";
        if (evt.target.label == "Gender: " + this.searchHouseHoldMembersLists.Gender__c)
            this.searchHouseHoldMembersLists.Gender__c = "";
        if (evt.target.label == "DOB: " + this.searchHouseHoldMembersLists.DOB__c)
            this.searchHouseHoldMembersLists.DOB__c = "";
        if (evt.target.label == "Approx. Age: " + this.searchHouseHoldMembersLists.ApproximateAge__c)
            this.searchHouseHoldMembersLists.ApproximateAge__c = "";
        if (evt.target.label == "SSN: " + this.searchHouseHoldMembersLists.SSN__c)
            this.searchHouseHoldMembersLists.SSN__c = "";

        this.searchHouseHoldData();
    }
    remove(array, key, value) {
        const index = array.findIndex(obj => obj[key] === value);
        return index >= 0 ? [...array.slice(0, index), ...array.slice(index + 1)] :
            array;
    }

    //Function used to show the household values based on the pages
    pageData = () => {
        let page = this.page;
        let perpage = this.perpage;
        let startIndex = (page * perpage) - perpage;
        let endIndex = (page * perpage);
        this.currentPageHouseHoldData = this.totalRecords.slice(startIndex, endIndex);
        if (this.currentPageHouseHoldData.length == 0) {
            if (this.page != 1) {
                this.page = this.page - 1;
                this.pageData();
            }
        }
    }

    //Function used to get the page number from child pagination component
    hanldeProgressValueChange(event) {
        this.page = event.detail;
        this.pageData();
    }

    //Function used to redirect to household home member page
    redirectHouseHoldMemberHome() {
        const onhouseholdredirect = new CustomEvent('redirecttoaddhouseholdmemberhome', {
            detail: {
                first: false
            }
        });
        this.dispatchEvent(onhouseholdredirect);
    }

    //Function used to redirect to household add member page
    redirectAddHouseHoldMember() {
        sharedData.setHouseHoldContactId(this.selectedContactId);
        const onhouseholdaddmemberredirect = new CustomEvent('redirecttoaddhouseholdaddmember', {
            detail: {
                first: false,
                searchLists: this.searchHouseHoldMembersLists
            }
        });
        this.dispatchEvent(onhouseholdaddmemberredirect);
    }
}