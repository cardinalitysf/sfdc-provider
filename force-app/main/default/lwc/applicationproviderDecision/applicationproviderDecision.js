/**
 * @Author        : Sindhu Venkateswarlu
 * @CreatedOn     : APRIL 6 ,2020
 * @Purpose       :  Decison and Signature Details.
 **/
import {
    LightningElement,
    wire,
    track,
    api
} from 'lwc';
import fatchPickListValue from '@salesforce/apex/applicationproviderDecision.getPickListValues';
import saveSign from '@salesforce/apex/applicationproviderDecision.saveSign';
import updateapplicationdetails from '@salesforce/apex/applicationproviderDecision.updateapplicationdetails';
import updateapplicationdetailsDraft from '@salesforce/apex/applicationproviderDecision.updateapplicationdetailsDraft';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';
import getReviewDetails from "@salesforce/apex/applicationproviderDecision.getReviewDetails";
import getmonitoringDetails from "@salesforce/apex/applicationproviderDecision.getmonitoringDetails";
import getAllUsersList from "@salesforce/apex/applicationproviderDecision.getAllUsersList";
import updateCaseworkerdetails from "@salesforce/apex/applicationproviderDecision.updateCaseworkerdetails";
import getDatatableDetails from "@salesforce/apex/applicationproviderDecision.getDatatableDetails";
import getSignatureDetails from "@salesforce/apex/applicationproviderDecision.getSignatureDetails";
import images from '@salesforce/resourceUrl/images';
import userId from '@salesforce/user/Id';
import {
    refreshApex
} from "@salesforce/apex";

//Utils
import { USER_PROFILE_NAME } from 'c/constants';

import * as sharedData from "c/sharedData";
import {
    utils
} from 'c/utils';


const dataTableColumns = [{
        label: 'Date',
        fieldName: 'SubmittedDate__c'
    },
    {
        label: 'Author',
        fieldName: 'authorName'
    },
    {
        label: 'Designation',
        fieldName: 'authorDesignation'
    },
    {
        label: 'Status',
        fieldName: 'status'
    },
    {
        label: 'Assigned To',
        fieldName: 'assignedToName'
    },
    {
        label: 'Designation',
        fieldName: 'supervisorDesignation'
    },
    {
        label: 'Action',
        fieldName: 'id',
        type: "button",
        typeAttributes: {
            iconName: 'utility:preview',
            name: 'View',
            title: 'Click to Preview',
            variant: 'border-filled',
            class: 'view-red',
            disabled: false,
            iconPosition: 'left',
            target: '_self'
        }
    },

]

const ModalColumns = [{
        label: 'Name',
        fieldName: 'Name',
        type: 'Name'
    },
    {
        label: 'CaseLoad',
        fieldName: 'CaseLoad__c',
        type: 'number'
    },
    {
        label: 'Availability',
        fieldName: 'Availability__c',
        type: 'text'
    },
    {
        label: 'Zip Code',
        fieldName: '',
        type: 'text'
    },
    {
        label: 'Type Of Worker',
        fieldName: 'ProfileName',
        type: 'text'
    },
    {
        label: 'Unit',
        fieldName: 'Unit__c',
        type: 'text'
    },
];


// declaration of variables
// for calculations
let isDownFlag,
    isDotFlag = false,
    prevX = 0,
    currX = 0,
    prevY = 0,
    currY = 0;

let x = "#0000A0"; //blue color
let y = 1.5; //weight of line width and dot.       

let canvasElement, ctx; //storing canvas context
let attachment; //holds attachment information after saving the sigture on canvas
let dataURL, convertedDataURI; //holds image data

export default class ApplicationproviderDecision extends LightningElement {
    @track handleVisibility = false;
    @track comments;
    @track reviewstatuscomments = {};
    @track statusOptions;
    @track ModalColumns = ModalColumns;
    @track dataTableColumns = dataTableColumns;
    @track isOpenModal = false;
    @track actionShowModal = false;
    @track openmodel = false;
    @track assignTierpopupshow = [];
    @track bShowsignatureModal = false;
    @track getContext;
    @track supervisor;
    @track caseworker;
    @track currenttablepage = [];
    @track ApprovalCommentsvalue;
    @track ApprovalStatus__c;
    @track assignDisable = true;
    @track filedisable = true;
    @track HideDatatable = false;
    @track MonitoringStatusValidation;
    @track SignatureValidation;
    @track SignatureValidationView;
    @track SignatureShowHide = false;

    //Page Freeze Initialization
    @track isDisable = false;
    @track isDisableTierApproval = false;
    @track isDisablesignature = false;
    @track isSaveAsDraftDisable = false;
    @track isSubmitBtnDisable = false;
    // @track profileName = "Caseworker";//"Supervisor";
    @track selectStatusShowHide = false;
    @track tierapprovalShowHide = true;
    @track preSelectedRows = [];
    @track workItemId = '';
    @track ApprovalCommentsvalueview;
    @track LicenseEnddate;
    @track LicenseStartdate;
    @track licenceDateValidationFlag = true;


    refreshIcon = images + '/refresh-icon.svg';

    //Sundar Modified
    @track isMonitoringNotCompleted = false;
    @track hideDraftBtn = false;
    @track Spinner = true;

    // Application id 
    get applicationId() {
        return sharedData.getApplicationId();
    }

    // Application id 
    get profileName() {
        return sharedData.getUserProfileName();
    }

    // @api
    // licenceDateValidation(event) {
    //     this.licenceDateValidationFlag = event;
    // }
    @api getLicenseDateFlag;

    connectedCallback() {
        if (this.profileName == "Supervisor") {
            this.hideDraftBtn = true;
            this.selectStatusShowHide = true;
            this.tierapprovalShowHide = false;
        }

        if (!this.ApprovalStatus__c) {
            this.isDisableTierApproval = true;
            this.isSubmitBtnDisable = true;
        }
        this.getReviewDetailsInitialLoad();
    }

    getReviewDetailsInitialLoad() {
        getReviewDetails({
                applicationid: this.applicationId
            })
            .then(result => {
                if (result.length > 0) {
                    if (this.profileName == "Supervisor") {
                        this.ApprovalStatus__c = result[0].SupervisorReview__c;
                        this.ApprovalCommentsvalue = result[0].SupervisorComments__c;
                        // this.SignatureValue = result[0].signatureUrl;
                    } else if (this.profileName == "Caseworker" || this.profileName == "System Administrator") {
                        this.ApprovalStatus__c = result[0].ApprovalStatus__c;
                        this.ApprovalCommentsvalue = result[0].ApprovalComments__c;
                        // this.SignatureValue = result[0].signatureUrl;                     
                    }

                    if (this.profileName == "Caseworker" || this.profileName == "System Administrator") {
                        if (result[0].Status__c == 'Caseworker Submitted' || result[0].Status__c == 'Approved' || result[0].Status__c == 'Rejected') {
                            this.isDisable = true;
                            this.isDisableTierApproval = true;
                            this.isDisablesignature = true;
                            this.isSaveAsDraftDisable = true;
                            this.isSubmitBtnDisable = true;
                        } else if (!this.ApprovalStatus__c) {
                            this.isDisableTierApproval = true;
                            this.isSubmitBtnDisable = true;
                        } else {
                            this.isDisable = false;
                            this.isDisableTierApproval = false;
                            this.isDisablesignature = false;
                            this.isSaveAsDraftDisable = false;
                            this.isSubmitBtnDisable = false;
                        }
                    } else if (this.profileName == "Supervisor") {
                        if (result[0].Status__c == 'Approved' || result[0].Status__c == 'Supervisor Rejected') {
                            this.isDisable = true;
                            this.isDisableTierApproval = true;
                            this.isDisablesignature = true;
                            this.isSaveAsDraftDisable = true;
                            this.isSubmitBtnDisable = true;
                        } else {
                            this.isDisable = false;
                            this.isDisableTierApproval = false;
                            this.isDisablesignature = false;
                            this.isSaveAsDraftDisable = false;
                            this.isSubmitBtnDisable = false;
                        }
                    }



                    if (result[0].ApprovalStatus__c == 'None') {
                        this.isDisableTierApproval = true;
                        this.isSubmitBtnDisable = true;
                    }
                    if (!result[0].LicenseStartdate__c && !result[0].LicenseEnddate__c) {
                        this.getLicenseDateFlag = true;
                    } else {
                        this.getLicenseDateFlag = false;
                    }
                    // this.LicenseStartdate = result[0].LicenseStartdate__c;
                    // this.LicenseEnddate = result[0].LicenseEnddate__c;

                    this.getDatatablevalues();
                    this.Spinner = false;
                }
            })
    }


    getDatatablevalues() {
        getDatatableDetails({
                applicationid: this.applicationId
            })
            .then(result => {
                let resultArray = [];
                result = JSON.parse(result);
                this.SignatureValidation = result[0].signatureUrl;
                this.preSelectedRows = [];
                if ((this.profileName == "Caseworker" || this.profileName == "System Administrator") && result[0].application.Caseworker__r !== undefined) {
                    resultArray.push({
                        SubmittedDate__c: utils.formatDate(result[0].application.SubmittedDate__c),
                        authorName: result[0].application.Caseworker__r.Name,
                        authorDesignation: 'OLM',
                        status: result[0].application.ApprovalStatus__c == 'Approved' ? 'Submit for Review' : result[0].application.ApprovalStatus__c,
                        assignedToName: result[0].application.Supervisor__r.Name,
                        ApprovalCommentsvalue: result[0].application.ApprovalComments__c,
                        supervisorDesignation: 'Supervisor',
                        SignatureValue: result[0].signatureUrl
                    });
                    this.currenttablepage = resultArray;
                    let array = [];
                    array.push(result[0].application.Supervisor__c);
                    this.preSelectedRows = array;
                    this.HideDatatable = true;
                    if (!this.SignatureValidation) {
                        this.SignatureShowHide = false;
                    } else {
                        this.SignatureShowHide = true;
                        this.SignatureValidationView = '/sfc/servlet.shepherd/version/download/' + result[0].signatureUrl;
                    }
                }
                if (result.length > 0) {
                    for (var i = 0; i < result.length; i++) {
                        if (result[i].application.Steps !== undefined) {
                            if (result[i].application.Steps.totalSize === 1) {
                                let signUrlIndex = result[i].signatureUrl.findIndex(sign => sign.OwnerId === result[i].application.Steps.records[0].ActorId);
                                let signUrl = signUrlIndex >= 0 ? result[i].signatureUrl[signUrlIndex].Id : '';
                                resultArray.push({
                                    authorName: result[i].application.Steps.records[0].Actor.Name,
                                    status: result[i].application.Steps.records[0].StepStatus == 'Started' ? 'Submit for Review' : (result[i].application.StepStatus == 'Removed' ? 'Returned' : result[0].application.StepStatus),
                                    assignedToName: result[i].application.Workitems.records[0].Actor.Name,
                                    ApprovalCommentsvalue: result[i].application.Steps.records[0].Comments,
                                    authorDesignation: result[i].application.Steps.records[0].Actor.Profile.Name, //== 'Supervisor' ? 'Supervisor' : 'OLM',
                                    supervisorDesignation: result[i].application.Workitems.records[0].Actor.Profile.Name,
                                    SubmittedDate__c: utils.formatDate(result[i].application.Steps.records[0].CreatedDate),
                                    SignatureValue: signUrl
                                });
                            } else if (result[i].application.Steps.totalSize > 1) {
                                let datas = result[i].application.Steps.records;
                                for (let data in datas) {
                                    let signUrlIndex = result[i].signatureUrl.findIndex(sign => sign.OwnerId === datas[data].ActorId);
                                    let signUrl = signUrlIndex >= 0 ? result[i].signatureUrl[signUrlIndex].Id : '';
                                    resultArray.push({
                                        authorName: datas[data].Actor.Name,
                                        status: datas[data].StepStatus == 'Started' && datas[data].Actor.Profile.Name !== "Supervisor" ? 'Submit for Review' : datas[data].StepStatus == 'Started' ? 'Approved' : datas[data].StepStatus == 'Removed' ? 'Returned' : datas[data].StepStatus,
                                        assignedToName: datas[parseInt(data) - 1] !== undefined ? datas[parseInt(data) - 1].Actor.Name : ' - ',
                                        ApprovalCommentsvalue: datas[data].Comments,
                                        authorDesignation: datas[data].Actor.Profile.Name, //== 'Supervisor' ? 'Supervisor' : 'OLM',
                                        supervisorDesignation: datas[parseInt(data) - 1] !== undefined ? datas[data - 1].Actor.Profile.Name : ' - ',
                                        SubmittedDate__c: utils.formatDate(datas[data].CreatedDate),
                                        SignatureValue: signUrl
                                    });

                                    if (datas[data].StepStatus == "Removed" && this.profileName != "Supervisor") {
                                        let array = [];
                                        array.push(datas[data].Actor.Id);
                                        this.preSelectedRows = array;
                                    }

                                }
                            }
                            if (result[i].application.Steps.totalSize === 1)
                                this.workItemId = result[i].application.Workitems.records[0].Id;
                            // if (result.length > 1 && result[0].application.Steps.records[1].StepStatus=="Removed" && result[1].application.Steps.totalSize === 1)
                            //     this.workItemId = result[1].application.Workitems.records[0].Id;

                            this.HideDatatable = true;

                            // this.ReviewStatus__c = result[0].application.Steps.records[0].StepStatus == 'Started' ? 'Approved' : result[0].application.StepStatus;
                            // this.ApprovalCommentsvalue = result[0].application.Steps.records[0].Comments,
                            // this.SignatureValue = result[0].signatureUrl;
                        }
                    }
                }
                this.currenttablepage = resultArray;

                if (resultArray.length > 0) {
                    this.SignatureShowHide = true;
                    if (resultArray.length > 1) {
                        if (this.profileName == "Supervisor") {
                            this.SignatureValidationView = '/sfc/servlet.shepherd/version/download/' + resultArray[resultArray.length - 2].SignatureValue;
                        } else {
                            this.SignatureValidationView = '/sfc/servlet.shepherd/version/download/' + resultArray[resultArray.length - 1].SignatureValue;
                        }
                    } else {
                        if (this.profileName == "Supervisor") {
                            this.SignatureShowHide = false;
                        } else {
                            this.SignatureValidationView = '/sfc/servlet.shepherd/version/download/' + resultArray[resultArray.length - 1].SignatureValue;
                        }
                    }

                } else {
                    this.SignatureShowHide = false;
                }

            });
    }

    openmodal() {
        this.openmodel = true;
    }
    DoneModal() {
        this.openmodel = false;
    }
    closeModal() {
        this.openmodel = false;
    }
    closeModalAction() {
        this.openmodel = false;
    }
    handleCloseModal() {
        this.isOpenModal = false;
    }

    actionCloseModal() {
        this.actionShowModal = false;
    }

    signaturemodal() {
        // to open modal window set 'bShowsignatureModal' track value as true
        this.bShowsignatureModal = true;

    }

    closeModal() {
        this.SignatureShowHide = true;
        this.bShowsignatureModal = false; // to close modal window set 'bShowsignatureModal' tarck value as false
    }



    //PickList for Status
    @wire(fatchPickListValue, {
        objInfo: {
            'sobjectType': 'Application__c'
        },
        picklistFieldApi: 'ApprovalStatus__c'
    })
    wireduserDetails2({
        error,
        data
    }) {
        if (data && (this.profileName == USER_PROFILE_NAME.USER_CASE_WRKR || this.profileName == USER_PROFILE_NAME.USER_SYS_ADMIN || this.profileName == USER_PROFILE_NAME.USER_PROVIDER_COMMUNITY)) {
            this.statusOptions = data;
        } else if (error) {

        }
    }

    //PickList for Status
    @wire(fatchPickListValue, {
        objInfo: {
            'sobjectType': 'Application__c'
        },
        picklistFieldApi: 'SupervisorReview__c'
    })
    wireduserDetails3({
        error,
        data
    }) {
        if (data && this.profileName == "Supervisor") {
            this.statusOptions = data;
        } else if (error) {

        }
    }

    changeStatusHandler(event) {
        this.ApprovalStatus__c = event.detail.value;

        if (this.ApprovalStatus__c == 'Approved')
            this.isDisableTierApproval = false;

        if (this.ApprovalStatus__c == 'None')
            this.isSubmitBtnDisable = true;
        else
            this.isSubmitBtnDisable = false;
    }

    commentschangeHandler(event) {
        this.ApprovalCommentsvalue = event.detail.value;
    }

    @wire(getAllUsersList)
    wireduserDetails1({
        error,
        data
    }) {
        /*eslint-disable*/
        if (data) {
            let userslist = [];
            userslist = data.filter(item => item.Name != "Automated Process" && item.Name != "Platform Integration User" && item.Name != "System");
            this.assignTierpopupshow = userslist;
        } else if (error) {

        }
    }

    handleRowSelection = event => {
        this.supervisor = event.detail.selectedRows[0].Id
        this.caseworker = userId;
        this.assignDisable = false;
    }

    dataAction(event) {
        let actiondt = event.detail.row;
        this.AuthorValue = event.detail.row.authorName;
        this.DateValue = event.detail.row.SubmittedDate__c;
        this.StatusValue = event.detail.row.status;
        this.ApprovalCommentsvalueview = event.detail.row.ApprovalCommentsvalue;
        this.SignatureValue = '/sfc/servlet.shepherd/version/download/' + event.detail.row.SignatureValue;
        this.openmodel = true;
    }

    toggleVisibility() {
        this.handleVisibility = false;
    }

    AuthorChange(event) {
        this.AuthorValue = event.target.value;
    }
    StatusChange(event) {
        this.StatusValue = event.target.value;
    }
    DateChange(event) {
        this.DateValue = event.target.value;
    }
    CommentsChange(event) {
        this.CommentsValue = event.target.value;
    }
    SignatureChange(event) {
        this.SignatureValue = event.target.value;
    }

    handleFilesChange(event) {
        let strFileNames = '';
        // Get the list of uploaded files
        const uploadedFiles = event.detail.files;

        for (let i = 0; i < uploadedFiles.length; i++) {
            strFileNames += uploadedFiles[i].name + ', ';

        }
        this.dispatchEvent(
            new ShowToastEvent({
                title: 'Success!!',
                message: strFileNames + ' Files uploaded Successfully!!!',
                variant: 'success',
            }),
        );
    }

    handleOpenModal() {
        this.isOpenModal = true;
        let localvar = [...this.assignTierpopupshow]
        for (var i = 0; i < localvar.length; i++) {
            var x = {
                ProfileName: localvar[i].Profile.Name
            };
            var y = Object.assign(x, localvar[i]);
            localvar[i] = y;
        }
        this.assignTierpopupshow = localvar;
    }

    handleAssignModal() {
        let myobjapp = {
            'sobjectType': 'Application__c'
        };
        myobjapp.Caseworker__c = this.caseworker;
        myobjapp.Supervisor__c = this.supervisor;
        if (this.profileName == "Caseworker" || this.profileName == "System Administrator") {
            myobjapp.ApprovalStatus__c = this.ApprovalStatus__c;
            myobjapp.ApprovalComments__c = this.ApprovalCommentsvalue;
        } else if (this.profileName == "Supervisor") {
            myobjapp.SupervisorReview__c = this.ApprovalStatus__c;
            myobjapp.SupervisorComments__c = this.ApprovalCommentsvalue;
        }
        myobjapp.SubmittedDate__c = utils.formatDateYYYYMMDD(new Date());


        if (this.applicationId)
            myobjapp.Id = this.applicationId;

        updateCaseworkerdetails({
                objSobjecttoUpdateOrInsert: myobjapp
            })
            .then(result => {
                this.isOpenModal = false;
                this.getDatatablevalues();
            })
            .catch(error => {
                this.isOpenModal = false;
                this.error = error.message;
            });
    }





    /**********Signature******/

    @api recordId;

    //event listeners added for drawing the signature within shadow boundary
    constructor() {
        super();
        this.template.addEventListener('mousemove', this.handleMouseMove.bind(this));
        this.template.addEventListener('mousedown', this.handleMouseDown.bind(this));
        this.template.addEventListener('mouseup', this.handleMouseUp.bind(this));
        this.template.addEventListener('mouseout', this.handleMouseOut.bind(this));
    }

    //retrieve canvase and context
    renderedCallback() {
        canvasElement = this.template.querySelector('canvas');
        ctx = canvasElement.getContext("2d");
    }

    //handler for mouse move operation
    handleMouseMove(event) {
        this.searchCoordinatesForEvent('move', event);
    }

    //handler for mouse down operation
    handleMouseDown(event) {
        this.searchCoordinatesForEvent('down', event);
    }

    //handler for mouse up operation
    handleMouseUp(event) {
        this.searchCoordinatesForEvent('up', event);
    }

    //handler for mouse out operation
    handleMouseOut(event) {
        this.searchCoordinatesForEvent('out', event);
    }

    /*
        handler to perform save operation.
        save signature as attachment.
        after saving shows success or failure message as toast
    */
    handleSaveClick() {
        //set to draw behind current content
        ctx.globalCompositeOperation = "destination-over";
        ctx.fillStyle = "#FFF"; //white
        ctx.fillRect(0, 0, canvasElement.width, canvasElement.height);

        //convert to png image as dataURL
        dataURL = canvasElement.toDataURL("image/png");
        //convert that as base64 encoding
        convertedDataURI = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");

        //call Apex method imperatively and use promise for handling sucess & failure
        saveSign({
                strSignElement: convertedDataURI,
                recId: this.applicationId
            })
            .then(result => {
                this.getDatatablevalues();
                this.bShowsignatureModal = false;
                this.isDisableTierApproval = this.ApprovalStatus__c == "Rejected" ||
                    this.ApprovalStatus__c == "None" ?
                    true :
                    false;

                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: ' Signature saved Successfully',
                        variant: 'success',
                    }),
                );
            })
            .catch(error => {

                //show error message
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error  in creating Signature',
                        message: error.body.message,
                        variant: 'error',
                    }),
                );
            });

    }

    //clear the signature from canvas
    handleClearClick() {
        this.SignatureShowHide = false;
        ctx.clearRect(0, 0, canvasElement.width, canvasElement.height);
    }

    searchCoordinatesForEvent(requestedEvent, event) {
        // event.preventDefault();
        if (requestedEvent === 'down') {
            this.setupCoordinate(event);
            isDownFlag = true;
            isDotFlag = true;
            if (isDotFlag) {
                this.drawDot();
                isDotFlag = false;
            }
        }
        if (requestedEvent === 'up' || requestedEvent === "out") {
            isDownFlag = false;
        }
        if (requestedEvent === 'move') {
            if (isDownFlag) {
                this.setupCoordinate(event);
                this.redraw();
            }
        }
    }

    //This method is primary called from mouse down & move to setup cordinates.
    setupCoordinate(eventParam) {
        //get size of an element and its position relative to the viewport 
        //using getBoundingClientRect which returns left, top, right, bottom, x, y, width, height.
        const clientRect = canvasElement.getBoundingClientRect();
        prevX = currX;
        prevY = currY;
        currX = eventParam.clientX - clientRect.left;
        currY = eventParam.clientY - clientRect.top;
    }

    //For every mouse move based on the coordinates line to redrawn
    redraw() {
        ctx.beginPath();
        ctx.moveTo(prevX, prevY);
        ctx.lineTo(currX, currY);
        ctx.strokeStyle = x; //sets the color, gradient and pattern of stroke
        ctx.lineWidth = y;
        ctx.closePath(); //create a path from current point to starting point
        ctx.stroke(); //draws the path
    }

    //this draws the dot
    drawDot() {
        ctx.beginPath();
        ctx.fillStyle = x; //blue color
        ctx.fillRect(currX, currY, y, y); //fill rectrangle with coordinates
        ctx.closePath();
    }

    // Save As Draft 
    saveAsDraftMethod() {
        if (!this.ApprovalStatus__c)
            return this.dispatchEvent(utils.toastMessage("Please Select Status", "Warning"));

        /*if (!this.SignatureValidation)
            return this.dispatchEvent(utils.toastMessage("Signature Is Mandatory", "Warning"));*/

        let myObj = {
            'sobjectType': 'Application__c'
        };

        myObj.ApprovalStatus__c = this.ApprovalStatus__c;
        myObj.ApprovalComments__c = this.ApprovalCommentsvalue;
        // myObj.SignatureValue = this.SignatureValue;
        myObj.Status__c = 'For Review';

        if (this.applicationId)
            myObj.Id = this.applicationId;

        updateapplicationdetailsDraft({
                objSobjecttoUpdateOrInsert: myObj
            })
            .then(result => {
                this.actionShowModal = true;
                this.dispatchEvent(utils.toastMessage("Application has been saved successfully !", "success"));
                window.location.reload();
            })
            .catch(error => {
                this.actionShowModal = false;
                this.error = error.message;
            });
    }

    submitMethod() {
        if (this.ApprovalStatus__c == 'Approved' || this.ApprovalStatus__c == 'Rejected')
            this.getMonitoringDetails();
        else
            this.submitMethod1();
    }

    getMonitoringDetails() {
        getmonitoringDetails({
                applicationid: this.applicationId
            })
            .then(result => {
                if (result.length == 0)
                    this.isMonitoringNotCompleted = true;
                else {
                    if (result.length > 0) {
                        let statusArray = [];
                        result.forEach(row => {
                            statusArray.push(row.Status__c);
                        });

                        if (statusArray.includes('Draft'))
                            this.isMonitoringNotCompleted = true;
                        else
                            this.isMonitoringNotCompleted = false;
                    }
                }
                this.submitMethod1();
            })
    }

    //Submit
    submitMethod1() {
        if (!this.ApprovalStatus__c)
            return this.dispatchEvent(utils.toastMessage("Please Select Status", "Warning"));

        if (this.isMonitoringNotCompleted)
            return this.dispatchEvent(utils.toastMessage("Monitoring should be completed to submit this application", "Warning"));

        if (!this.SignatureValidation)
            return this.dispatchEvent(utils.toastMessage("Signature Is Mandatory", "Warning"));

        if (this.getLicenseDateFlag)
            return this.dispatchEvent(utils.toastMessage("Please update License Dates", "Warning"));

        let myObj = {
            'sobjectType': 'Application__c'
        };

        if (this.profileName == "Caseworker" || this.profileName == "System Administrator") {
            myObj.ApprovalStatus__c = this.ApprovalStatus__c;
            myObj.ApprovalComments__c = this.ApprovalCommentsvalue;
        } else if (this.profileName == "Supervisor") {
            myObj.SupervisorReview__c = this.ApprovalStatus__c;
            // myObj.SupervisorReview__c = "Approve";
            myObj.SupervisorComments__c = this.ApprovalCommentsvalue;
        }
        var today = new Date();
        var monthDigit = today.getMonth() + 1;
        if (monthDigit <= 9) {
            monthDigit = '0' + monthDigit;
        }
        var dayDigit = today.getDate();
        if (dayDigit <= 9) {
            dayDigit = '0' + dayDigit;
        }
        var date = today.getFullYear() + "-" + monthDigit + "-" + dayDigit;
        myObj.SubmittedDate__c = date;

        if (this.profileName == "Caseworker" || this.profileName == "System Administrator") {
            myObj.Status__c = this.ApprovalStatus__c == 'Approved' ? 'Caseworker Submitted' : this.ApprovalStatus__c;
        } else if (this.profileName == "Supervisor") {
            myObj.Status__c = this.ApprovalStatus__c == 'Rejected' ? 'Supervisor Rejected' : this.ApprovalStatus__c == 'Returned to Worker' ? 'Returned' : this.ApprovalStatus__c;
        }

        if (this.applicationId)
            myObj.Id = this.applicationId;


        let preSelectedRows = this.preSelectedRows[0] == undefined ? '' : this.preSelectedRows[0];
        let workItemId = this.workItemId === '' ? '' : this.workItemId;


        if (myObj.Status__c != "Rejected" && myObj.Status__c != "None") {


            updateapplicationdetails({
                    objSobjecttoUpdateOrInsert: myObj,
                    supervisorId: preSelectedRows,
                    workItemId: workItemId
                })
                .then(result => {
                    this.dispatchEvent(utils.toastMessage("Application has been submitted successfully", "Success"));
                    window.location.reload();
                })
                .catch(error => {
                    this.error = error.message;
                });
        } else {
            updateapplicationdetailsDraft({
                    objSobjecttoUpdateOrInsert: myObj
                })
                .then(result => {
                    this.actionShowModal = true;
                    this.dispatchEvent(utils.toastMessage("Application Saved as Successfully !", "Success"));
                    window.location.reload();
                })
                .catch(error => {
                    this.actionShowModal = false;
                    this.error = error.message;
                });
        }

    }
}