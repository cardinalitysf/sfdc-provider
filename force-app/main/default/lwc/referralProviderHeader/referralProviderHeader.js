/**
 * @Author        : G.Tamilarasan
 * @CreatedOn     : March 03, 2020
 * @Purpose       : This component contains CASE console
 **/

import { LightningElement, wire, track } from 'lwc';
import fatchPickListValue from '@salesforce/apex/referralProviderHeaderController.getPickListValues';
import getIdAfterInsertSOQL from "@salesforce/apex/referralProviderHeaderController.getIdAfterInsertSOQL";
import getCaseHeaderDetails from "@salesforce/apex/referralProviderHeaderController.getCaseHeaderDetails";
import { CJAMS_CONSTANTS } from "c/constants";
import * as sharedData from 'c/sharedData';
import { utils } from "c/utils";


export default class ReferralProviderHeader extends LightningElement {

    @track error;
    @track stageNameValues;
    @track getrecords;
    @track Agency__c;
    @track showhidecaserecord = false;
    @track isDisableCommunicationReceived = false;
    @track NewReferralNumber;
    @track maxdate;
    blnDecideLabel = false;

    //get the existing CASE ID 
    get caseid() {
        return sharedData.getCaseId();
    }

    //Show the label header based on the New or Existing CASE 
    get labelHeader() {
        return this.blnDecideLabel ? 'EXISTING REFERRALS' : 'NEW PRIVATE REFERRALS';
    }

    //Function used to create the new CASE
    connectedCallback() {
        this.NewReferralNumber = this.caseid;
        this.blnDecideLabel = this.NewReferralNumber ? true : false;
        if (!this.NewReferralNumber) {
            let myobjcase = { 'sobjectType': 'Case' };
            myobjcase.Status = "New";
            getIdAfterInsertSOQL({ objIdSobjecttoUpdateOrInsert: myobjcase })
                .then(result => {
                    sharedData.setCaseId(result);
                    this.NewReferralNumber = result;
                    this.headerload();
                })
                .catch(errors => {
                    let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                    return this.dispatchEvent(utils.handleError(errors, config));
                });
        }
        else {
            this.headerload();
        }
    }

    //Function used to disable the future date in received date field
    futuredatedisable() {
        var today = new Date();
        var monthDigit = today.getMonth() + 1;
        if (monthDigit <= 9) {
            monthDigit = '0' + monthDigit;
        }
        var dayDigit = today.getDate();
        if (dayDigit <= 9) {
            dayDigit = '0' + dayDigit;
        }
        var hourDigit = today.getHours();
        if (hourDigit <= 9) {
            hourDigit = '0' + hourDigit;
        }
        var minuteDigit = today.getMinutes();
        if (minuteDigit <= 9) {
            minuteDigit = '0' + minuteDigit;
        }
        var secondDigit = today.getSeconds();
        if (secondDigit <= 9) {
            secondDigit = '0' + secondDigit;
        }
        var date = today.getFullYear() + "-" + monthDigit + "-" + dayDigit;
        var time = hourDigit + ":" + minuteDigit + ":" + secondDigit;
        //        var finaldate = date + 'T' + time + '.000'; 
        //         var finaldate = date + 'T' + '23:59:59' + '.000';
        var finaldate = date + 'T' + time + '.000';
        this.maxdate = finaldate;
    }

    //Function used to retrieve the CASE details
    headerload() {
        getCaseHeaderDetails({ newreferral: this.NewReferralNumber }).then(result => {
            this.getrecords = result[0];
            if (result[0].Status == "Approved" || result[0].Status == "Rejected") {
                this.isDisableCommunicationReceived = true;
            }
            else {
                this.isDisableCommunicationReceived = false;
            }

            if (!this.getrecords.ReceivedDate__c) {
                const target = { "ReceivedDate__c": this.getrecords.CreatedDate };
                const returnedTarget = Object.assign(target, this.getrecords);
                this.getrecords = returnedTarget;
                sharedData.setReceivedDateValue(this.getrecords.ReceivedDate__c);
            }
            else {
                sharedData.setReceivedDateValue(result[0].ReceivedDate__c);
            }
            sharedData.setCommunicationValue(result[0].Origin);
            this.showhidecaserecord = true;
        }).catch(errors => {
            let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
            return this.dispatchEvent(utils.handleError(errors, config));
        });
        this.futuredatedisable();
    }

    //Function used to disconnect all the connections
    disconnectedCallback() {
        this.showhidecaserecord = false;
        this.NewReferralNumber = null;
        sharedData.setCaseId(undefined);
        this.maxdate = undefined;
        sharedData.setReceivedDateValue(undefined);
        sharedData.setCommunicationValue(undefined);
    }

    //Function used to get the data from Communication field
    communicationchange(event) {
        sharedData.setCommunicationValue(event.detail.value);
    }

    //Function used to get the data from Received date field
    receiveddatechange(event) {
        sharedData.setReceivedDateValue(event.detail.value);
    }

    //Function used to fetch communication picklist values from object
    @wire(fatchPickListValue, {
        objInfo: { 'sobjectType': 'Case' },
        picklistFieldApi: 'Origin'
    })
    wireduserDetails1({ error, data }) {
        /*eslint-disable*/

        if (data) {
            this.stageNameValues = data;
        }
        else if (error) {
            this.dispatchEvent(utils.toastMessage("Error in fetching Origin", "error"));

        }
    }

    //Function used to fetch agency picklist values from object
    @wire(fatchPickListValue, {
        objInfo: { 'sobjectType': 'Case' },
        picklistFieldApi: 'Agency__c'
    })
    wireduserDetails2({ error, data }) {
        /*eslint-disable*/

        if (data) {
            this.Agency__c = data[0].label;
        }
        else if (error) {
            this.dispatchEvent(utils.toastMessage("Error in fetching Agency", "error"));

        }
    }

    PageRedirecttoDashBoard() {
        const onClickNewDirection = new CustomEvent('redirecttoreferralhome', {
            detail: {
                first: false
            }
        });
        this.dispatchEvent(onClickNewDirection);
    }
}