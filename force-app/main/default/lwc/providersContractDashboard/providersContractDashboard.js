/**
 * @Author        : G.sathishkumar
 * @CreatedOn     : April 16, 2020
 * @Purpose       : ProvidersContractDashboard.
 * @updatedBy     :
 * @updatedOn     :
 **/

import {
    LightningElement,
    track,
    wire
} from 'lwc';
import ProvidersContract from "@salesforce/apex/ProvidersContractDashboard.ProvidersContract";
import ProgramAddList from "@salesforce/apex/ProvidersContractDashboard.ProgramAddList";
import {
    deleteRecord
} from "lightning/uiRecordApi";
import {
    constPopupVariables,CJAMS_CONSTANTS
} from 'c/constants';
import USER_ID from '@salesforce/user/Id';
import {
    utils
} from "c/utils";
import * as shareddata from 'c/sharedData';

const columns = [{
    label: 'CONTRACT NO',
    sortable: true,
    type: 'button',
    fieldName: 'id',
    typeAttributes: {
        label: {
            fieldName: 'Contract_Name'
        },
        class: "blue",
        target: "_self",
        color: "blue"
    }
},
{
    label: 'LICENSE NO',
    fieldName: 'Name',
    type: 'text',
    sortable: true,
},
{
    label: 'PROGRAM TYPE',
    fieldName: 'ProgramType__c',
    type: 'button',
    sortable: true,
    typeAttributes: {
        name: 'dontRedirect',
        label: {
            fieldName: 'ProgramType__c'
        }
    },
    cellAttributes: {
        class: "title"
    }
},
{
    label: '$ AMOUNT',
    fieldName: 'TotalContractAmount__c',
    type: 'number',
    sortable: true
},
{
    label: 'START DATE',
    fieldName: 'ContractStartDate__c',
    type: 'text',
    sortable: true
},
{
    label: 'END DATE',
    fieldName: 'ContractEndDate__c',
    type: 'text',
    sortable: true
},
{
    label: 'CONTRACTED BEDS',
    fieldName: 'TotalContractBed__c',
    type: 'number',
    sortable: true,
},
{
    label: 'STATUS',
    fieldName: 'ContractStatus__c',
    type: 'text',
    cellAttributes: {
        class: {
            fieldName: 'statusClass'
        }
    }
},

{
    label: 'Action',
    type: 'button',
    initialWidth: 100,
    fieldName: 'id',
    typeAttributes: {
        iconName: 'utility:preview',
        name: 'viewRecordRow',
        title: 'View',
        disabled: false,
        initialWidth: 20,
        target: '_self'
    },
},
{
    type: 'button',
    initialWidth: 40,
    fieldName: 'id',
    typeAttributes: {
        iconName: 'utility:edit',
        name: 'editRecordRow',
        title: 'Edit',
        disabled: false,
        initialWidth: 20,
        class: "action-position"
    }
},
{
    type: 'button',
    initialWidth: 40,
    fieldName: 'id',
    typeAttributes: {
        iconName: 'utility:delete',
        name: 'deleteRecordRow',
        title: 'Delete',
        disabled: false,
        initialWidth: 20,
        class: "delLeft-position del-red"
    }
},


];

export default class ProvidersContractDashboard extends LightningElement {


    @track columns = columns;
    @track currentPageContractData = [];
    @track deleteRowId = null;
    @track disableStatus = false;
    @track addProgram;
    @track Spinner = true;
    @track norecorddisplay = true;

    //Pagination Tracks    
    @track page = 1;
    perpage = 10;
    setPagination = 5;
    @track totalRecordsCount = 0;
    @track totalRecords;
    //delpopup 

    @track openmodelDel = false;
    @track isSuperUserFlow = false;

    closeModal() {
        this.openmodelDel = false;
    }

    get ProfileUser() {
        let data = shareddata.getUserProfileName();
        if (data === 'Supervisor') {
            this.isSuperUserFlow = true;
        } else {
            this.isSuperUserFlow = false;
        }
        return data;
    }

    addOpenModal() {
        if (this.ProfileUser == 'Supervisor') {
            this.dispatchEvent(utils.toastMessage("Cannot add contract", "warning"));
        } else if (this.ProfileUser == 'Caseworker' || 'System Administrator') {
            if (this.addProgram.length == 0) {
                this.dispatchEvent(utils.toastMessage("There are no approved program", "warning"));
            } else {
                shareddata.setLicenseId(undefined);
                shareddata.setContractAction(undefined);
                const oncaseid = new CustomEvent('redirecttocontractadd', {
                    detail: {
                        first: false
                    }
                });
                this.dispatchEvent(oncaseid);
            }
        }
    }



    sortBy(field, reverse, primer) {
        const key = primer ?
            function (x) {
                return primer(x[field]);
            } :
            function (x) {
                return x[field];
            };
        return function (a, b) {
            a = key(a);
            b = key(b);
            return reverse * ((a > b) - (b > a));
        };
    }
    onHandleSort(event) {
        const {
            fieldName: sortedBy,
            sortDirection
        } = event.detail;
        const cloneData = [...this.currentPageContractData];
        cloneData.sort(this.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1));
        this.currentPageContractData = cloneData;
        this.sortDirection = sortDirection;
        this.sortedBy = sortedBy;
    }
    get providerId() {
        let data = shareddata.getProviderId();
        return data;
    }

    handleRowAction(event) {

        shareddata.setContractId(event.detail.row.id);
        shareddata.setLicenseId(event.detail.row.License__c);
        shareddata.setContractAction(event.detail.action.name);
        const action = event.detail.action;
        const row = event.detail.row;
        switch (action.name) {
            case 'viewRecordRow':
                break;
            case 'editRecordRow':
                break;
            case 'deleteRecordRow':
                if (event.detail.row.ContractStatus__c == 'Expired' || event.detail.row.ContractStatus__c == 'Active' || event.detail.row.ContractStatus__c == 'Inactive' || event.detail.row.ContractStatus__c == 'Returned' || event.detail.row.ContractStatus__c == 'Submitted') {

                    this.disableStatus = true;
                    this.dispatchEvent(utils.toastMessage("Can not delete submitted contract", "warning"));
                } else {
                    this.disableStatus = false;
                    this.openmodelDel = true;
                    this.deleteRowId = row.id;
                }
                break;
        }
        if (action.name == undefined) {
            const oncaseid = new CustomEvent('redirecttocontractmanage', {
                detail: {
                    first: false
                }
            });
            this.dispatchEvent(oncaseid);
        } else if (action.name == 'editRecordRow') {
            if (event.detail.row.ContractStatus__c == 'Expired' || event.detail.row.ContractStatus__c == 'Active' || event.detail.row.ContractStatus__c == 'Inactive' || event.detail.row.ContractStatus__c == 'Submitted') {

                this.disableStatus = true;
                this.dispatchEvent(utils.toastMessage("Can not edit submitted contract", "warning"));
            } else {

                this.disableStatus = false;
                const oncaseid = new CustomEvent('redirecttocontractadd', {
                    detail: {
                        first: false
                    }
                });
                this.dispatchEvent(oncaseid);
            }
        } else if (action.name == 'viewRecordRow') {

            const oncaseid = new CustomEvent('redirecttocontractadd', {
                detail: {
                    first: false
                }
            });
            this.dispatchEvent(oncaseid);

        } else if (action.name != undefined) return;
    }

    deleteFrSure() {
        deleteRecord(this.deleteRowId)
            .then(() => {
                this.closeModal();
                this.dispatchEvent(utils.toastMessage(constPopupVariables.DELETESUCCESS, "success"));
                this.initialContractMemberLoad();

            }).catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DELETE_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));

            })
    }


    connectedCallback() {
        this.initialContractMemberLoad();
    }
    initialContractMemberLoad() {

        ProvidersContract({
            newcontract: this.providerId
        }).then(result => {
            var StatusForProfile = result.filter(item => {
                return ((item.ContractStatus__c !== 'In process') && (item.Supervisor__c === USER_ID));
            })
            if (result.length == 0) {
                this.norecorddisplay = false;
            }
            if (this.ProfileUser == 'Supervisor') {
                this.totalRecords = StatusForProfile.map(item => {
                    return {
                        id: item.Id,
                        License__c: item.License__c,
                        Contract_Name: item.Name,
                        Name: item.License__r.Name,
                        ProgramType__c: item.License__r.ProgramType__c,
                        TotalContractAmount__c: item.TotalContractAmount__c,
                        ContractStartDate__c: utils.formatDate(item.ContractStartDate__c),
                        ContractEndDate__c: utils.formatDate(item.ContractEndDate__c),
                        TotalContractBed__c: item.TotalContractBed__c,
                        ContractStatus__c: item.ContractStatus__c,
                        Supervisor: item.Supervisor__c,
                        statusClass: (item.ContractStatus__c == 'In process' || item.ContractStatus__c == 'Returned') ? 'Draft' : item.ContractStatus__c,
                    }
                });
            } else if (this.ProfileUser == 'Caseworker' || 'System Administrator') {
                this.totalRecords = result.map(item => {
                    return {
                        id: item.Id,
                        License__c: item.License__c,
                        Contract_Name: item.Name,
                        Name: item.License__r.Name,
                        ProgramType__c: item.License__r.ProgramType__c,
                        TotalContractAmount__c: item.TotalContractAmount__c,
                        ContractStartDate__c: utils.formatDate(item.ContractStartDate__c),
                        ContractEndDate__c: utils.formatDate(item.ContractEndDate__c),
                        TotalContractBed__c: item.TotalContractBed__c,
                        ContractStatus__c: item.ContractStatus__c,
                        Supervisor: item.Supervisor__c,
                        statusClass: (item.ContractStatus__c == 'In process' || item.ContractStatus__c == 'Returned') ? 'Draft' : item.ContractStatus__c,
                    }

                });
            }

            this.Provider__c = this.providerId;

            this.totalRecordsCount = this.totalRecords.length;
            this.pageData();
            this.Spinner = false;
        }).catch(errors => {
            let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
            return this.dispatchEvent(utils.handleError(errors, config));

        })
    }



    @wire(ProgramAddList, {
        objInfo: {
            sobjectType: "Application__c"
        },
        ProviderId: '$providerId'
    })

    wiredAddProgramDetails({
        error,
        data
    }) {
        if (data) {
            this.addProgram = data.map(item => {
                return {
                    Program__c: item.Program__c
                }
            })

        } else if (error) { }
    }

    pageData() {
        let page = this.page;
        let perpage = this.perpage;
        let startIndex = page * perpage - perpage;
        let endIndex = page * perpage;
        this.currentPageContractData = this.totalRecords.slice(startIndex, endIndex);
    }

    //For Pagination Child Bind
    hanldeProgressValueChange(event) {
        this.page = event.detail;
        this.pageData();
    }



}