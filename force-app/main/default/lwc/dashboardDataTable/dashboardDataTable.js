/**
 * @Author        : Preethi Bojarajan
 * @CreatedOn     : Apr 16,2020
 * @Purpose       : Reusable Dashboard With Card Clickable
 * @updatedBy     :
 * @updatedOn     :
 **/
import {
  LightningElement,
  track,
  api,
  wire
} from "lwc";

//Style Sheet Load
import {
  loadStyle
} from "lightning/platformResourceLoader";
import myResource from "@salesforce/resourceUrl/styleSheet";
//Utils
import * as sharedData from "c/sharedData";
import {
  CJAMS_CONSTANTS
} from "c/constants";
import {
  registerListener,
  unregisterAllListeners
} from 'c/pubsub';
import {
  CurrentPageReference
} from 'lightning/navigation';
export default class DashboardDataTable extends LightningElement {
  @wire(CurrentPageReference) pageRef;
  //Card Tracks
  @api byTypes = [];
  @api byClasses = [];
  @track initLoad = false;
  @api redirectToPublicProvider;
  //Table Tracks
  @api searchKey = "";
  @api tableRecords = [];
  @api columns;
  @track currentPageRecords = [];
  @track tableTopAlign = false;
  @track noRecordsFound = false;
  //Pagination Tracks
  @api totalRecordsCount = 0;
  perpage = 10;
  setPagination = 5;
  @track page = 1;
  @track Spinner = true;
  // Sort Function Tracks
  @track defaultSortDirection = "asc";
  @track sortDirection = "asc";
  @track noRecordsFoundMessage = CJAMS_CONSTANTS.NO_RECORDS_FOUND;

  // for sprint 5 dashboard
  @track publicReferralDataShow = false;
  @track privateReferralDataShow = true;
  @track showPublicPrivateBtn = true;
  @track showPrivateBtn = true;
  @track showPublicBtn = false;

  /* Style Sheet Loading */
  renderedCallback() {
    if (this.initLoad) return;

    this.initLoad = true;
    Promise.all([loadStyle(this, myResource + "/styleSheet.css")]);
    /* For Card Color Depending Status */
    let allCards = this.template.querySelectorAll(".slds-card");
    if (this.byClasses.length === allCards.length) {
      for (let i = 0; i < allCards.length; i++) {
        allCards[i].classList.add(this.byClasses[i].title);
        if (this.byClasses[i].init == true)
          allCards[i].classList.add(this.byClasses[i].card);
      }
    }
    setTimeout(() => {
      this.pageData();
    }, 1500);
  }
  connectedCallback() {

    registerListener("RedirectToPublicPrividerFromPrivateProvider", this.handleNewProviderPageRedirection, this);
  }
  handleNewProviderPageRedirection(val) {

    this.publicReferralDataShow = true;
    this.privateReferralDataShow = false;
    this.showPublicPrivateBtn = false;
    this.showPublicBtn = true;
    this.showPrivateBtn = false;
    this.redirectToPublicProvider = val;

  }

  handleByStatus(event) {
    this.Spinner = true;
    let index = 0;
    if (event.target.dataset.id !== undefined)
      index = this.byTypes.findIndex((b) => b.Id === event.target.dataset.id);
    else {
      let statusName = null;
      if (event.target.outerText.includes("\n"))
        statusName = event.target.outerText.split("\n")[0];
      else statusName = event.target.outerText;
      index =
        this.byTypes.findIndex((b) => b.title == statusName) >= 0 ?
        this.byTypes.findIndex((b) => b.title == statusName) :
        this.byTypes.findIndex((b) => b.count == statusName);
    }

    const selectedEvent = new CustomEvent("cardvaluechange", {
      detail: index
    });
    this.dispatchEvent(selectedEvent);

    let datas = this.template.querySelectorAll(".slds-card");
    for (let i = 0; i < datas.length; i++) {
      if (index === i) datas[i].classList.add(this.byClasses[i].card);
      else datas[i].classList.remove(this.byClasses[i].card);
    }
    this.page = 1;
    setTimeout(() => {
      this.pageData();
    }, 1000);
  }

  /* Pagination Function Start */
  pageData = () => {
    let page = this.page;
    let perpage = this.perpage;
    let startIndex = page * perpage - perpage;
    let endIndex = page * perpage;
    this.currentPageRecords = this.tableRecords.slice(startIndex, endIndex);
    this.noRecordsFound = this.currentPageRecords.length > 0 ? false : true;
    this.Spinner = false;
  };

  sortedPageData = (datas) => {
    let page = this.page;
    let perpage = this.perpage;
    let startIndex = page * perpage - perpage;
    let endIndex = page * perpage;
    this.currentPageRecords = datas.slice(startIndex, endIndex);
  };

  hanldeProgressValueChange(event) {
    this.page = event.detail;
    this.pageData();
  }

  /* Pagination Function End */

  // Sorting the Data Table Column Function End
  sortBy(field, reverse, primer) {
    const key = primer ?
      function (x) {
        return primer(x[field]);
      } :
      function (x) {
        return x[field];
      };

    return function (a, b) {
      a = key(a);
      b = key(b);
      if (a === undefined) a = "";
      if (b === undefined) b = "";
      a = typeof a === "number" ? a : a.toLowerCase();
      b = typeof b === "number" ? b : b.toLowerCase();

      return reverse * ((a > b) - (b > a));
    };
  }
  onHandleSort(event) {
    const {
      fieldName: sortedBy,
      sortDirection: sortDirection
    } = event.detail;
    const cloneData = [...this.tableRecords];

    cloneData.sort(this.sortBy(sortedBy, sortDirection === "asc" ? 1 : -1));
    this.sortedPageData(cloneData);
    this.sortDirection = sortDirection;
    this.sortedBy = sortedBy;
  }
  // Sorting the Data Table Column Function End

  //Search Function Start
  @api
  searchFn() {
    setTimeout(() => {
      this.pageData();
    }, 1000);
  }
  //Search Funtion End

  //Redirection Function Start
  handleRowAction(event) {

    if (this.supervisorFlow === false) {

      const onClickId = new CustomEvent("redirecttonextpage", {
        detail: {
          first: false,
          redirectId: event.detail.row.id
        }
      });
      this.dispatchEvent(onClickId);
    } else {
      if (event.detail.action.name !== undefined) {

        const onClickId = new CustomEvent("redirecttonextpage", {
          detail: {
            first: false,
            redirectId: event.detail.row.id,
            name: event.detail.action.name,
            proId: event.detail.row.proId
          }
        });
        this.dispatchEvent(onClickId);
      } else {

        const onClickId = new CustomEvent("redirecttonextpage", {
          detail: {
            first: false,
            redirectId: event.detail.row.proId
          }
        });
        this.dispatchEvent(onClickId);
      }
    }
  }
  //Redirection Function End

  get supervisorFlow() {
    let userProfile = sharedData.getUserProfileName();
    if (userProfile === "Supervisor") return true;
    else return false;
  }

  //   handleRedirect functionality start
  handleRedirect = (event) => {
    const redirect = new CustomEvent("redirect", {
      detail: event.detail
    });
    this.dispatchEvent(redirect);
  };
  //   handleRedirect functionality end

  // handleClick functionality start
  handleClick = (event) => {
    const whichReferral = new CustomEvent("referralclick", {
      detail: {
        name: event.target.name
      }
    });
    this.dispatchEvent(whichReferral);
    if (event.target.name === "Public") {
      this.publicReferralDataShow = true;
      this.privateReferralDataShow = false;
      this.showPublicPrivateBtn = false;
      this.showPublicBtn = true;
      this.showPrivateBtn = false;
    } else {
      this.privateReferralDataShow = true;
      this.publicReferralDataShow = false;
      this.showPublicPrivateBtn = true;
      this.showPrivateBtn = true;
      this.showPublicBtn = false;
    }
  };
  // handleClick functionality end
}