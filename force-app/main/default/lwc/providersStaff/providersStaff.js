/**
 * @Author        : Pratheeba V
 * @CreatedOn     : April 16, 2020
 * @Purpose       : Provider Staff Tab
 **/

import {
  LightningElement,
  track,
  api
} from 'lwc';
import getAllStaffList from '@salesforce/apex/ProvidersStaff.getAllStaffList';
import * as sharedData from 'c/sharedData';
import {
  utils
} from 'c/utils';
import images from '@salesforce/resourceUrl/images';
export default class ProvidersStaff extends LightningElement {
  @track norecorddisplay = false;
  @track currentPageProvidersStaffData;
  @track Id;
  @track Name;
  @track program;
  @track programtype;
  @track Address;
  @track StaffCount;
  @track recId;
  upIcon = images + '/asc-arrow.gif';
  downIcon = images + '/desc-arrow.gif';
  staffIcon = images + '/application-staff.svg';
  @track Spinner = true;
  //This function used to get the backend data
  connectedCallback() {
    this.staffDetailsLoad();
  }

  //Function used to get provider id from shared data
  get getproviderid() {
    return sharedData.getProviderId();
  }

  //This function used to get the backend data and show tables
  staffDetailsLoad() {
    getAllStaffList({
      providerid: this.getproviderid
    }).then(result => {
      result = JSON.parse(result);
      if (result.length > 0)
        this.norecorddisplay = true;
      this.currentPageProvidersStaffData = result.map(row => {
        return {
          addressId: row.addressId,
          addressName: row.addressName,
          address: row.addressLine1 + ',' + row.addressCity,
          applicationProgram: row.applicationProgram,
          applicationProgramType: row.applicationProgramType,
          staffCount: row.staffCount,
          objproviderStaffCount: row.objproviderStaffCount,
          nostaffrecorddisplay: (row.objproviderStaffCount && row.objproviderStaffCount.length != 0) ? true : false
        }
      });
      this.Spinner = false;
    }).catch(errors => {
      return this.dispatchEvent(utils.handleError(errors));
    });
  }

  @api providerStaffId;

  ViewStaffDetails(event) {
    event.preventDefault();
    const oncaseid = new CustomEvent('redirecttostaffinfo', {
      detail: {
        first: false,
        providerStaffId: event.target.dataset.id
      }
    });
    this.dispatchEvent(oncaseid);
  }

  toggleRow(event) {
    event.preventDefault();
    this.recId = event.target.dataset.id;
    this.toggleRowContinuation();
  }
  toggleRowContinuation() {
    let conList = this.template.querySelectorAll('.conli');
    let btnList = this.template.querySelectorAll('.btn');
    for (let j = 0; j < btnList.length; j++) {
      btnList[j].iconName = "utility:chevrondown";
    }
    for (let i = 0; i < conList.length; i++) {
      let indvRow = conList[i];
      let indvRowId = conList[i].getAttribute('id');
      if (indvRowId.includes(this.recId)) {
        if (indvRow.classList.contains("slds-showd")) {
          indvRow.classList.add("slds-hide");
          indvRow.classList.remove("slds-showd");
          btnList[i].iconName = "utility:chevrondown";
        } else {
          indvRow.classList.remove("slds-hide");
          indvRow.classList.add("slds-showd");
          btnList[i].iconName = "utility:chevronup";
        }
      } else {
        indvRow.classList.add("slds-hide");
        indvRow.classList.remove("slds-showd");
      }
    }
  }
  sortedDirection = 'asc';
  sortedColumn;
  @track ascdscshowhide = true;
  sort(e) {
    if (this.sortedColumn === e.currentTarget.dataset.id) {
      this.sortedDirection = this.sortedDirection === 'asc' ? 'desc' : 'asc';
    } else {
      this.sortedDirection = 'asc';
    }
    var reverse = this.sortedDirection === 'asc' ? 1 : -1;
    let table = JSON.parse(JSON.stringify(this.currentPageProvidersStaffData));
    table.sort((a, b) => {
      return a[e.currentTarget.dataset.id] > b[e.currentTarget.dataset.id] ? 1 * reverse : -1 * reverse
    });
    this.sortedColumn = e.currentTarget.dataset.id;
    this.currentPageProvidersStaffData = table;
    if (this.sortedDirection == 'asc') {
      this.ascdscshowhide = true;
    } else {
      this.ascdscshowhide = false;
    }
  }
}