/**
 * @Author        : G.Tamilarasan
 * @CreatedOn     : March 16, 2020
 * @Purpose       : This component contains Staff Certification Creation
 * @UpdatedBy     : Sundar K
 * @UpdatedOn     : May 14, 2020
 **/

import { LightningElement, track, wire } from 'lwc';
import InsertUpdateStaffCertificate from '@salesforce/apex/ProviderStaffCertificationController.InsertUpdateStaffCertificate';
import getCertificationDetails from '@salesforce/apex/ProviderStaffCertificationController.getCertificationDetails';
import editCertificationDetails from '@salesforce/apex/ProviderStaffCertificationController.editCertificationDetails';
import deleteCertificationDetails from '@salesforce/apex/ProviderStaffCertificationController.deleteCertificationDetails';
import * as shareddata from 'c/sharedData';
import Certification__c_OBJECT from '@salesforce/schema/Certification__c';
import { CJAMS_CONSTANTS } from "c/constants";
import { getPicklistValuesByRecordType } from 'lightning/uiObjectInfoApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import { utils } from "c/utils";

//Declare column headers in certification table
const columns = [
    { label: 'Certificate Type', fieldName: 'CertificationType__c', type: 'text', wrapText: true, initialWidth: 155 },
    { label: 'Due Date', fieldName: 'DueDate__c', type: 'date', editable: false, typeAttributes: { month: '2-digit', day: '2-digit', year: 'numeric' }, wrapText: true },
    { label: 'Application Mailed Date', fieldName: 'ApplicationMailedDate__c', type: 'date', editable: false, typeAttributes: { month: '2-digit', day: '2-digit', year: 'numeric' }, wrapText: true },
    { label: 'Tested Date', fieldName: 'TestedDate__c', type: 'date', editable: false, typeAttributes: { month: '2-digit', day: '2-digit', year: 'numeric' }, wrapText: true },
    { label: 'Effective Date', fieldName: 'CertificationEffectiveDate__c', type: 'date', editable: false, typeAttributes: { month: '2-digit', day: '2-digit', year: 'numeric' }, wrapText: true },
    { label: 'Certificate Number', fieldName: 'CertificateNumber__c', type: 'text', wrapText: true, initialWidth: 100, },
    { label: 'Renewal Date', fieldName: 'RenewalDate__c', type: 'date', editable: false, typeAttributes: { month: '2-digit', day: '2-digit', year: 'numeric' }, wrapText: true },
    {
        label: 'Action', fieldName: 'Id', initialWidth: 75, type: "button", typeAttributes: {
            iconName: 'utility:edit',
            name: 'Edit',
            title: 'Edit',
            initialWidth: 15,
            disabled: false,
            iconPosition: 'left',
            target: '_self',
        }
    },
    {
        initialWidth: 5,
        type: "button", typeAttributes: {
            iconName: 'utility:delete',
            name: 'Delete',
            title: 'Delete',
            initialWidth: 15,
            class: "del-red del-position",
            disabled: false,
            iconPosition: 'left',
            target: '_self'
        }
    }
];

export default class ProviderStaffCertification extends LightningElement {

    @track columns = columns;
    @track currentPageCertificateData;
    @track openmodel = false;
    @track deleteopenmodel = false;
    @track certificationStatus;
    @track certificationType;
    @track certificationSubType;
    @track certificationSubTypeValues;
    @track BehavioralInterventions;
    @track trainingID;
    @track currentdeleterecord;
    @track controlValues;
    @track norecorddisplay = true;
    @track certificationSubTypeOtherShowHide = true;
    @track renewalmindate = '1970-01-01';
    @track renewalmaxdate = '1970-01-02';
    @track totalRecords;
    @track totalRecordsCount = 0;
    @track page = 1;
    perpage = 3;
    setPagination = 5;
    @track addoreditCertification = { CertificationStatus__c: '', CertificationType__c: '', CertificationSubType__c: '', ApplicationMailedDate__c: '', CertificationEffectiveDate__c: '', TestedDate__c: '', RenewalDate__c: '', DueDate__c: '', CertificateNumber__c: '', BehavioralInterventions__c: '' };

    //Sundar Adding This code
    @track title;
    @track btnLabel;
    @track isBtnDisable = false;
    @track isUpdateFlag = false;

    //Function used to get contact id from persional section page
    get getcontactid() {
        return shareddata.getStaffId();
    }

    connectedCallback() {
        this.certificationDetailsLoad();
    }

    //Function used to show the existing certification
    certificationDetailsLoad() {
        getCertificationDetails({ certificationdetails: this.getcontactid }).then(result => {
            this.totalRecords = result;
            this.totalRecordsCount = this.totalRecords.length;

            if (this.totalRecords.length == 0) {
                this.norecorddisplay = false;
            }
            else {
                this.norecorddisplay = true;
            }
            this.pageData();

        }).catch(errors => {
            let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
            return this.dispatchEvent(utils.handleError(errors, config));
        });
    }

    //Function used to show the certification based on the pages
    pageData = () => {
        let page = this.page;
        let perpage = this.perpage;
        let startIndex = (page * perpage) - perpage;
        let endIndex = (page * perpage);
        this.currentPageCertificateData = this.totalRecords.slice(startIndex, endIndex);
        if (this.currentPageCertificateData.length == 0) {
            if (this.page != 1) {
                this.page = this.page - 1;
                this.pageData();
            }
        }
    }

    //Function used to get the page number from child pagination component
    hanldeProgressValueChange(event) {
        this.page = event.detail;
        this.pageData();
    }

    //Function used to open the model popup
    certificationOpenModal() {
        this.addoreditCertification = {};
        this.trainingID = undefined;
        this.certificationSubType = [];
        this.certificationSubTypeOtherShowHide = true;
        this.openmodel = true;
        this.title = "ADDING CERTIFICATION";
        this.btnLabel = "SAVE";
        this.isBtnDisable = true;
        this.isUpdateFlag = false;
    }

    //Function used to close the model popup
    certificationCloseModal() {
        this.addoreditCertification = {};
        this.openmodel = false;
        this.certificationSubType = [];
    }

    //Function used to close the delete model popup
    deleteCloseModal() {
        this.deleteopenmodel = false
    }

    //Function used to save the certification records with validation
    saveMethod() {
        if (!this.addoreditCertification.CertificationStatus__c) {
            this.dispatchEvent(utils.toastMessage("Please enter a certification status", "warning"));
        }
        else if (!this.addoreditCertification.CertificationType__c) {
            this.dispatchEvent(utils.toastMessage("Please enter a certification type", "warning"));
        }
        else if (!this.addoreditCertification.CertificationSubType__c) {
            this.dispatchEvent(utils.toastMessage("Please enter a certification sub type", "warning"));
        }
        else if (!this.addoreditCertification.ApplicationMailedDate__c) {
            this.dispatchEvent(utils.toastMessage("Please enter a application mailed date", "warning"));
        }
        else if (!this.addoreditCertification.CertificationEffectiveDate__c) {
            this.dispatchEvent(utils.toastMessage("Please enter a certificate effective date", "warning"));
        }
        else if (!this.addoreditCertification.TestedDate__c) {
            this.dispatchEvent(utils.toastMessage("Please enter a certificate tested date", "warning"));
        }
        else if (!this.addoreditCertification.RenewalDate__c) {
            this.dispatchEvent(utils.toastMessage("Please enter a renewal date", "warning"));
        }
        else if (!this.addoreditCertification.DueDate__c) {
            this.dispatchEvent(utils.toastMessage("Please enter a due date", "warning"));
        }
        else if (!this.addoreditCertification.CertificateNumber__c) {
            this.dispatchEvent(utils.toastMessage("Please enter a certificate number", "warning"));
        }
        else {
            this.isBtnDisable = true;

            this.addoreditCertification.sobjectType = 'Certification__c';
            this.addoreditCertification.Staff__c = this.getcontactid;
            this.addoreditCertification.Id = this.trainingID;

            if (this.trainingID)
                this.isUpdateFlag = true;
            else
                this.isUpdateFlag = false;

            InsertUpdateStaffCertificate({ objSobjecttoUpdateOrInsert: this.addoreditCertification }).then(result => {
                if (this.isUpdateFlag)
                    this.dispatchEvent(utils.toastMessage("Staff Certificate Details are updated successfully", "success"));
                else
                    this.dispatchEvent(utils.toastMessage("Staff Certificate Details are added successfully", "success"));

                this.addoreditCertification = {};
                this.certificationCloseModal();
                this.isBtnDisable = false;

                this.certificationDetailsLoad();

            }).catch(errors => {
                this.isBtnDisable = false;
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
        }
    }

    //Function used to get all the certification records with change events
    handleChange(event) {
        this.isBtnDisable = false;
        if (event.target.name == "CertificationStatus__c") {
            this.addoreditCertification.CertificationStatus__c = event.target.value;
        }
        else if (event.target.name == "CertificationType__c") {
            this.addoreditCertification.CertificationType__c = event.target.value;
            if (this.addoreditCertification.CertificationType__c == "Other") {
                this.addoreditCertification.CertificationSubType__c = "";
            }
            this.certificationsubtypevaluechangemethod(); //this method invoked certification sub type changed
        }
        else if (event.target.name == "CertificationSubType__c") {
            this.addoreditCertification.CertificationSubType__c = event.target.value;
        }
        else if (event.target.name == "ApplicationMailedDate__c") {
            this.addoreditCertification.ApplicationMailedDate__c = event.target.value;
        }
        else if (event.target.name == "CertificationEffectiveDate__c") {
            this.addoreditCertification.CertificationEffectiveDate__c = event.target.value;
            this.renewalmindate = event.target.value;
            this.renewalmaxdate = event.target.value + 20;  //Renewal Date disable feature works 
        }
        else if (event.target.name == "TestedDate__c") {
            this.addoreditCertification.TestedDate__c = event.target.value;
        }
        else if (event.target.name == "RenewalDate__c") {
            this.addoreditCertification.RenewalDate__c = event.target.value;
        }
        else if (event.target.name == "DueDate__c") {
            this.addoreditCertification.DueDate__c = event.target.value;
        }
        else if (event.target.name == "CertificateNumber__c") {
            this.addoreditCertification.CertificateNumber__c = event.target.value;
        }
        else if (event.target.name == "BehavioralInterventions__c") {
            this.addoreditCertification.BehavioralInterventions__c = event.target.value;
        }

    }

    //Function used to filter the certification sub type based on selected certification type value 
    certificationsubtypevaluechangemethod() {
        let dependValues = [];
        this.certificationSubTypeValues.forEach(conValues => {
            if (conValues.validFor[0] === this.controlValues[this.addoreditCertification.CertificationType__c]) {
                dependValues.push({
                    label: conValues.label,
                    value: conValues.value
                })
            }
        })

        this.certificationSubType = dependValues;
        if (this.addoreditCertification.CertificationType__c == 'Other') {
            this.certificationSubTypeOtherShowHide = false;
        }
        else {
            this.certificationSubTypeOtherShowHide = true;
        }
    }

    //Function used to edit the existing certificates 
    handleRowAction(event) {
        if (event.detail.action.name == "Edit") {
            let certificaterowid = event.detail.row.Id;
            this.openmodel = true;
            this.title = "EDITING CERTIFICATION";
            this.btnLabel = "UPDATE";
            this.isBtnDisable = false;

            editCertificationDetails({ editcertificationdetails: certificaterowid }).then(result => {
                // this.addoreditCertification = result[0];
                this.addoreditCertification.CertificationStatus__c = result[0].CertificationStatus__c;
                this.addoreditCertification.CertificationType__c = result[0].CertificationType__c;
                this.addoreditCertification.CertificationSubType__c = result[0].CertificationSubType__c;
                this.addoreditCertification.ApplicationMailedDate__c = result[0].ApplicationMailedDate__c;
                this.addoreditCertification.CertificationEffectiveDate__c = result[0].CertificationEffectiveDate__c;
                this.addoreditCertification.TestedDate__c = result[0].TestedDate__c;
                this.addoreditCertification.RenewalDate__c = result[0].RenewalDate__c;
                this.addoreditCertification.DueDate__c = result[0].DueDate__c;
                this.addoreditCertification.CertificateNumber__c = result[0].CertificateNumber__c;
                this.addoreditCertification.BehavioralInterventions__c = result[0].BehavioralInterventions__c;
                this.trainingID = result[0].Id;
                this.renewalmindate = result[0].CertificationEffectiveDate__c;
                this.renewalmaxdate = result[0].CertificationEffectiveDate__c + 20;
                this.certificationsubtypevaluechangemethod();
            }).catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
        }
        else if (event.detail.action.name == "Delete") {
            this.deleteopenmodel = true;
            this.currentdeleterecord = event.detail.row.Id;
        }


    }

    //Function used to delete the existing certificates 
    handleDelete() {
        deleteCertificationDetails({ deletecertificationdetails: this.currentdeleterecord }).then(result => {
            this.dispatchEvent(utils.toastMessage("Staff Certificate Details are deleted successfully", "success"));
            this.deleteopenmodel = false;
            this.certificationDetailsLoad();

        }).catch(errors => {
            let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DELETE_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
            return this.dispatchEvent(utils.handleError(errors, config));
        });

    }

    //Declare certification object into one variable
    @wire(getObjectInfo, { objectApiName: Certification__c_OBJECT })
    objectInfo;

    //Function used to get all picklist values from certification object
    @wire(getPicklistValuesByRecordType, { objectApiName: Certification__c_OBJECT, recordTypeId: '$objectInfo.data.defaultRecordTypeId' })
    getAllPicklistValues({ error, data }) {
        if (data) {
            this.error = null;

            // Behavioral Field Picklist values
            let behavioralOptions = [];

            data.picklistFieldValues.BehavioralInterventions__c.values.forEach(key => {
                behavioralOptions.push({
                    label: key.label,
                    value: key.value
                })
            });
            this.BehavioralInterventions = behavioralOptions;

            // Certification Status Field Picklist values
            let certificationStatusOptions = [];
            data.picklistFieldValues.CertificationStatus__c.values.forEach(key => {
                certificationStatusOptions.push({
                    label: key.label,
                    value: key.value
                })
            });
            this.certificationStatus = certificationStatusOptions;

            // Certification Type Field Picklist values
            let certificationTypeOptions = [];
            data.picklistFieldValues.CertificationType__c.values.forEach(key => {
                certificationTypeOptions.push({
                    label: key.label,
                    value: key.value
                })
            });
            this.certificationType = certificationTypeOptions;

            // Certification Type Field Picklist values
            let certificationSubTypeOptions = [];
            data.picklistFieldValues.CertificationSubType__c.values.forEach(key => {
                certificationSubTypeOptions.push({
                    label: key.label,
                    value: key.value
                })
            });
            //  Certification Type Field Picklist controller values
            this.controlValues = data.picklistFieldValues.CertificationSubType__c.controllerValues;
            this.certificationSubTypeValues = data.picklistFieldValues.CertificationSubType__c.values;

        }
        else if (error) {
            this.error = JSON.stringify(error);
        }
    }
}