/*
     Author         : Sindhu
     @CreatedOn     : MARCH 21 ,2020
     @Purpose       : Provider Narrative Details.
     @UpdatedBy     : Sundar K
     @UpdatedOn     : May 18, 2020
*/

import {
    LightningElement,
    track,
    api,
    wire
} from 'lwc';
import {
    utils
} from 'c/utils';
import * as sharedData from "c/sharedData";
import updateNarrativeDetails from '@salesforce/apex/providerNarrative.updateNarrativeDetails';
import getHistoryOfNarrativeDetails from '@salesforce/apex/providerNarrative.getHistoryOfNarrativeDetails';
import getSelectedHistoryRec from '@salesforce/apex/providerNarrative.getSelectedHistoryRec';
import images from '@salesforce/resourceUrl/images';
import {
    refreshApex
} from "@salesforce/apex";

export default class ProviderNarrative extends LightningElement {
    @track result;
    @track data = [];
    @track narrativeValue;
    @track visibleHistoryData = false;
    @track Spinner = true;
    @track richTextDisable = false;
    @track btnDisable = false;
    @track openNarrative = true;
    @track disableAdd = false;
    @track btnspeechDisable = false;

    attachmentIcon = images + '/document-newIcon.svg';
    recordId;
    wireNarrativeHistoryDetails;
    providerForApiCall = this.providerid;

    //Get Provider ID
    @api
    get providerid() {
        return sharedData.getProviderId();
    }

    @wire(getHistoryOfNarrativeDetails, {
        providerId: '$providerForApiCall'
    })
    wiredNarrativeHistoryDetails(result) {
        this.wireNarrativeHistoryDetails = result;

        if (result.data) {
            if (result.data.length > 0) {
                this.visibleHistoryData = true;

                this.data = result.data.map((row, index) => {
                    return {
                        Id: row.Id,
                        author: row.LastModifiedBy.Name,
                        lastModifiedDate: row.LastModifiedDate,
                        dataIndex: index
                    }
                });
            } else {
                this.visibleHistoryData = false;
            }
        }
        this.Spinner = false;
    }

    //Add New Narrative
    openNarrativeModel() {
        this.openNarrative = true;
        this.btnDisable = false;
        this.richTextDisable = false;
        this.btnspeechDisable = false;
        this.narrativeValue = '';
    }
    cancelHandler(){
        this.narrativeValue ='';
    }
    //Google Speech Text
    handleSpeechToText(event) {
        this.narrativeValue = event.detail.speechValue;
    }

    //Narrative On Change Functionality
    narrativeOnChange(event) {
        this.narrativeValue = event.target.value;
    }

    //Save Functionality
    saveNarrativeDetails() {
        this.template.querySelector('c-common-speech-to-text').stopSpeech();
        if (!this.narrativeValue)
            return this.dispatchEvent(utils.toastMessage("Please enter Narrative", "warning"));

        let myObj = {
            'sobjectType': 'Account'
        };

        myObj.Narrative__c = this.narrativeValue;

        if (this.providerid)
            myObj.Id = this.providerid;

        updateNarrativeDetails({
                objSobjecttoUpdateOrInsert: myObj
            })
            .then(result => {
                this.narrativeValue = '';
                this.dispatchEvent(utils.toastMessage("Provider Narrative Details are saved successfully", "success"));
                return refreshApex(this.wireNarrativeHistoryDetails);
            })
            .catch(error => {
                this.dispatchEvent(utils.toastMessage("Error in saving provider narrative details. please check", "error"));
            })
    }

    //Get respective history record
    narrativeHistoryOnClick(event) {
        this.recordId = event.currentTarget.dataset.id;

        let dataIndex = event.currentTarget.dataset.value;
        let datas = this.template.querySelectorAll('.divHighLight');

        for (let i = 0; i < datas.length; i++) {
            if (i == dataIndex)
                datas[i].className = "slds-nav-vertical__item divHighLight slds-is-active";
            else
                datas[i].className = "slds-nav-vertical__item divHighLight";
        }

        //Get selected history record
        getSelectedHistoryRec({
                recId: this.recordId
            })
            .then(result => {
                if (result.length > 0 && result[0].RichNewValue__c) {
                    this.narrativeValue = result[0].RichNewValue__c;
                    this.btnDisable = true;
                    this.richTextDisable = true;
                } else {
                    this.narrativeValue = null;
                }
            })
    }
}