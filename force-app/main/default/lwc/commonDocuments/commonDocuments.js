/**
 * @Author        : Praveen
 * @CreatedOn     : Apr 15,2020
 * @Purpose       : Attachments based on Referral Documents
 **/


import {
    LightningElement,
    wire,
    track,
    api
} from 'lwc';
import {
    NavigationMixin
} from 'lightning/navigation';
import {
    utils
} from "c/utils";


import {
    refreshApex
} from '@salesforce/apex';
import images from '@salesforce/resourceUrl/images';
import {
    getRecord
} from 'lightning/uiRecordApi';
import USER_ID from '@salesforce/user/Id';
import NAME_FIELD from '@salesforce/schema/User.Name';

import getPicklistvalues from '@salesforce/apex/CommonUtils.getPickListValues';
import getCurrentDate from '@salesforce/apex/CommonUtils.getcurrentDateTime';
import saveDescription from "@salesforce/apex/CommonDocuments.saveDescription";
import getUploadedDocuments from "@salesforce/apex/CommonDocuments.getUploadedDocuments";
import getUploadedAttachmentsUserDetails from "@salesforce/apex/CommonDocuments.getUploadedAttachmentsUserDetails";
//import getSelectedFileDetails from "@salesforce/apex/CommonDocuments.getSelectedFileDetails";

import {
    deleteRecord
} from 'lightning/uiRecordApi';

import {
    CJAMS_OBJECT_QUERIES
} from 'c/constants';
import {
    CJAMS_CONSTANTS
} from 'c/constants';

const url = '/sfc/servlet.shepherd/document/download/';

export default class CommonDocuments extends NavigationMixin(LightningElement) {


    @track uploadedDate;
    @track userName;
    @track fileName;
    @track progress = 0;
    @track isProgressing = false;
    @track isBare = false;
    @track dataStringNotEmpty = false;
    @track documenttitle;
    @track category;
    @track subCategory;
    @track CategoryList;
    @track subCategoryList;
    @track description;
    @track dataStr = [];
    @track ViewFileData;
    @track CaseNumber;
    @track ReceivedDate;
    @track pageSpinner = false;
    @track isOpenModal = false;
    @track deleteModel;
    @track hasModalOpen;
    @track doctype = 'doctype:image';

    attachmentIcon = images + '/document-newIcon.svg';
    uploadicon = images + '/icon-upload.svg';
    _objCase;


    @api getUserProfileName;
    @api getMonitoringStatus;

    _documentIdForAttachment;
    @api get documentIdForAttachment() {
        return this._documentIdForAttachment;
    }
    set documentIdForAttachment(value) {
        this._documentIdForAttachment = value;
    }

    _sobjectType;
    @api get sobjectType() {
        return this._sobjectType;
    }
    set sobjectType(value) {
        this._sobjectType = value;
    }
    _selectQueryToGetData;


    connectedCallback() {

        this.buildQueryFOrApex();

        if (this.getMonitoringStatus !== 'Draft' && this.getMonitoringStatus != undefined) {
            this.btnDisable = true;
        } else {
            this.btnDisable = false;
        }
    }
    buildQueryFOrApex() {
        switch (this.sobjectType) {
            case CJAMS_CONSTANTS.CASE_OBJECT_NAME:
                this._selectQueryToGetData = CJAMS_OBJECT_QUERIES.SELECT_FIELDS_FOR_PUBLIC_REFERRAL_ATTACHMENT;
                break;
            case CJAMS_CONSTANTS.APPLICATION_OBJECT_NAME:
                this._selectQueryToGetData = CJAMS_OBJECT_QUERIES.SELECT_FIELDS_FOR_PUBLIC_APPLICATION_ATTACHMENT;
                break;
            case CJAMS_CONSTANTS.PROVIDER_OBJECT_NAME:
                this._selectQueryToGetData = CJAMS_OBJECT_QUERIES.SELECT_FIELDS_FOR_PRIVATE_PROVIDER_ATTACHMENT;
                break;
            case CJAMS_CONSTANTS.MONITORING_OBJECT_NAME:
                this._selectQueryToGetData = CJAMS_OBJECT_QUERIES.SELECT_FIELDS_FOR_PRIVATE_MONITORING_ATTACHMENT;
                break;
            case CJAMS_CONSTANTS.RECONSIDERATION_OBJECT_NAME:
                this._selectQueryToGetData = CJAMS_OBJECT_QUERIES.SELECT_FIELDS_FOR_PUBLIC_RECONSIDERATION_ATTACHMENT;
                break;
        }
        this.getDocumentDetailsFromApex();
    }
    getDocumentDetailsFromApex() {
        getUploadedAttachmentsUserDetails({
            documentID: this.documentIdForAttachment,
            sObjectToFetchData: this.sobjectType,
            strFieldsToQuery: this._selectQueryToGetData
        })
            .then(result => {
                this.CaseNumber = result[0] && result[0].Name != undefined ? result[0].Name : result[0].CaseNumber;
                this.ReceivedDate = result[0] && result[0].CreatedDate != undefined ? result[0].CreatedDate : result[0].VisitationStartDate__c != undefined ? result[0].VisitationStartDate__c : result[0].ReceivedDate__c != undefined ? result[0].ReceivedDate__c : result[0].ReconsiderationDate__c;
                if (this.getUserProfileName != 'Supervisor') {
                    if (result.length > 0 && (result[0].Status == "Approved" || result[0].Status == "Rejected" || result[0].Status == "Supervisor Rejected" || result[0].Status == "Caseworker Submitted" || result[0].Status == "Submitted" || result[0].Status == "Submitted to Supervisor" || result[0].Status == "Acknowledged"|| result[0].Status == "Rejected")) {
                        this.btnDisable = true;
                        this.displayDeleteIcon = true;
                    } else if (result[0].Status__c == "Approved" || result[0].Status__c == "Rejected" || result[0].Status__c == "Submitted") {
                        this.btnDisable = true;
                        this.displayDeleteIcon = true;
                    }
                }
                if (this.getUserProfileName == 'Supervisor') {
                    if (result.length > 0 && (result[0].Status == "Approved" || result[0].Status == "Supervisor Rejected" || result[0].Status == "Accepted" || result[0].Status == "Disputed")) {
                        this.btnDisable = true;
                        this.displayDeleteIcon = true;
                    } else if (result[0].Status__c == "Approved" || result[0].Status__c == "Rejected" || result[0].Status__c == "Submitted") {
                        this.btnDisable = true;
                        this.displayDeleteIcon = true;
                    }
                }
            })
            .catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
    }

    // get CaseNumber() {
    //     return this._objCase[0].CaseNumber;
    // }
    // get ReceivedDate() {
    //     return this._objCase[0].ReceivedDate__c;
    // }

    @track filesUploaded = {};
    @track UploadedLabel;
    documentTit;
    selectedDocumentId;
    ViewFileData;

    EditFileId;

    fileContents;
    dataStart;
    wiredAttachment;
    wiredDocumenttDetails;

    @track btnDisable = false;
    @track displayDeleteIcon = false;

    @track files = [];
    @track recordId = this.documentIdForAttachment;

    get acceptedFilesFormat() {
        return ['.jpg', '.jpeg', '.png', '.mp4', '.pdf', '.webm', '.pptx', '.svg', '.docx'];
    }

    handleFileUpload(event) {

        let uploadedFiles = event.detail.files;

        for (let index = 0; index < uploadedFiles.length; index++) { //for(let index in uploadedFiles) {
            if ({}.hasOwnProperty.call(uploadedFiles, index)) {
                this.files = [...this.files, {
                    Id: uploadedFiles[index].documentId,
                    name: uploadedFiles[index].name,
                    src: url + uploadedFiles[index].documentId,
                    description: ''
                }];
            }
        }
        this.saveDescription();

        this.isOpenModal = false;
        // refreshApex(this.wiredAttachment);

    }
    saveDescription() {
        let _documentID = this.files.map(({
            Id
        }) => Id).join(",");

        saveDescription({
            strDocumentId: _documentID,
            strDescription: this.description,
            strSubCategory: this.subCategory,
            strTitle: this.documenttitle
        }).then(result => {
            this.files = [];
            this.dispatchEvent(utils.toastMessage('Files uploaded Successfully!', "Success"));
            refreshApex(this.wiredAttachment);
            // this.wiredAttachment = value;
        }).catch(errors => {
            let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
            return this.dispatchEvent(utils.handleError(errors, config));
        });
    }

    handleOpenModal() {
        this.description = '';
        this.documenttitle = '';
        this.progress = 0;
        this.isProgressing = false;
        this.isBare = false;

        getCurrentDate().then(response => {
            this.uploadedDate = response;
            this.isOpenModal = true;
        }).catch(errors => {
            let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
            return this.dispatchEvent(utils.handleError(errors, config));
        });

    }
    handleCloseModal() {
        this.isOpenModal = false;
        //  refreshApex(this.wiredDocumenttDetails);
        refreshApex(this.wiredAttachment);
    }


    // get current user details
    @wire(getRecord, {
        recordId: USER_ID,
        fields: [NAME_FIELD]
    }) wireuser({
        error,
        data
    }) {
        if (error) { } else if (data) {
            this.userName = data.fields.Name.value;
        }
    }


    // It enables File Upload button on entering Category & SubCategory
    showUpload(event) {
        var documenttitlevalue = this.template.querySelector('[data-id="documentTitle"]')
        var field = event.target.name;

        if (field === 'category') {
            this.category = event.target.value;
        } else if (field === 'subCategory') {
            this.subCategory = event.target.value;
        }
        if (this.category && this.subCategory) {
            this.isUpload = true;
        }
    }
    @wire(getPicklistvalues, {
        objInfo: {
            sobjectType: "ContentVersion"
        },
        picklistFieldApi: "Health__c"
    })
    wireCheckPicklistProfitvalue({
        error,
        data
    }) {
        if (data) {
            this.subCategoryList = data;
        } else if (error) {

        }
    }


    // Wire Service for fetching files for specified target record
    @wire(getUploadedDocuments, {
        strDocumentID: '$documentIdForAttachment'
    })
    DocumentData(value) {
        this.wiredAttachment = value;
        const {
            data,
            error
        } = value;
        // this.dataStringNotEmpty = false;
        if (data) {

            // this.dataStr = data;
            setTimeout(() => {
                this.pageSpinner = false;
                this.dataStr = data;
                if (data.length > 0) {
                    this.UploadedLabel = 'Uploaded Documents';
                } else {

                    this.dataStringNotEmpty = false;
                }
                if (data.length !== 0) {
                    this.dataStringNotEmpty = true;
                }
            }, 300);
            // update the start position with end postion


        } else if (error) {
            this.dataStringNotEmpty = false;
        }
    }

    viewFile(event) {
        this.selectedDocumentId = event.currentTarget.dataset.id;
        this.ViewFileData = true;
        this[NavigationMixin.Navigate]({
            type: 'standard__namedPage',
            attributes: {
                pageName: 'filePreview'
            },
            state: {
                // assigning ContentDocumentId to show the preview of file
                selectedRecordId: event.currentTarget.dataset.id
            }
        });

    }
    downloadFile(event) {
        this.selectedDocumentId = event.currentTarget.dataset.id;
        this.ViewFileData = true;
        this[NavigationMixin.Navigate]({
            type: 'standard__webPage',
            attributes: {
                url: window.location.origin + '/sfc/servlet.shepherd/document/download/' + event.currentTarget.dataset.id
            }
        },
            false // Replaces the current page in your browser history with the URL
        );
    }
    CloseViewFile() {
        this.ViewFileData = false;
    }
    // close models
    closeDeleteModal() {
        this.deleteModel = false;
        this.hasModalOpen = false;
    }

    // Assign Value to Category and Sub Category when it is filled in Edit Modal Popup.
    assignValue(event) {
        var field = event.target.name;

        if (field === 'category') {
            this.category = event.target.value;
        } else if (field === 'subCategory') {
            this.subCategory = event.target.value;
        } else if (field === 'description') {
            this.description = event.target.value;
        } else if (field === 'documentTitle') {
            this.documenttitle = event.target.value;
        }

    }

    saveEditFile() {
        var currentIntakeSfdcId = this.CaseNum;
        var cate = this.category;
        var documentId = this.EditFileId;
        var sCate = this.subCategory;
        if (!sCate || !cate) {

            this.dispatchEvent(utils.toastMessage("Please Select Category and Sub Category", "Warning"));
        } else {
            getUploadedDocuments({
                strDocumentID: currentIntakeSfdcId

            }).then(response => {
                this.dataStr = response;

                this.dispatchEvent(utils.toastMessage("Saved successfully", "Success"));
                this.hasModalOpen = false;
                this.selectedDocumentId = null;

            }).catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
        }
    }
    deleteConformation(event) {
        this.deleteModel = true;
        this.deleteDocumentId = event.currentTarget.dataset.id
    }
    handleDelete() {
        this.deleteModel = false;
        deleteRecord(this.deleteDocumentId)
            .then(() => {
                this.dispatchEvent(utils.toastMessage("File Deleted Succesfully", "Success"));
                refreshApex(this.wiredAttachment);
            }).catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DELETE_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
    }

    disconnectedCallback() {
        // it's needed for the case the component gets disconnected
        // and the progress is being increased

        clearInterval(this._interval);
    }

}