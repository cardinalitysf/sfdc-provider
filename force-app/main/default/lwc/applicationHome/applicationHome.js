import {
  LightningElement,
  track
} from "lwc";
import * as sharedData from "c/sharedData";
import fetchDataForAddOrEdit from "@salesforce/apex/ApplicationGeneralInfo.fetchDataForAddOrEdit";
import fetchDataForAddressSite from "@salesforce/apex/ApplicationGeneralInfo.fetchDataForAddressSite";
import fetchDataForAddressPayment from "@salesforce/apex/ApplicationGeneralInfo.fetchDataForAddressPayment";
import fetchDataForAddressMain from "@salesforce/apex/ApplicationGeneralInfo.fetchDataForAddressMain";
import myResource from '@salesforce/resourceUrl/styleSheet';
import {
  loadStyle
} from 'lightning/platformResourceLoader';
import images from '@salesforce/resourceUrl/images';

export default class ApplicationHome extends LightningElement {

  siteicon = images + '/site-address.svg';
  mainaddress = images + '/main-address.svg';
  paymentaddress = images + '/payment-address.svg';
  providernameicon = images + '/provider-nameicon.svg';
  phone = images + '/phone.svg';
  mobile = images + '/mobile.svg';
  fax = images + '/fax.svg';
  email = images + '/email.svg';
  bedcapacity = images + '/bed-capacity.svg';
  agerange = images + '/age-range.svg';
  iqrange = images + '/iq-range.svg';
  gender = images + '/gender.svg';
  phone2 = images + '/phone2.svg';
  email2 = images + '/email2.svg';



  @track getapplicationid = "a090p000001GQPsAAO";
  //  @track getAddressid = "a0F0p000000LXw3EAG";
  @track getAddressSite = "site";
  @track getAddressMain = "main";
  @track getAddressPayment = "payment";
  @track Gender;
  @track Capacity;
  @track Program;
  @track ProgramType;
  @track RFP;
  @track TaxId;
  @track SON;
  @track Corporation;
  @track ParentCorporation;
  @track MaxAge;
  @track MaxIQ;
  @track MinAge;
  @track MinIQ;
  @track ProviderEmail;
  @track ProviderCellNumber;
  @track ProviderPhone;
  @track ProviderName;
  @track ProviderFirstName;
  @track ProviderLastName;
  @track nameDisplay = '';
  @track resultSite = [];
  @track resultPayment = [];
  @track resultMain = [];

  renderedCallback() {
    Promise.all([
      loadStyle(this, myResource + '/appstyle.css'),
    ])
  }


  connectedCallback() {
    this.fetchDatasFromApplication();
    this.fetchDatasFromAddressSite();
    this.fetchDatasFromAddressMain();
    this.fetchDatasFromAddressPayment();
  }

  fetchDatasFromApplication() {
    if (this.getapplicationid != undefined) {
      fetchDataForAddOrEdit({
        fetchdataname: this.getapplicationid
      }).then(result => {
        this.Capacity = result[0].Capacity__c ? result[0].Capacity__c : " - ";
        this.Gender = result[0].Gender__c ? result[0].Gender__c : ' - ';
        this.Program = result[0].Program__c ? result[0].Program__c : ' - ';
        this.ProgramType = result[0].ProgramType__c ? result[0].ProgramType__c : " - ";
        this.RFP = result[0].Provider__r.RFP__c ? result[0].Provider__r.RFP__c : ' - ';
        this.TaxId = result[0].Provider__r.TaxId__c ? result[0].Provider__r.TaxId__c : ' - ';
        this.SON = result[0].Provider__r.SON__c ? result[0].Provider__r.SON__c : ' - ';
        this.Corporation = result[0].Provider__r.Corporation__c ? result[0].Provider__r.Corporation__c : ' - ';
        this.ParentCorporation =
          result[0].Provider__r.ParentCorporation__c ? result[0].Provider__r.ParentCorporation__c : ' - ';
        this.MaxAge = result[0].MaxAge__c ? result[0].MaxAge__c : ' - ';
        this.MinAge = result[0].MinAge__c ? result[0].MinAge__c : ' - ';
        this.MaxIQ = result[0].MaxIQ__c ? result[0].MaxIQ__c : ' - ';
        this.MinIQ = result[0].MinIQ__c ? result[0].MinIQ__c : ' - ';
        this.ProviderEmail = result[0].Provider__r.Email__c ? result[0].Provider__r.Email__c : ' - ';
        this.ProviderCellNumber =
          result[0].Provider__r.CellNumber__c ? result[0].Provider__r.CellNumber__c : ' - ';
        this.ProviderPhone = result[0].Provider__r.Phone ? result[0].Provider__r.Phone : ' - ';
        this.ProviderName = result[0].Provider__r.Name ? result[0].Provider__r.Name : ' - ';
        const x = this.ProviderName;
        this.nameDisplay = x.substring(0, 1).toUpperCase();
        this.ProviderFirstName =
          result[0].Provider__r.FirstName__c;
        this.ProviderLastName =
          result[0].Provider__r.LastName__c;
      });
    }
  }


  // pentagonName() {
  //   const x = this.ProviderName;
  //   this.nameDisplay = x.substring(0, 1);
  // }

  fetchDatasFromAddressSite() {
    if (
      this.getapplicationid != undefined &&
      this.getAddressSite
    ) {
      fetchDataForAddressSite({
        fetchdataname: this.getapplicationid,
        fetchdatatype: this.getAddressSite
      }).then(result => {
        this.resultSite = result;

      });
    }
  }

  fetchDatasFromAddressPayment() {
    if (
      this.getapplicationid != undefined &&
      this.getAddressPayment
    ) {
      fetchDataForAddressPayment({
        fetchdataname: this.getapplicationid,
        fetchdatatype: this.getAddressPayment
      }).then(result => {
        this.resultPayment = result;
      });
    }
  }

  fetchDatasFromAddressMain() {
    if (
      this.getapplicationid != undefined &&
      this.getAddressMain
    ) {
      fetchDataForAddressMain({
        fetchdataname: this.getapplicationid,
        fetchdatatype: this.getAddressMain
      }).then(result => {
        this.resultMain = result;

      });
    }
  }
}