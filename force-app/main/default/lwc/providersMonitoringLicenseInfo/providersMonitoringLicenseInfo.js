/**
 * @Author        : BalaMurugan
 * @CreatedOn     : Apr 27,2020
 * @Purpose       : Add License Info based on Providers
 **/


import {
    LightningElement,
    track
} from 'lwc';
import images from '@salesforce/resourceUrl/images';
import getLicenseInformation from "@salesforce/apex/providersMonitoringLicenseInfo.getLicenseInformation";
import getProviderDetails from "@salesforce/apex/providersMonitoringLicenseInfo.getProviderDetails";
import getaddressdetails from "@salesforce/apex/providersMonitoringLicenseInfo.getaddressdetails";
import getIRCRatesCapacity from '@salesforce/apex/providersMonitoringLicenseInfo.getIRCRatesCapacity';
import getContractCapacity from '@salesforce/apex/providersMonitoringLicenseInfo.getContractCapacity';
import myResource from '@salesforce/resourceUrl/styleSheet';
import UpdateLicenseEndDate from '@salesforce/apex/providersMonitoringLicenseInfo.UpdateLicenseEndDate';
import * as sharedData from 'c/sharedData';
import {
    utils
} from 'c/utils';
import {
    CJAMS_CONSTANTS
} from 'c/constants';

export default class ProvidersMonitoringLicenseInfo extends LightningElement {

    ircIcon = images + '/ircIcon.svg';
    licenseBeds = images + '/license-bedIcon.svg';
    contractedBeds = images + '/contracted-bedIcon.svg';
    ageRange = images + '/agerangeIcon.svg';
    iQRange = images + '/iQ-RangeIcon.svg';
    genderIcon = images + '/genderIcon.svg';

    @track LicenseNumber;
    @track Program;
    @track ProgramType;
    @track ProviderName;
    @track IRCCapacity;
    @track LicenseBeds;
    @track ContractedBeds;
    @track SiteID;
    @track SiteAddress;
    @track Gender;
    @track licensesIdList = [];
    @track LicenseStartDate;
    @track LicenseEndDate;
    @track MinAge;
    @track MaxAge;
    @track MinIQ;
    @track MaxIQ;
    @track MonitoringPrimID;
    @track disdate = true;
    @track isSaveButtonShow = false;


    get getProviderId() {
        return sharedData.getProviderId();
    }

    get getMonitoringId() {
        return sharedData.getMonitoringId();
    }

    get getLicenseId() {
        return sharedData.getLicenseId();
    }

    get getMonitoringAddressId() {
        return sharedData.getMonitoringAddressId();
    }

    connectedCallback() {
        this.getLicenseInformation();
    }

    getLicenseInformation() {
        getLicenseInformation({
            MonitoringId: this.getMonitoringId
        })
            .then(result => {
                if (result.length > 0 && result[0].License__r.Name) {
                    this.LicenseNumber = result[0].License__r.Name;
                    this.Program = result[0].License__r.ProgramName__c;
                    this.ProgramType = result[0].License__r.ProgramType__c;
                    this.LicenseStartDate = result[0].License__r.StartDate__c;
                    this.LicenseEndDate = result[0].License__r.EndDate__c;
                    this.Gender = result[0].License__r.Application__r.Gender__c;
                    this.LicenseBeds = result[0].License__r.Application__r.Capacity__c;
                    this.MinAge = result[0].License__r.Application__r.MinAge__c;
                    this.MaxAge = result[0].License__r.Application__r.MaxAge__c;
                    this.MinIQ = result[0].License__r.Application__r.MinIQ__c;
                    this.MaxIQ = result[0].License__r.Application__r.MaxIQ__c;
                    this.MonitoringPrimID = result[0].Id;
                    this.SiteAddress = result[0].SiteAddress__r.AddressLine1__c;
                    this.SiteID = result[0].SiteAddress__r.Name;

                    if (this.LicenseStartDate && this.LicenseEndDate) {
                        var today = new Date();
                        var monthDigit = today.getMonth() + 1;
                        if (monthDigit <= 9) {
                            monthDigit = '0' + monthDigit;
                        }
                        var dayDigit = today.getDate();
                        if (dayDigit <= 9) {
                            dayDigit = '0' + dayDigit;
                        }

                        var date = today.getFullYear() + "-" + monthDigit + "-" + dayDigit;

                        if (date > this.LicenseEndDate) {
                            this.disdate = false;
                            this.isSaveButtonShow = true;
                        } else {
                            this.disdate = true;

                        }
                        this.getProviderNameDetails();
                    }
                }
            }).catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });


        getIRCRatesCapacity({
            licenseId: this.getLicenseId
        })
            .then((result) => {
                if (result.length > 0) {
                    this.IRCCapacity = result[0].IRCBedCapacity__c;
                }
            }).catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });

        getContractCapacity({
            licenseId: this.getLicenseId
        })
            .then((result) => {
                if (result.length > 0) {
                    this.ContractedBeds = result[0].TotalContractBed__c;
                }
            }).catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
    }
    handleChange(event) {

        if (event.target.name == "LicenseStartDate__c") {
            this.LicenseStartDate = event.target.value;
        } else if (event.target.name == "LicenseEndDate__c") {
            this.LicenseEndDate = event.target.value;
        }
    }
    handleSave() {
        let myobjcasenew = {
            sObject: "License__c"
        };
        myobjcasenew.StartDate__c = this.LicenseStartDate;
        myobjcasenew.EndDate__c = this.LicenseEndDate;
        myobjcasenew.Id = this.getLicenceId;

        UpdateLicenseEndDate({
            objSobjecttoUpdateOrInsert: myobjcasenew
        })
            .then(result => {
                this.dispatchEvent(
                    utils.toastMessage(
                        "License Expiry Date Saved Successfully",
                        "success"
                    )
                );
                this.getLicenseInformation();
            })
            .catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
    }


    getProviderNameDetails() {
        getProviderDetails({
            providerId: this.getProviderId
        })
            .then((result) => {
                if (result.length > 0 && result[0].Name) {
                    this.ProviderName = result[0].Name;
                }
            })
            .catch(errors => {
                let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
                return this.dispatchEvent(utils.handleError(errors, config));
            });
    }
}