/**
 * @Author        : G.Tamilarasan
 * @CreatedOn     : March 30, 2020
 * @Purpose       : This component contains Conference booking for each application monitoring
 **/

import {
  LightningElement,
  track,
  wire,
  api
} from 'lwc';
import getConferenceDetails from '@salesforce/apex/ApplicationConferenceController.getConferenceDetails';
import getAllUsersList from '@salesforce/apex/ApplicationConferenceController.getAllUsersList';
import getConferenceType from "@salesforce/apex/ApplicationConferenceController.getConferenceType";
import InsertUpdateConference from '@salesforce/apex/ApplicationConferenceController.InsertUpdateConference';
import { CJAMS_CONSTANTS } from "c/constants";
import {
  deleteRecord
} from 'lightning/uiRecordApi';

import assingStaffMember from "@salesforce/apex/ApplicationConferenceController.assingStaffMember";
import getStaffFromApplicationID from "@salesforce/apex/ApplicationConferenceController.getStaffFromApplicationID";

import images from '@salesforce/resourceUrl/images';

import {
  utils
} from "c/utils";
import * as shareddata from "c/sharedData";
import {
  CurrentPageReference
} from 'lightning/navigation';
import {
  fireEvent
} from 'c/pubsub';
export default class ApplicationMonitoringConference extends LightningElement {

  //Property Declarations
  @track currentPageConferenceData;
  @track currentPageEntranceConferenceData
  @track currentPageExitConferenceData;
  @track entranceopenmodel = false;
  @track deleteopenmodel = false;
  @track norecorddisplay = true;
  @track noexitrecorddisplay = false;
  @track stateemployes = [];
  @track stateEmployeeView = false;
  @track ConferenceTypeOptions;
  @track conferenceID;
  @track exitConferenceDisplay = false;
  @track enterexitlabel = true;
  @track staffnameshowhide = true;
  @track isDisableProgramChanges = false;
  @track currentExpanded;
  @track timebackup;
  @track assignStaffdropdownshow;
  @track assignDataItem;
  @track assignDataItem1;
  @track deleteEventId;
  @track deleteEventName;
  @track addoreditConference = {
    Type__c: '',
    Date__c: '',
    Time__c: '',
    StaffNames__c: '',
    StaffEmployees__c: '',
    ProgramChangesUpdates__c: '',
    Summary__c: ''
  };

  //customize picklist declarations
  @track activeForPType = false;
  @track pillValueForPtype = [];
  @track pillSingleValueForPtype;
  @track pillSingleValueForPtypeCount;
  @track pillSingleValueCondition = true;
  @track isDisableProgramType = false;
  @track activeForPTypeStaff = false;
  @track pillValueForPtypeStaff = [];
  @track pillSingleValueForPtypeStaff;
  @track pillSingleValueForPtypeCountStaff;
  @track pillSingleValueConditionStaff = true;
  @track isDisableProgramTypeStaff = false;
  @track hideAddButton = false;
  @track saveDisable = false;



  @track recId;
  @wire(CurrentPageReference) pageRef;
  attachmentIcon = images + '/application-conference.svg';
  upIcon = images + '/asc-arrow.gif';
  downIcon = images + '/desc-arrow.gif';
  deleteIcon = images + '/del-conference.svg';
  @track Spinner = true;

  get providerId() {
    return shareddata.getProviderId();
  }
  get applicationID() {
    return shareddata.getApplicationId();
  }
  get getMonitoringID() {
    return shareddata.getMonitoringId();
  }
  get getMonitoringStatus() {
    return shareddata.getMonitoringStatus();
  }

  //This function used to open the model popup which is based on entrance / exit
  entranceOpenModal(event) {

    if (this.getMonitoringStatus === "Draft") {

      this.entranceopenmodel = true;
      this.pillValueForPtypeStaff = [];
      this.pillValueForPtype = [];

      if (event.target.name == "entrance conference") {
        this.addoreditConference = {};
        this.addoreditConference.Type__c = "Entrance Conference";
        this.enterexitlabel = true;
        this.isDisableProgramChanges = false;
      } else {
        this.isDisableProgramChanges = true;
        let val = this.currentPageEntranceConferenceData.filter(item => {
          return item.Id == this.recId
        });
        this.conferenceID = val[0].Id;
        this.addoreditConference.Type__c = "Exit Conference";
        this.addoreditConference.ProgramChangesUpdates__c = val[0].ProgramChangesUpdates__c;
        this.addoreditConference.StaffEmployees__c = val[0].StaffEmployeesEdit;
        this.addoreditConference.StaffNames__c = val[0].StaffNamesEdit;
        this.addoreditConference.Summary__c = val[0].Summary__c;
        this.addoreditConference.Time__c = utils.formatTime12Hr(val[0].Time__c);
        this.timebackup = utils.formatTime12Hr(val[0].Time__c);
        this.timebackupoldvalue = val[0].Time__c;
        this.addoreditConference.Date__c = val[0].Date__c;

        this.programTypeCheckBoxSelectUnselectDuringPageLoad();
        this.programTypeCheckBoxSelectUnselectDuringPageLoadStaff();
        this.enterexitlabel = false;
      }

    } else {
      this.dispatchEvent(utils.toastMessage("Cannot add conference for completed monitoring", "warning"));
    }

  }

  //This function used to close the model popup
  entranceCloseModal() {
    this.entranceopenmodel = false;
  }

  deleteOpenModel() {
    this.deleteopenmodel = true;
  }
  deleteCloseModel() {
    this.deleteopenmodel = false;
  }

  //This function used to get the backend data
  connectedCallback() {
    if (this.userProfileName == 'Supervisor') {
      this.hideAddButton = true;

    }
    else {
      this.hideAddButton = false;
    }
    this.certificationDetailsLoad();
    this.initialStaffMemberLoad();

  }

  //This function used to get the backend data and show tables
  certificationDetailsLoad() {
    getConferenceDetails({
      conferencedetails: this.getMonitoringID
    }).then(result => {
      this.currentPageConferenceData = result.map((item) => {
        return Object.assign({
          "TimeConversion": utils.formatTime12Hr(item.Time__c)
        }, item)
      });

      this.currentPageEntranceConferenceData = this.currentPageConferenceData.filter(function (item) {
        return item.Type__c == "Entrance Conference"
      });
      this.currentPageExitConferenceData = this.currentPageConferenceData.filter(function (item) {
        return item.Type__c == "Exit Conference"
      });

      for (var i = 0; i < this.currentPageEntranceConferenceData.length; i++) {
        var staffnamemanipulation = [];
        var staffemployeemanipulation = [];
        this.currentPageEntranceConferenceData[i].StaffNames__c.split(/\s*;\s*/).forEach(function (myString) {
          staffnamemanipulation.push({
            value: myString
          });
        });
        this.currentPageEntranceConferenceData[i].StaffEmployees__c.split(/\s*;\s*/).forEach(function (myString) {
          staffemployeemanipulation.push({
            value: myString
          });
        });
        this.currentPageEntranceConferenceData[i].StaffNamesEdit = this.currentPageEntranceConferenceData[i].StaffNames__c;
        this.currentPageEntranceConferenceData[i].StaffEmployeesEdit = this.currentPageEntranceConferenceData[i].StaffEmployees__c;

        this.currentPageEntranceConferenceData[i].StaffNames__c = staffnamemanipulation;
        this.currentPageEntranceConferenceData[i].StaffEmployees__c = staffemployeemanipulation;

      }

      for (var i = 0; i < this.currentPageExitConferenceData.length; i++) {
        var staffnamemanipulation = [];
        var staffemployeemanipulation = [];
        this.currentPageExitConferenceData[i].StaffNames__c.split(/\s*;\s*/).forEach(function (myString) {
          staffnamemanipulation.push({
            value: myString
          });
        });
        this.currentPageExitConferenceData[i].StaffEmployees__c.split(/\s*;\s*/).forEach(function (myString) {
          staffemployeemanipulation.push({
            value: myString
          });
        });

        this.currentPageExitConferenceData[i].StaffNames__c = staffnamemanipulation;
        this.currentPageExitConferenceData[i].StaffEmployees__c = staffemployeemanipulation;

      }

      if (this.currentPageConferenceData.length == 0) {
        this.norecorddisplay = false;
      } else {
        this.norecorddisplay = true;
      }
      this.Spinner = false;

      this.toggleRowContinuation();


    }).catch(errors => {
      let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
      return this.dispatchEvent(utils.handleError(errors, config));
    });

  }

  //This function used to get the frontend data while changing the inputs
  handleChange(event) {
    if (event.target.name == "Type__c") {
      this.addoreditConference.Type__c = event.target.value;
    } else if (event.target.name == "Date__c") {
      this.addoreditConference.Date__c = event.target.value;
    } else if (event.target.name == "Time__c") {
      this.addoreditConference.Time__c = event.target.value;
    } else if (event.target.name == "StaffNames__c") {
      this.addoreditConference.StaffNames__c = event.target.value;
    } else if (event.target.name == "ProgramChangesUpdates__c") {
      this.addoreditConference.ProgramChangesUpdates__c = event.target.value;
    } else if (event.target.name == "Summary__c") {
      this.addoreditConference.Summary__c = event.target.value;
    }
  }

  //This function used to save the conference records with validation
  saveMethod() {
    this.saveDisable = true;
    this.addoreditConference.StaffEmployees__c = this.getStateEmployeeValues();
    this.addoreditConference.StaffNames__c = this.getStateEmployeeValuesStaff();
    if (!this.addoreditConference.Type__c) {
      this.dispatchEvent(utils.toastMessage("Please enter a conference type", "error"));
    } else if (!this.addoreditConference.Date__c) {
      this.dispatchEvent(utils.toastMessage("Please enter a conference date", "error"));
    } else if (!this.addoreditConference.Time__c) {
      this.dispatchEvent(utils.toastMessage("Please enter a conference time", "error"));
    } else if (!this.addoreditConference.StaffNames__c) {
      this.dispatchEvent(utils.toastMessage("Please enter a conference staff names", "error"));
    } else if (!this.addoreditConference.StaffEmployees__c) {
      this.dispatchEvent(utils.toastMessage("Please enter a conference staff employees", "error"));
    } else {

      if (this.addoreditConference.Type__c == "Exit Conference") {
        this.addoreditConference.Conference__c = this.conferenceID;
        if (this.addoreditConference.Time__c == this.timebackup) {
          this.addoreditConference.Time__c = this.timebackupoldvalue;
        }
      }
      this.addoreditConference.sobjectType = 'Conference__c';
      this.addoreditConference.Monitoring__c = this.getMonitoringID;
      InsertUpdateConference({
        objSobjecttoUpdateOrInsert: this.addoreditConference
      }).then(result => {
        this.dispatchEvent(utils.toastMessage("Record Saved Successfully", "Success"));
        this.certificationDetailsLoad();
        this.addoreditConference = {};
        this.entranceCloseModal();
        fireEvent(this.pageRef, "RefreshActivityPage", true);
        this.saveDisable = false;
      }).catch(errors => {
        let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DOCUMENT_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
        return this.dispatchEvent(utils.handleError(errors, config));
      });
    }

  }

  //This function used to delete the conference records with validation
  deleteConferenceOpenModel(event) {
    if (this.userProfileName === 'Supervisor')
      return this.dispatchEvent(utils.toastMessage("Sorry, you cannot delete conference", "warning"));

    this.deleteEventId = event.target.dataset.id;
    this.deleteEventName = event.target.name;

    this.deleteOpenModel();
  }

  deleteConferenceRecord() {

    if (this.getMonitoringStatus !== "Completed") {

      if (this.deleteEventName == "DeleteChild") {
        deleteRecord(this.deleteEventId).then(result => {
          this.dispatchEvent(utils.toastMessage("Record has been deleted Successfully", "Success"));
          this.deleteCloseModel();
          this.certificationDetailsLoad();
        }).catch(errors => {
          let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DELETE_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
          return this.dispatchEvent(utils.handleError(errors, config));
        });

      } else if (this.deleteEventName == "DeleteParent") {
        var childID = this.currentPageExitConferenceData.filter(item => {
          return item.Conference__c == this.deleteEventId
        });
        var delParentandChild = [];

        if (childID.length > 0) {
          delParentandChild.push({
            "Id": childID[0].Id
          }, {
            "Id": this.deleteEventId
          })
        } else {
          delParentandChild.push({
            "Id": this.deleteEventId
          })
        }
        for (let i = 0; i < delParentandChild.length; i++) {
          deleteRecord(delParentandChild[i].Id).then(result => {
            this.dispatchEvent(utils.toastMessage("Record has been deleted Successfully", "Success"));
            this.deleteCloseModel();
            this.certificationDetailsLoad();
          }).catch(errors => {
            let config = { friendlyErrorMsg: CJAMS_CONSTANTS.DELETE_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
            return this.dispatchEvent(utils.handleError(errors, config));
          })
        }

      }
    } else {
      this.dispatchEvent(utils.toastMessage("Cannot delete conference for completed monitoring", "warning"));
    }


  }

  //This function used to collapse and expand the rows

  @track exitID;
  @track exitType__c;
  @track exitDate__c;
  @track exitTime__c;
  @track exitStaffNames__c;
  @track exitStaffEmployees__c;
  @track exitProgramChangesUpdates__c;
  @track exitSummary__c;


  toggleRow(event) {

    event.preventDefault();

    this.staffnameshowhide = false;

    this.recId = event.target.dataset.id;
    this.conferenceID = event.target.dataset.id;

    var exitarray = [];

    for (var i = 0; i < this.currentPageExitConferenceData.length; i++) {
      if (this.currentPageExitConferenceData[i].Conference__c == this.recId) {
        exitarray.push(this.currentPageExitConferenceData[i]);
      }
    }

    if (exitarray.length != 0) {
      this.noexitrecorddisplay = true;
      this.exitID = exitarray[0].Id;
      this.exitType__c = exitarray[0].Type__c;
      this.exitDate__c = exitarray[0].Date__c;
      this.exitTime__c = exitarray[0].TimeConversion;
      this.exitStaffNames__c = exitarray[0].StaffNames__c;
      this.exitStaffEmployees__c = exitarray[0].StaffEmployees__c;
      this.exitProgramChangesUpdates__c = exitarray[0].ProgramChangesUpdates__c;
      this.exitSummary__c = exitarray[0].Summary__c;
    } else {
      this.noexitrecorddisplay = false;
    }

    this.toggleRowContinuation();

  }

  toggleRowContinuation() {
    let conList = this.template.querySelectorAll('.conli');
    let btnList = this.template.querySelectorAll('.btn');

    for (let j = 0; j < btnList.length; j++) {
      btnList[j].iconName = "utility:chevrondown";
    }


    for (let i = 0; i < conList.length; i++) {
      let indvRow = conList[i];
      let indvRowId = conList[i].getAttribute('id');



      if (indvRowId.includes(this.recId)) {

        if (indvRow.classList.contains("slds-showd")) {
          indvRow.classList.add("slds-hide");
          indvRow.classList.remove("slds-showd");
          btnList[i].iconName = "utility:chevrondown";
          btnList[i].label = "Detail View";
        } else {
          indvRow.classList.remove("slds-hide");
          indvRow.classList.add("slds-showd");
          btnList[i].iconName = "utility:chevronup";
          btnList[i].label = "Hide";
        }
      } else {
        indvRow.classList.add("slds-hide");
        indvRow.classList.remove("slds-showd");
        btnList[i].label = "Detail View"
      }


    }
  }

  //PickList for conference type
  @wire(getConferenceType, {
    objInfo: {
      sobjectType: "Conference__c"
    },
    picklistFieldApi: "Type__c"
  })
  wiregetpicklistconferencetype({
    error,
    data
  }) {
    /*eslint-disable*/
    if (data) {
      this.ConferenceTypeOptions = data;
    } else if (error) {
      this.dispatchEvent(utils.toastMessage("Conference type not fetched", "error"));
    }
  }

  //get all active users list from respective server
  @wire(getAllUsersList)
  wireduserDetails1({
    error,
    data
  }) {
    /*eslint-disable*/

    if (data) {
      let userslist = [];
      userslist = data.filter(item => item.Name != "Automated Process" && item.Name != "Platform Integration User" && item.Name != "System");
      this.stateemployes = userslist;
    } else if (error) {
      this.dispatchEvent(utils.toastMessage("State Employees not fetched", "error"));
    }
  }

  //This function used to get all the staffs from provider
  initialStaffMemberLoad() {

    assingStaffMember({
      newassignStaff: this.providerId
    }).then(result => {
      this.assignDataItem = result;
      this.staffLoad1();

    }).catch(errors => {
      let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
      return this.dispatchEvent(utils.handleError(errors, config));
    });

  }

  //This function used to get all the staffs from application contact
  staffLoad1() {
    getStaffFromApplicationID({
      getcontactfromapp: this.applicationID
    }).then(result => {
      this.assignDataItem1 = result;
      this.staffload2();
    }).catch(errors => {
      let config = { friendlyErrorMsg: CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, errorType: CJAMS_CONSTANTS.ERROR_TOASTMESSAGE }
      return this.dispatchEvent(utils.handleError(errors, config));
    });
  }

  //This function used to get all the consolidated staffs from respective application
  staffload2() {
    let tmperrarray = [];
    for (let i = 0; i < this.assignDataItem.length; i++) {
      for (let j = 0; j < this.assignDataItem1.length; j++) {
        if (this.assignDataItem1[j].Staff__c == this.assignDataItem[i].Id) {
          tmperrarray.push(this.assignDataItem[i]);
        }
      }
    }
    let testarray = [];
    tmperrarray.forEach(item => {
      testarray.push({
        Id: item.Id,
        Name: item.FirstName__c + " " + item.LastName__c
      });
    });
    this.assignStaffdropdownshow = testarray;
  }

  //Code start multi select in state employees
  get pillvalueForPType() {
    if (this.pillValueForPtype.length > 1) {
      this.pillSingleValueForPtype = this.pillValueForPtype[0].value;
      this.pillSingleValueCondition = false;
      this.pillSingleValueForPtypeCount = this.pillValueForPtype.length - 1;
      return this.pillSingleValueForPtype;
    } else {
      this.pillSingleValueCondition = true;
      return this.pillValueForPtype;
    }

  }

  handleMouseOutButton(evr) {
    this.activeForPType = false;
  }
  get toggledivForPtype() {
    return this.pillValueForPtype.length == 0 ? false : true;
  }
  handlePType(evt) {
    //if (!this.isDisableProgramType)
    this.programTypeCheckBoxSelectUnselectDuringPageLoad();
    this.activeForPType = this.activeForPType ? false : true;
  }

  handleRemove(evt) {
    this.pillValueForPtype = this.remove(
      this.pillValueForPtype,
      "value",
      evt.target.label
    );
    let i;
    let checkboxes = this.template.querySelectorAll("[data-id=checkbox]");
    checkboxes.forEach(element => {
      if (element.value == evt.target.label) {
        if (element.checked) {
          element.checked = false;
        }
      }
    });
    this.activeForPType = true;
  }
  get tabClassForPType() {
    return this.activeForPType ?
      "slds-popover slds-popover_full-width slds-popover_show" :
      "slds-popover slds-popover_full-width slds-popover_hide";
  }
  handleclickofPTypeCheckBox(evt) {
    if (evt.target.checked) {
      this.pillValueForPtype.push({
        value: evt.target.value
      });
    } else {
      this.pillValueForPtype = this.remove(
        this.pillValueForPtype,
        "value",
        evt.target.value
      );
    }
    evt.target.checked != evt.target.checked;
  }
  remove(array, key, value) {
    const index = array.findIndex(obj => obj[key] === value);
    return index >= 0 ? [...array.slice(0, index), ...array.slice(index + 1)] :
      array;
  }

  getStateEmployeeValues() {
    var sArray = [];
    this.pillValueForPtype.forEach(function (item) {
      sArray.push(item.value);
    });
    return sArray.join(";");
  }

  programTypeCheckBoxSelectUnselectDuringPageLoad() {
    let localObject = [];
    if (this.addoreditConference.StaffEmployees__c) {
      this.addoreditConference.StaffEmployees__c.split(/\s*;\s*/).forEach(function (myString) {
        localObject.push({
          value: myString
        });
      });

      var inp = [...this.template.querySelectorAll("[data-id=checkbox]")];
      inp.forEach(element => {
        localObject.forEach(function (item) {
          if (item.value == element.value) element.checked = true;
        });
      });
      this.pillValueForPtype = localObject;
    }
  }

  //Code end multi select in state employees


  //Code start multi select in staff employees

  get pillvalueForPTypeStaff() {
    if (this.pillValueForPtypeStaff.length > 1) {
      this.pillSingleValueForPtypeStaff = this.pillValueForPtypeStaff[0].value;
      this.pillSingleValueConditionStaff = false;
      this.pillSingleValueForPtypeCountStaff = this.pillValueForPtypeStaff.length - 1;
      return this.pillSingleValueForPtypeStaff;
    } else {
      this.pillSingleValueConditionStaff = true;
      return this.pillValueForPtypeStaff;
    }

  }

  handleMouseOutButtonStaff(evr) {
    this.activeForPTypeStaff = false;
  }
  get toggledivForPtypeStaff() {
    return this.pillValueForPtypeStaff.length == 0 ? false : true;
  }
  handlePTypeStaff(evt) {
    //if (!this.isDisableProgramType);
    this.programTypeCheckBoxSelectUnselectDuringPageLoadStaff();
    this.activeForPTypeStaff = this.activeForPTypeStaff ? false : true;
  }

  handleRemoveStaff(evt) {
    this.pillValueForPtypeStaff = this.remove(
      this.pillValueForPtypeStaff,
      "value",
      evt.target.label
    );
    let i;
    let checkboxes = this.template.querySelectorAll("[data-id=checkbox]");
    checkboxes.forEach(element => {
      if (element.value == evt.target.label) {
        if (element.checked) {
          element.checked = false;
        }
      }
    });
    this.activeForPTypeStaff = true;
  }
  get tabClassForPTypeStaff() {
    return this.activeForPTypeStaff ?
      "slds-popover slds-popover_full-width slds-popover_show" :
      "slds-popover slds-popover_full-width slds-popover_hide";
  }
  handleclickofPTypeCheckBoxStaff(evt) {
    if (evt.target.checked) {
      this.pillValueForPtypeStaff.push({
        value: evt.target.value
      });
    } else {
      this.pillValueForPtypeStaff = this.remove(
        this.pillValueForPtypeStaff,
        "value",
        evt.target.value
      );
    }
    evt.target.checked != evt.target.checked;
  }

  getStateEmployeeValuesStaff() {
    var sArray = [];
    this.pillValueForPtypeStaff.forEach(function (item) {
      sArray.push(item.value);
    });
    return sArray.join(";");
  }

  programTypeCheckBoxSelectUnselectDuringPageLoadStaff() {
    let localObject = [];
    if (this.addoreditConference.StaffNames__c) {
      this.addoreditConference.StaffNames__c.split(/\s*;\s*/).forEach(function (myString) {
        localObject.push({
          value: myString
        });
      });

      var inp = [...this.template.querySelectorAll("[data-id=checkbox]")];
      inp.forEach(element => {
        localObject.forEach(function (item) {
          if (item.value == element.value) element.checked = true;
        });
      });
      this.pillValueForPtypeStaff = localObject;
    }
  }

  //Code end multi select in staff employees

  //Table sort function here

  sortedDirection = 'asc';
  sortedColumn;
  @track ascdscshowhide = true;

  sort(e) {
    if (this.sortedColumn === e.currentTarget.dataset.id) {
      this.sortedDirection = this.sortedDirection === 'asc' ? 'desc' : 'asc';
    } else {
      this.sortedDirection = 'asc';
    }
    var reverse = this.sortedDirection === 'asc' ? 1 : -1;
    let table = JSON.parse(JSON.stringify(this.currentPageEntranceConferenceData));
    table.sort((a, b) => {
      return a[e.currentTarget.dataset.id] > b[e.currentTarget.dataset.id] ? 1 * reverse : -1 * reverse
    });
    this.sortedColumn = e.currentTarget.dataset.id;
    this.currentPageEntranceConferenceData = table;

    if (this.sortedDirection == 'asc') {
      this.ascdscshowhide = true;
    } else {
      this.ascdscshowhide = false;
    }


  }
  /*User profile based button hide */
  get userProfileName() {
    return shareddata.getUserProfileName();
  }


}