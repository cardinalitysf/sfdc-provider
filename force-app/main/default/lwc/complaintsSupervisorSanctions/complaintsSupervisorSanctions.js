/**
 * @Author        : K.Sundar
 * @CreatedOn     : JULY 05, 2020
 * @Purpose       : This component contains Sanctions for Supervisor
 **/

import { LightningElement, track, wire } from 'lwc';

import getAllSanctionDetails from '@salesforce/apex/ComplaintsSupervisorSanctions.getAllSanctionDetails';

import myResource from '@salesforce/resourceUrl/styleSheet';
import images from '@salesforce/resourceUrl/images';
import * as sharedData from 'c/sharedData';
import { refreshApex } from '@salesforce/apex';
import { utils } from 'c/utils';
import { CJAMS_CONSTANTS } from 'c/constants';

//List Screen Data Table Header Action Columns Declaration
const actions = [{
        label: 'Edit',
        name: 'Edit',
        iconName: 'utility:edit',
        target: '_self'
    }, {
        label: 'View',
        name: 'View',
        iconName: 'utility:preview',
        target: '_self'
    }
];

//List Screen Data Table Header Columns Declaration
const columns = [{
        label: 'SANCTION ID',
        fieldName: 'Name',
        type: 'text'
    }, {
        label: 'PROVIDER ID',
        fieldName: 'Provider__c',
        type: 'text'
    }, {
        label: 'PROVIDER NAME',
        fieldName: 'ProviderName__c',
        type: 'text'
    }, {
        label: 'PROGRAM',
        fieldName: 'Program__c',
        type: 'text'
    }, {
        label: 'SUSPENSION',
        fieldName: 'Suspension__c',
        type: 'text'
    }, {
        label: 'REVOCATION',
        fieldName: 'Revocation__c',
        type: 'text'
    }, {
        label: 'LIMITATION',
        fieldName: 'Limitation__c',
        type: 'text'
    }, {
        type: 'action',
        typeAttributes: {
            rowActions: actions
        },
        cellAttributes: {
            iconName: 'utility:threedots_vertical',
            iconAlternativeText: '',
            class: "tripledots"
        }
    }
];

export default class ComplaintsSupervisorSanctions extends LightningElement {
    //Variable Initializations
    @track columns = columns;
    @track sanctionDetails = {};
    @track noRecordsFound = false;

    //Initialization to Pagination
    @track totalSanctionsCount = 0;
    @track totalSanctions = [];

    //Pop Up Model Flags Initialization
    @track openModel = false;
    @track generateModel = false;
    @track viewModel = false;

    //Initialization to get Complaint Data
    complaintforApiCall = this.complaintId;
    sanctionId;
    attachmentIcon = images + '/document-newIcon.svg';
    wiredSanctionData;

    @track Spinner = true;
    @track generateSanctionModel = false;

    //Get Complaint ID
    get complaintId() {
        return sharedData.getCaseId();
    }

    //get Complaint Status
    get complaintStatus() {
        return sharedData.getComplaintsStatus();
    }

    //Get Data Table Values for Complaint Sanctions
    @wire(getAllSanctionDetails, {
        complaintId: '$complaintforApiCall'
    })
    allIRCRateData(result) {
        this.wiredSanctionData = result;

        if (result.data) {
            this.totalSanctionsCount = result.data.length;

            if (this.totalSanctionsCount == 0)
                this.noRecordsFound = true;
            else
                this.noRecordsFound = false;

            this.totalSanctions = result.data.map(row => {
                return {
                    Id: row.Id,
                    Name: row.Name,
                    Provider__c: (row.Provider__r && row.Provider__r.ProviderId__c) ? row.Provider__r.ProviderId__c : ' - ',
                    ProviderName__c: (row.Provider__r && row.Provider__r.FirstName__c) ? row.Provider__r.FirstName__c + (row.Provider__r &&  row.Provider__r.LastName__c ? ' - ' + row.Provider__r.LastName__c : ' - ') : (row.Provider__r &&  row.Provider__r.LastName__c ? ' - ' + row.Provider__r.LastName__c : ' - '),
                    Program__c: row.Complaint__r.Program__c,
                    Suspension__c: row.Suspension__c == true ? 'Yes' : 'No',
                    Revocation__c: row.Revocation__c == true ? 'Yes' : 'No',
                    Limitation__c: row.Limitation__c == true ? 'Yes' : 'No',
                    PageType: row.RecordType.Name
                }
            });

            //Event for Sanction Added
            if (this.totalSanctions.length > 0) {
                const selectedEvent = new CustomEvent("sanctionflag", {
                    detail: {
                        sanctionflag: true,
                    }
                });
                this.dispatchEvent(selectedEvent);
            }

            refreshApex(this.wiredSanctionData);
            this.Spinner = false;
        } else {
            this.noRecordsFound = true;
        }
    }

    /* Generate Sanction Open */
    openSanctionModel() {
        this.generateSanctionModel = true;
    }

    intimateParentToCloseModal() {
        this.generateSanctionModel = false;
    }

    handleSanctionTypeRedirection(event) {
        const onClickNewDirection = new CustomEvent('redirecttosanctiontype', {
            detail: {
                first: false,
                pageType: event.detail.pageType
            }
        });
        this.dispatchEvent(onClickNewDirection);
    }
    /* Generate Sanction Close */

    //On Click Event to see Edit/View Model
    handleRowAction(event) {
        let actionName = event.detail.action.name;
        let selectedSanctionId = event.detail.row.Id;
        let pageName = event.detail.row.PageType;
        let sanctionName = event.detail.row.Name;

        if (actionName == 'Edit' && [CJAMS_CONSTANTS.APPLICATION_REVIEW_STATUS_APPROVED, CJAMS_CONSTANTS.APPLICATION_REVIEW_STATUS_REJECTED].includes(this.complaintStatus)) {
            return this.dispatchEvent(utils.toastMessage(`Sorry, you do not have access to edit ${this.complaintStatus} complaint`, "warning"))
        }

        const handleSanctionTypeRedirection = new CustomEvent('redirecttosanctiontype', {
            detail: {
                pageType: pageName,
                selectedSanctionId: selectedSanctionId,
                actionName: actionName,
                sanctionName: sanctionName
            }
        });
        this.dispatchEvent(handleSanctionTypeRedirection);
    }
}