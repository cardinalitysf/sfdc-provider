import {
    LightningElement,
    track

} from 'lwc';
import * as referralsharedDatas from "c/sharedData";
import {
    CJAMS_CONSTANTS
} from 'c/constants';


export default class PublicApplicationNarrative extends LightningElement {
    get applicationId() {
        return referralsharedDatas.getApplicationId();
     
    }
    get sobjectName() {
     return CJAMS_CONSTANTS.APPLICATION_OBJECT_NAME;
    }
}