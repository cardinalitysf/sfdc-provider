import { LightningElement, api } from 'lwc';

export default class ProviderAllDashboardCmps extends LightningElement {
    @api validatecheckfromprogramdetails = false;
    hideShowHomeAndTypesCmp = true;

    get showHideDashBoardAndViewAllProviders() {
        return this.hideShowHomeAndTypesCmp;
    }

    handleredirectEditApplication(event) {
        this.hideShowHomeAndTypesCmp = event.detail.first;
    }

    handleValidationCheck(event) {
        this.validatecheckfromprogramdetails = event.detail.validate;
    }

    showProviderApplicationDashbord(event) {
        this.hideShowHomeAndTypesCmp = event.detail.first;
    }
}