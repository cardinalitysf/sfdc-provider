/**
 * @Author        : Naveen S
 * @CreatedOn     : May 14 ,2020
 * @Purpose       : Traning Session View and Update Attandance 
 * @updatedBy     :
 * @updatedOn     :
 **/


//Record Fileds

import ID_FIELD from '@salesforce/schema/Training__c.Id';
import STATUS_FIELD from '@salesforce/schema/Training__c.Status__c';
import {
    updateRecord
} from "lightning/uiRecordApi";
import {
    LightningElement,
    track,
    wire
} from 'lwc';
import {
    utils
} from "c/utils";
import { CJAMS_CONSTANTS } from 'c/constants';
import * as sharedData from 'c/sharedData';
import getTrainingDetails from "@salesforce/apex/PublicProviderViewTrainingSession.getTrainingDetails";
import getPrideBasicDetails from "@salesforce/apex/PublicProviderViewTrainingSession.getPrideBasicDetails";
import getmeetingInfoPrideList from "@salesforce/apex/PublicProviderViewTrainingSession.getmeetingInfoPrideList";
import getPrideHistoryList from "@salesforce/apex/PublicProviderViewTrainingSession.getPrideHistoryList";
import getmeetingInfoOtherPrideList from "@salesforce/apex/PublicProviderViewTrainingSession.getmeetingInfoOtherPrideList";
import bulkUpdateIntentAttend from "@salesforce/apex/PublicProviderViewTrainingSession.bulkUpdateIntentAttend";
import getPickListValues from "@salesforce/apex/PublicProviderViewTrainingSession.getPickListValues";
import prideBulkUpdateIntentAttend from "@salesforce/apex/PublicProviderViewTrainingSession.prideBulkUpdateIntentAttend";
import getPrideCandinateAttendCount from "@salesforce/apex/PublicProviderViewTrainingSession.getPrideCandinateAttendCount";
import getOtherPrideCandinateAttendCount from "@salesforce/apex/PublicProviderViewTrainingSession.getOtherPrideCandinateAttendCount";
import images from '@salesforce/resourceUrl/images';

export default class PublicProviderViewTrainingSession extends LightningElement {
    @track trainingNo;
    @track type;
    @track sessionType;
    @track medium;
    @track date;
    @track startTime;
    @track endTime;
    @track instructors;
    @track topicDescription;
    @track address;
    @track Jurisdiction;
    @track duration;
    @track URL;
    @track hasAddresSwitch = false;
    @track hasUrlSwitch = false;
    @track otherPrideDatas;
    @track hasDurationTable = false;
    @track hasAttendTraningTable = false;
    @track hasRadioButtonTable = false;
    @track hasDropdownTable = false;
    @track PrideHistoryDatas;
    @track hasChildHistoryTable = false;
    @track Spinner = true;
    @track hasCandidateAttend;
    @track PrideHistoryCandinateAttendStatus = [];
    @track currentDate;
    @track disCandinateAttendStauts = false;
    @track saveDisable = false;
    @track dataTrainingCard = true;
    @track NoTrainingCard = false;
    @track descModel = false;
    @track topicDescriptionPopup;
    @track hasActorID;
    @track referralLabel = 'Referral No';
    @track PrideCandinateTotalCount = [];
    //Pagination Tracks    
    @track page = 1;
    perpage = 5;
    setPagination = 5;
    @track totalRecordsCount = 0;
    @track dataTableShow = true;
    @track totalRecords = [];
    @track datas = [];
    //decisionIcon = images + '/decision-icon.svg';
    descriptionIcon = images + '/description-icon.svg';
    attachmentIcon = images + '/application-monitoring.svg';
    get getTrainingId() {
        sharedData.getPublicTrainingrowData();
        let sharedDatas = sharedData.getPublicTrainingrowData();
        let array = sharedDatas.split(', ');
        return array[1];
    }

    get getPrideSessionID() {
        let sharedDatas = sharedData.getPublicTrainingrowData();
        let array = sharedDatas.split(', ');
        return array[0];
    }
    get getPublicTrainingStatus() {
        let sharedDatas = sharedData.getPublicTrainingrowData();
        let array = sharedDatas.split(', ');
        return array[2];
    }
    @wire(getPickListValues, {
        objInfo: {
            sobjectType: "MeetingInfo__c"
        },
        picklistFieldApi: "CandidateAttend__c"
    })
    wireCheckPicklistCandinateAttend({
        error,
        data
    }) {
        if (data) {
            this.hasCandidateAttend = data;
        } else if (error) { }
    }


    connectedCallback() {
        this.getTrainingInformation();
        this.trainingCompleteStatus();
        if (this.getPublicTrainingStatus == 'Completed') {
            this.saveDisable = true;
            this.disablestatusCA = true;
        } else {
            this.saveDisable = false;
        }
        var today = new Date();
        var monthDigit = today.getMonth() + 1;
        if (monthDigit <= 9) {
            monthDigit = '0' + monthDigit;
        }
        var dayDigit = today.getDate();
        if (dayDigit <= 9) {
            dayDigit = '0' + dayDigit;
        }
        var date = today.getFullYear() + "-" + monthDigit + "-" + dayDigit;
        this.currentDate = date;
    }

    getTrainingInformation() {
        getTrainingDetails({
            trainingId: this.getTrainingId
        })
            .then((result) => {
                if (result.length > 0 && result[0].Name) {
                    let _instructor2Name = result[0].Instructor2__r != undefined ? ' , ' + result[0].Instructor2__r.Name : '';
                    this.trainingNo = result[0].Name;
                    this.type = result[0].Type__c ? result[0].Type__c : ' - ';
                    this.sessionType = result[0].SessionType__c ? result[0].SessionType__c : ' - ';
                    this.medium = result[0].Medium__c ? result[0].Medium__c : ' - ';
                    this.instructors = result[0].Instructor1__r.Name + _instructor2Name;
                    this.topicDescription = result[0].Description__c ? result[0].Description__c : ' - ';
                    this.Jurisdiction = result[0].Jurisdiction__c ? result[0].Jurisdiction__c : ' - ';
                    this.duration = result[0].Duration__c ? result[0].Duration__c : ' - ';
                    this.date = utils.formatDate(result[0].Date__c);
                    this.startTime = utils.formatTime12Hr(result[0].StartTime__c);
                    this.endTime = utils.formatTime12Hr(result[0].EndTime__c);
                    if (this.medium != ' - ') {
                        if (this.medium == 'Online') {
                            this.URL = result[0].URL__c ? result[0].URL__c : ' - ';
                            this.hasUrlSwitch = true;
                            this.hasAddresSwitch = false;
                        } else {
                            this.address = result[0].AddressLine1__c ? result[0].AddressLine1__c : ' - ';
                            this.hasUrlSwitch = false;
                            this.hasAddresSwitch = true;
                        }
                    }
                    if (this.sessionType == 'PRIDE') {
                        getPrideBasicDetails({
                            trainingId: this.getTrainingId,
                            sessionNo: this.getPrideSessionID,
                        })
                            .then((result) => {
                                if (result.length > 0) {
                                    this.duration = result[0].Duration__c ? result[0].Duration__c : ' - ';
                                    this.date = utils.formatDate(result[0].Date__c);
                                    this.startTime = utils.formatTime12Hr(result[0].StartTime__c);
                                    this.endTime = utils.formatTime12Hr(result[0].EndTime__c);
                                    this.hasAttendTraningTable = false;
                                    this.hasDurationTable = true;
                                    this.hasDurationValue = true;
                                    this.hasRadioButtonTable = false;
                                    this.hasDropdownTable = true;
                                }
                                this.meetingInformationsPrideDetailsLoad();
                            });
                    } else {
                        this.hasAttendTraningTable = true;
                        this.hasDurationTable = false;
                        this.hasDurationValue = false;
                        this.hasRadioButtonTable = true;
                        this.hasDropdownTable = false;
                        this.meetingInformationsOtherPrideLoad();
                    }
                    this.Spinner = false;
                }
            })
            .catch(errors => {
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const { title, message, errorType } = error;
                    this.dispatchEvent(
                        utils.toastMessageWithTitle(title, message, errorType)
                    );
                } else {
                    this.dispatchEvent(
                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                    );
                }
            });
    }

    //This function used to get the backend data and show tables 
    meetingInformationsPrideDetailsLoad() {
        getmeetingInfoPrideList({
            trainingId: this.getTrainingId,
            sessionNo: this.getPrideSessionID,
        }).then(result => {
            if (result.length > 0) {
                this.dataTrainingCard = true;
                this.NoTrainingCard = false;
                this.otherPrideDatas = result.map(row => {
                    return {
                        meetingInfoId: row.Id,
                        duration: row.MeetingInfo__r.Duration__c + ' : hrs',
                        traineeFirstName: row.Actor__r != undefined ? row.Actor__r.Contact__r.FirstName__c : '',
                        traineeLastName: row.Actor__r != undefined ? row.Actor__r.Contact__r.LastName__c : '',
                        program: row.Application__r != undefined ? row.Application__r.Program__c : '',
                        programType: row.Application__r != undefined ? row.Application__r.ProgramType__c : '',
                        referralNo: row.Application__r != undefined ? row.Application__r.Name : '',
                        CandidateAttend: row.CandidateAttend__c,
                        actorid: row.Actor__c,
                        phase: 'Application',
                        colorstatus: row.MeetingInfo__r.Duration__c >= 27 ? 'green-duration' : 'red-duration'
                    }
                });
                this.totalRecordsCount = this.otherPrideDatas.length;
                this.pageData();
                if (this.sessionType == 'PRIDE') {
                    this.hasDropdownTable = true;
                }
                else {
                    this.hasDropdownTable = false;
                }
            } else {
                this.dataTrainingCard = false;
                this.NoTrainingCard = true;
            }
            this.referralLabel = 'Application No';
        })
            .catch(errors => {
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const { title, message, errorType } = error;
                    this.dispatchEvent(
                        utils.toastMessageWithTitle(title, message, errorType)
                    );
                } else {
                    this.dispatchEvent(
                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                    );
                }
            });
    }

    //This function used to get the backend data and show tables 
    meetingInformationsOtherPrideLoad() {
        getmeetingInfoOtherPrideList({
            trainingId: this.getTrainingId,
        }).then(result => {
            if (result.length > 0) {
                this.dataTrainingCard = true;
                this.NoTrainingCard = false;
                var statusComplete = this.getPublicTrainingStatus;
                this.otherPrideDatas = result.map(row => {
                    return {
                        meetingInfoId: row.Id,
                        duration: row.Duration__c,
                        traineeFirstName: row.Actor__r != undefined ? row.Actor__r.Contact__r.FirstName__c : '',
                        traineeLastName: row.Actor__r != undefined ? row.Actor__r.Contact__r.LastName__c : '',
                        program: row.Referral__r != undefined ? row.Referral__r.Program__c : '',
                        programType: row.Referral__r != undefined ? row.Referral__r.ProgramType__c : '',
                        referralNo: row.Referral__r != undefined ? row.Referral__r.CaseNumber : '',
                        CandidateAttend: row.CandidateAttend__c,
                        phase: 'Referral',
                        date: utils.formatDate(row.Date__c),
                        disablestatusCA: this.currentDate < row.Date__c || statusComplete == 'Completed' ? true : false
                    }
                });
                this.hasDropdownTable = false;
            } else {
                this.dataTrainingCard = false;
                this.NoTrainingCard = true;
            }
            this.referralLabel = 'Referral No';
        })
            .catch(errors => {
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const { title, message, errorType } = error;
                    this.dispatchEvent(
                        utils.toastMessageWithTitle(title, message, errorType)
                    );
                } else {
                    this.dispatchEvent(
                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                    );
                }
            });
    }

    //This function used to get the backend data and showing Pride History 
    prideSessionHistory() {
        getPrideHistoryList({
            trainingId: this.getTrainingId,
            actorid: this.hasActorID
        }).then(result => {
            if (result.length > 0) {
                this.Spinner = false;
                var statusComplete = this.getPublicTrainingStatus;
                this.PrideHistoryDatas = result.map(row => {
                    return {
                        meetingInfoId: row.Id,
                        duration: row.MeetingInfo__r.Duration__c + ' : hrs',
                        date: utils.formatDate(row.MeetingInfo__r.Date__c),
                        startDate: utils.formatTime12Hr(row.MeetingInfo__r.StartTime__c),
                        endDate: utils.formatTime12Hr(row.MeetingInfo__r.EndTime__c),
                        sessionNumber: row.MeetingInfo__r.SessionNumber__c,
                        CandidateAttend: row.CandidateAttend__c,
                        disablestatusCA: this.currentDate < row.Date__c || statusComplete == 'Completed' ? true : false
                    }
                });
                this.totalRecordsCount = this.PrideHistoryDatas.length;
                this.pageData();
            }
        })
            .catch(errors => {
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const { title, message, errorType } = error;
                    this.dispatchEvent(
                        utils.toastMessageWithTitle(title, message, errorType)
                    );
                } else {
                    this.dispatchEvent(
                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                    );
                }
            });
    }
    prideSessionHistoryLoad(event) {
        this.hasActorID = event.target.dataset.value;
        this.Spinner = true;
        this.prideSessionHistory();
        event.preventDefault();
        let conList = this.template.querySelectorAll('.conli');
        let btnList = this.template.querySelectorAll('.btn');
        btnList.forEach((item) => {
            item.className = "dropdown";
        })
        conList.forEach((item) => {
            let indvRow = item;
            let indvRowId = item.getAttribute('id');
            if (indvRowId.includes(event.target.dataset.id)) {
                if (indvRow.classList.contains("slds-showd")) {
                    indvRow.classList.add("slds-hide");
                    indvRow.classList.remove("slds-showd");
                } else {
                    indvRow.classList.remove("slds-hide");
                    indvRow.classList.add("slds-showd");
                }
            } else {
                indvRow.classList.add("slds-hide");
                indvRow.classList.remove("slds-showd");
            }
        })
    }
    handleSave() {
        this.Spinner = true;
        let datasString = JSON.stringify(this.PrideHistoryCandinateAttendStatus);
        let prideCondtionCheck = this.hasDropdownTable;
        if (prideCondtionCheck) {
            prideBulkUpdateIntentAttend({
                datas: datasString
            }).then(result => {
                this.trainingCompleteStatus();
                setTimeout(() => {
                    this.prideCandinateAttendedUpdate();
                }, 2000);
            })
                .catch(errors => {
                    if (errors) {
                        let error = JSON.parse(errors.body.message);
                        const { title, message, errorType } = error;
                        this.dispatchEvent(
                            utils.toastMessageWithTitle(title, message, errorType)
                        );
                    } else {
                        this.dispatchEvent(
                            utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                        );
                    }
                });
        }
        else {
            let datasString = JSON.stringify(this.PrideHistoryCandinateAttendStatus);
            bulkUpdateIntentAttend({
                datas: datasString
            })
                .then(result => {
                    this.otherTrainingCompleteStatus();
                    setTimeout(() => {
                        this.otherPrideCandinateAttendedUpdate();
                    }, 2000);
                })
                .catch(errors => {
                    if (errors) {
                        let error = JSON.parse(errors.body.message);
                        const { title, message, errorType } = error;
                        this.dispatchEvent(
                            utils.toastMessageWithTitle(title, message, errorType)
                        );
                    } else {
                        this.dispatchEvent(
                            utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                        );
                    }
                });
        }
    }

    PrideHistoryRadioChange(event) {
        if (this.PrideHistoryCandinateAttendStatus.findIndex(data => data.Id == event.target.dataset.id) >= 0) {
            this.PrideHistoryCandinateAttendStatus.find(v => v.Id == event.target.dataset.id).CandidateAttend__c = event.target.value;
            this.PrideHistoryCandinateAttendStatus.find(v => v.Id == event.target.dataset.id).MeetingStatus__c = event.target.value == 'Yes' ? 'Completed' : 'DNA';
            this.PrideHistoryCandinateAttendStatus.find(v => v.Id == event.target.dataset.id).SessionStatus__c = event.target.value == 'Yes' ? 'Completed' : 'DNA';
        } else {
            this.PrideHistoryCandinateAttendStatus.push({
                "Id": event.target.dataset.id,
                "CandidateAttend__c": event.target.value,
                "MeetingStatus__c": event.target.value == 'Yes' ? 'Completed' : 'DNA',
                "SessionStatus__c": event.target.value == 'Yes' ? 'Completed' : 'DNA'
            })
        }
    }

    otherPrideHistoryRadioOnChange(event) {

        if (this.PrideHistoryCandinateAttendStatus.findIndex(data => data.Id == event.target.dataset.id) >= 0) {
            this.PrideHistoryCandinateAttendStatus.find(v => v.Id == event.target.dataset.id).CandidateAttend__c = event.target.value;
            this.PrideHistoryCandinateAttendStatus.find(v => v.Id == event.target.dataset.id).SessionStatus__c = event.target.value == 'Yes' ? 'Completed' : 'DNA';

        } else {
            this.PrideHistoryCandinateAttendStatus.push({
                "Id": event.target.dataset.id,
                "CandidateAttend__c": event.target.value,
                "SessionStatus__c": event.target.value == 'Yes' ? 'Completed' : 'DNA'
            })
        }
    }
    //For Pagination Child Bind
    hanldeProgressValueChange(event) {
        this.page = event.detail;
        this.pageData();
    }

    pageData() {
        let page = this.page;
        let perpage = this.perpage;
        let startIndex = page * perpage - perpage;
        let endIndex = page * perpage;
        this.datas = [this.totalRecords.slice(startIndex, endIndex)];
    }

    redirectToTrainingSessionDashboard() {
        const onClickId = new CustomEvent('redirttotrainingsession', {
            detail: {
                first: false
            }
        });
        this.dispatchEvent(onClickId);
    }

    closeDescModal() {
        this.descModel = false;
    }
    descriptionModal() {
        if (this.topicDescription.length != 0) {
            this.descModel = true;
            this.topicDescriptionPopup = this.topicDescription;
        } else {
            this.descModel = false;
        }
    }
    redirectToTraining() {
        this.dispatchEvent(new CustomEvent('redirttotrainingsession'));
    }

    prideCandinateAttendedUpdate() {
        let totalCount = 0;
        let count = 0;
        if (this.PrideCandinateTotalCount != undefined && this.PrideCandinateTotalCount.length > 0) {
            totalCount = this.PrideCandinateTotalCount.length;
            this.PrideCandinateTotalCount.forEach(data => {
                if (data.CandidateAttend__c == 'Yes' || data.CandidateAttend__c == 'No') {
                    count = count + 1;
                }
            })
        }
        let status;
        if (totalCount == count) {
            status = "Completed";
        } else {
            status = "In Progress";
        }
        let fields = {};
        fields[ID_FIELD.fieldApiName] = this.getTrainingId;
        fields[STATUS_FIELD.fieldApiName] = status;
        const recordInput = {
            fields
        };
        updateRecord(recordInput)
            .then(result => {
                this.Spinner = false;
                this.dispatchEvent(utils.toastMessage("Records Has Been Updated Successfully!..", "success"));
                window.location.reload();
            })
    }

    trainingCompleteStatus() {
        getPrideCandinateAttendCount({
            trainingId: this.getTrainingId
        }).then(result => {
            this.PrideCandinateTotalCount = result;
        })
            .catch(errors => {
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const { title, message, errorType } = error;
                    this.dispatchEvent(
                        utils.toastMessageWithTitle(title, message, errorType)
                    );
                } else {
                    this.dispatchEvent(
                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                    );
                }
            });
    }

    otherTrainingCompleteStatus() {
        getOtherPrideCandinateAttendCount({
            trainingId: this.getTrainingId
        }).then(result => {
            this.PrideCandinateTotalCount = result;
        })
            .catch(errors => {
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const { title, message, errorType } = error;
                    this.dispatchEvent(
                        utils.toastMessageWithTitle(title, message, errorType)
                    );
                } else {
                    this.dispatchEvent(
                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                    );
                }
            });
    }
    otherPrideCandinateAttendedUpdate() {
        let totalCount = 0;
        let count = 0;
        totalCount = this.PrideCandinateTotalCount.length;
        this.PrideCandinateTotalCount.forEach(data => {
            if (data.CandidateAttend__c == 'Yes' || data.CandidateAttend__c == 'No') {
                count = count + 1;
            }
        })
        let status;
        if (totalCount == count) {
            status = "Completed";
        } else {
            status = "In Progress";
        }
        let fields = {};
        fields[ID_FIELD.fieldApiName] = this.getTrainingId;
        fields[STATUS_FIELD.fieldApiName] = status;
        const recordInput = {
            fields
        };
        updateRecord(recordInput)
            .then(result => {
                this.Spinner = false;
                this.dispatchEvent(utils.toastMessage("Records Has Been Updated Successfully!..", "success"));
                window.location.reload();
            })
            .catch(errors => {
                if (errors) {
                    let error = JSON.parse(errors.body.message);
                    const { title, message, errorType } = error;
                    this.dispatchEvent(
                        utils.toastMessageWithTitle(title, message, errorType)
                    );
                } else {
                    this.dispatchEvent(
                        utils.toastMessage(CJAMS_CONSTANTS.FRIENDLY_ERROR_MESSAGE, CJAMS_CONSTANTS.ERROR_TOASTMESSAGE)
                    );
                }
            });
    }


}