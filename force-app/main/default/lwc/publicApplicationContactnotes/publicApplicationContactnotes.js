/**
 * @Author        : Vijayaraj M
 * @CreatedOn     : June 04 , 2020
 * @Purpose       : This component for ContactNotes reusability Js file **/

 import { LightningElement, track } from "lwc";
import * as referralsharedDatas from "c/sharedData";
import { CJAMS_CONSTANTS } from "c/constants";

export default class PublicApplicationContactnotes extends LightningElement {
  get applicationId() {
    return referralsharedDatas.getApplicationId();
    //return "a0A0w000000WfB6EAK";
  }

  get sobjectName() {
    return CJAMS_CONSTANTS.APPLICATION_OBJECT_NAME;
  }
}
