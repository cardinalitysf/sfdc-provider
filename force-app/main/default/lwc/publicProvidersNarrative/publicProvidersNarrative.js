import {
    LightningElement,
    track

} from 'lwc';
import * as referralsharedDatas from "c/sharedData";
import {
    CJAMS_CONSTANTS
} from 'c/constants';


export default class PublicProvidersNarrative extends LightningElement {
    
    get providerid() {
       return referralsharedDatas.getProviderId();
     //  return '0019D000009xuaJQAQ';
    }


    get sobjectName() {
     return CJAMS_CONSTANTS.PROVIDER_OBJECT_NAME;
    }
}