/**
 * @Author        : Sindhu Venkateswarlu
 * @CreatedOn     : APRIL 6 ,2020
 * @Purpose       : Responsible for fetch or upsert the Decison and Signature Details.
**/

public with sharing class applicationproviderDecision {
    @AuraEnabled(cacheable =true)
    public static List<DMLOperationsHandler.FetchValueWrapper> getPickListValues(sObject objInfo, string picklistFieldApi) {
        return DMLOperationsHandler.fetchPickListValue(objInfo,picklistFieldApi);
    }

    @AuraEnabled
        public static void updateapplicationdetails (sObject objSobjecttoUpdateOrInsert, String supervisorId, String workItemId){
        if(supervisorId != '') {
            String Id = DMLOperationsHandler.updateOrInsertSOQLReturnId(objSobjecttoUpdateOrInsert);
            String comments = (String) objSobjecttoUpdateOrInsert.get('ApprovalComments__c');
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments(comments);
            req1.setObjectId(Id);
            req1.setSubmitterId(UserInfo.getUserId());
            req1.setNextApproverIds(new Id[] {supervisorId});
            // Submit the approval request for the Opportunity
            Approval.ProcessResult result = Approval.process(req1);
            
            System.assert(result.isSuccess(), ' Result Status: '+result.isSuccess());
        } else {
            // Approval.unlock(objSobjecttoUpdateOrInsert.Id, false);
            String Id = DMLOperationsHandler.updateOrInsertSOQLReturnId(objSobjecttoUpdateOrInsert);
            String comments = (String) objSobjecttoUpdateOrInsert.get('SupervisorComments__c');
            String status;
            if(objSobjecttoUpdateOrInsert.get('SupervisorReview__c') == 'Approved'){
                status = 'Approve';  
            }
            else if(objSobjecttoUpdateOrInsert.get('SupervisorReview__c') == 'Rejected'){
                status = 'Reject';  
            }
            else if(objSobjecttoUpdateOrInsert.get('SupervisorReview__c') == 'Return to Worker'){
                status = 'Removed';  
            }
            String caseworker = (String) objSobjecttoUpdateOrInsert.get('Caseworker__c');
            Approval.ProcessWorkitemRequest req1 = new Approval.ProcessWorkitemRequest();
            req1.setComments(comments);
            req1.setWorkitemId(workItemId);
            req1.setAction(status);
            req1.setNextApproverIds(new Id[] {caseworker});
            // Submit the approval request for the Opportunity
            Approval.ProcessResult result = Approval.process(req1);
            
            System.assert(result.isSuccess(), ' Result Status: '+result.isSuccess());
        }
    }

    @AuraEnabled
	public static string updateapplicationdetailsDraft(sObject objSobjecttoUpdateOrInsert){
        return DMLOperationsHandler.updateOrInsertSOQLReturnId(objSobjecttoUpdateOrInsert);
    }
    
    @AuraEnabled(cacheable=false)
	public static List<Application__c> getReviewDetails(string applicationid){
		String model = 'Application__c';
		String fields = 'Id,ApprovalComments__c ,ApprovalStatus__c,Status__c,SupervisorReview__c,SupervisorComments__c,LicenseStartdate__c,LicenseEnddate__c';
		String cond = 'Id =\'' + applicationid + '\'';
		return DMLOperationsHandler.selectSOQLWithConditionParameters(model, fields, cond);
    }
    @AuraEnabled(cacheable=false)
	public static List<Monitoring__c> getmonitoringDetails(string applicationid){
		String model = 'Monitoring__c';
		String fields = 'Id, Status__c';
        String cond = 'ApplicationLicenseId__c  =\'' + applicationid + '\'';

		return DMLOperationsHandler.selectSOQLWithConditionParameters(model, fields, cond);
    }
    @AuraEnabled
	public static string getDatatableDetails(string applicationid) {
        String model = 'ProcessInstance';
		String fields = 'Id, (SELECT Id, ProcessInstanceId, ActorId, Actor.Name, Actor.Profile.Name, CreatedDate FROM Workitems), (SELECT Id, StepStatus, ActorId, Actor.Name, Actor.Profile.Name, Comments, CreatedDate FROM Steps ORDER BY ID DESC)';
		String cond = 'TargetObjectId =\'' + applicationid + '\' ORDER BY ID DESC';
        List<SObject> lstApp= DMLOperationsHandler.selectSOQLWithConditionParameters(model, fields, cond);
        Map<Id, ContentDocumentLink> contentDocumentIdsmap = new Map<Id, ContentDocumentLink>();
        for(ContentDocumentLink conDocu : [SELECT ContentDocumentId, LinkedEntityId,LinkedEntity.Name FROM ContentDocumentLink WHERE LinkedEntityId =:applicationid Order By SystemModstamp DESC]){
            contentDocumentIdsmap.put(conDocu.ContentDocumentId, conDocu);
        }
        List<ContentVersion> contelist = [SELECT Id, OwnerId, CreatedById FROM ContentVersion WHERE ContentDocumentId In: contentDocumentIdsmap.keyset() And IsSignature__c=true];
        Set<Id> filteredContOwner = new Set <Id>();
        Set<ContentVersion> filteredContList = new Set <ContentVersion>();
        for(ContentVersion clist : contelist){
            if(filteredContOwner.add(clist.OwnerId) == true)
            filteredContList.add(clist);
        }
        if(lstApp.size() > 0){
            List<processingWrapper> appWrap = new List<processingWrapper>();
            for(SObject app : lstApp) {
                processingWrapper prdWrapper = new processingWrapper(app, filteredContList);
                appWrap.add(prdWrapper);
            }
            return JSON.serialize(appWrap);
        } else {
            List<processingWrapperString> appWrap = new List<processingWrapperString>();
            String modelm = 'Application__c';
            String fieldsm = 'Id,SubmittedDate__c,Caseworker__r.Name,Supervisor__r.Name,ApprovalStatus__c,ApprovalComments__c,SupervisorReview__c,SupervisorComments__c';
            String condm = 'Id =\'' + applicationid + '\'';
            List<SObject> monApp= DMLOperationsHandler.selectSOQLWithConditionParameters(modelm, fieldsm, condm);
                for(SObject app : monApp) {
                    if(contelist.size() > 0) {
                        for (contentversion conversion: contelist) {
                            processingWrapperString prdWrapper=new processingWrapperString(app, conversion.id);
                            appWrap.add(prdWrapper);
                        }
                    } else {
                        processingWrapperString prdWrapper=new processingWrapperString(app, '');
                        appWrap.add(prdWrapper);
                    }
                }
                return JSON.serialize(appWrap);

            }
        }

    @AuraEnabled
	public static list<contentversion>  getSignatureDetails(string applicationid){

       // SELECT ContentDocumentId,(SELECT Id,ContentDocumentId FROM ContentVersion) from ContentDocumentLink where LinkedEntityId='a090p000001GefSAAS'
        
       
        Map<Id, ContentDocumentLink> contentDocumentIdsmap = new Map<Id, ContentDocumentLink>();
        for(ContentDocumentLink conDocu : [SELECT ContentDocumentId, LinkedEntityId,LinkedEntity.Name FROM ContentDocumentLink WHERE LinkedEntityId =:applicationid]){
            contentDocumentIdsmap.put(conDocu.ContentDocumentId, conDocu);
        }
        list<contentversion> contelist = [select id,ContentDocumentId,Document__c,	IsSignature__c from contentversion where ContentDocumentId In: contentDocumentIdsmap.keyset()];

        return contelist; 
      
      
      //return contVersionId;
    }
    
    @AuraEnabled
    public static string updateCaseworkerdetails(sObject objSobjecttoUpdateOrInsert){
        return JSON.serialize(DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert));
    }
    
    @AuraEnabled(cacheable=true)
    public static List<User> getAllUsersList() {
        String type = 'Supervisor';
        String model = 'User';
        String fields = 'Id, Name, isActive,Profile.Name,Availability__c,CaseLoad__c,ProfileId,Unit__c';
        String cond = 'isActive = true AND Profile.Name = \''+ type +'\'';
        return DMLOperationsHandler.selectSOQLWithConditionParameters(model,fields,cond);
    }
   

    @AuraEnabled
    public static void saveSign(String strSignElement,Id recId){

        // Create Salesforce File
        //Insert ContentVersion
        ContentVersion cVersion = new ContentVersion();
        cVersion.ContentLocation = 'S'; //S-Document is in Salesforce. E-Document is outside of Salesforce. L-Document is on a Social Netork.
        cVersion.PathOnClient = 'Signature-'+System.now() +'.png';//File name with extention
        cVersion.Origin = 'H';//C-Content Origin. H-Chatter Origin.
        
        
        //cVersion.OwnerId = attach.OwnerId;//Owner of the file
        cVersion.Title = 'Signature-'+System.now() +'.png';//Name of the file
        cVersion.VersionData = EncodingUtil.base64Decode(strSignElement);//File content
        cVersion.IsSignature__c=true;
        Insert cVersion;
        
        
        //After saved the Content Verison, get the ContentDocumentId
        Id conDocument = [SELECT ContentDocumentId  FROM ContentVersion WHERE Id =:cVersion.Id].ContentDocumentId;
        
        //Insert ContentDocumentLink
        ContentDocumentLink cDocLink = new ContentDocumentLink();
        cDocLink.ContentDocumentId = conDocument;//Add ContentDocumentId
        cDocLink.LinkedEntityId = recId;//Add attachment parentId
        cDocLink.ShareType = 'I';//V - Viewer permission. C - Collaborator permission. I - Inferred permission.
        cDocLink.Visibility = 'AllUsers';//AllUsers, InternalUsers, SharedUsers
        Insert cDocLink;
      
    }


    public class processingWrapper {
        public SObject application {get;set;}
        public Set<ContentVersion> signatureUrl {get;set;}
        public processingWrapper(SObject app, Set<ContentVersion> url) {
            application = app;
            this.signatureUrl = url;
        }
    }
    public class processingWrapperString {
        public SObject application {get;set;}
        public String signatureUrl {get;set;}  
        public processingWrapperString(SObject app, String url) {
            application = app;
            this.signatureUrl = url;
        }
    }    
}