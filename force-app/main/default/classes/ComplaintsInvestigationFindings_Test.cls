/**
 * @Author        : Tamilarasan G
 * @CreatedOn     : July 16, 2020
 * @Purpose       : Complaints Investigation / Findings Dashboard AND Detail Page
**/
@isTest
    public class ComplaintsInvestigationFindings_Test {
              @isTest static void testComplaintsInvestigationFindings(){

                Deficiency__c InsertDeficiency = new Deficiency__c(Status__c = 'Pending');
                insert InsertDeficiency;
                Case InsertobjCase=new Case(Status = 'Pending'); 
                insert InsertobjCase;

                try
                {
                      ComplaintsInvestigationFindings.getInvestigationDetails(null);
                }catch(Exception Ex)
                {
                    
                }
                ComplaintsInvestigationFindings.getInvestigationDetails(InsertDeficiency.Id);

                try
                {
                      ComplaintsInvestigationFindings.getSupervisorResponse(null);
                }catch(Exception Ex)
                {
                    
                }
                ComplaintsInvestigationFindings.getSupervisorResponse(InsertobjCase.Id); 

                try
                {
                      ComplaintsInvestigationFindings.getWorkItemId(null);
                }catch(Exception Ex)
                {
                    
                }
                   ComplaintsInvestigationFindings.getWorkItemId(InsertobjCase.Id); 
                   ComplaintsInvestigationFindings.InsertUpdateProviderDecision(InsertDeficiency);
                
                try
                {
                    ComplaintsInvestigationFindings.updateProviderApprovalProcess(UserInfo.getUserId(),'','Approve','Comments');                 
                }catch(Exception Ex)
                {
                    
                }
                try
                {
                        ComplaintsInvestigationFindings.getUploadedDocuments(null);
                }catch(Exception Ex)
                {
                    
                }                
                  ComplaintsInvestigationFindings.getUploadedDocuments(InsertobjCase.Id);
                  try
                  {
                              ComplaintsInvestigationFindings.saveSign(null,null);
                  }catch(Exception Ex)
                  {
                        
                  }                
                              ComplaintsInvestigationFindings.saveSign('test',InsertobjCase.Id);
                  try
                  {
                              ComplaintsInvestigationFindings.getLoginUserSignature(null);
                  }catch(Exception Ex)
                  {
                        
                  }                
                              ComplaintsInvestigationFindings.getLoginUserSignature(InsertobjCase.Id);                        

            }
}