public with sharing class CommonDocuments {
    public static string strClassNameForLogger='CommonDocuments';
    @AuraEnabled
    public static string saveDescription(string strDocumentId,string strDescription,string strSubCategory,string strTitle){       
        string strResultStatus;
        try {         
            Map<Id, ContentDocumentLink> contentDocumentIdsmap = new Map<Id, ContentDocumentLink>();
            contentDocumentIdsmap=getMapOfContentDocumentIDFromDocumentId(strDocumentId);
            List<ContentVersion> oVersions = new List<ContentVersion>();        
            for(Id cdl: contentDocumentIdsmap.keyset()) {// Form List to update the desription
                    ContentVersion version = new ContentVersion();
                    version.id = contentDocumentIdsmap.get(cdl).ContentDocument.LatestPublishedVersionId;
                    version.description = strDescription;
                    version.CWhealth__c = strSubCategory; 
                    version.Title = strTitle;
                    oVersions.add(version);
             } 
             DMLOperationsHandler.updateOrInsertSOQLForList(oVersions);    
            strResultStatus=ProviderConstants.SUCCESS;


        } catch (Exception Ex) {
            strResultStatus=ProviderConstants.ERROR;
            String[] arguments = new String[] {strDocumentId , strDescription, strSubCategory, strTitle};
            string strInputRecord= String.format('Input parameters are :: strDocumentId -- {0} :: strDescription -- {1}  :: strSubCategory -- {2} :: strTitle -- {3}', arguments);
              CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'saveDescription',strInputRecord,Ex);
	          CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error on save description',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
	          throw new AuraHandledException(JSON.serialize(oErrorData));

        }          
          return strResultStatus;// May we I will return status
    }
   
   
    /**
         //! Important Dont alter this Method as its referenced in PublicApplicationBackgroundChecks class
    */
    public static Map<Id, ContentDocumentLink> getMapOfContentDocumentIDFromDocumentId(String strDocumentID)
    {       
     
        Map<Id, ContentDocumentLink> contentDocumentIdsmap = new Map<Id, ContentDocumentLink>();
        try{
               List<String> fieldList = strDocumentID.split(', *');
            List<ContentDocumentLink> oContentDocumetLink= new DMLOperationsHandler('ContentDocumentLink').
                                                            selectFields('Id, ContentDocument.Description, ContentDocumentId, ContentDocument.LatestPublishedVersionId').
                                                            addConditionIn('contentdocumentid ', fieldList).
                                                            run();   
                   
            for(ContentDocumentLink oContentDoc : oContentDocumetLink){
            contentDocumentIdsmap.put(oContentDoc.ContentDocumentId, oContentDoc);
            }  
             
         }
          catch (Exception Ex) {
            String[] arguments = new String[] {strDocumentID};
            string strInputRecord= String.format('Input parameters are :: strDocumentID -- {0}', arguments);
 
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getMapOfContentDocumentIDFromDocumentId',strInputRecord,Ex);
	        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error on ContentDocumentId',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
	        throw new AuraHandledException(JSON.serialize(oErrorData));
	    }	
         return contentDocumentIdsmap;
    }
   


    @AuraEnabled(cacheable=true)
    public static List<sObject> getUploadedAttachmentsUserDetails(String documentID,string sObjectToFetchData,String strFieldsToQuery) {
        
        List<sObject> oList=new List<sObject>();
        try{
            oList= new DMLOperationsHandler(sObjectToFetchData).
                            selectFields(strFieldsToQuery).
                            addConditionEq('Id ', documentID).                                      
                            run(); 
                            
       }
     catch (Exception Ex) {
         String[] arguments = new String[] {documentID , sObjectToFetchData, strFieldsToQuery};
         string strInputRecord= String.format('Input parameters are :: documentID -- {0} :: sObjectToFetchData -- {1}  :: strFieldsToQuery -- {2}', arguments);

         CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getUploadedAttachmentsUserDetails',strInputRecord,Ex);
	     CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error on Upload Atachments',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
	     throw new AuraHandledException(JSON.serialize(oErrorData));
	    }	
       return oList;      
    }

@AuraEnabled(cacheable=true)
    public static List<contentversion>  getUploadedDocuments(String strDocumentID) {
        List<contentversion> getDocuments = new List<contentversion>();
        try {
           getDocuments =  CommonUtils.getUploadedDocuments(strDocumentID);
        } catch (Exception ex) {
            //To Log into object
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getUploadedDocuments', 'Document Id' + strDocumentID, ex);
            //To return back to the UI
            CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Documents Fetch Issues', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return getDocuments;
    }
     
}