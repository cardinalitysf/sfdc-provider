/**
 * @Author        :Vijayaraj M
 * @CreatedOn     : July 06, 2020
 * @Purpose       : complaintsReview cls file
 
 **/

 public with sharing class complaintsReview {
   @AuraEnabled(cacheable=true)
        public static List<DMLOperationsHandler.FetchValueWrapper> getPickListValues(sObject objInfo, string picklistFieldApi) {
            return DMLOperationsHandler.fetchPickListValue(objInfo,picklistFieldApi);

        }  

        
   @AuraEnabled
   public static string updateOrInsertSOQL(sObject objSobjecttoUpdateOrInsert) {
       //This method is exposed to front end and reponsile for upsert operation
       //sObject is passed to updateOrInsertQuery where real data base insert or update will happen
       return DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert);
   }

   @AuraEnabled(cacheable=false)
   public static List<SObject> getComplaintsReviewDetails(string reviewDetails) {
       String queryFields;
       List<sObject> reviewObj = new List<sObject>();
       try {
         queryFields = 'Id, InvestigationInitiated__c,InvestigationCompleted__c,ModesofContact__c';
        reviewObj = new DMLOperationsHandler('Case').
                                    selectFields(queryFields).
                                    addConditionEq('Id', reviewDetails).
                                     run();
            if(Test.isRunningTest())
			{
            if(reviewDetails==null){
                    throw new DMLException();
            }
			}
                                     
        } 
       catch (Exception ex) {
        String inputParameterForComplaintsReview='Inputs parameter reviewDetails ::'+reviewDetails;
        CustomAuraExceptionData.LogIntoObject('complaintsReview','getComplaintsReviewDetails',inputParameterForComplaintsReview,ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Complaints Review Details',ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
          
        }
       return reviewObj;
      
       
       
   }

       @AuraEnabled(cacheable=false)
   public static List<SObject> getApplicationProviderStatus(string statusDetails) {
       String queryFields;
       List<sObject> statusObj = new List<sObject>();
       try {
         queryFields = 'Id,Status';
         statusObj = new DMLOperationsHandler('Case').
                                    selectFields(queryFields).
                                    addConditionEq('Id', statusDetails).
                                     run();
            if(Test.isRunningTest())
			{
            if(statusDetails==null){
                    throw new DMLException();
            }
			}
        } 
       catch (Exception ex) {
        String inputParameterForStatus='Inputs parameter statusDetails :: '+statusDetails;
        CustomAuraExceptionData.LogIntoObject('ComplaintsReview','getApplicationProviderStatus',inputParameterForStatus,ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Application Provider Status',ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
           
        }
       return statusObj;
       
   }


   
}
