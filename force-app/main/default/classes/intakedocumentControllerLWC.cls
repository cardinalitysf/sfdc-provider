public with sharing class intakedocumentControllerLWC {
    @AuraEnabled(cacheable=true)
    public static Case fetchCase(String intakeId){
        
        Case objCase =  [Select id,CaseNumber,CreatedDate,Owner.FirstName,Owner.Lastname,Origin
                          From Case where Id =:intakeId];
        return objCase;
    }

    @AuraEnabled
    public static list<contentversion>  SaveFile(Id parentId, String fileName, String base64Data, String contentType,string category,string subCategory,string documentTitle,String description) {
         
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        ContentVersion conVer = new ContentVersion();
        conVer.Document__c    = category; 
        conVer.CWhealth__c = subCategory; 
        //conVer.Document_Title__c = documentTitle; 
        conVer.PathOnClient = fileName; 
        conVer.Description = description;
        conVer.Title = documentTitle; 
        conVer.VersionData = EncodingUtil.base64Decode(base64Data);
        insert conVer;
        
        Id conDoc = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id].ContentDocumentId;
 
        //Create ContentDocumentLink
        ContentDocumentLink cDe = new ContentDocumentLink();
        cDe.ContentDocumentId = conDoc;
        cDe.LinkedEntityId = parentId; // you can use objectId,GroupId etc
        cDe.Visibility = 'AllUsers';
        cDe.ShareType = 'I';
        insert cDe;
        
        Map<Id, ContentDocumentLink> contentDocumentIdsmap = new Map<Id, ContentDocumentLink>();
        for(ContentDocumentLink conDocu : [SELECT ContentDocumentId, LinkedEntityId,LinkedEntity.Name FROM ContentDocumentLink WHERE LinkedEntityId =:parentId LIMIT 50000]){
            contentDocumentIdsmap.put(conDocu.ContentDocumentId, conDocu);
        }
        list<contentversion> contelist = [select id,ContentDocumentId,PathOnClient,Createddate,filetype,createdBy.Name from contentversion where ContentDocumentId In: contentDocumentIdsmap.keyset() limit 50000];

        return contelist;
    }
    
    @AuraEnabled
    public static list<contentversion>  EditFile(Id parentId, Id recordId,string category,string subCategory) {
        // Category__c,Document_Title__c, Sub_Category__c
        contentversion conUpdate = [SELECT Id from contentversion where id =:recordId limit 1];
        // conUpdate.Category__c = category;
        // conUpdate.Sub_Category__c = subCategory;
        update conUpdate;
        
        Map<Id, ContentDocumentLink> contentDocumentIdsmap = new Map<Id, ContentDocumentLink>();
        for(ContentDocumentLink conDocu : [SELECT ContentDocumentId, LinkedEntityId,LinkedEntity.Name FROM ContentDocumentLink WHERE LinkedEntityId =:parentId LIMIT 50000]){
            contentDocumentIdsmap.put(conDocu.ContentDocumentId, conDocu);
        }
        list<contentversion> contelist = [select id,ContentDocumentId,PathOnClient, Createddate,filetype,createdBy.Name from contentversion where ContentDocumentId In: contentDocumentIdsmap.keyset() limit 50000];
        
        return contelist;
        
    }
    
    @AuraEnabled(cacheable=true)
    public static list<contentversion>  fetchDocuments(String intakeId) {
        Map<Id, ContentDocumentLink> contentDocumentIdsmap = new Map<Id, ContentDocumentLink>();
        for(ContentDocumentLink conDocu : [SELECT ContentDocumentId, LinkedEntityId,LinkedEntity.Name FROM ContentDocumentLink WHERE LinkedEntityId =:intakeId LIMIT 50000]){
            contentDocumentIdsmap.put(conDocu.ContentDocumentId, conDocu);
        }
        
        list<contentversion> contelist = [select id,ContentDocumentId,Description,CWhealth__c,PathOnClient,Document__c,Title, Createddate,filetype,createdBy.Name from contentversion where ContentDocumentId In: contentDocumentIdsmap.keyset() limit 50000];

        return contelist;
    }
    
     @AuraEnabled
    public static contentversion  getSelectedFileDetails(String recordId) {
       
        contentversion contentver = [select id,ContentDocumentId,PathOnClient,Description,Createddate,filetype,createdBy.Name from contentversion where id =:recordId limit 1];

        return contentver;
      }
    
    public class wrapperClass{
        @AuraEnabled  public string Name {get;set;}
        @AuraEnabled  public string AttachId {get;set;}
        @AuraEnabled  public string ContentType {get;set;}
        @AuraEnabled  public String dat {get;set;}
        @AuraEnabled  public String category {get;set;}
        @AuraEnabled  public String subCategory {get;set;}
        @AuraEnabled  public String documetTitle {get;set;}
        @AuraEnabled  public String CreatedBy {get;set;}
        @AuraEnabled  public DateTime Createddate {get;set;}
    }
    
    @AuraEnabled  
    public static list<contentversion> deleteFiles(Id parentId,string sdocumentId){ 
      //  delete [SELECT Id,Name,parentId,ContentType,Createddate,createdBy.Name FROM Attachment WHERE id=:sdocumentId];       
        delete [SELECT Id FROM ContentDocument WHERE id=:sdocumentId];       
       Map<Id, ContentDocumentLink> contentDocumentIdsmap = new Map<Id, ContentDocumentLink>();
        for(ContentDocumentLink conDocu : [SELECT ContentDocumentId, LinkedEntityId,LinkedEntity.Name FROM ContentDocumentLink WHERE LinkedEntityId =:parentId LIMIT 50000]){
            contentDocumentIdsmap.put(conDocu.ContentDocumentId, conDocu);
        }
        list<contentversion> contelist = [select id,ContentDocumentId,PathOnClient, Createddate,filetype,createdBy.Name from contentversion where ContentDocumentId In: contentDocumentIdsmap.keyset() limit 50000];

        return contelist;
    }
    
    @AuraEnabled(cacheable=true)
    public static List<String> getPicklistvalues(String objectName, String field_apiname, Boolean nullRequired){
        List<String> optionlist = new List<String>();
        
        Map<String,Schema.SObjectType> obj = Schema.getGlobalDescribe(); 
        Map<String, Schema.SObjectField> field_map = obj.get(objectName.toLowerCase()).getDescribe().fields.getMap(); 
        
        List<Schema.PicklistEntry> picklistValues = field_map.get(field_apiname).getDescribe().getPickListValues();
        
        if(nullRequired == true){
            optionlist.add('--None--');
        }
        
        for (Schema.PicklistEntry pv : picklistValues) {
            optionlist.add(pv.getValue());
        }
        
        return optionlist;
    }
}