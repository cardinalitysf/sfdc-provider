/**
 * @Author        : K.Sundar
 * @CreatedOn     : March 17 ,2020
 * @Purpose       : Responsible for fetch or insert the Address Object.
**/

public with sharing class providerContactDetails {

    @AuraEnabled(cacheable = true)
    public static List<DMLOperationsHandler.FetchValueWrapper> getAddressTypeValues ( sObject objInfo, string pickListFieldApi ) {
        return DMLOperationsHandler.fetchPickListValue( objInfo, pickListFieldApi );
    }

    @AuraEnabled(cacheable = false)
    public static List<Address__c> getAllContactDetails (String providerId) {
       
        List<Address__c> lstContactAddress = new DMLOperationsHandler('Address__c').
                                        selectFields('Id, AddressType__c, AddressLine1__c, AddressLine2__c, County__c, City__c, State__c, ZipCode__c, Email__c, Phone__c').
                                        addConditionEq('Provider__c',providerId).
                                        addConditionEq('Application__c','').
                                        orderBy('Id','Desc').                                   
                                        run();
         return lstContactAddress;
    }

    @AuraEnabled
    public static List<SObject> editContactDetails (String selectedAddressId) {   
        List<Address__c> lstEditContactAddress = new DMLOperationsHandler('Address__c').
                                            selectFields('Id, AddressType__c, AddressLine1__c, AddressLine2__c, County__c, City__c, State__c, ZipCode__c, Email__c, Phone__c').
                                            addConditionEq('Id',selectedAddressId).              
                                            run();
        return lstEditContactAddress;
    }

    @AuraEnabled
    public static string insertContactDetails(sObject objSobjecttoUpdateOrInsert){
        return DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert);
    }

    @AuraEnabled(cacheable=true)
    public static List<SObject> getStateDetails() {       

        List<ReferenceValue__c> lstStateDetails = new DMLOperationsHandler('ReferenceValue__c').
                                                selectFields('Id, RefValue__c').
                                                addConditionEq('Domain__c','State').              
                                                run();
        return lstStateDetails;
    }
}