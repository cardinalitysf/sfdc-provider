/**
     * @Author        : Vijayaraj M
     * @CreatedOn     : June 25,2020
     * @Purpose       :Test Methods to unit test for PublicProvidersLicenseInfo.cls
     * @Updated by     : Naveen S
     * @updated on    : July 22,2020
     **/
 @isTest
private with sharing class PublicProvidersLicenseInfo_Test {
    @isTest static void testPublicProvidersLicenseInfo_Test() {
        Application__c InsertobjNewApplication=new Application__c(ProgramType__c = 'XYZ'); 
        insert InsertobjNewApplication;
        Account InsertobjNewAccount=new Account(Name = 'Test Account'); 
        insert InsertobjNewAccount;
        Actor__c InsertobjNewActor=new Actor__c(Role__c = 'XYZ'); 
        insert InsertobjNewActor;
        Address__c InsertobjNewAddress=new Address__c(AddressLine1__c = 'XYZ'); 
        insert InsertobjNewAddress;
        HomeInformation__c InsertobjNewHomeInformation=new HomeInformation__c(SwimmingPool__c = 'XYZ'); 
        insert InsertobjNewHomeInformation;
      
        try
        {
            PublicProvidersLicenseInfo.getApplicationId(null);
        }
        catch(Exception Ex)
        {
            
        }  
        PublicProvidersLicenseInfo.getApplicationId(InsertobjNewAccount.Id);
        try
        {
            PublicProvidersLicenseInfo.getHouseholdMembers(null);
        }
        catch(Exception Ex)
        {
            
        }  
        PublicProvidersLicenseInfo.getHouseholdMembers(InsertobjNewAccount.Id);
        
        
        try
        {
            PublicProvidersLicenseInfo.getCoApplicantDetails(null);
        }
        catch(Exception Ex)
        {
            
        }  
        PublicProvidersLicenseInfo.getCoApplicantDetails(InsertobjNewAccount.Id);
        try
        {
            PublicProvidersLicenseInfo.getLicenseInformation(null);
        }
        catch(Exception Ex)
        {
            
        }  
        PublicProvidersLicenseInfo.getLicenseInformation(InsertobjNewApplication.Id);
        try
        {
            PublicProvidersLicenseInfo.fetchDataForAddress(null,null);
        }
        catch(Exception Ex)
        {
            
        }  
        PublicProvidersLicenseInfo.fetchDataForAddress(InsertobjNewAccount.Id,'HGDF');
        try
        {
            PublicProvidersLicenseInfo.fetchHomeInformation(null);
        }
        catch(Exception Ex)
        {
            
        }  
        PublicProvidersLicenseInfo.fetchHomeInformation(InsertobjNewAccount.Id);
        try
        {
            PublicProvidersLicenseInfo.getContactImage(null);
        }
        catch(Exception Ex)
        {
            
        }  
        PublicProvidersLicenseInfo.getContactImage(InsertobjNewAccount.Id);

    }
}
