/**
     * @Author        : Sindhu Venkateswarlu
     * @CreatedOn     : June 6,2020
     * @Purpose       :Test Methods to unit test PublicApplicationHomeStudyVisit class in PublicApplicationHomeStudyVisit.cls
     **/

    @isTest
    
    public with sharing class PublicApplicationHomeStudyVisit_Test {
        @isTest static void testgetPersons (){
            Actor__c InsertobjNewActor=new Actor__c(Role__c='Applicant'); 
            insert InsertobjNewActor;

            Account InsertobjNewAccount=new Account(Name = 'test Account'); 
            insert InsertobjNewAccount;

            ActorDetails__c actdet = new ActorDetails__c();
            insert actdet;
            Application__c app = new Application__c();
            insert app;

            try
            {
                PublicApplicationHomeStudyVisit.getPersons(null);
            }
            catch(Exception Ex)
            {
            }
            PublicApplicationHomeStudyVisit.getPersons(InsertobjNewAccount.Id);
            string str = '[{"Actor__c":"'+InsertobjNewActor.Id+'","MeetingInfo__c":"'+app.Id+'","SessionStatus__c":"Scheduled","Application__c":"'+app.Id+'","TotalHoursAttended__c":"27"}]';
            
            PublicApplicationHomeStudyVisit.InsertUpdateHomeStudyInfo(str);
            try
            {
                PublicApplicationHomeStudyVisit.getHomeStudyDetails(null);
            }
            catch(Exception Ex)
            {
            }
            PublicApplicationHomeStudyVisit.getHomeStudyDetails(app.Id);
            try
            {
                PublicApplicationHomeStudyVisit.editHomeStudyDetails(null);
            }
            catch(Exception Ex)
            {
            }
            PublicApplicationHomeStudyVisit.editHomeStudyDetails('');
        }
}
