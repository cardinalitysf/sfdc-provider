/**
 * @Author        : K.Sundar
 * @CreatedOn     : AUGUST 04, 2020
 * @Purpose       : This component contains Clearance/Medical CPA Home Household Member Details
 **/

public with sharing class CpaClearanceMedicalInfo {
    public static string strClassNameForLogger = 'CpaClearanceMedicalInfo';

    //Get Actor Details against CPA Home ID
    @AuraEnabled(cacheable=true)
    public static List<sObject> getCPAHomeHouseHoldMembersDetails(String cpaHomeId) {
        List<sObject> cpaHomeMemberObj = new List<sObject>();

        String queryFields = 'Id, Name, Role__c, Contact__r.Id, Contact__r.FirstName__c, Contact__r.LastName__c, Contact__r.ApproximateAge__c, Contact__r.Gender__c, CPAHomes__c, CPAHomes__r.ProgramType__c, ClearanceStatus__c';

        try {
            cpaHomeMemberObj = new DMLOperationsHandler('Actor__c')
                                .selectFields(queryFields)
                                .addConditionEq('CPAHomes__c', cpaHomeId)
                                .orderBy('LastModifiedDate', 'desc')
                                .run();
        } catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getCPAHomeHouseHoldMembersDetails', cpaHomeId, Ex);
        
            //To return back to the UI
            CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('CPA Household Member Details', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());

            throw new AuraHandledException(JSON.serialize(oErrorData));
        }

        return cpaHomeMemberObj;
    }

    //Fetch members record data based on CPA Home ID
    @AuraEnabled(cacheable=false)
    public static List<providerRecordQuestionAnswerWrapper> getCPARecordDetails(String cpaHomeId, String stractorId) {
   
        String questionQueryFields = 'Id, Name, RecordTypeId, Actor__c, Status__c, CPAHomes__c';

        String answerQueryFields = 'Id, Name, ProviderRecordQuestion__c, Actor__c, AsPerStateRegulations__c, IsResultsinRecord__c, RequestDate__c, ResultDate__c, Comar__c';

        String referenceValueFields = 'Id, Name, RefKey__c, RecordTypeId, Question__c, Type__c, SubQuestion1__c, SubQuestion2__c, QuestionOrder__c';

        Id questionId;
        Map<id,ReferenceValue__c> oRefValue=new Map<id,ReferenceValue__c>();
        List<ReferenceValue__c> referenceValueObj = new List<ReferenceValue__c>();
        List<ProviderRecordQuestion__c> providerQuestionObj = new List<ProviderRecordQuestion__c>();
        List<ProviderRecordAnswer__c> providerAnswerObj = new List<ProviderRecordAnswer__c>();
        List<providerRecordQuestionAnswerWrapper> questionAnswerWrapper = new List<providerRecordQuestionAnswerWrapper>();

        try {
            providerQuestionObj = new DMLOperationsHandler('ProviderRecordQuestion__c')
                                    .selectFields(questionQueryFields)
                                    .addConditionEq('CPAHomes__c', cpaHomeId)
                                    .addConditionEq('Actor__c', stractorId)
                                    .run();

            referenceValueObj = new DMLOperationsHandler('ReferenceValue__c')
                                .selectFields(referenceValueFields)
                                .addConditionEq('Type__c', 'Clearance')
                                .orderBy('RefKey__c', 'asc')
                                .orderBy('QuestionOrder__c', 'asc')
                                .run();

            if (providerQuestionObj.size() > 0) {
                questionId = providerQuestionObj[0].Id;

                Map<Id, ProviderRecordAnswer__c> oMap = new Map<Id, ProviderRecordAnswer__c>();

                providerAnswerObj = new DMLOperationsHandler('ProviderRecordAnswer__c')
                                    .selectFields(answerQueryFields)
                                    .addConditionEq('ProviderRecordQuestion__c', questionId)
                                    .addConditionEq('Actor__c', stractorId)
                                    .run();

                for (ProviderRecordAnswer__c recordObj : providerAnswerObj) {
                    oMap.put(recordObj.Comar__c, recordObj);
                }
             
                for (ReferenceValue__c objRef : referenceValueObj) {
                    oRefValue.put(oMap.get(objRef.Id).Id,objRef);
                }

                Map<Id,contentversion[]> getDocuments =  CommonUtils.getUploadedDocumentsByLinkedEntityID(oRefValue.Keyset());  

                for (ReferenceValue__c objRef : referenceValueObj) {
                    providerRecordQuestionAnswerWrapper oWrapper = new providerRecordQuestionAnswerWrapper(
                       objRef.RefKey__c,
                       objRef.Question__c,
                       objRef.SubQuestion1__c,
                       objRef.SubQuestion2__c,
                       objRef.QuestionOrder__c,
                       oMap.get(objRef.Id).Id,
                       oMap.get(objRef.Id).Actor__c,
                       oMap.get(objRef.Id).Comar__c,
                       oMap.get(objRef.Id).ProviderRecordQuestion__c,
                       oMap.get(objRef.Id).RequestDate__c,
                       oMap.get(objRef.Id).ResultDate__c,
                       oMap.get(objRef.Id).AsPerStateRegulations__c,
                       oMap.get(objRef.Id).IsResultsinRecord__c,
                       getDocuments.get(oMap.get(objRef.Id).Id)
                    );

                    questionAnswerWrapper.add(oWrapper);
                }
                
            } else {
                ProviderRecordQuestion__c questionObj = new ProviderRecordQuestion__c();
                questionObj.CPAHomes__c = cpaHomeId;
                questionObj.Actor__c = stractorId;

                //Insert query method for Question Object and will return Inserted ID
                questionId = DMLOperationsHandler.getIdAfterInsertSOQL(questionObj);

                Map<Id, ProviderRecordAnswer__c> oMap = new Map<Id, ProviderRecordAnswer__c>();
                List<ProviderRecordAnswer__c> lstAnsObj = new List<ProviderRecordAnswer__c>();

                for (ReferenceValue__c objRef : referenceValueObj) {
                    ProviderRecordAnswer__c objRecordAns = new ProviderRecordAnswer__c();
                    objRecordAns.IsResultsinRecord__c = '';
                    objRecordAns.AsPerStateRegulations__c = '';
                    objRecordAns.RequestDate__c = '';
                    objRecordAns.ResultDate__c = '';
                    objRecordAns.ProviderRecordQuestion__c = questionId;
                    objRecordAns.Comar__c = objRef.Id;
                    objRecordAns.Actor__c = stractorId;
                    lstAnsObj.add(objRecordAns);
                }
                insert lstAnsObj;

                providerAnswerObj = new DMLOperationsHandler('ProviderRecordAnswer__c')
                                    .selectFields(answerQueryFields)
                                    .addConditionEq('ProviderRecordQuestion__c', questionId)
                                    .addConditionEq('Actor__c', stractorId)
                                    .run();

                for (ProviderRecordAnswer__c recordObj : providerAnswerObj) {
                    oMap.put(recordObj.Comar__c, recordObj);
                }

                for (ReferenceValue__c objRef : referenceValueObj) {
                    providerRecordQuestionAnswerWrapper oWrapper = new providerRecordQuestionAnswerWrapper(
                        objRef.RefKey__c,
                        objRef.Question__c,
                        objRef.SubQuestion1__c,
                        objRef.SubQuestion2__c,
                        objRef.QuestionOrder__c,
                        oMap.get(objRef.Id).Id,
                        oMap.get(objRef.Id).Actor__c,
                        oMap.get(objRef.Id).Comar__c,
                        oMap.get(objRef.Id).ProviderRecordQuestion__c,
                        oMap.get(objRef.Id).RequestDate__c,
                        oMap.get(objRef.Id).ResultDate__c,
                        oMap.get(objRef.Id).AsPerStateRegulations__c,
                        oMap.get(objRef.Id).IsResultsinRecord__c,
                        null
                     );
                    questionAnswerWrapper.add(oWrapper);
                }
            }
        } catch (Exception Ex) {
            //To Log into object
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getCPARecordDetails', cpaHomeId, Ex);

            //To return back to the UI
            CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('CPA Household Member Details', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());

            throw new AuraHandledException(JSON.serialize(oErrorData));
        }

        return questionAnswerWrapper;
    }

    @AuraEnabled
    public static void bulkUpdateRecordAns(String datas, String actorId){
        try{
            List<ProviderRecordAnswer__c> dataToInsert = (List<ProviderRecordAnswer__c>)System.JSON.deserialize(datas, List<ProviderRecordAnswer__c>.class);
            List<ProviderRecordAnswer__c> listInsert = new List<ProviderRecordAnswer__c>();

            for(ProviderRecordAnswer__c p: dataToInsert){
                ProviderRecordAnswer__c ansObj = new ProviderRecordAnswer__c();
                ansObj.Id = p.Id;
                ansObj.ProviderRecordQuestion__c = p.ProviderRecordQuestion__c;
                ansObj.RequestDate__c = p.RequestDate__c;
                ansObj.ResultDate__c = p.ResultDate__c;
                ansObj.AsPerStateRegulations__c = p.AsPerStateRegulations__c;
                ansObj.IsResultsinRecord__c = p.IsResultsinRecord__c;
                listInsert.add(ansObj);
            }

            String status = DMLOperationsHandler.updateOrInsertSOQLForListWithStatus(listInsert);

            if (status == ProviderConstants.INSERTSUCCESS) {
                Actor__c actorObj = new Actor__c();
                actorObj.Id = actorId;
                actorObj.ClearanceStatus__c = 'Completed';

                DMLOperationsHandler.updateOrInsertSOQL(actorObj);
            }
        } catch (Exception Ex) {
            String[] arguments = new String[] {datas};
            string strInputRecord = String.format('Input parameters are :: datas -- {0} ::  ', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'bulk insert Clearance/Medical', strInputRecord, Ex);

            CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('bulk insert Clearance/Medical', Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());

            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
    }

    public class providerRecordQuestionAnswerWrapper {
        @AuraEnabled public string questionHeading {get;set;}
        @AuraEnabled public string questionType{get;set;}
        @AuraEnabled public string subQuestion1{get;set;}
        @AuraEnabled public string subQuestion2{get;set;}
        @AuraEnabled public decimal questionOrder{get;set;}
        @AuraEnabled public string providerAnswerId{get;set;}
        @AuraEnabled public string actorId{get;set;}
        @AuraEnabled public string referenceValueObjId{get;set;}
        @AuraEnabled public string providerRecordQuestionId{get;set;}
        @AuraEnabled public string requestDate{get;set;}
        @AuraEnabled public string resultDate{get;set;}
        @AuraEnabled public string subQuestion1Answer{get;set;}
        @AuraEnabled public string subQuestion2Answer{get;set;}
        @AuraEnabled public contentversion[] uploadedDocs {get;set;}       

        public providerRecordQuestionAnswerWrapper(string questionHeading,string questionType,string subQuestion1,string subQuestion2,decimal questionOrder,string providerAnswerId,string actorId,string referenceValueObjId, string providerRecordQuestionId,string requestDate,string resultDate,string subQuestion1Answer,string subQuestion2Answer,contentversion[] uploadedDocs){
            this.questionHeading=questionHeading;
            this.questionType=questionType;
            this.subQuestion1=subQuestion1;
            this.subQuestion2=subQuestion2;
            this.questionOrder=questionOrder;
            this.providerAnswerId=providerAnswerId;
            this.actorId=actorId;
            this.referenceValueObjId=referenceValueObjId;
            this.providerRecordQuestionId=providerRecordQuestionId;
            this.requestDate=requestDate;
            this.resultDate=resultDate;
            this.subQuestion1Answer=subQuestion1Answer;
            this.subQuestion2Answer=subQuestion2Answer;
            this.uploadedDocs=uploadedDocs;
        }
    }
}