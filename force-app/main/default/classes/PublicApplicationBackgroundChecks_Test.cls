  /**
	 * @Author        : Balamurugan B
	 * @CreatedOn     : June 29 ,2020
	 * @Purpose       : Test Class for PublicApplicationBackgroundChecks.cls
   * @Update         : Naveen S
**/

@IsTest
private with sharing class PublicApplicationBackgroundChecks_Test {
    @IsTest static void testPublicAppBackGrounChecks(){

        Account InsertobjNewAccount=new Account(Name = 'Test Account'); 
        insert InsertobjNewAccount;
        Application__c InsertobjNewApplication=new Application__c(Status__c = 'Pending'); 
        insert InsertobjNewApplication;
        Id MyId2=Schema.Sobjecttype.ProviderRecordQuestion__c.getRecordTypeInfosByName().get('Criminal History').getRecordTypeId();
        Id MyId3=Schema.Sobjecttype.ProviderRecordQuestion__c.getRecordTypeInfosByName().get('Personal Security').getRecordTypeId();

        ProviderRecordQuestion__c   InsertobjInProviderRecordQues =new ProviderRecordQuestion__c(RecordTypeId=MyId2,Application__c=InsertobjNewApplication.Id);
        insert InsertobjInProviderRecordQues;
        ProviderRecordQuestion__c   InsertobjInProviderRecordQues1 =new ProviderRecordQuestion__c(RecordTypeId=MyId3,Application__c=InsertobjNewApplication.Id);
        insert InsertobjInProviderRecordQues1;
        ProviderRecordAnswer__c   InsertobjInProviderRecordAnswer =new ProviderRecordAnswer__c();
        insert InsertobjInProviderRecordAnswer;
        ReferenceValue__c InsertobjInReference =new ReferenceValue__c();
        insert InsertobjInReference;
        Actor__c Act=new Actor__c();
        insert Act;
        string bulkobject = '[{"id":"'+InsertobjInProviderRecordAnswer.Id+'","Date__c":"2020-06-10","Comments__c":"'+InsertobjInProviderRecordAnswer.Comments__c+'","ProviderRecordQuestion__c":"'+InsertobjInProviderRecordAnswer.ProviderRecordQuestion__c+'","HouseholdStatus__c":"'+InsertobjInProviderRecordAnswer.HouseholdStatus__c+'","Dependent__c":"'+InsertobjInProviderRecordAnswer.Dependent__c+'", "State__c":"'+InsertobjInProviderRecordAnswer.State__c+'","Comar__c":"'+InsertobjInProviderRecordAnswer.Comar__c+'"}]';
        string quesId = InsertobjInProviderRecordAnswer.ProviderRecordQuestion__c;

        try
        {
          PublicApplicationBackgroundChecks.getHouseholdMembers(null);        
        }
        catch(Exception Ex)
        {
            
        }
        PublicApplicationBackgroundChecks.getHouseholdMembers(InsertobjNewAccount.Id);    
        
        try
        {
          PublicApplicationBackgroundChecks.getQuestionsByType(null);        
        }
        catch(Exception Ex)
        {
            
        }
        PublicApplicationBackgroundChecks.getQuestionsByType('');

        try
        {
          PublicApplicationBackgroundChecks.bulkAddRecordAns(null, null);      
        }
        catch(Exception Ex)
        {
            
        }
        PublicApplicationBackgroundChecks.bulkAddRecordAns(bulkobject, quesId);   
        
        try
        {
          PublicApplicationBackgroundChecks.bulkUpdateRecordAns(null);      
        }
        catch(Exception Ex)
        {
            
        }
        PublicApplicationBackgroundChecks.bulkUpdateRecordAns(bulkobject);

       
        PublicApplicationBackgroundChecks.getRecordType ('Personal Security');
        try
        {
          PublicApplicationBackgroundChecks.editRecordDet (null);    
        }
        catch(Exception Ex)
        {
            
        }     
        PublicApplicationBackgroundChecks.editRecordDet (InsertobjNewApplication.Id);      
               
                
        PublicApplicationBackgroundChecks.getUploadedDocuments('');
        PublicApplicationBackgroundChecks.updateActorFieldInContentversion(InsertobjNewApplication.Id,Act.Id);
        PublicApplicationBackgroundChecks.getRecordDetails(InsertobjNewApplication.Id);

        try {
        List<DMLOperationsHandler.FetchValueWrapper> objgetPickListValues = PublicApplicationBackgroundChecks.getPickListValues(InsertobjNewApplication,'Pending');
        } 
        catch (Exception e)
        {
            
        }

        }
}
