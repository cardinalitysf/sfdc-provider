/**
     * @Author        : Vijayaraj M
     * @CreatedOn     : May 12,2020
     * @Purpose       :Test Methods to unit test for ProviderDecisionController.cls
     **/
    @IsTest
     private with sharing class ProviderDecisionController_Test {
        @isTest static void  ProviderDecisionControllerTest() {

            Application__c InsertobjNewProviderDecision=new Application__c(ProgramType__c = 'XYZ'); 
            insert InsertobjNewProviderDecision;
           try
           {
            ProviderDecisionController.getReviewDetails(null);
            }
            catch(Exception Ex){

            }
            ProviderDecisionController.getReviewDetails(InsertobjNewProviderDecision.Id);

            try
            {
               ProviderDecisionController.getProgramInformation(null);
            }
            catch(Exception Ex){

            }
            ProviderDecisionController.getProgramInformation(InsertobjNewProviderDecision.Id);


            ProviderDecisionController.updateapplicationdetails(InsertobjNewProviderDecision);


            try
            {
                // Testing getPickListValues method getPickListValues
                List<DMLOperationsHandler.FetchValueWrapper>  objgetPickListValues = ProviderDecisionController.getPickListValues(InsertobjNewProviderDecision,'XXX');
            }
            catch(Exception e)
            {
            }

          }
    }