/**
 * @Author        : Preethi Bojarajan
 * @CreatedOn     : Feb 25,2020
 * @Purpose       : Referral Dashboard with Pending, Approved and Rejected
 * @updatedBy     : Preethi Bojarajan
 * @updatedOn     : May 01,2020
 **/

public with sharing class referalTable {
    @AuraEnabled(cacheable=true)
    public static List<Case> getReferalList(String searchKey, String type) {
        String model = 'Case';
        String cond = '';
        String privateObj = 'Private';

        if(type != 'total'){
            cond = 'Status = \''+ type +'\'';
        } else {
            type = 'Draft\''+','+'\'Rejected\''+','+'\'Approved';
            cond = 'Status in(\''+ type +'\')';
        }
        if (searchKey != null && searchKey != '' ) 
            cond += ' AND CaseNumber LIKE \'%'+  searchKey +'%\''+' ORDER BY LastModifiedDate DESC ';
        else 
            cond = cond += 'AND Account.ProviderType__c = \''+ privateObj +'\'' + ' ORDER BY LastModifiedDate DESC'; 
        //searchKey = '%' + searchKey + '%';
        String fields = 'Id, CaseNumber, Account.ProviderType__c, Program__c, Account.Name, Account.SON__c, Account.RFP__c, Status, Account.Id';
        /* List<SObject> referalList = new DMLOperationsHandler(model).
                                    selectFields(fields).
                                    addConditionEq('Status', type).
                                    //addConditionLike('CaseNumber', searchKey).
                                    orderBy('CaseNumber', 'DESC').
                                    run();
        return referalList; */
        return DMLOperationsHandler.selectSOQLWithConditionParameters(model,fields,cond);
    }

    @AuraEnabled(cacheable=true)
    public static List<WrapperCasecount> getReferalCount(String searchKey) {
        List<WrapperCasecount> lstWrapper = new List<WrapperCasecount>();
        String key = '%' + searchKey + '%';
        Integer countDraft = 0;
        Integer countApproved = 0;
        Integer countRejected = 0;
        List<AggregateResult> results =  new DMLOperationsHandler('Case'). 
                                 selectFields('Status').
                                 addConditionLike('CaseNumber', key).
                                 addConditionEq('Account.ProviderType__c', 'Private').
                                 count('Id').   
                                 groupBy('Status').
                                 run();
            for(AggregateResult result : results) {
                if(result.get('Status') == 'Draft')
                countDraft = (Integer) result.get('expr0');
                else if(result.get('Status') == 'Approved')
                countApproved = (Integer) result.get('expr0');
                else if(result.get('Status') == 'Rejected')
                countRejected = (Integer) result.get('expr0');
            }
        lstWrapper.add(new Wrappercasecount(countDraft, countApproved, countRejected)); 
        return lstWrapper;
    }

         // Wrapper Class
    public class Wrappercasecount {
        @AuraEnabled public Integer draft;
        @AuraEnabled public Integer approved;
	    @AuraEnabled public Integer rejected;
        @AuraEnabled public Integer total;
    public Wrappercasecount(Integer countDraft, Integer countApproved, Integer countRejected) {
        this.draft = countDraft;
        this.approved = countApproved;
        this.rejected = countRejected;
        this.total = countDraft + countApproved + countRejected;
    }
} 
}