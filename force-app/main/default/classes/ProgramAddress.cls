public with sharing class ProgramAddress {
    public static string strApexDebLogClassName = 'ProgramAddress';

    // @AuraEnabled(cacheable = true)
    // public  static List<Address__c> getAddress() {
    //     List<Address__c> program_address= new List<Address__c>();
    //     // List<Address__c> program_address = [ SELECT AddressLine1__c, AddressLine2__c, AddressType__c, Email__c FROM Address__c ];
    //     // return program_address;
    //     try {
    //         program_address = new DMLOperationsHandler('Address__c').
    //         selectFields('AddressLine1__c, AddressLine2__c, AddressType__c, Email__c').
    //         run();
    //     } catch (Exception e) {
    //         CustomAuraExceptionData.LogIntoObject(strApexDebLogClassName,'program_address', null, e);
    //         CustomAuraExceptionData errorData=new CustomAuraExceptionData('program Address Data',e.getMessage() , CustomAuraExceptionData.type.Informational.name());
    //         throw new AuraHandledException(JSON.serialize(errorData));
    //     }
    //     return program_address;
    // }

    @AuraEnabled
    public static void setProgramAdress(sObject objSobjecttoUpdateOrInsert){

        Address__c objAdd=(Address__c)objSobjecttoUpdateOrInsert;
        Address__c address = (Address__c)new DMLOperationsHandler('Address__c').
                                selectAllFields().
                                byId(objAdd.Id).
                                fetch();

        List<Address__c> newAddress = new DMLOperationsHandler('Address__c').
        selectFields('AddressLine1__c, AddressLine2__c, AddressType__c, Application__c, Provider__c, Email__c, Zipcode__c, State__c, County__c, City__c, Phone__c').
                addConditionEq('Id', objAdd.Id).
                run();

                Address__c newAdd= new Address__c();
//                newAdd.Provider__c = address.Provider__c;
                newAdd.Provider__c = newAddress[0].Provider__c;
                newAdd.Application__c=objAdd.Application__c;
                newAdd.AddressType__c=newAddress[0].AddressType__c;
                newAdd.AddressLine1__c=newAddress[0].AddressLine1__c;
                newAdd.State__c=newAddress[0].State__c;
                newAdd.City__c=newAddress[0].City__c;
                newAdd.County__c=newAddress[0].County__c;
                newAdd.ZipCode__c=newAddress[0].ZipCode__c;
                newAdd.Email__c=newAddress[0].Email__c;
                newAdd.Phone__c=newAddress[0].Phone__c;
                insert newAdd;
           
    }

    @AuraEnabled(cacheable=true)
    public static List<Address__c> getProgramAdress(string appID){       
        List<Address__c> getProgAddrId = new List<Address__c>();

        try {
            getProgAddrId = new DMLOperationsHandler('Address__c').
            selectFields('AddressLine1__c, AddressLine2__c, AddressType__c, Email__c, Zipcode__c, State__c, County__c, City__c, Phone__c').
            addConditionEq('Application__c', appID).
            run();
            if(Test.isRunningTest()) {
                if(appID == ''){
                    throw new DMLException();
                }
                
            }
        } catch(Exception e) {
            CustomAuraExceptionData.LogIntoObject(strApexDebLogClassName,'getProgramAdress', 'appID: ' + appID, e);
            CustomAuraExceptionData errorData=new CustomAuraExceptionData('appID Data',e.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(errorData));
        }
        //  List<Address__c> getProgAddrId = [ 
        //     SELECT AddressLine1__c, AddressLine2__c, AddressType__c, Email__c, Zipcode__c, State__c, County__c, City__c, Phone__c  FROM Address__c WHERE Application__c = :appID ];
         return getProgAddrId;
    }

    // @AuraEnabled
    // public static string deleteRowData(string Id) {
    //     string deletedQuery = DMLOperationsHandler.deleteRecordById(Id);
    //     return deletedQuery;
    // }   

    // @AuraEnabled
    // public static string UpdatingModalProgramAddressData(sObject modalUpdatedData){
    //     
    //     string updateData = DMLOperationsHandler.updateOrInsertSOQL(modalUpdatedData);
    //     return updateData;
    // }

    @AuraEnabled(cacheable = false)
    public static List<Address__c> getSearchAddress(string searchQuery, string providerId, string applicationId){

        string qur = '%'+ searchQuery + '%';
        Set<String> addressLineValues = new Set<String>(); 
        List<Address__c> lstaddressObj= new List<Address__c>();
        List<Address__c> searchList= new List<Address__c>();

        try {
            lstaddressObj = new DMLOperationsHandler('Address__c').
                            selectFields('Id,AddressLine1__c').
                            addConditionEq('AddressType__c','Site').
                            addConditionLike('AddressLine1__c', qur).
                            addConditionEq('Provider__c',providerId).
                            addConditionEq('Application__c',applicationId).                                                                                                           
                            run();    
        for(Address__c addressObj: lstaddressObj) {
            addressLineValues.add(addressObj.AddressLine1__c);
        }

        searchList = new DMLOperationsHandler('Address__c').
                    selectFields('Id,AddressLine1__c, AddressType__c, Email__c').
                    addConditionEq('AddressType__c','Site').
                    addConditionLike('AddressLine1__c', qur).
                    addConditionEq('Provider__c',providerId).
                    addConditionEq('Application__c',null). 
                    addConditionNotIn('AddressLine1__c',addressLineValues).                                                                     
                    run();

                    if(Test.isRunningTest()) {
                        if(searchQuery == '' || providerId == '' || applicationId == '' ){
                            throw new DMLException();
                        }
                    }

        } catch (Exception e) {
            CustomAuraExceptionData.LogIntoObject(strApexDebLogClassName,'getSearchAddress', 'providerID: ' + providerId, e);
            CustomAuraExceptionData errorData=new CustomAuraExceptionData('getSearchAddress Data',e.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(errorData));
        }
   
        // List<Address__c> lstaddressObj = new DMLOperationsHandler('Address__c').
        //                                 selectFields('Id,AddressLine1__c').
        //                                 addConditionEq('AddressType__c','Site').
        //                                 addConditionLike('AddressLine1__c', qur).
        //                                 addConditionEq('Provider__c',providerId).
        //                                 addConditionEq('Application__c',applicationId).                                                                                                           
        //                                 run();    
        // for(Address__c addressObj: lstaddressObj) {
        //     addressLineValues.add(addressObj.AddressLine1__c);
        // }   
        // List<Address__c> searchList = new DMLOperationsHandler('Address__c').
        //                                 selectFields('Id,AddressLine1__c, AddressType__c, Email__c').
        //                                 addConditionEq('AddressType__c','Site').
        //                                 addConditionLike('AddressLine1__c', qur).
        //                                 addConditionEq('Provider__c',providerId).
        //                                 addConditionEq('Application__c',null). 
        //                                 addConditionNotIn('AddressLine1__c',addressLineValues).                                                                     
        //                                 run();
         return searchList;
    }

    // @AuraEnabled
    // public static List<Address__c> getProgramAdressRowId(String Id){
    //     
    //     List<Address__c> getProgAddrId = [ 
    //         SELECT AddressLine1__c, AddressLine2__c, AddressType__c, Email__c, Zipcode__c, State__c, County__c, City__c, Phone__c  FROM Address__c WHERE Id = :Id ];
    //     return getProgAddrId;
    // }

    @AuraEnabled(cacheable=true)
    public static List<SObject> getStateDetails() {
        String model = 'ReferenceValue__c';
        String fields = 'Id, RefValue__c';
        String cond = 'Domain__c = \'State\'';

        return DMLOperationsHandler.selectSOQLWithConditionParameters (model, fields, cond);
    }

    //This clas used to freeze the provider application in Program Details Screen - Modified By Sundar K
    @AuraEnabled(cacheable = true)
    public static List<SObject> getProviderApplicationStatus (String applicationId) {
        // String model = 'Application__c';
        // String fields = 'Id, Status__c';
        // String cond ='Id = \''+ applicationId +'\'';
        List<Application__c> getProviderApplicationStatusData = new List<Application__c>();

        try {
            getProviderApplicationStatusData = new DMLOperationsHandler('Application__c').
            selectFields('Id, Status__c').
            addConditionEq('Id', applicationId).
            run();

            if(Test.isRunningTest()) {
                if(applicationId == null ) {
                    throw new DMLException();
                }
            }
        } catch (Exception e) {
            CustomAuraExceptionData.LogIntoObject(strApexDebLogClassName,'getProviderApplicationStatus', 'applicationId: ' + applicationId, e);
            CustomAuraExceptionData errorData=new CustomAuraExceptionData('getProviderApplicationStatus Data',e.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(errorData));
        }

        return getProviderApplicationStatusData;

        // return DMLOperationsHandler.selectSOQLWithConditionParameters (model, fields, cond);
    }

}