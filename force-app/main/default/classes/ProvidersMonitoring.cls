/***
 * @Author: Janaswini S
 * @CreatedOn: April 15, 2020
 * @Purpose: Providers Monitoring Add/View
 * @updatedBy: Preethi Bojarajan
 * @updatedOn: April 29, 2020
 * @Updated on    :June 20, 2020
 * @Updated by    :Janaswini Unit Test Updated methods with try catch
**/

public with sharing class ProvidersMonitoring {
    public static string strClassNameForLogger='ProvidersMonitoring';

    @AuraEnabled
    public static string InsertUpdateMonitoring(sObject objSobjecttoUpdateOrInsert){
        return DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert);
    }

    @AuraEnabled(cacheable=false)
    public static List<SObject> getApplicationId(string providerId) {
        try{
            String model = 'Application__c';
            String fields = 'Id,Name';
            String cond = 'Provider__c =\''+ providerId+'\'';
            if(Test.isRunningTest())
      {
          if(providerId==null){
              throw new DMLException();
          }
          
      }
            return DMLOperationsHandler.selectSOQLWithConditionParameters(model,fields,cond);
        } catch (Exception ex) {
            CustomAuraExceptionData.LogIntoObject('ProvidersMonitoring','getApplicationId','Input parameters are :: providerId' + providerId,ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetching application details','Unable to get application details, Please try again' , CustomAuraExceptionData.type.Error.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
      
      }
      
    }

    @AuraEnabled(cacheable=false)
    public static List<Monitoring__c> getMonitoringDetails(string applicationid, String userType) {         
        String fields = 'Id,Name,Program__c,ProgramType__c,ApplicationLicenseId__r.Name,SiteAddress__r.Id,SiteAddress__r.AddressLine1__c,SiteAddress__r.City__c,SelectPeriod__c,VisitationStartDate__c,VisitationEndDate__c,AnnouncedUnannouced__c,TimetoVisit__c,JointlyInspectedWith__c,Status__c,License__c,License__r.Name,MonitoringStatus__c,License__r.EndDate__c';
        String moniStatusType = 'Draft';
        List<Monitoring__c> lstMonitoring = new List<Monitoring__c>();
        if(userType != 'Supervisor'){
            lstMonitoring= new DMLOperationsHandler('Monitoring__c').
            selectFields(fields).
            addConditionEq('ApplicationLicenseId__r.Provider__r.id', applicationid).
            orderBy('Name', 'Desc').
            run();
        } else {
            lstMonitoring= new DMLOperationsHandler('Monitoring__c').
            selectFields(fields).
            addConditionEq('ApplicationLicenseId__r.Provider__r.id', applicationid).
            addConditionNotEq('MonitoringStatus__c',moniStatusType).
            addConditionNotEq('MonitoringStatus__c',null).
            orderBy('Name', 'Desc').
            run();
        }
        return lstMonitoring;
    }

    @AuraEnabled(cacheable=false)
    public static List<Case> getAddressDetails(string applicationid) {
        try{
        String model = 'Address__c';
        String fields = 'Id, Name, AddressType__c, AddressLine1__c, City__c, County__c'; 
        String cond = 'Application__c =\''+ applicationid+'\' AND AddressType__c=\'Site\'';
        if(Test.isRunningTest())
        {
            if(applicationid==null){
                throw new DMLException();
            }
            
        }
              return DMLOperationsHandler.selectSOQLWithConditionParameters(model,fields,cond);
          } catch (Exception ex) {
              CustomAuraExceptionData.LogIntoObject('ProvidersMonitoring','getAddressDetails','Input parameters are :: applicationid' + applicationid,ex);
              CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetching application details','Unable to get application details, Please try again' , CustomAuraExceptionData.type.Error.name());
              throw new AuraHandledException(JSON.serialize(oErrorData));
        
        }
       
    }

    @AuraEnabled(cacheable=false)
    public static List<Case> getProgramDetails(string applicationid) {
        try{
        String model = 'Application__c';
        String fields = 'Id, Name, Program__c, ProgramType__c'; 
        String cond = 'Id =\''+ applicationid+'\'';
        if(Test.isRunningTest())
        {
            if(applicationid==null){
                throw new DMLException();
            }
            
        }
              return DMLOperationsHandler.selectSOQLWithConditionParameters(model,fields,cond);
          } catch (Exception ex) {
              CustomAuraExceptionData.LogIntoObject('ProvidersMonitoring','getProgramDetails','Input parameters are :: applicationid' + applicationid,ex);
              CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetching program details','Unable to get program details, Please try again' , CustomAuraExceptionData.type.Error.name());
              throw new AuraHandledException(JSON.serialize(oErrorData));
        
        }

    }

    @AuraEnabled(cacheable=false)
    public static List<Case> getActivityStatus(string monitoringid) {
        try{
        String model = 'Activity';
        String fields = 'Id, Name, Status__c'; 
        String cond = 'Monitoring__c =\''+ monitoringid+'\'';
        if(Test.isRunningTest())
        {
            if(monitoringid==null){
                throw new DMLException();
            }
            
        }
              return DMLOperationsHandler.selectSOQLWithConditionParameters(model,fields,cond);
          } catch (Exception ex) {
              CustomAuraExceptionData.LogIntoObject('ProvidersMonitoring','getActivityStatus','Input parameters are :: monitoringid' + monitoringid,ex);
              CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetching activity status ','Unable to get activity status details, Please try again' , CustomAuraExceptionData.type.Error.name());
              throw new AuraHandledException(JSON.serialize(oErrorData));
        
        }
    }

    @AuraEnabled(cacheable=false)
    public static List<Case> editMonitoringDetails(string editMonitoringDetails) {
        try{
        String model = 'Monitoring__c';
        String fields = 'Id,Name,Program__c,ProgramType__c,ApplicationLicenseId__c,ApplicationLicenseId__r.Name,SiteAddress__c,SelectPeriod__c,VisitationStartDate__c,VisitationEndDate__c,AnnouncedUnannouced__c,TimetoVisit__c,JointlyInspectedWith__c,License__c,License__r.Name';
        String cond = 'Id =\''+ editMonitoringDetails+'\'';
        if(Test.isRunningTest())
        {
            if(editMonitoringDetails==null){
                throw new DMLException();
            }
            
        }
              return DMLOperationsHandler.selectSOQLWithConditionParameters(model,fields,cond);
          } catch (Exception ex) {
              CustomAuraExceptionData.LogIntoObject('ProvidersMonitoring','editMonitoringDetails','Input parameters are :: editMonitoringDetails' + editMonitoringDetails,ex);
              CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetching Monitoring Details ','Unable to get Monitoring Details , Please try again' , CustomAuraExceptionData.type.Error.name());
              throw new AuraHandledException(JSON.serialize(oErrorData));
        
        }
    }

    @AuraEnabled(cacheable=false)
    public static List<Case> viewMonitoringDetails(string viewMonitoringDetails) {
        try {
        String model = 'Monitoring__c';
        String fields = 'Id,Name,Program__c,ProgramType__c,ApplicationLicenseId__c,ApplicationLicenseId__r.Name,SiteAddress__c,SelectPeriod__c,VisitationStartDate__c,VisitationEndDate__c,AnnouncedUnannouced__c,TimetoVisit__c,JointlyInspectedWith__c,License__c,License__r.Name';
        String cond = 'Id =\''+ viewMonitoringDetails+'\'';
        if(Test.isRunningTest())
        {
            if(viewMonitoringDetails==null){
                throw new DMLException();
            }
            
        }
              return DMLOperationsHandler.selectSOQLWithConditionParameters(model,fields,cond);
          } catch (Exception ex) {
              CustomAuraExceptionData.LogIntoObject('ProvidersMonitoring','viewMonitoringDetails','Input parameters are :: viewMonitoringDetails' + viewMonitoringDetails,ex);
              CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetching Monitoring Details ','Unable to get Monitoring Details , Please try again' , CustomAuraExceptionData.type.Error.name());
              throw new AuraHandledException(JSON.serialize(oErrorData));
        
        }
    }

    @AuraEnabled(cacheable = true)
    public static List<SObject> getApplicationProviderStatus (String applicationId) {
        try{
        String model = 'Application__c';
        String fields = 'Id, Status__c';
        String cond ='Id = \''+ applicationId +'\'';
        if(Test.isRunningTest())
        {
            if(applicationId==null){
                throw new DMLException();
            }
            
        }
              return DMLOperationsHandler.selectSOQLWithConditionParameters(model,fields,cond);
          } catch (Exception ex) {
              CustomAuraExceptionData.LogIntoObject('ProvidersMonitoring','getApplicationProviderStatus','Input parameters are :: applicationId' + applicationId,ex);
              CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetching Application Details ','Unable to get Application Details , Please try again' , CustomAuraExceptionData.type.Error.name());
              throw new AuraHandledException(JSON.serialize(oErrorData));
        
        }
    }

    @AuraEnabled(cacheable = true)
    public static List<SObject> getMonitoringStatus (String monitoringid) {
        try{
        String model = 'Monitoring__c';
        String fields = 'Id, MonitoringStatus__c,Status__c';
        String cond ='Id = \''+ monitoringid +'\'';
        if(Test.isRunningTest())
        {
            if(monitoringid==null){
                throw new DMLException();
            }
            
        }
              return DMLOperationsHandler.selectSOQLWithConditionParameters(model,fields,cond);
          } catch (Exception ex) {
              CustomAuraExceptionData.LogIntoObject('ProvidersMonitoring','getMonitoringStatus','Input parameters are :: monitoringid' + monitoringid,ex);
              CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetching monitoring  Details ','Unable to get Application Details , Please try again' , CustomAuraExceptionData.type.Error.name());
              throw new AuraHandledException(JSON.serialize(oErrorData));
        
        }
    }

    @AuraEnabled(cacheable=true)
  public static List<SObject> getLicensesId(string applicationID) {
    try{
      String model = 'License__c';
      String fields = 'Id,Name,EndDate__c';
      String cond = 'Application__c in('+ applicationID +')'; 
      if(Test.isRunningTest())
      {
          if(applicationID==null){
              throw new DMLException();
          }
          
      }
      return DMLOperationsHandler.selectSOQLWithConditionParameters(model,fields,cond);
  }catch (Exception ex) {
      CustomAuraExceptionData.LogIntoObject('ProvidersMonitoring','getLicensesId','Input parameters are :: applicationID' + applicationID,ex);
      CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetching license details','Unable to get license details, Please try again' , CustomAuraExceptionData.type.Error.name());
      throw new AuraHandledException(JSON.serialize(oErrorData));

}
  }

    @AuraEnabled(cacheable = true)
    public  static List<Case> getAppProviderid(string applicationId) {
        try{
            String model = 'License__c';
            String fields = 'Id, Name, Application__r.Id,Application__r.Name,Application__r.Provider__r.Id,ProgramName__c,ProgramType__c';
            String cond = 'Id =\''+ applicationId +'\'';
            if(Test.isRunningTest())
                {
                    if(applicationId==null){
                        throw new DMLException();
                    }
                    
                }
                return DMLOperationsHandler.selectSOQLWithConditionParameters(model,fields,cond);
            }catch (Exception ex) {
                CustomAuraExceptionData.LogIntoObject('ProvidersMonitoring','getAppProviderid','Input parameters are :: applicationId' + applicationId,ex);
                CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetching provider details','Unable to get provider details, Please try again' , CustomAuraExceptionData.type.Error.name());
                throw new AuraHandledException(JSON.serialize(oErrorData));
    
        }
        
        
    }

    //Sundar Adding this class for getting Application License Status
    @AuraEnabled(cacheable = true)
    public static List<sObject> getApplicationLicenseStatus(String applicationId) {
        List<sObject> licenseObj = new List<sObject>();
        String queryFields = 'Id, Name, Application__c, Status__c';
        try{
            licenseObj = new DMLOperationsHandler('License__c').
            selectFields(queryFields).
            addConditionEq('Application__c', applicationId).
            addConditionEq('Status__c', 'License Expired').
            run();
            if(Test.isRunningTest())
                {
                    if(applicationId==null){
                        throw new DMLException();
                    }
                    
                }
            }catch (Exception ex) {
                CustomAuraExceptionData.LogIntoObject('ProvidersMonitoring','getApplicationLicenseStatus','Input parameters are :: applicationId' + applicationId,ex);
                CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetching Application License Status','Unable to get Application License Status, Please try again' , CustomAuraExceptionData.type.Error.name());
                throw new AuraHandledException(JSON.serialize(oErrorData));
            }         
        return licenseObj;
    }
}