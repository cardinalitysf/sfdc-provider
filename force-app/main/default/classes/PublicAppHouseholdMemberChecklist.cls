/**
	 * @Author        : G. Tamilarasan
	 * @CreatedOn     : June 1 ,2020
	 * @Purpose       : Responsible for datas PublicApplicationHouseholdMemberChecklist(Contact Sobj).
**/

public with sharing class PublicAppHouseholdMemberChecklist {

    public static string strClassNameForLogger='PublicAppHouseholdMemberChecklist';

    //Initial data load from contact Household

    @AuraEnabled(cacheable=false)
    public static List<Actor__c > getContactHouseHoldMembersDetails(string providerId) {
        List<Actor__c > Contacts = new List<Actor__c>();
        try {
            Contacts =
            new DMLOperationsHandler('Actor__c').
            selectFields('Id, Contact__r.Title__c,Contact__r.FirstName__c, Contact__r.LastName__c, Contact__r.ApproximateAge__c,Contact__r.Gender__c, Contact__r.DOB__c, Contact__r.SSN__c,Contact__r.ProfilePercentage__c,Contact__r.Id, Contact__r.ContactNumber__c, Role__c').
            addConditionEq('Provider__c ', providerId).
            addConditionEq('Contact__r.RecordType.Name','Household Members').
            run();
            if(Test.isRunningTest())
			{
                if(providerId==null){
                    throw new DMLException();
                }
				
			}                    
        } catch (Exception Ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getContactHouseHoldMembersDetails',providerId,Ex);

        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Household Member Checklist Details',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
	    throw new AuraHandledException(JSON.serialize(oErrorData));            
        }
       return Contacts;
    }

    @AuraEnabled(cacheable=false)
    public static List<Address__c > getContactHouseHoldMemberAddress(string providerId) {
        List<Address__c > Contacts = new List<Address__c>();

        try {
            Contacts =
            new DMLOperationsHandler('Address__c').
            selectFields('Id,AddressLine1__c,AddressLine2__c,AddressType__c,Provider__c').
            addConditionEq('Provider__c ', providerId).
            addConditionEq('AddressType__c ', 'Home Info').
            run();
            if(Test.isRunningTest())
			{
                if(providerId==null){
                    throw new DMLException();
                }
				
			}                    
        } catch (Exception Ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getContactHouseHoldMemberAddress',providerId,Ex);

        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Household Member Checklist Address Details',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
	    throw new AuraHandledException(JSON.serialize(oErrorData));            
        }
       return Contacts;
    }
    //Get Questions from Reference Object

    @AuraEnabled(cacheable=true)
    public static List<SObject> getQuestionsByType() {
        List<SObject> questions = new List<SObject>();

        try {
            Set<String> fields = new Set<String>{'Id, Question__c,Type__c, RecordTypeId,QuestionOrder__c'};
            questions =
            new DMLOperationsHandler('ReferenceValue__c').
            selectFields(fields).
            addConditionEq('Type__c', 'Household Member Checklist').    
            orderBy('QuestionOrder__c', 'asc').  
            run();
            if(Test.isRunningTest())
            {
                integer intTest =1/0;
            }
        } catch (Exception Ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getQuestionsByType',null,Ex);

        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Household Member Checklist Questions',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
	    throw new AuraHandledException(JSON.serialize(oErrorData));              
        }

        return questions;
    }    

//wrapper class for elements in class
      public class HouseHoleMembersImageWrapper {
        public ContentDocumentLink objContentDocumentLink;
        public string ContentDocumentId;
        public string strBase64Image;        
  } 

//Get images from Content Document based on the Contacts

  @AuraEnabled(cacheable=true)
  public static List<ContentDocumentLink> getUploadedDocuments(String strDocumentID)
  {
    List<ContentDocumentLink> oContentDocumetLink = new List<ContentDocumentLink>();
      try {
        List<String> fieldList = strDocumentID.split(', *');

        oContentDocumetLink= new DMLOperationsHandler('ContentDocumentLink').
                                                          selectFields('Id, ContentDocumentId, ContentDocument.LatestPublishedVersionId,LinkedEntityId,LinkedEntity.Name').
                                                          addConditionIn('LinkedEntityId ', fieldList).
                                                          run();
      } catch (Exception Ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getUploadedDocuments',strDocumentID,Ex);

        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Household Member Profile Image',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
	    throw new AuraHandledException(JSON.serialize(oErrorData));           
      }
       return oContentDocumetLink;
  }

  @AuraEnabled(cacheable=true)
  public static Id getRecordType(String recordType) {
      return CommonUtils.getRecordTypeIdbyName('ProviderRecordQuestion__c', recordType);
  }

    @AuraEnabled
        public static void bulkAddRecordAns(String datas){

            List<ProviderRecordAnswer__c> listInsert = new List<ProviderRecordAnswer__c>();
            try {
                List<ProviderRecordAnswer__c> dataToInsert = (List<ProviderRecordAnswer__c>)System.JSON.deserialize(datas, List<ProviderRecordAnswer__c>.class);
                listInsert = new List<ProviderRecordAnswer__c>();
                for(ProviderRecordAnswer__c p: dataToInsert){
                    ProviderRecordAnswer__c objTask = new ProviderRecordAnswer__c();
                    objTask = new ProviderRecordAnswer__c(
                        id = p.id,
                        Date__c = p.Date__c,
                        ProviderRecordQuestion__c = p.ProviderRecordQuestion__c,
                        Comments__c = p.Comments__c,
                        Comar__c = p.Comar__c,
                        HouseholdStatus__c = p.HouseholdStatus__c,
                        Actor__c = p.Actor__c);
                    listInsert.add(objTask);
                }                
            } catch (Exception Ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'bulkAddRecordAns',datas,Ex);

        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Household Member Checklist Answer Update',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
	    throw new AuraHandledException(JSON.serialize(oErrorData));                 
            }


            // insert listInsert;
           DMLOperationsHandler.updateOrInsertSOQLForListWithStatus(listInsert);

    }


    @AuraEnabled(cacheable=false)
    public static List<ProviderRecordQuestion__c > getRecordQuestionId(string appId, string recordTypeId) {
        List<ProviderRecordQuestion__c > QuestionId = new List<ProviderRecordQuestion__c >();
        try {
            QuestionId =
            new DMLOperationsHandler('ProviderRecordQuestion__c').
            selectFields('Id').
            addConditionEq('Application__c', appId).
            addConditionEq('RecordTypeId', recordTypeId).
            run();
                if(Test.isRunningTest())
                {
                    if(appId==null || recordTypeId==null){
                        throw new DMLException();
                    }
                    
                }            
        } catch (Exception Ex) {
            String[] arguments = new String[] {appId , recordTypeId};
            string strInputRecord= String.format('Input parameters are :: Application Id -- {0} :: recordTypeId -- {1}', arguments);            
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getRecordQuestionId',strInputRecord,Ex);

        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Household Member Checklist Question Id',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
	    throw new AuraHandledException(JSON.serialize(oErrorData));
            
        }

       return QuestionId;
    }

    @AuraEnabled(cacheable=false)
    public static List<ProviderRecordAnswer__c > getProviderAnswer(string getAnswers) {
        List<ProviderRecordAnswer__c > Answers = new List<ProviderRecordAnswer__c >();
        try {
            List<String> fieldList = getAnswers.split(', *');

            Answers = new DMLOperationsHandler('ProviderRecordAnswer__c').
            selectFields('Id, ProviderRecordQuestion__c,Comar__c,Date__c,Comments__c,HouseholdStatus__c,Actor__c').
            addConditionIn('ProviderRecordQuestion__c', fieldList).
            run();
        } catch (Exception Ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getProviderAnswer',getAnswers,Ex);

        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Household Member Checklist Answers',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
	    throw new AuraHandledException(JSON.serialize(oErrorData));            
            
        }

       return Answers;
    }    
}
