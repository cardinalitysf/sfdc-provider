/**
 * @Author        : Tamilarasan G
 * @CreatedOn     : August 13, 2020
 * @Purpose       : CPA Home Dashboard details
**/
@isTest
public with sharing class cpaHomeDashboard_Test {

    @isTest static void testCpaHomeDashboard(){
        CPAHome__c CPAHomeDetails = new CPAHome__c(Status__c = 'Pending');
        insert CPAHomeDetails;
        Application__c ApplicationDetails = new Application__c(Status__c = 'Pending');
        insert ApplicationDetails;
        Account AccountDetails = new Account(Name = 'account name',ProgramType__c='test');
        insert AccountDetails;

        
        try
        {
            cpaHomeDashboard.getProgramTypeList(null);
        }catch(Exception Ex)
        {
            
        }
        cpaHomeDashboard.getProgramTypeList(AccountDetails.Id);

        try
        {
            cpaHomeDashboard.getCPAHomeDetails(null,null);
        }catch(Exception Ex)
        {
            
        }
        cpaHomeDashboard.getCPAHomeDetails(AccountDetails.Id,AccountDetails.ProgramType__c);
        cpaHomeDashboard.UpdateCPAHome(CPAHomeDetails);

    }
    @isTest static void testCpaHomeDashboardNegative(){
        Account AccountDetails = new Account(Name = 'account name',ProgramType__c='test');
        insert AccountDetails;
        try
        {
            cpaHomeDashboard.getCPAHomeDetails(null,null);
        }catch(Exception Ex)
        {
            
        }
        cpaHomeDashboard.getCPAHomeDetails(AccountDetails.Id,'All');
    }
    
}
