/*
     Author         : Naveen S
     @CreatedOn     : July 24 ,2020
     @Purpose       : Provider License Information.
*/ 


public with sharing class providerLicensesInfo {    

    public static string strClassNameForLogger='providerLicensesInfo';
            @AuraEnabled(cacheable=false)
            public static List<SObject> getApplicationId(string providerId) {
            try{
                Set<String> fields = new Set<String>{'Id,Name'};
                List<SObject> applications =
                new DMLOperationsHandler('Application__c').
                selectFields(fields).
                addConditionEq('Provider__c', providerId).
                run();
                if(Test.isRunningTest())
                {
                if(providerId==null ){
                throw new DMLException();
                }
                }
                return applications;
            }
            catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getApplicationId',providerId,Ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Application Id from Provider ID',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
           }
            }

            @AuraEnabled(cacheable=false)
            public static List<SObject> getLicensesId(string applicationID) {
                try{
                List<String> lstapplicationID = applicationID.split(',');  
                Set<String> fields = new Set<String>{'Id,Name'};
                List<SObject> licenses =
                new DMLOperationsHandler('License__c').
                selectFields(fields).
                addConditionIn('Application__c', lstapplicationID).
                run();
                if(Test.isRunningTest())
                {
                if(applicationID==null ){
                throw new DMLException();
                }
                }
                return licenses;
            }
            catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getLicensesId',applicationID,Ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get License ID from Application ID',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
           }
                
            }        

            @AuraEnabled(cacheable=false)
            public static List<License__c> getLicenseInformation(string licenseId) {
                try{
                Set<String> fields = new Set<String>{'Id,Name,ProgramName__c,ProgramType__c,StartDate__c,EndDate__c,Status__c,Application__r.Gender__c,Application__r.Capacity__c,Application__r.Name,Application__r.MinAge__c,Application__r.MaxAge__c,Application__r.MinIQ__c,Application__r.MaxIQ__c,Application__r.Id,CreatedDate'};
                List<License__c> licenses =
                new DMLOperationsHandler('License__c').
                selectFields(fields).
                addConditionEq('Id', licenseId).
                run();
                if(Test.isRunningTest())
                {
                if(licenseId==null ){
                throw new DMLException();
                }
                }
                return licenses;
            }
                catch (Exception Ex) {
                    CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getLicenseInformation',licenseId,Ex);
                    CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('get License Information',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
                    throw new AuraHandledException(JSON.serialize(oErrorData));
                   }
            }          


            @AuraEnabled(cacheable=false)
            public static List<Account> getProviderDetails(string providerId) {
                try{
                Set<String> fields = new Set<String>{'Id, Name,Fax'};
                List<Account> accounts =
                new DMLOperationsHandler('Account').
                selectFields(fields).
                addConditionEq('Id', providerId).
                run();
                if(Test.isRunningTest())
                {
                if(providerId==null ){
                throw new DMLException();
                }
                }
                return accounts;
            }
            catch (Exception Ex) {
                CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getProviderDetails',providerId,Ex);
                CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('get Provider Information',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
                throw new AuraHandledException(JSON.serialize(oErrorData));
               }
            }

            @AuraEnabled(cacheable=false)
            public static List<Address__c> getaddressdetails(string getaddressdetails) {
                try{
            Set<String> fields = new Set<String>{'Id, Name, AddressType__c, AddressLine1__c,Email__c,Phone__c'};
            List<Address__c> addresses =
            new DMLOperationsHandler('Address__c').
            selectFields(fields).
            addConditionEq('Application__c', getaddressdetails).
            addConditionEq('AddressType__c', 'Site').
            run();
            if(Test.isRunningTest())
            {
            if(getaddressdetails==null ){
            throw new DMLException();
            }
            }
            return addresses;
        }catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getaddressdetails',getaddressdetails,Ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('get address details',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
           }
            }  

            @AuraEnabled(cacheable=false)
            public static List<Monitoring__c> getMonitoringDetails(string applicationid) {
                try{
            Set<String> fields = new Set<String>{'Id,Name,VisitationEndDate__c'};
            List<Monitoring__c> monitorings = 
            new DMLOperationsHandler('Monitoring__c').
            selectFields(fields).
            addConditionEq('ApplicationLicenseId__c', applicationid).
            orderBy('SystemModstamp', 'desc').
                setLimit(1).
            run();
            if(Test.isRunningTest())
            {
            if(applicationid==null ){
            throw new DMLException();
            }
            }
            return monitorings;
        }catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getMonitoringDetails',applicationid,Ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('get Mointoring details from Application based',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
           }
            }

            
            @AuraEnabled(cacheable=false)
            public static List<IRCRates__c> getIRCRatesCapacity(string licenseId) {
                try{
            Set<String> fields = new Set<String>{'Id','IRCBedCapacity__c', 'Status__c'};
            List<IRCRates__c> ircrates = 
            new DMLOperationsHandler('IRCRates__c').
            selectFields(fields).
            addConditionEq('LicenseType__c', licenseId).
            addConditionEq('Status__c', 'Active').
            run();
            if(Test.isRunningTest())
            {
            if(licenseId==null ){
            throw new DMLException();
            }
            }
            return ircrates;
        }catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getIRCRatesCapacity',licenseId,Ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('get IRCRates Capacity ',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
           }
            }


            @AuraEnabled(cacheable=false)
            public static List<Contract__c> getContractCapacity(string licenseId) {
                try{
            Set<String> fields = new Set<String>{'Id','TotalContractBed__c'};
            List<Contract__c> contracts = new DMLOperationsHandler('Contract__c').
            selectFields(fields).
            addConditionEq('License__c', licenseId).
            run();
            if(Test.isRunningTest())
            {
            if(licenseId==null ){
            throw new DMLException();
            }
            }
            return contracts;
            }
            catch (Exception Ex) {
                CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getContractCapacity',licenseId,Ex);
                CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('get Contract Capacity ',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
                throw new AuraHandledException(JSON.serialize(oErrorData));
               }
        }

  

        }