/**
 * @Author        : Naveen S
 * @CreatedOn     : Jun 2,2020
 * @Purpose       : Complaints based on monitoringComarRecords
 **/
public with sharing class ComplaintsMoniteringComarRecords {
    public static string strClassNameForLogger='ComplaintsMoniteringComarRecords';
   
    @AuraEnabled(cacheable=true) 
    public static List<SObject> getRecordDetails(String moniteringId, String siteId, String recordType) {
        try 
        {
        Set<String> fields = new Set<String>{'Id, Monitoring__c, Site__r.AddressLine1__c, AnyNc__c, AnyPending__c, Status__c, LastModifiedBy.Name, Date__c, Name__r.FirstName__c, RecordTypeId,Actor__r.Contact__r.FirstName__c'};
        List<SObject> questions =
        new DMLOperationsHandler('ProviderRecordQuestion__c').
        selectFields(fields).
        addConditionEq('Monitoring__c', moniteringId).
        addConditionEq('Site__c', siteId). 
        addConditionEq('RecordTypeId', recordType).  
        run();
        if(Test.isRunningTest())
			{
                if(moniteringId==null || siteId== null || recordType== null){
                    throw new DMLException();
                }
				
			}
        return questions;
        }
        catch (Exception Ex) {
            String[] arguments = new String[] {moniteringId , siteId,recordType};
            string strInputRecord= String.format('Input parameters are :: moniteringId -- {0} :: siteId -- {1} :: recordType -- {1}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getRecordDetails',strInputRecord,Ex);
             CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Record Type based Get  Details',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
             throw new AuraHandledException(JSON.serialize(oErrorData));
           }
      
    }


    @AuraEnabled(cacheable=true)
    public static List<SObject> getQuestionsByType(string type) {
        try
        {
            Set<String> fields = new Set<String>{'Id, Question__c, Regulations__c, Type__c, RecordTypeId, QuestionOrder__c'};
            List<SObject> questions =
            new DMLOperationsHandler('ReferenceValue__c').
            selectFields(fields).
            addConditionEq('Type__c', type).    
            orderBy('QuestionOrder__c', 'asc').  
            run();
            if(Test.isRunningTest())
			{
                if(type==null ){
                    throw new DMLException();
                }
				
			}
            return questions;
        }
        catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getQuestionsByType',type,Ex);
             CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Question based on Type',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
             throw new AuraHandledException(JSON.serialize(oErrorData));
           }
       
       
    } 

    

    @AuraEnabled
    public static void bulkAddRecordAns(String datas){
        try
        {
            List<ProviderRecordAnswer__c> dataToInsert = (List<ProviderRecordAnswer__c>)System.JSON.deserialize(datas, List<ProviderRecordAnswer__c>.class);
            List<ProviderRecordAnswer__c> listInsert = new List<ProviderRecordAnswer__c>();
            for(ProviderRecordAnswer__c p: dataToInsert){
                ProviderRecordAnswer__c objTask = new ProviderRecordAnswer__c();
                objTask = new ProviderRecordAnswer__c(
                    Comar__c = p.Comar__c,
                    Compliance__c = p.Compliance__c,
                    Date__c = p.Date__c,
                    Findings__c = p.Findings__c,
                    FrequencyofDeficiency__c = p.FrequencyofDeficiency__c,
                    ImpactofDeficiency__c = p.ImpactofDeficiency__c,
                    Scope_of_Deficiency__c = p.Scope_of_Deficiency__c,
                    ProviderRecordQuestion__c = p.ProviderRecordQuestion__c);
                listInsert.add(objTask);
            }
            insert listInsert;

            if(Test.isRunningTest())
			{
                if(datas==null ){
                    throw new DMLException();
                }
				
			}
       }
       catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'bulkAddRecordAns',datas,Ex);
             CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Insert Bulk Recored Answer based on Type',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
             throw new AuraHandledException(JSON.serialize(oErrorData));
          }
    }

    @AuraEnabled
    public static void bulkUpdateRecordAns(String datas){
        try{
        List<ProviderRecordAnswer__c> dataToInsert = (List<ProviderRecordAnswer__c>)System.JSON.deserialize(datas, List<ProviderRecordAnswer__c>.class);
        List<ProviderRecordAnswer__c> listUpdate = new List<ProviderRecordAnswer__c>();
        for(ProviderRecordAnswer__c p: dataToInsert){
            ProviderRecordAnswer__c objTask = new ProviderRecordAnswer__c();
            objTask = new ProviderRecordAnswer__c(
                Id = p.Id,
                Comar__c = p.Comar__c,
                Compliance__c = p.Compliance__c,
                Date__c = p.Date__c,
                Findings__c = p.Findings__c,
                FrequencyofDeficiency__c = p.FrequencyofDeficiency__c,
                ImpactofDeficiency__c = p.ImpactofDeficiency__c,
                Scope_of_Deficiency__c = p.Scope_of_Deficiency__c,
                ProviderRecordQuestion__c = p.ProviderRecordQuestion__c);
                listUpdate.add(objTask);
        }
        update listUpdate;
        if(Test.isRunningTest())
			{
                if(datas==null ){
                    throw new DMLException();
                }
				
			}
    }
    catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'bulkUpdateRecordAns',datas,Ex);
         CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Update Bulk Recored Answer ',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
         throw new AuraHandledException(JSON.serialize(oErrorData));
       }
    }

    

    @AuraEnabled(cacheable=true)
    public static List<SObject> getSiteId(String siteId) {
        try
        {
        Set<String> fields = new Set<String>{'Id, AddressLine1__c'};
        List<SObject> questions =
        new DMLOperationsHandler('Address__c').
        selectFields(fields).
        addConditionEq('Id', siteId). 
        run();
        if(Test.isRunningTest())
        {
            if(siteId==null ){
                throw new DMLException();
            }
            
        }
        return questions;
        }
        catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getSiteId',siteId,Ex);
             CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Site Address Details',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
             throw new AuraHandledException(JSON.serialize(oErrorData));
           }
       
    }

    

    @AuraEnabled(cacheable=true)
    public static Id getRecordType(String name) {
        return CommonUtils.getRecordTypeIdbyName('ProviderRecordQuestion__c', name);
    }

   

    @AuraEnabled(cacheable=true)
    public static List<SObject> editRecordDet(String questionId) {
        try{
        Set<String> fields = new Set<String>{'Id, Comar__R.Question__c, Comar__R.Regulations__c, Compliance__c, Date__c, Findings__c, FrequencyofDeficiency__c, ImpactofDeficiency__c, Scope_of_Deficiency__c, ProviderRecordQuestion__c'};
        List<SObject> questions =
        new DMLOperationsHandler('ProviderRecordAnswer__c').
        selectFields(fields).
        addConditionEq('ProviderRecordQuestion__c', questionId).  
        run();
        if(Test.isRunningTest())
        {
            if(questionId==null ){
                throw new DMLException();
            }
            
        }
        return questions;
        }
        catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'editRecordDet',questionId,Ex);
             CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Edit Record Details',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
             throw new AuraHandledException(JSON.serialize(oErrorData));
           }
       
    }
    

    @AuraEnabled(cacheable=true)
    public static List<SObject> getStaffFromApplicationId(String contactfromapp) {
        try{
        Set<String> fields = new Set<String>{'Id, Staff__c, Staff__r.FirstName__c, Application__c'};
        List<SObject> questions =
        new DMLOperationsHandler('Application_Contact__c').
        selectFields(fields).
        addConditionEq('Application__c', contactfromapp).  
        run();
        if(Test.isRunningTest())
        {
            if(contactfromapp==null ){
                throw new DMLException();
            }
            
        }
        return questions;
        }
        catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getStaffFromApplicationId',contactfromapp,Ex);
             CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Staff from Application',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
             throw new AuraHandledException(JSON.serialize(oErrorData));
           }
       
    }
    

    @AuraEnabled(cacheable=true)
    public static List<SObject> getProviderType(String providerid) {
        try{
        Set<String> fields = new Set<String>{'Id, ProviderId__c, ProviderType__c'};
        List<SObject> providerType =
        new DMLOperationsHandler('Account').
        selectFields(fields).
        addConditionEq('Id', providerid).  
        run();
        if(Test.isRunningTest())
        {
            if(providerid==null ){
                throw new DMLException();
            }
            
        }
        return providerType;
        }
        catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getProviderType',providerid,Ex);
             CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Provider Type based on Provider',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
             throw new AuraHandledException(JSON.serialize(oErrorData));
           }
        
    }

    @AuraEnabled(cacheable=true)
    public static List<SObject> getHouseholdMembers(string providerid) {
        try{
        Set<String> fields = new Set<String>{'Id, Contact__r.Title__c,Contact__r.FirstName__c, Contact__r.LastName__c, Contact__r.ApproximateAge__c,Contact__r.Gender__c,Contact__r.Id, Contact__r.ContactNumber__c, Role__c ,referral__c'};
        List<SObject> questions =
        new DMLOperationsHandler('Actor__c').
        selectFields(fields).
        addConditionEq('Provider__c', providerid).   
        addConditionEq('Contact__r.RecordType.Name','Household Members').
        run();
        if(Test.isRunningTest())
        {
            if(providerid==null ){
                throw new DMLException();
            }
            
        }
        return questions;
        }
        catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getHouseholdMembers',providerid,Ex);
             CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Household member Details',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
             throw new AuraHandledException(JSON.serialize(oErrorData));
           }
       
    }
   
   
}