@isTest
private class PublicApplicationDecision_Test{
    @isTest static void testupdateapplicationdetails (){
        Profile prf = [Select Id, Name from Profile where Name= 'Supervisor'];
        List<User> usrList = TestDataFactory.createTestUsers( 2, prf.Id,true );
        List<Account> lstAcct= TestDataFactory.testAccountData();
        List<Account> accList= new List<Account>();
        for(Account acct : lstAcct)
        {
           acct.Narrative__c = 'New Comments';
           accList.add(acct);
        }
        insert accList;
        update accList;
        List<Application__c> appList = TestDataFactory.TestApplicationData(2,'Rejected');
        insert appList;
        update appList;
        List<Monitoring__c> monList = TestDataFactory.createTestMonitoringData(5,true);
        update monList;
        List<ContentVersion> cvList = TestDataFactory.createTestContentVersionData();
        insert cvList;
        Set<ContentVersion> cvrset = new Set<ContentVersion>();
        for(ContentVersion cv:cvList)
        {
            cvrset.add(cv);
        }
         String str = 'Application__c';
        //Casting String to sObject
        sObject obj = Schema.getGlobalDescribe().get(str).newSObject();
        obj.put('ApprovalComments__c', 'test Comments');
        obj.put('ApprovalStatus__c', 'Caseworker Submitted');
        obj.put('Capacity__c', 10);
        obj.put('Id', appList[0].Id);
        obj.put('Provider__c', lstAcct[0].Id);
        obj.put('ProgramType__c','');
        obj.put('Comments__c','test 1');
        obj.put('Gender__c','male');
        obj.put('MaxAge__c',18);
        obj.put('MinAge__c',3);
        obj.put('MaxIQ__c',200);
        obj.put('MinIQ__c',1);
        obj.put('Narrative__c','text');
        obj.put('Status__c','Caseworker Submitted');
        SObject sobj = (SObject)Type.forName('ContentVersion').newInstance();
        List<DMLOperationsHandler.FetchValueWrapper> strgetPicklistvalues=PublicApplicationDecision.getRecommendationPickListValues(sobj,'Document__c');
        PublicApplicationDecision.getDecisionDetails (obj.Id);
        PublicApplicationDecision.getAllSuperUsersList();
        PublicApplicationDecision.saveSign('test',obj.Id);
        PublicApplicationDecision.getAssignedSuperUserTableDetails(obj.Id);
        PublicApplicationDecision.processingWrapper pWrap = new PublicApplicationDecision.processingWrapper(obj, cvrset);
        PublicApplicationDecision.processingWrapperString pWrapStr = new PublicApplicationDecision.processingWrapperString(obj, '');
        // PublicApplicationDecision.updateapplicationdetails(obj,usrList[0].Id, obj.Id);
    }
    @isTest static void testupdateapplicationdetailsTwo (){
        Profile prf = [Select Id, Name from Profile where Name= 'Supervisor'];
        List<User> usrList = TestDataFactory.createTestUsers( 2, prf.Id,true );
        List<Account> lstAcct= TestDataFactory.testAccountData();
        insert lstAcct;
        for(Account acct : lstAcct)
        {
           
        }
        List<Application__c> appList = TestDataFactory.TestApplicationData(2,'Rejected');
        insert appList;
        List<Monitoring__c> monList = TestDataFactory.createTestMonitoringData(5,true);
        update monList;
        List<ContentVersion> cvList = TestDataFactory.createTestContentVersionData();
        insert cvList;
        Set<ContentVersion> cvrset = new Set<ContentVersion>();
        for(ContentVersion cv:cvList)
        {
            cvrset.add(cv);
        }
         String str = 'Application__c';
        //Casting String to sObject
        sObject obj = Schema.getGlobalDescribe().get(str).newSObject();
        obj.put('ApprovalComments__c', 'test Comments');
        obj.put('ApprovalStatus__c', 'Caseworker Submitted');
        obj.put('Capacity__c', 10);
        obj.put('Id', appList[0].Id);
        obj.put('Provider__c', lstAcct[0].Id);
        obj.put('ProgramType__c','');
        obj.put('Comments__c','test 1');
        obj.put('Gender__c','male');
        obj.put('MaxAge__c',18);
        obj.put('MinAge__c',3);
        obj.put('MaxIQ__c',200);
        obj.put('MinIQ__c',1);
        obj.put('Narrative__c','text');
        obj.put('Status__c','Caseworker Submitted');
        SObject sobj = (SObject)Type.forName('ContentVersion').newInstance();
        List<DMLOperationsHandler.FetchValueWrapper> strgetPicklistvalues=PublicApplicationDecision.getRecommendationPickListValues(sobj,'Document__c');
        PublicApplicationDecision.getDecisionDetails (obj.Id);
        PublicApplicationDecision.getAllSuperUsersList();
        PublicApplicationDecision.saveSign('test',obj.Id);
        PublicApplicationDecision.getAssignedSuperUserTableDetails(obj.Id);
        PublicApplicationDecision.processingWrapper pWrap = new PublicApplicationDecision.processingWrapper(obj, cvrset);
        PublicApplicationDecision.processingWrapperString pWrapStr = new PublicApplicationDecision.processingWrapperString(obj, '');
        // PublicApplicationDecision.updateapplicationdetails(obj,'', obj.Id);
    }

    @IsTest static void testPublicApplicationDecisionValues()
    {
        Profile prf = [Select Id, Name from Profile where Name= 'Supervisor'];
        List<User> userList =  TestDataFactory.createTestUsers(1, prf.Id, true);

        Application__c InsertobjPublicApplicationDecisionPeriod=new Application__c(ApprovalStatus__c='Approved', Status__c='Caseworker Submitted',ApprovalComments__c='test',Caseworker__c=userList[0].id,ReviewStatus__c='Submit for Review',CommonDecision__c='Approve');
        insert InsertobjPublicApplicationDecisionPeriod;

        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments('Submitting request for approval.');
        req.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        req.setObjectId(InsertobjPublicApplicationDecisionPeriod.Id);
        Approval.ProcessResult resu = Approval.process(req);
        System.assert(resu.isSuccess());
        System.assertEquals('Pending', resu.getInstanceStatus(),'Instance Status'+resu.getInstanceStatus());
        List<Id> newWorkItemIds = resu.getNewWorkitemIds();
        PublicApplicationDecision.updateapplicationdetails(InsertobjPublicApplicationDecisionPeriod,'',newWorkItemIds.get(0));
    }

    
}