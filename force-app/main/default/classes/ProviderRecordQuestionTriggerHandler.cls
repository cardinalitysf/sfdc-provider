public with sharing class ProviderRecordQuestionTriggerHandler implements ITriggerHandler{
    	/**
	 * @Author        : V.S.Marimuthu
	 * @CreatedOn     : April 21 ,2020
	 * @Purpose       : To update Task Object when status is moved to completed in ProviderRecordQuestion
	 **/
    public Boolean isDisabled(){
		// string blnTriggerSwitch = 'false';
		// List<Bypass_Switch_For_Trigger__mdt> triggerSwitch = [select
		//                                                             Trigger_Switch__c
		//                                                       from Bypass_Switch_For_Trigger__mdt];
		// for (Bypass_Switch_For_Trigger__mdt value : triggerSwitch){
		//     blnTriggerSwitch = value.Trigger_Switch__c;
		// }
		// 
		// return blnTriggerSwitch == 'false' ? false : true;
		return false;
	}

	public void beforeInsert(List<Sobject> newItems){
		
	}

	public void beforeUpdate(Map<id, sObject> newItems, Map<id, sObject> oldItems){
	
	}

	public void beforeDelete(Map<id, sObject> oldItems){
	
	}

	public void afterInsert(List<Sobject> newMap){
		try{
			List<ProviderRecordQuestion__c> objProviderQuestion= new List<ProviderRecordQuestion__c>();
			List<Monitoring__c> objMonitoring= new List<Monitoring__c>();
			set<string> setActivityTypes=new set<string>();
			set<Id> setActivityTypeRecordID= new set<Id>();
			// 	// Iterate over the list of monitor list and extract the values
			// 	// During first time insert be default we are setting "Draft as status"
				for(ProviderRecordQuestion__c providerQuestion:(List<ProviderRecordQuestion__c>)newMap)
				{
					if(providerQuestion.Status__c == 'Completed')
					{
					Monitoring__c monitoring= new Monitoring__c();
                    monitoring.Id=providerQuestion.Monitoring__c; 
					setActivityTypeRecordID.add(providerQuestion.RecordTypeId);
					objMonitoring.add(monitoring);
					}
				}
				if(objMonitoring.Size()>0)
				{
					fetchRecordNameFromProviderQuestionAndUpdateTaskStatus(objMonitoring,setActivityTypeRecordID);
				}
			
		}
		catch(Exception Ex)
		{

		}
	}

	public void afterUpdate(Map<Id, sObject> newMap, Map<Id, sObject> oldMap){
		
		try{			
            List<ProviderRecordQuestion__c> objProviderQuestion= new List<ProviderRecordQuestion__c>();
            List<Monitoring__c> objMonitoring= new List<Monitoring__c>();
		 
			set<string> setActivityTypes=new set<string>();
			set<Id> setActivityTypeRecordID= new set<Id>();
		    for(ProviderRecordQuestion__c providerQuestion : ((Map<Id,ProviderRecordQuestion__c>)newMap).values()){
		        //Create an old and new map so that we can compare values
		        ProviderRecordQuestion__c oldProviderQuestion = (ProviderRecordQuestion__c)oldMap.get(providerQuestion.ID);
		        ProviderRecordQuestion__c newProviderQuestion = (ProviderRecordQuestion__c)newMap.get(providerQuestion.ID);
		        //If the fields are different, then ID has changed
		        if(oldProviderQuestion.Status__c != newProviderQuestion.Status__c && newProviderQuestion.Status__c== 'Completed' ){
					Monitoring__c monitoring= new Monitoring__c();
                    monitoring.Id=providerQuestion.Monitoring__c; 
					setActivityTypeRecordID.add(providerQuestion.RecordTypeId);
                    objMonitoring.add(monitoring);
		        }
            }//for	
			if(objMonitoring.Size()>0)
			{
			fetchRecordNameFromProviderQuestionAndUpdateTaskStatus(objMonitoring,setActivityTypeRecordID);  
			}      	   
		}
		catch(Exception Ex)
		{
		}
	}
	/**
	 * Changed by V.S.Marimuthu on August 15th inorder to satisfy both Application monitoring and complaints monitoring flow.
	 */
	private void fetchRecordNameFromProviderQuestionAndUpdateTaskStatus(List<Monitoring__c> objMonitoring,set<Id> setActivityTypeRecordID)
	{
	   set<string> setActivityTypes=new set<string>();
	   List<Task> lstTask= new List<Task>();
	   Boolean blnAllStaffPresent=false;
		try{		
			   List<ProviderRecordQuestion__c> lstProviderQuestion=[select RecordType.Name,Name__c,Status__c from ProviderRecordQuestion__c WHERE  RecordTypeid in : setActivityTypeRecordID and Monitoring__c in :objMonitoring];
			   Map<Id, String> staffStatus = new Map<Id, String>();
			   string strRecordTypeName='';
				   for(ProviderRecordQuestion__c prq:lstProviderQuestion)
				   {
					   strRecordTypeName=prq.RecordType.Name == 'Office'?'Office inspection':prq.RecordType.Name == 'Physical Plant'? 'Physical Plant Inspection' : prq.RecordType.Name;
					   setActivityTypes.add(strRecordTypeName);
					   staffStatus.put(prq.Name__c,prq.Status__c);
				   }
				   if(setActivityTypes.contains('Board Interview') || setActivityTypes.contains('Staff Interview') || setActivityTypes.contains('Staff Record') )
					{
						//If lstApp size > 0 then Application monitoring flow and  below query will be executed
						//If lstApp size == 0 then we have to consider as complaints monitoring flow
						
						//Application Monitoring flow --- To get the staff ID we have to filter the ApplicationLicenseId__c from Monitoring object and then get the staff from Application_Contacts__r
						List<Application__c> lstApp=[Select Id,(select Id,Staff__c from Application_Contacts__r)  from Application__c where id in(select ApplicationLicenseId__c from monitoring__c where id in :objMonitoring)];
						
						if(lstApp.size()==0)
						{
							//Complaints Monitoring flow --- In complaints flow we wont get ApplicationLicenseId__c instead we will get License ID.
							//With that first get the Application Id by linking License object and monitoring object and form one application ID set
							List<License__c> lstLic=[Select Application__c from License__c where id in(select License__c from monitoring__c where id in :objMonitoring)];
							Set<ID> osetAppID=new Set<ID>();
							for (License__c oLic : lstLic) {
								osetAppID.add(oLic.Application__c);
							}	
							//Now get the staff from Application_Contacts__r from the set of application ID formed above 					
							lstApp=[Select Id,(select Id,Staff__c from Application_Contacts__r)  from Application__c where id in : osetAppID];
						}
							Set<Id> setstaffID=new Set<ID>();   
								for(Application__c application : lstApp)
									{
										for(Application_Contact__c objAppContact: application.Application_Contacts__r)
										{
											setstaffID.add(objAppContact.Staff__c);
										}  
									} 
							blnAllStaffPresent = staffStatus.keySet().containsAll(setstaffID) && setstaffID.containsAll(staffStatus.keySet()) && staffStatus.values().indexOf('Draft') == -1;
						
					}
					else {
						blnAllStaffPresent=true;
					}
					if(blnAllStaffPresent)
					{
						lstTask =[select Id,Status__c,Monitoring__c,ActivityName__c  from Task where Monitoring__c in :objMonitoring and ActivityName__c in :setActivityTypes ] ;		
						List<Task> tasktoUpdate=new List<Task>();
							for(Task indtask:lstTask)
						{
							Task tsk=new Task();
							tsk.id=indtask.Id;
							tsk.Status__c='Completed';
							tsk.CompletionDate__c=System.today();
							tasktoUpdate.add(tsk);
						}
							new TaskService().updateTaskStatus(tasktoUpdate);	
					}
		   }
		   catch(Exception Ex)
		   {
   
		   }
	 
	}
	public void afterDelete(Map<id, sObject> oldMap){
		
	}

	public void afterUndelete(List<Sobject> newMap){
		
	}
}