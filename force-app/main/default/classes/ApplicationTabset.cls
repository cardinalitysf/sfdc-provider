/**
 * @Author        : Janaswini S
 * @CreatedOn     : March 20 ,2020
 * @Purpose       : Application Tabsets
 **/

public with sharing class ApplicationTabset {
    public static string strClassNameForLogger='ApplicationTabset';
 @AuraEnabled(cacheable = true)
    public  static List<SObject> fetchDataForTabset(string fetchdataname) {
        String fields = 'Id,Name';
        List<sObject> lstApplication = new List<sObject>();
        try {
            lstApplication= new DMLOperationsHandler('Application__c').
            selectFields(fields).
            addConditionEq('Id',fetchdataname).
            run(); 
            if(Test.isRunningTest())
            {
                if(fetchdataname==null){
                    throw new DMLException();
                }
                
            }
        } catch (Exception ex) {
            CustomAuraExceptionData.LogIntoObject('ApplicationTabset','fetchDataForTabset','Input parameters are :: fetchdataname' + fetchdataname,ex);
                CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetching application details','Unable to get Application details, Please try again' , CustomAuraExceptionData.type.Error.name());
                throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        
        return lstApplication;
    }
}