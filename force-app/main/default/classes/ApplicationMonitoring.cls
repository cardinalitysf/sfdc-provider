/***
 * @Author: Pratheeba V
 * @CreatedOn: April 6, 2020
 * @Purpose: Application Monitoring Add/List/Edit
 * @updatedBy: Pratheeba V
 * @updatedOn: May 5,2020 
 * @Purpose: Changed select query format
**/

public with sharing class ApplicationMonitoring {
    public static string strClassNameForLogger='ApplicationMonitoring';
    @AuraEnabled
    public static string InsertUpdateMonitoring(sObject objSobjecttoUpdateOrInsert){
        return DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert);
    }

    @AuraEnabled(cacheable=false)
    public static List<Monitoring__c> getMonitoringDetails(string applicationid) {
        List<Monitoring__c> monitoringDetailsObj = new List<Monitoring__c>();
        Set<String> fields = new Set<String>{'Id','Name','Program__c','ProgramType__c','ApplicationLicenseId__r.Name','SiteAddress__c','SiteAddress__r.Id','SiteAddress__r.AddressLine1__c','SiteAddress__r.City__c','SelectPeriod__c','VisitationStartDate__c','VisitationEndDate__c','AnnouncedUnannouced__c','TimetoVisit__c','JointlyInspectedWith__c','Status__c'};
        try {
            monitoringDetailsObj = new DMLOperationsHandler('Monitoring__c').
            selectFields(fields).
            addConditionEq('ApplicationLicenseId__c', applicationid).
            orderBy('Name', 'Desc').
            run();
            if(Test.isRunningTest()) {
                if(applicationid=='') {
                    throw new DMLException();
                }				
			}
        } catch (Exception Ex) { 
            String[] arguments = new String[] {applicationid};
            string strInputRecord= String.format('Input parameters are :: applicationid -- {0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getMonitoringDetails',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('All Monitoring List',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return monitoringDetailsObj;
    }

    @AuraEnabled(cacheable=false)
    public static List<Address__c> getAddressDetails(string applicationid) {
        List<Address__c> addressObj = new List<Address__c>();
        Set<String> fields = new Set<String>{'Id','Name','AddressType__c','AddressLine1__c','City__c','County__c'};
        try {
            addressObj = new DMLOperationsHandler('Address__c').
            selectFields(fields).
            addConditionEq('Application__c', applicationid).
            addConditionEq('AddressType__c', 'Site').
            run();
            if(Test.isRunningTest()) {
                if(applicationid=='') {
                    throw new DMLException();
                }				
            }
        } catch (Exception Ex) { 
            String[] arguments = new String[] {applicationid};
            string strInputRecord= String.format('Input parameters are :: applicationid -- {0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getAddressDetails',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Address List',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }   
        return addressObj;
    }

    @AuraEnabled(cacheable=false)
    public static List<Application__c> getProgramDetails(string applicationid) {
        List<Application__c> programObj = new List<Application__c>();
        Set<String> fields = new Set<String>{'Id','Name','Program__c','ProgramType__c'};
        try {
            programObj = new DMLOperationsHandler('Application__c').
            selectFields(fields).
            addConditionEq('Id', applicationid).
            run();
            if(Test.isRunningTest()) {
                if(applicationid=='') {
                    throw new DMLException();
                }				
            }
        } catch (Exception Ex) { 
            String[] arguments = new String[] {applicationid};
            string strInputRecord= String.format('Input parameters are :: applicationid -- {0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getProgramDetails',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Program List',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }   
        return programObj;
    }

    @AuraEnabled(cacheable=false)
    public static List<Monitoring__c> editMonitoringDetails(string editMonitoringDetails) {
        List<Monitoring__c> monitoringEditObj = new List<Monitoring__c>();
        Set<String> fields = new Set<String>{'Id','Name','Program__c','ProgramType__c','ApplicationLicenseId__c','SiteAddress__c','SelectPeriod__c','VisitationStartDate__c','VisitationEndDate__c','AnnouncedUnannouced__c','TimetoVisit__c','JointlyInspectedWith__c'};
        try {
            monitoringEditObj = new DMLOperationsHandler('Monitoring__c').
            selectFields(fields).
            addConditionEq('Id', editMonitoringDetails).
            run();
            if(Test.isRunningTest()) {
                if(editMonitoringDetails=='') {
                    throw new DMLException();
                }				
            }
        } catch (Exception Ex) { 
            String[] arguments = new String[] {editMonitoringDetails};
            string strInputRecord= String.format('Input parameters are :: editMonitoringDetails -- {0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'editMonitoringDetails',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Monitoring List',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }   
        return monitoringEditObj;
    }

    @AuraEnabled(cacheable=false)
    public static List<Monitoring__c> viewMonitoringDetails(String viewMonitoringDetails) {
        List<Monitoring__c> monitoringObj = new List<Monitoring__c>();
        Set<String> fields = new Set<String>{'Id','Name','Program__c','ProgramType__c','ApplicationLicenseId__c','SiteAddress__c','SelectPeriod__c','VisitationStartDate__c','VisitationEndDate__c','AnnouncedUnannouced__c','TimetoVisit__c','JointlyInspectedWith__c'};
        try {
            monitoringObj = new DMLOperationsHandler('Monitoring__c').
            selectFields(fields).
            addConditionEq('Id', viewMonitoringDetails).
            run();
            if(Test.isRunningTest()) {
                if(viewMonitoringDetails=='') {
                    throw new DMLException();
                }				
            }
        } catch (Exception Ex) { 
            String[] arguments = new String[] {viewMonitoringDetails};
            string strInputRecord= String.format('Input parameters are :: viewMonitoringDetails -- {0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'viewMonitoringDetails',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Monitoring List',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        } 
        return monitoringObj;
    }

    @AuraEnabled(cacheable=true)
    public static List<Application__c> getApplicationProviderStatus(String applicationId) {
        List<Application__c> applicationStatusObj = new List<Application__c>();
        Set<String> fields = new Set<String>{'Id','Status__c'};
        try {
            applicationStatusObj = new DMLOperationsHandler('Application__c').
            selectFields(fields).
            addConditionEq('Id', applicationId).
            run();
            if(Test.isRunningTest()) {
                if(applicationId=='') {
                    throw new DMLException();
                }				
            }
        } catch (Exception Ex) { 
            String[] arguments = new String[] {applicationId};
            string strInputRecord= String.format('Input parameters are :: applicationId -- {0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getApplicationProviderStatus',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Status List',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        } 
        return applicationStatusObj;
    }

    @AuraEnabled(cacheable=true)
    public static List<Monitoring__c> getMonitoringStatus(String monitoringid) {
        List<Monitoring__c> monitoringStatusObj = new List<Monitoring__c>();
        Set<String> fields = new Set<String>{'Id','Status__c'};
        try {
            monitoringStatusObj = new DMLOperationsHandler('Monitoring__c').
            selectFields(fields).
            addConditionEq('Id', monitoringid).
            run();
            if(Test.isRunningTest()) {
                if(monitoringid=='') {
                    throw new DMLException();
                }				
            }
        } catch (Exception Ex) { 
            String[] arguments = new String[] {monitoringid};
            string strInputRecord= String.format('Input parameters are :: monitoringid -- {0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getMonitoringStatus',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Monitoring Status',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        } 
        return monitoringStatusObj;
    }

    //Sundar Adding this class for getting Application License Status
    @AuraEnabled(cacheable = true)
    public static List<sObject> getApplicationLicenseStatus(String applicationId) {
        List<sObject> licenseObj = new List<sObject>();
        String queryFields = 'Id, Name, Application__c, Status__c';
        try {
            licenseObj = new DMLOperationsHandler('License__c').
                                    selectFields(queryFields).
                                    addConditionEq('Application__c', applicationId).
                                    addConditionEq('Status__c', 'License Expired').
                                    run();
            if(Test.isRunningTest()) {
                if(applicationId=='') {
                    throw new DMLException();
                }				
            }
        } catch (Exception Ex) { 
            String[] arguments = new String[] {applicationId};
            string strInputRecord= String.format('Input parameters are :: applicationId -- {0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getApplicationLicenseStatus',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Application License Status',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        } 
        return licenseObj;
    }
}