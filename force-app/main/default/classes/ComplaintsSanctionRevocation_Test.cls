/**
 * @Author        : Nandhini 
 * @CreatedOn     : JULY 23 ,2020
 * @Purpose       : Sanctions Revocation Screen
 **/
@isTest
private with sharing class ComplaintsSanctionRevocation_Test {
    @isTest static void testsanctionrevocation() {

        Sanctions__c insertsanction = new Sanctions__c();
        insert insertsanction;

        try
        {
            ComplaintsSanctionRevocation.getSelectedSanctionData(null);
        }
        catch(Exception Ex)
        {
        }               
        ComplaintsSanctionRevocation.getSelectedSanctionData(insertsanction.id);    
        

        try
        {
            ComplaintsSanctionRevocation.getRecordType(null);
        }
        catch(Exception Ex)
        {
            
        } 
        ComplaintsSanctionRevocation.getRecordType('Revocation');

    }
}
