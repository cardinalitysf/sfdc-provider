public with sharing class providerLicense {
    @AuraEnabled(cacheable=false)
    public static List<SObject> getlicenseDetails(string licensedetails) {

        String model = 'License__c';
        String fields = 'Id, Name,ProgramName__c,ProgramType__c,StartDate__c,EndDate__c';
        String cond = 'Application__c =\''+ licensedetails+'\'';
        return DMLOperationsHandler.selectSOQLWithConditionParameters(model,fields,cond);
}
    

    @AuraEnabled(cacheable=false)
        public static List<SObject> editlicenseDetails(string editlicensedetails) {
    
            String model = 'License__c';
            String fields = 'Id, Name, ProgramName__c, ProgramType__c,StartDate__c,EndDate__c,Application__r.MinAge__c,Application__r.MaxAge__c,Application__r.Gender__c,Application__r.Capacity__c';
            String cond = 'Id =\''+ editlicensedetails+'\'';
            return DMLOperationsHandler.selectSOQLWithConditionParameters(model,fields,cond);
        }

        @AuraEnabled(cacheable=false)
       public static List<SObject> getaddressdetails(string getaddressdetails) {
        String model = 'Address__c';
        String fields = 'Id, Name, AddressType__c, AddressLine1__c'; 
        String cond = 'Application__c =\''+ getaddressdetails+'\' AND AddressType__c=\'Site\'';
        return DMLOperationsHandler.selectSOQLWithConditionParameters(model,fields,cond);
  }

  @AuraEnabled(cacheable=false)
  public static List<SObject> getcontactnamedetails(string getcontactnamedetails) {

      String model = 'Application__c';
      String fields = 'Id,Provider__r.Name,Provider__r.FirstName__c,Provider__r.LastName__c';
      String cond = 'Id =\''+ getcontactnamedetails+'\'';
      return DMLOperationsHandler.selectSOQLWithConditionParameters(model,fields,cond);
  }

  @AuraEnabled(cacheable=false)
  public static List<sObject> getsignaturedetails() {

      String model = 'ContentVersion';
      String fields = 'Id,ContentDocumentId,FileExtension, Document__c, IsSignature__c';
      String cond = 'Id=\'0680p000000R3vUAAS\'AND IsSignature__c=true AND Document__c=\'Images\'';
      return DMLOperationsHandler.selectSOQLWithConditionParameters(model,fields,cond);
  }
  @AuraEnabled
  public static string getSignatureAsBas64Image(string appID) {
    ContentVersion cv=[select id,ContentDocumentId,versiondata from Contentversion where Id='0680p000000R3vUAAS' ];
    Blob base64Image =cv.VersionData;
    String base64ImageAsString=EncodingUtil.Base64Encode(base64Image);
    return base64ImageAsString;
  }
  @AuraEnabled(cacheable=false)
  public static List<SObject> getcaseworkerdetails(string getcaseworkerdetails) {

      String model = 'Application__c';
      String fields = 'Id,Caseworker__r.Name,SubmittedDate__c';
      String cond = 'Id =\''+ getcaseworkerdetails+'\'';
      return DMLOperationsHandler.selectSOQLWithConditionParameters(model,fields,cond);
  }

}