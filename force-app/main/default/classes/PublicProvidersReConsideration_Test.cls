/**
     * @Author        : Vijayaraj M
     * @CreatedOn     : June 25,2020
     * @Purpose       :Test Methods to unit test for PublicProvidersReConsideration.cls
     **/
    
     @isTest 
private with sharing class PublicProvidersReConsideration_Test {
    @isTest static void testPublicProvidersReConsideration_Test() {
        Reconsideration__c InsertobjNewReconsideration=new Reconsideration__c(Status__c = 'XYZ'); 
        insert InsertobjNewReconsideration;

        try {
              PublicProvidersReConsideration.getPublicProvidersReConsideration('');
        } catch(Exception e) {}
           
        PublicProvidersReConsideration.getPublicProvidersReConsideration(InsertobjNewReconsideration.Provider__c);
           

    }
}
