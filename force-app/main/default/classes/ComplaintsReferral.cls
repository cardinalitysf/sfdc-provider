/**
 * @Author        : Preethi Bojarajan
 * @CreatedOn     : July 09, 2020
 * @Purpose       : New Complaints Registering Model Functions
 * @updatedBy     : 
 * @updatedOn     : 
 **/
public with sharing class ComplaintsReferral {
    public static string strClassNameForLogger='ComplaintsReferral';

    @AuraEnabled(cacheable=true)
    public static List<SObject> getProvidersList(String providerType, String providerId, String name) {
        String model = 'Account';
        String fields = 'Id, ProviderId__c, Name, Email__c, ProviderType__c, TaxId__c, Status__c';
        providerId = '%' + providerId + '%';
        name = '%' + name + '%';
        String type = 'Inactive,Active';
        List<String> lstStatus = type.split(',');
        String justRole = 'Applicant,Co-Applicant';
        List<String> role = justRole.split(',');
        List<SObject> proviList = new List<SObject>();
     try {
        if (providerType != null && providerType == 'Public') {
                    proviList = new DMLOperationsHandler(model).
                                        selectFields(fields).
                                        addSubquery(
                                        DMLOperationsHandler.subquery('Actors__r').
                                        selectFields('Id, Contact__r.LastName, Contact__r.SSN__c, Role__c, Referral__r.Program__c').
                                        addConditionLike('Contact__r.LastName', name).
                                        addConditionIn('Role__c', role)).
                                        addSubquery(
                                        DMLOperationsHandler.subquery('License__r').
                                        selectFields('Id, Address__r.AddressLine1__c, ProgramName__c, ProgramType__c')).
                                        addConditionIn('Status__c', lstStatus).
                                        addConditionEq('ProviderType__c', providerType).
                                        addConditionLike('ProviderId__c', providerId).
                                        orderBy('LastModifiedDate', 'DESC').
                                        run();
                                        if(Test.isRunningTest()) {
                                            if(providerType == null || providerId == null || name == ''){
                                                throw new DMLException();
                                            }
                                        }
                                        return proviList;
            } else {
                proviList = new DMLOperationsHandler(model).
                                selectFields(fields).
                                addSubquery(
                                DMLOperationsHandler.subquery('License__r').
                                selectFields('Id, Address__r.AddressLine1__c, ProgramName__c, ProgramType__c')).
                                addConditionIn('Status__c', lstStatus).
                                addConditionEq('ProviderType__c', providerType).
                                addConditionLike('ProviderId__c', providerId).
                                addConditionLike('Name', name).
                                orderBy('LastModifiedDate', 'DESC').
                                run();
                                if(Test.isRunningTest()) {
                                    if(providerType== '' || providerId == null || name == ''){
                                        throw new DMLException();
                                    }
                                }
                                return proviList;
            }
        } catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getProvidersList',providerType+ 'providerType' +providerId + 'providerId' + name + 'name' ,Ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Sorry,Failed to load Referral list details ,Please Try Again..!',Ex.getMessage() , CustomAuraExceptionData.type.error.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
    }

    @AuraEnabled(cacheable = true)
        public static List<SObject> getCaseHeaderDetails(String newreferral, String model) {
            List<sObject> compRefObj = new List<sObject>();
            try {
                String fieldsCase = 'Id, CaseNumber, Origin, SourceofInformation__c, ComplaintSource__c, ComplaintReceivedDate__c, OwnerId, License__c, Address__c, Status, Account.ProviderId__c, Account.Name, Account.Email__c, Account.ProviderType__c, Account.TaxId__c, Account.Status__c';
                String fieldsAddress = 'Id, AddressLine1__c, AddressLine2__c, City__c, County__c, Phone__c, Email__c, State__c, ZipCode__c, Actor__r.Referral__r.CaseNumber, Actor__r.Referral__r.Origin, Actor__r.Referral__r.SourceofInformation__c, Actor__r.Referral__r.ComplaintSource__c, Actor__r.Referral__r.ComplaintReceivedDate__c, Actor__r.Referral__r.OwnerId, Actor__r.Referral__r.License__c, Actor__r.Referral__r.Address__c, Actor__r.Referral__r.Status, Actor__r.Referral__r.Account.ProviderId__c, Actor__r.Referral__r.Account.Name, Actor__r.Referral__r.Account.Email__c, Actor__r.Referral__r.Account.ProviderType__c, Actor__r.Referral__r.Account.TaxId__c, Actor__r.Referral__r.Account.Status__c, Actor__r.Contact__r.FirstName__c, Actor__r.Contact__r.LastName__c, Actor__r.Contact__r.Description';
                String fields = model == 'Case' ? fieldsCase : fieldsAddress;
                String eqParamAdd = model == 'Case' ? 'Id' : 'Actor__r.Referral__c';
                compRefObj = new DMLOperationsHandler(model).
                                    selectFields(fields).
                                    addConditionEq(eqParamAdd, newreferral).               
                                    run();
                if(compRefObj.size() == 0)
                compRefObj = new DMLOperationsHandler('Case').
                                    selectFields(fieldsCase).
                                    addConditionEq('Id', newreferral).               
                                    run();
                                    if(Test.isRunningTest()) {
                                        if(newreferral == null || model == null){
                                            throw new DMLException();
                                        }
                                    }
            } catch (Exception ex) {
                CustomAuraExceptionData.LogIntoObject('ComplaintsReferral','getCaseHeaderDetails','Input parameters are :: newreferral' + newreferral ,ex);
                CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in creating referral details','Unable to get referral details, Please try again' , CustomAuraExceptionData.type.Error.name());
                throw new AuraHandledException(JSON.serialize(oErrorData));
            }           
            return compRefObj;
    }

    @AuraEnabled(cacheable=true)
     public static Map<String, List<DMLOperationsHandler.FetchValueWrapper>> getMultiplePicklistValues(String objInfo, String picklistFieldApi) {
         return DMLOperationsHandler.fetchMultiplePickListValue(objInfo,picklistFieldApi);
    }

    @AuraEnabled(cacheable = true)
    public static List<SObject> fetchUserId(String emailId) {
        List<sObject> compRefObj = new List<sObject>();
       try {
            compRefObj = new DMLOperationsHandler('User').
                                selectFields('Id, Email, Username').
                                addConditionEq('Email', emailId).            
                                run();
                                if(Test.isRunningTest()) {
                                    if(emailId == null) {
                                        throw new DMLException();
                                    }
                                }
        } catch (Exception ex) {
            CustomAuraExceptionData.LogIntoObject('ComplaintsReferral','Get User Id','Input parameters are :: Provider Mail Id' + emailId ,ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in Fetching User Id','Unable to get Provider\'s User Id, Please try again' , CustomAuraExceptionData.type.Error.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }   
        return compRefObj;
    }

    @AuraEnabled
    public static Id getIdAfterInsertSOQL(sObject objIdSobjecttoUpdateOrInsert) {
        return DMLOperationsHandler.getIdAfterInsertSOQL(objIdSobjecttoUpdateOrInsert);
    }

    @AuraEnabled(cacheable=true)
    public static Id getRecordType(String name, String model) {
        try {
            return CommonUtils.getRecordTypeIdbyName(model, name);
        } catch (Exception ex) {
            //To Log into object
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getRecordType', 'Record Type Name' + name, ex);
            //To return back to the UI
            CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Record Type Fetch Issue,Please Try Again..!', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
    }

    @AuraEnabled(cacheable=true)
    public static List<SObject> getStateDetails() {
        try {
            return CommonUtils.getStateDetails();
        } catch (Exception ex) {
            //To Log into object
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getStateDetails', 'State List Types', ex);
            //To return back to the UI
            CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('State Details Fetch Issue,Please Try Again..!', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }       
    }
}