/**
 * @Author        : Janaswini S
 * @CreatedOn     : June 12 ,2020
 * @Purpose       : Test Methods to unit test providernarrativeHistory class
 * @Updated By    : Sindhu
*  @UpdatedOn     :  20 july,2020
 **/
@isTest
public with sharing class providernarrativeHistory_Test {
    @isTest static void testprovidernarrativeHistory() {

        Application__c InsertobjNewApplication=new Application__c(Status__c = 'Pending'); 
        insert InsertobjNewApplication;

        HistoryTracking__c InsertobjNewHistoryTracking=new HistoryTracking__c(); 
        insert InsertobjNewHistoryTracking;

        string applicationId= providernarrativeHistory.updateOrInsertSOQL(InsertobjNewApplication);
        try{
            providernarrativeHistory.fetchDataForNarrative(null);
          }catch(Exception Ex){

            }
        providernarrativeHistory.fetchDataForNarrative(InsertobjNewApplication.Id);
        try{
            providernarrativeHistory.fieldTrack(null);
          }catch(Exception Ex){

            }
        providernarrativeHistory.fieldTrack('');
    }
   
}
