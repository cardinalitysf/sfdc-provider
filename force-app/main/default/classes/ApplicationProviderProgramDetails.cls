/**
 * @Author        : Sindhu Venkateswarlu
 * @CreatedOn     : March 17,2020
 * @Purpose       :Upsert and Fetching ApplicationProviderProgramDetails Details
 **/
public with sharing class ApplicationProviderProgramDetails {
    
    @AuraEnabled(cacheable =true)
    public static List<DMLOperationsHandler.FetchValueWrapper> getPickListValues(sObject objInfo, string picklistFieldApi) {
        return DMLOperationsHandler.fetchPickListValue(objInfo,picklistFieldApi);
    }
    @AuraEnabled
	public static string updateOrInsertSOQL(sObject objSobjecttoUpdateOrInsert) {
		//This method is exposed to front end and reponsile for upsert operation
		//sObject is passed to updateOrInsertQuery where real data base insert or update will happen
		return DMLOperationsHandler.DMLupdateOrInsertQuery(objSobjecttoUpdateOrInsert);
    }
    @AuraEnabled
	public static string updateOrInsertSOQLnew(sObject objSobjecttoUpdateOrInsert) {
		//This method is exposed to front end and reponsile for upsert operation
		//sObject is passed to updateOrInsertQuery where real data base insert or update will happen
		return DMLOperationsHandler.DMLupdateOrInsertQuery(objSobjecttoUpdateOrInsert);
	}
   

    @AuraEnabled
    public  static List<Application__c> fetchDataForProgramDetails(string fetchdataname) {
        String model = 'Application__c';
        List<Application__c> query = [SELECT Id,Provider__r.Name,Provider__r.Profit__c,Provider__r.TaxId__c,Provider__r.Accrediation__c,Gender__c,Capacity__c,MinAge__c,MaxAge__c,MinIQ__c,MaxIQ__c,Narrative__c,Program__c,ProgramType__c FROM Application__c where Id=:fetchdataname];
        String cond = 'Id = \'' + fetchdataname + '\'';
       return query;
    }
    @AuraEnabled(cacheable=true)
    public static List<SObject> getAccreditationValues() {
        String model = 'ReferenceValue__c';
        String fields = 'RefValue__c';
        String cond = 'Domain__c = \'Accreditation\'';

        return DMLOperationsHandler.selectSOQLWithConditionParameters (model, fields, cond);
    }

    //This clas used to freeze the provider application in Program Details Screen
    @AuraEnabled(cacheable = true)
    public static List<SObject> getProviderApplicationStatus (String applicationId) {
        String model = 'Application__c';
        String fields = 'Id, Status__c';
        String cond ='Id = \''+ applicationId +'\'';

        return DMLOperationsHandler.selectSOQLWithConditionParameters (model, fields, cond);
    }
}