/**
 * @Author        : Pratheeba.V
 * @CreatedOn     : May 7 ,2020
 * @Purpose       : Test Methods to unit test applicationStaff class
 **/
@IsTest
private class applicationStaff_Test {
    public applicationStaff_Test() {}
    // Testing the Positive flow
    @IsTest static void testapplicationStaffPositive()
    {
        Application__c InsertobjNewApplication=new Application__c(Status__c = 'Pending'); 
        insert InsertobjNewApplication;
        Account InsertobjNewAccount=new Account(Name = 'Test Account'); 
        insert InsertobjNewAccount;
        applicationStaff.getStaffFromProviderID(InsertobjNewAccount.Id);
       
        try{
             applicationStaff.getStaffFromProviderID(null); 
        }catch (Exception Ex) {}
         applicationStaff.getStaffFromProviderID(InsertobjNewAccount.Id);

         try{
            applicationStaff.getStaffFromApplicationId(null);
        }catch (Exception Ex) {}
         applicationStaff.getStaffFromApplicationId(InsertobjNewApplication.Id);
    }

    // Testing the Exception flow
    @isTest static void testapplicationStaffException()
    {   
        applicationStaff.getStaffFromProviderID('');
        applicationStaff.getStaffFromApplicationId('');        
    }
}