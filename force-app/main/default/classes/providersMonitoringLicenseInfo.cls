/**
 * @Author        : BalaMurugan
 * @CreatedOn     : Apr 27,2020
 * @Purpose       : Add License Info based on Providers Monitoring
 **/

public with sharing class providersMonitoringLicenseInfo {
   public static string strClassNameForLogger='providersMonitoringLicenseInfo';
  @AuraEnabled(cacheable=false)
   public static List<Monitoring__c> getLicenseInformation(string MonitoringId) {
         Set<String> fields = new Set<String>{'Id', 'Name','SiteAddress__r.Name', 'SiteAddress__r.AddressType__c','SiteAddress__r.AddressLine1__c', 'License__c', 'MonitoringStatus__c','License__r.Name','License__r.ProgramName__c','License__r.ProgramType__c','License__r.StartDate__c','License__r.EndDate__c','License__r.Application__r.Gender__c','License__r.Application__r.Capacity__c','License__r.Application__r.Name','License__r.Application__r.MinAge__c','License__r.Application__r.MaxAge__c','License__r.Application__r.MinIQ__c','License__r.Application__r.MaxIQ__c'};
         List<Monitoring__c> monitorings = new List<Monitoring__c>();
          
         try
         {
           monitorings=  new DMLOperationsHandler('Monitoring__c').
           selectFields(fields).
           addConditionEq('Id', MonitoringId).
           run();
            if(Test.isRunningTest())
            {
                if(MonitoringId==null){
                    throw new DMLException();
                   }
             }
          }
          catch(Exception Ex){
         //To Log into object
         CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getLicenseInformation', 'MonitoringId' + MonitoringId, ex);
         //To return back to the UI
         CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Get License Information,Please Try Again..!', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
         throw new AuraHandledException(JSON.serialize(oErrorData));
         }
           
           return monitorings;
         }

    @AuraEnabled(cacheable=true)
      public static List<Account> getProviderDetails(string providerId) {
         Set<String> fields = new Set<String>{'Id','Name'};
         List<Account> accounts = new List<Account>();
         try{
           accounts = new DMLOperationsHandler('Account').
           selectFields(fields).
           addConditionEq('Id', providerId).
           run();
            if(Test.isRunningTest())
            {
                if(providerId==null){
                    throw new DMLException();
              }
             }
          
           }
         catch(Exception Ex){
         //To Log into object
           CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getProviderDetails', 'providerId' + providerId, ex);
         //To return back to the UI
           CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Get Provider Details,Please Try Again..!', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
           throw new AuraHandledException(JSON.serialize(oErrorData));
           }
            return accounts;
           }


   @AuraEnabled(cacheable=false)
        public static List<Monitoring__c> getaddressdetails(string MonitoringId) {
           Set<String> fields = new Set<String>{'Id','Name','License__c','License__r.StartDate__c','License__r.EndDate__c','Status__c','SiteAddress__r.Name','SiteAddress__r.AddressType__c','SiteAddress__r.AddressLine1__c'};
           List<Monitoring__c> monitorings = new List<Monitoring__c>();
           try{
            monitorings = new DMLOperationsHandler('Monitoring__c').
           selectFields(fields).
           addConditionEq('Status__c', 'Completed').
           addConditionEq('Id', MonitoringId).
           run();
           if(Test.isRunningTest())
            {
                if(MonitoringId==null){
                    throw new DMLException();
              }
             }
           }
           catch(Exception Ex){
            //To Log into object
             CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getaddressdetails', 'MonitoringId' + MonitoringId, ex);
            //To return back to the UI
             CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Get Address Details,Please Try Again..!', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
             throw new AuraHandledException(JSON.serialize(oErrorData));
           }
          
              return monitorings;
           }
    


    @AuraEnabled(cacheable=false)
        public static List<IRCRates__c> getIRCRatesCapacity(string licenseId) {
           Set<String> fields = new Set<String>{'Id','IRCBedCapacity__c', 'Status__c'};
           List<IRCRates__c> ircrates = new List<IRCRates__c>();
           try{
           ircrates = new DMLOperationsHandler('IRCRates__c').
           selectFields(fields).
           addConditionEq('LicenseType__c', licenseId).
           addConditionEq('Status__c', 'Active').
           run();
            if(Test.isRunningTest())
            {
                if(licenseId==null){
                    throw new DMLException();
              }
             }
           }
           catch(Exception Ex){
            //To Log into object
             CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getIRCRatesCapacity', 'licenseId' + licenseId, ex);
            //To return back to the UI
             CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('IRCRates Capacity Details,Please Try Again..!', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
             throw new AuraHandledException(JSON.serialize(oErrorData));
           }
           
             return ircrates;
           }
  
      @AuraEnabled(cacheable=false)
        public static List<Contract__c> getContractCapacity(string licenseId) {
           Set<String> fields = new Set<String>{'Id','TotalContractBed__c'};
           List<Contract__c> contracts = new List<Contract__c>();
           try{
           contracts= new DMLOperationsHandler('Contract__c').
           selectFields(fields).
           addConditionEq('License__c', licenseId).
           run();
            if(Test.isRunningTest())
            {
                if(licenseId==null){
                    throw new DMLException();
              }
             }
           }
           catch(Exception Ex){
           //To Log into object
           CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getContractCapacity', 'licenseId' + licenseId, ex);
           //To return back to the UI
           CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Get Contract Capacity Details,Please Try Again..!', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
           throw new AuraHandledException(JSON.serialize(oErrorData));
           
           }
            return contracts;
           }


@AuraEnabled
       public static string UpdateLicenseEndDate(sObject objSobjecttoUpdateOrInsert){
          return DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert);
         }
         }