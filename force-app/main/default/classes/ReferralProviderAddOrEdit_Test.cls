/**
 * @Author        : Sindhu Venkateswarlu
 * @CreatedOn     : June 12 ,2020
 * @Purpose       :  Test Methods to unit test ReferralproviderAddOrEdit class in ReferralproviderAddOrEditTest.cls
 * @Updated on    :June 20, 2020
 * @Updated by    :Janaswini Unit Test Updated methods with try catch
 **/
 @isTest
public class ReferralProviderAddOrEdit_Test {
     @isTest static void testgetDataByCaseOrProvider (){
     Case cs = new Case(Status ='status');
     insert cs;
     ReferenceValue__c refv = new ReferenceValue__c();
     insert refv;  
     Account InsertobjNewAccount=new Account(Name = 'Test Account'); 
        insert InsertobjNewAccount;
     try
        {
          ReferralProviderAddOrEdit.getDataByCaseOrProvider(null,null);
        }
        
        catch(Exception ex)
           {
               
           } 
     ReferralProviderAddOrEdit.getDataByCaseOrProvider(InsertobjNewAccount.Id,'');
     ReferralProviderAddOrEdit.getDataByCaseOrProvider(cs.Id,'');
          ReferralProviderAddOrEdit.getDataByCaseOrProvider(cs.Id,'case');
     string caseId= ReferralProviderAddOrEdit.updateOrInsertSOQL(cs);
  	try{
                List<DMLOperationsHandler.FetchValueWrapper>objgetProfitValues = ReferralProviderAddOrEdit.getPickListValues(cs,'abc');
              }
             catch(Exception e){
                       }
     string ReturnId= ReferralProviderAddOrEdit.updateOrInsertSOQLReturnId(cs);
     ReferralProviderAddOrEdit.getStateDetails();
   
     ReferralProviderAddOrEdit.getAccreditationValues();
    
     ReferralProviderAddOrEdit.getProgramOptions();
    
     ReferralProviderAddOrEdit.getProgramTypeOptions();
     try
        {
          ReferralProviderAddOrEdit.getReferralProviderStatus(null);
        }
        
        catch(Exception ex)
           {
               
           } 
     ReferralProviderAddOrEdit.getReferralProviderStatus('');

     }

     @isTest static void testReferralProviderAddOrEditException()
     {   
      ReferralProviderAddOrEdit.getDataByCaseOrProvider('','');
      ReferralProviderAddOrEdit.getReferralProviderStatus('');
      ReferralProviderAddOrEdit.getProgramTypeOptions();
      ReferralProviderAddOrEdit.getAccreditationValues();
     }
    
	
}