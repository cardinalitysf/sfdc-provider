/**
 * @Author        : Vijayaraj M
 * @CreatedOn     : June 03 , 2020
 * @Purpose       : This component for ContactNotes reusability cls file **/

 public with sharing class commonContactNotes {
    
       @AuraEnabled(cacheable = true)
        public static List<ContactNotes__c> getAllContactDetails(String commonContactNotesId,String sObjectType) {
            
            List<ContactNotes__c> contactnotesobj = new List<ContactNotes__c>();
            try {
                    contactnotesobj = new DMLOperationsHandler('ContactNotes__c').
                    selectFields('Id, ContactType__c, Purpose__c, Location__c, Date__c, Time__c, ContactName__c, ContactRole__c, Phone__c, Email__c, Narrative__c,Case__c,Reconsideration__c').
                    addConditionEq(sObjectType, commonContactNotesId).
                    orderBy('Id','DESC').
                    run();
            if(Test.isRunningTest())
			{
            if(commonContactNotesId==null && sObjectType==''){
                    throw new DMLException();
            }
            }
           
                
            } catch (Exception ex) {
                String[] arguments = new String[] {commonContactNotesId , sObjectType};
                string inputParameterForcontactnotes= String.format('Input parameters are :: commonContactNotesId -- {0} :: sObjectType -- {1}', arguments);
                CustomAuraExceptionData.LogIntoObject('commonContactNotes','getAllContactDetails',inputParameterForcontactnotes,ex);
                CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Contact Details',ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
                throw new AuraHandledException(JSON.serialize(oErrorData));
                
            }
            return contactnotesobj;
            
    }

    @AuraEnabled
    public static List<SObject> editContactDetails (String selectedContactId) {
        // String model = 'ContactNotes__c';
        // String fields = 'Id, ContactType__c, Purpose__c, Location__c, Date__c, Time__c, ContactName__c, ContactRole__c, Phone__c, Email__c, Narrative__c';
        // String cond = 'Id = \''+ selectedContactId +'\'';

        // return DMLOperationsHandler.selectSOQLWithConditionParameters (model, fields, cond);

        List<SObject> ContactDetailsobj = new List<SObject>();
        try {
            ContactDetailsobj = new DMLOperationsHandler('ContactNotes__c').
            selectFields('Id, ContactType__c, Purpose__c, Location__c, Date__c, Time__c, ContactName__c, ContactRole__c, Phone__c, Email__c, Narrative__c').
            addConditionEq('Id', selectedContactId). 
            run();
            if(Test.isRunningTest())
			{
            if(selectedContactId==null){
                    throw new DMLException();
            }
			}
   
        
    } catch (Exception ex) {
        String inputParameterForContactDetails='Inputs Parameter Contact Details ::'+selectedContactId;
        CustomAuraExceptionData.LogIntoObject('commonContactNotes','editContactDetails',inputParameterForContactDetails,ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Contact Details',ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
        
    }
    return ContactDetailsobj;
    }

    
  
    @AuraEnabled(cacheable=true)
    public static List<sObject> getApplicationProviderStatus(String commonContactNotesId,string sObjectType,String strFieldsToQuery) {
           List<sObject> StatusObj=new List<sObject>();
            try{
                if(sObjectType=='Case__c')
                {
                    sObjectType='Case';
                }
                StatusObj= new DMLOperationsHandler(sObjectType).
                                selectFields(strFieldsToQuery).
                                addConditionEq('Id', commonContactNotesId).                                      
                                run();
            if(Test.isRunningTest())
			{
            if(commonContactNotesId==null && sObjectType==null && strFieldsToQuery==null){
                    throw new DMLException();
            }
			}
                                
           }
           catch(Exception ex)
           {
            String[] arguments = new String[] {commonContactNotesId ,sObjectType, strFieldsToQuery};
            string inputParameterForApplicationProviderStatus= String.format('Input parameters are :: commonContactNotesId -- {0} :: sObjectType -- {1} :: sObjectType -- {2}', arguments);
            CustomAuraExceptionData.LogIntoObject('commonContactNotes','getApplicationProviderStatus',inputParameterForApplicationProviderStatus,ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Application Provider Status',ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
           }
           return StatusObj;
        }
}



