/**
 * @Author        : Murugan M
 * @CreatedOn     : August 3, 2020
 * @Purpose       : CPA Home Dashboard details
**/
public with sharing class cpaHomeDashboard {
    public static string strClassNameForLogger='cpaHomeDashboard';

    @AuraEnabled(cacheable=true)
    public static List<Application__c> getProgramTypeList(String ProviderId) {
        List<Application__c> OBJ = new List<Application__c>();
        try {
            Set<String> fields = new Set<String>{'Id','ProgramType__c'};
            OBJ = new DMLOperationsHandler('Application__c').
            selectFields(fields).
            addConditionEq('Provider__c', ProviderId).
            addConditionEq('Status__c', 'Approved').
            addConditionEq('Program__c', 'CPA').
            run();
            if(Test.isRunningTest())
            {
                if(ProviderId==null){
                    throw new DMLException();
                }
                
            }      
                    
        } catch (Exception Ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getProgramTypeList',ProviderId,Ex);

        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Program Type Error',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
	    throw new AuraHandledException(JSON.serialize(oErrorData));            
        }
    return OBJ;
    }

// get getCPAHomeDetails

    @AuraEnabled(cacheable=false)
    public static List<CPAHome__c> getCPAHomeDetails(String ProviderId, String ProgramType) {
        List<CPAHome__c> OBJ =new List<CPAHome__c>();
        try {

            Set<String> fields = new Set<String>{'Id','Name', 'Address__r.AddressLine1__c', 'StartDate__c', 'EndDate__c', 'Status__c', 'ClearanceMedicalStatus__c', 'Dateofsupensionlettersent__c', 'SuspenstionStartDate__c','ProgramType__c', 'SuspensionEndDate__c', 'SuspensionComments__c'};
            if(ProgramType !='All'){
                OBJ = new DMLOperationsHandler('CPAHome__c').
                selectFields(fields).
                addSubquery(
                        DMLOperationsHandler.subquery('Actors__r').
                        selectFields('Id, Contact__r.LastName,Role__c').
                orderBy('Role__c', 'ASC')
                ).
                addConditionEq('Provider__c', ProviderId).
                addConditionEq('ProgramType__c', ProgramType).
                orderBy('CreatedDate', 'DESC').
                run();
            }
            else {
                OBJ = new DMLOperationsHandler('CPAHome__c').
                selectFields(fields).
                addSubquery(
                        DMLOperationsHandler.subquery('Actors__r').
                        selectFields('Id, Contact__r.LastName,Role__c').
                orderBy('Role__c', 'ASC')
                ).
                addConditionEq('Provider__c', ProviderId).
                orderBy('CreatedDate', 'DESC').
                run();                
            }
            if(Test.isRunningTest())
            {
                if(ProviderId==null || ProgramType==null){
                    throw new DMLException();
                }
                
            }      
        } catch (Exception Ex) {
            String[] arguments = new String[] {ProviderId , ProgramType};
            string strInputRecord= String.format('Input parameters are :: ProviderId -- {0} :: ProgramType -- {1}', arguments);
    
            //To Log into object
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getCPAHomeDetails',strInputRecord,Ex);
    
            //To return back to the UI
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get CPA Home Detail Error',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));            
        }

    return OBJ;
    }

    
// Update CPA Home page
    @AuraEnabled
   public static string UpdateCPAHome(sObject objSobjecttoUpdateOrInsert){
       return DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert);
   }

    
}
