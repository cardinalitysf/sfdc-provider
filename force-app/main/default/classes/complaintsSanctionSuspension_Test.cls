/**
 * @Author        : Nandhini 
 * @CreatedOn     : JULY 23 ,2020
 * @Purpose       : Sanctions Suspension Screen
 **/
@isTest
private with sharing class complaintsSanctionSuspension_Test {
    @isTest static void testcomplaintsSanction() {

        Sanctions__c insertsanction = new Sanctions__c();
        insert insertsanction;
        Case insertcase = new Case();
        insert insertcase;
        try
        {
            complaintsSanctionSuspension.getSelectedSanctionData(null);
        }
        catch(Exception Ex)
        {
        }               
        complaintsSanctionSuspension.getSelectedSanctionData(insertsanction.id);    
        

        try
        {
            complaintsSanctionSuspension.getRecordType('');
        }
        catch(Exception Ex)
        {
            
        } 
        complaintsSanctionSuspension.getRecordType('Suspension');
        try
        {
            complaintsSanctionSuspension.getComplaintLicenseDetails(null);
        }
        catch(Exception Ex)
        {
            
        } 
        complaintsSanctionSuspension.getComplaintLicenseDetails(insertcase.id);
    }
}