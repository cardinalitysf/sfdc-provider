public with sharing class StaffTraining {
     
        @AuraEnabled(cacheable=true)
        public static List<DMLOperationsHandler.FetchValueWrapper> getPickListValues(sObject objInfo, string picklistFieldApi) {
            return DMLOperationsHandler.fetchPickListValue(objInfo,picklistFieldApi);
       
    }

   @AuraEnabled
   public static string updateOrInsertSOQL(sObject objSobjecttoUpdateOrInsert) {
       //This method is exposed to front end and reponsile for upsert operation
       //sObject is passed to updateOrInsertQuery where real data base insert or update will happen
       return DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert);
   }

        @AuraEnabled(cacheable=false)
        public static List<SObject> gettrainingDetails(string trainingdetails) {
            String queryFields;
            List<sObject> trainingObj = new List<sObject>();
            try {
                 queryFields = 'Id, Name, TrainingName__c, TrainingHours__c, CompletionDate__c';
                 trainingObj = new DMLOperationsHandler('Training__c').
                                        selectFields(queryFields).
                                        addConditionEq('Staff__c', trainingdetails).
                                        orderBy('Name', 'Desc').
                                        run();
            if(Test.isRunningTest())
			{
            if(trainingdetails==''){
                    throw new DMLException();
            }
			}
            } catch (Exception ex) {
                String inputParameterFortrainingDetails='Inputs Parameter Training Details ::'+trainingdetails;
                CustomAuraExceptionData.LogIntoObject('StaffTraining','gettrainingDetails',inputParameterFortrainingDetails,ex);
                CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Training Details',ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
                throw new AuraHandledException(JSON.serialize(oErrorData));
                
            }
            return trainingObj;
            
        }


        @AuraEnabled(cacheable=false)
        public static List<sObject> edittrainingDetails(string edittrainingdetails) {
              String queryFields;
              List<sObject> edittrainingObj = new List<sObject>();
            try {
                 queryFields = 'Id, TrainingName__c, TrainingHours__c, CompletionDate__c';
                 edittrainingObj = new DMLOperationsHandler('Training__c').
                                        selectFields(queryFields).
                                        addConditionEq('Id', edittrainingdetails).
                                        run();
            if(Test.isRunningTest())
			{
            if(edittrainingdetails==null){
                    throw new DMLException();
            }
			}
            } catch (Exception ex) {
                String inputParameterForedittrainingDetails='Inputs Parameter Training Details Edit ::'+edittrainingdetails;
                CustomAuraExceptionData.LogIntoObject('StaffTraining','edittrainingDetails',inputParameterForedittrainingDetails,ex);
                CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Training Details Edit',ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
                throw new AuraHandledException(JSON.serialize(oErrorData));
                
            }
            return edittrainingObj;
            
        }

        @AuraEnabled
        public static string deletetrainingDetails(Id deletetrainingdetails){
            return DMLOperationsHandler.deleteRecordById(deletetrainingdetails);
        }

        @AuraEnabled(cacheable=true)
    public static Id getRecordType(String name) {

        return CommonUtils.getRecordTypeIdbyName('Training__c', name);
    
    
    }


}