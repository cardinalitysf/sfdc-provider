/** 
*    Author : Balamurugan B
*   Modified On : July 02, 2020
**/


public with sharing class ComplaintsSummary {
public static string strClassNameForLogger='ComplaintsSummary';
     //This class used in Case Summary
    @AuraEnabled(cacheable=true)
     public static List<Case> getNarrativeDetails(String applicationId) {
    List<sObject> cases= new List<sObject>();
    String queryFields = 'Id, Summary__c';
    try {
	    cases = new DMLOperationsHandler('Case').
	    selectFields(queryFields).
	    addConditionEq('Id', applicationId).
	    run();
       if(Test.isRunningTest())
			{
                if(applicationid==null){
                    throw new DMLException();
                }
				
			}
    } catch (Exception Ex) {
     CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getNarrativeDetails',applicationId,Ex);
	  CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Case based summary',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
	  throw new AuraHandledException(JSON.serialize(oErrorData));
	}
    return cases;
     }

    //This class used in Case Summary
    @AuraEnabled
    public static string updateNarrativeDetails(sObject objSobjecttoUpdateOrInsert){
        return JSON.serialize(DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert));
    }




    //This class used in History Tracking Summary
     @AuraEnabled(cacheable=true)
       public static List<HistoryTracking__c> getHistoryOfNarrativeDetails(String applicationId) {
    List<sObject> histtrack= new List<sObject>();
    String queryFields = 'Id, RichOldValue__c, RichNewValue__c, LastModifiedDate, LastModifiedBy.Name';
    try {
            histtrack = new DMLOperationsHandler('HistoryTracking__c').
            selectFields(queryFields).
            addConditionEq('ObjectRecordId__c', applicationId).
            addConditionEq('HistoryName__c', 'Summary').
            orderBy('Id','DESC').
            run();
             if(Test.isRunningTest())
			{
                if(applicationid==null){
                    throw new DMLException();
                }
				
			}
		} catch (Exception Ex) {
      CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getHistoryOfNarrativeDetails',applicationId,Ex);
	  CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error on history tracking',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
	  throw new AuraHandledException(JSON.serialize(oErrorData));
	    }	
			
            return histtrack;
       }

   

    //This class used in History Tracking on particular Summary
     @AuraEnabled(cacheable=true)
      public static List<HistoryTracking__c> getSelectedHistoryRec(String recId) {
    List<sObject> histtrack= new List<sObject>();
    String queryFields = 'Id, RichOldValue__c, RichNewValue__c, LastModifiedDate, LastModifiedBy.Name';
    try {
             histtrack = new DMLOperationsHandler('HistoryTracking__c').
            selectFields(queryFields).
            addConditionEq('Id', recId).
            orderBy('Id','DESC').
            run();
             if(Test.isRunningTest())
			{
                if(recId==null){
                    throw new DMLException();
                }
				
			}
			} catch (Exception Ex) {
      CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getSelectedHistoryRec',recId,Ex);
	  CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error on Selected History tracking',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
	  throw new AuraHandledException(JSON.serialize(oErrorData));
	    }	
			
            return histtrack;
      }

    //This class used in Case Summary
    @AuraEnabled(cacheable=true)
     public static List<Case> getPublicProviderStatus(String applicationId) {
    List<sObject> cases= new List<sObject>();
    String queryFields = 'Id,Summary__c, Status';
    try {
             cases = new DMLOperationsHandler('Case').
            selectFields(queryFields).
            addConditionEq('Id', applicationId).
            run();
              if(Test.isRunningTest())
			{
                if(applicationid==null){
                    throw new DMLException();
                }
				
			}
			} catch (Exception Ex) {
      CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getPublicProviderStatus',applicationId,Ex);
	  CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error on Status',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
	  throw new AuraHandledException(JSON.serialize(oErrorData));
	    }	
            return cases;
     }

     @AuraEnabled(cacheable=true)
    public static Id getRecordType(String name) {
        return CommonUtils.getRecordTypeIdbyName('Case', name);
    }
  
    }

