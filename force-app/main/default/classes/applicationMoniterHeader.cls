public with sharing class applicationMoniterHeader {
    @AuraEnabled(cacheable=true)
    public static List<SObject> getMoniteringId(String Id) {
        String model = 'Monitoring__c';
        String fields ='Id, Name'; 
        String cond = 'Id = \''+ Id +'\'';     
    return DMLOperationsHandler.selectSOQLWithConditionParameters(model,fields,cond);
    }
}