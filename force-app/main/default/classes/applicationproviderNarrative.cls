//Sundar Modified apex class naming conventions

public with sharing class applicationproviderNarrative {
    public static string strClassNameForLogger='applicationproviderNarrative';


    //This class used in Provider Narrative & Application Narrative
    @AuraEnabled(cacheable=true)
    public static List<SObject> getNarrativeDetails(string applicationId) {
        List<sObject> getNarrativeDetailsObj= new List<sObject>();
        Set<String> queryFields = new Set<String>{'Id, Narrative__c'};
        try {
            getNarrativeDetailsObj = new DMLOperationsHandler('Application__c').
        selectFields(queryFields).
        addConditionEq('Id', applicationId).
        run();
        if(Test.isRunningTest())
        {
            if(applicationId==null){
                throw new DMLException();
            }
            
        }
        } catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getNarrativeDetails',applicationId,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetching record',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return getNarrativeDetailsObj;
    }


    //This class used in Provider Narrative & Application Narrative
    @AuraEnabled
    public static string updateNarrativeDetails(sObject objSobjecttoUpdateOrInsert){
        return JSON.serialize(DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert));
    }

    //This class used in Application Narrative
    @AuraEnabled(cacheable=true)
    public static List<SObject> getHistoryOfNarrativeDetails(string applicationId) {
        List<sObject> getHistoryDetailsObj= new List<sObject>();
        Set<String> queryFields = new Set<String>{'Id, RichOldValue__c, RichNewValue__c, LastModifiedDate, LastModifiedBy.Name'};
        try {
            getHistoryDetailsObj = new DMLOperationsHandler('HistoryTracking__c').
        selectFields(queryFields).
        addConditionEq('ObjectRecordId__c', applicationId).
        orderBy('Name','desc').
        run();
        if(Test.isRunningTest())
        {
            if(applicationId==null){
                throw new DMLException();
            }
            
        }
        } catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getHistoryOfNarrativeDetails',applicationId,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetching record',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return getHistoryDetailsObj;
    }

    //This class used in Application Narrative
    @AuraEnabled(cacheable=true)
    public static List<SObject> getSelectedHistoryRec(string recId) {
        List<sObject> getSelectedHistoryDetailsObj= new List<sObject>();
        Set<String> queryFields = new Set<String>{'Id, RichOldValue__c, RichNewValue__c, LastModifiedDate, LastModifiedBy.Name'};
        try {
            getSelectedHistoryDetailsObj = new DMLOperationsHandler('HistoryTracking__c').
        selectFields(queryFields).
        addConditionEq('Id', recId).
        orderBy('Id','desc').
        run();
        if(Test.isRunningTest())
        {
            if(recId==null){
                throw new DMLException();
            }
            
        }
        } catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getSelectedHistoryRec',recId,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in record',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return getSelectedHistoryDetailsObj;
    }

    //This class used in Application Narrative
    @AuraEnabled(cacheable=true)
    public static List<SObject> getApplicationProviderStatus(string applicationId) {
        List<sObject> getApplicationDetailsObj= new List<sObject>();
        Set<String> queryFields = new Set<String>{'Id, Status__c'};
        try {
            getApplicationDetailsObj = new DMLOperationsHandler('Application__c').
        selectFields(queryFields).
        addConditionEq('Id', applicationId).
        run();
        if(Test.isRunningTest())
        {
            if(applicationId==null){
                throw new DMLException();
            }
            
        }
        } catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getApplicationProviderStatus',applicationId,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in record',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return getApplicationDetailsObj;
    }

    @AuraEnabled(cacheable=true)
    public static List<SObject> getUserEmail(){
        String emailId = UserInfo.getUserEmail();
        String model = 'User';
        String fields ='Id, Profile.Name'; 
        String cond = 'Email  = \''+ emailId +'\'';
        return DMLOperationsHandler.selectSOQLWithConditionParameters(model,fields,cond);
    }

}

