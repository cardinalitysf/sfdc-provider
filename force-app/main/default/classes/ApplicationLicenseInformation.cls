/**
 * @Author        : B.Balamurugan
 * @CreatedOn     : April 30, 2020
 * @Purpose       : Application License Date Approval generation.
 * @updatedBy     : 
 * @updatedOn     : 
 **/

public with sharing class ApplicationLicenseInformation {
    public static string strClassNameForLogger='ApplicationLicenseInformation';
    @AuraEnabled(cacheable=true)
    public static List<Application__c> getapplicationDetails(string applicationDetails) {
       List<Application__c> lstApplication= new List<Application__c>();
       try{
            lstApplication= new DMLOperationsHandler('Application__c').
            selectFields('Id,Provider__r.Name,Provider__r.BillingStreet,SubmittedDate__c,Provider__r.FirstName__c,Provider__r.LastName__c,Caseworker__r.Name,MinAge__c,MaxAge__c,MinIQ__c,MaxIQ__c,Gender__c,Capacity__c,ProgramType__c,Program__c,LicenseStartdate__c,LicenseEnddate__c').
            addSubquery(
            DMLOperationsHandler.subquery('Address__r').
            selectFields('Id, Name, AddressType__c, AddressLine1__c').
            addConditionEq('AddressType__c', 'Site')
              ).
            addConditionEq('Id', applicationDetails).
            run();
             if(Test.isRunningTest())
			{
                if(applicationDetails==null){
                    throw new DMLException();
                }
				
			}
           }
        catch(Exception Ex){
            //To Log into object
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getapplicationDetails',applicationDetails,Ex);
    
            //To return back to the UI
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Application Details',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
            }
    
             return lstApplication;

            }

    @AuraEnabled(cacheable=true)
    public static string getSignatureAsBas64Image(string appID) {
          String base64ImageAsString;
        try{
            Map<Id, ContentDocumentLink> contentDocumentIdsmap = new Map<Id, ContentDocumentLink>();
            for(ContentDocumentLink conDocu : [SELECT ContentDocumentId, LinkedEntityId,LinkedEntity.Name FROM ContentDocumentLink WHERE LinkedEntityId =:appID  order by SystemModstamp desc LIMIT 1]){
            contentDocumentIdsmap.put(conDocu.ContentDocumentId, conDocu);
            }
            ContentVersion cv = [select id,ContentDocumentId,versiondata from Contentversion where ContentDocumentId In: contentDocumentIdsmap.keyset()  LIMIT 1];
            Blob base64Image =cv.VersionData;
            base64ImageAsString=EncodingUtil.Base64Encode(base64Image);
           
            }
            catch(Exception Ex){
             CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getSignatureAsBas64Image',appID,Ex);
	         CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Signature base64Image',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
             throw new AuraHandledException(JSON.serialize(oErrorData));
            }
           
          
              return base64ImageAsString;
            }
  
    @AuraEnabled
    public static string UpdateLicenseEndDate(sObject objSobjecttoUpdateOrInsert){   
        return DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert);
    }
      
    @AuraEnabled(cacheable=true)
    public static List<Application__c> getLicensesId(string applicationID) {
          List<Application__c> lstApplication= new   List<Application__c>();
          try{
                lstApplication= new DMLOperationsHandler('Application__c').
                selectFields('Name, Id,ApprovalStatus__c,LicenseStartdate__c,LicenseEnddate__c, Status__c').
                addSubquery(
                DMLOperationsHandler.subquery('Licences__r').
                selectFields('Id, Name')
                ).
                addConditionEq('Id', applicationID).
               run();
                if(Test.isRunningTest())
			     {
                if(applicationID==null){
                    throw new DMLException();
                }
				
			}
            }
           catch(Exception Ex){
            //To Log into object
                CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getLicensesId',applicationID,Ex);
    
            //To return back to the UI
                CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get License id',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
                throw new AuraHandledException(JSON.serialize(oErrorData));
            }
        
                return lstApplication;

          }

        }