/**
* @Author        : Pratheeba.V
* @CreatedOn     : May 11 ,2020
* @Purpose       : Test Methods to unit test ProviderLicensesInfo class
  @Updated By    : Naveen S
  @UpdatedDate   : July 24 ,2020
**/

@IsTest
private class ProviderLicensesInfo_Test {
    public ProviderLicensesInfo_Test() {}
    @IsTest static void testProviderLicenseInfoPositive(){
        Application__c InsertobjNewApplication=new Application__c(Status__c = 'Pending'); 
        insert InsertobjNewApplication;
        Monitoring__c InsertobjNewMonitoring=new Monitoring__c(ApplicationLicenseId__c = InsertobjNewApplication.Id); 
        insert InsertobjNewMonitoring;
        Account InsertobjNewAccount=new Account(Name = 'Test Account'); 
        insert InsertobjNewAccount;
        License__c license = new License__c();
        insert license;

        try{
            providerLicensesInfo.getApplicationId(null);
          }
          catch(Exception e){

          } 
          providerLicensesInfo.getApplicationId(InsertobjNewAccount.Id);

          try{
            providerLicensesInfo.getLicensesId(null);
          }
          catch(Exception e){

          } 
          providerLicensesInfo.getLicensesId(InsertobjNewApplication.Id);

          try{
            providerLicensesInfo.getLicenseInformation(null);
          }
          catch(Exception e){

          }      
           providerLicensesInfo.getLicenseInformation(license.Id);
           try{
            providerLicensesInfo.getProviderDetails(null);
          }
          catch(Exception e){

          }
            providerLicensesInfo.getProviderDetails(InsertobjNewAccount.Id);
            try{
                providerLicensesInfo.getaddressdetails(null);
              }
              catch(Exception e){
    
              }   
             providerLicensesInfo.getaddressdetails(InsertobjNewApplication.Id);
             try{
                providerLicensesInfo.getMonitoringDetails(null);
              }
              catch(Exception e){
    
              }      
        providerLicensesInfo.getMonitoringDetails(InsertobjNewApplication.Id);
        try{
            providerLicensesInfo.getIRCRatesCapacity(null);
          }
          catch(Exception e){

          } 
        providerLicensesInfo.getIRCRatesCapacity(license.Id);
        try{
            providerLicensesInfo.getContractCapacity(null);
          }
          catch(Exception e){

          } 
        providerLicensesInfo.getContractCapacity(license.Id);
    }

    @IsTest static void testProviderLicenseInfoException(){
        providerLicensesInfo.getApplicationId('');
        providerLicensesInfo.getLicensesId('');
        providerLicensesInfo.getLicenseInformation('');
        providerLicensesInfo.getProviderDetails('');
        providerLicensesInfo.getaddressdetails('');
        providerLicensesInfo.getMonitoringDetails('');
        providerLicensesInfo.getIRCRatesCapacity('');
        providerLicensesInfo.getContractCapacity('');
    }
}