/**
     * @Author        : Jayachandran s
     * @CreatedOn     : July 28,2020
     * @Purpose       : Test class for ComplaintsDashboard Case Object(Activity Sobj).
**/
@isTest
private with sharing class ComplaintsDashboard_Test {
     @IsTest static void TestComplaintsDashboard(){
        Case InsertobjNewCase1= new Case(Status = 'Draft');
        insert InsertobjNewCase1;
        Case InsertobjNewCase2= new Case(Status = 'Submitted to Caseworker');
        insert InsertobjNewCase2;
//Public Method
        try {    
            ComplaintsDashboard.getComplaintsPrivateList(null,null,null);
        } catch (Exception Ex) {
            
        }
        ComplaintsDashboard.getComplaintsPrivateList(InsertobjNewCase1.Id,'Private','Intake');
        ComplaintsDashboard.getComplaintsPrivateList(InsertobjNewCase2.Id,'Private','Caseworker');
//Private Method
        try {
            ComplaintsDashboard.getPublicComplaintList(null,null,null);
   
              } catch (Exception Ex) {
       
           }
           ComplaintsDashboard.getPublicComplaintList('C-0001280','Public','Intake');
//Status List
        ComplaintsDashboard.statusForProviders('total','Caseworker');
        ComplaintsDashboard.statusForProviders('pending','Supervisor');
        ComplaintsDashboard.statusForProviders('pending','Caseworker');
        ComplaintsDashboard.statusForProviders('total','Supervisor');
        ComplaintsDashboard.statusForProviders('pending','System Administrator');
        ComplaintsDashboard.statusForProviders('total','System Administrator');
    }
//Count Of List Testclass.
@isTest
    static void CountsTest() {
       
        Account Acc1 = new Account(ProviderType__c='Public' ,Name='Name');
        insert Acc1;
        Case CountCase1= new Case(Status = 'Draft');
        Id MyId2=Schema.Sobjecttype.Case.getRecordTypeInfosByName().get('Complaints').getRecordTypeId();
        CountCase1.RecordTypeId = MyId2;
        CountCase1.AccountId=Acc1.Id;
        insert CountCase1;
        ComplaintsDashboard.getComplaintsCount('','Public','Intake');

        CountCase1.Status='Approved';
        update CountCase1;
        ComplaintsDashboard.getComplaintsCount('','Public','Intake');
        CountCase1.Status='Rejected';
        update CountCase1;
        ComplaintsDashboard.getComplaintsCount('','Public','Intake');
        CountCase1.Status='Submitted to Caseworker';
        update CountCase1;
        ComplaintsDashboard.getComplaintsCount('','Public','Intake');
        CountCase1.Status='Submitted to Supervisor';
        update CountCase1;
        ComplaintsDashboard.getComplaintsCount('','Public','Intake');
        CountCase1.Status='Awaiting Confirmation';
        update CountCase1;
        ComplaintsDashboard.getComplaintsCount('','Public','Intake');
        CountCase1.Status='Accepted';
        update CountCase1;
        ComplaintsDashboard.getComplaintsCount('','Public','Intake');
        CountCase1.Status='Disputed';
        update CountCase1;
        ComplaintsDashboard.getComplaintsCount('','Public','Intake');
//For Caseworker, Supervisor
        Case CountCase2= new Case(Status = 'Approved');
        insert CountCase2;
        Case CountCase3= new Case(Status = 'Rejected');
        insert CountCase3;
        ComplaintsDashboard.getComplaintsCount(CountCase2.CaseNumber,'Private','Caseworker');
        ComplaintsDashboard.getComplaintsCount(CountCase3.CaseNumber,'Private','Supervisor'); 
    }

    
} 


