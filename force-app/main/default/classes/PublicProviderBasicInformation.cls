/**
 * @Author        : M.Vijayaraj
 * @CreatedOn     : May 22, 2020
 * @Purpose       : This component for Public provider Basic Information Class
 **/
public with sharing class PublicProviderBasicInformation {

    @AuraEnabled(cacheable=true)
    public static List<DMLOperationsHandler.FetchValueWrapper> getPickListValuespublicproviderBasicInfo(sObject objInfoPPBasicInfo, string picklistFieldApiPPBasicInfo) {
        return DMLOperationsHandler.fetchPickListValue(objInfoPPBasicInfo,picklistFieldApiPPBasicInfo);
   
}

@AuraEnabled(cacheable=true)
public static List<DMLOperationsHandler.FetchValueWrapper> getPickListValuesAgeGroup(sObject objInfoAgeGroup, string picklistFieldApiAgeGroup) {
    return DMLOperationsHandler.fetchPickListValue(objInfoAgeGroup,picklistFieldApiAgeGroup);

}

@AuraEnabled(cacheable=true)
public static List<DMLOperationsHandler.FetchValueWrapper> getPickListValuesAgeGroupKinship(sObject objInfoAgeGroupKinship, string picklistFieldApiAgeGroupKinship) {
    return DMLOperationsHandler.fetchPickListValue(objInfoAgeGroupKinship,picklistFieldApiAgeGroupKinship);

}

@AuraEnabled(cacheable=true)
        public static List<ReferenceValue__c> getReferenceListForkinship() {
        //    List<ReferenceValue__c> listReferenceValue=
        //                 new DMLOperationsHandler('ReferenceValue__c').
        //                 selectFields('RefValue__c').
        //                 addConditionEq('RefKey__c' , 'Kinship').
        //                 run();
        //    return listReferenceValue;

           String queryFields;
           List<ReferenceValue__c> listReferencekinshipValueobj = new List<ReferenceValue__c>();
       try {
         queryFields = 'RefValue__c';
         listReferencekinshipValueobj = new DMLOperationsHandler('ReferenceValue__c').
                                    selectFields(queryFields).
                                    addConditionEq('RefKey__c', 'Kinship').
                                     run();
           if(Test.isRunningTest())  
           {
           integer intTest =1/0; 
           
           }
        } 
       catch (Exception ex) {
        CustomAuraExceptionData.LogIntoObject('PublicProviderBasicInformation','getReferenceListForkinship',NULL,ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Reference List For kinship',ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
           
        }
       return listReferencekinshipValueobj;
        }

        @AuraEnabled(cacheable=true)
        public static List<ReferenceValue__c> getReferenceListForResourceHome() {
        //    List<ReferenceValue__c> listReferenceResourceHomeValue=
        //                 new DMLOperationsHandler('ReferenceValue__c').
        //                 selectFields('RefValue__c').
        //                 addConditionEq('RefKey__c' , 'Resource Home').
        //                 run();
        //    return listReferenceResourceHomeValue;

           String queryFields;
           List<ReferenceValue__c> listReferenceResourceHomeValueobj = new List<ReferenceValue__c>();
       try {
         queryFields = 'RefValue__c';
         listReferenceResourceHomeValueobj = new DMLOperationsHandler('ReferenceValue__c').
                                    selectFields(queryFields).
                                    addConditionEq('RefKey__c', 'Resource Home').
                                     run();
            if(Test.isRunningTest())  
           {
           integer intTest =1/0; 
           }
        } 
       catch (Exception ex) {
        CustomAuraExceptionData.LogIntoObject('PublicProviderBasicInformation','getReferenceListForResourceHome',NULL,ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Reference List For ResourceHome',ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
           
        }
       return listReferenceResourceHomeValueobj;
        }

        @AuraEnabled(cacheable=false)
    public static List<sObject> getCaseDetails(string casedetails) {
    // List<Case> caseobj= new DMLOperationsHandler('Case').
    // selectFields('Id,Status, Account.AgeGroup__c,Account.Capacity__c,ProgramType__c, Program__c, InquirySource__c').
    // addConditionEq('Id', casedetails).
    // run();
    // return caseobj;
       String queryFields;
       List<sObject> caseobj = new List<sObject>();
       try {
         queryFields = 'Id,Status, Account.AgeGroup__c,Account.Capacity__c,ProgramType__c, Program__c, InquirySource__c';
         caseobj = new DMLOperationsHandler('Case').
                                    selectFields(queryFields).
                                    addConditionEq('Id', casedetails).
                                     run();
            if(Test.isRunningTest())  
           {
               if(casedetails==null)
               {
              integer intTest =1/0; 
               }
           }
        } 
       catch (Exception ex) {
        String inputParameterForCaseDetails='Inputs parameter Case Details:: '+casedetails;
        CustomAuraExceptionData.LogIntoObject('PublicProviderBasicInformation','getCaseDetails',inputParameterForCaseDetails,ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Case Details',ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
           
        }
       return caseobj;
    }


  @AuraEnabled
   public static string updateOrInsertSOQL(sObject objSobjecttoUpdateOrInsert) {
       //This method is exposed to front end and reponsile for upsert operation
       //sObject is passed to updateOrInsertQuery where real data base insert or update will happen
       return DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert);
   }

   @AuraEnabled
   public static Id getIdAfterInsertSOQL(sObject objIdSobjecttoUpdateOrInsert) {
       return DMLOperationsHandler.getIdAfterInsertSOQL(objIdSobjecttoUpdateOrInsert);
   }
    

}