/**
 * @Author        : Pratheeba.V
 * @CreatedOn     : May 7 ,2020
 * @Purpose       : Test Methods to unit test ApplicationConference class
 **/
@IsTest
private class ApplicationConferenceController_Test {
    public ApplicationConferenceController_Test() {}
    @IsTest static void testApplicationConferencePositive()
    {
        Application__c InsertobjNewApplication=new Application__c(Status__c = 'Pending'); 
        insert InsertobjNewApplication;
        Account InsertobjNewAccount=new Account(Name = 'Test Account'); 
        insert InsertobjNewAccount;
        Monitoring__c InsertobjNewMonitoring=new Monitoring__c(ApplicationLicenseId__c = InsertobjNewApplication.Id); 
        insert InsertobjNewMonitoring; 
        Conference__c InsertobjNewConference=new Conference__c(); 
        insert InsertobjNewConference;
        try {
            ApplicationConferenceController.assingStaffMember(null);
        } catch (Exception Ex) {
            
        }
        ApplicationConferenceController.assingStaffMember(InsertobjNewAccount.Id);
        try {
            ApplicationConferenceController.getStaffFromApplicationID(null);        
        } catch (Exception Ex) {
            
        }
        ApplicationConferenceController.getStaffFromApplicationID(InsertobjNewApplication.Id);
        try {
            ApplicationConferenceController.getAllUsersList();
        } catch (Exception Ex) {
            
        }
        try {
            ApplicationConferenceController.getConferenceDetails(null);
        } catch (Exception Ex) {
            
        }
        ApplicationConferenceController.getConferenceDetails(InsertobjNewMonitoring.Id);
        try{
            List<DMLOperationsHandler.FetchValueWrapper> objApplicationConferencePickList = ApplicationConferenceController.getConferenceType(InsertobjNewConference,'Type__c');
        } catch (Exception e) {
        }
        try {
            ApplicationConferenceController.getConferenceExitDetails(null);
        } catch (Exception Ex) {
            
        }
        ApplicationConferenceController.getConferenceExitDetails(InsertobjNewConference.Id);
        ApplicationConferenceController.InsertUpdateConference(InsertobjNewConference);
       // ApplicationConferenceController.deleteExitConferenceDetails(InsertobjNewConference.Id);
    }

    @isTest static void testgetMonitoringDetailsException()
    {
        ApplicationConferenceController.getConferenceExitDetails('');
        ApplicationConferenceController.assingStaffMember('');
        ApplicationConferenceController.getStaffFromApplicationID(''); 
        ApplicationConferenceController.getConferenceDetails('');
    }
}