/**
 * @Author        : Saranraj
 * @CreatedOn     : Aug 03 ,2020
 * @Purpose       : Incident/law enforcement/FosterParent
 * @updatedBy     :
 * @updatedOn     :
 **/

public with sharing class IncidentFosterParentLawEnforcement {
    public static string strApexDebLogClassName = 'ProgramAddress';
    
    @AuraEnabled(cacheable=true)
    public static List<ReferenceValue__c> incidentPickListValueFromReferenceValue(String selectedIncidentType){
        List<ReferenceValue__c> incidentPickListValueFromReferenceValueData = new List<ReferenceValue__c>();

        try {
            incidentPickListValueFromReferenceValueData = new DMLOperationsHandler('ReferenceValue__c').
            selectFields('Id, RefValue__c').
            addConditionEq('RecordType.Name', 'Picklist').
            addConditionEq('Domain__c', 'Incident').
            addConditionEq('RefKey__c', selectedIncidentType).
            run();
            if(Test.isRunningTest())
            {
            if(selectedIncidentType=='' ){
            throw new DMLException();
            }
    
            }

        } catch(Exception e) {
            CustomAuraExceptionData.LogIntoObject(strApexDebLogClassName,'incidentPickListValueFromReferenceValue', null, e);
            CustomAuraExceptionData errorData=new CustomAuraExceptionData('incidentPickListValueFromReferenceValue Data',e.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(errorData));
        }
        return incidentPickListValueFromReferenceValueData;
    }

    @AuraEnabled(cacheable=true)
    public static List<ReferenceValue__c> getNarrativeQuestionsFromReference() {
         List<ReferenceValue__c> getNarrativeQuestionsFromReferenceData = new List<ReferenceValue__c>();

         try {
            getNarrativeQuestionsFromReferenceData = new DMLOperationsHandler('ReferenceValue__c').
            selectFields('Id, Question__c').
            addConditionEq('RecordType.Name', 'Question').
            addConditionEq('Type__c', 'Incident').
            addConditionEq('RefKey__c', 'Narrative').
            run();
            if(Test.isRunningTest())
            {
            if(true){
            throw new DMLException();
            }
        }
         } catch(Exception e) {
            CustomAuraExceptionData.LogIntoObject(strApexDebLogClassName,'getNarrativeQuestionsFromReference', null, e);
            CustomAuraExceptionData errorData=new CustomAuraExceptionData('getNarrativeQuestionsFromReference Data',e.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(errorData));
         }
         return getNarrativeQuestionsFromReferenceData;
    }

    @AuraEnabled(cacheable=true)
    public static Id getRecordTypeValue(String recordType) {
        return CommonUtils.getRecordTypeIdbyName('Contact', recordType);
    }

    //    @AuraEnabled(cacheable=true)
    //    public static List<sObject> getRecordTypeValue(String recordType) {
    //         List<sObject> getRecordTypeValueData = new List<sObject>();
    //         try {
    //             getRecordTypeValueData = commonUtils.getRecordType(recordType);

    //             // if(Test.isRunningTest()) {
    //             //         if(recordType == '') {
    //             //             throw new DMLException();
    //             //         }
    //             //     }
                    
    //         } catch(Exception e) {
    //             CustomAuraExceptionData.LogIntoObject(strApexDebLogClassName,'getRecordTypeValue', 'recordType' + recordType, e);
    //             CustomAuraExceptionData errorData=new CustomAuraExceptionData('recordType Data',e.getMessage() , CustomAuraExceptionData.type.Informational.name());
    //             throw new AuraHandledException(JSON.serialize(errorData));
    //         }

    //         return getRecordTypeValueData;
    //    }


    @AuraEnabled(cacheable=true)
    public static Id getIncidentRecordTypeValue(String recordType) {
        return CommonUtils.getRecordTypeIdbyName('ProviderRecordQuestion__c', recordType);
    }
    //     @AuraEnabled(cacheable=true)
    //    public static List<sObject> getIncidentRecordTypeValue(String recordType) {
    //         List<sObject> getIncidentRecordTypeValueData = new List<sObject>();
    //         try {
    //             getIncidentRecordTypeValueData = commonUtils.getRecordType(recordType);

    //             if(Test.isRunningTest()) {
    //                     if(recordType == '') {
    //                         throw new DMLException();
    //                     }
    //                 }

    //         } catch(Exception e) {
    //             CustomAuraExceptionData.LogIntoObject(strApexDebLogClassName,'getIncidentRecordTypeValue', 'recordType' + recordType, e);
    //             CustomAuraExceptionData errorData=new CustomAuraExceptionData('getIncidentRecordTypeValue Data',e.getMessage() , CustomAuraExceptionData.type.Informational.name());
    //             throw new AuraHandledException(JSON.serialize(errorData));
    //         }
    //         return getIncidentRecordTypeValueData;
    //    }


       @AuraEnabled(cacheable=false)
       public static List<Case> getReferralData(String referralId) {
               List<Case> getReferralDataForDataTable = new List<Case>();

               try {
                    getReferralDataForDataTable = new DMLOperationsHandler('Case').
                    selectFields('Id, Area__c, Incident__c, IncidentType__c, LocationoftheIncident__c, LevelofSupervision__c, ComplaintReceivedTime__c, IncidentDate__c, ComplaintReceivedDate__c, IncidentTime__c').
                    addConditionEq('Id', referralId).

                    run();

                    if(Test.isRunningTest()) {
                        if(referralId == '') {
                            throw new DMLException();
                        }
                    }
               } catch(Exception e) {
                    CustomAuraExceptionData.LogIntoObject(strApexDebLogClassName,'getReferralData', 'referralId' + referralId, e);
                    CustomAuraExceptionData errorData=new CustomAuraExceptionData('getReferralData Data',e.getMessage() , CustomAuraExceptionData.type.Informational.name());
                    throw new AuraHandledException(JSON.serialize(errorData));
               }
               return getReferralDataForDataTable;
       }

// for insert answers
@AuraEnabled
    public static void insertAnswerToProviderAnswer(String data) {
          List<ProviderRecordAnswer__c> listInsert = new List<ProviderRecordAnswer__c>();
        try{

        List<ProviderRecordAnswer__c> dataToInsert = (List<ProviderRecordAnswer__c>) System.JSON.deserialize(data, List<ProviderRecordAnswer__c>.class);
        for (ProviderRecordAnswer__c p : dataToInsert) {
            ProviderRecordAnswer__c objTask = new ProviderRecordAnswer__c();
            objTask = new ProviderRecordAnswer__c(
                    Comar__c = p.Comar__c,
                    Comments__c = p.Comments__c,
                    ProviderRecordQuestion__c = p.ProviderRecordQuestion__c);
            listInsert.add(objTask);
        }
        }
        catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strApexDebLogClassName,'insertAnswerToProviderAnswer',data,Ex);
         CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Insert Bulk Recored Answer based on Type',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
         throw new AuraHandledException(JSON.serialize(oErrorData));
       }
        insert listInsert;
       }

       @AuraEnabled(cacheable=true)
       public static List<Actor__c> getFosterParentData(String cpahomesId){
        List<Actor__c>  getFosterParent = new List<Actor__c>();

        try {
            getFosterParent = new DMLOperationsHandler('Actor__c').
            selectFields('Id,cpahomes__r.name,Contact__r.Firstname__c,Contact__r.lastname__c,Contact__r.lastname,cpahomes__r.Address__r.AddressLine1__c, cpahomes__r.Address__r.AddressLine2__c, cpahomes__r.Address__r.County__c, cpahomes__r.Address__r.City__c, cpahomes__r.Address__r.ZipCode__c, cpahomes__r.Address__r.State__c, Contact__r.id,').
            addConditionEq('cpahomes__c', cpahomesId).
            run();
             if(Test.isRunningTest()) {
                        if(cpahomesId == null) {
                            throw new DMLException();
                        }
                    }
        } catch(Exception e) {
            CustomAuraExceptionData.LogIntoObject(strApexDebLogClassName,'getFosterParentData',null,e);
         CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('getFosterParentData', e.getMessage() , CustomAuraExceptionData.type.Informational.name());
         throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return getFosterParent;
       }

       @AuraEnabled(cacheable=false)
       public static List<Address__c> getAddressDetail(String addressType, String referralId) {
        List<Address__c> getAddressDetailData = new List<Address__c>();
        try {
            getAddressDetailData = new DMLOperationsHandler('Address__c').
            selectFields('Id, AddressLine1__c, AddressLine2__c, City__c, County__c, State__c, ZipCode__c').
            addConditionEq('AddressType__c', addressType).
            addConditionEq('Incident__c', referralId).
            run();
            if(Test.isRunningTest()) {
                if(referralId == null) {
                    throw new DMLException();
                }
            }
        } catch(Exception e) {  
            CustomAuraExceptionData.LogIntoObject(strApexDebLogClassName,'getAddressDetail', 'addressType' + addressType, e);
            CustomAuraExceptionData errorData=new CustomAuraExceptionData('getAddressDetail Data',e.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(errorData));
        }
        return getAddressDetailData;    
       }

       @AuraEnabled(cacheable=false)
       public static List<Actor__c> fosterParentTableData(String referralId) {
        List<Actor__c> fosterParentTable = new List<Actor__c>();

        try {
            fosterParentTable = new DMLOperationsHandler('Actor__c').
            selectFields('Id, Contact__r.FirstName__c, Contact__r.LastName__c, Contact__r.lastName, Referral__r.Address__r.AddressLine1__c, Referral__r.Address__r.AddressLine2__c, Referral__r.Address__r.State__c, Referral__r.Address__r.City__c, Referral__r.Address__r.County__c, Referral__r.Address__r.ZipCode__c').
            addConditionEq('Referral__c', referralId).
            addConditionEq('Role__c', 'Foster Parent').
            run();
            if(Test.isRunningTest()) {
                if(referralId == null) {
                    throw new DMLException();
                }
            }
        } catch(Exception e) {
            CustomAuraExceptionData.LogIntoObject(strApexDebLogClassName,'fosterParentTableData', 'referralId' + referralId, e);
         CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('fosterParentTableData', e.getMessage() , CustomAuraExceptionData.type.Informational.name());
         throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return fosterParentTable;      
       }

       @AuraEnabled(cacheable=false)
       public static List<Actor__c> getLawEnforcementDataTableData(String referralId) {
        List<Actor__c> getLawEnforcementDataTable = new List<Actor__c>();

        try {
            getLawEnforcementDataTable = new DMLOperationsHandler('Actor__c').
            selectFields('Id, Contact__r.FirstName__c, Contact__r.Id, Contact__r.LastName__c, Contact__r.Phone, Contact__r.ReportNumber__c, Contact__r.LawEnforcementDate__c, Contact__r.LawEnforcementTime__c').
            addConditionEq('Referral__c', referralId).
            addConditionEq('Role__c', 'Law Enforcement').
            run();
            if(Test.isRunningTest()) {
                if(referralId == null) {
                    throw new DMLException();
                }
            }
        } catch(Exception e) {
            CustomAuraExceptionData.LogIntoObject(strApexDebLogClassName,'getLawEnforcementDataTableData', 'referralId' + referralId, e);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('getLawEnforcementDataTableData', e.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return getLawEnforcementDataTable;
       }

       @AuraEnabled(cacheable=false)
       public static List<ProviderRecordAnswer__c> getNarrativeAnswerForEdit(String providerQuestionId) {
        List<ProviderRecordAnswer__c> getNarrativeAnswerForEditData = new List<ProviderRecordAnswer__c>();
      

        try {
            getNarrativeAnswerForEditData = new DMLOperationsHandler('ProviderRecordAnswer__c').
            selectFields('Id, Comar__c, Comments__c').
            addConditionEq('ProviderRecordQuestion__c', providerQuestionId).
            run();
            if(Test.isRunningTest()) {
                if(providerQuestionId == null) {
                    throw new DMLException();
                }
            }

        } catch(Exception e) {
            CustomAuraExceptionData.LogIntoObject(strApexDebLogClassName,'getNarrativeAnswerForEdit', 'providerQuestionId' + providerQuestionId, e);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('getNarrativeAnswerForEdit', e.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));

        }
        return getNarrativeAnswerForEditData;
       }



// update Record 
@AuraEnabled
public static void bulkUpdateRecordAns(String datas){
     List<ProviderRecordAnswer__c> listUpdate = new List<ProviderRecordAnswer__c>();
     try{
     List<ProviderRecordAnswer__c> dataToInsert = (List<ProviderRecordAnswer__c>)System.JSON.deserialize(datas, List<ProviderRecordAnswer__c>.class);
   
     for(ProviderRecordAnswer__c p: dataToInsert){
        ProviderRecordAnswer__c objTask = new ProviderRecordAnswer__c();
        objTask = new ProviderRecordAnswer__c(
          Id = p.Id,
          Comments__c = p.Comments__c,
          ProviderRecordQuestion__c = p.ProviderRecordQuestion__c);
          listUpdate.add(objTask);
        }
              
      } catch (Exception Ex) {
    CustomAuraExceptionData.LogIntoObject(strApexDebLogClassName,'bulkUpdateRecordAns',datas,Ex);
     CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Update Bulk Recored Answer ',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
     throw new AuraHandledException(JSON.serialize(oErrorData));
      }
    update listUpdate;
      }

// update record end

}