/**
     * @Author        : Balamurugan.B
     * @CreatedOn     : June 07,2020
     * @Purpose       : Test Methods to unit test for PublicProviderHouseHoldMembersHome.cls
     * @UpadtedBy     : JayaChandran Unit Test Updated methods with try catch.
     * @Date          : 17 july,2020
     **/

    @isTest
    public class PublicProviderHouseHoldMHome_Test {
             @isTest static void testHouseHoldMembers(){

                Actor__c InsertobjNewHHM=new Actor__c(Role__c = 'test Account');
                  insert InsertobjNewHHM;
                Case InsertobjCase=new Case(Status = 'Pending'); 
                  insert InsertobjCase;
                  List<Contact> contacts = new List<Contact>();
                Contact InsertobjContact = new Contact(LastName = 'Name');
                  insert InsertobjContact;

            try{
                PublicProviderHouseHoldMembersHome.getContactHouseHoldMembersDetails(null);
            }catch(Exception Ex) {

            }
            PublicProviderHouseHoldMembersHome.getContactHouseHoldMembersDetails(InsertobjNewHHM.id);

            try{
                PublicProviderHouseHoldMembersHome.getPublicDecisionStatusDetails(null);
            }catch(Exception Ex) {

            }
            PublicProviderHouseHoldMembersHome.getPublicDecisionStatusDetails(InsertobjCase.id);
            try{
                PublicProviderHouseHoldMembersHome.getContactImgAsBas64Image('');
               }
            catch(Exception Ex){
            
               }
            PublicProviderHouseHoldMembersHome.getContactImgAsBas64Image(InsertobjContact.id);


         }


    }