/**
     * @Author        : Vijayaraj M
     * @CreatedOn     : August 17,2020
     * @Purpose       :Test Methods to unit test for IncidentProgramInfo.cls
     **/
    @isTest
     private with sharing class IncidentProgramInfo_Test {
        @isTest static void testIncidentProgramInfo_Test() {
            Account InsertobjNewAccount=new Account(ProviderType__c='HGF',Name='JHG'); 
            insert InsertobjNewAccount;
            
            Application__c InsertobjApplication=new Application__c(Provider__c = InsertobjNewAccount.Id); 
            insert InsertobjApplication;

            Address__c InsertobjAddress=new Address__c(Application__c = InsertobjApplication.Id,AddressType__c='Site'); 
            insert InsertobjAddress;

            Case InsertobjCase=new Case(Program__c = 'XYZ'); 
            insert InsertobjCase;

            CPAHome__c InsertobjCPA=new CPAHome__c(); 
            insert InsertobjCPA;

 
try
            {IncidentProgramInfo.getProgramDetails('');
            }catch(Exception ex){}
            IncidentProgramInfo.getProgramDetails(InsertobjApplication.Provider__c); 
            
            try
            {IncidentProgramInfo.getProgramTypeDetails('','');
            }catch(Exception ex){}
            IncidentProgramInfo.getProgramTypeDetails(InsertobjNewAccount.Id,'RCC');

            try
            {IncidentProgramInfo.getProgramSiteDetails('');
            }catch(Exception ex){}
            IncidentProgramInfo.getProgramSiteDetails(InsertobjApplication.Id);

            try
            {IncidentProgramInfo.getAgencyDetails('');
            }catch(Exception ex){}
            IncidentProgramInfo.getAgencyDetails(InsertobjApplication.Id);
            
            try
            {IncidentProgramInfo.getIncidentDetails('');
            }catch(Exception ex){}
            IncidentProgramInfo.getIncidentDetails(InsertobjCase.Id);
            
            try
            {IncidentProgramInfo.getProviderDetails('');
            }catch(Exception ex){}
            IncidentProgramInfo.getProviderDetails(InsertobjNewAccount.Id);

            try
            {IncidentProgramInfo.getCPAaddressDetails('','');
            }catch(Exception ex){}
            IncidentProgramInfo.getCPAaddressDetails('Independent Living Program',InsertobjNewAccount.Id);
           

           IncidentProgramInfo.updateOrInsertSOQL(InsertobjCase);

           IncidentProgramInfo.getIdAfterInsertSOQL(InsertobjCase);
           

    }
}
