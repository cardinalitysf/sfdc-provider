public with sharing class PublicProvidersLicenseInfo {
    public static string strClassNameForLogger='PublicProvidersLicenseInfo';
    
    @AuraEnabled(cacheable=false)
    public static List<SObject> getApplicationId(string providerId) {
        try 
        {
        Set<String> fields = new Set<String>{'Id,Name'};
        List<SObject> applications =
        new DMLOperationsHandler('Application__c').
        selectFields(fields).
        addConditionEq('Provider__c', providerId).
        run();
        if(Test.isRunningTest())
        {
        if(providerId==null ){
        throw new DMLException();
        }

        }
        return applications;
    }
    catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getApplicationId',providerId,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Application details based on Provider',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
   }
    }

    @AuraEnabled(cacheable=true)
    public static List<SObject> getHouseholdMembers(string providerid) {
        try{
        Set<String> fields = new Set<String>{'Id,Contact__r.Title__c,Contact__r.FirstName__c, Contact__r.LastName__c,  Role__c, Contact__r.Id,'};
        List<SObject> questions =
        new DMLOperationsHandler('Actor__c').
        selectFields(fields).
        addConditionEq('Provider__c', providerid).   
        addConditionEq('Contact__r.RecordType.Name','Household Members').
        addConditionEq('Role__c','Applicant').
        run();
        if(Test.isRunningTest())
        {
        if(providerid==null ){
        throw new DMLException();
        }

        }
        return questions;
        
    }
    catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getHouseholdMembers',providerid,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get HouseholdMembers details based on Provider',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
   }
    }

    @AuraEnabled(cacheable=true)
    public static List<SObject> getCoApplicantDetails(string providerid) {
        try{
        Set<String> fields = new Set<String>{'Id,Contact__r.Title__c,Contact__r.FirstName__c, Contact__r.LastName__c,  Role__c'};
        List<SObject> questions =
        new DMLOperationsHandler('Actor__c').
        selectFields(fields).
        addConditionEq('Provider__c', providerid).   
        addConditionEq('Contact__r.RecordType.Name','Household Members').
        addConditionEq('Role__c','Co-Applicant').        
        run();
        if(Test.isRunningTest())
        {
        if(providerid==null ){
        throw new DMLException();
        }

        }
        return questions;
        }
        catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getCoApplicantDetails',providerid,Ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get CoApplicantDetails details based on Provider',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
       }
    }
       

    @AuraEnabled(cacheable=false)
    public static List<Application__c> getLicenseInformation(string applicationID) {
        try{
        Set<String> fields = new Set<String>{'Provider__c, Id, Name, Capacity__c, Gender__c, LicenseEnddate__c, LicenseStartdate__c, MaxAge__c, MinAge__c, ProgramType__c, Program__c, Race__c'};
        List<Application__c> applications =
        new DMLOperationsHandler('Application__c').
        selectFields(fields).
        addConditionEq('Id', applicationID).
        run();
        if(Test.isRunningTest())
        {
        if(applicationID==null ){
        throw new DMLException();
        }

        }
        return applications;
        }
        catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getLicenseInformation',applicationID,Ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get License  details based on Application',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
       }
    }          


    @AuraEnabled(cacheable=false)
    public static List<Address__c> fetchDataForAddress(string fetchdataname,string fetchdatatype) {
        try{
    Set<String> fields = new Set<String>{'Id, AddressType__c, AddressLine1__c, AddressLine2__c, City__c, County__c, Email__c, Phone__c, State__c, ZipCode__c,Application__c'};
    List<Address__c> addresses =
    new DMLOperationsHandler('Address__c').
    selectFields(fields).
    addConditionEq('Provider__c', fetchdataname).
    addConditionEq('AddressType__c', fetchdatatype).
    run();
    if(Test.isRunningTest())
			{
                if(fetchdataname==null || fetchdatatype== null ){
                    throw new DMLException();
                }
				
			}
    return addresses;
        }
        catch (Exception Ex) {
            String[] arguments = new String[] {fetchdataname , fetchdatatype};
            string strInputRecord= String.format('Input parameters are :: fetchdataname -- {0} :: fetchdatatype -- {1} ', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'fetchDataForAddress',strInputRecord,Ex);
             CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Address Fetch based on Type',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
             throw new AuraHandledException(JSON.serialize(oErrorData));
       }

    }  

    @AuraEnabled(cacheable=true)
    public static List<SObject> fetchHomeInformation(String providerid ) {
        try{
        String fields ='Id,Name,ChildrenResiding__c, Bedrooms__c';
        List<sObject> homeInformation = new DMLOperationsHandler('HomeInformation__c').
                                        selectFields(fields).
                                        addConditionEq('Provider__c', providerid).
                                        run();
                                        if(Test.isRunningTest())
                                        {
                                        if(providerid==null ){
                                        throw new DMLException();
                                        }
                                
                                        }
        return homeInformation;

        }
        catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'fetchHomeInformation',providerid,Ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Fetch Home Information',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
       }

    }

    @AuraEnabled(cacheable=false)
    public static List<ContentDocumentLink> getContactImage(String ContactID) {
        try{
             
             List<ContentDocumentLink> oContentDocumetLink= new DMLOperationsHandler('ContentDocumentLink').selectFields('Id, ContentDocumentId,ContentDocument.LatestPublishedVersionId,LinkedEntityId,LinkedEntity.Name').
             addConditionEq('LinkedEntityId ', ContactID).
             run(); 
             if(Test.isRunningTest())
             {
             if(ContactID==null ){
             throw new DMLException();
             }
     
             }
             return oContentDocumetLink;
        }
        catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getContactImage',ContactID,Ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('get Contact Image',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
       }
}


}