/**
 * @Author        : Balamurugan B
 * @CreatedOn     : APRIL 23 ,2020
 * @Purpose       :Narrative Trigger Handler Class on Reconsideration Object.
**/

public class reconsiderationTriggerHandler
{
public static void afterupdate(Map<Id, Reconsideration__c> newMap, Map<Id, Reconsideration__c> oldMap){
    updateaccountHistoryTracking(newMap.values(),oldMap);
}
public static void afterinsert(List<Reconsideration__c> newvaluelist){
    list<HistoryTracking__c> hstTrackList = new list<HistoryTracking__c>();
    if(!newvaluelist.isEmpty()){
        for (Reconsideration__c rec: newvaluelist){
            HistoryTracking__c hstTrack = new HistoryTracking__c();
                    hstTrack.ObjectName__c = 'Reconsideration__c';
                    hstTrack.ObjectRecordId__c = rec.Id;
                    hstTrack.RichNewValue__c = rec.Narrative__c;
                    hstTrackList.add(hstTrack);

        }
    }
    if(!hstTrackList.isEmpty()){
        insert hstTrackList;
    }
}

private static void updateaccountHistoryTracking(list<Reconsideration__c> Reconsiderations, Map<Id, Reconsideration__c> oldrecMap){
    Reconsideration__c oldrecInfo = new Reconsideration__c();
        list<HistoryTracking__c> hstTrackList = new list<HistoryTracking__c>();
        for (Reconsideration__c rec: Reconsiderations){

            if (oldrecMap != null){

                oldrecInfo = oldrecMap.get(rec.Id);
                if(oldrecInfo.Narrative__c != rec.Narrative__c){

                    HistoryTracking__c hstTrack = new HistoryTracking__c();
                    hstTrack.ObjectName__c = 'Reconsideration__c';
                    hstTrack.ObjectRecordId__c = rec.Id;
                    hstTrack.RichOldValue__c = oldrecInfo.Narrative__c;
                    hstTrack.RichNewValue__c = rec.Narrative__c;
                    hstTrack.HistoryName__c = ProviderConstants.HISTORY_DESCRIPTION;
                    hstTrackList.add(hstTrack);
                }
            }
        }
        if(hstTrackList.size() >0){
            insert hstTrackList;
        }
    }
}