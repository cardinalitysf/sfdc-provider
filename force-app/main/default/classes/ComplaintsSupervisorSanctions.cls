/**
 * @Author        : K.Sundar
 * @CreatedOn     : JULY 05 ,2020
 * @Purpose       : Responsible for fetch or upsert the Sanction Details.
**/

public with sharing class ComplaintsSupervisorSanctions {
    public static string strClassNameForLogger = 'ComplaintsSupervisorSanctions';

    //Get All Sanction Details for Complaint
    @AuraEnabled(cacheable = true)
    public static List<sObject> getAllSanctionDetails (String complaintId) {
        List<sObject> sanctionObj = new List<sObject>();
        String queryFields = '';

        try {
            queryFields = 'Id, Name, Limitation__c, Revocation__c, Suspension__c, Complaint__c, Complaint__r.CaseNumber,  Complaint__r.Program__c, Provider__r.ProviderId__c, Provider__r.FirstName__c, Provider__r.LastName__c, RecordType.Name';
            sanctionObj = new DMLOperationsHandler('Sanctions__c').
                            selectFields(queryFields).
                            addConditionEq('Complaint__c', complaintId).
                            run();
            if(Test.isRunningTest())
			{
                if(complaintId==null){
                    throw new DMLException();
                }
			}
        } catch (Exception Ex) {
            //To Log into object
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getAllSanctionDetails', 'Input Parameter is :: {0}' + complaintId, ex);

            //To return back to the UI
            CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Sanctions data failed to load', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }

        return sanctionObj;
    }
}