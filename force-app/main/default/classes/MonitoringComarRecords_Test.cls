/**
     * @Author        : Sindhu Venkateswarlu
     * @CreatedOn     : May 7,2020
     * @Purpose       :Test Methods to unit test for MonitoringComarRecords.cls
     * @Updated on    :June 11, 2020
     * @Updated by    :Janaswini S
     **/

@isTest
public class MonitoringComarRecords_Test {
             @isTest static void testgetRecordDetails(){
             ReferenceValue__c InsertobjNewReference=new ReferenceValue__c(); 
             insert InsertobjNewReference;
            
             Monitoring__c InsertobjNewMonitoring=new Monitoring__c(); 
             insert InsertobjNewMonitoring;
             ProviderRecordAnswer__c InsertobjNewProviderRecordAnswer=new ProviderRecordAnswer__c(Comments__c	 = 'test comments'); 
             insert InsertobjNewProviderRecordAnswer;
             String str = JSON.serialize(InsertobjNewProviderRecordAnswer);
            
             Address__c InsertobjNewAddress=new Address__c(AddressLine1__c = '345 California St'); 
             insert InsertobjNewAddress;
         
             ProviderRecordQuestion__c InsertobjNewProviderRecordQuestion=new ProviderRecordQuestion__c(Status__c= 'Draft'); 
             insert InsertobjNewProviderRecordQuestion;
            
             Application__c InsertobjNewApplication=new Application__c(); 
             insert InsertobjNewApplication;
         
          
             Account InsertobjNewAccount=new Account(Name = 'test Account'); 
            insert InsertobjNewAccount;
            TestDataFactory.createTestContacts(1,InsertobjNewAccount.id,true);
          
            ReferenceValue__c ref = new ReferenceValue__c();
            insert ref;
            string str1 = '[{"Comar__c":"'+InsertobjNewReference.Id+'","Compliance__c":"'+InsertobjNewProviderRecordAnswer.Id+'","Findings__c":"'+InsertobjNewProviderRecordAnswer.Id+'","ProviderRecordQuestion__c":"'+InsertobjNewProviderRecordQuestion.Id+'"}]';
            string strUpdate = '[{"Id":"'+InsertobjNewProviderRecordAnswer.Id+'","Compliance__c":"","Findings__c":"","ProviderRecordQuestion__c":"'+InsertobjNewProviderRecordQuestion.Id+'"}]';
           


            try{
                moniteringComarRecords.getRecordDetails(null,null,null);
            }catch(Exception Ex){}
            moniteringComarRecords.getRecordDetails(InsertobjNewMonitoring.Id,InsertobjNewAddress.Id,'Id');
              
            try{
               moniteringComarRecords.getQuestionsByType(null);
            }catch(Exception Ex){}
            moniteringComarRecords.getQuestionsByType('questions');
              
            try{
               moniteringComarRecords.bulkAddRecordAns(null);
            }catch(Exception Ex){}
            moniteringComarRecords.bulkAddRecordAns(str1);
              
            try{
               moniteringComarRecords.bulkUpdateRecordAns(null);

            }catch(Exception Ex){}
            moniteringComarRecords.bulkUpdateRecordAns(strUpdate);

            try{
               moniteringComarRecords.getSiteId(null);

            }catch(Exception Ex){}
            moniteringComarRecords.getSiteId('address');

         
            moniteringComarRecords.getRecordType('Physical Plant');

            try{
               moniteringComarRecords.editRecordDet(null);
            }catch(Exception Ex){}
            moniteringComarRecords.editRecordDet('question');
            
            try{
               moniteringComarRecords.applicationStaff(null);
            }catch(Exception Ex){}
            moniteringComarRecords.applicationStaff('xyz');


            try{
               moniteringComarRecords.getStaffFromApplicationId(null);
            }catch(Exception Ex){}
            moniteringComarRecords.getStaffFromApplicationId('abc');

                 
             }

}