/**
 * @Author        : Vijayaraj M
 * @CreatedOn     : August 03,2020
 * @Purpose       : IncidentProgramInfo cls file
 **/
public with sharing class IncidentProgramInfo {
    @AuraEnabled
    public static Id getIdAfterInsertSOQL(sObject objIdSobjecttoUpdateOrInsert) {

        Case ocase=new Case();
        ocase.Status = 'New';
        ocase.RecordTypeId =CommonUtils.getRecordTypeIdbyName('Case','Incident');

        return DMLOperationsHandler.getIdAfterInsertSOQL(ocase);
    }
 
   
    @AuraEnabled(cacheable=false)
    public static List<SObject> getProgramDetails(string Programdetails) {
        String queryFields;
        List<sObject> ProgramObj = new List<sObject>();
        try {
             queryFields = 'Provider__c,Program__c';
             ProgramObj = new DMLOperationsHandler('Application__c').
                                    selectFields(queryFields).
                                    addConditionEq('Provider__c', Programdetails).
                                    addConditionEq('Status__c', 'Approved').
                                    run();
        if(Test.isRunningTest())
  {
        if(Programdetails==''){
                throw new DMLException();
        }
 }
        } catch (Exception ex) {
            String inputParameterForProgramdetails='Inputs Parameter Program Details ::'+Programdetails;
            CustomAuraExceptionData.LogIntoObject('IncidentProgramInfo','getProgramDetails',inputParameterForProgramdetails,ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Program Details',ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
            
        }
        return ProgramObj;
        
    }
 
 
    @AuraEnabled(cacheable=false)
    public static List<SObject> getProgramTypeDetails(string Providerdetails,String getProgramdetails) {
        String queryFields;
        List<sObject> ProgramTypeObj = new List<sObject>();
         try {
             queryFields = 'Id,Provider__c,ProgramType__c';
             ProgramTypeObj = new DMLOperationsHandler('Application__c').
                                    selectFields(queryFields).
                                    addConditionEq('Provider__c', Providerdetails).
                                    addConditionEq('Program__c', getProgramdetails).
                                    addConditionEq('Status__c', 'Approved').
                                    run();
        if(Test.isRunningTest())
  {
        if(Providerdetails==null || getProgramdetails==''){
                throw new DMLException();
        }
  }
        } catch (Exception ex) {
            String[] arguments = new String[] {Providerdetails , getProgramdetails};
            string strInputRecord= String.format('Input parameters Program Type Details :: Providerdetails -- {0} :: getProgramdetails -- {1}', arguments);
            CustomAuraExceptionData.LogIntoObject('IncidentProgramInfo','getProgramTypeDetails',strInputRecord,ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Program Type Details ',ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
            
        }
        return ProgramTypeObj;
        
    }
 
    
    @AuraEnabled(cacheable=false)
    public static List<SObject> getProgramSiteDetails(string ProgramSiteDetails) {
        String queryFields;
        List<sObject> ProgramSiteObj = new List<sObject>();
        try {
             queryFields = 'Id,Provider__c,Application__c,AddressType__c,AddressLine1__c,Email__c,Phone__c';
             ProgramSiteObj = new DMLOperationsHandler('Address__c').
                                    selectFields(queryFields).
                                    addConditionEq('Application__c', ProgramSiteDetails).
                                    addConditionEq('AddressType__c', 'Site').
                                    run();
        if(Test.isRunningTest())
  {
        if(ProgramSiteDetails==''){
                throw new DMLException();
        }
  }
        } catch (Exception ex) {
            String inputParameterForProgramSiteDetails='Inputs Parameter Program Site Details ::'+ProgramSiteDetails;
            CustomAuraExceptionData.LogIntoObject('IncidentProgramInfo','getProgramSiteDetails',inputParameterForProgramSiteDetails,ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Program Site Details',ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
            
        }
        return ProgramSiteObj;
        
    }



    
    
    @AuraEnabled(cacheable=false)
    public static List<SObject> getProviderDetails(string ProviderBoxDetails) {
        String queryFields;
        List<sObject> providerDetailsObj = new List<sObject>();
        try {
             queryFields = 'Id,Name,Phone,Email,IsProfilePhotoActive,MediumPhotoUrl';
             providerDetailsObj = new DMLOperationsHandler('User').
                                    selectFields(queryFields).
                                    addConditionEq('Id', ProviderBoxDetails).
                                    run();
        if(Test.isRunningTest())
  {
        if(ProviderBoxDetails==''){
                throw new DMLException();
        }
  }
        } catch (Exception ex) {
            String inputParameterForProviderDetails='Inputs Parameter Provider Details ::'+ProviderBoxDetails;
            CustomAuraExceptionData.LogIntoObject('IncidentProgramInfo','getProviderDetails',inputParameterForProviderDetails,ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Provider Details',ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
            
        }
        return providerDetailsObj;
        
    }



    @AuraEnabled(cacheable=false)
    public static List<SObject> getAgencyDetails(string AgencyDetails) {
        String queryFields;
        List<sObject> agencyObj = new List<sObject>();
        try {
             queryFields = 'Id,Caseworker__r.Id,Caseworker__r.Name,Caseworker__r.Unit__c,Caseworker__r.Email,Caseworker__r.Phone';
             agencyObj = new DMLOperationsHandler('Application__c').
                                    selectFields(queryFields).
                                    addConditionEq('Id', AgencyDetails).
                                    run();
                                    
        if(Test.isRunningTest())
  {
        if(AgencyDetails==''){
                throw new DMLException();
        }
  }
        } catch (Exception ex) {
            String inputParameterForAgencyDetails='Inputs Parameter Agency Details ::'+AgencyDetails;
            CustomAuraExceptionData.LogIntoObject('IncidentProgramInfo','getAgencyDetails',inputParameterForAgencyDetails,ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Agency Details',ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
            
        }
        return agencyObj;
        
    }


    
    @AuraEnabled(cacheable=true)
    public static List<SObject> getIncidentDetails(string incidentProgramDetails) {
        
        List<sObject> incidentDetailsObj = new List<sObject>();
        
        try {
             String queryFields = 'Id,Program__c,ProgramType__c,Address__r.AddressLine1__c,Application__c,Address__r.Email__c,Address__r.Phone__c,Status,Provider__r.Name,Provider__r.Email,Provider__r.Phone,Provider__r.IsProfilePhotoActive ,Provider__r.MediumPhotoUrl';
             incidentDetailsObj = new DMLOperationsHandler('Case').
                                    selectFields(queryFields).
                                    addConditionEq('Id', incidentProgramDetails).
                                    run();
                                   
        if(Test.isRunningTest())
  {
        if(incidentProgramDetails==''){
                throw new DMLException();
        }
  }
        } catch (Exception ex) {
            String inputParameterForincidentProgramDetails='Inputs Parameter Incident Program Info ::'+incidentProgramDetails;
            CustomAuraExceptionData.LogIntoObject('IncidentProgramInfo','getIncidentDetails',inputParameterForincidentProgramDetails,ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Incident Program Info',ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
            
        }
        return incidentDetailsObj;
        
    }


    @AuraEnabled(cacheable=true)
    public static List<SObject> getCPAaddressDetails(string cpaProgramTypeDetails,String cpaProviderDetails) {
        
        List<sObject> cpaAddressObj = new List<sObject>();
        
        try {
             String queryFields = 'Id, ProgramType__c, Provider__c,Address__r.AddressLine1__c';
             cpaAddressObj = new DMLOperationsHandler('CPAHome__c').
                                    selectFields(queryFields).
                                    addConditionEq('ProgramType__c', cpaProgramTypeDetails).
                                    addConditionEq('Provider__c', cpaProviderDetails).
                                    run();
                                    
                                   
                                    if(Test.isRunningTest())
                                    {
                                          if(cpaProgramTypeDetails=='' || cpaProviderDetails==null){
                                                  throw new DMLException();
                                          }
                                    }
                                          } catch (Exception ex) {
                                              String[] arguments = new String[] {cpaProgramTypeDetails , cpaProviderDetails};
                                              string strInputRecord= String.format('Input parameters Program Type Details :: cpaProgramTypeDetails -- {0} :: cpaProgramTypeDetails -- {1}', arguments);
                                              CustomAuraExceptionData.LogIntoObject('IncidentProgramInfo','getCPAaddressDetails',strInputRecord,ex);
                                              CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('CPA Address Details ',ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
                                              throw new AuraHandledException(JSON.serialize(oErrorData));
                                              
                                          }
        return cpaAddressObj;
        
    }

    @AuraEnabled
    public static string updateOrInsertSOQL(sObject objSobjecttoUpdateOrInsert) {
        //This method is exposed to front end and reponsile for upsert operation
        //sObject is passed to updateOrInsertQuery where real data base insert or update will happen
        return DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert);
    }
}
