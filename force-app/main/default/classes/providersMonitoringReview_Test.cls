@IsTest

public class providersMonitoringReview_Test {
    @isTest
    private static void getPickListValues_Test() {
        Monitoring__c Monitoring=new Monitoring__c();
        Test.startTest();
        providersMonitoringReview.getPickListValues(Monitoring,'ReviewStatus__c');
        Test.stopTest();
    }

    @isTest
    private static void updateapplicationdetails_Test() {
        Profile prf = [Select Id, Name from Profile where Name= 'Supervisor'];
        List<User> userList =  TestDataFactory.createTestUsers(1, prf.Id, true);
        Monitoring__c monitoringDraft = new Monitoring__c(SelectPeriod__c = 'Initial',MonitoringStatus__c='Submitted',ApprovalComments__c='test',Caseworker__c=userList[0].id,ReviewStatus__c='Draft');
        /* ContentVersion contentVersion = new ContentVersion(
               Title = 'Test',
               PathOnClient = 'Test.jpg',
               VersionData = Blob.valueOf('ApplicationLicenseInformation_Test')
             );
                insert contentVersion;
        ContentDocumentLink contentVersion = new ContentDocumentLink(
               LinkedEntityId = monitoringDraft.Id,
               ContentDocumentId = 'xxx'
             );
        insert contentVersion; */
        Test.startTest();
        providersMonitoringReview.updateapplicationdetails(monitoringDraft, userList[0].id, '');
        providersMonitoringReview.updateapplicationdetailsDraft(monitoringDraft,userList[0].id);
        /* providersMonitoringReview.getDatatableDetails(monitoringDraft.Id);
        providersMonitoringReview.getDatatableDetails(''); */
        providersMonitoringReview.saveSign('image',monitoringDraft.Id);
        providersMonitoringReview.updateCaseworkerdetails(monitoringDraft);
        Test.stopTest();
    }

    @IsTest static void testprovidersMonitoringReviewNegative1() {
        Profile prf = [Select Id, Name from Profile where Name= 'Supervisor'];
        List<User> userList =  TestDataFactory.createTestUsers(1, prf.Id, true);
        Monitoring__c InsertobjMonitoringPeriod=new Monitoring__c(SelectPeriod__c = 'Initial',MonitoringStatus__c='Submitted',ApprovalComments__c='test',Caseworker__c=userList[0].id,ReviewStatus__c='Reject'); 
        insert InsertobjMonitoringPeriod;
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments('Submitting request for approval.');
        req.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        req.setObjectId(InsertobjMonitoringPeriod.Id);
        Approval.ProcessResult resu = Approval.process(req);      
        System.assert(resu.isSuccess());
        System.assertEquals('Pending', resu.getInstanceStatus(),'Instance Status'+resu.getInstanceStatus());
        List<Id> newWorkItemIds = resu.getNewWorkitemIds();

        providersMonitoringReview.updateapplicationdetails(InsertobjMonitoringPeriod,'',newWorkItemIds.get(0));
    }

    @IsTest static void testprovidersMonitoringReviewNegative2() {
        Profile prf = [Select Id, Name from Profile where Name= 'Supervisor'];
        List<User> userList =  TestDataFactory.createTestUsers(1, prf.Id, true);
        Monitoring__c InsertobjMonitoringPeriod=new Monitoring__c(SelectPeriod__c = 'Initial',MonitoringStatus__c='Submitted',ApprovalComments__c='test',Caseworker__c=userList[0].id,ReviewStatus__c='Reassign'); 
        insert InsertobjMonitoringPeriod;
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments('Submitting request for approval.');
        req.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        req.setObjectId(InsertobjMonitoringPeriod.Id);
        Approval.ProcessResult resu = Approval.process(req);
        System.assert(resu.isSuccess());
        System.assertEquals('Pending', resu.getInstanceStatus(),'Instance Status'+resu.getInstanceStatus());
        List<Id> newWorkItemIds = resu.getNewWorkitemIds();

        providersMonitoringReview.updateapplicationdetails(InsertobjMonitoringPeriod,'',newWorkItemIds.get(0));
    }
    @IsTest static void testprovidersMonitoringReviewNegative3() {
        Profile prf = [Select Id, Name from Profile where Name= 'Supervisor'];
        List<User> userList =  TestDataFactory.createTestUsers(1, prf.Id, true);
        Monitoring__c InsertobjMonitoringPeriod=new Monitoring__c(SelectPeriod__c = 'Initial',MonitoringStatus__c='Submitted',ApprovalComments__c='test',Caseworker__c=userList[0].id,ReviewStatus__c='Approve'); 
        insert InsertobjMonitoringPeriod;

        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments('Submitting request for approval.');
        req.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        req.setObjectId(InsertobjMonitoringPeriod.Id);
        Approval.ProcessResult resu = Approval.process(req);
        System.assert(resu.isSuccess());
        System.assertEquals('Pending', resu.getInstanceStatus(),'Instance Status'+resu.getInstanceStatus());
        List<Id> newWorkItemIds = resu.getNewWorkitemIds();

        providersMonitoringReview.updateapplicationdetails(InsertobjMonitoringPeriod,'',newWorkItemIds.get(0));
    }

    @isTest
    private static void getReviewDetails_Test(){
        Test.startTest();
        try {
            providersMonitoringReview.getReviewDetails('a');
            providersMonitoringReview.getReviewDetails(null);
        } catch(Exception e){}
        Test.stopTest();
    }

    @isTest
    private static void getAllUsersList_Test(){
        Test.startTest();
        try {
            providersMonitoringReview.getAllUsersList();
        } catch(Exception e){}
        Test.stopTest();
    }

    @isTest
    private static void getDatatableDetails_Test() {  
        Profile prf = [Select Id, Name from Profile where Name= 'Supervisor'];
        List<User> userList =  TestDataFactory.createTestUsers(1, prf.Id, true);
      
        Monitoring__c monitoringDraft = new Monitoring__c();
        monitoringDraft.SelectPeriod__c = 'Initial';
        monitoringDraft.MonitoringStatus__c='Submitted';
        monitoringDraft.ApprovalComments__c='test';
        monitoringDraft.Caseworker__c=userList[0].id;
        monitoringDraft.ReviewStatus__c='Approve';
        Insert monitoringDraft;
        Monitoring__c InsertobjMonitoringPeriod=new Monitoring__c(SelectPeriod__c = 'Initial',MonitoringStatus__c='Submitted',ApprovalComments__c='test',Caseworker__c=userList[0].id,ReviewStatus__c='Approve'); 
        insert InsertobjMonitoringPeriod;
        Monitoring__c InsertobjMonitoringSec=new Monitoring__c(SelectPeriod__c = 'Initial',MonitoringStatus__c='Submitted',ApprovalComments__c='test',Caseworker__c=userList[0].id,ReviewStatus__c='Approve'); 
        insert InsertobjMonitoringSec;

        String strSignElement = 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8z/C/HgAGgwJ/lK3Q6wAAAABJRU5ErkJggg==';
        ContentVersion contentVersion = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            OwnerId = UserInfo.getUserId(),
            VersionData = EncodingUtil.base64Decode(strSignElement)
          );
             insert contentVersion;
        Id conDocument = [SELECT ContentDocumentId  FROM ContentVersion WHERE Id =:contentVersion.Id].ContentDocumentId;

        ContentDocumentLink cDocLink = new ContentDocumentLink();
        cDocLink.ContentDocumentId = conDocument;
        cDocLink.LinkedEntityId = InsertobjMonitoringSec.Id;
        cDocLink.ShareType = 'I';
        cDocLink.Visibility = 'AllUsers';
        Insert cDocLink;

        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments('Submitting request for approval.');
        req.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        req.setObjectId(monitoringDraft.Id);
        Approval.ProcessResult resu = Approval.process(req);      
        System.assert(resu.isSuccess());
        System.assertEquals('Pending', resu.getInstanceStatus(),'Instance Status'+resu.getInstanceStatus());
        List<Id> newWorkItemIds = resu.getNewWorkitemIds();

        Test.startTest();
        providersMonitoringReview.getDatatableDetails(monitoringDraft.Id);
        providersMonitoringReview.getDatatableDetails(InsertobjMonitoringPeriod.Id);
        providersMonitoringReview.getDatatableDetails(InsertobjMonitoringSec.Id);
        providersMonitoringReview.getDatatableDetails('');
        Test.stopTest();
    }
}
