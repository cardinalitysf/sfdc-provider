/**
 * @Author        : Preethi Bojarajan
 * @CreatedOn     : May 18, 2020
 * @Purpose       : New House Hold Member Adding Updating Classes
 * @updatedBy     : 
 * @updatedOn     : 
 **/
public with sharing class PublicProviderAddHouseHoldMembers {
    public static string strClassNameForLogger='PublicProviderAddHouseHoldMembers';

    @AuraEnabled(cacheable=true)
    public static Id getRecordType(String name) {
        try {
            return CommonUtils.getRecordTypeIdbyName('Contact', name);
        } catch (Exception ex) {
            //To Log into object
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getRecordType', 'Record Type Name' + name, ex);
            //To return back to the UI
            CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Record Type Fetch Issue,Please Try Again..!', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
    }
    
   
    @AuraEnabled(cacheable=false)
    public static String fetchHouseHoldMemberDetails(String id) {
        String fields ='Title__c, LivingSituationDescription__c, ApproximateAge__c, HeightinInch__c, WeightsinPound__c, Gender__c, HairColor__c, SkinTone__c, FirstName__c, MiddleName__c, LastName__c, DOB__c, PhysicalMark__c, IsaUsClient__c, SSNVerified__c, StateIDDriversLicense__c, SSN__c, PrimaryCitizenship__c, PrimaryLanguage__c, Nationality__c, DateofDeath__c, Occupation__c, Religion__c, ChildpreviouslyAdopted__c, LivingSituation__c, LivingSituationDescription__c, PersonRole__c, SexOffenderregisteryChecked__c, JudiciarycaseSearch__c, Substanceexposednewborn__c, Safeheavenbaby__c, DangertoselfReason__c, DangertoworkerReason__c, MentalillnessReason__c, MentallyimpairedReason__c, PersonRole__c, Role__c, Dangertoself__c, Appearanceofmentallyimpaired__c, Dangertoworker__c, Signsofmentalillness__c, DeclaredDisable__c, LicensedChildCareProvider__c, ApplyingforChildCareProvider__c, LicensePreviouslyDenied__c, Caringforaged__c';
        List<sObject> houseHoldMem = new List<sObject>();
        List<ContentDocumentLink> conDocLink  = new List<ContentDocumentLink>();
        List<processingWrapper> appWrap = new List<processingWrapper>();
        try {
        houseHoldMem = new DMLOperationsHandler('Contact').
                            selectFields(fields).
                            addConditionEq('Id', id).
                            run();
                            if(Test.isRunningTest()) {
                                if(id == null) {
                                    throw new DMLException();
                                }
                            }
        conDocLink = new DMLOperationsHandler('ContentDocumentLink').
                            selectField('ContentDocumentId').
                            addConditionEq('LinkedEntityId', id).
                            run();
        if(conDocLink.size() > 0) {
            String conDocId = conDocLink[0].ContentDocumentId;
            List<ContentVersion> conVersion = new DMLOperationsHandler('ContentVersion').
                                        selectField('Id').
                                        addConditionEq('ContentDocumentId', conDocId).
                                        run();
            processingWrapper prdWrapper = new processingWrapper(houseHoldMem, conVersion[0].Id);
            appWrap.add(prdWrapper);
        } else {
            processingWrapper prdWrapper = new processingWrapper(houseHoldMem, '');
            appWrap.add(prdWrapper);
        }
        } catch (Exception ex) {
            //To Log into object
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'fetchHouseHoldMemberDetails', 'Id' + id, ex);
            //To return back to the UI
            CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Fetch House Hold Member Issue,Please Try Again..!', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return JSON.serialize(appWrap);
    }

    @AuraEnabled(cacheable=false)
    public static String fetchHouseHoldMemberFrmActorDetails(String id, String contactId, String type) {
        String fields ='Id, Contact__r.Title__c, Contact__r.ApproximateAge__c, Contact__r.HeightinInch__c, Contact__r.WeightsinPound__c, Contact__r.Gender__c, Contact__r.HairColor__c, Contact__r.SkinTone__c, Contact__r.FirstName__c, Contact__r.MiddleName__c, Contact__r.LastName__c, Contact__r.DOB__c, Contact__r.PhysicalMark__c, Contact__r.IsaUsClient__c, Contact__r.SSNVerified__c, Contact__r.StateIDDriversLicense__c, Contact__r.SSN__c, Contact__r.PrimaryCitizenship__c, Contact__r.PrimaryLanguage__c, Contact__r.Nationality__c, Contact__r.DateofDeath__c, Contact__r.Occupation__c, Contact__r.Religion__c, Contact__r.ChildpreviouslyAdopted__c, Contact__r.LivingSituation__c, Contact__r.LivingSituationDescription__c, Contact__r.PersonRole__c, Contact__r.SexOffenderregisteryChecked__c, Contact__r.JudiciarycaseSearch__c, Contact__r.Substanceexposednewborn__c, Contact__r.Safeheavenbaby__c, Contact__r.DangertoselfReason__c, Contact__r.DangertoworkerReason__c, Contact__r.MentalillnessReason__c, Contact__r.MentallyimpairedReason__c, Contact__r.Dangertoself__c, Contact__r.Appearanceofmentallyimpaired__c, Contact__r.Dangertoworker__c, Contact__r.Signsofmentalillness__c, Contact__r.DeclaredDisable__c, Contact__r.LicensedChildCareProvider__c, Contact__r.ApplyingforChildCareProvider__c, Contact__r.LicensePreviouslyDenied__c, Contact__r.Caringforaged__c, Contact__r.ChildCareLicenseNumber__c, Contact__r.ExplanantionforLicenseDenial__c, Role__c';
        List<sObject> houseHoldMem = new List<sObject>();
        List<ContentDocumentLink> conDocLink  = new List<ContentDocumentLink>();
        List<processingWrapper> appWrap = new List<processingWrapper>();
        try {
            houseHoldMem = new DMLOperationsHandler('Actor__c').
                                selectFields(fields).
                                addConditionEq(type, id).
                                addConditionEq('Contact__c', contactId).
                                run();
                                if(Test.isRunningTest()) {
                                    if(id == null) {
                                        throw new DMLException();
                                    }
                                }
            conDocLink = new DMLOperationsHandler('ContentDocumentLink').
                                        selectField('ContentDocumentId').
                                        addConditionEq('LinkedEntityId', contactId).
                                        run();
            if(conDocLink.size() > 0) {
                String conDocId = conDocLink[0].ContentDocumentId;
                List<ContentVersion> conVersion = new DMLOperationsHandler('ContentVersion').
                                                        selectField('Id').
                                                        addConditionEq('ContentDocumentId', conDocId).
                                                        run();
                processingWrapper prdWrapper = new processingWrapper(houseHoldMem, conVersion[0].Id);
                appWrap.add(prdWrapper);
        } else {
            processingWrapper prdWrapper = new processingWrapper(houseHoldMem, '');
            appWrap.add(prdWrapper);
        }
    } catch (Exception ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'fetchHouseHoldMemberDetails', 'Id' + id, ex);
        //To return back to the UI
        CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Fetch House Hold Member Issue,Please Try Again..!', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
    }
        return JSON.serialize(appWrap);
    }
    
    @AuraEnabled(cacheable =true)
     public static Map<String, List<DMLOperationsHandler.FetchValueWrapper>> getMultiplePicklistValues(String objInfo, String picklistFieldApi) {
         return DMLOperationsHandler.fetchMultiplePickListValue(objInfo,picklistFieldApi);
    }

    @AuraEnabled
    public static void saveProfilePic(String strProfileImg, Id recId){
        // Create Salesforce File
        //Insert ContentVersion
        ContentVersion cVersion = new ContentVersion();
        cVersion.ContentLocation = 'S'; //S-Document is in Salesforce. E-Document is outside of Salesforce. L-Document is on a Social Netork.
        cVersion.PathOnClient = 'ProfilePic-'+System.now() +'.png';//File name with extention
        cVersion.Origin = 'H';//C-Content Origin. H-Chatter Origin.
        //cVersion.OwnerId = attach.OwnerId;//Owner of the file
        cVersion.Title = 'ProfilePic-'+System.now() +'.png';//Name of the file
        cVersion.VersionData = EncodingUtil.base64Decode(strProfileImg);//File content
        cVersion.IsSignature__c = false;
        cVersion.IsMajorVersion = false;
        Insert cVersion;

        //After saved the Content Verison, get the ContentDocumentId
        Id conDocument = [SELECT ContentDocumentId  FROM ContentVersion WHERE Id =:cVersion.Id].ContentDocumentId;
        //Insert ContentDocumentLink
        ContentDocumentLink cDocLink = new ContentDocumentLink();
        cDocLink.ContentDocumentId = conDocument;//Add ContentDocumentId
        cDocLink.LinkedEntityId = recId;//Add attachment parentId
        cDocLink.ShareType = 'I';//V - Viewer permission. C - Collaborator permission. I - Inferred permission.
        cDocLink.Visibility = 'AllUsers';//AllUsers, InternalUsers, SharedUsers
        Insert cDocLink;      
    }

    private class processingWrapper {
            public List<SObject> housHoldRecord;
            public String profilePicUrl;
            public processingWrapper(List<SObject> contact, String Id) {
                this.housHoldRecord = contact;
                this.profilePicUrl = Id;
            }
    }

    @AuraEnabled
    public static void updateProfilePic(String strProfileImg, Id recId){
        //Update ContentVersion
        ContentVersion cVersion = new ContentVersion();
        cVersion.Id = recId;
        cVersion.VersionData = EncodingUtil.base64Decode(strProfileImg);//File content
        Update cVersion;
    }

    @AuraEnabled(cacheable = true)
    public static List<WrapperContactcount> getRefContactExistDet(String providerId) {
    List<WrapperContactcount> lstWrapper = new List<WrapperContactcount>();
        Integer countApplicant = 0;
        Integer countCoApplicant = 0;
        List<AggregateResult> results =  new List<AggregateResult>();
        results =  new DMLOperationsHandler('Actor__c'). 
                        selectFields('Role__c').
                        count('Id').
                        addConditionEq('Provider__c', providerId). 
                        groupBy('Role__c').
                        aggregate();
        for(AggregateResult result : results) {
            if(result.get('Role__c') == 'Applicant')
                countApplicant = (Integer) result.get('expr0');
            else if(result.get('Role__c') == 'Co-Applicant')
                countCoApplicant = (Integer) result.get('expr0');
        }
        lstWrapper.add(new WrapperContactcount(countApplicant, countCoApplicant));
        return lstWrapper;
    }

    // Wrapper Class
    private class WrapperContactcount {
        @AuraEnabled public Integer applicant;
        @AuraEnabled public Integer coApplicant;
        public WrapperContactcount(Integer countApplicant, Integer countCoApplicant) {
            this.applicant = countApplicant;
            this.coApplicant = countCoApplicant;
        }
    }
}