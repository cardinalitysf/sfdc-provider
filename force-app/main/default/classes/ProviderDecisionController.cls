/**
 * @Author        : Balamurugan
 * @CreatedOn     : March 30,2020
 * @Purpose       :Upsert and Fetching Review Details
 **/

public with sharing class ProviderDecisionController{
    public static string strClassNameForLogger='ProviderDecisionController';
	@AuraEnabled(cacheable = true)
	public static List<DMLOperationsHandler.FetchValueWrapper> getPickListValues(sObject objInfo, string picklistFieldApi){
		return DMLOperationsHandler.fetchPickListValue(objInfo, picklistFieldApi);
	}

	@AuraEnabled
	public static string updateapplicationdetails(sObject objSobjecttoUpdateOrInsert){
		return DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert);
	}

	//Modified by Sundar K
   @AuraEnabled(cacheable=true)
        public static List<Application__c> getReviewDetails(string applicationid) {
           Set<String> fields = new Set<String>{'Id', 'Status__c', 'Comments__c', 'ReviewStatus__c'};
           List<Application__c> apps = new List<Application__c>();
           try{
           apps= new DMLOperationsHandler('Application__c').
           selectFields(fields).
           addConditionEq('Id', applicationid).
           run();
            if(Test.isRunningTest())
            {
                if(applicationid==null)
                {
                    throw new DMLException();
                }
             }
           }
           catch(Exception Ex){
           //To Log into object
             CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getReviewDetails', 'applicationid' + applicationid, ex);
           //To return back to the UI
             CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Get Review Information,Please Try Again..!', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
             throw new AuraHandledException(JSON.serialize(oErrorData));
            }
           
             return apps;
           }


  @AuraEnabled(cacheable=true)
        public static List<Application__c> getProgramInformation(string applicationid) {
           Set<String> fields = new Set<String>{'Id','Name','Gender__c','MinAge__c','MaxAge__c','Capacity__c','MinIQ__c','MaxIQ__c'};
           List<Application__c> apps = new  List<Application__c>();
           try{
           apps =   new DMLOperationsHandler('Application__c').
           selectFields(fields).
           addConditionEq('Id', applicationid).
           run();
            if(Test.isRunningTest())
            {
                if(applicationid==null){
                    throw new DMLException();
                   }
             }
           }
           catch(Exception Ex){
             CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getProgramInformation', 'applicationid' + applicationid, ex);
            //To return back to the UI
             CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Get Program Information,Please Try Again..!', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
             throw new AuraHandledException(JSON.serialize(oErrorData));
            }
         
           return apps;
            }

       }