public with sharing class DialogFlowResponse {

	public class Parameters {
		public String Name {get;set;} 
		public String Email {get;set;} 
        public String Program {get;set;} 
        public String ProgramType {get;set;} 
        public String Communication {get;set;} 
        public String TaxId {get;set;} 
        public String CaseNumber {get;set;} 
        public String Status {get;set;} 
        public String ReferralType {get;set;}
      
        
		public Parameters(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'Name') {
							Name = parser.getText();
						} else if(text == 'Email') {
							Email = parser.getText();
						}else if(text == 'Program') {
							Program = parser.getText();
						}else if(text == 'ProgramType') {
							ProgramType = parser.getText();
						}else if(text == 'Communication') {
							Communication = parser.getText();
						}else if(text == 'TaxId') {
							TaxId = parser.getText();
						}else if(text == 'RefNumber') {
							CaseNumber = parser.getText();
						}else if(text == 'Status') {
							Status = parser.getText();
						}
                        else if(text == 'ReferralType') {
							ReferralType = parser.getText();
						}
						 else {
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class User {
		public String locale {get;set;} 
		public String lastSeen {get;set;} 
		public String userVerificationStatus {get;set;} 

		public User(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'locale') {
							locale = parser.getText();
						} else if (text == 'lastSeen') {
							lastSeen = parser.getText();
						} else if (text == 'userVerificationStatus') {
							userVerificationStatus = parser.getText();
						} else {
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class RawInputs {
		public String inputType {get;set;} 
		public String query {get;set;} 

		public RawInputs(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'inputType') {
							inputType = parser.getText();
						} else if (text == 'query') {
							query = parser.getText();
						} else {
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Arguments {
		public String name {get;set;} 
		public String rawText {get;set;} 
		public String textValue {get;set;} 

		public Arguments(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'name') {
							name = parser.getText();
						} else if (text == 'rawText') {
							rawText = parser.getText();
						} else if (text == 'textValue') {
							textValue = parser.getText();
						} else {
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class OriginalDetectIntentRequest {
		public String source {get;set;} 
		public String version {get;set;} 
		public Payload payload {get;set;} 

		public OriginalDetectIntentRequest(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'source') {
							source = parser.getText();
						} else if (text == 'version') {
							version = parser.getText();
						} else if (text == 'payload') {
							payload = new Payload(parser);
						} else {
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Text {
		public List<String> text {get;set;} 

		public Text(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String stext = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (stext == 'text') {
							text = arrayOfString(parser);
						} else {
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Intent {
		public String name {get;set;} 
		public String displayName {get;set;} 
		public Boolean endInteraction {get;set;} 

		public Intent(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'name') {
							name = parser.getText();
						} else if (text == 'displayName') {
							displayName = parser.getText();
						} else if (text == 'endInteraction') {
							endInteraction = parser.getBooleanValue();
						} else {
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Conversation {
		public String conversationId {get;set;} 
		public String type_Z {get;set;} // in json: type
		public String conversationToken {get;set;} 

		public Conversation(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'conversationId') {
							conversationId = parser.getText();
						} else if (text == 'type') {
							type_Z = parser.getText();
						} else if (text == 'conversationToken') {
							conversationToken = parser.getText();
						} else {
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class SimpleResponses {
		public String textToSpeech {get;set;} 

		public SimpleResponses(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'textToSpeech') {
							textToSpeech = parser.getText();
						} else {
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Surface {
		public List<Capabilities> capabilities {get;set;} 

		public Surface(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'capabilities') {
							capabilities = arrayOfCapabilities(parser);
						} else {
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class QueryResult {
		public String queryText {get;set;} 
		public Parameters parameters {get;set;} 
		public Intent intent {get;set;} 

		public QueryResult(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'queryText') {
							queryText = parser.getText();
						} else if (text == 'parameters') {
							parameters = new Parameters(parser);
						} else if (text == 'intent') {
							intent = new Intent(parser);
						}else {
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class FulfillmentMessages {
		public String platform {get;set;} 
		public SimpleResponses_Z simpleResponses {get;set;} 
		public Text text {get;set;} 

		public FulfillmentMessages(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String stext = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (stext == 'platform') {
							platform = parser.getText();
						} else if (stext == 'simpleResponses') {
							simpleResponses = new SimpleResponses_Z(parser);
						} else if (stext == 'text') {
							text = new Text(parser);
						} else {
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Capabilities {
		public String name {get;set;} 

		public Capabilities(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'name') {
							name = parser.getText();
						} else {
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class OutputContexts {
		public String name {get;set;} 
		public Parameters_Z parameters {get;set;} 

		public OutputContexts(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'name') {
							name = parser.getText();
						} else if (text == 'parameters') {
							parameters = new Parameters_Z(parser);
						} else {
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class SimpleResponses_Z {
		public List<SimpleResponses> simpleResponses {get;set;} 

		public SimpleResponses_Z(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'simpleResponses') {
							simpleResponses = arrayOfSimpleResponses(parser);
						} else {
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Payload {
		public User user {get;set;} 
		public Conversation conversation {get;set;} 
		public List<Inputs> inputs {get;set;} 
		public Surface surface {get;set;} 
		public Boolean isInSandbox {get;set;} 
		public List<Surface> availableSurfaces {get;set;} 
		public String requestType {get;set;} 

		public Payload(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'user') {
							user = new User(parser);
						} else if (text == 'conversation') {
							conversation = new Conversation(parser);
						} else if (text == 'inputs') {
							inputs = arrayOfInputs(parser);
						} else if (text == 'surface') {
							surface = new Surface(parser);
						} else if (text == 'isInSandbox') {
							isInSandbox = parser.getBooleanValue();
						} else if (text == 'availableSurfaces') {
							availableSurfaces = arrayOfSurface(parser);
						} else if (text == 'requestType') {
							requestType = parser.getText();
						} else {
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public String responseId {get;set;} 
	public QueryResult queryResult {get;set;} 
	public OriginalDetectIntentRequest originalDetectIntentRequest {get;set;} 
	public String session {get;set;} 

	public DialogFlowResponse(JSONParser parser) {
		while (parser.nextToken() != System.JSONToken.END_OBJECT) {
			if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
				String text = parser.getText();
				if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
					if (text == 'responseId') {
						responseId = parser.getText();
					} else if (text == 'queryResult') {
						queryResult = new QueryResult(parser);
					} else if (text == 'originalDetectIntentRequest') {
						originalDetectIntentRequest = new OriginalDetectIntentRequest(parser);
					} else if (text == 'session') {
						session = parser.getText();
					} else {
						consumeObject(parser);
					}
				}
			}
		}
	}
	
	public class Inputs {
		public String intent {get;set;} 
		public List<RawInputs> rawInputs {get;set;} 
		public List<Arguments> arguments {get;set;} 

		public Inputs(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'intent') {
							intent = parser.getText();
						} else if (text == 'rawInputs') {
							rawInputs = arrayOfRawInputs(parser);
						} else if (text == 'arguments') {
							arguments = arrayOfArguments(parser);
						} else {
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Parameters_Y {
		public Double no_input {get;set;} // in json: no-input
		public Double no_match {get;set;} // in json: no-match
		public String Name {get;set;} 
		public String Name_original {get;set;} // in json: Name.original

		public Parameters_Y(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'no-input') {
							no_input = parser.getDoubleValue();
						} else if (text == 'no-match') {
							no_match = parser.getDoubleValue();
						} else if (text == 'Name') {
							Name = parser.getText();
						} else if (text == 'Name.original') {
							Name_original = parser.getText();
						} else {
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Parameters_Z {
		public String Name {get;set;} 
		public String Name_original {get;set;} // in json: Name.original

		public Parameters_Z(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'Name') {
							Name = parser.getText();
						} else if (text == 'Name.original') {
							Name_original = parser.getText();
						} else {
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	
	public static DialogFlowResponse parse(String json) {
		System.JSONParser parser = System.JSON.createParser(json);
		return new DialogFlowResponse(parser);
	}
	
	public static void consumeObject(System.JSONParser parser) {
		Integer depth = 0;
		do {
			System.JSONToken curr = parser.getCurrentToken();
			if (curr == System.JSONToken.START_OBJECT || 
				curr == System.JSONToken.START_ARRAY) {
				depth++;
			} else if (curr == System.JSONToken.END_OBJECT ||
				curr == System.JSONToken.END_ARRAY) {
				depth--;
			}
		} while (depth > 0 && parser.nextToken() != null);
	}
	
    private static List<RawInputs> arrayOfRawInputs(System.JSONParser p) {
        List<RawInputs> res = new List<RawInputs>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new RawInputs(p));
        }
        return res;
    }








    private static List<Arguments> arrayOfArguments(System.JSONParser p) {
        List<Arguments> res = new List<Arguments>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Arguments(p));
        }
        return res;
    }



    private static List<FulfillmentMessages> arrayOfFulfillmentMessages(System.JSONParser p) {
        List<FulfillmentMessages> res = new List<FulfillmentMessages>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new FulfillmentMessages(p));
        }
        return res;
    }






    private static List<Surface> arrayOfSurface(System.JSONParser p) {
        List<Surface> res = new List<Surface>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Surface(p));
        }
        return res;
    }








    private static List<String> arrayOfString(System.JSONParser p) {
        List<String> res = new List<String>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(p.getText());
        }
        return res;
    }



    private static List<Capabilities> arrayOfCapabilities(System.JSONParser p) {
        List<Capabilities> res = new List<Capabilities>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Capabilities(p));
        }
        return res;
    }






    private static List<Inputs> arrayOfInputs(System.JSONParser p) {
        List<Inputs> res = new List<Inputs>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Inputs(p));
        }
        return res;
    }


    private static List<SimpleResponses> arrayOfSimpleResponses(System.JSONParser p) {
        List<SimpleResponses> res = new List<SimpleResponses>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new SimpleResponses(p));
        }
        return res;
    }





    private static List<OutputContexts> arrayOfOutputContexts(System.JSONParser p) {
        List<OutputContexts> res = new List<OutputContexts>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new OutputContexts(p));
        }
        return res;
    }


}