/**
 * @Author        : V.S.Marimuthu 
 * @CreatedOn     : March 18 ,2020
 * @Purpose       : Test Methods to unit test referralProviderHeaderController class in referralProviderHeaderController.cls
 * @UpdatedBy     : Nandhini
 **/


@isTest
private class referralProviderHeaderController_Test {
   
    //Testing Positive flow
    @isTest static void testreferralProviderHeaderControllerPositive()
    {
         //Inserting records into case object where status=New               
         Case objNewCase=new Case(Status = 'New'); 

         // Testing getIdAfterInsertSOQL by passing one record in case object
         Id refferalId= referralProviderHeaderController.getIdAfterInsertSOQL(objNewCase);

          // Testing getCaseHeaderDetails by passing id which we got from above method
          try {
               referralProviderHeaderController.getCaseHeaderDetails(null);
          }
          catch (Exception ex) {
 
          }
         List<Case> lstgetCaseDetails= referralProviderHeaderController.getCaseHeaderDetails((string)refferalId);
         System.assertEquals(true, lstgetCaseDetails.size() > 0 , 'Inserted cases we can able to retrive');

         // Testing getPickListValues method getPickListValues
         try{
          List<DMLOperationsHandler.FetchValueWrapper>  objgetPickListValues = referralProviderHeaderController.getPickListValues(objNewCase,'Origin');
         }
         catch (Exception ex) {
 
           }
    }

    //Testing Negative flow
    @isTest static void testreferralProviderHeaderControllerNegative()
    {
         //Inserting records into case object where status=New               
         Case objNewCase=new Case(Program__c = ''); 

         // Testing getIdAfterInsertSOQL by passing one record in case object
         Id refferalId= referralProviderHeaderController.getIdAfterInsertSOQL(objNewCase);

         // Testing getCaseHeaderDetails by passing id which we got from above method
         List<Case> lstgetCaseDetails= referralProviderHeaderController.getCaseHeaderDetails((string)refferalId);
    }
}