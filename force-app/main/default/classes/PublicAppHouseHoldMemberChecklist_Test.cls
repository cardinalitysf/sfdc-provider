// @Author        : JayaChandran s
// @CreatedOn     : June 8 ,2020
//@Purpose        : Test for PublicAppHouseHoldMemberChecklist_Test
@IsTest
private with sharing class PublicAppHouseHoldMemberChecklist_Test {
    @IsTest static void testPublicAppHouseHoldMemberChecklist(){

        Application__c InsertobjNewApplication=new Application__c(); 
                insert InsertobjNewApplication;

        Account InsertobjNewAccount=new Account(Name = 'Test Account'); 
                insert InsertobjNewAccount;

        ProviderRecordAnswer__c   InsertobjInProviderRecordAnswer =new ProviderRecordAnswer__c();
                insert InsertobjInProviderRecordAnswer;

                string str1 = '[{"id":"'+InsertobjInProviderRecordAnswer.Id+'","Date__c":"2020-06-10","Comments__c":"'+InsertobjInProviderRecordAnswer.Comments__c+'","ProviderRecordQuestion__c":"'+InsertobjInProviderRecordAnswer.ProviderRecordQuestion__c+'","HouseholdStatus__c":"'+InsertobjInProviderRecordAnswer.HouseholdStatus__c+'", "Actor__c":"'+InsertobjInProviderRecordAnswer.Actor__c+'","Comar__c":"'+InsertobjInProviderRecordAnswer.Comar__c+'"}]';
              
                try
                {
                        PublicAppHouseholdMemberChecklist.getContactHouseHoldMembersDetails(null);
                }catch(Exception Ex)
                {
                    
                }
                        PublicAppHouseholdMemberChecklist.getContactHouseHoldMembersDetails(InsertobjNewAccount.Id);

                try {
                        PublicAppHouseholdMemberChecklist.getContactHouseHoldMemberAddress(null);
                } catch (Exception Ex) {
                        
                }
                PublicAppHouseholdMemberChecklist.getContactHouseHoldMemberAddress(InsertobjNewAccount.Id);
                try {
                        PublicAppHouseholdMemberChecklist.getQuestionsByType();
                } catch (Exception Ex) {
                        
                }
               
                        PublicAppHouseHoldMemberChecklist.getRecordType('Household Member');

                try {
                        PublicAppHouseHoldMemberChecklist.getRecordQuestionId(null, null);
                } catch (Exception Ex) {
                        
                }
                        PublicAppHouseHoldMemberChecklist.getRecordQuestionId(InsertobjNewApplication.Id, '');

                try {
                        PublicAppHouseHoldMemberChecklist.bulkAddRecordAns(null);
                } catch (Exception Ex) {
                        
                }
                        PublicAppHouseHoldMemberChecklist.bulkAddRecordAns(str1);

                try {
                        PublicAppHouseHoldMemberChecklist.getProviderAnswer(null);
                } catch (Exception Ex) {
                        
                }
                        PublicAppHouseHoldMemberChecklist.getProviderAnswer(InsertobjInProviderRecordAnswer.Id);

                try {
                        PublicAppHouseholdMemberChecklist.getUploadedDocuments(null);
                } catch (Exception Ex) {
                        
                }
                        PublicAppHouseholdMemberChecklist.getUploadedDocuments(InsertobjNewApplication.Id);
            }
        }
        