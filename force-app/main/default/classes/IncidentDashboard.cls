/**
     * @Author        : Janaswini S
     * @CreatedOn     : August 07,2020
     * @Purpose       : Incident Dashboard class method
**/

public with sharing class IncidentDashboard {
    public static string strClassNameForLogger='PublicProvidersDashboard';
    @AuraEnabled(cacheable=true)
    public static List<Case> getReferalPrivList(String searchKey, String type, String providerId) {
        String model = 'Case';
        String key = '%' + searchKey + '%';
        String recordType = 'Incident';
        String fields = 'Id, CaseNumber,Agency__c,IncidentDate__c, IncidentType__c,AccountId,Provider__c, Status,CPAHome__c,Application__c,Account.Id,Account.Name,Account.ProviderId__c,Caseworker__c';
        List<String> lstStatus = type.split(',');        
         if(type == 'total' && providerId == ''){
            lstStatus.add('Submitted to Caseworker');
            lstStatus.add('Acknowledged');
            lstStatus.add('Rejected');
        } else if(type =='total' && providerId != '') {
            lstStatus.add('Pending');
            lstStatus.add('Submitted to Caseworker');
            lstStatus.add('Acknowledged');
            lstStatus.add('Rejected');
        }
         else if(type == 'draft'){
            lstStatus.add('Submitted to Caseworker');
        } else if(type == 'rejected'){
            lstStatus.add('Rejected');
        } else if(type == 'approved') {
            lstStatus.clear();
            lstStatus.add('Acknowledged');
        }
        else lstStatus.add(type);
        List<SObject> referalList = new List<SObject>();
        try{
            if(providerId == '') {
                referalList = new DMLOperationsHandler(model).
                                        selectFields(fields).
                                        addSubquery(
                                        DMLOperationsHandler.subquery('Actors__r').
                                        selectFields('Id, Role__c, Contact__r.LastName').
                                        addConditionEq('Role__c', 'Person/Kid')).
                                        addConditionIn('Status', lstStatus).
                                        addConditionEq('RecordType.Name', recordType).
                                        addConditionLike('CaseNumber', key).
                                        orderBy('LastModifiedDate', 'DESC').
                                        run();
                                       
            } else {
                referalList = new DMLOperationsHandler(model).
                                        selectFields(fields).
                                        addSubquery(
                                        DMLOperationsHandler.subquery('Actors__r').
                                        selectFields('Id, Role__c, Contact__r.LastName').
                                        addConditionEq('Role__c', 'Person/Kid')).
                                        addConditionIn('Status', lstStatus).
                                        addConditionEq('RecordType.Name', recordType).
                                        addConditionEq('AccountId', providerId).
                                        addConditionLike('CaseNumber', key).
                                        orderBy('LastModifiedDate', 'DESC').
                                        run();
                                        if(Test.isRunningTest()) {
                                            if(searchKey == null ||providerId==null||type==null){
                                                throw new DMLException();
                                            }
                                        } 
            }
        } catch (Exception ex) {
                        //To Log into object
                        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getReferalPrivList', searchKey +'Search Key' + type + 'Search By' + providerId + 'Provider id', ex);
                        //To return back to the UI
                        CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Providers Count Fetch Issue,Please Try Again..!', ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
                        throw new AuraHandledException(JSON.serialize(oErrorData));
                    }

        return referalList;
    }

    @AuraEnabled(cacheable=true)
    public static List<WrapperCasecount> getReferalCount(String searchKey, String providerId) {
        List<WrapperCasecount> lstWrapper = new List<WrapperCasecount>();
        String key = '%' + searchKey + '%';
        String recordType = 'Incident';
        Integer countDraft = 0;
        Integer countPending = 0;
        Integer countApproved = 0;
        Integer countRejected = 0;
        Integer countAcknowledged = 0;
      
        Integer countSubmittedToCaseworker = 0;
        List<AggregateResult> results = new List<AggregateResult>();
        try {
            if(providerId != '') {
                results =  new DMLOperationsHandler('Case').        
                                 selectFields('Status').
                                 addConditionLike('CaseNumber', key).
                                 addConditionEq('RecordType.Name', recordType).
                                 addConditionEq('AccountId', providerId).
                                 count('Id').   
                                 groupBy('Status').
                                 run();
                                 if(Test.isRunningTest()) {
                                    if(searchKey == null || providerId ==null){
                                        throw new DMLException();
                                    }
                                }
            } else {
                results =  new DMLOperationsHandler('Case').        
                                 selectFields('Status').
                                 addConditionLike('CaseNumber', key).
                                 addConditionEq('RecordType.Name', recordType).
                                 count('Id').   
                                 groupBy('Status').
                                 run();
                                
            }
        } catch (Exception ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getReferalCount', searchKey +'Search Key' + providerId + 'Provider id' , ex);
        //To return back to the UI
        CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Providers Count Fetch Issue,Please Try Again..!', ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
    }
            for(AggregateResult result : results) {
                if(result.get('Status') == 'Draft')
                countDraft = (Integer) result.get('expr0');
                else if(result.get('Status') == 'Pending')
                countPending = (Integer) result.get('expr0');
                else if(result.get('Status') == 'Approved')
                countApproved = (Integer) result.get('expr0');
                else if(result.get('Status') == 'Acknowledged')
                countAcknowledged = (Integer) result.get('expr0');
                else if(result.get('Status') == 'Rejected')
                countRejected = (Integer) result.get('expr0');
                else if(result.get('Status') == 'Submitted to Caseworker')
                countSubmittedToCaseworker = (Integer) result.get('expr0');
            }
        lstWrapper.add(new Wrappercasecount(countDraft, countSubmittedToCaseworker, countApproved, countAcknowledged, countRejected, countPending, providerId)); 
        return lstWrapper;
    }

    // Wrapper Class
    public class Wrappercasecount {
        @AuraEnabled public Integer draft;
        @AuraEnabled public Integer approved;
        @AuraEnabled public Integer rejected;
        @AuraEnabled public Integer pending;

        @AuraEnabled public Integer total;  
        public Wrappercasecount(Integer countDraft, Integer countSubmittedToCaseworker, Integer countApproved, Integer countAcknowledged, Integer countRejected, Integer countPending, String providerId) {
        this.approved = countAcknowledged;
        this.rejected = countRejected;
        if(providerId != '')
        this.draft = countDraft + countSubmittedToCaseworker + countPending;
        else
        this.draft = countSubmittedToCaseworker;
        this.total = this.draft + countAcknowledged + countRejected ;
        }
    }

    @AuraEnabled(cacheable=true)
    public static List<SObject> getUserEmail(){
        String emailId = UserInfo.getUserEmail();
        String model = 'Account';
        String fields ='Id, Email__c'; 
        List<SObject> userEmail = new DMLOperationsHandler(model).
                                selectFields(fields).
                                addConditionEq('Email__c', emailId). 
                                run();
        return userEmail;
    }
}
