
public class APIAIResponse {
	public class Fulfillment {
		public String speech {get;set;} 
		public List<Messages> messages {get;set;} 
	
		public Fulfillment(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'speech') {
							speech = parser.getText();
						} else if (text == 'messages') {
							messages = arrayOfMessages(parser);
						} else {
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Status {
		public Integer code {get;set;} 
		public String errorType {get;set;} 

		public Status(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'code') {
							code = parser.getIntegerValue();
						} else if (text == 'errorType') {
							errorType = parser.getText();
						} else {
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Parameters {
		public String ApplicationTypes {get;set;} 
        public String AppNumber {get;set;} 
        public String ProviderName {get;set;} 
		public String programtype {get;set;} 


		public Parameters(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'ApplicationTypes') {
							ApplicationTypes = parser.getText();
						} else if (text == 'AppNumber') {
							AppNumber = parser.getText();
                        }
                        if (text == 'ProviderName') {
							ProviderName = parser.getText();
						}
						if (text == 'programtype') {
							programtype = parser.getText();
						} if (text == 'number') {
							AppNumber = parser.getText();
						}
						else {
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Messages {
		public String lang {get;set;} 
		public Integer type_Z {get;set;} // in json: type
		public String speech {get;set;} 

		public Messages(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'lang') {
							lang = parser.getText();
						} else if (text == 'type') {
							type_Z = parser.getIntegerValue();
						} else if (text == 'speech') {
							speech = parser.getText();
						} else {
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Metadata {
		public String intentId {get;set;} 
		public String intentName {get;set;} 
		public String webhookUsed {get;set;} 
		public String webhookForSlotFillingUsed {get;set;} 
		public String isFallbackIntent {get;set;} 

		public Metadata(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'intentId') {
							intentId = parser.getText();
						} else if (text == 'intentName') {
							intentName = parser.getText();
						} else if (text == 'webhookUsed') {
							webhookUsed = parser.getText();
						} else if (text == 'webhookForSlotFillingUsed') {
							webhookForSlotFillingUsed = parser.getText();
						} else if (text == 'isFallbackIntent') {
							isFallbackIntent = parser.getText();
						} else {
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public String id {get;set;} 
	public String lang {get;set;} 
	public String sessionId {get;set;} 
	public String timestamp {get;set;} 
	public Result result {get;set;} 
	public Status status {get;set;} 

	public APIAIResponse(JSONParser parser) {
		while (parser.nextToken() != System.JSONToken.END_OBJECT) {
			if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
				String text = parser.getText();
				if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
					if (text == 'id') {
						id = parser.getText();
					} else if (text == 'lang') {
						lang = parser.getText();
					} else if (text == 'sessionId') {
						sessionId = parser.getText();
					} else if (text == 'timestamp') {
						timestamp = parser.getText();
					} else if (text == 'result') {
						result = new Result(parser);
					} else if (text == 'status') {
						status = new Status(parser);
					} else {
						consumeObject(parser);
					}
				}
			}
		}
	}
	
	public class Contexts {

		public Contexts(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						{
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Result {
		public String source {get;set;} 
		public String resolvedQuery {get;set;} 
		public String action {get;set;} 
		public Boolean actionIncomplete {get;set;} 
		public Integer score {get;set;} 
		public Parameters parameters {get;set;} 
		public List<Contexts> contexts {get;set;} 
		public Metadata metadata {get;set;} 
		public Fulfillment fulfillment {get;set;} 

		public Result(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'source') {
							source = parser.getText();
						} else if (text == 'resolvedQuery') {
							resolvedQuery = parser.getText();
						} else if (text == 'action') {
							action = parser.getText();
						} else if (text == 'actionIncomplete') {
							actionIncomplete = parser.getBooleanValue();
						} else if (text == 'score') {
							score = parser.getIntegerValue();
						} else if (text == 'parameters') {
							parameters = new Parameters(parser);
						} else if (text == 'contexts') {
							contexts = arrayOfContexts(parser);
						} else if (text == 'metadata') {
							metadata = new Metadata(parser);
						} else if (text == 'fulfillment') {
							fulfillment = new Fulfillment(parser);
						} else {
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	
	public static APIAIResponse parse(String json) {
		System.JSONParser parser = System.JSON.createParser(json);
		return new APIAIResponse(parser);
	}
	
	public static void consumeObject(System.JSONParser parser) {
		Integer depth = 0;
		do {
			System.JSONToken curr = parser.getCurrentToken();
			if (curr == System.JSONToken.START_OBJECT || 
				curr == System.JSONToken.START_ARRAY) {
				depth++;
			} else if (curr == System.JSONToken.END_OBJECT ||
				curr == System.JSONToken.END_ARRAY) {
				depth--;
			}
		} while (depth > 0 && parser.nextToken() != null);
	}
	



    private static List<Contexts> arrayOfContexts(System.JSONParser p) {
        List<Contexts> res = new List<Contexts>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Contexts(p));
        }
        return res;
    }




    private static List<Messages> arrayOfMessages(System.JSONParser p) {
        List<Messages> res = new List<Messages>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Messages(p));
        }
        return res;
    }
}