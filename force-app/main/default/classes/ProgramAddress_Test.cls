/**
 * @Author        : Janaswini S
 * @CreatedOn     : June 10 ,2020
 * @Purpose       : Test Methods to unit test ProgramAddress class
 **/
        @isTest private class ProgramAddress_Test {
       Static testmethod void testgetAddress() {
        Address__c InsertobjNewAddress=new Address__c(AddressLine1__c = '345 California St'); 
        insert InsertobjNewAddress;
        Application__c InsertobjNewApplication=new Application__c(Status__c = 'Pending'); 
        insert InsertobjNewApplication;
        Account InsertobjNewAccount=new Account(Name = 'test Account'); 
        insert InsertobjNewAccount;
        ReferenceValue__c InsertobjReferenceValue=new ReferenceValue__c(); 
        insert InsertobjReferenceValue;
        
        ProgramAddress.getStateDetails();

        try {
                ProgramAddress.getProgramAdress('');
        } catch (Exception e) {

        }
        ProgramAddress.getProgramAdress('InsertobjNewApplication.Id');
//        ProgramAddress.getAddress();
        try {
                ProgramAddress.getProviderApplicationStatus(null);
        } catch (Exception e) {

        }
       ProgramAddress.getProviderApplicationStatus(InsertobjNewApplication.Id);
//       ProgramAddress.getProgramAdressRowId('');

        try {
        ProgramAddress.getSearchAddress('','', '');

        } catch (Exception e) {

        }
        ProgramAddress.getSearchAddress('InsertobjNewApplication.searchQuery','InsertobjNewAccount.Id','InsertobjNewApplication.Id');

        // try {
        //         ProgramAddress.setProgramAdress('');

        
        //         } catch (Exception e) {
        
        //         }
        ProgramAddress.setProgramAdress(InsertobjNewAddress);
      }
}
