/**
 * Author   : Pratheeba.V
 * Date     : 29-06-2020
 * Purpose  : Test class for AccoutTrigger
 */
@isTest
public with sharing class accountTrigger_Test {
    @isTest(SeeAllData=false)
    private static void testAccountTrigger() {
            List<Account> accList = new List<Account>();
            Account testAccount;
            for(Integer i=0; i<5 ;i++){
                testAccount = new Account(Name = 'Test');
                accList.add(testAccount);
            }
            insert accList;
            for(Account rec : accList){
                rec.Status__c = 'InProgress';
            }
            Test.startTest();
            update accList;
            Test.stopTest();
        }
        private static void testDeleteAccountMethod() {
            List<Account> accList = new List<Account>();
            Account testAccount;
            for(Integer i=0; i<5 ;i++){
                testAccount = new Account(Name = 'Test');
                accList.add(testAccount);
            }
            insert accList;
            Test.startTest();
            delete accList;
            Test.stopTest();
        }
        private static void testUnDeleteAccountMethod() {
            List<Account> accList = new List<Account>();
            Account testAccount;
            for(Integer i=0; i<5 ;i++){
                testAccount = new Account(Name = 'Test');
                accList.add(testAccount);
            }
            insert accList;
            delete accList;
            Test.startTest();
            undelete accList;
            Test.stopTest();
        }
    }
