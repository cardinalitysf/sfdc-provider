/**
 * @Author        : Naveen
 * @CreatedOn     : June 12 ,2020
 * @Purpose       : Responsible for fetch or upsert the Decison and Signature Details.
 * @UpdatedBy     : Sindhu Venkateswarlu
 * @UpdatedOn     : July 23,2020
**/

@isTest
public with sharing class ApplicationProviderDecision_Test {
    @isTest static void testupdateapplicationdetails (){
        Profile prf = [Select Id, Name from Profile where Name= 'Supervisor'];
        List<User> usrList = TestDataFactory.createTestUsers( 2, prf.Id,true );
        List<Account> lstAcct= TestDataFactory.testAccountData();
        insert lstAcct;
        for(Account acct : lstAcct)
        {
           
        }
        List<Application__c> appList = TestDataFactory.TestApplicationData(2,'Rejected');
        insert appList;
        for(Account acct : lstAcct)
        {
           
        }
        List<Monitoring__c> monList = TestDataFactory.createTestMonitoringData(5,true);
        update monList;
        List<ContentVersion> cvList = TestDataFactory.createTestContentVersionData();
        insert cvList;
        SObject sobj = (SObject)Type.forName('ContentVersion').newInstance();
        List<DMLOperationsHandler.FetchValueWrapper> strgetPicklistvalues=applicationproviderDecision.getPickListValues(sobj,'Document__c');
         String str = 'Application__c';
        //Casting String to sObject
        sObject obj = Schema.getGlobalDescribe().get(str).newSObject();
        obj.put('ApprovalComments__c', 'test Comments');
        obj.put('ApprovalStatus__c', 'Caseworker Submitted');
        obj.put('Capacity__c', 10);
        obj.put('Id', appList[0].Id);
        obj.put('Provider__c', lstAcct[0].Id);
        obj.put('ProgramType__c','');
        obj.put('Comments__c','test 1');
        obj.put('Gender__c','male');
        obj.put('MaxAge__c',18);
        obj.put('MinAge__c',3);
        obj.put('MaxIQ__c',200);
        obj.put('MinIQ__c',1);
        obj.put('Narrative__c','text');
        obj.put('Status__c','Caseworker Submitted');
        applicationproviderDecision.updateapplicationdetails(obj,usrList[0].Id,obj.Id);
        applicationproviderDecision.updateapplicationdetailsDraft(obj);
        applicationproviderDecision.getReviewDetails(obj.Id);
        applicationproviderDecision.getmonitoringDetails(obj.Id);
        applicationproviderDecision.getSignatureDetails(obj.Id);
        applicationproviderDecision.updateCaseworkerdetails(obj);
        applicationproviderDecision.getAllUsersList();
        applicationproviderDecision.saveSign('test',appList[0].Id);
       applicationproviderDecision.getDatatableDetails(appList[0].Id);

    }
     @isTest static void testupdateapplicationdetailsTwo (){
        Profile prf = [Select Id, Name from Profile where Name= 'Caseworker'];
        List<User> usrList = TestDataFactory.createTestUsers( 2, prf.Id,true );
        List<Account> lstAcct= TestDataFactory.testAccountData();
        insert lstAcct;
        for(Account acct : lstAcct)
        {
           
        }
        List<Application__c> appList = TestDataFactory.TestApplicationData(2,'Rejected');
        insert appList;
        for(Account acct : lstAcct)
        {
           
        }
        List<Monitoring__c> monList = TestDataFactory.createTestMonitoringData(5,true);
        update monList;
        List<ContentVersion> cvList = TestDataFactory.createTestContentVersionData();
        insert cvList;
        SObject sobj = (SObject)Type.forName('ContentVersion').newInstance();
        List<DMLOperationsHandler.FetchValueWrapper> strgetPicklistvalues=applicationproviderDecision.getPickListValues(sobj,'Document__c');
         String str = 'Application__c';
        //Casting String to sObject
        sObject obj = Schema.getGlobalDescribe().get(str).newSObject();
        obj.put('ApprovalComments__c', 'test Comments');
        obj.put('ApprovalStatus__c', 'Approved');
        obj.put('Capacity__c', 10);
        obj.put('Id', appList[0].Id);
        obj.put('Provider__c', lstAcct[0].Id);
        obj.put('ProgramType__c','');
        obj.put('Comments__c','test 1');
        obj.put('Gender__c','male');
        obj.put('MaxAge__c',18);
        obj.put('MinAge__c',3);
        obj.put('MaxIQ__c',200);
        obj.put('MinIQ__c',1);
        obj.put('Narrative__c','text');
        obj.put('Status__c','Approved');
        obj.put('Caseworker__c',usrList[0].Id);
        //applicationproviderDecision.updateapplicationdetails(obj,usrList[0].Id,obj.Id);
       // applicationproviderDecision.updateapplicationdetailsDraft(obj);
        applicationproviderDecision.getReviewDetails(obj.Id);
        applicationproviderDecision.getmonitoringDetails(obj.Id);
        applicationproviderDecision.getSignatureDetails(obj.Id);
        applicationproviderDecision.updateCaseworkerdetails(obj);
        applicationproviderDecision.getAllUsersList();
        applicationproviderDecision.saveSign('test',appList[0].Id);
        applicationproviderDecision.getDatatableDetails(appList[0].Id);

        SObject sObjt = (SObject)Type.forName('Application__c').newInstance();
  
        applicationproviderDecision.processingWrapperString appParse1= new applicationproviderDecision.processingWrapperString(sObjt,'test');        
    }
    @IsTest static void testprovidersNegative1()
    {
        Profile prf = [Select Id, Name from Profile where Name= 'Supervisor'];
        List<User> userList =  TestDataFactory.createTestUsers(1, prf.Id, true);
        Application__c InsertobjApplication=new Application__c(Status__c ='Caseworker Submitted',Caseworker__c=userList[0].id,ReviewStatus__c='Submit for Review',SupervisorReview__c='Approved'); 
        insert InsertobjApplication;
      
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments('Submitting request for approval.');
        req.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        req.setObjectId(InsertobjApplication.Id);
        Approval.ProcessResult resu = Approval.process(req);
        System.assert(resu.isSuccess());
        System.assertEquals('Pending', resu.getInstanceStatus(),'Instance Status'+resu.getInstanceStatus());
        List<Id> newWorkItemIds = resu.getNewWorkitemIds();
        applicationproviderDecision.updateapplicationdetails(InsertobjApplication,'',newWorkItemIds.get(0));
    }
    @IsTest static void testprovidersNegative2()
    {
        Profile prf = [Select Id, Name from Profile where Name= 'Supervisor'];
        List<User> userList =  TestDataFactory.createTestUsers(1, prf.Id, true);
        Application__c InsertobjApplication=new Application__c(Status__c ='Caseworker Submitted',Caseworker__c=userList[0].id,ReviewStatus__c='Submit for Review',SupervisorReview__c='Rejected'); 
        insert InsertobjApplication;
      
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments('Submitting request for approval.');
        req.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        req.setObjectId(InsertobjApplication.Id);
        Approval.ProcessResult resu = Approval.process(req);
        System.assert(resu.isSuccess());
        System.assertEquals('Pending', resu.getInstanceStatus(),'Instance Status'+resu.getInstanceStatus());
        List<Id> newWorkItemIds = resu.getNewWorkitemIds();
        applicationproviderDecision.updateapplicationdetails(InsertobjApplication,'',newWorkItemIds.get(0));
    }
    @IsTest static void testprovidersNegative3()
    {
        Profile prf = [Select Id, Name from Profile where Name= 'Supervisor'];
        List<User> userList =  TestDataFactory.createTestUsers(1, prf.Id, true);
        Application__c InsertobjApplication=new Application__c(Status__c ='Caseworker Submitted',Caseworker__c=userList[0].id,ReviewStatus__c='Submit for Review',SupervisorReview__c='Return to Worker'); 
        insert InsertobjApplication;
      
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments('Submitting request for approval.');
        req.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        req.setObjectId(InsertobjApplication.Id);
        Approval.ProcessResult resu = Approval.process(req);
        System.assert(resu.isSuccess());
        System.assertEquals('Pending', resu.getInstanceStatus(),'Instance Status'+resu.getInstanceStatus());
        List<Id> newWorkItemIds = resu.getNewWorkitemIds();
        applicationproviderDecision.updateapplicationdetails(InsertobjApplication,'',newWorkItemIds.get(0));
    }
    
    
}