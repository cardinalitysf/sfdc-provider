/**
 * @Author        : Preethi Bojarajan
    * @CreatedOn     : July 22, 2020
    * @Purpose       : Test class for PublicApplicationReferenceChecks.cls
    **/

        @isTest
        public with sharing class PublicApplicationReferenceChecks_Test {
        @isTest
        private static void getRecordType_Test(){
            Test.startTest();
            try {
                PublicApplicationReferenceChecks.getRecordType('Reference Check');
                PublicApplicationReferenceChecks.getRecordType(null);
            } catch(Exception e){}
            Test.stopTest();
        }

        @isTest
        private static void getMultiplePicklistValues_Test() {
            Test.startTest();
            PublicApplicationReferenceChecks.getMultiplePicklistValues('ActorDetails__c','TypeofContact__c');
            Test.stopTest();
        }

        @isTest
        private static void getHouseHoldMembers_Test(){
            Test.startTest();
            try {
                PublicApplicationReferenceChecks.getHouseHoldMembers('a');
                PublicApplicationReferenceChecks.getHouseHoldMembers(null);
            } catch(Exception e){}
            Test.stopTest();
        }
        
        @isTest
        private static void fetchRefCheckedMemDetails_Test(){
            Test.startTest();
            try {
                PublicApplicationReferenceChecks.fetchRefCheckedMemDetails('a','a');
                PublicApplicationReferenceChecks.fetchRefCheckedMemDetails('','');
                PublicApplicationReferenceChecks.fetchRefCheckedMemDetails(null,null);
            } catch(Exception e){}
            Test.stopTest();
        }
        
        @isTest
        private static void fetchAppRefCheckDetails_Test(){
            Test.startTest();
            try {
                PublicApplicationReferenceChecks.fetchAppRefCheckDetails('a','Address__c');
                PublicApplicationReferenceChecks.fetchAppRefCheckDetails('a',null);
                PublicApplicationReferenceChecks.fetchAppRefCheckDetails(null,null);
            } catch(Exception e){}
            Test.stopTest();
        }

        @isTest
        private static void getStateDetails_Test(){
            Test.startTest();
            try {
                PublicApplicationReferenceChecks.getStateDetails();
            } catch(Exception e){}
            Test.stopTest();
        }
    }
