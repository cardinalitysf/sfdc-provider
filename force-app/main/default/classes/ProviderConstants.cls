public with sharing class ProviderConstants {
    public ProviderConstants() {

    }
    public static final string SUCCESS='SUCCESS';
    public static final string ERROR='ERROR';
    public static final string INSERTSUCCESS='SUCCESSFULLY INSERTED';
    public static final string UPDATESUCCESS='SUCCESSFULLY UPDATED';
    public static final string DELETESUCCESS='SUCCESSFULLY DELETED';
    public static final string APPROVED='APPROVED';
    public static final string INACTIVE='Inactive';
    public static final string ACTIVE='Active';
    public static final string CASEWORKERSUBMITTED='caseworker submitted';
    public static final string APPLICATION_ACTIVITY_INCOMPLETE='INCOMPLETE';
    public static final string APPLICATION_ACTIVITY_PENDING='PENDING';
    public static final string APPLICATION_ACTIVITY_SUCCESS_TODISPLAY='Completed';
    public static final string APPLICATION_ACTIVITY_DRAFT_TODISPLAY='Draft';
    public static final string LICENSEISSUED='License Issued';
    public static final string LICENSEEXPIRED='License Expired';
    public static final string APPLICANT='Applicant';
    public static final string COAPPLICANT='Co-Applicant';
    public static final string MEETINGINFO_COMPLETED='Completed';
    public static final string MEETINGINFO_DNA='DNA';
    public static final string MEETINGINFO_SCHEDULED='Scheduled';
    public static final string MEETINGINFO_CANCELLED='Cancelled';
    public static final string CASE_TRAINING_STATUS_COMPLETED='Training Completed';
    public static final string CASE_TRAINING_STATUS_UNATTENTED='Training Unattended';
    public static final string TRAINING_TYPE_INFORMATION_MEETING='Information Meeting';
    public static final string TRAINING_TYPE_PRE_SERVICE_TRAINING='Pre-Service Training';
    public static final string USER_PROFILE_SA = 'System Administrator';
    public static final string USER_PROFILE_SP = 'Supervisor';
    public static final string USER_PROFILE_CW = 'Caseworker';
    public static final string HISTORY_DESCRIPTION = 'Description';
    public static final string HISTORY_SUMMARY = 'Summary';
    public static final string HISTORY_OUTCOMES = 'Outcomes';
    // Referred in case trigger handler
    public static final string CASE_RECORDTYPE_REFERRAL = 'Referral';
    public static final string CASE_RECORDTYPE_COMPLAINTS = 'Complaints';

}