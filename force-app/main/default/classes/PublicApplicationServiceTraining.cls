/**
 * @Author        : Pratheeba V
 * @CreatedOn     : June 6, 2020
 * @Purpose       : Public Application Service Training tab 
 **/
public with sharing class PublicApplicationServiceTraining {
    public static string strClassNameForLogger='PublicApplicationServiceTraining';
    @AuraEnabled(cacheable=false)
    public static List<Actor__c > getHouseholdMembers(string providerid) {
        List<Actor__c> householdMemberObj = new List<Actor__c>();
        try {
            householdMemberObj = new DMLOperationsHandler('Actor__c').
                                    selectFields('Contact__r.Name,Contact__r.Id, Id, Role__c').
                                    addConditionEq('Provider__c ', providerid).
                                    run();
            if(Test.isRunningTest()) {
                if(providerid=='') {
                    throw new DMLException();
                }				
			}
        } catch (Exception Ex) { 
            String[] arguments = new String[] {providerid};
            string strInputRecord= String.format('Input parameters are :: providerid -- {0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getHouseholdMembers',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Members List',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return householdMemberObj;
    }

    @AuraEnabled(cacheable=false)
    public static List<MeetingInfo__c> getTrainingDetails(string param) {
        List<MeetingInfo__c> meetingInfoObj = new List<MeetingInfo__c>();
        Set<String> fields = new Set<String>{'Id','Training__c','Training__r.Name','SessionNumber__c','Date__c','StartTime__c','EndTime__c','Training__r.Description__c','Training__r.Duration__c'};
        try {
            meetingInfoObj = new DMLOperationsHandler('MeetingInfo__c').
            selectFields(fields).
            addConditionEq('Training__r.SessionType__c', param).
            addConditionEq('Training__r.RecordType.Name', 'Public').
            addConditionEq('Training__r.Status__c', 'Scheduled').
            addConditionGe('Date__c', Date.today()).
            orderBy('CreatedDate', 'Asc').
            run();
            if(Test.isRunningTest()) {
                if(param=='') {
                    throw new DMLException();
                }				
            }
        } catch (Exception Ex) { 
            String[] arguments = new String[] {param};
            string strInputRecord= String.format('Input parameters are :: param -- {0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getTrainingDetails',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Training List',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return meetingInfoObj;
    }
     
    @AuraEnabled(cacheable=false)
    public static List<SessionAttendee__c> getSessionTraining(string applicationid) {
        List<SessionAttendee__c> sessionDetailsObj = new List<SessionAttendee__c>();
        Set<String> fields = new Set<String>{'Id','MeetingInfo__c','MeetingInfo__r.Training__r.Name','MeetingInfo__r.Date__c','MeetingInfo__r.Training__r.Duration__c','MeetingInfo__r.Training__r.SessionType__c','MeetingInfo__r.SessionNumber__c','TotalHoursAttended__c','Actor__r.Contact__r.Name','Actor__r.Role__c','MeetingInfo__r.Training__r.Jurisdiction__c','IntenttoAttend__c','SessionStatus__c'};
        try {
            sessionDetailsObj = new DMLOperationsHandler('SessionAttendee__c').
            selectFields(fields).
            addConditionEq('Application__c', applicationid).
            orderBy('CreatedDate', 'Desc').
            run();
            if(Test.isRunningTest()) {
                if(applicationid=='') {
                    throw new DMLException();
                }				
            }
        } catch (Exception Ex) { 
            String[] arguments = new String[] {applicationid};
            string strInputRecord= String.format('Input parameters are :: applicationid -- {0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getSessionTraining',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Session Training List',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return sessionDetailsObj;
    }

    @AuraEnabled(cacheable=false)
    public static List<SessionAttendee__c> viewSessionDetails(String sessionid) {
        List<SessionAttendee__c> sessionObj = new List<SessionAttendee__c>();
        Set<String> fields = new Set<String>{'Id','MeetingInfo__r.Training__r.Name','MeetingInfo__r.Training__r.Duration__c','MeetingInfo__r.Training__r.Description__c','MeetingInfo__r.Training__r.Description__c','MeetingInfo__r.Training__r.Date__c','MeetingInfo__r.Training__r.SessionType__c','MeetingInfo__r.Training__r.StartTime__c','MeetingInfo__r.Training__r.EndTime__c','MeetingInfo__r.SessionNumber__c','TotalHoursAttended__c','Actor__r.Contact__r.Name','Actor__r.Role__c','MeetingInfo__r.Training__r.Jurisdiction__c','IntenttoAttend__c','SessionStatus__c'};
        try {
            sessionObj = new DMLOperationsHandler('SessionAttendee__c').
            selectFields(fields).
            addConditionEq('Id', sessionid).
            run();
            if(Test.isRunningTest()) {
                if(sessionid=='') {
                    throw new DMLException();
                }				
            }
        } catch (Exception Ex) { 
            String[] arguments = new String[] {sessionid};
            string strInputRecord= String.format('Input parameters are :: sessionid -- {0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'viewSessionDetails',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Session List',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return sessionObj;
    }

    @AuraEnabled(cacheable=true)
    public static List<Actor__c> selectActor(string applicationid, string providerid) {
        List<Actor__c> actorId = new List<Actor__c>();
        Set<String> fields = new Set<String>{'Id'};
        try {
            actorId = new DMLOperationsHandler('Actor__c').
            selectFields(fields).
            addConditionEq('Application__c', applicationid).
            addConditionEq('Provider__c', providerid).
            addConditionEq('Role__c', 'Applicant').
            run();
            if(Test.isRunningTest()) {
                if(applicationid=='' || providerid=='') {
                    throw new DMLException();
                }				
            }
        } catch (Exception Ex) { 
            String[] arguments = new String[] {applicationid,providerid};
            string strInputRecord= String.format('Input parameters are :: applicationid -- {0},providerid--{1}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'selectActor',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Actor List',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return actorId;
    }

    @AuraEnabled
    public static void InsertUpdateSessionInfo(string strTrainingDetails){
        List<SessionAttendee__c> lstMeetingToParse = (List<SessionAttendee__c>)System.JSON.deserialize(strTrainingDetails, List<SessionAttendee__c>.class);
        List<SessionAttendee__c> lstMeetingToInsert = new List<SessionAttendee__c> ();
        for(SessionAttendee__c meeting : lstMeetingToParse) {
            SessionAttendee__c intMeetingInfo = new SessionAttendee__c();
            intMeetingInfo.Actor__c = meeting.Actor__c;
            intMeetingInfo.Training__c = meeting.Training__c;
            intMeetingInfo.MeetingInfo__c = meeting.MeetingInfo__c;
            intMeetingInfo.SessionStatus__c = meeting.SessionStatus__c;
            intMeetingInfo.Application__c = meeting.Application__c;
            intMeetingInfo.TotalHoursAttended__c = meeting.TotalHoursAttended__c;
            lstMeetingToInsert.add(intMeetingInfo);
        }
        DMLOperationsHandler.updateOrInsertSOQLForList(lstMeetingToInsert);
    }

    @AuraEnabled
    public static List<AggregateResult> getSessionAttendedHours(string applicationid) {
        string role='Applicant';
        string status = 'Completed';
        List<AggregateResult> groupedResults =  new List<AggregateResult>();
        try {
            groupedResults =  new DMLOperationsHandler('SessionAttendee__c').
                                                    selectField('Actor__r.Contact__r.Name').   
                                                    sum('TotalHoursAttended__c', 'totalhours').
                                                    addConditionEq('Actor__r.Role__c',role).
                                                    addConditionEq('SessionStatus__c',status).
                                                    addConditionEq('Application__c',applicationid).
                                                    groupBy('Actor__r.Contact__r.Name').
                                                    aggregate();  
            if(Test.isRunningTest()) {
                if(applicationid=='') {
                    throw new DMLException();
                }				
            }
        } catch (Exception Ex) { 
            String[] arguments = new String[] {applicationid};
            string strInputRecord= String.format('Input parameters are :: applicationid -- {0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getSessionAttendedHours',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Attended Hours',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }     
        return groupedResults;
    }
}
