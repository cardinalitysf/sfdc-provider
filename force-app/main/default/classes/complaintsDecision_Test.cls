/**
 * @Author        : Pratheeba.V
 * @CreatedOn     : July 20 ,2020
 * @Purpose       : Test Class for Complaint Decision Class
**/
@IsTest
private class complaintsDecision_Test {
    @isTest static void testComplaintsDecisionPositive(){
        Case InsertobjCase=new Case(Status = 'Pending'); 
        insert InsertobjCase;
        Profile prf = [Select Id, Name from Profile where Name= 'Supervisor'];
        List<User> usrList = TestDataFactory.createTestUsers(2, prf.Id,true);
        List<Account> lstAcct= TestDataFactory.testAccountData();
        insert lstAcct;
        List<Case> csList = TestDataFactory.TestCaseData(5,'Approved');
        List<Case> cList =new List<Case>();
        for(Case cs: csList)
        {
            cs.AccountId = lstAcct[0].Id;
            cList.add(cs);
        }       
        insert cList;
        List<ContentVersion> cvList = TestDataFactory.createTestContentVersionData();
        insert cvList;
        SObject sobj = (SObject)Type.forName('ContentVersion').newInstance();
        complaintsDecision.processingWrapperString appParse1= new complaintsDecision.processingWrapperString(sobj,'test');
        String str = 'Case';
        sObject obj = Schema.getGlobalDescribe().get(str).newSObject();
        obj.put('Origin', 'Email');
        obj.put('Description', 'Test Comments');
        obj.put('Status', 'Approved');
        obj.put('Id', cList[0].Id);
        obj.put('AccountId', lstAcct[0].Id);
        obj.put('ProgramType__c','');
        obj.put('Program__c','');
        try {
            List<DMLOperationsHandler.FetchValueWrapper> objgetPickListValues = complaintsDecision.getPickListValues(InsertobjCase,'XXX');
        } catch (Exception e) { }
        try {
            complaintsDecision.getAllUsersList('Caseworker');
        } catch(Exception Ex) { }
        try {
            complaintsDecision.getAllUsersList('System Administrator');
        } catch(Exception Ex) { }
        try {
            complaintsDecision.getAllUsersList('Supervisor');
        } catch(Exception Ex) { }
        try {
            complaintsDecision.updatecasedetailsDraft(InsertobjCase);
        } catch(Exception Ex) { }  
        try {
            complaintsDecision.saveSign('test',obj.Id);
        } catch(Exception Ex) { }  
        try {
            complaintsDecision.getDatatableDetails(obj.Id);    
        } catch(Exception Ex) { }   
        try {
            complaintsDecision.updatecasedetails(obj,usrList[0].Id,'','Caseworker','Approve');
        } catch(Exception Ex) { }
        try {
            complaintsDecision.updatecasedetails(obj,usrList[0].Id,'','System Administrator','Approve');
        } catch(Exception Ex) { }
        try {
            complaintsDecision.updatecasedetails(obj,usrList[0].Id,'','Supervisor','Approve');
        } catch(Exception Ex) { }
        try {
            complaintsDecision.getReviewDetails(InsertobjCase.Id);
        } catch(Exception Ex) { }    
        try {
            complaintsDecision.getDeficiencyDetails(InsertobjCase.Id);
        } catch(Exception Ex) { } 
        try {
            complaintsDecision.getCaseworker(InsertobjCase.Id);
        } catch(Exception Ex) { }
        try {
            complaintsDecision.getSanctionDetails(InsertobjCase.Id);
        } catch(Exception Ex) { }
        try {
            complaintsDecision.getMonitoringDetails(InsertobjCase.Id);
        } catch(Exception Ex) { }        
    }   
    @isTest static void testComplaintsDecisionException(){
        Case InsertobjCase=new Case(Status = 'Pending'); 
        insert InsertobjCase;
        Profile prf = [Select Id, Name from Profile where Name= 'Supervisor'];
        List<User> usrList = TestDataFactory.createTestUsers(2, prf.Id,true);
        List<Account> lstAcct= TestDataFactory.testAccountData();
        insert lstAcct;
        List<Case> csList = TestDataFactory.TestCaseData(5,'Approved');
        List<Case> cList =new List<Case>();
        for(Case cs: csList)
        {
            cs.AccountId = lstAcct[0].Id;
            cList.add(cs);
        }       
        insert cList;
        List<ContentVersion> cvList = TestDataFactory.createTestContentVersionData();
        insert cvList;
        SObject sobj = (SObject)Type.forName('ContentVersion').newInstance();
        complaintsDecision.processingWrapperString appParse1= new complaintsDecision.processingWrapperString(sobj,'test');
        String str = 'Case';
        sObject obj = Schema.getGlobalDescribe().get(str).newSObject();
        obj.put('Origin', 'Email');
        obj.put('Description', 'Test Comments');
        obj.put('Status', 'Approved');
        obj.put('Id', cList[0].Id);
        obj.put('AccountId', lstAcct[0].Id);
        obj.put('ProgramType__c','');
        obj.put('Program__c','');
        try {
            complaintsDecision.getAllUsersList('');
        } catch(Exception Ex) { }
        try {
            complaintsDecision.getReviewDetails('');
        } catch(Exception Ex) { }    
        try {
            complaintsDecision.getDeficiencyDetails('');
        } catch(Exception Ex) { } 
        try {
            complaintsDecision.getCaseworker('');
        } catch(Exception Ex) { }
        try {
            complaintsDecision.saveSign('test',null);
        } catch(Exception Ex) { } 
        try {
            complaintsDecision.saveSign('',obj.Id);
        } catch(Exception Ex) { } 
        try {
            complaintsDecision.getSanctionDetails('');
        } catch(Exception Ex) { }
        try {
            complaintsDecision.getMonitoringDetails('');
        } catch(Exception Ex) { }
        try {
            complaintsDecision.getDatatableDetails('');    
        } catch(Exception Ex) { } 
    }

    @IsTest static void testcaseNegative3()
    {
        Profile prf = [Select Id, Name from Profile where Name= 'Supervisor'];
        List<User> userList =  TestDataFactory.createTestUsers(1, prf.Id, true);
        Account acc = new Account(ProviderType__c='Private',Name='Test');
        insert acc;
        Reconsideration__c recon = new Reconsideration__c();
        insert recon;
        Id MyId2=Schema.Sobjecttype.Case.getRecordTypeInfosByName().get('Complaints').getRecordTypeId();
        Case InsertobjCase=new Case(RecordTypeId = MyId2,AccountId = acc.Id); 
        insert InsertobjCase;
      
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments('Submitting request for approval.');
        req.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        req.setObjectId(InsertobjCase.Id);
        Approval.ProcessResult resu = Approval.process(req);
        System.assert(resu.isSuccess());
        System.assertEquals('Pending', resu.getInstanceStatus(),'Instance Status'+resu.getInstanceStatus());
        List<Id> newWorkItemIds = resu.getNewWorkitemIds();
        complaintsDecision.updatecasedetails(InsertobjCase,userList[0].Id,newWorkItemIds.get(0),'Caseworker','Approve');
        try {
            complaintsDecision.getDatatableDetails(InsertobjCase.Id);    
        } catch(Exception Ex) { } 
    }
}
