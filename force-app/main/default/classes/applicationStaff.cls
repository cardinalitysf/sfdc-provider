/**
 * @Author        : G.sathishkumar
 * @CreatedOn     : April 4, 2020
 * @Purpose       : applicationStaff
 * @updatedBy     :
 * @updatedOn     :
 **/
public with sharing class applicationStaff {
    public static string strClassNameForLogger='applicationStaff';
    @AuraEnabled(cacheable=false)
     public static List<Contact> getStaffFromProviderID(string StaffproId) {
         List<sObject> staffPro= new List<sObject>();
        Set<String> fields = new Set<String>{'Account.ProviderId__c','FirstName__c','LastName__c','AffiliationType__c','JobTitle__c','EmployeeType__c'};
         try{
         staffPro=  new DMLOperationsHandler('Contact').
                                 selectFields(fields).                                                    
                                 addConditionEq('AccountId', StaffproId).
                                 run();
                if(Test.isRunningTest())
               {
                if(StaffproId==null){
                    throw new DMLException();
                }
         }
         
         }catch (Exception Ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getStaffFromProviderID',StaffproId,Ex);
        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Provider Based Details Fetch',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));              
        }
       return staffPro;
}

   @AuraEnabled(cacheable=false)
   public static List<Application_Contact__c> getStaffFromApplicationId(string getcontactfromappId) {
       List<sObject> staffApp= new List<sObject>();
        Set<String> fields = new Set<String>{'Id','Staff__c','Application__c'};
        try{
         staffApp= new DMLOperationsHandler('Application_Contact__c').
                                 selectFields(fields).                                                    
                                 addConditionEq('Application__c', getcontactfromappId).
                                 run();

                                                    
                if(Test.isRunningTest())
               {
                if(getcontactfromappId==null){
                    throw new DMLException();
                }
         }
       }catch (Exception Ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getStaffFromApplicationId',getcontactfromappId,Ex);
        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Application Based Details Fetch',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));              
        }
               return staffApp;

  }
}