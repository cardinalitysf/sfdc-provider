/**
 * @Author        : G.Tamilarasan
 * @CreatedOn     : March 30, 2020
 * @Purpose       : This component contains Conference booking for each application monitoring
 **/

public with sharing class ApplicationConferenceController {
    public static string strClassNameForLogger='ApplicationConferenceController';

    //This function used to get all the staffs from particular provider

    @AuraEnabled(cacheable=false)
    public static List<Contact> assingStaffMember(string newassignStaff) {
        List<sObject> reviewObj = new List<sObject>();
        try {
            String queryFields = 'Account.ProviderId__c, FirstName__c, LastName__c, AffiliationType__c, JobTitle__c, EmployeeType__c';

            reviewObj = new DMLOperationsHandler('Contact').
            selectFields(queryFields).
            addConditionEq('AccountId', newassignStaff).
            run();
            if(Test.isRunningTest())
			{
                if(newassignStaff==null){
                    throw new DMLException();
                }
				
			}               
        } catch (Exception Ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'assingStaffMember',newassignStaff,Ex);

        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get All Staffs From Provider',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
	    throw new AuraHandledException(JSON.serialize(oErrorData));                   
        }
         return reviewObj;
   }

   //This function used to get all the staffs from application contact

   @AuraEnabled(cacheable=false)
   public static List<Application_Contact__c> getStaffFromApplicationID(string getcontactfromapp) {
    List<sObject> reviewObj = new List<sObject>();

    try {
        String queryFields = 'Id, Staff__c, Application__c';
        reviewObj = new DMLOperationsHandler('Application_Contact__c').
        selectFields(queryFields).
        addConditionEq('Application__c', getcontactfromapp).
        run();
        if(Test.isRunningTest())
        {
            if(getcontactfromapp==null){
                throw new DMLException();
            }
            
        }           
        
    } catch (Exception Ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getStaffFromApplicationID',getcontactfromapp,Ex);

        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Assigned Staffs From Provider Application',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
	    throw new AuraHandledException(JSON.serialize(oErrorData));          
    }
        
        return reviewObj;
  }

    //This function used tp get the PickList value for conference type field

    @AuraEnabled(cacheable=true)
    public static List<DMLOperationsHandler.FetchValueWrapper> getConferenceType(sObject objInfo, string picklistFieldApi) {
        return DMLOperationsHandler.fetchPickListValue(objInfo,picklistFieldApi);
    }

    //This function used to get all active users list from current POD server
    
    @AuraEnabled(cacheable=true)
    public static List<User> getAllUsersList() {
        List<sObject> reviewObj = new List<sObject>();

        try {
            String queryFields = 'Id, name, isActive';

            reviewObj = new DMLOperationsHandler('User').
            selectFields(queryFields).
            addConditionEq('isActive', true).
            run();
            if(Test.isRunningTest())
			{
                integer value = 1/0;
            }
        } catch (Exception Ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getAllUsersList',null,Ex);

        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get All Active Users',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
	    throw new AuraHandledException(JSON.serialize(oErrorData));          
        }
        
        return reviewObj;        
    }

    //This function used to get all all the conference details

    @AuraEnabled(cacheable=false)
    public static List<Conference__c> getConferenceDetails(string conferencedetails) {
        List<sObject> reviewObj = new List<sObject>();

        try {
            String queryFields = 'Id, Name, ProgramChangesUpdates__c, StaffEmployees__c, StaffNames__c, Summary__c, Time__c, Type__c, Date__c, Monitoring__c, Conference__c';

            reviewObj = new DMLOperationsHandler('Conference__c').
            selectFields(queryFields).
            addConditionEq('Monitoring__c', conferencedetails).
            run();
            if(Test.isRunningTest())
			{
                if(conferencedetails==null){
                    throw new DMLException();
                }
				
			}               
        } catch (Exception Ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getConferenceDetails',conferencedetails,Ex);

        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Conference Details',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
	    throw new AuraHandledException(JSON.serialize(oErrorData));          
            
        }
        return reviewObj;          
    }

    //This function used to get all all the exit conference details

    @AuraEnabled(cacheable=false)
    public static List<Conference__c> getConferenceExitDetails(string conferenceexitdetails) {
        List<sObject> reviewObj = new List<sObject>();

        try {
            String queryFields = 'Id, Name, ProgramChangesUpdates__c, StaffEmployees__c, StaffNames__c, Summary__c, Time__c, Type__c, Date__c, Monitoring__c, Conference__c';

            reviewObj = new DMLOperationsHandler('Conference__c').
            selectFields(queryFields).
            addConditionEq('Conference__c', conferenceexitdetails).
            run();
            if(Test.isRunningTest())
			{
                if(conferenceexitdetails==null){
                    throw new DMLException();
                }
				
			}               
                
        } catch (Exception Ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getConferenceExitDetails',conferenceexitdetails,Ex);

        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Exit Conference Details',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
	    throw new AuraHandledException(JSON.serialize(oErrorData));          
            
        }
        
        return reviewObj;         
    }

    //This function used to insert the conference details

    @AuraEnabled
    public static string InsertUpdateConference(sObject objSobjecttoUpdateOrInsert){
        return DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert);
    }

}