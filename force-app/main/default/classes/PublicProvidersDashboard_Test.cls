/**
 * @Author        : Preethi Bojarajan
 * @CreatedOn     : July 23, 2020
 * @Purpose       : Test class for PublicProvidersDashboard.cls
 **/

@isTest
public with sharing class PublicProvidersDashboard_Test {
    @isTest
    private static void getProvidersCount_Test(){
        Test.startTest();
        try {
            PublicProvidersDashboard.getProvidersCount('a','ProviderId__C','Public');
        } catch(Exception e){}
        try {
            PublicProvidersDashboard.getProvidersCount('','ProviderId__C','Public');
        } catch(Exception e){}
        try {
            PublicProvidersDashboard.getProvidersCount(null,'ProviderId__C','Public');
        } catch(Exception e){}
        try {
            PublicProvidersDashboard.getProvidersCount('','','Public');
        } catch(Exception e){}
        Test.stopTest();
    }

    @isTest
    private static void getProvidersSupCount_Test(){
        Test.startTest();
        try {
            PublicProvidersDashboard.getProvidersSupCount('a','a','Monitoring__c');
        } catch(Exception e){}
        try {
            PublicProvidersDashboard.getProvidersSupCount('a','a','Contract__c');
        } catch(Exception e){}
        try {
            PublicProvidersDashboard.getProvidersSupCount('a','ProviderId__c','Monitoring__c');
        } catch(Exception e){}
        try {
            PublicProvidersDashboard.getProvidersSupCount('a','Name__c','Contract__c');
        } catch(Exception e){}
        try {
            PublicProvidersDashboard.getProvidersSupCount(null,'Name__c','Contract__c');
        } catch(Exception e){}
        try {
            PublicProvidersDashboard.getProvidersSupCount('a','Name__c','a');
        } catch(Exception e){}
        try {
            PublicProvidersDashboard.getProvidersSupCount('','Name__c','a');
        } catch(Exception e){}
        Test.stopTest();
    }

    @isTest
    private static void getProvidersList_Test(){
        Test.startTest();
        try {
            PublicProvidersDashboard.getProvidersList('a','a','a','Public');
        } catch(Exception e){}
        try {
            PublicProvidersDashboard.getProvidersList('a','a','a','Private');
        } catch(Exception e){}
        try {
            PublicProvidersDashboard.getProvidersList(null,'active','ProviderId__c','Private');
        } catch(Exception e){}
        try {
            PublicProvidersDashboard.getProvidersList('a','total','ProviderId__c','Public');
        } catch(Exception e){}
        try {
            PublicProvidersDashboard.getProvidersList('a','active','ProviderId__c','Private');
        } catch(Exception e){}
        try {
            PublicProvidersDashboard.getProvidersList('','a','a','a');
        } catch(Exception e){}
        Test.stopTest();
    }

    @isTest
    private static void getMonitoringList_Test(){
        Test.startTest();
        try {
            PublicProvidersDashboard.getMonitoringList('a','a','a');
        } catch(Exception e){}
        try {
            PublicProvidersDashboard.getMonitoringList('ApplicationLicenseId__r.Provider__r.ProviderId__c','Approved','');
        } catch(Exception e){}
        try {
            PublicProvidersDashboard.getMonitoringList('ApplicationLicenseId__r.Provider__r.ProviderId__c','Pending','');
        } catch(Exception e){}
        try {
            PublicProvidersDashboard.getMonitoringList('ApplicationLicenseId__r.Provider__r.ProviderId__c','Rejected','a');
        } catch(Exception e){}
        try {
            PublicProvidersDashboard.getMonitoringList('a',null,'a');
        } catch(Exception e){}
        try {
            PublicProvidersDashboard.getMonitoringList('','a','a');
        } catch(Exception e){}
        try {
            PublicProvidersDashboard.getMonitoringList(null,null,null);
        } catch(Exception e){}
        try {
            PublicProvidersDashboard.getMonitoringList('','','');
        } catch(Exception e){}
        Test.stopTest();
    }

    @isTest
    private static void getContractList_Test(){
        Test.startTest();
        try {
            PublicProvidersDashboard.getContractList('a',null,'a');
        } catch(Exception e){}
        try {
            PublicProvidersDashboard.getContractList('',null,'a');
        } catch(Exception e){}
        try {
            PublicProvidersDashboard.getContractList('Provider__r.ProviderId__c','Pending','a');
        } catch(Exception e){}
        try {
            PublicProvidersDashboard.getContractList('Provider__r.ProviderId__c','Submitted','');
        } catch(Exception e){}
        try {
            PublicProvidersDashboard.getContractList('','','');
        } catch(Exception e){}
        try {
            PublicProvidersDashboard.getContractList(null,null,null);
        } catch(Exception e){}
        Test.stopTest();
    }

    @isTest
    private static void fetchApplicationName_Test(){
        Test.startTest();
        try {
            PublicProvidersDashboard.fetchApplicationName('a');
            PublicProvidersDashboard.fetchApplicationName(null);
        } catch(Exception e){}
            Test.stopTest();
    }
}