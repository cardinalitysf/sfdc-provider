/**
     * @Author        : Janaswini S
     * @CreatedOn     : August 18,2020
     * @Purpose       : Incident Dashboard test class method
**/

@isTest
public with sharing class IncidentDashboard_Test {
    
    @isTest static void testgetDashboardDetails (){
    IncidentDashboard.getUserEmail();
    }
    @isTest
    private static void getReferalPrivList_Test(){
        Test.startTest();
        try {
            IncidentDashboard.getReferalPrivList('a','total','');
        } catch(Exception ex){}
        try {
            IncidentDashboard.getReferalPrivList('a','total',null);
        } catch(Exception ex){}
        try {
            IncidentDashboard.getReferalPrivList(null,'active','ProviderId__c');
        } catch(Exception ex){}
        try {
            IncidentDashboard.getReferalPrivList('a','total','ProviderId__c');
        } catch(Exception ex){}
        try {
            IncidentDashboard.getReferalPrivList('a','draft','');
        } catch(Exception ex){}
        try {
            IncidentDashboard.getReferalPrivList('','approved','a');
        } catch(Exception ex){}
        try {
            IncidentDashboard.getReferalPrivList('','rejected','a');
        } catch(Exception ex){}
        Test.stopTest();
    }
    @isTest
    private static void getReferalCount_Test(){
        Test.startTest();
        Account Acc1 = new Account(ProviderType__c='Public' ,Name='Name');
        insert Acc1;
        Case CountCase1= new Case(Status = 'Draft');
        Id MyId2=Schema.Sobjecttype.Case.getRecordTypeInfosByName().get('Incident').getRecordTypeId();
        CountCase1.RecordTypeId = MyId2;
        CountCase1.AccountId=Acc1.Id;
        insert CountCase1;
        IncidentDashboard.getReferalCount('',Acc1.Id);

        CountCase1.Status='Draft';
        update CountCase1;
        IncidentDashboard.getReferalCount('',Acc1.Id);

        CountCase1.Status='Pending';
        update CountCase1;
        IncidentDashboard.getReferalCount('',Acc1.Id);

        CountCase1.Status='Approved';
        update CountCase1;
        IncidentDashboard.getReferalCount('',Acc1.Id);  

        CountCase1.Status='Acknowledged';
        update CountCase1;
        IncidentDashboard.getReferalCount('',Acc1.Id);        

        CountCase1.Status='Rejected';
        update CountCase1;
        IncidentDashboard.getReferalCount('',Acc1.Id);

        CountCase1.Status='Submitted to Caseworker';
        update CountCase1;
        IncidentDashboard.getReferalCount('',Acc1.Id);

        try {
            IncidentDashboard.getReferalCount('a','ProviderId__C');
        } catch(Exception ex){}
        try {
            IncidentDashboard.getReferalCount('','ProviderId__C');
        } catch(Exception ex){}
        try {
            IncidentDashboard.getReferalCount(null,'ProviderId__C');
        } catch(Exception ex){}
        try {
            IncidentDashboard.getReferalCount('','');
        } catch(Exception ex){}
        Test.stopTest();
    }


}
 