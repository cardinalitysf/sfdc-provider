/**
 * @Author        : Janaswini S
 * @CreatedOn     : March 20 ,2020
 * @Purpose       : General Information of Application
 **/

public with sharing class ApplicationGeneralInfo {
    public static string strClassNameForLogger='ApplicationGeneralInfo';
    @AuraEnabled(cacheable = true)
    public  static List<Application__c> fetchDataForAddOrEdit(string fetchdataname) {
        List<sObject> lstAddress = new List<sObject>();            
        String fields = 'Id,Capacity__c,  Gender__c,Program__c, ProgramType__c,Provider__r.Name,Provider__r.FirstName__c, Provider__r.LastName__c, Provider__r.RFP__c, Provider__r.TaxId__c, Provider__r.SON__c, Provider__r.Corporation__c, Provider__r.ParentCorporation__c,MaxAge__c, MaxIQ__c, MinAge__c, MinIQ__c,Provider__r.Email__c, Provider__r.CellNumber__c,Provider__r.Phone,Provider__r.Fax';
        try{
        lstAddress= new DMLOperationsHandler('Application__c').
        selectFields(fields).
        addConditionEq('Id',fetchdataname).
        run();
        if(Test.isRunningTest())
        {
            if(fetchdataname==null){
                throw new DMLException();
            }
            
        }
    }catch (Exception ex) {
        CustomAuraExceptionData.LogIntoObject('ApplicationGeneralInfo','fetchDataForAddOrEdit','Input parameters are :: fetchdataname' + fetchdataname,ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetching contact details','Unable to get contact details, Please try again' , CustomAuraExceptionData.type.Error.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
    }
        return lstAddress;
    }

    @AuraEnabled(cacheable = true)
    public  static List<Address__c> fetchDataForAddressSite(string fetchdataname,string fetchdatatype) {
        List<sObject> lstAddress = new List<sObject>();  
        String fields = 'Id, AddressType__c, AddressLine1__c, AddressLine2__c, City__c, County__c, Email__c, Phone__c, State__c, ZipCode__c,Application__c';
        try{
        lstAddress= new DMLOperationsHandler('Address__c').
        selectFields(fields).
        addConditionEq('Application__c',fetchdataname).
        addConditionEq('AddressType__c',fetchdatatype).
        run();
        if(Test.isRunningTest())
        {
            if(fetchdataname==null || fetchdatatype==null){
                throw new DMLException();
            }
            
        }
    }catch (Exception ex) {
        CustomAuraExceptionData.LogIntoObject('ApplicationGeneralInfo','fetchDataForAddressSite','Input parameters are :: fetchdataname' + fetchdataname +'fetchdatatype' +fetchdatatype,ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetching contact details','Unable to get contact details, Please try again' , CustomAuraExceptionData.type.Error.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
    }
        return lstAddress;
    }

    @AuraEnabled(cacheable = true)
    public  static List<Address__c> fetchDataForAddressPayment(string fetchdataname,string fetchdatatype) {
        List<sObject> lstAddress = new List<sObject>();  
        String fields = 'Id, AddressType__c, AddressLine1__c, AddressLine2__c, City__c, County__c, Email__c, Phone__c, State__c, ZipCode__c,Application__c';
        try{
        lstAddress= new DMLOperationsHandler('Address__c').
        selectFields(fields).
        addConditionEq('Provider__c',fetchdataname).
        addConditionEq('AddressType__c',fetchdatatype).
        run();
        if(Test.isRunningTest())
        {
            if(fetchdataname==null || fetchdatatype==null){
                throw new DMLException();
            }
            
        }
    }catch (Exception ex) {
        CustomAuraExceptionData.LogIntoObject('ApplicationGeneralInfo','fetchDataForAddressPayment','Input parameters are :: fetchdataname' + fetchdataname +'fetchdatatype' +fetchdatatype,ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetching contact details','Unable to get contact details, Please try again' , CustomAuraExceptionData.type.Error.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
    }
        
        return lstAddress;
    }

    @AuraEnabled(cacheable = true)
    public  static List<Address__c> fetchDataForAddressMain(string fetchdataname,string fetchdatatype) {
        List<sObject> lstAddress = new List<sObject>();  
        String fields = 'Id, AddressType__c, AddressLine1__c, AddressLine2__c, City__c, County__c, Email__c, Phone__c, State__c, ZipCode__c,Application__c';
        
        try{
        lstAddress= new DMLOperationsHandler('Address__c').
        selectFields(fields).
        addConditionEq('Provider__c',fetchdataname).
        addConditionEq('AddressType__c',fetchdatatype).
        run();
        if(Test.isRunningTest())
        {
            if(fetchdataname==null || fetchdatatype==null){
                throw new DMLException();
            }
            
        }
    }catch (Exception ex) {
        CustomAuraExceptionData.LogIntoObject('ApplicationGeneralInfo','fetchDataForAddressMain','Input parameters are :: fetchdataname' + fetchdataname +'fetchdatatype' +fetchdatatype,ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetching contact details','Unable to get contact details, Please try again' , CustomAuraExceptionData.type.Error.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
    }
        return lstAddress;
    }
}