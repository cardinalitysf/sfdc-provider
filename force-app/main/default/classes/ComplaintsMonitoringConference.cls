/**
 * @Author        : Sindhu Venkateswarlu
 * @CreatedOn     : April 1, 2020
 * @Purpose       : This component contains Conference booking for each Complaints monitoring
 **/
public with sharing class ComplaintsMonitoringConference {
    public static string strClassNameForLogger='ComplaintsMonitoringConference';

    //This function used to get all the staffs from particular provider
    @AuraEnabled(cacheable=false)
    public static List<Contact> assingStaffMember(string newassignStaff) {
        List<sObject> reviewObj= new List<sObject>();
        Set<String> queryFields = new Set<String>{'Account.ProviderId__c, FirstName__c, LastName__c, AffiliationType__c, JobTitle__c, EmployeeType__c'};
        try {
            reviewObj = new DMLOperationsHandler('Contact').
        selectFields(queryFields).
        addConditionEq('AccountId', newassignStaff).
        run();
        if(Test.isRunningTest())
        {
            if(newassignStaff==null){
                throw new DMLException();
            }
            
        }
        } catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'assingStaffMember',newassignStaff,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetching record',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return reviewObj;
    }

   //This function used to get all the Actors from particular provider
    @AuraEnabled(cacheable=false)
    public static List<Actor__c > getassignActors(string providerid) {
       List<Actor__c > Contacts =
       new DMLOperationsHandler('Actor__c').
       selectFields('Id,Contact__r.FirstName__c, Contact__r.LastName__c').
       addConditionEq('Provider__c ', providerid).
       addConditionEq('Contact__r.RecordType.Name','Household Members').
       run();
       return Contacts;
    }

    //This function used to get all the staffs from application contact

    @AuraEnabled(cacheable=false)
    public static List<Application_Contact__c> getStaffFromApplicationID(string getcontactfromapp) {

       String queryFields = 'Id, Staff__c, Application__c';
       List<sObject> reviewObj = new DMLOperationsHandler('Application_Contact__c').
       selectFields(queryFields).
       addConditionEq('Application__c', getcontactfromapp).
       run();
       
       return reviewObj;
    }

   //This function used tp get the PickList value for conference type field

   @AuraEnabled(cacheable=true)
   public static List<DMLOperationsHandler.FetchValueWrapper> getConferenceType(sObject objInfo, string picklistFieldApi) {
       return DMLOperationsHandler.fetchPickListValue(objInfo,picklistFieldApi);
   }

   //This function used to get all active users list from current POD server
   
   @AuraEnabled(cacheable=true)
   public static List<User> getAllUsersList() {

       String queryFields = 'Id, name, isActive';

       List<sObject> reviewObj = new DMLOperationsHandler('User').
       selectFields(queryFields).
       addConditionEq('isActive', true).
       run();
       
       return reviewObj;        
   }

   //This function used to get all all the conference details

   @AuraEnabled(cacheable=false)
   public static List<Conference__c> getConferenceDetails(string conferencedetails) {

       String queryFields = 'Id, Name, ProgramChangesUpdates__c, StaffEmployees__c, StaffNames__c, Summary__c, Time__c, Type__c, Date__c, Monitoring__c, Conference__c';

       List<sObject> reviewObj = new DMLOperationsHandler('Conference__c').
       selectFields(queryFields).
       addConditionEq('Monitoring__c', conferencedetails).
       run();        
       return reviewObj;          
   }

   //This function used to get all all the exit conference details

   @AuraEnabled(cacheable=false)
   public static List<Conference__c> getConferenceExitDetails(string conferenceexitdetails) {

       String queryFields = 'Id, Name, ProgramChangesUpdates__c, StaffEmployees__c, StaffNames__c, Summary__c, Time__c, Type__c, Date__c, Monitoring__c, Conference__c';

       List<sObject> reviewObj = new DMLOperationsHandler('Conference__c').
       selectFields(queryFields).
       addConditionEq('Conference__c', conferenceexitdetails).
       run();
       
       return reviewObj;         
   }

   //This function used to insert the conference details
   @AuraEnabled
   public static string InsertUpdateConference(sObject objSobjecttoUpdateOrInsert){
       return DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert);
   }

}

