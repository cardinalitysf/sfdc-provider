/**
     * @Author        : Sindhu Venkateswarlu
     * @CreatedOn     : June 6,2020
     * @Purpose       :Test Methods to unit test PublicProviderHomeInformation class in PublicProviderHomeInformation.cls
     * @Updated on    :June 20, 2020
     * @Updated by    :Janaswini Unit Test Updated methods with try catch
     **/

@isTest
public class PublicProviderHomeInformation_Test {
    @isTest static void testfetchHomeInformation (){
        
         Account InsertobjNewAccount=new Account(Name = 'test Account'); 
        insert InsertobjNewAccount;
        
        HomeInformation__c InsertobjNewHome=new HomeInformation__c(SwimmingPool__c='pool'); 
        insert InsertobjNewHome;
        try{
                 // Testing getPickListValues method getPickListValues
               Map<String,  List<DMLOperationsHandler.FetchValueWrapper>>objgetPickListValues = PublicProviderHomeInformation.getMultiplePicklistValues('InsertobjNewHome','LicensedProvider__c');
               }
            	catch(Exception e){
                                }   
                                
        
        try {
            PublicProviderHomeInformation.fetchHomeInformation(null);
        }
        
        catch(Exception ex){
               
        }
        PublicProviderHomeInformation.fetchHomeInformation(InsertobjNewHome.Id);
    }
    
    @isTest static void testPublicProviderHomeInformationException()
    {   
        PublicProviderHomeInformation.fetchHomeInformation('');       
    }
  
}