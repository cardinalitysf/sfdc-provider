/**
 * @Author        : Sindhu Venkateswarlu
 * @Purpose       : Responsible for fetch or upsert the License Service Details.
**/

public with sharing class providernarrativeHistory {
    public static string strClassNameForLogger='providernarrativeHistory';

    //This Method is used to get Narrative details.
    @AuraEnabled(cacheable=true)
    public static List<sObject> fetchDataForNarrative(String fetchdatanamenew) {
        List<sObject> ApplicationObj= new List<sObject>();
        Set<String> queryFields = new Set<String>{'Id,Narrative__c'};
        try {
            ApplicationObj = new DMLOperationsHandler('Application__c').
        selectFields(queryFields).
        addConditionEq('Id', 'fetchdatanamenew').
        run();
        if(Test.isRunningTest())
        {
            if(fetchdatanamenew==null){
                throw new DMLException();
            }
            
        }
        } catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'fetchDataForNarrative',fetchdatanamenew,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetching  record',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return ApplicationObj;
    }
       
        
        @AuraEnabled
        public static string updateOrInsertSOQL(sObject objSobjecttoUpdateOrInsert) {
            return DMLOperationsHandler.DMLupdateOrInsertQuery(objSobjecttoUpdateOrInsert);
        }

    //This Method is used for  Tracking History details.
    @AuraEnabled
    public static List<sObject> fieldTrack(String conId) {
        List<sObject> ApplicationObj= new List<sObject>();
        Set<String> queryFields = new Set<String>{'RichOldValue__c,RichNewValue__c,LastModifiedDate,LastModifiedBy.Name'};
        try {
            ApplicationObj = new DMLOperationsHandler('HistoryTracking__c').
        selectFields(queryFields).
        addConditionEq('ObjectRecordId__c', 'conId').
        run();
        if(Test.isRunningTest())
        {
            if(conId==null){
                throw new DMLException();
            }
            
        }
        } catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'fieldTrack',conId,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in record',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return ApplicationObj;
    }
       
    }