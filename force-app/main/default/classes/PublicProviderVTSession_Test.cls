/**
     * @Author        : Balamurugan.B
     * @CreatedOn     : June 06,2020
     * @Purpose       :Test Methods to unit test for PublicProviderViewTrainingSession.cls
     * @Updated by    : Naveen S
     * @Updated On    : July 23,2020
     **/

    @isTest
    public class PublicProviderVTSession_Test {
             @isTest static void testViewTrainingDetails(){
                Training__c InsertobjNewTraining=new Training__c(TrainingName__c = 'Yes'); 
                insert InsertobjNewTraining;

                MeetingInfo__c InsertobjNewMeeting=new MeetingInfo__c(Training__c = InsertobjNewTraining.Id); 
                insert InsertobjNewMeeting;

                SessionAttendee__c InsertobjNewSession=new SessionAttendee__c(Training__c = InsertobjNewTraining.Id); 
                insert InsertobjNewSession;

                Actor__c InsertobjNewActor=new Actor__c(Role__c = 'Applicant'); 
                insert InsertobjNewActor;
                
                string strUpdate = '[{"Id":"'+InsertobjNewSession.Id+'","CandidateAttend__c":"Yes","SessionStatus__c":"Completed"}]';
                
                string strDatas = '[{"Id":"'+InsertobjNewSession.Id+'","CandidateAttend__c":"Yes","SessionStatus__c":"Completed"}]';

               
                PublicProviderViewTrainingSession.bulkUpdateIntentAttend(strUpdate);

                PublicProviderViewTrainingSession.prideBulkUpdateIntentAttend(strDatas);
               
                try{
                  PublicProviderViewTrainingSession.getTrainingDetails(null);
                }
                catch(Exception e){

                }  
                PublicProviderViewTrainingSession.getTrainingDetails('InsertobjNewTraining.Id');
                try{
                  PublicProviderViewTrainingSession.getPrideBasicDetails(null,null);
                }
                catch(Exception e){

                }  
                PublicProviderViewTrainingSession.getPrideBasicDetails('InsertobjNewTraining.Id','');
                try{
                  
                PublicProviderViewTrainingSession.getmeetingInfoPrideList(null,null);
                }
                catch(Exception e){

                }  

                PublicProviderViewTrainingSession.getmeetingInfoPrideList('InsertobjNewTraining.Id','yyy');
                try{
                  PublicProviderViewTrainingSession.getmeetingInfoOtherPrideList(null);
                 }
                catch(Exception e){

                }  
                PublicProviderViewTrainingSession.getmeetingInfoOtherPrideList('zzz');
                try{
                  PublicProviderViewTrainingSession.getPrideHistoryList(null,null);

                }
                catch(Exception e){

                }  
                PublicProviderViewTrainingSession.getPrideHistoryList('InsertobjNewTraining.Id','InsertobjNewActor.Id');

                
                try{
                List<DMLOperationsHandler.FetchValueWrapper>  objgetPickListValues = PublicProviderViewTrainingSession.getPickListValues(InsertobjNewMeeting,'XXX');  
                }
                catch(Exception e){

                } 
                
                try{
                  PublicProviderViewTrainingSession.getPrideCandinateAttendCount(null);
                }
                catch(Exception e){

                }  
                PublicProviderViewTrainingSession.getPrideCandinateAttendCount('InsertobjNewTraining.Id');

                try{
                  PublicProviderViewTrainingSession.getOtherPrideCandinateAttendCount(null);
                }
                catch(Exception e){

                }  
                PublicProviderViewTrainingSession.getOtherPrideCandinateAttendCount('InsertobjNewTraining.Id');

            }


             }