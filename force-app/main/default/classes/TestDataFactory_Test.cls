/*
     Author         : Sindhu
     @CreatedOn     : June 26 ,2020
     @Purpose       : Test class for TestDataFactory.cls .
*/

@isTest

public with sharing class TestDataFactory_Test {
    @isTest static void  testTestDataDetails() {
    Training__c trang = new Training__c();
    insert trang;
    Contact con = new Contact(LastName='Names');
    insert con;
    Training__c Tran =new Training__c();
    insert Tran;
    Account acct = new Account(Name = 'Cardinality-CW-');
    insert acct;
    Certification__c certf = new Certification__c();
    insert certf;
    /*User u = new User(alias = 'name1', email='test1'+'@test12.com', emailencodingkey='UTF-8', 
    lastname='Testing', languagelocalekey='en_US', localesidkey='en_US', profileid = '',
    timezonesidkey='America/Los_Angeles', username= 'testZZ'+'@testZZ-00.com'); 
    insert u;*/
    Profile prf = [Select Id, Name from Profile where Name= 'Supervisor'];
    List<User> usrList = TestDataFactory.createTestUsers( 2, prf.Id,true );

       TestDataFactory.testAccountData();
       TestDataFactory.TestCaseData(1,'');
       TestDataFactory.createTestContentVersionData();
       
       TestDataFactory.getPartnerUser(con,'OwnerID','AccName');
      
       TestDataFactory.createTestTraining(1,trang.Id,true);
       TestDataFactory.TestApplicationData(1,'');
       TestDataFactory.createTestAddressData(0,true);
       TestDataFactory.createTestMonitoringData(1,true);
       TestDataFactory.createTestProviderRecordAnswerData(1,true);
       TestDataFactory.createTestProviderRecordQuestionData(1,true);
       TestDataFactory.createTestContactNotesData(1,true);
       TestDataFactory.createTestLicenseServicesData(1,true);
       TestDataFactory.createTestLicenseData(1,true);
       TestDataFactory.createTestContractData(1,true);
       TestDataFactory.createTestIRCData(1,true);
       TestDataFactory.createTestActorData(1,true);
       TestDataFactory.createTestActorDetailsData(1,true);
       TestDataFactory.createTestHomeInformationData(1,true);
       TestDataFactory.createTestMeetingInformationData(1,true);
       TestDataFactory.createTestHistoryTrackingData(1,true);
       TestDataFactory.createTestReconsiderationData(1,true);
       TestDataFactory.createTestReferenceValueData(1,true);
       TestDataFactory.createTestContacts(1,acct.Id,true);
       TestDataFactory.createTestCertification(1,certf.Id,true);
       TestDataFactory.createTestTraining(1,trang.Id,true);
       TestDataFactory.createTestSessionAttendeeData(1,true);
      //TestDataFactory.createTestUsers(1,u.Id,true);
      try{
        
      }catch(Exception e){
      TestDataFactory.createTestUsers(1,prf.Id,true);

      }






    }
}