/**
     * @Author        : Sindhu Venkateswarlu
     * @CreatedOn     : June 25,2020
     * @Purpose       : Test Methods to unit test PublicProvidersPetInformation class in PublicProvidersPetInformation.cls
     * @UpadtedBy     : JayaChandran Unit Test Updated methods with try catch.
     * @Date          : 21 july,2020
**/

    @isTest

    public with sharing class PublicProvidersPetInformation_Test {
        @isTest static void testSavePetRecordDetails (){
            
            Reconsideration__c recon = new Reconsideration__c();
            insert recon;
            Contact InsertobjNewContact=new Contact(LastName = 'Cat'); 
            insert InsertobjNewContact;
      
            PublicProvidersPetInformation.SavePetRecordDetails(InsertobjNewContact);

            PublicProvidersPetInformation.getRecordType('Pet');

            try{
                PublicProvidersPetInformation.getProvidersPetInfo(null);
            }catch(Exception Ex) {

            }
            PublicProvidersPetInformation.getProvidersPetInfo(recon.Id);

            try{
                PublicProvidersPetInformation.editPetDetails(null);
            }catch(Exception Ex) {

            }
            PublicProvidersPetInformation.editPetDetails(InsertobjNewContact.Id);

    }
}
