/*
     Author         : G.sathishkumar
     @CreatedOn     : june 09 ,2020
     @Purpose       : test class for PublicApplicationHouseholdChecklist Details .
*/
@isTest
public  class PublicApplicationHouseholdChecklist_Test {
    @isTest static void testPublicApplicationHouseholdChecklist() {

        Application__c InsertobjApplication=new Application__c(); 
                insert InsertobjApplication;

        ProviderRecordAnswer__c InsertobjInProviderRecordAnswer =new ProviderRecordAnswer__c();
        insert InsertobjInProviderRecordAnswer;

        string str1 = '[{"id":"'+InsertobjInProviderRecordAnswer.Id+'","Date__c":"2020-06-10","Comments__c":"'+InsertobjInProviderRecordAnswer.Comments__c+'","ProviderRecordQuestion__c":"'+InsertobjInProviderRecordAnswer.ProviderRecordQuestion__c+'","HouseholdStatus__c":"'+InsertobjInProviderRecordAnswer.HouseholdStatus__c+'","Comar__c":"'+InsertobjInProviderRecordAnswer.Comar__c+'"}]';

       try{
        PublicApplicationHouseholdChecklist.getQuestionsByType();
       } catch (Exception Ex) {
                        
                }
         
          PublicApplicationHouseholdChecklist.getRecordType ('Household');

        try {
             PublicApplicationHouseholdChecklist.getRecordQuestionId(null, null);
            } catch (Exception Ex) {
                }
         PublicAppHouseHoldMemberChecklist.getRecordQuestionId(InsertobjApplication.Id, '');
          
          try {
                 PublicApplicationHouseholdChecklist.bulkAddRecordAns(null);
            } catch (Exception Ex) {
              }
        PublicApplicationHouseholdChecklist.bulkAddRecordAns(str1);
        try {
             PublicApplicationHouseholdChecklist.getProviderAnswer(null);
            } catch (Exception Ex) {
             }
        
        PublicApplicationHouseholdChecklist.getProviderAnswer ('');

    }
}
