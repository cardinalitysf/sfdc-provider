/**
 * @Author        : B.Balamurugan
 * @CreatedOn     : May 24, 2020
 * @Purpose       : This component for Public provider Contact Details Class
 **/

public with sharing class PublicProviderContactDetails {
    @AuraEnabled(cacheable = true)
        public static List<ContactNotes__c> getAllContactDetails(String applicationId) {
            List<ContactNotes__c> contnote = new DMLOperationsHandler('ContactNotes__c').
            selectFields('Id, ContactType__c, Purpose__c, Location__c, Date__c, Time__c, ContactName__c, ContactRole__c, Phone__c, Email__c, Narrative__c,Case__c').
            addConditionEq('Case__c', applicationId).
            orderBy('Id','DESC').
            run();
            return contnote;
    }

    
    @AuraEnabled
   public static List<ContactNotes__c> editContactDetails(String selectedContactId) {
            List<ContactNotes__c> contnote = new DMLOperationsHandler('ContactNotes__c').
            selectFields('Id, ContactType__c, Purpose__c, Location__c, Date__c, Time__c, ContactName__c, ContactRole__c, Phone__c, Email__c, Narrative__c').
            addConditionEq('Id', selectedContactId).
            run();
            return contnote;

    }

    // @AuraEnabled(cacheable=true)
    // public static List<SObject> getRecordType(String name) {
    // String queryFields = 'Id, Name, sObjectType';
    //     List<SObject> recordObj=  new DMLOperationsHandler('RecordType').
    //                          selectFields(queryFields).
    //                          addConditionEq('Name', name).                           
    //                          run();
    //  return recordObj;
    // }
}