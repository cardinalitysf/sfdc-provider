/**
 * @Author        : B Balamurugan.
 * @CreatedOn     : Aug 03,2020
 * @Purpose       : Incident Witness Fetching -Staff and Person/Kid.
 **/

public with sharing class IncidentWitness {

    public static string strClassNameForLogger = 'IncidentWitness';
    @AuraEnabled(cacheable=true)
        public static List<Actor__c> getPkcDetails(string IncidentDetails) {
        List<Actor__c> PersonKid= new List<Actor__c>();
        try{
            PersonKid =
            new DMLOperationsHandler('Actor__c').
            selectFields('Id,Assign__c, WitnessStatement__c,Contact__r.FirstName__c,Contact__r.LastName__c,Contact__r.LastName,Contact__r.DOB__c, Contact__r.IdentifierNumber__c').
            addConditionEq('Referral__c', IncidentDetails).
            addConditionEq('Role__c','Person/Kid').
            run();
            if(Test.isRunningTest()) {
                if(IncidentDetails==null)
                {
                 throw new DMLException();
                }
            } 
        }catch (Exception Ex) {
            //To Log into object
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getPkcDetails',IncidentDetails + 'IncidentDetails', Ex);
            //To return back to the UI
            CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Sorry,Failed to load Person/Kid details ,Please Try Again..!', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
            return PersonKid; 
        }    

    @AuraEnabled(cacheable=false)
        public static List<Actor__c> getSelectedPersonKidChildDropdownValueData(String id) {
        List<Actor__c> getSelectedPersonKidChildDropdownValue = new List<Actor__c>();
        
            try {
                getSelectedPersonKidChildDropdownValue = new DMLOperationsHandler('Actor__c').
                selectFields('Id, Contact__r.DOB__c,WitnessStatement__c, Contact__r.IdentifierNumber__c').
                addConditionEq('id', id).
                run();
            if(Test.isRunningTest())
             {
                if(id==null){
                    throw new DMLException();
                }
            } 
            } catch(Exception Ex) {
                CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getSelectedPersonKidChildDropdownValueData', 'id' + id, Ex);
                CustomAuraExceptionData errorData=new CustomAuraExceptionData('Unable to get Id based  Data',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
                throw new AuraHandledException(JSON.serialize(errorData));
            }

            return getSelectedPersonKidChildDropdownValue;
            
        }

    //Fetch Staff Detail
        @AuraEnabled(cacheable=true)
        public static List<Actor__c> getStaffDetails(string StaffDetails) {
        List<Actor__c> Staff= new List<Actor__c>();
        try{
            Staff =
            new DMLOperationsHandler('Actor__c').
            selectFields('Id,WitnessStatement__c,Contact__r.FirstName__c,Contact__r.LastName__c,Contact__r.LastName,Contact__r.JobTitle__c,Contact__r.EmployeeType__c').
            addConditionEq('Referral__c', StaffDetails).
            addConditionEq('Role__c','Staff').
            run();
            if(Test.isRunningTest()) {
                if(StaffDetails==null){
                    throw new DMLException();
                }
            } 
        }catch(Exception Ex){
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getStaffDetails', 'StaffDetails' + StaffDetails, Ex);
            CustomAuraExceptionData errorData=new CustomAuraExceptionData('Sorry,Failed to load Staff details ,Please Try Again..!',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(errorData));

        }
    
        return Staff; 
    }    

    //Based On Id Fetch the data
    @AuraEnabled(cacheable=false)
    public static List<Actor__c> getSelectedStaffDropdownValueData(String id) {
        List<Actor__c> getSelectedStaffDropdownValue = new List<Actor__c>();
    
        try {
            getSelectedStaffDropdownValue = new DMLOperationsHandler('Actor__c').
            selectFields('Id,WitnessStatement__c, Contact__r.JobTitle__c,Contact__r.EmployeeType__c').
            addConditionEq('id', id).
            run();
            if(Test.isRunningTest()) {
                if(id==null){
                    throw new DMLException();
                }
            } 

        } catch(Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getSelectedStaffDropdownValueData', 'id' + id, Ex);
            CustomAuraExceptionData errorData=new CustomAuraExceptionData('Unable to get Particular Staff Id based  Data',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(errorData));
        }

        return getSelectedStaffDropdownValue;
        
    }


    //Update the Name From Contact With Actor id.

    @AuraEnabled
        public static string InsertUpdateContactActor(SObject saveObjInsert){
        return DMLOperationsHandler.updateOrInsertSOQLReturnId(saveObjInsert);
    }

    @AuraEnabled(cacheable=true)
    public static Id getRecordType(String name) {
        return CommonUtils.getRecordTypeIdbyName('Contact', name);
    }


    @AuraEnabled(cacheable=false)
        public static List<Actor__c> getWitnessDetails(string IncidentId) {
        List<sObject> actorObj = new List<sObject>();
        Set<String> fields = new Set<String>{'Id,Contact__r.FirstName__c,Contact__r.LastName__c,Contact__r.LastName,Role__c, WitnessStatement__c'};
    
    try{
        actorObj= new DMLOperationsHandler('Actor__c').
        selectFields(fields).
        addConditionEq('Referral__c',IncidentId).
        addConditionEq('RoleName__c', true).
        orderBy('CreatedDate', 'DESC').
        run();
        if(Test.isRunningTest()) {
            if(IncidentId==null){
                throw new DMLException();
            }
        } 
    
       }catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getWitnessDetails','IncidentId'+IncidentId,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('DataTable based error in fetching from Actor. try later',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
       }
        return actorObj;
    }
     

     @AuraEnabled(cacheable=false)
        public static List<Actor__c> getActionDetails(string EId) {
        List<sObject> actorObj = new List<sObject>();
        Set<String> fields = new Set<String>{'Id,Contact__r.FirstName__c,Contact__r.LastName__c,Contact__r.LastName,Role__c,Contact__r.DOB__c, Contact__r.IdentifierNumber__c,Contact__r.JobTitle__c,Contact__r.EmployeeType__c, WitnessStatement__c'};
        
        try{
            actorObj= new DMLOperationsHandler('Actor__c').
            selectFields(fields).
            addConditionEq('Id',EId).
            run();
            if(Test.isRunningTest()) {
                if(EId==null){
                    throw new DMLException();
                }
            } 
        }catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getActionDetails','EId'+EId,Ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Row based error in Action. try later',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
            return actorObj;
        }

}