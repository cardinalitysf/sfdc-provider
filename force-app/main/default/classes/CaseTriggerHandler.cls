public with sharing class CaseTriggerHandler implements ITriggerHandler{
	/**
	 * @Author        : V.S.Marimuthu
	 * @CreatedOn     : March 17 ,2020
	 * @Purpose       : Handler class which will do hand shake between trigger and task service
	 **/
	public Boolean IsDisabled(){
		// string blnTriggerSwitch = 'false';
		// List<Bypass_Switch_For_Trigger__mdt> triggerSwitch = [select
		//                                                             Trigger_Switch__c
		//                                                       from Bypass_Switch_For_Trigger__mdt];
		// for (Bypass_Switch_For_Trigger__mdt value : triggerSwitch){
		//     blnTriggerSwitch = value.Trigger_Switch__c;
		// }
		// return blnTriggerSwitch == 'false' ? false : true;
		return false;
	}

	public void BeforeInsert(List<Sobject> newItems){
	
	}

	public void BeforeUpdate(Map<id, sObject> newItems, Map<id, sObject> oldItems){
	
	}

	public void BeforeDelete(Map<id, sObject> oldItems){
	
	}

	public void AfterInsert(List<Sobject> newMap){
		
	}

	
	private void createApplicationOrChangeStatusForReferralOrComplaints(Map<Id, sObject> newMap, Map<Id, sObject> oldMap){
		try{
			List<Case> oToCreateApplication= new List<Case>();
			List<Case> oToChangeTheStatus= new List<Case>();
		    for(Case objCase : ((Map<Id,Case>)newMap).values()){
		        //Create an old and new map so that we can compare values
		        Case oldCase = (Case)oldMap.get(objCase.ID);
		        Case newCase = (Case)newMap.get(objCase.ID);
		        //If the fields are different, then ID has changed
		        if(oldCase.Status != newCase.Status && newCase.Status.toUppercase() == ProviderConstants.APPROVED){
				  //Referral
				 
				  if(CommonUtils.getRecordTypeNameById('Case',newCase.RecordTypeId)==ProviderConstants.CASE_RECORDTYPE_REFERRAL)
				   {
					oToCreateApplication.add(objCase);
				   }
				   //Complaint
				   else if(CommonUtils.getRecordTypeNameById('Case',newCase.RecordTypeId)==ProviderConstants.CASE_RECORDTYPE_COMPLAINTS)
				   {
					oToChangeTheStatus.add(objCase); 
				   }
		        }
			}
			
			//Create Application
		    if(oToCreateApplication.size()>0){
		    	new CaseService().createApplications(oToCreateApplication);
			}// if
			
			//Change Status in License,Applications and deficiency
			if(oToChangeTheStatus.size()>0){
				new CaseService().updateStatusDependingOnSanctions(oToChangeTheStatus);
		    	//new CaseService().createApplications(oToCreateApplication);
			}// if
		}
		catch(Exception Ex)
		{
			String[] arguments = new String[] { string.ValueOf(newMap),string.ValueOf(oldMap)};
			CustomAuraExceptionData.LogIntoObject('CaseTriggerHandler','createApplicationOrChangeStatusForReferralOrComplaints',String.format('Input parameters are :: newMap -- {0}:: oldMap -- {1}',arguments),Ex);   
		}
	}

	//To Update History Tracking with Old and New values
	private  void updateHistoryTracking(Map<Id, sObject> newMap, Map<Id, sObject> oldMap){

		list<HistoryTracking__c> hstTrackList = new list<HistoryTracking__c>();
		for (Case contr:  ((Map<Id,Case>)newMap).values()){
	
	 //Update an old and new map so that we can compare values
			Case newCase = (Case)newMap.get(contr.ID);
			Case oldCase = (Case)oldMap.get(contr.ID);
			
				if(oldCase.Outcomes__c != newCase.Outcomes__c){
					
					HistoryTracking__c hstTrack = new HistoryTracking__c();
					hstTrack.ObjectName__c = 'Case';
					hstTrack.ObjectRecordId__c = contr.Id;
					hstTrack.RichOldValue__c = oldCase.Outcomes__c;
					hstTrack.RichNewValue__c = newCase.Outcomes__c;
					hstTrack.HistoryName__c = ProviderConstants.HISTORY_OUTCOMES; 
					hstTrackList.add(hstTrack);
				}else if(oldCase.Summary__c != newCase.Summary__c){
					
					HistoryTracking__c hstTrack = new HistoryTracking__c();
					hstTrack.ObjectName__c = 'Case';
					hstTrack.ObjectRecordId__c = contr.Id;
					hstTrack.RichOldValue__c = oldCase.Summary__c;
					hstTrack.RichNewValue__c = newCase.Summary__c; 
					hstTrack.HistoryName__c = ProviderConstants.HISTORY_SUMMARY; 
					hstTrackList.add(hstTrack);
				
				}else if(oldCase.Description != newCase.Description){
					
					HistoryTracking__c hstTrack = new HistoryTracking__c();
					hstTrack.ObjectName__c = 'Case';
					hstTrack.ObjectRecordId__c = contr.Id;
					hstTrack.RichOldValue__c = oldCase.Description;
					hstTrack.RichNewValue__c = newCase.Description;
					hstTrack.HistoryName__c = ProviderConstants.HISTORY_DESCRIPTION; 
					hstTrackList.add(hstTrack);
				
				}
		   
		}
		if(hstTrackList.size() >0){
			insert hstTrackList;
		}
	}

	/**
	 * Updated by	: V.S.Marimuthu
	 * Date			: July 24th 2020\
	 * Purpose		: To create Application(Record Type: Referral) or to change the status ((Record Type: Complaints)
	 * *			: Alter the test class of casetriggerhandler_Test.cls depends on below change.
	 */
	public void AfterUpdate(Map<Id, sObject> newMap, Map<Id, sObject> oldMap){

		// If Record Type is Referral then create Application
		// If Record type is complaint then change the status in License , Application and Deficiency ojects
		// and status will be either Revocation or Suspension or Limitation
		createApplicationOrChangeStatusForReferralOrComplaints(newMap,oldMap);	
		updateHistoryTracking(newMap,oldMap);
	}

	public void AfterDelete(Map<id, sObject> oldMap){
		
	}

	public void AfterUndelete(List<Sobject> newMap){
		
	}
}