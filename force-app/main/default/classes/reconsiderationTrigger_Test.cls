@isTest(SeeAllData=false)
private class reconsiderationTrigger_Test{
    private static testMethod void testReconsiderationTrigger() {
        List<Reconsideration__c> reconsiderList = new List<Reconsideration__c>();
        Reconsideration__c testReconsider;
        for(Integer i=0; i<5 ;i++){
        	testReconsider = new Reconsideration__c(Status__c = 'InProgress');
        	reconsiderList.add(testReconsider);
        }
        insert reconsiderList;
        
        for(Reconsideration__c rec : reconsiderList){
        	rec.Status__c = 'InProgress';
        }
        
        Test.startTest();
        update reconsiderList;
        Test.stopTest();
    }
    
    private static testMethod void testDeleteReconsiderMethod() {
        List<Reconsideration__c> reconsiderList = new List<Reconsideration__c>();
        Reconsideration__c testReconsider;
        for(Integer i=0; i<5 ;i++){
            testReconsider = new Reconsideration__c(Status__c = 'InProgress');
        	reconsiderList.add(testReconsider);
        }
        insert reconsiderList;  
        
        Test.startTest();
        delete reconsiderList;
        Test.stopTest();
    }
    
    private static testMethod void testUnDeleteReconsiderMethod() {
        List<Reconsideration__c> reconsiderList = new List<Reconsideration__c>();
        Reconsideration__c testReconsider;
        for(Integer i=0; i<5 ;i++){
            testReconsider = new Reconsideration__c(Status__c = 'InProgress');
        	reconsiderList.add(testReconsider);
        }
        insert reconsiderList;  
        delete reconsiderList;
        Test.startTest();
        undelete reconsiderList;
        Test.stopTest();
    }
    
}