/**
     * @Author        : Sindhu Venkateswarlu
     * @CreatedOn     : May 6,2020
     * @Purpose       :Test Methods to unit test for ApplicationProviderNarrative.cls
     **/

@isTest
public class ApplicationProviderNarrative_Test {
      @isTest static void testgetNarrativeDetails (){
          Application__c InsertobjNewApplication=new Application__c(Status__c = 'Pending'); 
          insert InsertobjNewApplication;
          try
           {
            applicationproviderNarrative.getNarrativeDetails(null);
           }catch(Exception Ex)
           {
           }
          applicationproviderNarrative.getNarrativeDetails('XYZ');
          try
           {
            applicationproviderNarrative.getApplicationProviderStatus(null);
           }catch(Exception Ex)
           {
           }
          applicationproviderNarrative.getApplicationProviderStatus('status');
          applicationproviderNarrative.getUserEmail();
          string applicationId= applicationproviderNarrative.updateNarrativeDetails(InsertobjNewApplication);
      }
    @istest static void testgetHistoryOfNarrativeDetails(){
        HistoryTracking__c InsertobjNewHistoryTracking=new HistoryTracking__c(); 
        insert InsertobjNewHistoryTracking;
        try
           {
            applicationproviderNarrative.getHistoryOfNarrativeDetails(null);
           }catch(Exception Ex)
           {
           }
        applicationproviderNarrative.getHistoryOfNarrativeDetails('abc');
        try
        {
         applicationproviderNarrative.getSelectedHistoryRec(null);
        }catch(Exception Ex)
        {
        }
        applicationproviderNarrative.getSelectedHistoryRec('history');    
    }

}