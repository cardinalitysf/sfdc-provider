public with sharing class TaskService {   

    public void updateMonitoring(List<Monitoring__c> lstMonitoringToUpdate)
    {
       DMLOperationsHandler.updateOrInsertSOQLForList(lstMonitoringToUpdate) ;
    }
    public void updateTaskStatus(List<Task> lstTasksToUpdate)
    {
       DMLOperationsHandler.updateOrInsertSOQLForList(lstTasksToUpdate) ;
    }
    
}