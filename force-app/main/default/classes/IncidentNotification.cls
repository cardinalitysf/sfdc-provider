public with sharing class IncidentNotification {
    public static string strClassNameForLogger='IncidentNotification';
    @AuraEnabled(cacheable=true)
    public static List<SObject> getQuestionsByType(string type) {
     List<sObject> questions= new List<sObject>();
     try{
        Set<String> fields = new Set<String>{'Id, Question__c, Type__c, RecordTypeId,QuestionOrder__c,RefKey__c'};
         questions =new DMLOperationsHandler('ReferenceValue__c').
        selectFields(fields).
        addConditionEq('Type__c', type).    
        run();
        if(Test.isRunningTest())
        {
        if(type==null ){
        throw new DMLException();
        }

        }

    } catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getQuestionsByType',type,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Notification Questions',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));              
        }
        return questions;
    }

    @AuraEnabled(cacheable=true)
    public static Id getRecordType(String name) {
        return CommonUtils.getRecordTypeIdbyName('ProviderRecordQuestion__c', name);
    }

    
@AuraEnabled
public static void bulkAddRecordAns(String datas,string questionId){
try{
        List<ProviderRecordAnswer__c> dataToInsert = (List<ProviderRecordAnswer__c>)System.JSON.deserialize(datas, List<ProviderRecordAnswer__c>.class);
        List<ProviderRecordAnswer__c> listInsert = new List<ProviderRecordAnswer__c>();
        for(ProviderRecordAnswer__c p: dataToInsert){
        ProviderRecordAnswer__c objTask = new ProviderRecordAnswer__c();
        objTask = new ProviderRecordAnswer__c(             
        
        Date__c = p.Date__c, 
        Comar__c=p.Comar__c,
        Comments__c  = p.Comments__c,  
        Received__c=p.Received__c,
        Phone__c=p.Phone__c,
        Name__c=p.Name__c,
        Method__c=p.Method__c,
        Email__c =p.Email__c ,
        Findings__c=p.Findings__c,
        StaffMembercompletingReport__c=p.StaffMembercompletingReport__c,
        ProviderRecordQuestion__c = questionId);
        listInsert.add(objTask);
        }
        DMLOperationsHandler.updateOrInsertSOQLForListWithStatus(listInsert);
}
catch (Exception Ex) {
        String[] arguments = new String[] {datas , questionId};
        string strInputRecord= String.format('Input parameters are :: datas -- {0} :: questionId -- {1} ', arguments);
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'bulk insert Notications',strInputRecord,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('bulk insert Notifications',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
}

}

@AuraEnabled(cacheable=false)
public static List<ProviderRecordQuestion__c > getRecordQuestionId(string incidentId, string recordTypeId) {
     List<ProviderRecordQuestion__c > QuestionId = new List<ProviderRecordQuestion__c >();
     try {
         QuestionId =new DMLOperationsHandler('ProviderRecordQuestion__c').
         selectFields('Id,status__c').
         addConditionEq('Incident__c', incidentId).
         addConditionEq('RecordTypeId', recordTypeId).
         run();
             if(Test.isRunningTest())
             {
                 if(incidentId==null || recordTypeId==null){
                     throw new DMLException();
                 }
             }            
     } catch (Exception Ex) {
         String[] arguments = new String[] {incidentId , recordTypeId};
         string strInputRecord= String.format('Input parameters are :: Application Id -- {0} :: recordTypeId -- {1}', arguments);            
     //To Log into object
     CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getRecordQuestionId',strInputRecord,Ex);
     //To return back to the UI
     CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Notificationschecklist Question Id',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
     throw new AuraHandledException(JSON.serialize(oErrorData));
     }
    return QuestionId;
 }
 @AuraEnabled(cacheable=false)
    public static List<ProviderRecordAnswer__c > getProviderAnswer(string getAnswers) {
        List<ProviderRecordAnswer__c > Answers = new List<ProviderRecordAnswer__c >();
        try {
            List<String> fieldList = getAnswers.split(', *');
            Answers =
            new DMLOperationsHandler('ProviderRecordAnswer__c').
            selectFields('Id,Name__c,ProviderRecordQuestion__c,Date__c,Comments__c,Phone__c,StaffMembercompletingReport__c,Received__c,Findings__c,Email__c,Method__c,Comar__r.RefKey__c,Comar__r.QuestionOrder__c,Comar__r.Question__c').
            addConditionIn('ProviderRecordQuestion__c', fieldList).
            run();
                            
        } catch (Exception Ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getProviderAnswer',getAnswers,Ex);
        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Notifications Answers',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));            
        }
       return Answers;
    }  

    @AuraEnabled
public static void bulkUpdateRecordAns(String datas){
try{
        List<ProviderRecordAnswer__c> dataToInsert = (List<ProviderRecordAnswer__c>)System.JSON.deserialize(datas, List<ProviderRecordAnswer__c>.class);
        List<ProviderRecordAnswer__c> listUpdate = new List<ProviderRecordAnswer__c>();
        for(ProviderRecordAnswer__c p: dataToInsert){
        ProviderRecordAnswer__c objTask = new ProviderRecordAnswer__c();
        objTask = new ProviderRecordAnswer__c(
        Id = p.Id,
        Date__c = p.Date__c, 
        Comar__c=p.Comar__c,
        Comments__c  = p.Comments__c,  
        Received__c=p.Received__c,
        Phone__c=p.Phone__c,
        Name__c=p.Name__c,
        Method__c=p.Method__c,
        Email__c =p.Email__c ,
        Findings__c=p.Findings__c,
        StaffMembercompletingReport__c=p.StaffMembercompletingReport__c,
        ProviderRecordQuestion__c = p.ProviderRecordQuestion__c);
        listUpdate.add(objTask);
        }
        DMLOperationsHandler.updateOrInsertSOQLForListWithStatus(listUpdate);
}
catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'bulkUpdateRecordAns',datas,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Update Notification  based on Incident',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
}

}

}