/**
 * @Author        : Marimuthu V S
 * @CreatedOn     : July 09 , 2020
 * @Purpose       : Common Class to store the exception into custom Object
 **/
global class CommonDebugLogger {
    
    //Structure of the Log
    public virtual class Log{
        public String Type;
        public String ApexClass;
        public String Method;

        public String RecordId;
        public String Message;
        public String StackTrace;
        public Exception Cause;
        public Integer LineNumber;
        public String TypeName;
    }
    
    //  A Class that Extends the Virtual Class - Log  to define a log of type - Error   
    public class Error extends Log{
        public Error(String cls, String routine, String recId, Exception ex){
            this.Type = 'Error';
            this.ApexClass = cls;
            this.Method = routine;
            this.RecordId = recId;
            this.Message = ex.getMessage();
            this.StackTrace = ex.getStackTraceString();
            this.cause=  ex.getCause();
            this.LineNumber=ex.getLineNumber();
            this.TypeName=ex.getTypeName();
        }
    }

   // A Class that Extends the Virtual Class - Log  to define a log of type - Information   
    public class Information extends Log{
        public Information(String cls, String routine, String recId, String msg){
            this.Type = 'Information';
            this.ApexClass = cls;
            this.Method = routine;
            this.RecordId = recId;
            this.Message = msg;
            this.StackTrace = NULL;
            
        }
    }
    /*  A Public Method that can be utilized by
        other Apex Classes to create a record into
        the Apex Debug Log object stating the Error
        or Information.
    */
    public void createLog(Log logToCreate){
        try{           
            if(
                (Limits.getDMLRows() < Limits.getLimitDMLRows()) && 
                (Limits.getDMLStatements() < Limits.getLimitDMLStatements())
            )
            {
                DebugLog__c apexDebuglog = new DebugLog__c(
                    Type__c         = logToCreate.Type,
                    Apex_Class__c   = logToCreate.ApexClass,
                    Method__c       = logToCreate.Method,
                    Record_Id__c    = logToCreate.RecordId,
                    Message__c      = logToCreate.Message,
                    Stack_Trace__c  = logToCreate.StackTrace,
                    TypeName__c     = logToCreate.TypeName,
                    LineNumber__c   = logToCreate.LineNumber,
                    cause__c        = Null
                );

                Database.insert(apexDebuglog, FALSE);
            }
            else{
            }
        }
        catch(DMLException ex){
        }
    }

    /*
        A Public Method that can be utilized from
        JavaScript to create record(s) [aka Logs] into the
        Custom Object.
    */
    // webService
    // static void createLog(String log){
    //     try{
    //         /*
    //             Expects a JSON of the form - 
    //             {
    //                 "Type"       : "---",
    //                 "ApexClass"  : "---",
    //                 "Method"     : "---",
    //                 "RecordId"   : "---",
    //                 "Message"    : "---",
    //                 "StackTrace" : "---",
    //             }
    //         */

    //         /*Deserialize the same to create an instance of the virtual class - ApexDebugLog.Log*/
    //         CommonDebugLogger.Log logToCreate = (CommonDebugLogger.Log)JSON.deserialize(log, CommonDebugLogger.Log.class);
            
    //         new CommonDebugLogger().createLog(logToCreate);
    //     }
    //     catch(Exception ex){
    //     }
    // }
}
