/*
     Author         : G.sathishkumar
     @CreatedOn     : june 11,2020
     @Purpose       : test class for PublicProviderDecision_Test Details .
*/
@isTest
private class PublicProviderDecision_Test {
    @isTest static void testPublicProviderDecision() {
 
        Case oncase= new Case(Description='test Comments');
        insert oncase;

        Actor__c onActor= new Actor__c(Role__c='Applicant');
        insert onActor;

        Training__c onTraining =new Training__c();
        insert onTraining;

        MeetingInfo__c onMeetinginfo = new MeetingInfo__c(IntenttoAttend__c='Yes', Training__c=onTraining.Id);
        insert onMeetinginfo;


        try{
            List<DMLOperationsHandler.FetchValueWrapper>  objgetPickListValue = PublicProviderDecision.getPickListValues(oncase,'XXX');
        } catch (Exception e) {
        }
    
    try{
        PublicProviderDecision.getDecisionDetails(null); 
    }
    catch(Exception Ex){

    }
        PublicProviderDecision.getDecisionDetails(oncase.Id);

         try{
         PublicProviderDecision.getContactApplicantDetails(null);
    }
    catch(Exception Ex){

    }
        PublicProviderDecision.getContactApplicantDetails(onActor.Id);
        PublicProviderDecision.updateStatus(oncase);

        try{
         PublicProviderDecision.getMeetingDetails(null);
    }
    catch(Exception Ex){

    }
        PublicProviderDecision.getMeetingDetails(onMeetinginfo.Id);

    }
}
