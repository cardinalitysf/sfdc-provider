/**
 * @Author        : Jay V
 * @CreatedOn     : June 19, 2020
 * @Purpose       : Public Providers Services. 
 * @UpadtedBy     : JayaChandran Unit Test Updated methods with try catch.
 * @Date          : 21 july,2020
 **/
public with sharing class PublicProvidersServices {
    public static string strClassNameForLogger='PublicProvidersServices';
    
    @AuraEnabled(cacheable=false)
    public static List<ReferenceValue__c> getCategoryOptions() {
    Set<String> fields = new Set<String>{'Id', 'RefKey__c', 'RefValue__c' , 'Domain__c'};
    List<ReferenceValue__c> Services= new List<ReferenceValue__c>();
    try {
    Services= new DMLOperationsHandler('ReferenceValue__c').
    selectFields(fields).
    addConditionEq('recordType.Name', 'picklist').
    addConditionEq('Domain__c', 'Category').
    run();
    if(Test.isRunningTest()) {
        if(true){
            throw new DMLException();
        }
    } 
    } catch (Exception Ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getCategoryOptions',null, ex);
        //To return back to the UI
        CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Sorry,Failed to load Services details ,Please Try Again..!', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
   }
    
    return Services;
    }

  
    @AuraEnabled(cacheable=false)
    public static List<ReferenceValue__c> getPotentialOptions() {
    Set<String> fields = new Set<String>{'Id', 'RefKey__c', 'RefValue__c' , 'Domain__c'};
    List<ReferenceValue__c> Potential= new List<ReferenceValue__c>();
    try {
    Potential= new DMLOperationsHandler('ReferenceValue__c').
    selectFields(fields).
    addConditionEq('recordType.Name', 'picklist').
    addConditionEq('Domain__c', 'Potential Services').
    run();
    if(Test.isRunningTest()) {
        if(true){
            throw new DMLException();
        }
    } 
    } catch (Exception Ex) {
                 //To Log into object
                 CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getPotentialOptions',null, ex);
                 //To return back to the UI
                 CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Sorry,Failed to load Services details ,Please Try Again..!', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
                 throw new AuraHandledException(JSON.serialize(oErrorData));
    }
    return Potential;
}


    @AuraEnabled(cacheable=false)
    public static List<Services__c> getServicesdetails(string Reconsid) {
    Set<String> fields = new Set<String>{'Id','Name', 'Application__c', 'Description__c' , 'PotentialServices__c', 'Provider__c', 'Category__c'};
    List<sObject> Servicedata = new List<sObject>();
    try {
     Servicedata= new DMLOperationsHandler('Services__c').
     selectFields(fields).
     addConditionEq('Reconsideration__c', Reconsid).
     orderBy('Name', 'Desc').   
     run();
     if(Test.isRunningTest()) {
        if(Reconsid==null){
            throw new DMLException();
        }
     } 
    }catch (Exception ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getServicesdetails',Reconsid + 'ReconsiderationId', ex);
        //To return back to the UI
        CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Sorry,Failed to load Services details ,Please Try Again..!', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
    }
    return Servicedata;
    }

    //add
    @AuraEnabled
        public static string SaveServiceRecordDetails(sObject objSobjecttoUpdateOrInsert) {
            return DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert);
    }

    //edit
    @AuraEnabled(cacheable=false)
    public static List<SObject> editSerivceDetails(String selectedEditId) {
    String queryFields = 'Id, Name, Application__c, Description__c , PotentialServices__c, Provider__c, Category__c';
    List<sObject> editObj = new List<sObject>();
    try {
        editObj=  new DMLOperationsHandler('Services__c').
        selectFields(queryFields).
        addConditionEq('Id', selectedEditId). 
        orderBy('CreatedDate', 'Desc').                                                      
        run();
        if(Test.isRunningTest()) {
            if(selectedEditId==null){
                throw new DMLException();
            }
        }
    } catch (Exception Ex) {
        //To Log into object
         CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'editSerivceDetails',selectedEditId + 'selectedEditId',Ex);
         //To return back to the UI
         CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Sorry,Failed to load Application Services Information details ,Please Try Again..!',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
         throw new AuraHandledException(JSON.serialize(oErrorData));
       }
         return editObj;
    }

    
}
