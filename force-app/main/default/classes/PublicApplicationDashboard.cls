/**
 * @Author        : Preethi Bojarajan
 * @CreatedOn     : Jun 09,2020
 * @Purpose       : Public/ Private Applictions Dashboard Apex Classes
 * @updatedBy     : Preethi Bojarajan
 * @updatedOn     : July 15, 2020
 **/

public with sharing class PublicApplicationDashboard {
    @AuraEnabled(cacheable=false)
    public static string getDashboardData(String searchKey, String type, String user) {

        String model = 'Application__c';
        String key = '%' + searchKey + '%';
        String appType = 'Public';
        List<String> lstStatus = type.split(',');
        String fields = 'Id, Name, Program__c, ProgramType__c, Provider__r.ProviderId__c, ReviewStatus__c, Status__c, Provider__r.ProviderId__c, RecordType.Name, Supervisor__c';

        if(type == 'pending' && user != 'Supervisor'){
            lstStatus.add('Pending');
            lstStatus.add('Assigned for Training');
            lstStatus.add('Training Completed');
            lstStatus.add('Training Incompleted');
            lstStatus.add('Caseworker Submitted');
        } else if(type == 'pending' && user == 'Supervisor')
        lstStatus.add('Caseworker Submitted');
        else if(type == 'rejected' && user != 'Supervisor'){
            lstStatus.add('Rejected');            
            lstStatus.add('Supervisor Rejected');
        } else if(type == 'approved'){
            lstStatus.add('Approved');
            lstStatus.add('Revocation');
            lstStatus.add('Suspension');
            lstStatus.add('Limitation');
        } else if(type == 'rejected' && user == 'Supervisor')
        lstStatus.add('Supervisor Rejected');
        else if(type == 'total' && user != 'Supervisor'){
            lstStatus.add('Pending');
            lstStatus.add('Assigned for Training');
            lstStatus.add('Training Completed');
            lstStatus.add('Training Incompleted');
            lstStatus.add('Caseworker Submitted');
            lstStatus.add('Approved');
            lstStatus.add('Rejected');
            lstStatus.add('Returned');
            lstStatus.add('Supervisor Rejected');
            lstStatus.add('Revocation');
            lstStatus.add('Suspension');
            lstStatus.add('Limitation');
        } else if(type == 'total' && user == 'Supervisor'){
            lstStatus.add('Caseworker Submitted');
            lstStatus.add('Approved');
            lstStatus.add('Returned');
            lstStatus.add('Supervisor Rejected');
            lstStatus.add('Revocation');
            lstStatus.add('Suspension');
            lstStatus.add('Limitation');
        }
        List<Application__c> recordType = new DMLOperationsHandler(model).
                                            selectFields(fields).
                                            addConditionEq('RecordType.Name', appType).
                                            addConditionIn('Status__c', lstStatus).
                                            addConditionLike('Name', key).
                                            orderBy('LastModifiedDate', 'DESC').
                                            run();
        Set<Id> filteredProvId = new Set <Id>();
        for(Application__c clist : recordType){
          filteredProvId.add(clist.Provider__c);
        }
        String actorFields = 'Id, Provider__c, Contact__r.LastName, Contact__r.FirstName__c, Contact__r.Lastname__c, Role__c';
        List<Actor__c> results =  new DMLOperationsHandler('Actor__c'). 
                                 selectFields(actorFields).
                                 addConditionIn('Provider__c', filteredProvId).
                                 run();
        List<processingWrapper> appWrap = new List<processingWrapper>();
                processingWrapper prdWrapper = new processingWrapper(recordType, results);
                appWrap.add(prdWrapper);
        return JSON.serialize(appWrap);
    }

    public class processingWrapper {
        public List<Application__c> application {get;set;}
        public List<Actor__c> actor {get;set;}
        public processingWrapper(List<Application__c> app, List<Actor__c> actorDet) {
            application = app;
            actor = actorDet;
        }
    }
    

    @AuraEnabled(cacheable=true)
    public static List<WrapperCasecount> getApplicationCount(String searchKey, Boolean user, String type) {
        List<WrapperCasecount> lstWrapper = new List<WrapperCasecount>();
        String key = '%' + searchKey + '%';
        Integer countPending = 0;
        Integer countForReview = 0;
        Integer countAssignTrain = 0;
        Integer countTrainComp = 0;
        Integer countTrainIncomp = 0;
        Integer countCaseworkSum = 0;
        Integer countApproved = 0;
        Integer countRejected = 0;
        Integer countSupRejected = 0;
        Integer countReturned = 0;
        Integer countRevocation = 0;
        Integer countSuspension = 0;
        Integer countLimitation = 0;
        List<AggregateResult> results =  new DMLOperationsHandler('Application__c'). 
                                 selectFields('Status__c').
                                 addConditionLike('Name', key).
                                 addConditionEq('RecordType.Name', type).
                                 count('Id').   
                                 groupBy('Status__c').
                                 run();
            for(AggregateResult result : results) {
                if(result.get('Status__c') == 'Pending')
                countPending = (Integer) result.get('expr0');
                else if(result.get('Status__c') == 'For Review')
                countForReview = (Integer) result.get('expr0');
                else if(result.get('Status__c') == 'Assigned for Training')
                countAssignTrain = (Integer) result.get('expr0');
                else if(result.get('Status__c') == 'Training Completed')
                countTrainComp = (Integer) result.get('expr0');
                else if(result.get('Status__c') == 'Training Incompleted')
                countTrainIncomp = (Integer) result.get('expr0');
                else if(result.get('Status__c') == 'Caseworker Submitted')
                countCaseworkSum = (Integer) result.get('expr0');
                else if(result.get('Status__c') == 'Approved')
                countApproved = (Integer) result.get('expr0');
                else if(result.get('Status__c') == 'Rejected')
                countRejected = (Integer) result.get('expr0');
                else if(result.get('Status__c') == 'Supervisor Rejected')
                countSupRejected = (Integer) result.get('expr0');
                else if(result.get('Status__c') == 'Returned')
                countReturned = (Integer) result.get('expr0');
                else if(result.get('Status__c') == 'Revocation')
                countRevocation = (Integer) result.get('expr0');
                else if(result.get('Status__c') == 'Suspension')
                countSuspension = (Integer) result.get('expr0');
                else if(result.get('Status__c') == 'Limitation')
                countLimitation = (Integer) result.get('expr0');
            }
        lstWrapper.add(new Wrappercasecount(countPending, countForReview, countAssignTrain, countTrainComp, countTrainIncomp, countCaseworkSum, countApproved, countRejected, countSupRejected, countReturned, countRevocation, countSuspension, countLimitation, user, type)); 
        return lstWrapper;
    }
         // Wrapper Class
    public class Wrappercasecount {
        @AuraEnabled public Integer pending;
        @AuraEnabled public Integer approved;
        @AuraEnabled public Integer rejected;
        @AuraEnabled public Integer returned;
        @AuraEnabled public Integer total;
    public Wrappercasecount(Integer countPending, Integer countForReview, Integer countAssignTrain, Integer countTrainComp, Integer countTrainIncomp, Integer countCaseworkSum, Integer countApproved, Integer countRejected, Integer countSupRejected, Integer countReturned,Integer countRevocation,Integer countSuspension,Integer countLimitation, Boolean user, String type) {
        if(user == true && type == 'Public'){
            this.pending = countCaseworkSum;
            this.rejected = countSupRejected;
        } else if(user == false && type == 'Public'){
            this.pending = countPending + countAssignTrain + countTrainComp + countTrainIncomp + countCaseworkSum;
            this.rejected = countRejected + countSupRejected;
        } else if(user == false && type == 'Private'){
            this.pending = countForReview + countCaseworkSum;
            this.rejected = countRejected;
        } else {
            this.pending = countCaseworkSum;
            this.rejected = countSupRejected;
        }
        this.approved = countApproved + countRevocation + countSuspension + countLimitation;
        this.returned = countReturned;
        this.total = this.pending + this.approved + this.rejected + countReturned;
    }
}

@AuraEnabled(cacheable=true)
public static List<Application__c> getPrivateDashboardData(String searchKey, String type, String user) {
    String model = 'Application__c';
    String key = '%' + searchKey + '%';
    String provType = 'Private';
    String fields = 'Name, Capacity__c, Program__c, ProgramType__c, Provider__r.ProviderId__c, ReviewStatus__c, Status__c, Provider__r.ProviderId__c, RecordType.Name, Supervisor__c';
    String status = '';
    List<String> lstStatus = status.split(',');
    if(type == 'total' && user != 'Supervisor'){
        lstStatus.add('For Review');
        lstStatus.add('Caseworker Submitted');
        lstStatus.add('Approved');
        lstStatus.add('Rejected');
        lstStatus.add('Returned');
        lstStatus.add('Revocation');
        lstStatus.add('Suspension');
        lstStatus.add('Limitation');
    } else if(type == 'total' && user == 'Supervisor'){
        lstStatus.add('Approved');
        lstStatus.add('Rejected');
        lstStatus.add('Returned');
        lstStatus.add('Revocation');
        lstStatus.add('Suspension');
        lstStatus.add('Limitation');
    } else if(type == 'pending' && user != 'Supervisor'){
        lstStatus.add('For Review');
        lstStatus.add('Caseworker Submitted');
    } else if(type == 'pending' && user == 'Supervisor'){
        lstStatus.add('Caseworker Submitted');
    } else if(type == 'approved'){
        lstStatus.add('Approved');
        lstStatus.add('Revocation');
        lstStatus.add('Suspension');
        lstStatus.add('Limitation');
    } else lstStatus.add(type);
    List<SObject> applicationList = new DMLOperationsHandler(model).
                                        selectFields(fields).
                                        addConditionEq('RecordType.Name', provType).
                                        addConditionIn('Status__c', lstStatus).
                                        addConditionLike('Name', key).
                                        orderBy('LastModifiedDate', 'DESC').
                                        run();
    return applicationList;
}
}
