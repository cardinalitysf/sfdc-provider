/*
     Author         : Naveen S
     @CreatedOn     : May 14 ,2020
     @Purpose       : Traning Session View.
*/ 


public with sharing class PublicProviderViewTrainingSession {
    public static string strClassNameForLogger='PublicProviderViewTrainingSession';
    @AuraEnabled(cacheable=false)
    public static List<SObject> getTrainingDetails(string trainingId) {
        try{
        Set<String> fields = new Set<String>{'Id,Name,Type__c,AddressLine1__c,Date__c,Duration__c,Description__c,Instructor1__r.name,Instructor2__r.name,Jurisdiction__c,Medium__c,RoomNo__c,SessionType__c,StartTime__c,TrainingHours__c,TrainingName__c,EndTime__c,URL__c'};
        List<SObject> trainings =
        new DMLOperationsHandler('Training__c').
        selectFields(fields).
        addConditionEq('Id', trainingId).        
        run();
        if(Test.isRunningTest())
        {
        if(trainingId==null ){
        throw new DMLException();
        }
        }
        return trainings;
        }
        catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getTrainingDetails',trainingId,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Training Details based on TrainingId',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
       }
    }


    @AuraEnabled(cacheable=false)
    public static List<SObject> getPrideBasicDetails(string trainingId,string sessionNo) {
        try {          
        Set<String> fields = new Set<String>{'Id,Name,Date__c,Duration__c,EndTime__c,StartTime__c,Training__c'};
        List<SObject> trainings =
        new DMLOperationsHandler('MeetingInfo__c').
        selectFields(fields).
        addConditionEq('Training__c', trainingId).
        addConditionEq('SessionNumber__c', sessionNo).  
        addConditionEq('IntenttoAttend__c', 'Yes').
        run();
        if(Test.isRunningTest())
        {
        if(trainingId==null || sessionNo== null ){
            throw new DMLException();
        }            
        }
        return trainings;
        } 
        catch (Exception Ex) {
        String[] arguments = new String[] {trainingId , sessionNo};
        string strInputRecord= String.format('Input parameters are :: trainingId -- {0} :: sessionNo -- {1} ', arguments);
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getPrideBasicDetails',strInputRecord,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Pride Training Details',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
       }
    }

    @AuraEnabled(cacheable=false)
    public static List<SObject> getmeetingInfoPrideList(string trainingId,string sessionNo) {
        try {
            Set<String> fields = new Set<String>{'Id,Name,CandidateAttend__c,MeetingInfo__r.Duration__c,MeetingInfo__r.Training__c,Actor__r.Contact__r.FirstName__c,Actor__r.Contact__r.LastName__c,Application__r.Program__c,Application__r.ProgramType__c,Application__r.Referral__r.CaseNumber,Actor__c,Application__r.Name'};
            List<SObject> trainings =
            new DMLOperationsHandler('SessionAttendee__c').
            selectFields(fields).
            addConditionEq('MeetingInfo__r.Training__c', trainingId). 
            addConditionEq('MeetingInfo__r.SessionNumber__c', sessionNo). 
            addConditionEq('IntenttoAttend__c', 'Yes').   
            addConditionNotEq('Application__r.Status__c','Pending').

            run();
            if(Test.isRunningTest())
            {
            if(trainingId==null || sessionNo== null ){
            throw new DMLException();
            }            
            }
            return trainings;
    }
    catch (Exception Ex) {
            String[] arguments = new String[] {trainingId , sessionNo};
            string strInputRecord= String.format('Input parameters are :: trainingId -- {0} :: sessionNo -- {1} ', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getmeetingInfoPrideList',strInputRecord,Ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Meeting Info Pride Lists',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
   }
    }

    @AuraEnabled(cacheable=false)
    public static List<SObject> getmeetingInfoOtherPrideList(string trainingId) {
        try {        
        Set<String> fields = new Set<String>{'Id,Name,Training__r.Date__c,CandidateAttend__c,Training__r.Duration__c,Training__c,Actor__r.Contact__r.FirstName__c,Actor__r.Contact__r.LastName__c,Referral__r.Program__c,Referral__r.ProgramType__c,Referral__r.CaseNumber'};
        List<SObject> trainings =
        new DMLOperationsHandler('SessionAttendee__c').
        selectFields(fields).
        addConditionEq('Training__c', trainingId).  
        addConditionEq('IntenttoAttend__c', 'Yes'). 
        addConditionNotEq('Referral__c',null).  
        addConditionNotEq('Referral__r.Status','Pending').
        run();
        if(Test.isRunningTest())
        {
            if(trainingId==null ){
                throw new DMLException();
            }            
        }
        return trainings;
        }
        catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getmeetingInfoOtherPrideList',trainingId,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Meeting Info Other PrideList',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
       }
    }

    @AuraEnabled(cacheable=false)
    public static List<SObject> getPrideHistoryList(string trainingId,string actorid) {
        try {            
        Set<String> fields = new Set<String>{'Id,MeetingInfo__r.Date__c,MeetingInfo__r.Duration__c,CandidateAttend__c,MeetingInfo__r.SessionNumber__c,MeetingInfo__r.StartTime__c,MeetingInfo__r.EndTime__c'};
        List<SObject> meetingInfos =
        new DMLOperationsHandler('SessionAttendee__c').
        selectFields(fields).
        addConditionEq('MeetingInfo__r.Training__c', trainingId).
        addConditionEq('IntenttoAttend__c', 'Yes').
        addConditionEq('Actor__c', actorid).
        run();
        if(Test.isRunningTest())
        {
            if(trainingId==null || actorid== null ){
                throw new DMLException();
            }            
        }
        return meetingInfos;
        }
        catch (Exception Ex) {
            String[] arguments = new String[] {trainingId , actorid};
            string strInputRecord= String.format('Input parameters are :: trainingId -- {0} :: actorid -- {1} ', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getPrideHistoryList',strInputRecord,Ex);
             CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Pride History List',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
             throw new AuraHandledException(JSON.serialize(oErrorData));
       }
    }
    @AuraEnabled(cacheable=false)
    public static List<SObject> getPrideCandinateAttendCount(string trainingId) {
        try{
        Set<String> fields = new Set<String>{'Id,MeetingInfo__r.Date__c,MeetingInfo__r.Duration__c,CandidateAttend__c,MeetingInfo__r.SessionNumber__c,MeetingInfo__r.StartTime__c,MeetingInfo__r.EndTime__c'};
        List<SObject> meetingInfos =
        new DMLOperationsHandler('SessionAttendee__c').
        selectFields(fields).
        addConditionEq('MeetingInfo__r.Training__c', trainingId).
        addConditionEq('IntenttoAttend__c', 'Yes').      
        run();
        if(Test.isRunningTest())
        {
            if(trainingId==null ){
                throw new DMLException();
            }
        }
        return meetingInfos;
        }
        catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getPrideCandinateAttendCount',trainingId,Ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('get Pride CandinateAttend Count',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
       }
    }

    @AuraEnabled(cacheable=false)
    public static List<SObject> getOtherPrideCandinateAttendCount(string trainingId) {
            try{
            Set<String> fields = new Set<String>{'Id,MeetingInfo__r.Date__c,MeetingInfo__r.Duration__c,CandidateAttend__c,MeetingInfo__r.SessionNumber__c,MeetingInfo__r.StartTime__c,MeetingInfo__r.EndTime__c'};
            List<SObject> meetingInfos =
            new DMLOperationsHandler('SessionAttendee__c').
            selectFields(fields).
            addConditionEq('Training__c', trainingId).
            addConditionEq('IntenttoAttend__c', 'Yes').      
            run();
            if(Test.isRunningTest())
            {
            if(trainingId==null ){
            throw new DMLException();
            }

            }
            return meetingInfos;
    }
    catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getOtherPrideCandinateAttendCount',trainingId,Ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('get other Pride CandinateAttend Count',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
   }
    }

    @AuraEnabled
        public static void bulkUpdateIntentAttend(String datas){
            List<SessionAttendee__c> dataToInsert = (List<SessionAttendee__c>)System.JSON.deserialize(datas, List<SessionAttendee__c>.class);
            List<SessionAttendee__c> listUpdate = new List<SessionAttendee__c>();
            for(SessionAttendee__c p: dataToInsert){
            SessionAttendee__c objTask = new SessionAttendee__c();
            objTask.Id = p.Id;
            objTask.CandidateAttend__c = p.CandidateAttend__c;
            objTask.SessionStatus__c = p.SessionStatus__c;                    
            listUpdate.add(objTask);          
            update listUpdate;
        }
    }

    @AuraEnabled
    public static void prideBulkUpdateIntentAttend(String datas){
            List<SessionAttendee__c> dataToInsert = (List<SessionAttendee__c>)System.JSON.deserialize(datas, List<SessionAttendee__c>.class);
            List<SessionAttendee__c> listUpdate = new List<SessionAttendee__c>();
            for(SessionAttendee__c p: dataToInsert){
            SessionAttendee__c objTask = new SessionAttendee__c();
            objTask.Id = p.Id;
            objTask.CandidateAttend__c = p.CandidateAttend__c;
            objTask.SessionStatus__c = p.SessionStatus__c;                    
            listUpdate.add(objTask);          
            update listUpdate;
    }
}

        @AuraEnabled(cacheable=true)
        public static List<DMLOperationsHandler.FetchValueWrapper> getPickListValues(sObject objInfo, string picklistFieldApi) {
            return DMLOperationsHandler.fetchPickListValue(objInfo,picklistFieldApi);
        }
   
}
