/**
     * @Author        : Sindhu Venkateswarlu
     * @CreatedOn     : May 6,2020
     * @Purpose       :Test Methods to unit test for AssignStaffMember.cls
     **/

@isTest
public class AssignStaffMember_Test {
     @isTest static void testgetStaffFromProviderID(){
        
        Account InsertobjNewAccount=new Account(Name = 'test Account'); 
           insert InsertobjNewAccount;
        
         TestDataFactory.createTestContacts(1,InsertobjNewAccount.id,true);
      
        Application__c InsertobjNewApplication=new Application__c(Status__c = 'Pending'); 
           insert InsertobjNewApplication;

         Application_Contact__c InsertobjNewApplicationContact=new Application_Contact__c(Application__c = InsertobjNewApplication.Id); 
           insert InsertobjNewApplicationContact;
        
        
        try{
            assingStaffMember.getStaffFromProviderID(null);
        }catch (Exception Ex) {}
         assingStaffMember.getStaffFromProviderID('contact');

           try{
              assingStaffMember.getStaffFromApplicationID(null);
        }catch (Exception Ex) {}
           assingStaffMember.getStaffFromApplicationID(InsertobjNewApplicationContact.Id);
            
          try{
             assingStaffMember.InsertUpdateStaffApplication(null);
            }catch (Exception Ex) {}
             assingStaffMember.InsertUpdateStaffApplication(InsertobjNewAccount);

          try{
            assingStaffMember.getProviderApplicationStatus(null);
           }catch (Exception Ex) {}
           assingStaffMember.getProviderApplicationStatus('');

        try{
            assingStaffMember.deleteStaffRecord(InsertobjNewAccount.Id);
          }catch (Exception Ex) {}
       
       }
       
}