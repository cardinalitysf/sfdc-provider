/*
     Author         : G.sathishkumar
     @CreatedOn     : june 25 ,2020
     @Purpose       : test class for PublicProvidersServices_Test Details .
     @UpadtedBy     : JayaChandran Unit Test Updated methods with try catch.
     @Date          : 21 july,2020
     */

@isTest
public  class PublicProvidersServices_Test {
    @isTest static void  testPublicProvidersServices() {

        Services__c serviceval= new Services__c(Description__c ='ABC'); 
        insert serviceval;

        try{
            PublicProvidersServices.getPotentialOptions();
        }catch(Exception Ex) {

            }

        try{
            PublicProvidersServices.getCategoryOptions();
         }catch(Exception Ex) {
    
            }
        
        try{
            PublicProvidersServices.getServicesdetails(null);
            }catch(Exception Ex) {

            }
            PublicProvidersServices.getServicesdetails(serviceval.Id);

        try{
            PublicProvidersServices.SaveServiceRecordDetails(null);
            }catch(Exception Ex) {

            }
            PublicProvidersServices.SaveServiceRecordDetails(serviceval);

         try{
            PublicProvidersServices.editSerivceDetails(null);
            }catch(Exception Ex) {

            }
            PublicProvidersServices.editSerivceDetails(serviceval.Id);  

    }
}
