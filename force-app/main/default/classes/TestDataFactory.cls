public without sharing class TestDataFactory { 

    public TestDataFactory(){}
    
    static public List<Account> testAccountData(){
        
        //RecordType RecType = [Select Id From RecordType  Where SobjectType = 'Account'];
        
        Integer numToInsert = 5;
        List<Account> accList =new List<Account>();
        
        for(Integer i=0; i< numToInsert; i++)
        {
            Account acct = new Account();
            
            acct.ParentCorporation__c = 'Cardinality Provider-'+i;
            acct.Corporation__c = 'Cardinality-'+i;
            acct.TaxId__c = 'c2345679';
            acct.SON__c = 'Yes';
            acct.RFP__c = 'No';
            acct.FEINTaxID__c = 'Yes';
            acct.Profit__c = 'Non-Profit';
            acct.Name = 'Cardinality-CW-'+i;
            acct.FirstName__c = 'test';
            acct.LastName__c = 'Provider-'+i;
            acct.Email__c = 'Provider-'+i+'@test.com';
            acct.CellNumber__c = '2345672882'+i;
            acct.ProviderType__c = '';
            acct.Accrediation__c = '';
            
            accList.add(acct);
        }
            return accList;
    }
      
    public static List<Case> TestCaseData(Integer numToInsert,string status){
        
         numToInsert = numToInsert == null ? 5 :numToInsert;
        List<Case> caseList =new List<Case>();
        
        for(Integer i=0; i< numToInsert; i++)
        {
            Case cs = new Case();
            cs.Origin = 'Email';
            cs.Description = 'test Comments';
            cs.Status = status;
            cs.Program__c = '';
            cs.ProgramType__c = '';          
			caseList.add(cs);            
        }
        return caseList;
    }
    public static List<Application__c> TestApplicationData(Integer numToInsert,string status){
        
         numToInsert = numToInsert == null ? 5 :numToInsert;
        List<Application__c> applicationList =new List<Application__c>();
        
        for(Integer i=0; i< numToInsert; i++)
        {
            Application__c app = new Application__c();
            app.ApprovalComments__c = 'test Comments';
            app.ApprovalStatus__c = '';
            app.Capacity__c = 10; 
            app.ProgramType__c	='';
            app.Comments__c ='comments';
            app.Gender__c ='male';
            app.MaxAge__c = 18;
            app.MaxIQ__c = 200;
            app.MinAge__c= 3;
            app.MinIQ__c= 1;
            app.Narrative__c='text';

			applicationList.add(app);            
        }
        return applicationList;
    }
    
	public static List<ContentVersion> createTestContentVersionData()
    {
        Integer numToInsert = 5;
        List<ContentVersion> cvList =new List<ContentVersion>();
        
        for(Integer i=0; i< numToInsert; i++)
        {
            ContentVersion cv=new Contentversion();
            cv.title='ABC'+i;
            cv.PathOnClient ='test'+i;
            Blob b=Blob.valueOf('Unit Test Attachment Body');
            cv.versiondata=EncodingUtil.base64Decode('Unit Test Attachment Body');
            cvList.add(cv);
        }
        
        return cvList;
    } 
    
     public static List<User> createTestUsers( Integer numToInsert, Id profileId, Boolean doInsert ){
            List<User> users = new List<User>();
            for(Integer i=0; i< numToInsert; i++)
            {
                 User u = new User(alias = 'name1'+i, email='test1'+i+'@test12.com', emailencodingkey='UTF-8', 
                                   lastname='Testing', languagelocalekey='en_US', localesidkey='en_US', profileid = profileId,
                                   timezonesidkey='America/Los_Angeles', username= 'testZZ'+i +'@testZZ-00.com'); 
                 
                  users.add( u );
             }
            if( doInsert)
              insert users;
            return users;
    }
    
    private static Profile tProf;
    
    public static Profile getTestProfile()
    {
        if (tProf == null)
        {
            tProf = [Select Id, Name From Profile limit 1].get(0);
        }
      return tProf; 
    }
    
     public static List<Address__c> createTestAddressData( Integer numToInsert, Boolean doInsert ){
        List<Address__c> addsList = new List<Address__c>();
        for(Integer i=0; i< numToInsert; i++)
        {
            Address__c add = new Address__c();
            add.AddressLine1__c = '345 California St';
            add.City__c='CA';
            add.State__c = 'CA';
            add.County__c = 'USA';
            add.Zipcode__c = 94104;
            
            addsList.add( add);
        }
        if( doInsert)
            insert addsList;
        return addsList;
    }
    public static List<Monitoring__c> createTestMonitoringData( Integer numToInsert, Boolean doInsert ){
        List<Monitoring__c> monitoring = new List<Monitoring__c>();
        for(Integer i=0; i< numToInsert; i++)
        {
            Monitoring__c mon = new Monitoring__c();
            mon.AnnouncedUnannouced__c	 = 'Announced';
            mon.JointlyInspectedWith__c='MDH';
            mon.ApprovalComments__c = 'Test Comments';
            
            monitoring.add( mon );
        }
        if( doInsert)
            insert monitoring;
        return monitoring;
    }
    public static List<ProviderRecordAnswer__c> createTestProviderRecordAnswerData( Integer numToInsert, Boolean doInsert ){
        List<ProviderRecordAnswer__c> providerrecordanswer = new List<ProviderRecordAnswer__c>();
        for(Integer i=0; i< numToInsert; i++)
        {
            ProviderRecordAnswer__c Prvra = new ProviderRecordAnswer__c();
            Prvra.Comments__c= 'test comments';

            providerrecordanswer.add( Prvra );
        }
        if( doInsert)
            insert providerrecordanswer;
        return providerrecordanswer;
    }
    public static List<ProviderRecordQuestion__c> createTestProviderRecordQuestionData( Integer numToInsert, Boolean doInsert ){
        List<ProviderRecordQuestion__c> providerrecordquestion = new List<ProviderRecordQuestion__c>();
        for(Integer i=0; i< numToInsert; i++)
        {
            ProviderRecordQuestion__c Prvrq = new ProviderRecordQuestion__c();
            Prvrq.Status__c= 'Draft';

            providerrecordquestion.add( Prvrq );
        }
        if( doInsert)
            insert providerrecordquestion;
        return providerrecordquestion;
    }
    public static List<ContactNotes__c> createTestContactNotesData( Integer numToInsert, Boolean doInsert ){
        List<ContactNotes__c> contactnotes = new List<ContactNotes__c>();
        for(Integer i=0; i< numToInsert; i++)
        {
            ContactNotes__c conte = new ContactNotes__c();
            conte.ContactName__c= 'test';

            contactnotes.add( conte );
        }
        if( doInsert)
            insert contactnotes;
        return contactnotes;
    }
    public static List<LicenseServices__c> createTestLicenseServicesData( Integer numToInsert, Boolean doInsert ){
        List<LicenseServices__c> licenseservices = new List<LicenseServices__c>();
        for(Integer i=0; i< numToInsert; i++)
        {
            LicenseServices__c lsrv = new LicenseServices__c();
            lsrv.ServiceDescription__c= 'test';

            licenseservices.add( lsrv );
        }
        if( doInsert)
            insert licenseservices;
        return licenseservices;
    }
    public static List<License__c> createTestLicenseData( Integer numToInsert, Boolean doInsert ){
        List<License__c> license = new List<License__c>();
        for(Integer i=0; i< numToInsert; i++)
        {
            License__c lcs = new License__c();
            lcs.ProgramType__c= '';

            license.add( lcs );
        }
        if( doInsert)
            insert license;
        return license;
    }
    public static List<Contract__c> createTestContractData( Integer numToInsert, Boolean doInsert ){
        List<Contract__c> contract = new List<Contract__c>();
        for(Integer i=0; i< numToInsert; i++)
        {
            Contract__c contr = new Contract__c();
            contr.ContractType__c= 'Grant';

            contract.add( contr );
        }
        if( doInsert)
            insert contract;
        return contract;
    }
    public static List<IRCRates__c> createTestIRCData( Integer numToInsert, Boolean doInsert ){
        List<IRCRates__c> ircrates = new List<IRCRates__c>();
        for(Integer i=0; i< numToInsert; i++)
        {
            IRCRates__c irc = new IRCRates__c();
            irc.RateCode__c= '';

            ircrates.add( irc );
        }
        if( doInsert)
            insert ircrates;
        return ircrates;
    }
    public static List<Actor__c> createTestActorData( Integer numToInsert, Boolean doInsert ){
        List<Actor__c> actor = new List<Actor__c>();
        for(Integer i=0; i< numToInsert; i++)
        {
            Actor__c actr = new Actor__c();
            actr.Role__c= 'Applicant';

            actor.add( actr );
        }
        if( doInsert)
            insert actor;
        return actor;
    }
    public static List<ActorDetails__c> createTestActorDetailsData( Integer numToInsert, Boolean doInsert ){
        List<ActorDetails__c> actordetails = new List<ActorDetails__c>();
        List<Application__c> appList = TestDataFactory.TestApplicationData(2,'Rejected');
        insert appList;
      
        for(Integer i=0; i< numToInsert; i++)
        {
            ActorDetails__c actdet = new ActorDetails__c();
            actdet.HomeAddess__c= 'Yes';
            actdet.Application__c=appList[0].Id;
            //actdet.Actor__c='';
            actordetails.add( actdet);
        }
        if( doInsert)
            insert actordetails;
        return actordetails;
    }
    public static List<HomeInformation__c> createTestHomeInformationData( Integer numToInsert, Boolean doInsert ){
        List<HomeInformation__c> homeinfo = new List<HomeInformation__c>();
        for(Integer i=0; i< numToInsert; i++)
        {
            HomeInformation__c hmeinfo = new HomeInformation__c();
            hmeinfo.SwimmingPool__c= '';

            homeinfo.add( hmeinfo );
        }
        if( doInsert)
            insert homeinfo;
        return homeinfo;
    }
    public static List<MeetingInfo__c> createTestMeetingInformationData( Integer numToInsert, Boolean doInsert ){
        List<MeetingInfo__c> meetinfo = new List<MeetingInfo__c>();
        Training__c trang = new Training__c(TrainingName__c='test');
        insert trang;

        for(Integer i=0; i< numToInsert; i++)
        {
            MeetingInfo__c meetnginfo = new MeetingInfo__c();
            meetnginfo.IntenttoAttend__c= 'Yes';
meetnginfo.Training__c=trang.Id;
            meetinfo.add( meetnginfo );
        }
        if( doInsert)
            insert meetinfo;
        return meetinfo;
    }
    public static List<HistoryTracking__c> createTestHistoryTrackingData( Integer numToInsert, Boolean doInsert ){
        List<HistoryTracking__c> hsttrck = new List<HistoryTracking__c>();
        for(Integer i=0; i< numToInsert; i++)
        {
            HistoryTracking__c hstck = new HistoryTracking__c();
            hstck.RichNewValue__c= '';

            hsttrck.add( hstck );
        }
        if( doInsert)
            insert hsttrck;
        return hsttrck;
    }
    public static List<Reconsideration__c> createTestReconsiderationData( Integer numToInsert, Boolean doInsert ){
        List<Reconsideration__c> recons = new List<Reconsideration__c>();
        for(Integer i=0; i< numToInsert; i++)
        {
            Reconsideration__c recon = new Reconsideration__c();
            recon.Gender__c	= 'Male';

            recons.add( recon );
        }
        if( doInsert)
            insert recons;
        return recons;
    }
    public static List<ReferenceValue__c> createTestReferenceValueData( Integer numToInsert, Boolean doInsert ){
        List<ReferenceValue__c> refe = new List<ReferenceValue__c>();
        for(Integer i=0; i< numToInsert; i++)
        {
            ReferenceValue__c ref = new ReferenceValue__c();
            ref.FieldType__c	= '';
            refe.add( ref );
        }
        if( doInsert)
            insert refe;
        return refe;
    }
    public static List<SessionAttendee__c> createTestSessionAttendeeData( Integer numToInsert, Boolean doInsert ){
       List<SessionAttendee__c> sessat = new List<SessionAttendee__c>();
       Training__c trang = new Training__c(TrainingName__c='test');
        insert trang;

        for(Integer i=0; i< numToInsert; i++)
        {
            SessionAttendee__c sesatd = new SessionAttendee__c();
            sesatd.CandidateAttend__c= '';
            sesatd.Training__c= trang.id;
            sessat.add( sesatd );
        }
        if( doInsert)
            insert sessat;
        return sessat;
    }
   
    
    public static List<Contact> createTestContacts( Integer numToInsert, Id accId, Boolean doInsert ){
        List<Contact> contacts = new List<Contact>();
        for(Integer i=0; i< numToInsert; i++)
        {
            Contact con = new Contact();
            con.FirstName = 'Test';
            con.LastName = 'Name' +i;
            con.accountId = accId;
            con.phone ='0123456789';
            con.email='test@test.com';
			   con.MailingCountry = 'country';		
	            con.MailingStreet = 'street';		
	            con.MailingCity = 'city';		
	            con.MailingState = 'state';		
                con.MailingPostalCode = '11235';
                con.AffiliationType__c	=''	;
                con.CICPA__c=''	;	
                con.CIRCC__c=''	;	
                con.Comments__c	='Test Comments';	
                con.Department='';	
                con.SCCPA__c='';	
                con.SCRCC__c='';		            
            contacts.add( con );
        }
        if( doInsert)
            insert contacts;
        return contacts;
    }
   
    
    public static List<Certification__c> createTestCertification( Integer numToInsert, Id certfId, Boolean doInsert ){
        List<Certification__c> certification = new List<Certification__c>();
        for(Integer i=0; i< numToInsert; i++)
        {
            Certification__c certf = new Certification__c();
            certf.CertificationType__c='General' ;
            certf.CertificationSubType__c = '';
            certf.BehavioralInterventions__c ='' ;
            certf.CertificateNumber__c ='0123456789'+i;
            certification.add( certf );
        }
        if( doInsert)
            insert certification;
        return certification;
    }
    
    public static List<Training__c> createTestTraining( Integer numToInsert, Id trangId, Boolean doInsert ){
        List<Training__c> training = new List<Training__c>();
        for(Integer i=0; i< numToInsert; i++)
        {
            Training__c trang = new Training__c();
            trang.TrainingName__c='Budget Preparation' ;
            training.add( trang );
        }
        if( doInsert)
            insert training;
        return training;
    }
    
    
    static public User getPartnerUser(Contact C, string OwnerID, string AccName)
    {
        id ProfileId;
        try {
            ProfileId = [select id from Profile where UserType = 'PowerPartner' limit 1].id;
        } catch (Exception e){
		   /* Errorlog__c log = new Errorlog__c();
     		log.Class_and_Method__c = 'TestDataFactory & getPartnerUser method';
     		log.Stack_Trace__c = e.getMessage();   
      		insert log;*/
		}
        
                
        User partnerUser = new User();
        partnerUser.ContactId = C.id;
        partnerUser.Username = C.email;
        partnerUser.LastName = C.LastName;
        partnerUser.Email = C.Email;
        partnerUser.Alias = C.LastName.substring(5);
        partnerUser.CommunityNickname = C.LastName.substring(5);
        partnerUser.TimeZoneSidKey = 'America/Los_Angeles';
        partnerUser.LocaleSidKey = 'en_US';
        partnerUser.EmailEncodingKey = 'ISO-8859-1';
        partnerUser.ProfileId = ProfileID;
        partnerUser.LanguageLocaleKey = 'en_US';
        partnerUser.CompanyName = AccName;
        return PartnerUser;
    }
            
}