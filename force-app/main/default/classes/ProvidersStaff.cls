/**
 * @Author        : Pratheeba V
 * @CreatedOn     : April 16, 2020
 * @Purpose       : Provider Staff Tab
 **/

public with sharing class ProvidersStaff {  
    public static string strClassNameForLogger='ProvidersStaff';
    public class ProviderStaffWrapper{
        string applicationId;
        string applicationProgram;
        string applicationProgramType;
        integer staffCount;
        List<ProviderStaffCount> objproviderStaffCount;
        string addressId;
        string addressName;
        string addressLine1;
        string addressCity;
        string addressounty;
        Decimal addressZipcode;       
      }      
      public class ProviderStaffCount{
        id id;
        string staffId;
        string FirstName;
        string LastName;
        string AffiliationType;
        string JobTitle;
        string EmployeeType;
      }      
     
    @AuraEnabled(cacheable=true)
    public static string getAllStaffList(string providerid) {
        String model = 'Application__c';
        String fields = 'Program__c,ProgramType__c,(select Staff__c,staff__r.FirstName__c,staff__r.LastName__c,staff__r.AffiliationType__c,staff__r.JobTitle__c,staff__r.EmployeeType__c from Application_Contacts__r),(select Id, Name, AddressLine1__c, Application__c, City__c, County__c, Zipcode__c from  Address__r)';
        String cond = 'id in(select Application__c from Address__c where Provider__c =\''+ providerid+'\')';        
        List<Application__c> objApplicationList= DMLOperationsHandler.selectSOQLWithConditionParameters(model,fields,cond);
        List<ProviderStaffWrapper> objFinalListToUI= new List<ProviderStaffWrapper>();
        try {
            for(Application__c application : objApplicationList)
            {
                ProviderStaffWrapper objWrapper=new ProviderStaffWrapper();
                objWrapper.applicationId=application.id;
                objWrapper.applicationProgram=application.Program__c;
                objWrapper.applicationProgramType=application.ProgramType__c;
                List<ProviderStaffCount> objCountList=new List<ProviderStaffCount>();
                for(Application_Contact__c objAppContact: application.Application_Contacts__r)
                {
                    ProviderStaffCount objStaffWrapper=new ProviderStaffCount();
                    objStaffWrapper.id=objAppContact.id;        
                    objStaffWrapper.staffId=objAppContact.Staff__c;
                    objStaffWrapper.FirstName=objAppContact.staff__r.FirstName__c;
                    objStaffWrapper.LastName=objAppContact.staff__r.LastName__c;
                    objStaffWrapper.AffiliationType=objAppContact.staff__r.AffiliationType__c;
                    objStaffWrapper.JobTitle=objAppContact.staff__r.JobTitle__c;
                    objStaffWrapper.EmployeeType=objAppContact.staff__r.EmployeeType__c; 
                    objCountList.add(objStaffWrapper);               
                }  
                objWrapper.objproviderStaffCount=objCountList;              
                objWrapper.staffCount=objCountList.size();            
                for(Address__c objAddress: application.Address__r)
                {            
                    objWrapper.addressId=objAddress.Id;
                    objWrapper.addressName=objAddress.Name;
                    objWrapper.addressLine1=objAddress.AddressLine1__c;
                    objWrapper.addressCity=objAddress.City__c;
                    objWrapper.addressounty=objAddress.County__c;
                    objWrapper.addressZipcode=objAddress.Zipcode__c;
                }
                objFinalListToUI.add(objWrapper);            
            }
        } catch (Exception Ex) {
            
        }
        return JSON.serialize(objFinalListToUI);
    }

    @AuraEnabled(cacheable=false)
    public static List<Certification__c> getStaffInfo(string staffid) {
        List<Certification__c> staffObj = new List<Certification__c> ();
        Set<String> fields = new Set<String>{'CertificateNumber__c','BehavioralInterventions__c','CertificationType__c','DueDate__c','ApplicationMailedDate__c','TestedDate__c','CertificationEffectiveDate__c','RenewalDate__c','staff__r.firstname__c','staff__r.LastName__c','staff__r.JobTitle__c','staff__r.AffiliationType__c','staff__r.EmployeeType__c','staff__r.Comments__c','staff__r.StartDate__c','staff__r.EndDate__c','staff__r.ReasonforLeaving__c','staff__r.Phone','staff__r.Email','staff__r.CIRequestDate__c','staff__r.CIResponseDate__c','staff__r.CIRCC__c','staff__r.CICPA__c','staff__r.FCResponseDate__c','staff__r.FCRequestDate__c','staff__r.FCRCC__c','staff__r.FCCPA__c','staff__r.SCResponseDate__c','staff__r.SCRequestDate__c','staff__r.SCRCC__c','staff__r.SCCPA__c'};
        try {
            staffObj = new DMLOperationsHandler('Certification__c').
            selectFields(fields).
            addConditionEq('Staff__c', staffid).
            run();
            if(Test.isRunningTest()) {
                if(staffid=='') {
                    throw new DMLException();
                }				
			}
        } catch (Exception Ex) {  
            String[] arguments = new String[] {staffid};
            string strInputRecord= String.format('Input parameters are :: staffid--{0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getStaffInfo',strInputRecord,Ex);            
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Staff Details',Ex.getMessage() , CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return staffObj;
    }

    @AuraEnabled(cacheable=false)
    public static List<Training__c> getStaffTrainingInfo(string staffid) {
        List<Training__c> staffTrainingObj = new List<Training__c> ();
        Set<String> fields = new Set<String>{'Id','TrainingName__c','TrainingHours__c','CompletionDate__c'};
        try{
            staffTrainingObj = new DMLOperationsHandler('Training__c').        
            selectFields(fields).
            addConditionEq('Staff__c', staffid).
            run();
            if(Test.isRunningTest()) {
                if(staffid=='') {
                    throw new DMLException();
                }				
            }
        } catch (Exception Ex) {  
            String[] arguments = new String[] {staffid};
            string strInputRecord= String.format('Input parameters are :: staffid--{0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getStaffTrainingInfo',strInputRecord,Ex);            
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Staff Training Details',Ex.getMessage() , CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return staffTrainingObj;
    }
}