/**
 * @Author        : Pratheeba V
 * @CreatedOn     : June 9 ,2020
 * @Purpose       : Test class for ProvidersContractReview class
 * @Update By     : Sindhu.v
 * @date          : june 11,2020
**/
@IsTest
private class ProvidersContractReview_Test {

    @IsTest static void testProvidersContractDashboardPositive(){
        Contract__c InsertobjNewContract=new Contract__c(ReviewComments__c='pending'); 
        insert InsertobjNewContract;
        Actor__c InsertobjNewActor=new Actor__c(); 
        insert InsertobjNewActor;
        Map<Id, ContentDocumentLink> contentDocumentIdsmap = new Map<Id, ContentDocumentLink>();
        Contract__c InsertobjNewCt=new Contract__c();
        Database.UpsertResult Upsertresults = Database.upsert(InsertobjNewCt);
        string strAppId= Upsertresults.getId();
        ContentVersion contentVersion = new ContentVersion(
        Title = 'Cardinality',
        PathOnClient = 'Cardinality.jpg',
        VersionData = Blob.valueOf('ProvidersContractReview_Test')
        );
        insert contentVersion;
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = strAppId;
        cdl.ContentDocumentId = documents[0].Id;
        insert cdl;
        String fileData = 'I am String to be converted in base64 encoding!';
        String fileDateBase64 = EncodingUtil.base64Encode(Blob.valueOf(fileData));

        String str = 'Contract__c';
        Profile prf = [Select Id, Name from Profile where Name= 'Supervisor'];
        List<User> usrList = TestDataFactory.createTestUsers( 2, prf.Id,true );
        //Casting String to sObject
        sObject obj = Schema.getGlobalDescribe().get(str).newSObject();
        obj.put('ReviewComments__c', 'test Comments');
        obj.put('Id', InsertobjNewContract.Id);
        obj.put('SupervisorComments__c', 'Caseworker Submitted');
        obj.put('ReviewStatus__c', 'Approved');
        obj.put('Caseworker__c',usrList[0].Id);        
        obj.put('ContractStatus__c','Submitted');
        obj.put('Status__c','Active');
        List<ContentVersion> cvList = TestDataFactory.createTestContentVersionData();
        insert cvList;
        Set<ContentVersion> cvrset = new Set<ContentVersion>();
        for(ContentVersion cv:cvList)
        {
            cvrset.add(cv);
        }
        ProvidersContractReview.getContractReviewDetails(InsertobjNewContract.Id);
        ProvidersContractReview.getAllSuperUsersList();
        ProvidersContractReview.saveSign(fileDateBase64,strAppId);
        ProvidersContractReview.getAssignedSuperUserTableDetails(InsertobjNewContract.Id);
        
        ProvidersContractReview.processingWrapper pWrap = new ProvidersContractReview.processingWrapper(InsertobjNewContract, cvrset);
       ProvidersContractReview.processingWrapper pWrapStr = new ProvidersContractReview.processingWrapper(InsertobjNewContract, cvrset);


 
       
        
    }
    @IsTest static void testprovidersMonitoringReviewNegative3()
    {
        Profile prf = [Select Id, Name from Profile where Name= 'Supervisor'];
        List<User> userList =  TestDataFactory.createTestUsers(1, prf.Id, true);
       Contract__c InsertobjMonitoringPeriod=new Contract__c(ContractStatus__c='Submitted', Status__c='Active',ReviewStatus__c='Approve',Caseworker__c=userList[0].id);
        insert InsertobjMonitoringPeriod;
        ProvidersContractReview.getAssignedSuperUserTableDetails(InsertobjMonitoringPeriod.Id);
        ProvidersContractReview.updateapplicationdetails(InsertobjMonitoringPeriod,userList[0].id,InsertobjMonitoringPeriod.Id);

    }
    @IsTest static void testprovidersMonitoringReviewNegative4()
    {
        Profile prf = [Select Id, Name from Profile where Name= 'Supervisor'];
        List<User> userList =  TestDataFactory.createTestUsers(1, prf.Id, true);
        // Application__c InsertobjApplication=new Application__c(Status__c = 'Approved',ApprovalComments__c='test',Caseworker__c=userList[0].id,ReviewStatus__c=''); 
        // insert InsertobjApplication;
       Contract__c InsertobjMonitoringPeriod=new Contract__c(ContractStatus__c='Submitted', Status__c='Active',ReviewStatus__c='Return to Caseworker',Caseworker__c=userList[0].id);
        insert InsertobjMonitoringPeriod;
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments('Submitting request for approval.');
        req.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        req.setObjectId(InsertobjMonitoringPeriod.Id);
        Approval.ProcessResult resu = Approval.process(req);
        System.assert(resu.isSuccess());
        System.assertEquals('Pending', resu.getInstanceStatus(),'Instance Status'+resu.getInstanceStatus());
        List<Id> newWorkItemIds = resu.getNewWorkitemIds();
       ProvidersContractReview.updateapplicationdetails(InsertobjMonitoringPeriod,'',newWorkItemIds.get(0));
       ProvidersContractReview.getAssignedSuperUserTableDetails(InsertobjMonitoringPeriod.Id);

    }
    @IsTest static void testprovidersMonitoringReviewNegative5()
    {
        Profile prf = [Select Id, Name from Profile where Name= 'Supervisor'];
        List<User> userList =  TestDataFactory.createTestUsers(1, prf.Id, true);
        // Application__c InsertobjApplication=new Application__c(Status__c = 'Approved',ApprovalComments__c='test',Caseworker__c=userList[0].id,ReviewStatus__c=''); 
        // insert InsertobjApplication;
       Contract__c InsertobjMonitoringPeriod=new Contract__c(ContractStatus__c='Submitted', Status__c='Active',ReviewStatus__c='Removed',Caseworker__c=userList[0].id);
        insert InsertobjMonitoringPeriod;
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments('Submitting request for approval.');
        req.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        req.setObjectId(InsertobjMonitoringPeriod.Id);
        Approval.ProcessResult resu = Approval.process(req);
        System.assert(resu.isSuccess());
        System.assertEquals('Pending', resu.getInstanceStatus(),'Instance Status'+resu.getInstanceStatus());
        List<Id> newWorkItemIds = resu.getNewWorkitemIds();
       ProvidersContractReview.updateapplicationdetails(InsertobjMonitoringPeriod,'',newWorkItemIds.get(0));
       ProvidersContractReview.getAssignedSuperUserTableDetails(InsertobjMonitoringPeriod.Id);

    }
    
}