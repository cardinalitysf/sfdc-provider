public class CaseService{
	/**
	 * @Author        : V.S.Marimuthu
	 * @CreatedOn     : March 17 ,2020
	 * @Purpose       : Responsible for all the actions in Case object.
	 **/
	public void createApplications(List<Case> objcase){
		try{
			Set<Id> resultIds = (new Map<Id,Case>(objcase)).keySet();
			List<Case> oCase= new DMLOperationsHandler('Case').
									selectFields('Account.ProviderType__c,Account.AgeGroup__c').
									addConditionIn('Id', resultIds).									
									run();
			Map<Id, Case> caseMap = new Map<Id, Case>(oCase);

			List<Application__c> lstApplications = new List<Application__c>();
			for (Case individualCase : objcase){
				List<String> lstProgramType = individualCase.ProgramType__c.split(',');
				for (string ProgramType : lstProgramType){
					Application__c objApplication = new Application__c();
					objApplication.Program__c = individualCase.Program__c;
					objApplication.RecordTypeId= Schema.Sobjecttype.Application__c.getRecordTypeInfosByName().get(caseMap.get(individualCase.Id).Account.ProviderType__c).getRecordTypeId();
					objApplication.AgeGroup__c= caseMap.get(individualCase.Id).Account.AgeGroup__c;
					objApplication.ProgramType__c = ProgramType;
					objApplication.OwnerId = UserInfo.getUserId();
					objApplication.Provider__c = individualCase.AccountId;
					objApplication.Referral__c = individualCase.Id;				
					objApplication.Status__c = 'Pending';
					lstApplications.add(objApplication);
				}
			}
			DMLOperationsHandler.updateOrInsertSOQLForList(lstApplications);
		} catch (Exception Ex){
			CustomAuraExceptionData.LogIntoObject('CaseService','createApplications',String.format('Input parameters are :: objcase -- {0}', objcase),Ex);   
		}
	}
	/**
	 * @Author        : V.S.Marimuthu
	 * @CreatedOn     : July 24 ,2020
	 * @Purpose       : Responsible for updating status in three objects(License,Deficiency and Application) if record type is complaint
	 **/
	public void updateStatusDependingOnSanctions(List<Case> cases){
	
		try {
			//Construct the sanction ID in order to retrive the sanction Record type Name(Revocation or Suspension or Limitation)
			//from sanctions object
			Set<Id> osanctionID = new Set<Id>(); 
			for (Case cse : cases) 
			{
    			osanctionID.add(cse.Sanctions__c);
			}
			//License List to update license object
			List<License__c> oLicense= new List<License__c>();

			//Deficiency List to update Deficiency object
			List<Deficiency__c> oDeficiency= new List<Deficiency__c>();

			//Map to store License ID as key and  based in LicensSanction Record type as value
			Map<Id,String> oLicenseApplictionMap= new Map<Id,String>();

			//Since we cant get the child objects record type name in new map below sunbquery is used
			//to take the record type name from sanctions object
			List<case> olstGetSanctionsRecordType = new DMLOperationsHandler('case').
						selectFields('Id,License__c,CAPNumber__c').
						addSubquery(
									DMLOperationsHandler.subquery('Sanctions__r').
									selectFields('RecordType.Name').
									addConditionIn('Id ', osanctionID)
									).
						addConditionIn('Id ', cases).
						run();
			// In the below for loop construct the license and deficincy list to update againt the
			// respective objects  
			for(Case oCase:olstGetSanctionsRecordType)
			{
				License__c license= new License__c();
				Deficiency__c deficiency= new Deficiency__c();
				license.id=oCase.License__c;
				deficiency.id=oCase.CAPNumber__c;
				for(Sanctions__c sanction : oCase.Sanctions__r){
					license.Status__c=sanction.RecordType.Name;	
					deficiency.Status__c=sanction.RecordType.Name;	
					oLicenseApplictionMap.put(oCase.License__c,sanction.RecordType.Name);				
				}
				//Add License list to update
				oLicense.add(license);	

				//Add Deficiency list to update
				oDeficiency.add(deficiency);				
			}

			//In order to update the application object since there is no direct relation between case and
			//application , below logic is to get the application id from license as the relation is
			//Case --> License --> Application
			List<License__c> oAppLicense= new DMLOperationsHandler('License__c').
											selectFields('Id,Application__c').
											addConditionIn('Id ', oLicense).
											run();   		
			List<Application__c> oApplication=new List<Application__c>();
			for (License__c license : oAppLicense) {
				Application__c app= new Application__c();
				app.Id=license.Application__c;
				app.Status__c=oLicenseApplictionMap.get(license.Id);

				//Add Application list to update
				oApplication.add(app);
			}

			//Declare a save point and if there is any error during update in any one of the object(because going to update 3 objects)
			//Then rollback the excption else commot the transaction
			Savepoint sp = Database.setSavepoint();
			try {
					//Update License
					if(oLicense.size()>0)
					{
						DMLOperationsHandler.updateOrInsertSOQLForList(oLicense);
					}

					//Update Application
					if(oApplication.size()>0)
					{
						DMLOperationsHandler.updateOrInsertSOQLForList(oApplication);
					}

					//Update Deficiency
					if(oDeficiency.size()>0)
					{
						DMLOperationsHandler.updateOrInsertSOQLForList(oDeficiency);
					}
				} catch (Exception Ex) {

					//If error roll back the transaction
					Database.RollBack(sp);
					CustomAuraExceptionData.LogIntoObject('CaseService','updateStatusDependingOnSanctions',String.format('Input parameters are :: cases -- {0}', cases),Ex);   

				}
			
		} catch (Exception Ex) {
			CustomAuraExceptionData.LogIntoObject('CaseService','updateStatusDependingOnSanctions',String.format('Input parameters are :: cases -- {0}', cases),Ex);   
		}
	}
}