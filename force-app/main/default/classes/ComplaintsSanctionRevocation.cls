/**
 * @Author        : Sundar Karuppalagu
 * @CreatedOn     : JULY 05 ,2020
 * @Purpose       : Sanctions Suspension Screen
 * @Updated By    : Nandhini
 * @UpdatedBy     : Sundar K -> July 24, 2020 -> cacheable=true removed from getRecordType method
 **/

public with sharing class ComplaintsSanctionRevocation {
    public static string strClassNameForLogger = 'ComplaintsSanctionRevocation';

    @AuraEnabled
    public static List<sObject> getSelectedSanctionData(String sanctionId) {
        List<sObject> revocationObj = new List<sObject>();
        String queryFields = 'Id, StartDate__c, Document__c, Delivery__c';

        try {
            revocationObj = new DMLOperationsHandler('Sanctions__c').
                            selectFields(queryFields).
                            addConditionEq('Id', sanctionId).
                            addConditionEq('RecordType.Name', 'Revocation').
                            run();
                            if(Test.isRunningTest())
                            {
                                if(sanctionId==null){
                                    throw new DMLException();
                                }
                            }
        } catch (Exception Ex) {
            //To Log into object
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getSelectedSanctionData', 'Input Parameter Revocation ID is :: ' + sanctionId, Ex);

            //To return back to the UI
            CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Revocation data failed to load', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }

        return revocationObj;
    }

    @AuraEnabled(cacheable=false)
    public static Id getRecordType(String name) {
        return CommonUtils.getRecordTypeIdbyName('Sanctions__c', name);
    }
}