/**
 * @Author        : Pratheeba.V
 * @CreatedOn     : May 11 ,2020
 * @Purpose       : Test Methods to unit test monitoringBoardinterview class
**/

@IsTest
private class monitoringBoardinterview_Test{
    @IsTest static void testMonitoringBoardinterviewPositive(){
        Application__c InsertobjNewApplication=new Application__c(Status__c = 'Pending'); 
        insert InsertobjNewApplication;
        ApplicationMonitoring.getMonitoringDetails(InsertobjNewApplication.Id);
        Monitoring__c InsertobjNewMonitoring=new Monitoring__c(ApplicationLicenseId__c = InsertobjNewApplication.Id); 
        insert InsertobjNewMonitoring;
        Address__c InsertobjNewAddress=new Address__c(); 
        insert InsertobjNewAddress; 
        Account InsertobjNewAccount=new Account(Name = 'Test Account'); 
        insert InsertobjNewAccount;

        ProviderRecordQuestion__c Prvrq = new ProviderRecordQuestion__c(Status__c = 'Pending',Monitoring__c=InsertobjNewMonitoring.Id);
        insert Prvrq;
        
        ProviderRecordAnswer__c prvans = new ProviderRecordAnswer__c();    
        insert prvans;

                 try
                {
                    monitoringBoardinterview.getRecordDetails(null,null,null);
                }catch(Exception Ex)
                {
                    
                }
                monitoringBoardinterview.getRecordDetails(InsertobjNewMonitoring.Id,InsertobjNewAddress.Id,'Id');
                try
                {
                    monitoringBoardinterview.getSiteId(null);
                }catch(Exception Ex)
                {
                    
                }
                monitoringBoardinterview.getSiteId(InsertobjNewAddress.Id);

                try
                {
                    monitoringBoardinterview.getQuestionsByType(null);   
                }catch(Exception Ex)
                {
                    
                }
                monitoringBoardinterview.getQuestionsByType('Web');  

                try
                {
                    monitoringBoardinterview.applicationStaff(null);
                }catch(Exception Ex)
                {
                    
                }
                monitoringBoardinterview.applicationStaff(InsertobjNewAccount.Id);

                try
                {
                    monitoringBoardinterview.getStaffFromApplicationId(null);
                    
                }catch(Exception Ex)
                {
                    
                }
                monitoringBoardinterview.getStaffFromApplicationId(InsertobjNewApplication.Id);
                
                monitoringBoardinterview.getDataForTable();                    
                
             
                monitoringBoardinterview.getRecordType('Board Interview');
                try
                {
                    monitoringBoardinterview.editRecordData(null);
                    
                }catch(Exception Ex)
                {
                    
                }
                monitoringBoardinterview.editRecordData('Id');        

                string str = '[{"Comar__c":"","Comments__c":"Test Comments","Profit__c":"","ProviderRecordQuestion__c":"'+Prvrq.Id+'"}]';
                string strUpdate = '[{"Id":"'+prvans.Id+'","Comments__c":"Test Comments","Profit__c":"","ProviderRecordQuestion__c":"'+Prvrq.Id+'"}]';
                       
                try
                {
                    monitoringBoardinterview.bulkAddRecordAns(null);
                    
                }catch(Exception Ex)
                {
                    
                }
                monitoringBoardinterview.bulkAddRecordAns(str);
                try
                {
                    monitoringBoardinterview.editRecordData(null);
                    
                }catch(Exception Ex)
                {
                   
                }
                monitoringBoardinterview.editRecordData('Id');
                try
                {
                    monitoringBoardinterview.bulkUpdateRecordAns(null);
                    
                }catch(Exception Ex)
                {
                }
                monitoringBoardinterview.bulkUpdateRecordAns(strUpdate);
               
    }
}