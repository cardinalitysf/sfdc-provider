/**
 * @Author        : Sindhu V
 * @CreatedOn     : June 10 ,2020
 * @Purpose       : Test Methods to unit test CommonDocuments  class
 **/
@isTest
private class CommonDocuments_Test{
@testSetup
static void setupTestData(){
    test.startTest();
    Application__c InsertobjNewLicenseInformation=new Application__c(ProgramType__c = 'XYZ');
        Database.UpsertResult Upsertresults = Database.upsert(InsertobjNewLicenseInformation);
        string strAppId= Upsertresults.getId();  

    ContentVersion contentVersion = new ContentVersion(
               Title = 'Test',
               PathOnClient = 'Test.jpg',
               VersionData = Blob.valueOf('ApplicationLicenseInformation_Test')
             );
                insert contentVersion;

    List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
              ContentDocumentLink cdl = New ContentDocumentLink();
              cdl.LinkedEntityId = strAppId;
              cdl.ContentDocumentId = documents[0].Id;
                insert cdl;  

    test.stopTest();
  }

  static testMethod void test_saveDescription_UseCase1(){
       Application__c oApp=[select id from Application__C];
      ContentVersion oDoc=[Select Id,ContentDocumentId from ContentVersion];   
      try{
             CommonDocuments.saveDescription(null,null,null,null);

         }catch(Exception Ex){
         }
        
      CommonDocuments.saveDescription(oDoc.ContentDocumentId,'Test','Test','Test');

  }

  static testMethod void test_getMapOfContentDocumentIDFromDocumentId_UseCase1(){
      try{
          CommonDocuments.getMapOfContentDocumentIDFromDocumentId('');
         }
      catch(Exception Ex){
      
         }
     List<ContentDocument> contents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
 	       // CommonDocuments.getMapOfContentDocumentIDFromDocumentId(contents[0].Id);
         }
  
  static testMethod void test_getUploadedAttachmentsUserDetails_UseCase1(){
    try{
        CommonDocuments.getUploadedAttachmentsUserDetails(null,null,null);
        }
    catch(Exception Ex){
      
        }
          Application__c oApp=[select id from Application__C];
 	        CommonDocuments.getUploadedAttachmentsUserDetails('oApp.Id','Application__c','Id');
       }
 
 static testMethod void test_getUploadedDocuments_UseCase1(){
     try{
        CommonDocuments.getUploadedDocuments(null);
        }
    catch(Exception Ex){
      
        }
    CommonDocuments.getUploadedDocuments('');
    
   }

}