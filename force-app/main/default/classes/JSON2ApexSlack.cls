global class JSON2ApexSlack {

	public String fulfillmentText;

	
	public static JSON2ApexSlack parse(String json) {
		return (JSON2ApexSlack) System.JSON.deserialize(json, JSON2ApexSlack.class);
	}
}