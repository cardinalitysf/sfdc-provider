/**
 * @Author        : G.sathishkumar
 * @CreatedOn     : April 16, 2020
 * @Purpose       : ProvidersContractDashboard.
 * @updatedBy     :
 * @updatedOn     :
 **/
public with sharing class ProvidersContractDashboard {
      public static string strClassNameForLogger='ProvidersContractDashboard';
    @AuraEnabled(cacheable=false)
    public static List<Contract__c> ProvidersContract(string newcontract) {
         List<sObject> contractPro= new List<sObject>();
        Set<String> fields = new Set<String>{'Id','Name','License__r.Name','License__r.ProgramType__c','TotalContractAmount__c','ContractStartDate__c','ContractEndDate__c', 'TotalContractBed__c','ContractStatus__c','Supervisor__c'};
             try{
             contractPro=  new DMLOperationsHandler('Contract__c').
                                 selectFields(fields).                                                    
                                 addConditionEq('Provider__c', newcontract).
                                 orderBy('Name', 'Desc').                                
                                 run();
               if(Test.isRunningTest())
               {
                if(newcontract==null){
                    throw new DMLException();
                }
            }
            
        }catch (Exception Ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'ProvidersContract',newcontract,Ex);
        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Contract Details Fetch',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));              
        }
         return contractPro;

    }

    @AuraEnabled(cacheable=true)
    public static List<Application__c> ProgramAddList(String ProviderId) {
         List<sObject> contractApp= new List<sObject>();
    Set<String> fields = new Set<String>{'Program__c','id','Name'};
    try{
     contractApp= new DMLOperationsHandler('Application__c').
                             selectFields(fields).                                                    
                             addConditionEq('Provider__c', ProviderId).
                             addConditionEq('Status__c', 'Approved').
                             run();
                    if(Test.isRunningTest()){
                        if(ProviderId==null){
                            throw new DMLException();
                        }
                    }
        }catch (Exception Ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'ProgramAddList',ProviderId,Ex);
        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Provider based Details Fetch',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));              
        }
       return contractApp;
    }

    //Sundar adding this to display contract no on contract tabs - This method used in ContractTabs.js file.
    @AuraEnabled(cacheable = true)
    public static List<SObject> getContractId (String contractId) {
        List<sObject> contractNoObj= new List<sObject>();
        String queryFields = 'Id, Name';
        try{
         contractNoObj = new DMLOperationsHandler('Contract__c').
                                    selectFields(queryFields).
                                    addConditionEq('Id', contractId).
                                    run();

                       if(Test.isRunningTest()){
                        if(contractId==null){
                            throw new DMLException();
                        }
                    }
        }catch (Exception Ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getContractId',contractId,Ex);
        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('contractId based Details Fetch',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));              
        }  
        return contractNoObj;
    }
}