public without sharing class referralProviderSearchController{
    @AuraEnabled
    public static list<SearchProviderWrapper> getAccounts(string providerId, string providerTaxId, string status, string jurisdiction){
        List<SearchProviderWrapper> lstSearchProviderWrapper = new List<SearchProviderWrapper>();
            
        
        //Query on Case, we are getting account field from case query.
        string query = 'select Program__c,ProgramType__c,AccountId,Account.Name, Account.ProviderId__c, Account.Email__c, Account.ProviderType__c,Account.Name__c, Account.Phone, Account.TaxId__c, Account.SON__c from case where AccountId <> null';
        if(providerId <> null)query += ' and Account.ProviderId__c like \'%'+providerId+'%\'';
        if(providerTaxId <> null)query += ' and Account.TaxId__c like \'%'+providerTaxId+'%\'';
        if(status <> 'All' && status <> null && status <> '')query += ' and Status =: status';
        //Assuming jurisdiction is list of countries and we are mapping with Account's BillingCountry.
        string countryname;
        if(jurisdiction <> null && jurisdiction <> ''){
            List<ReferenceValue__c> refvs = [select Id,RefValue__c from ReferenceValue__c where Id=: jurisdiction];
            
            if(refvs.size()>0){
                countryname = refvs[0].RefValue__c;
                query += ' and Account.BillingCity =: countryname ';
            }
        } 

        //query += 'ORDER BY Account.ProviderId__c Desc';
        query += ' ORDER BY Account.ProviderId__c Desc';
        
        set<Id> setAccountIds = new set<Id>();
        for(Case c: database.query(query)){
            lstSearchProviderWrapper.add(new SearchProviderWrapper(c.AccountId,c.Account.ProviderId__c,c.Account.Name,c.Account.Email__c,c.Account.Phone,c.Program__c,c.ProgramType__c,c.Account.TaxId__c,c.Account.SON__c));
        }       
        
        return lstSearchProviderWrapper;
    }
    public class SearchProviderWrapper{
        @AuraEnabled public string AccountId;
        @AuraEnabled public string ProviderId;
        @AuraEnabled public string ProviderName;
        @AuraEnabled public string Email;
        @AuraEnabled public string Phone;
        @AuraEnabled public string Program;
        @AuraEnabled public string ProgramType;
        @AuraEnabled public string TaxId;
        @AuraEnabled public string SON;
        public SearchProviderWrapper(string AccountId, string ProviderId,string ProviderName, string Email, string Phone, string Program, string ProgramType, string TaxId, string SON){
            this.AccountId = AccountId;
            this.ProviderId = ProviderId;
            this.ProviderName = ProviderName;
            this.Email = Email;
            this.Phone = Phone;
            this.Program = Program;
            this.ProgramType = ProgramType;
            this.TaxId = TaxId;
            this.SON = SON;
                   }
    }
    
    @AuraEnabled
    public static string getProviderStatusOptions(){
        SObject sobj = (SObject)Type.forName('Case').newInstance();
        return JSON.serialize(DMLOperationsHandler.fetchPickListValue(sobj ,'Status'));
    }
    
    @AuraEnabled
    public static string getProviderJuridictionOptions(){
        return JSON.serialize(DMLOperationsHandler.selectSOQLWithConditionParameters('ReferenceValue__c','Id,RefValue__c','RefKey__c = \'AK\' and Domain__c=\'County\''));
    }    
}