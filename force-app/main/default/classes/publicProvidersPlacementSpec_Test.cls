/**
 * @Author        : Pratheeba
 * @CreatedOn     : July 24 , 2020
 * @Purpose       : Test class for PublicProvidersPlacementSpec class
 **/

@isTest
private class publicProvidersPlacementSpec_Test {
    @isTest static void testPublicProvidersPlacementSpec_Positive() {
        Application__c InsertobjNewApplication=new Application__c(Status__c = 'Pending'); 
        insert InsertobjNewApplication;
        Account InsertobjNewAccount=new Account(Name = 'Test Account'); 
        insert InsertobjNewAccount;
        Address__c InsertobjNewAddress=new Address__c(); 
        insert InsertobjNewAddress;
        Reconsideration__c InsertobjNewReconsider=new Reconsideration__c(Gender__c = 'Male'); 
        insert InsertobjNewReconsider;
        try {
            publicProvidersPlacementSpec.InsertUpdateAppPlacement(InsertobjNewReconsider);
        } catch (Exception e) { }
        try {
            publicProvidersPlacementSpec.InsertUpdateAppPlacementAddr(InsertobjNewAddress);
        } catch (Exception e) { }
        try {
            publicProvidersPlacementSpec.getApplicationByProvider(InsertobjNewApplication.Id);
        } catch (Exception e) { }
        try {
            publicProvidersPlacementSpec.getDataByReconsiderationId(InsertobjNewReconsider.Id);
        } catch (Exception e) { }
        try {
            publicProvidersPlacementSpec.getProgramDetails(InsertobjNewApplication.Id);
        } catch (Exception e) { }
        try {
            publicProvidersPlacementSpec.fetchPlacementInformation(InsertobjNewReconsider.Id);
        } catch (Exception e) { }
        try {
            publicProvidersPlacementSpec.fetchAddressInformation(InsertobjNewAddress.Id);
        } catch (Exception e) { }        
        try{
            List<DMLOperationsHandler.FetchValueWrapper> objApplicationPlacementPickList = publicProvidersPlacementSpec.getPickListValues(InsertobjNewApplication,'Type__c');
        } catch (Exception e) {
        }
    }
    @isTest static void testPublicProvidersPlacementSpec_Exception() {
        try {
            publicProvidersPlacementSpec.fetchAddressInformation('');
        } catch (Exception e) { } 
        try {
            publicProvidersPlacementSpec.fetchPlacementInformation('');
        } catch (Exception e) { }
        try {
            publicProvidersPlacementSpec.getProgramDetails('');
        } catch (Exception e) { }
        try {
            publicProvidersPlacementSpec.getDataByReconsiderationId('');
        } catch (Exception e) { }
        try {
            publicProvidersPlacementSpec.getApplicationByProvider('');
        } catch (Exception e) { }
    }
}
