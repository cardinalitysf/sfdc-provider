/**
 * @Author        : K.Sundar
 * @CreatedOn     : APRIL 26 ,2020
 * @Purpose       : This method used to handle all screens review process
**/

public with sharing class ProvidersContractReview {
    //This method used to get Review Details for Contract Screen
    @AuraEnabled
	public static List<SObject> getContractReviewDetails(String contractId) {
        String queryFields = 'Id, ContractStatus__c, ReviewStatus__c, ReviewComments__c, SupervisorComments__c';
        List<sObject> reviewObj = new DMLOperationsHandler('Contract__c').
                                    selectFields(queryFields).
                                    addConditionEq('Id', contractId).
                                    run();
        return reviewObj;
    }

    //This method used to get All Users List for Supervisor
    @AuraEnabled(cacheable=true)
    public static List<SObject> getAllSuperUsersList() {
        String queryFields = 'Id, Name, isActive, Profile.Name, Availability__c, CaseLoad__c, ProfileId, Unit__c';
        List<sObject> superuserObj = new DMLOperationsHandler('User').
                                        selectFields(queryFields).
                                        addConditionEq('Profile.Name', 'Supervisor').
                                        run();
        return superuserObj;
    }

    @AuraEnabled
    public static void saveSign(String strSignElement,Id recId){

        // Create Salesforce File
        //Insert ContentVersion
        ContentVersion cVersion = new ContentVersion();
        cVersion.ContentLocation = 'S'; //S-Document is in Salesforce. E-Document is outside of Salesforce. L-Document is on a Social Netork.
        cVersion.PathOnClient = 'Signature-'+System.now() +'.png';//File name with extention
        cVersion.Origin = 'H';//C-Content Origin. H-Chatter Origin.
        //cVersion.OwnerId = attach.OwnerId;//Owner of the file
        cVersion.Title = 'Signature-'+System.now() +'.png';//Name of the file
        cVersion.VersionData = EncodingUtil.base64Decode(strSignElement);//File content
        cVersion.IsSignature__c=true;
        Insert cVersion;

        //After saved the Content Verison, get the ContentDocumentId
        Id conDocument = [SELECT ContentDocumentId  FROM ContentVersion WHERE Id =:cVersion.Id].ContentDocumentId;
        //Insert ContentDocumentLink
        ContentDocumentLink cDocLink = new ContentDocumentLink();
        cDocLink.ContentDocumentId = conDocument;//Add ContentDocumentId
        cDocLink.LinkedEntityId = recId;//Add attachment parentId
        cDocLink.ShareType = 'I';//V - Viewer permission. C - Collaborator permission. I - Inferred permission.
        cDocLink.Visibility = 'AllUsers';//AllUsers, InternalUsers, SharedUsers
        Insert cDocLink;      
    }

    //Preethi code reference
    @AuraEnabled
	public static string getAssignedSuperUserTableDetails(string contractId) {

        String model = 'ProcessInstance';
		String fields = 'Id, (SELECT Id, ProcessInstanceId, ActorId, Actor.Name, Actor.Profile.Name, CreatedDate FROM Workitems), (SELECT Id, StepStatus, ActorId, Actor.Name, Actor.Profile.Name, Comments, CreatedDate FROM Steps ORDER BY ID DESC)';
		String cond = 'TargetObjectId =\'' + contractId + '\'' + ' ORDER BY ID DESC';
        List<SObject> lstApp= DMLOperationsHandler.selectSOQLWithConditionParameters(model, fields, cond);

        Map<Id, ContentDocumentLink> contentDocumentIdsmap = new Map<Id, ContentDocumentLink>();

        for(ContentDocumentLink conDocu : [SELECT ContentDocumentId, LinkedEntityId,LinkedEntity.Name FROM ContentDocumentLink WHERE LinkedEntityId =:contractId  ORDER BY SystemModstamp DESC]){
            contentDocumentIdsmap.put(conDocu.ContentDocumentId, conDocu);
        }

        
        list<ContentVersion> contelist = [SELECT Id, OwnerId, CreatedById FROM ContentVersion WHERE ContentDocumentId In: contentDocumentIdsmap.keyset() AND IsSignature__c=true];
        Set<Id> filteredContOwner = new Set <Id>();
        Set<ContentVersion> filteredContList = new Set <ContentVersion>();
        for(ContentVersion clist : contelist){
            if(filteredContOwner.add(clist.OwnerId) == true)
            filteredContList.add(clist);
        }
        if(lstApp.size() > 0) {
            List<processingWrapper> appWrap=new List<processingWrapper>();
            for(SObject app : lstApp) {
                processingWrapper prdWrapper = new processingWrapper(app, filteredContList);
                appWrap.add(prdWrapper);
            }
            return JSON.serialize(appWrap);
        } else {
            List<processingWrapperString> appWrap = new List<processingWrapperString>();
            String fieldsm = 'Id, SubmittedDate__c, Caseworker__c, Caseworker__r.Name, Supervisor__c, Supervisor__r.Name, ReviewStatus__c, ReviewComments__c, Caseworker__r.Unit__c';
            List<SObject> contractApp = new DMLOperationsHandler('Contract__c').
                                    selectFields(fieldsm).
                                    addConditionEq('Id', contractId).     
                                    run();

            for(SObject app : contractApp) {
                if(contelist.size() > 0) {
                    for (contentversion conversion: contelist) {
                        processingWrapperString prdWrapper = new processingWrapperString(app, conversion.id);
                        appWrap.add(prdWrapper);
                    }
                } else {
                    processingWrapperString prdWrapper = new processingWrapperString(app, '');
                    appWrap.add(prdWrapper);
                }
            }
            return JSON.serialize(appWrap);
        }
    }

    //Preethi code reference
    public class processingWrapper {
        public SObject contractRecord {get;set;}
        public Set<ContentVersion> signatureUrl {get;set;}
        public processingWrapper(SObject app, Set<ContentVersion> url) {
            contractRecord = app;
            this.signatureUrl = url;
        }
    }

    //Preethi code reference
    public class processingWrapperString {
        public SObject contractRecord {get;set;}
        public String signatureUrl {get;set;}  
        public processingWrapperString(SObject app, String url) {
            contractRecord = app;
            this.signatureUrl = url;
        }
    }

    //Preethi code reference
    @AuraEnabled
    public static void updateapplicationdetails (sObject resultObj, String supervisorId, String workItemId) {
        //String status = (String) resultObj.get('ReviewStatus__c');
        if(supervisorId != '') {
            String Id = (String) resultObj.get('Id');
            String comments = (String) resultObj.get('ReviewComments__c');

            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments(comments);
            req1.setObjectId(Id);
            req1.setSubmitterId(UserInfo.getUserId());
            req1.setNextApproverIds(new Id[] {supervisorId});

            // Submit the approval request for the Opportunity
            Approval.ProcessResult result = Approval.process(req1);
        } else {
            String Id = (String) resultObj.get('Id');
            String caseworker = (String) resultObj.get('Caseworker__c');
            String comments = (String) resultObj.get('SupervisorComments__c');

            String status;
            if(resultObj.get('ReviewStatus__c') == 'Return to Caseworker') {
                status = 'Removed';  
            } else {
                status = (String) resultObj.get('ReviewStatus__c');
            }

            Approval.ProcessWorkitemRequest req1 = new Approval.ProcessWorkitemRequest();
            req1.setComments(comments);
            req1.setWorkitemId(workItemId);
            req1.setAction(status);
            req1.setNextApproverIds(new Id[] {caseworker});

            // Submit the approval request for the Opportunity
            Approval.ProcessResult result = Approval.process(req1);
        }
    }
}