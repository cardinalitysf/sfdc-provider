/**
     * @Author        : Sindhu Venkateswarlu
     * @CreatedOn     : May 4,2020
     * @Purpose       :Test Methods to unit test ProviderApplication class in ProviderApplication.cls
     **/

@isTest

public with sharing class ProviderApplication_Test {
   
    @isTest static void testgetDashboardDatas (){

        Application__c InsertobjNewApplication=new Application__c(Status__c = 'Pending'); 
        insert InsertobjNewApplication;
       // List<Account> acc=TestDataFactory.testAccountData();
        Account InsertobjNewAccount=new Account(Name = 'test Account'); 
        insert InsertobjNewAccount;
            TestDataFactory.createTestContacts(1,InsertobjNewAccount.id,true);
            providerApplication.getDashboardDatas(InsertobjNewAccount.Id,'applicationLists');
            providerApplication.getDashboardDatas(InsertobjNewAccount.Id,'capacityCount');
            providerApplication.getDashboardDatas(InsertobjNewAccount.Id,'staffCount');
         	providerApplication.getUserEmail();
         	providerApplication.getApplicationDet('abc');

    }
    
}