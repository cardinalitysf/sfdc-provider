/**
 * @Author        : M.Vijayaraj
 * @CreatedOn     : May 22, 2020
 * @Purpose       : This component for Public provider Header Class
 **/

 public with sharing class PublicProviderHeader {
    
    @AuraEnabled(cacheable=false)
    public static List<sObject> getCaseDetails(string casedetails) {
    List<Case> caseobj= new DMLOperationsHandler('Case').
    selectFields('Id,AccountId,CreatedDate,CaseNumber, Origin, Status, ReceivedDate__c,Account.Jurisdiction__c').
    addConditionEq('Id', casedetails).
    run();
    return caseobj;
    
    }

    @AuraEnabled(cacheable=true)
    public static List<DMLOperationsHandler.FetchValueWrapper> getPickListValues(sObject objInfo, string picklistFieldApi) {
        return DMLOperationsHandler.fetchPickListValue(objInfo,picklistFieldApi);
   
}

@AuraEnabled(cacheable=true)
public static List<DMLOperationsHandler.FetchValueWrapper> getPickListValuesJurisdiction(sObject objInfoJuris, string picklistFieldApiJuris) {
    return DMLOperationsHandler.fetchPickListValue(objInfoJuris,picklistFieldApiJuris);

}

@AuraEnabled
public static Id getIdAfterInsertSOQL(sObject objIdSobjecttoUpdateOrInsert) {
    return DMLOperationsHandler.getIdAfterInsertSOQL(objIdSobjecttoUpdateOrInsert);
}


}
