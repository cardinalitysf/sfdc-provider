/**
 * @Author        : Pratheeba.V
 * @CreatedOn     : May 11 ,2020
 * @Purpose       : Test Methods to unit test ProvidersStaff class
**/

@IsTest
private class ProvidersStaff_Test {
    public ProvidersStaff_Test() {}
    @IsTest static void testProviderStaffPositive(){
        Account InsertobjNewAccount=new Account(Name = 'Test Account'); 
        insert InsertobjNewAccount;
        Application__c InsertobjNewApplication=new Application__c(Status__c = 'Approved',Provider__c = InsertobjNewAccount.Id);
        insert InsertobjNewApplication;  
        Address__c InsertobjNewAddress=new Address__c(Provider__c=InsertobjNewAccount.Id,Application__c=InsertobjNewApplication.Id); 
        insert InsertobjNewAddress; 
        Contact InsertobjNewContact = new Contact(LastName = 'Test Staff');
        insert InsertobjNewContact;     
        Application_Contact__c InsertobjNewAppContact = new Application_Contact__c(Application__c = InsertobjNewApplication.Id,Staff__c=InsertobjNewContact.Id);
        insert InsertobjNewAppContact;
        TestDataFactory.createTestContacts(1,InsertobjNewAccount.id,true);
        try {
            ProvidersStaff.getAllStaffList(InsertobjNewAccount.Id); 
        } catch (Exception e) { }
        try {    
            ProvidersStaff.getStaffInfo(InsertobjNewAccount.id);
        } catch (Exception e) { }
        try {    
            ProvidersStaff.getStaffTrainingInfo(InsertobjNewAccount.id);
        } catch (Exception e) { }
    }

    @IsTest static void testProviderStaffNegative(){
        Account InsertobjNewAccount=new Account(Name = 'Test Account'); 
        insert InsertobjNewAccount;
        Application__c InsertobjNewApplication=new Application__c(Provider__c = InsertobjNewAccount.Id);
        insert InsertobjNewApplication;  
        Address__c InsertobjNewAddress=new Address__c(Provider__c=InsertobjNewAccount.Id,Application__c=InsertobjNewApplication.Id); 
        insert InsertobjNewAddress; 
        Contact InsertobjNewContact = new Contact(LastName = 'Test Staff');
        insert InsertobjNewContact;     
        Application_Contact__c InsertobjNewAppContact = new Application_Contact__c(Application__c = InsertobjNewApplication.Id,Staff__c=InsertobjNewContact.Id);
        insert InsertobjNewAppContact;
        try {    
            ProvidersStaff.getAllStaffList(InsertobjNewAccount.Id); 
        } catch (Exception e) { }         
    }

    @IsTest static void testProviderStaffException(){        
        try {    
            ProvidersStaff.getAllStaffList(''); 
        } catch (Exception e) { }
        try {    
            ProvidersStaff.getStaffInfo('');
        } catch (Exception e) { }
        try {    
            ProvidersStaff.getStaffTrainingInfo('');
        } catch (Exception e) { }        
    }
}