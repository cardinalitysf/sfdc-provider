/**
 * @Author        : Pratheeba.V
 * @CreatedOn     : June 8 ,2020
 * @Purpose       : Test Methods to unit test ProvidersContractDashboard class
**/

@IsTest
private class ProvidersContractDashboard_Test {
    @IsTest static void testProvidersContractDashboardPositive(){
        Contract__c InsertobjNewContract=new Contract__c(); 
        insert InsertobjNewContract;
        Account InsertobjNewAccount=new Account(Name = 'Test Account'); 
        insert InsertobjNewAccount;
       
  
           try {
              ProvidersContractDashboard.ProvidersContract(null);
               } catch (Exception Ex) {}
           ProvidersContractDashboard.ProvidersContract(InsertobjNewAccount.Id);

             try {
              ProvidersContractDashboard.ProgramAddList(null);
               } catch (Exception Ex) {}
                 ProvidersContractDashboard.ProgramAddList(InsertobjNewAccount.Id);

                  try {
             ProvidersContractDashboard.getContractId(null);
               } catch (Exception Ex) {}
                 ProvidersContractDashboard.getContractId(InsertobjNewContract.Id);

    }

    @IsTest static void testProvidersContractDashboardException(){
        ProvidersContractDashboard.ProvidersContract('');
        ProvidersContractDashboard.ProgramAddList('');
        ProvidersContractDashboard.getContractId('');
    }
}
