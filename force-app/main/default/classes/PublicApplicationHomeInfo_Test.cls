/**
 * @Author        : Sindhu Venkateswarlu
 * @CreatedOn     : May 8,2020
 * @Purpose       :Test Methods to unit test for PublicApplicationHomeInfo.cls
 * @Updated on    :June 20, 2020
 * @Updated by    :Janaswini Unit Test Updated methods with try catch
 **/

@isTest
public class PublicApplicationHomeInfo_Test {
  @isTest
  static void testgetStateDetails() {
    ReferenceValue__c InsertobjNewreference = new ReferenceValue__c();
    insert InsertobjNewreference;
    Account InsertobjNewAccount = new Account(Name = 'test Account');
    insert InsertobjNewAccount;
    HomeInformation__c hmeinfo = new HomeInformation__c();
    insert hmeinfo;
    Address__c acc = new Address__c(AddressLine1__c = '345 California St');
    insert acc;
    Application__c InsertobjNewApplication = new Application__c(
      Status__c = 'Pending'
    );
    insert InsertobjNewApplication;

    PublicApplicationHomeInfo.getStateDetails();
    try {
      Map<String, List<DMLOperationsHandler.FetchValueWrapper>> objgetProfitValues = PublicApplicationHomeInfo.getMultiplePicklistValues(
        InsertobjNewAccount.Id,
        'XXX'
      );
    } catch (Exception e) {
    }
    try {
      PublicApplicationHomeInfo.fetchHomeInformation(null);
    } catch (Exception ex) {
    }
    PublicApplicationHomeInfo.fetchHomeInformation(
      InsertobjNewAccount.Id
    );
    try {
      PublicApplicationHomeInfo.fetchAddressInformation(null, null);
    } catch (Exception ex) {
    }

    PublicApplicationHomeInfo.fetchAddressInformation(
      acc.Id,
      InsertobjNewApplication.Id
    );
  }

  @isTest
  static void testPublicApplicationHomeInfoException() {
    PublicApplicationHomeInfo.fetchHomeInformation('');
    PublicApplicationHomeInfo.fetchAddressInformation('', '');
  }
}
