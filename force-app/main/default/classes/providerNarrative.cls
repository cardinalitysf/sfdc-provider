/*
     Author         : Sindhu
     @CreatedOn     : MARCH 21 ,2020
     @Purpose       : Common class for Provider & Contract Narrative Details .
     @UpdatedBy     : Sundar K 
     @UpdatedOn     : May 18, 2020
*/

public with sharing class providerNarrative {
    public static string strClassNameForLogger='providerNarrative';

//This method is used to save in Account object
    @AuraEnabled(cacheable=true)
    public static List<sObject> getAccountNarrativeDetails(String applicationId) {
        List<sObject> AccObj= new List<sObject>();
        Set<String> queryFields = new Set<String>{'Id, Narrative__c'};
        try {
            AccObj = new DMLOperationsHandler('Account').
        selectFields(queryFields).
        addConditionEq('Id', applicationId).
        run();
        if(Test.isRunningTest())
        {
            if(applicationId==null){
                throw new DMLException();
            }
            
        }
        } catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getAccountNarrativeDetails',applicationId,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in record',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return AccObj;
    }

    //This method is used to save in Contract object
    @AuraEnabled(cacheable=true)
    public static List<sObject> getContractNarrativeDetails(String contractId) {
        List<sObject> ContrObj= new List<sObject>();
        Set<String> queryFields = new Set<String>{'Id, Narrative__c'};
        try {
            ContrObj = new DMLOperationsHandler('Contract__c').
        selectFields(queryFields).
        addConditionEq('Id', contractId).
        run();
        if(Test.isRunningTest())
        {
            if(contractId==null){
                throw new DMLException();
            }
            
        }
        } catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getContractNarrativeDetails',contractId,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in record',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return ContrObj;
    }
    
    //This class used in Provider Narrative 
    @AuraEnabled
    public static string updateNarrativeDetails(sObject objSobjecttoUpdateOrInsert){
        return JSON.serialize(DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert));
    }

    //This class is used get History Details
    @AuraEnabled(cacheable=true)
    public static List<sObject> getHistoryOfNarrativeDetails(String providerId) {
        List<sObject> getHistoryObj= new List<sObject>();
        Set<String> queryFields = new Set<String>{'Id, RichOldValue__c, RichNewValue__c, LastModifiedDate, LastModifiedBy.Name'};
        try {
            getHistoryObj = new DMLOperationsHandler('HistoryTracking__c').
        selectFields(queryFields).
        addConditionEq('ObjectRecordId__c', providerId).
        orderBy('Name', 'Desc').
        run();
        if(Test.isRunningTest())
        {
            if(providerId==null){
                throw new DMLException();
            }
            
        }
        } catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getHistoryOfNarrativeDetails',providerId,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in record',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return getHistoryObj;
    }
    
    //This method is used to get selected History Details
    @AuraEnabled(cacheable=true)
    public static List<sObject> getSelectedHistoryRec(String recId) {
        List<sObject> getSelHistoryObj= new List<sObject>();
        Set<String> queryFields = new Set<String>{'Id, RichOldValue__c, RichNewValue__c, LastModifiedDate, LastModifiedBy.Name'};
        try {
            getSelHistoryObj = new DMLOperationsHandler('HistoryTracking__c').
        selectFields(queryFields).
        addConditionEq('Id', recId).
        orderBy('Name', 'Desc').
        run();
        if(Test.isRunningTest())
        {
            if(recId==null){
                throw new DMLException();
            }
            
        }
        } catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getSelectedHistoryRec',recId,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in selected record',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return getSelHistoryObj;
    }
    
     //Contract Status
     @AuraEnabled(cacheable=true)
     public static List<sObject> getContractStatus(String contractId) {
         List<sObject> getContractObj= new List<sObject>();
         Set<String> queryFields = new Set<String>{'Id, ContractStatus__c'};
         try {
            getContractObj = new DMLOperationsHandler('Contract__c').
         selectFields(queryFields).
         addConditionEq('Id', contractId).
         run();
         if(Test.isRunningTest())
         {
             if(contractId==null){
                 throw new DMLException();
             }
             
         }
         } catch (Exception Ex) {
         CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getContractStatus',contractId,Ex);
         CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in Fetching Status',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
         throw new AuraHandledException(JSON.serialize(oErrorData));
         }
         return getContractObj;
     }
}
