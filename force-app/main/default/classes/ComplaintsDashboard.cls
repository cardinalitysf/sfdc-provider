/**
     * @Author        : Jayachandran s
     * @CreatedOn     : June 29,2020
     * @Purpose       : Responsible for fetch or insert the ComplaintsDashboard Case Object(Activity Sobj).
**/
public with sharing class ComplaintsDashboard {
    public static string strClassNameForLogger='ComplaintsDashboard';
//PrivateProviders Data Fetch Method.
    @AuraEnabled(cacheable=true)
    public static List<Account> getComplaintsPrivateList(String searchKey, String type, String user) {
        String model = 'Case';
        String fields = 'Id, CaseNumber,License__r.Application__r.Id, Account.Name,Address__r.AddressLine1__c,Address__r.City__c, Account.ProviderType__c,Account.ProviderId__c,Account.OwnerId, ComplaintReceivedDate__c, Status';
        searchKey = '%' + searchKey + '%';
        List<String> lstStatus = new List<String>();
        lstStatus = statusForProviders(type, user);
       
        List<sObject> privateList = new List<sObject>();
        try {
            privateList =  new DMLOperationsHandler(model).
                                    selectFields(fields).
                                    addConditionEq('Status', lstStatus).
                                    addConditionEq('Account.ProviderType__c', 'Private').
                                    addConditionEq('RecordType.Name', 'Complaints').
                                    addConditionLike('CaseNumber', searchKey).
                                    orderBy('LastModifiedDate', 'DESC').
                                    run();
                                    if(Test.isRunningTest()) {
                                        if(searchKey==null || type==null || user==null) {
                                            throw new DMLException();
                                        }
                                    } 
        } catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getComplaintsPrivateList',searchKey+ 'searchKeyId' +type + 'typeofprovider' + user + 'userProfile' ,Ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Sorry,Failed to load PrivateComplaints list details ,Please Try Again..!',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return privateList; 
    }
//PublicProviders Data Fetch Method.
    @AuraEnabled(cacheable=true)
    public static String getPublicComplaintList(String searchKey, String type, String user) {
        String model = 'Case';
        String fields = 'Id, CaseNumber,License__r.Application__r.Id, Account.ProviderType__c,Account.ProviderId__c,Account.OwnerId, Status, ComplaintReceivedDate__c, AccountId';
        String actorFields = 'Id, Role__c, Contact__r.LastName, Provider__c, Contact__r.SSN__c';
        searchKey = '%' + searchKey + '%';
        List<String> lstStatus =new List<string>();
        lstStatus  = statusForProviders(type, user);
      
        List<Case> publicList = new List<Case>();
        List<Actor__c> applicantList = new List<Actor__c>();
        try {
            publicList = new DMLOperationsHandler(model).
                                    selectFields(fields).
                                    addConditionIn('Status', lstStatus).
                                    addConditionEq('Account.ProviderType__c', 'Public').
                                    addConditionEq('RecordType.Name', 'Complaints').
                                    addConditionLike('CaseNumber',searchKey).
                                    orderBy('LastModifiedDate', 'DESC').
                                    run();
                                    if(Test.isRunningTest()) {
                                        if(searchKey==null || type==null || user==null) {
                                            throw new DMLException();
                                        }
                                    } 
            Set<Id> allIds = new Set<Id>();
            for (Case publist : publicList) {
                allIds.add(publist.AccountId);
            }
            applicantList = new DMLOperationsHandler('Actor__c').
                                    selectFields(actorFields).
                                    addConditionIn('Provider__c', allIds).
                                    run();
        } catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getPublicComplaintList',searchKey+ 'searchKeyId' +type + 'typeofprovider' + user + 'userProfile' ,Ex);
             CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Sorry,Failed to load PublicComplaints list details ,Please Try Again..!',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
             throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        processingWrapper prdWrapper = new processingWrapper(publicList, applicantList);        
        return JSON.serialize(prdWrapper);
    }
//Complaints Counts Class With Search Key And Type Public or Private And User.
    @AuraEnabled(cacheable=true)
    public static List<WrapperCasecount> getComplaintsCount(String searchKey, String type, String user) {
        List<WrapperCasecount> lstWrapper = new List<WrapperCasecount>();
        String key = '%' + searchKey + '%';
        String recordType = 'Complaints';
        Integer countPending = 0;
        Integer countApproved = 0;
        Integer countRejected = 0;

        Integer countSubmittedToCaseworker = 0;
        Integer countSubmittedToSupervisor = 0;
        Integer countAwaitingConfirmation = 0; 
        Integer countAccepted = 0;
        Integer countDisputed = 0;
        List<AggregateResult> results =  new DMLOperationsHandler('Case'). 
                                 selectFields('Status').
                                 addConditionLike('CaseNumber', key).
                                 addConditionEq('Account.ProviderType__c', type).
                                 addConditionEq('RecordType.Name', recordType).
                                 count('Id').   
                                 groupBy('Status').
                                 run();
            for(AggregateResult result : results) {
                if(result.get('Status') == 'Draft' || result.get('Status') == 'Pending')
                countPending = (Integer) result.get('expr0');
                else if(result.get('Status') == 'Approved')
                countApproved = (Integer) result.get('expr0');
                else if(result.get('Status') == 'Rejected')
                countRejected = (Integer) result.get('expr0');

                else if(result.get('Status') == 'Submitted to Caseworker')
                countSubmittedToCaseworker = (Integer) result.get('expr0');
                else if(result.get('Status') == 'Submitted to Supervisor')
                countSubmittedToSupervisor = (Integer) result.get('expr0');
                else if(result.get('Status') == 'Awaiting Confirmation')
                countAwaitingConfirmation = (Integer) result.get('expr0');
                else if(result.get('Status') == 'Accepted')
                countAccepted = (Integer) result.get('expr0');
                else if(result.get('Status') == 'Disputed')
                countDisputed = (Integer) result.get('expr0');
            }
        lstWrapper.add(new Wrappercasecount(countPending, countApproved, countRejected, countSubmittedToCaseworker, countSubmittedToSupervisor, countAwaitingConfirmation, countAccepted, countDisputed, user)); 
        return lstWrapper;
    }

// Wrapper Class For Count Of All Cards Datas.
    public class Wrappercasecount {
        @AuraEnabled public Integer approved;
        @AuraEnabled public Integer rejected;
        @AuraEnabled public Integer pending;
        @AuraEnabled public Integer total; 

        public Wrappercasecount(Integer countPending, Integer countApproved, Integer countRejected,  Integer countSubmittedToCaseworker,Integer countSubmittedToSupervisor,Integer countAwaitingConfirmation,Integer countAccepted,Integer countDisputed, String user) {
        this.approved = countApproved;
        this.rejected = countRejected;
        switch on user {
            when 'System Administrator','Intake'{
                this.pending = countPending + countSubmittedToSupervisor + countSubmittedToCaseworker + countAwaitingConfirmation + countAccepted + countDisputed;
                this.total = countPending + countApproved + countRejected + countSubmittedToCaseworker + countSubmittedToSupervisor + countAwaitingConfirmation + countAccepted + countDisputed;
            }when 'Caseworker'{
                this.pending =  countSubmittedToSupervisor + countSubmittedToCaseworker + countAwaitingConfirmation + countAccepted + countDisputed;
                this.total = countApproved + countRejected + countSubmittedToCaseworker + countSubmittedToSupervisor + countAwaitingConfirmation +  countAccepted + countDisputed;
            }when 'Supervisor'{
                this.pending = countPending + countSubmittedToSupervisor + countSubmittedToCaseworker + countAwaitingConfirmation + countAccepted + countDisputed;
                this.total = countPending + countApproved + countRejected + countSubmittedToCaseworker + countSubmittedToSupervisor + countAwaitingConfirmation +  countAccepted + countDisputed;
            }
        }
    }
    }
//lstStatus Common For Public And Private Data.
    Public static List<string> statusForProviders(string type,string user){
        List<string> lstStatus = new List<string>();
         switch on type {
            when 'total' {
               switch on user {
                   when 'Intake','System Administrator'
                   {
                     lstStatus=new List<string>{'Pending','Draft','Submitted to Caseworker','Submitted to Supervisor','Awaiting Confirmation','Accepted','Disputed','Approved','Rejected'};
                   }
                   when 'Caseworker'
                   {
                    lstStatus=new List<string>{'Submitted to Caseworker','Submitted to Supervisor','Awaiting Confirmation','Accepted','Disputed','Approved','Rejected'};
                     
                   }
                   when 'Supervisor'
                   {
                    lstStatus=new List<string>{'Pending','Draft','Submitted to Caseworker','Submitted to Supervisor','Awaiting Confirmation','Accepted','Disputed','Approved','Rejected'};
                   }
               }
            }
            when 'pending' {
               switch on user {
                   when 'Intake','System Administrator'
                   {
                     lstStatus=new List<string>{'Pending','Draft','Submitted to Caseworker','Submitted to Supervisor','Awaiting Confirmation','Accepted','Disputed'};
                   }
                   when 'Caseworker'
                   {
                    lstStatus=new List<string>{'Submitted to Caseworker','Submitted to Supervisor','Awaiting Confirmation','Accepted','Disputed'};
                     
                   }
                   when 'Supervisor'
                   {
                    lstStatus=new List<string>{'Pending','Draft','Submitted to Caseworker','Submitted to Supervisor','Awaiting Confirmation','Accepted','Disputed'};
                   }
               }
            }
            when else {
              lstStatus.add(type);
            }
         }
         return lstStatus;
    }
//Wrapper For Complaints And Actor Applicant Name Fetch.
    public class processingWrapper {
        public List<Case> Complaints {get;set;}
        public List<Actor__c> Actor {get;set;}
        public processingWrapper(List<Case> app, List<Actor__c> app2) {
            Complaints = app;
            Actor = app2;
        }
    }
}
