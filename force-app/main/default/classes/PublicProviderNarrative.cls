public with sharing class PublicProviderNarrative {

    //This class used in Provider Narrative & Application Narrative
    @AuraEnabled(cacheable=true)
     public static List<Case> getNarrativeDetails(String applicationId) {
        List<Case> cases = new List<Case> ();
         try {
            cases = new DMLOperationsHandler('Case').
            selectFields('Id, Description').
            addConditionEq('Id', applicationId).
            run();
            if(Test.isRunningTest())
			{
                if(applicationId==null){
                    throw new DMLException();
                }
			}
            }
         catch (Exception ex) {
            String inputParametergetOfNarrativeDetails='Inputs parameter get values of Narrative Details ::'+applicationId;
            CustomAuraExceptionData.LogIntoObject('PublicProviderNarrative','getNarrativeDetails',inputParametergetOfNarrativeDetails,ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Narrative Details',ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
            }
        return cases;
    }

    //This class used in Provider Narrative & Application Narrative
    @AuraEnabled
    public static string updateNarrativeDetails(sObject objSobjecttoUpdateOrInsert){
        return JSON.serialize(DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert));
    }




    //This class used in Application Narrative
    @AuraEnabled(cacheable=true)
        public static List<HistoryTracking__c> getHistoryOfNarrativeDetails(String applicationId) {
            List<HistoryTracking__c> histtrack = new List<HistoryTracking__c>();
            try {
            histtrack = new DMLOperationsHandler('HistoryTracking__c').
            selectFields('Id, RichOldValue__c, RichNewValue__c, LastModifiedDate, LastModifiedBy.Name').
            addConditionEq('ObjectRecordId__c', applicationId).
            orderBy('Id','DESC').
            run();
            if(Test.isRunningTest())
			{
                if(applicationId==null){
                    throw new DMLException();
                }
			}
            }
            catch (Exception ex) {
                String inputParametergethistoryOfNarrativeDetails='Inputs parameter get history of Narrative Details ::'+applicationId;
                CustomAuraExceptionData.LogIntoObject('PublicProviderNarrative','getHistoryOfNarrativeDetails',inputParametergethistoryOfNarrativeDetails,ex);
                CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('History of Narrative Details',ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
                throw new AuraHandledException(JSON.serialize(oErrorData));
            }
            return histtrack;
    }

   

    //This class used in Application Narrative
    @AuraEnabled(cacheable=true)
        public static List<HistoryTracking__c> getSelectedHistoryRec(String recId) {
            List<HistoryTracking__c> histtrack = new List<HistoryTracking__c>();
            try {
                histtrack = new DMLOperationsHandler('HistoryTracking__c').
                selectFields('Id, RichOldValue__c, RichNewValue__c, LastModifiedDate, LastModifiedBy.Name').
                addConditionEq('Id', recId).
                orderBy('Id','DESC').
                run();
                if(Test.isRunningTest())
		    	{
                if(recId==null){
                    throw new DMLException();
                 }   
		    	}
                }
            catch (Exception ex) {
                String inputParametergetselectedhistoryRec='Inputs parameter get selected history of Narrative Details ::'+recId;
                CustomAuraExceptionData.LogIntoObject('PublicProviderNarrative','getSelectedHistoryRec',inputParametergetselectedhistoryRec,ex);
                CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get selected history of Narrative Details',ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
                throw new AuraHandledException(JSON.serialize(oErrorData));
            }
            return histtrack;
    }

    //This class used in Application Narrative
    @AuraEnabled(cacheable=true)
        public static List<Case> getPublicProviderStatus(String applicationId) {
            List<Case> cases = new List<Case>();
            try {
                cases = new DMLOperationsHandler('Case').
                selectFields('Id, Status').
                addConditionEq('Id', applicationId).
                run();
                if(Test.isRunningTest())
		    	{
                if(applicationId==null){
                    throw new DMLException();
                 }   
		    	}
            }
            catch (Exception ex) {
                String inputParametergetpublicproviderstatus='Inputs parameter get public provider status ::'+applicationId;
                CustomAuraExceptionData.LogIntoObject('PublicProviderNarrative','getPublicProviderStatus',inputParametergetpublicproviderstatus,ex);
                CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Status of public provider',ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
                throw new AuraHandledException(JSON.serialize(oErrorData));
            }
            return cases;
    }


}

