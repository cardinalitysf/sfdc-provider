 /**
 * @Author        : Saranraj
 * @CreatedOn     : July 6 ,2020
 * @Purpose       : Proivider Sanctions complaints
 **/
public with sharing class ComplaintsProviderSanctions {
    public static string strApexDebLogClassName ='ComplaintsProviderSanctions';
 
    @AuraEnabled(cacheable=true)
    public static List<Sanctions__c> getSanctionsData(String providerId) {
        List<Sanctions__c> getSanctionsTableData= new List<Sanctions__c>();
               try {
                     getSanctionsTableData = new DMLOperationsHandler('Sanctions__c').
                    selectFields('Id,Complaint__r.CaseNumber, Complaint__r.CAPNumber__r.Name, Complaint__r.License__r.ProgramType__c, RecordType.Name, Delivery__c, Document__c, EndDate__c, StartDate__c, Numberofyouthserved__c, MinAge__c, MaxAge__c, PopulationEffectiveDate__c,Gender__c,PopulationEndDate__c,PopulationLimitationReason__c,ProgramEffectiveDate__c,ProgramEndDate__c,ProgramLimitationReason__c,ProviderAssignedService__c,YouthEffectiveDate__c, YouthEndDate__c, YouthLimitationReason__c').
                    addConditionEq('Provider__c', providerId).
                    run();
                    if(Test.isRunningTest())
                        {
                            if(providerId==null){
                                throw new DMLException();
                            }
                            
                        }
                } catch(Exception e ) {
                    CustomAuraExceptionData.LogIntoObject(strApexDebLogClassName,'getSanctionsData', 'providerID: ' + providerId, e);
                    CustomAuraExceptionData errorData=new CustomAuraExceptionData('Sanctions Data',e.getMessage() , CustomAuraExceptionData.type.Informational.name());
                    throw new AuraHandledException(JSON.serialize(errorData));
                }

                return getSanctionsTableData;
            }
   
}
