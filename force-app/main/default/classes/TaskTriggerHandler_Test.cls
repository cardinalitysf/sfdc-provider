/**
 * @Author        : Sindhu V
 * @CreatedOn     : June 11 ,2020
 * @Purpose       : Test Methods to unit test TaskTriggerHandler  class
 **/
@isTest
private class TaskTriggerHandler_Test{
  @testSetup
  static void setupTestData(){
    test.startTest();
     Monitoring__c monitoring_Obj = new Monitoring__c(Status__c = 'Completed');
     Insert monitoring_Obj;
   
      Task task_Obj = new Task(Status = 'Open', Priority = 'High', IsReminderSet = false, IsRecurrence = false,Monitoring__c=monitoring_Obj.id,Status__c='Open');
      Insert task_Obj;
      task_Obj.Status__c='Incomplete';  
      update task_Obj;
      task_Obj.Status__c='Completed';  
      update task_Obj;
     
      Task task_Obj1 = new Task(Status = 'Open', Priority = 'High', IsReminderSet = false, IsRecurrence = false,Monitoring__c=monitoring_Obj.id,Status__c='Incomplete');
      Insert task_Obj1;
     
      task_Obj1.Status__c='Incomplete';  
      update task_Obj1;
      task_Obj1.Status__c='Completed';  
      update task_Obj1;
     
       Task task_Obj2 = new Task(Status = 'Open', Priority = 'High', IsReminderSet = false, IsRecurrence = false,Status__c='Incomplete');
      Insert task_Obj2;
     
      task_Obj2.Status__c='Incomplete';  
      update task_Obj2;
   
     
     
    test.stopTest();
  }
  static testMethod void test_isDisabled_UseCase1(){
    List<Task> task_Obj  =  [SELECT IsReminderSet,IsRecurrence from Task];
    System.assertEquals(true,task_Obj.size()>0);
    TaskTriggerHandler obj01 = new TaskTriggerHandler();
    obj01.isDisabled();
  }
  static testMethod void test_beforeInsert_UseCase1(){
    List<Task> task_Obj  =  [SELECT IsReminderSet,IsRecurrence from Task];
    System.assertEquals(true,task_Obj.size()>0);
    TaskTriggerHandler obj01 = new TaskTriggerHandler();
    obj01.beforeInsert(new List<Sobject>());
  }
  static testMethod void test_beforeUpdate_UseCase1(){
    List<Task> task_Obj  =  [SELECT IsReminderSet,IsRecurrence from Task];
    System.assertEquals(true,task_Obj.size()>0);
    TaskTriggerHandler obj01 = new TaskTriggerHandler();
    obj01.beforeUpdate(new Map<id,sObject>(),new Map<id,sObject>());
  }
  static testMethod void test_beforeDelete_UseCase1(){
    List<Task> task_Obj  =  [SELECT IsReminderSet,IsRecurrence from Task];
    System.assertEquals(true,task_Obj.size()>0);
    TaskTriggerHandler obj01 = new TaskTriggerHandler();
    obj01.beforeDelete(new Map<id,sObject>());
  }
  static testMethod void test_afterInsert_UseCase1(){
    List<Task> task_Obj  =  [SELECT IsReminderSet,IsRecurrence from Task];
    System.assertEquals(true,task_Obj.size()>0);
    TaskTriggerHandler obj01 = new TaskTriggerHandler();
    obj01.afterInsert(new List<Sobject>());
  }
  static testMethod void test_afterUpdate_UseCase1(){
    List<Task> task_Obj  =  [SELECT IsReminderSet,IsRecurrence from Task];
    System.assertEquals(true,task_Obj.size()>0);
    TaskTriggerHandler obj01 = new TaskTriggerHandler();
    obj01.afterUpdate(new Map<Id,sObject>(),new Map<Id,sObject>());
  }
  static testMethod void test_afterDelete_UseCase1(){
    List<Task> task_Obj  =  [SELECT IsReminderSet,IsRecurrence from Task];
    System.assertEquals(true,task_Obj.size()>0);
    TaskTriggerHandler obj01 = new TaskTriggerHandler();
    obj01.afterDelete(new Map<id,sObject>());
  }
  static testMethod void test_afterUndelete_UseCase1(){
    List<Task> task_Obj  =  [SELECT IsReminderSet,IsRecurrence from Task];
    System.assertEquals(true,task_Obj.size()>0);
    TaskTriggerHandler obj01 = new TaskTriggerHandler();
    obj01.afterUndelete(new List<Sobject>());
  }
}
