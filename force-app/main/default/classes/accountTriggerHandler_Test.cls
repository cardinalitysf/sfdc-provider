  /**
	 * @Author        : Pratheeba V
	 * @CreatedOn     : June 29 ,2020
	 * @Purpose       : Test Class for AccountTriggerHandler.
**/
  
   @isTest
private class accountTriggerHandler_Test {
    @testSetup
  static void setupTestData(){
    test.startTest();
    //Account account_Obj = new Account(Narrative__c='Narrative', Status__c = 'Active';
    
      Account account_Obj = new Account(Name = 'Test',Status__c='Active');
      //insert account_Obj;
      Database.UpsertResult Upsertresults1 = Database.upsert(account_Obj);
        id strProviderId= Upsertresults1.getId();
      
     List<Account> account_Obj1 = new List<Account>{
    new Account(Name='Test',Narrative__c='Narrative', Status__c = 'Active'),
    new Account(Name='Test',Narrative__c='Narrative', Status__c = 'Active'),
    new Account(Name='Test',Narrative__c='Narrative', Status__c = 'Active')
      };  
   
          
     Insert account_Obj1;
    HistoryTracking__c historytracking_Obj = new HistoryTracking__c(CreatedDate__c = Date.today(), FieldName__c = '14', NewValue__c = '15', ObjectName__c = 'Account', ObjectRecordId__c = '17', RichOldValue__c = '18', RichNewValue__c = '19');
    Insert historytracking_Obj;
    test.stopTest();
  }
    
    

  static testMethod void test_afterupdate_UseCase1(){
 List<Account> account_Obj  =  [SELECT Id,Name,Narrative__c,Status__c from Account];
 List<Account> account_Obj1  =  [SELECT Id,Name,Narrative__c,Status__c from Account];
 System.assertEquals(true,account_Obj.size()>0);

 Map<Id, Account> newAccountMap = new Map<Id, Account>();   
 Map<Id, Account> oldAccountMap = new Map<Id, Account>();   
 
 newAccountMap.putAll(account_Obj);
 oldAccountMap.putAll(account_Obj1);
 for(id oid : newAccountMap.keySet()){
          oldAccountMap.get(oid).Narrative__c='Changed Narrative';
      }     
    accountTriggerHandler obj01 = new accountTriggerHandler();
    accountTriggerHandler.afterupdate(newAccountMap,oldAccountMap);
     

  }
  static testMethod void test_afterinsert_UseCase1(){
    List<Account> account_Obj  =  [SELECT Id,Narrative__c,Status__c from Account];
    System.assertEquals(true,account_Obj.size()>0);
    accountTriggerHandler obj01 = new accountTriggerHandler();
    accountTriggerHandler.afterinsert(account_Obj);
     
     List<HistoryTracking__c> historytracking_Obj  =  [SELECT Id,CreatedDate__c,FieldName__c,NewValue__c,ObjectName__c,ObjectRecordId__c,RichOldValue__c,RichNewValue__c from HistoryTracking__c];
    System.assertEquals(true,historytracking_Obj.size()>0);
    accountTriggerHandler obj02 = new accountTriggerHandler();
    accountTriggerHandler.afterinsert(new List<Account>());
  }
}