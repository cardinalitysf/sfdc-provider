/**
 * @Author        : Pratheeba V
 * @CreatedOn     : June 10 ,2020
 * @Purpose       : ProviderRecordQuestionTriggerHandler Test class
 **/
@isTest
private class ProviderRecordQuestionTrigger_Test {
    @IsTest static void setupTestData(){
    Account InsertobjNewAccount=new Account(Name = 'Test Account'); 
    insert InsertobjNewAccount;
    
    Application__c InsertobjNewApplication=new Application__c(Status__c = 'Pending',Provider__c = InsertobjNewAccount.Id);
    insert InsertobjNewApplication;  
   
        
    Address__c InsertobjNewAddress=new Address__c(Provider__c=InsertobjNewAccount.Id,Application__c=InsertobjNewApplication.Id); 
    insert InsertobjNewAddress; 
    
    Contact InsertobjNewContact = new Contact(LastName = 'Test Staff');
    insert InsertobjNewContact;     
   
    Application_Contact__c InsertobjNewAppContact = new Application_Contact__c(Application__c = InsertobjNewApplication.Id,Staff__c=InsertobjNewContact.Id);
    insert InsertobjNewAppContact;
    Id MyId1=Schema.Sobjecttype.ProviderRecordQuestion__c.getRecordTypeInfosByName().get('Office').getRecordTypeId();
    
    Id MyId2=Schema.Sobjecttype.ProviderRecordQuestion__c.getRecordTypeInfosByName().get('Board Interview').getRecordTypeId();
        

        
    Monitoring__c Insmonitoring= new Monitoring__c(ApplicationLicenseId__c=InsertobjNewApplication.Id);
    insert Insmonitoring;
    
         Task task_Obj1 = new Task(ActivityName__c='Office inspection',Monitoring__c=Insmonitoring.Id,Status__c='Completed');
    insert task_Obj1;
        
    ProviderRecordQuestion__c Prvrq1 = new ProviderRecordQuestion__c(Status__c = 'Pending',Monitoring__c=Insmonitoring.Id,RecordTypeId=MyId2);
    insert Prvrq1;
    Prvrq1.Status__c='Completed';
    update Prvrq1;
    
    ProviderRecordQuestion__c Prvrq2 = new ProviderRecordQuestion__c(Status__c = 'Completed',Monitoring__c=Insmonitoring.Id,RecordTypeId=MyId2);
    insert Prvrq2;
        
    ProviderRecordQuestion__c Prvrq3 = new ProviderRecordQuestion__c(Status__c = 'Completed',Monitoring__c=Insmonitoring.Id,RecordTypeId=MyId1);
    insert Prvrq3;


   
    //task_Obj1.Status__c='Completed';
   // update task_Obj1;
  }

  static testMethod void test_AfterUpdate_UseCase1(){
    List<ProviderRecordQuestion__c> task_Obj  =  [SELECT Id,Status__c,Monitoring__c,RecordTypeId from ProviderRecordQuestion__c];
    System.assertEquals(false,task_Obj.size()>0);
     Map<Id, ProviderRecordQuestion__c> historyMap = new Map<Id, ProviderRecordQuestion__c>(task_Obj);
     ProviderRecordQuestionTriggerHandler obj01 = new ProviderRecordQuestionTriggerHandler();
    obj01.AfterUpdate(historyMap,historyMap);
  }

  static testMethod void test_BeforeInsert_UseCase1(){
    List<Task> task_Obj  =  [SELECT Id,WhoId,Status__c,Status from Task];
    System.assertEquals(false,task_Obj.size()>0);
    ProviderRecordQuestionTriggerHandler obj01 = new ProviderRecordQuestionTriggerHandler();
    obj01.BeforeInsert(new List<Sobject>());
}

static testMethod void test_BeforeUpdate_UseCase1(){
    List<Task> task_Obj  =  [SELECT Id,WhoId,Status__c,Status from Task];
    System.assertEquals(false,task_Obj.size()>0);
    ProviderRecordQuestionTriggerHandler obj01 = new ProviderRecordQuestionTriggerHandler();
    obj01.BeforeUpdate(new Map<id,sObject>(),new Map<id,sObject>());
  }
  static testMethod void test_BeforeDelete_UseCase1(){
    List<Task> task_Obj  =  [SELECT Id,WhoId,Status__c,Status from Task];
    System.assertEquals(false,task_Obj.size()>0);
    ProviderRecordQuestionTriggerHandler obj01 = new ProviderRecordQuestionTriggerHandler();
    obj01.BeforeDelete(new Map<id,sObject>());
  }

  static testMethod void test_AfterDelete_UseCase1(){
    List<Task> task_Obj  =  [SELECT Id,WhoId,Status__c,Status from Task];
    System.assertEquals(false,task_Obj.size()>0);
    ProviderRecordQuestionTriggerHandler obj01 = new ProviderRecordQuestionTriggerHandler();
    obj01.AfterDelete(new Map<id,sObject>());
  }
  static testMethod void test_AfterUndelete_UseCase1(){
    List<Task> task_Obj  =  [SELECT Id,WhoId,Status__c,Status from Task];
    System.assertEquals(false,task_Obj.size()>0);
    ProviderRecordQuestionTriggerHandler obj01 = new ProviderRecordQuestionTriggerHandler();
    obj01.AfterUndelete(new List<Sobject>());
  }
}