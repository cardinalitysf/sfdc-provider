/*
     Author         : G.sathishkumar
     @CreatedOn     : june 25 ,2020
     @Purpose       : test class for PublicProvidersHouseholdCheck_Test Details .
*/

@isTest
public  class PublicProvidersHouseholdCheck_Test {
    @isTest static void testPublicProvidersHouseholdCheck() {
      
        Reconsideration__c InsertobjReconsider=new Reconsideration__c(); 
        insert InsertobjReconsider;

       ProviderRecordAnswer__c InsertobjInProviderRecordAnswer =new ProviderRecordAnswer__c();
      insert InsertobjInProviderRecordAnswer;

     string str1 = '[{"id":"'+InsertobjInProviderRecordAnswer.Id+'","Date__c":"2020-06-10","Comments__c":"'+InsertobjInProviderRecordAnswer.Comments__c+'","ProviderRecordQuestion__c":"'+InsertobjInProviderRecordAnswer.ProviderRecordQuestion__c+'","HouseholdStatus__c":"'+InsertobjInProviderRecordAnswer.HouseholdStatus__c+'","Comar__c":"'+InsertobjInProviderRecordAnswer.Comar__c+'"}]';


  
      try{
       PublicProvidersHouseholdCheck.getQuestionsByType();
       } catch (Exception Ex) {
                        
        }
       
         PublicProvidersHouseholdCheck.getRecordType ('Household');
       try {
            PublicProvidersHouseholdCheck.getRecordQuestionId (null, null);
            } catch (Exception Ex) {
                }
         PublicProvidersHouseholdCheck.getRecordQuestionId(InsertobjReconsider.Id, '');
          
          try {
                 PublicProvidersHouseholdCheck.bulkAddRecordAns(null);
            } catch (Exception Ex) {
              }
        PublicProvidersHouseholdCheck.bulkAddRecordAns(str1);
        try {
            PublicProvidersHouseholdCheck.getProviderAnswer(null);
            } catch (Exception Ex) {
             }
        
       PublicProvidersHouseholdCheck.getProviderAnswer('');

    }
    
}
