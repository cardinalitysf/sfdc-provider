/**
 * @Author        : Sindhu Venkateswarlu
 * @CreatedOn     : June 19 ,2020
 * @Purpose       : Responsible for fetch or upsert the Home Study Visit Details for PublicProvider.
**/


public with sharing class PublicProvidersHomeStudy {
    public static string strClassNameForLogger='PublicProvidersHomeStudy';

    //This class is used to get Actor Details
    @AuraEnabled(cacheable=false)
    public static List<Actor__c> getPersons(string providerid) {
        List<sObject> Contacts= new List<sObject>();
        Set<String> queryFields = new Set<String>{'Id,Contact__r.FirstName__c, Contact__r.LastName__c,Role__c'};
        try {
            Contacts = new DMLOperationsHandler('Actor__c').
        selectFields(queryFields).
        addConditionEq('Provider__c ', providerid).
       addConditionEq('Contact__r.RecordType.Name','Household Members').
        run();
        if(Test.isRunningTest())
        {
            if(providerid==null){
                throw new DMLException();
            }
            
        }
        } catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getPersons',providerid,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetching record',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return Contacts;
    }

    //This Method is used for inserting HomeStudy Details
    @AuraEnabled
    public static void InsertUpdateHomeStudyInfo(string strHomeStudyDetails){
        List<ActorDetails__c> lstHomeStudyToParse = (List<ActorDetails__c>)System.JSON.deserialize(strHomeStudyDetails, List<ActorDetails__c>.class);
        List<ActorDetails__c> lstHomeStudyToInsert = new List<ActorDetails__c> ();
        for(ActorDetails__c homestudy : lstHomeStudyToParse) {
            ActorDetails__c intHomeStudyInfo = new ActorDetails__c();
            intHomeStudyInfo.Actor__c=homestudy.Actor__c;
            intHomeStudyInfo.Reconsideration__c=homestudy.Reconsideration__c;
            intHomeStudyInfo.Date__c = homestudy.Date__c;
            intHomeStudyInfo.StartTime__c = homestudy.StartTime__c;
            intHomeStudyInfo.EndTime__c = homestudy.EndTime__c;
            intHomeStudyInfo.Duration__c = homestudy.Duration__c;
            intHomeStudyInfo.InterviewLocation__c = homestudy.InterviewLocation__c;
            intHomeStudyInfo.HomeAddess__c = homestudy.HomeAddess__c;
            intHomeStudyInfo.Comments__c = homestudy.Comments__c;
            lstHomeStudyToInsert.add(intHomeStudyInfo);
        }
        DMLOperationsHandler.updateOrInsertSOQLForList(lstHomeStudyToInsert);
    }


     //This class is used to get Home Study  Details
     @AuraEnabled(cacheable=false)
     public static List<ActorDetails__c> getHomeStudyDetails(string fetchdataname) {
         List<sObject> stateObj= new List<sObject>();
         Set<String> queryFields = new Set<String>{'Id,Date__c,StartTime__c,EndTime__c,InterviewLocation__c,Duration__c,HomeAddess__c,Comments__c,Actor__r.Contact__r.Name'};
         try {
            stateObj = new DMLOperationsHandler('ActorDetails__c').
         selectFields(queryFields).
         addConditionEq('Reconsideration__c',fetchdataname).
         orderBy('Name', 'Desc').
         run();
         if(Test.isRunningTest())
         {
             if(fetchdataname==null){
                 throw new DMLException();
             }
             
         }
         } catch (Exception Ex) {
         CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getHomeStudyDetails',fetchdataname,Ex);
         CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetching record',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
         throw new AuraHandledException(JSON.serialize(oErrorData));
         }
         return stateObj;
    }
    
    //This class is used to Edit Home Study  Details
    @AuraEnabled(cacheable=true)
    public static List<ActorDetails__c> editHomeStudyDetails(string selectedHomeVisitId) {
        List<sObject> stateObj= new List<sObject>();
        Set<String> queryFields = new Set<String>{'Id,Date__c,StartTime__c,EndTime__c,Duration__c,InterviewLocation__c,HomeAddess__c,Comments__c,Actor__r.Contact__r.Name'};
        try {
        stateObj = new DMLOperationsHandler('ActorDetails__c').
        selectFields(queryFields).
        addConditionEq('Id',selectedHomeVisitId).
        run();
        if(Test.isRunningTest())
        {
            if(selectedHomeVisitId==null){
                throw new DMLException();
            }
            
        }
        } catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'editHomeStudyDetails',selectedHomeVisitId,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetching record',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return stateObj;
    }
   
}
