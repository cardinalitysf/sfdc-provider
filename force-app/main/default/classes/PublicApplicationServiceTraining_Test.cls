/**
 * @Author        : Pratheeba.V
 * @CreatedOn     : June 8 ,2020
 * @Purpose       : Test Methods to unit test PublicApplicationServiceTraining class
**/

@IsTest
private class PublicApplicationServiceTraining_Test {
    @IsTest static void testPublicApplicationServiceTrainingPositive(){
        Application__c InsertobjNewApplication=new Application__c(Status__c = 'Pending'); 
        insert InsertobjNewApplication;
        Account InsertobjNewAccount=new Account(Name = 'Test Account'); 
        insert InsertobjNewAccount;
        Training__c InsertobjNewTraining=new Training__c();
        insert InsertobjNewTraining;
        SessionAttendee__c InsertobjNewSession=new SessionAttendee__c(Training__c = InsertobjNewTraining.Id);
        insert InsertobjNewSession;
        Actor__c InsertobjNewActor=new Actor__c();
        insert InsertobjNewActor;
        MeetingInfo__c InsertobjNewMeeting = new MeetingInfo__c(Training__c = InsertobjNewTraining.Id);
        insert InsertobjNewMeeting;

        try {
            PublicApplicationServiceTraining.getHouseholdMembers(InsertobjNewAccount.Id);
        } catch (Exception e) { }
        try {  
            PublicApplicationServiceTraining.getTrainingDetails('Pride');
        } catch (Exception e) { }
        try {  
            PublicApplicationServiceTraining.getSessionTraining(InsertobjNewApplication.Id);
        } catch (Exception e) { }
        try {  
            PublicApplicationServiceTraining.viewSessionDetails(InsertobjNewSession.Id);
        } catch (Exception e) { }
        try {  
            PublicApplicationServiceTraining.selectActor(InsertobjNewApplication.Id,InsertobjNewAccount.Id);
        } catch (Exception e) { }
        try {  
            PublicApplicationServiceTraining.getSessionAttendedHours(InsertobjNewApplication.Id);
        } catch (Exception e) { }
        string str = '[{"Actor__c":"'+InsertobjNewActor.Id+'","MeetingInfo__c":"'+InsertobjNewMeeting.Id+'","SessionStatus__c":"Scheduled","Application__c":"'+InsertobjNewApplication.Id+'","TotalHoursAttended__c":"27","Training__c":"'+InsertobjNewTraining.Id+'"}]';
        try {  
            PublicApplicationServiceTraining.InsertUpdateSessionInfo(str);
        } catch (Exception e) { }
    }

    @IsTest static void testPublicApplicationServiceTrainingException(){  
        try { 
            PublicApplicationServiceTraining.getHouseholdMembers('');
        } catch (Exception e) { }  
        try { 
            PublicApplicationServiceTraining.getTrainingDetails('');
        } catch (Exception e) { }
        try { 
            PublicApplicationServiceTraining.getSessionTraining('');
        } catch (Exception e) { }
        try { 
            PublicApplicationServiceTraining.viewSessionDetails('');
        } catch (Exception e) { }
        try { 
            PublicApplicationServiceTraining.selectActor('','');
        } catch (Exception e) { }
        try { 
            PublicApplicationServiceTraining.getSessionAttendedHours('');
        } catch (Exception e) { }  
    }
}