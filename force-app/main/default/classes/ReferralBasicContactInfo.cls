public class ReferralBasicContactInfo {
	@AuraEnabled(cacheable=true)
	public static List<Contract__c> getContactInfoData() {
		List<Contract__c> getData = [ SELECT UtilizationRate__c, ChildCharacteristics__c, IRCRate__c, FiscalDate__c, Status__c, ContractEndDate__c, ContractStartDate__c, ContractTermType__c, TotalContractBed__c, ContractType__c, ContractStatus__c, License__c, Name, Id FROM Contract__c ];
		
	 return getData;

//		String model = 'Contract__c';
//		String fields =
//				'UtilizationRate__c, ChildCharacteristics__c, IRCRate__c, FiscalDate__c, Status__c, ContractEndDate__c, ContractStartDate__c, ContractTermType__c, TotalContractBed__c, ContractType__c, ContractStatus__c, License__c, Name, Id';
//		//        String cond = 'Monitoring__c = \'' + monitoringId + '\'';
//		String cond = 'Monitoring__c = \'' + monitoringId + '\'';
//
//		return DMLOperationsHandler.selectSOQLWithConditionParameters(model, fields, cond);
	}

}