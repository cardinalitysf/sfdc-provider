public with sharing class providersMonitoringReview {
    public static string strClassNameForLogger='providersMonitoringReview';

    @AuraEnabled(cacheable =true)
    public static List<DMLOperationsHandler.FetchValueWrapper> getPickListValues(sObject objInfo, string picklistFieldApi) {
        return DMLOperationsHandler.fetchPickListValue(objInfo,picklistFieldApi);
    }

    @AuraEnabled
        public static void updateapplicationdetails (sObject objSobjecttoUpdateOrInsert, String supervisorId, String workItemId){
        if(supervisorId != '') {
            String Id = DMLOperationsHandler.updateOrInsertSOQLReturnId(objSobjecttoUpdateOrInsert);
            String comments = (String) objSobjecttoUpdateOrInsert.get('ApprovalComments__c');
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments(comments);
            req1.setObjectId(Id);
            req1.setSubmitterId(UserInfo.getUserId());
            req1.setNextApproverIds(new Id[] {supervisorId});
            // Submit the approval request for the Opportunity
            Approval.ProcessResult result = Approval.process(req1);
        } else {
            String comments = (String) objSobjecttoUpdateOrInsert.get('ApprovalComments__c');
            String status = (String) objSobjecttoUpdateOrInsert.get('ReviewStatus__c');
            String caseworker = (String) objSobjecttoUpdateOrInsert.get('Caseworker__c');

            if(status == 'Reassign')
                status = 'Removed';
            Approval.ProcessWorkitemRequest req1 = new Approval.ProcessWorkitemRequest();
            req1.setComments(comments);
            req1.setWorkitemId(workItemId);
            req1.setAction(status);
            req1.setNextApproverIds(new Id[] {caseworker});
            // Submit the approval request for the Opportunity
            Approval.ProcessResult result = Approval.process(req1);
        }
    }

    @AuraEnabled
	public static string updateapplicationdetailsDraft(sObject objSobjecttoUpdateOrInsert, String supervisorId){
        return DMLOperationsHandler.updateOrInsertSOQLReturnId(objSobjecttoUpdateOrInsert);
    }

    @AuraEnabled
	public static List<Monitoring__c> getReviewDetails(string monitoringId){
        String fields = 'Id, ApprovalComments__c, ReviewStatus__c, Status__c';
        List<SObject> monApp = new List<SObject>();
        try {
            monApp = new DMLOperationsHandler('Monitoring__c').
                        selectFields(fields).
                        addConditionEq('Id', monitoringId).     
                        run();
                        if(Test.isRunningTest()) {
                            if(monitoringId == null) {
                                throw new DMLException();
                            }
                        }
        } catch (Exception Ex) {
                CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getReviewDetails',monitoringId+ 'Monitoring Id', Ex);
                CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Sorry,Failed to load Referral list details ,Please Try Again..!',Ex.getMessage() , CustomAuraExceptionData.type.error.name());
                throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return monApp;
    }

    @AuraEnabled
	public static string getDatatableDetails(string monitoringId) {
        String model = 'ProcessInstance';
		String fields = 'Id, (SELECT Id, ProcessInstanceId, ActorId, Actor.Name, Actor.Profile.Name, CreatedDate FROM Workitems), (SELECT Id, StepStatus, ActorId, Actor.Name, Actor.Profile.Name, Comments, CreatedDate FROM Steps ORDER BY ID DESC)';
		String cond = 'TargetObjectId =\'' + monitoringId + '\'';
        List<SObject> lstApp= DMLOperationsHandler.selectSOQLWithConditionParameters(model, fields, cond);
        Map<Id, ContentDocumentLink> contentDocumentIdsmap = new Map<Id, ContentDocumentLink>();
        for(ContentDocumentLink conDocu : [SELECT ContentDocumentId, LinkedEntityId,LinkedEntity.Name FROM ContentDocumentLink WHERE LinkedEntityId =:monitoringId Order By SystemModstamp DESC]){
            contentDocumentIdsmap.put(conDocu.ContentDocumentId, conDocu);
        }
        List<ContentVersion> contelist = [SELECT Id, OwnerId, CreatedById FROM ContentVersion WHERE ContentDocumentId In: contentDocumentIdsmap.keyset() And IsSignature__c=true];
        Set<Id> filteredContOwner = new Set <Id>();
        Set<ContentVersion> filteredContList = new Set <ContentVersion>();
        for(ContentVersion clist : contelist){
            if(filteredContOwner.add(clist.OwnerId) == true)
            filteredContList.add(clist);
        }
        if(lstApp.size() > 0){
            List<processingWrapper> appWrap = new List<processingWrapper>();
            for(SObject app : lstApp) {
                processingWrapper prdWrapper = new processingWrapper(app, filteredContList);
                appWrap.add(prdWrapper);
            }
            return JSON.serialize(appWrap);
        } else {
            List<processingWrapperString> appWrap = new List<processingWrapperString>();
            String fieldsm = 'Id, SubmittedDate__c, Caseworker__r.Name, Supervisor__r.Name, MonitoringStatus__c, ReviewStatus__c, ApprovalComments__c';
            List<SObject> monApp = new DMLOperationsHandler('Monitoring__c').
                                    selectFields(fieldsm).
                                    addConditionEq('Id', monitoringId).     
                                    run();
                for(SObject app : monApp) {
                    if(contelist.size() > 0) {
                        for (contentversion conversion: contelist) {
                            processingWrapperString prdWrapper = new processingWrapperString(app, conversion.id);
                            appWrap.add(prdWrapper);
                        }
                    } else {
                        processingWrapperString prdWrapper = new processingWrapperString(app, '');
                        appWrap.add(prdWrapper);
                    }
                }
                return JSON.serialize(appWrap);
            }
        }
    
    @AuraEnabled
    public static string updateCaseworkerdetails(sObject objSobjecttoUpdateOrInsert){
        return JSON.serialize(DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert));
    }
    
    @AuraEnabled(cacheable=true)
    public static List<User> getAllUsersList() {
        String type = 'Supervisor';
        String fields = 'Id, Name, isActive, Profile.Name, Availability__c, CaseLoad__c, ProfileId, Unit__c';
        List<SObject> outputObj = new List<SObject>();
        try {
            outputObj = new DMLOperationsHandler('User').
                        selectFields(fields).
                        addConditionEq('Profile.Name', type).     
                        run();
                        if(Test.isRunningTest()) {
                            integer intTest =1/0;
                        }
        } catch (Exception ex) {
            //To Log into object
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getAllUsersList', 'User Lists', ex);
            //To return back to the UI
            CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('State Details Fetch Issue,Please Try Again..!', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return outputObj;               
    }
   

    @AuraEnabled
    public static void saveSign(String strSignElement,Id recId){

        // Create Salesforce File
        //Insert ContentVersion
        ContentVersion cVersion = new ContentVersion();
        cVersion.ContentLocation = 'S'; //S-Document is in Salesforce. E-Document is outside of Salesforce. L-Document is on a Social Netork.
        cVersion.PathOnClient = 'Signature-'+System.now() +'.png';//File name with extention
        cVersion.Origin = 'H';//C-Content Origin. H-Chatter Origin.
        
        
        //cVersion.OwnerId = attach.OwnerId;//Owner of the file
        cVersion.Title = 'Signature-'+System.now() +'.png';//Name of the file
        cVersion.VersionData = EncodingUtil.base64Decode(strSignElement);//File content
        cVersion.IsSignature__c=true;
        Insert cVersion;
        
        
        //After saved the Content Verison, get the ContentDocumentId
        Id conDocument = [SELECT ContentDocumentId  FROM ContentVersion WHERE Id =:cVersion.Id].ContentDocumentId;
        
        //Insert ContentDocumentLink
        ContentDocumentLink cDocLink = new ContentDocumentLink();
        cDocLink.ContentDocumentId = conDocument;//Add ContentDocumentId
        cDocLink.LinkedEntityId = recId;//Add attachment parentId
        cDocLink.ShareType = 'I';//V - Viewer permission. C - Collaborator permission. I - Inferred permission.
        cDocLink.Visibility = 'AllUsers';//AllUsers, InternalUsers, SharedUsers
        Insert cDocLink;
      
    }

    public class processingWrapper {
        public SObject monitorRecord {get;set;}
        public Set<ContentVersion> signatureUrl {get;set;}
        public processingWrapper(SObject app, Set<ContentVersion> url) {
            monitorRecord = app;
            this.signatureUrl = url;
        }
    }

    public class processingWrapperString {
        public SObject monitorRecord {get;set;}
        public String signatureUrl {get;set;}  
        public processingWrapperString(SObject app, String url) {
            monitorRecord = app;
            this.signatureUrl = url;
        }
    }
}
