/**
 * @Author        : G.sathishkumar
 * @CreatedOn     : March 18, 2020
 * @Purpose       : assingStaffMember
 * @updatedBy     :
 * @updatedOn     :
 **/
public with sharing class assingStaffMember {
   public static string strClassNameForLogger='assingStaffMember';
    @AuraEnabled(cacheable=false)
    public static List<Contact> getStaffFromProviderID(string getcontactfromprov) {
      List<sObject> staffPro= new List<sObject>();
        Set<String> fields = new Set<String>{'Account.ProviderId__c','FirstName__c','LastName__c','AffiliationType__c','JobTitle__c','EmployeeType__c'};
         try{
         staffPro=  new DMLOperationsHandler('Contact').
                                 selectFields(fields).                                                    
                                 addConditionEq('AccountId', getcontactfromprov).
                                 run();
         if(Test.isRunningTest())
               {
                if(getcontactfromprov==null){
                    throw new DMLException();
                }
         }
         
         }catch (Exception Ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getStaffFromProviderID',getcontactfromprov,Ex);
        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Provider Based Details Fetch',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));              
        }
                return staffPro;
   }

   @AuraEnabled(cacheable=false)
   public static List<Application_Contact__c> getStaffFromApplicationID(string getcontactfromapp) {
        List<sObject> staffApp= new List<sObject>();
        Set<String> fields = new Set<String>{'Id','Staff__c','Application__c'};
       try{
         staffApp= new DMLOperationsHandler('Application_Contact__c').
                                 selectFields(fields).                                                    
                                 addConditionEq('Application__c', getcontactfromapp).
                                 run();
                 if(Test.isRunningTest())
                  {
                    if(getcontactfromapp==null){
                       throw new DMLException();
                    }
              }
         
         }catch (Exception Ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getStaffFromApplicationID',getcontactfromapp,Ex);
        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Application Based Details Fetch',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));              
        }
                return staffApp;

  }

    @AuraEnabled
    public static string InsertUpdateStaffApplication(sObject objSobjecttoUpdateOrInsert){
        return DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert);
    }

    
    @AuraEnabled
    public static string deleteStaffRecord(Id deletestaffrecord){
        return DMLOperationsHandler.deleteRecordById(deletestaffrecord);
    }

    //This class used to freeze the provider application in Program Details Screen
    @AuraEnabled(cacheable = true)
    public static List<SObject> getProviderApplicationStatus (String applicationId) {
        List<sObject> staffStatus= new List<sObject>();
        Set<String> fields = new Set<String>{'Id','Status__c'};
        try{
         staffStatus= new DMLOperationsHandler('Application__c').
                                 selectFields(fields).                                                    
                                 addConditionEq('Id', applicationId).
                                 run();
                   if(Test.isRunningTest())
                   {
                       if(applicationId==null){
                       throw new DMLException();
                     }
              }
         
         }catch (Exception Ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getProviderApplicationStatus',applicationId,Ex);
        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Application Status Based Details Fetch',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));              
        }
                return staffStatus;
            }
        }