/**
 * @Author        : V.S.Marimuthu
 * @CreatedOn     : March 18 ,2020
 * @Purpose       : Test Methods to unit test referalTable class in referalTable.cls
 **/
@isTest
private class referalTable_Test {
    @isTest static void testgetReferalListPositive()
    { 
        string strCaseNumber='';
        // Insert records into cases object  which are approved 
        List<Case> lstCase=TestDataFactory.TestCaseData(10,'Approved');
        insert lstCase;// Count 10 - Approved
        //Select one ID from the inserted record
        lstCase = [Select CaseNumber from Case where Status='Approved'  LIMIT 1 ];       
        for(Case casenumber: lstCase)
        {
            strCaseNumber=casenumber.CaseNumber;
        }
       // 
        // Verify the record.
        // Send the inserted id to the  getReferalList method and check really we can able to retrive the data
        List<Case> lstSelectCase = referalTable.getReferalList(strCaseNumber,'Approved'); 
       // 
        System.assertEquals(true, lstSelectCase.Size() == 1 , 'Size ' +lstSelectCase.Size()+ 'Inserted approved passed');

       
        lstCase = TestDataFactory.TestCaseData(15,'Pending');
        insert lstCase;// Count 15 - Pending

         //Select one ID from the inserted record
         lstCase = [Select CaseNumber from Case where Status='Pending' LIMIT 1 ];
            
         for(Case casenumber: lstCase)
         {
             strCaseNumber=casenumber.CaseNumber;
         }
         
         // Verify the record.
         // Send the inserted id to the  getReferalList method and check really we can able to retrive the data
         lstSelectCase = referalTable.getReferalList(strCaseNumber,'Pending'); 
     
         System.assertEquals(true, lstSelectCase.Size() == 1 , 'Size ' +lstSelectCase.Size()+ 'Inserted rejected cases count passed');
 
       
        lstCase = TestDataFactory.TestCaseData(7,'Rejected');
        insert lstCase;// Count 7 - Rejected
          //Select one ID from the inserted record
          lstCase = [Select CaseNumber from Case where Status='Rejected' LIMIT 1];       
          for(Case casenumber: lstCase)
          {
              strCaseNumber=casenumber.CaseNumber;
          }
          // Verify the record.
          // Send the inserted id to the  getReferalList method and check really we can able to retrive the data
           lstSelectCase = referalTable.getReferalList(strCaseNumber,'Rejected'); 
          System.assertEquals(true, lstSelectCase.Size() == 1 , 'Size ' +lstSelectCase.Size()+ 'Inserted rejected cases count equals 10');
 
        // End Test
     
    }

    // Testing the Negative flow.ie, else condition
    @isTest static void testgetReferalListNegative()
    {     
        // Insert records into cases object  which are approved 
        List<Case> lstCase=TestDataFactory.TestCaseData(10,'Approved');
        insert lstCase;// Count 10 - Approved     
        
        List<Case> lstSelectCase = referalTable.getReferalList('A','Approved'); 
        System.assertEquals(false, lstSelectCase.Size() == 15 , 'Size ' +lstSelectCase.Size()+ 'Inserted approved cases count not equals 10');
        
        lstSelectCase = referalTable.getReferalList('^[A-Za-z]+$','Not Approved'); 
        System.assertEquals(false, lstSelectCase.Size() == 15 , 'Size ' +lstSelectCase.Size()+ 'Inserted approved cases count not equals 10');

    }

    // Testing the Exception flow
    @isTest static void testgetReferalListExceptin()
    {     
        List<Case> lstCase = referalTable.getReferalList('','');        
    }
}