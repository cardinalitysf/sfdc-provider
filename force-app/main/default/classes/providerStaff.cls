/**
 * @Author        : Preethi Bojarajan
 * @CreatedOn     : March 18,2020
 * @Purpose       : Provider's Staff List With Edit, History and Delete Options 
 * @updatedBy     : Preethi Bojarajan
 * @updatedOn     : Apr 25,2020
 **/

 public with sharing class providerStaff {
    @AuraEnabled(cacheable=true)
    public static List<WrapperTwoClass> getStaffsOfProvider(String providerId) {
        List<WrapperTwoClass> lstWrapper = new List<WrapperTwoClass>();
        String model = 'Contact';
        String fields ='Id, FirstName__c, LastName__c, AffiliationType__c, JobTitle__c, EmployeeType__c, StartDate__c, EndDate__c, ReasonforLeaving__c, Comments__c'; 
        String cond = 'AccountId = \''+ providerId +'\''+' ORDER BY Id DESC';
        List<Contact> contacts = DMLOperationsHandler.selectSOQLWithConditionParameters(model,fields,cond);
        String contactIds = '';
        List<Certification__c> certs = new List<SObject>();
        if(contacts.size() > 0) {
            for(Contact contact : contacts){
                contactIds += '\''+ contact.Id +'\',';
            }
            contactIds = contactIds.substring(0, contactIds.length()-1);
            String modelContract = 'Certification__c';
            String fieldsNext = 'BehavioralInterventions__c, Staff__c'; 
            String fieldFilter = 'Yes';
            String condi = 'Staff__c In ('+  contactIds  +') AND BehavioralInterventions__c = \''+ fieldFilter +'\'';
            certs = DMLOperationsHandler.selectSOQLWithConditionParameters(modelContract,fieldsNext,condi);
        }
        lstWrapper.add(new WrapperTwoClass(contacts, certs)); 
        return lstWrapper;
    }
        public class WrapperTwoClass {
            @AuraEnabled public List<SObject> contacts;
            @AuraEnabled public List<SObject> certificates;
        public WrapperTwoClass(List<SObject>contacts, List<SObject>certs) {
            this.contacts = contacts;
            this.certificates = certs;
        }
    }

    @AuraEnabled(cacheable=true)
    public static List<SObject> getStaffCertDetails(String staffId) {
        String model = 'Certification__c';
        String fields ='Id, CertificationType__c, DueDate__c, ApplicationMailedDate__c, TestedDate__c, CertificationEffectiveDate__c, CertificateNumber__c, RenewalDate__c, Staff__c'; 
        String cond = 'Staff__c = \''+ staffId +'\''+' ORDER BY Id DESC';
        return DMLOperationsHandler.selectSOQLWithConditionParameters(model,fields,cond);
    }
}