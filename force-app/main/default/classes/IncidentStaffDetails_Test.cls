/**
 * @Author        : Preethi Bojarajan
 * @CreatedOn     : July 20, 2020
 * @Purpose       : Test class for IncidentStaffDetails.cls
 **/
@isTest
public with sharing class IncidentStaffDetails_Test {
    @isTest
    private static void getStaffMembers_Test(){
        Test.startTest();
        try {
            IncidentStaffDetails.getStaffMembers('aa');
        } catch(Exception e){}
        try{
            IncidentStaffDetails.getStaffMembers(null);
            IncidentStaffDetails.getStaffMembers('');   
        } catch(Exception e){}
        Test.stopTest();
    }

    @isTest
    private static void getActorDetails_Test(){
        Test.startTest();
        try {
            IncidentStaffDetails.getActorDetails(null);
            IncidentStaffDetails.getActorDetails('');
        } catch(Exception e){}
        try{
            IncidentStaffDetails.getActorDetails('aa');   
        } catch(Exception e){}
        Test.stopTest();
    }

    @isTest
    private static void getMultiplePicklistValuess_Test(){
        Test.startTest();
        try {
            IncidentStaffDetails.getMultiplePicklistValues('Actor__c','InjurySeverityLevel__c');
        } catch(Exception e){}
        Test.stopTest();
    }
}
