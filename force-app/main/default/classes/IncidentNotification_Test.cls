    /*
    Author         : Naveen S
    @CreatedOn     : Aug 15 ,2020
    @Purpose       : test class for IncidentNotification Details .
    */
    @isTest
    public with sharing class IncidentNotification_Test {
         @isTest static void IncidentNotification_Test() {

        Application__c InsertobjApplication=new Application__c(); 
        insert InsertobjApplication;

        ProviderRecordAnswer__c   InsertobjInProviderRecordAnswer =new ProviderRecordAnswer__c();
        insert InsertobjInProviderRecordAnswer;

        string bulkobject = '[{"id":"'+InsertobjInProviderRecordAnswer.Id+'","Date__c":"2020-06-29","Comments__c":"'+InsertobjInProviderRecordAnswer.Comments__c+'","ProviderRecordQuestion__c":"'+InsertobjInProviderRecordAnswer.ProviderRecordQuestion__c+'","HouseholdStatus__c":"'+InsertobjInProviderRecordAnswer.HouseholdStatus__c+'","Dependent__c":"'+InsertobjInProviderRecordAnswer.Dependent__c+'", "State__c":"'+InsertobjInProviderRecordAnswer.State__c+'","Comar__c":"'+InsertobjInProviderRecordAnswer.Comar__c+'"}]';
        string quesId = InsertobjInProviderRecordAnswer.ProviderRecordQuestion__c;

        try {
        IncidentNotification.getQuestionsByType(null);
        } catch (Exception Ex) {
        }
        IncidentNotification.getQuestionsByType('Notification');

        
        IncidentNotification.getRecordType ('Notification');

        try {
        IncidentNotification.getRecordQuestionId(null, null);
        } catch (Exception Ex) {
        }
        PublicAppHouseHoldMemberChecklist.getRecordQuestionId(InsertobjApplication.Id, '');

        try {
        IncidentNotification.bulkAddRecordAns(null,null);
        } catch (Exception Ex) {
        }
        IncidentNotification.bulkAddRecordAns(bulkobject,quesId);

        try {
        IncidentNotification.getProviderAnswer(null);
        } catch (Exception Ex) {
        }
       IncidentNotification.getProviderAnswer ('');

        try {
        IncidentNotification.bulkUpdateRecordAns(null);
        } catch (Exception Ex) {
        }
        IncidentNotification.bulkUpdateRecordAns(bulkobject);

        }
        }
