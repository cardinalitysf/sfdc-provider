//apex class naming conventions

public with sharing class CommonNarrative {

    //This class used in Provider Narrative & Application Narrative
    @AuraEnabled
    public static string updateNarrativeDetails(sObject objSobjecttoUpdateOrInsert){
        return JSON.serialize(DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert));
    }

    
    //This class used in Application Narrative
    @AuraEnabled(cacheable = true)
        public static List<SObject> getHistoryOfNarrativeDetails(String applicationId) {
            List<sObject> history = new List<sObject>();

            try {
                history = new DMLOperationsHandler('HistoryTracking__c').
                selectFields('Id, RichOldValue__c, RichNewValue__c, LastModifiedDate, LastModifiedBy.Name').
                addConditionEq('ObjectRecordId__c', applicationId).
                addConditionEq('HistoryName__c', 'Description').
                orderBy('Id','DESC').
                run();
                 if(Test.isRunningTest())
			{
                if(applicationid==null){
                    throw new DMLException();
                }
				
			}
             } catch (Exception ex) {
                String inputParameterForHistoryOfNarrativeDetails='Inputs parameter History of Narrative Details ::'+applicationId;
                CustomAuraExceptionData.LogIntoObject('CommonNarrative','getHistoryOfNarrativeDetails',inputParameterForHistoryOfNarrativeDetails,ex);
                CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('History of NarrativeDetails',ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
                throw new AuraHandledException(JSON.serialize(oErrorData));
            }
            return history;
           
    }

    //This class used in Application Narrative
    @AuraEnabled(cacheable=true)
    public static List<SObject> getSelectedHistoryRec (String recId) {
        List<sObject> historyRec = new List<sObject>();
        try {
            historyRec = new DMLOperationsHandler('HistoryTracking__c').
        selectFields('Id, RichOldValue__c, RichNewValue__c, LastModifiedDate, LastModifiedBy.Name').
        addConditionEq('Id', recId).
        orderBy('Id','DESC').
        run();
         if(Test.isRunningTest())
			{
                if(recId==null){
                    throw new DMLException();
                }
				
			}
            
        } catch (Exception ex) {
                String inputParameterForSelectedHistoryRec='Inputs parameter Selected History Rec ::'+recId;
                CustomAuraExceptionData.LogIntoObject('CommonNarrative','getSelectedHistoryRec',inputParameterForSelectedHistoryRec,ex);
                CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Selected History Rec',ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
                throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return historyRec;
        
    }

    

    //This class used in Application Narrative & Provider Narrative 

    @AuraEnabled(cacheable=true)
    public static List<SObject> getApplicationProviderStatus (String applicationId,String sObjectType) {
        String fields;
        List<sObject> appstatus = new List<sObject>();
        try {
            if(sObjectType=='Case')
            {
                 fields = 'Id,Description,Status';
            }
            else
            {
                 fields = 'Id,Narrative__c, Status__c';
            }
             appstatus = new DMLOperationsHandler(sObjectType).
            selectFields(fields).
            addConditionEq('Id', applicationId).
            run();
             if(Test.isRunningTest())
			{
                if(applicationid==null || sObjectType==''){
                    throw new DMLException();
                }
				
			}
          } 
        catch (Exception ex) {
            String[] arguments = new String[] {applicationId , sObjectType};
            string strInputRecord= String.format('Input parameters Application Provider Status :: applicationId -- {0} :: sObjectType -- {1}', arguments);
            CustomAuraExceptionData.LogIntoObject('CommonNarrative','getApplicationProviderStatus',strInputRecord,ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Application Provider Status',ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
         }
        return appstatus;
       
    }



    // @AuraEnabled(cacheable=true)
    // public static List<SObject> getRecordType(String name) {
    //     List<sObject> recordObj = new List<sObject>();

    //     try {
    //         String queryFields = 'Id, Name, sObjectType';
    //      recordObj=  new DMLOperationsHandler('RecordType').
    //                          selectFields(queryFields).
    //                          addConditionEq('Name', name).                           
    //                          run();
    //                           if(Test.isRunningTest())
	// 		{
    //             if(name==null){
    //                 throw new DMLException();
    //             }
				
	// 		}
            
    //     } catch (Exception ex) {
    //         String inputParameterForRecordType='Inputs parameter RecordType ::'+name;
    //         CustomAuraExceptionData.LogIntoObject('CommonNarrative','getRecordType',inputParameterForRecordType,ex);
    //         CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Record Type',ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
    //         throw new AuraHandledException(JSON.serialize(oErrorData));
    //     }
    //     return recordObj;

    
    // }

    @AuraEnabled(cacheable=true)
    public static Id getRecordType(String name) {
        return CommonUtils.getRecordTypeIdbyName('Case', name);
    }
  

}

