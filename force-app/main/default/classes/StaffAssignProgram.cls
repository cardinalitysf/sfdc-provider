public with sharing class StaffAssignProgram {

    @AuraEnabled(cacheable=true)
    public static List<DMLOperationsHandler.FetchValueWrapper> getPickListValues(sObject objInfo, string picklistFieldApi) {
        return DMLOperationsHandler.fetchPickListValue(objInfo,picklistFieldApi);
    }

    @AuraEnabled(cacheable = true)
    public  static List<Contact> fetchDataForAddOrEdit(string fetchdataname) {
        String model = 'Contact';
        String fields = 'Id, AccountId,Account.Name,Name,ProgramType__c,LastName,EmployeeType__c';
        String cond = 'Id = \'' + fetchdataname + '\'';
        return DMLOperationsHandler.selectSOQLWithConditionParameters(model,fields,cond);
    }
    @AuraEnabled
    public static string updateOrInsertSOQL(sObject objSobjecttoUpdateOrInsert) {
		//This method is exposed to front end and reponsile for upsert operation
        //sObject is passed to updateOrInsertQuery where real data base insert or update will happen
		return DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert);
    }

}