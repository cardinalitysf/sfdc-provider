/**
 * @Author        : Vijayaraj M
 * @CreatedOn     : MAY 11 ,2020
 * @Purpose       : Test Methods to unit test for ProvidersIRC.cls.
**/
@isTest
private with sharing class ProvidersIRC_Test {
    @isTest static void testProvidersIRCTest() {
        IRCRates__c InsertobjNewIRCRates=new IRCRates__c(RateType__c  = 'XYZ'); 
        insert InsertobjNewIRCRates;
        License__c InsertobjNewLicense=new License__c(ProgramName__c  = 'PQR'); 
        insert InsertobjNewLicense;
        Contract__c InsertobjNewContract=new Contract__c(ContractStatus__c  = 'KJY'); 
        insert InsertobjNewContract;
        
        ProvidersIRC.getAllIRCRateDetails(InsertobjNewIRCRates.Provider__c);
        ProvidersIRC.getAllIRCRateDetails(InsertobjNewIRCRates.Status__c);
        ProvidersIRC.editIRCRateDetails(InsertobjNewIRCRates.Id);
        ProvidersIRC.ircRateHistoryForId(InsertobjNewIRCRates.LicenseType__c);
        ProvidersIRC.ircRateHistoryForId(InsertobjNewIRCRates.Status__c);
        ProvidersIRC.getAvailableLicenseTypesForProvider(InsertobjNewLicense.Application__r.Provider__c);
        ProvidersIRC.getProviderLicenseContractStatus(InsertobjNewContract.License__c);
        ProvidersIRC.getIrcRateCode();






 

    }
}