@RestResource(urlMapping='/DialogflowSlack')
global class SlackBot {
    @HTTPPost
    global static JSON2ApexSlack createRecords(){
         String request = RestContext.request.requestBody.toString();
         DialogFlowResponse r = DialogFlowResponse.parse(request);
         Integer counter;
         
       
          string text ;     
         RestResponse res = RestContext.response;
      
         if(r.queryResult.intent.displayName=='ReferralCount')
         {
             string referralType=r.queryResult.parameters.ReferralType;
             set<string> status;

           
           if(referralType == 'Private')
           {
           counter =  [SELECT count() FROM Case Where Account.ProviderType__c=:referralType and RecordType.Name='Referral' and status in ('Approved','Draft','Rejected','Pending')];
 
           }
           else if(referralType=='Public') {
            counter =  [SELECT count() FROM Case Where Account.ProviderType__c=:referralType and RecordType.Name='Referral' and status in ('Approved','Draft','Rejected','Pending','Assigned for Training','Training Unattended','Training Completed')];

           }
            text = '{"fulfillmentText":"There are '+string.valueOf(counter) +' application(s) for '+referralType+' referral"}';
        }      
        if(r.queryResult.intent.displayName=='IndividualReferralCount')
        {
            string referralType=r.queryResult.parameters.ReferralType;
            string Status=r.queryResult.parameters.Status;
          
         
            counter =  [SELECT count() FROM Case Where Account.ProviderType__c=:referralType and RecordType.Name='Referral' and status in (:Status)];

           text = '{"fulfillmentText":"There are '+string.valueOf(counter) +' '+referralType+' application(s) got '+Status+'"}';
       }      
         res.responseBody = Blob.valueOf(JSON.serialize(text));
         res.statusCode = 200;  
       return JSON2ApexSlack.parse(text);      
    }
    
}