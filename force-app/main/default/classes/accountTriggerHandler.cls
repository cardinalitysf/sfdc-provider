/**
 * @Author        : Sindhu Venkateswarlu
 * @CreatedOn     : APRIL 23 ,2020
 * @Purpose       :Narrative Trigger Handler Class on Contract Object.
**/

public class accountTriggerHandler
{
public static void afterupdate(Map<Id, Account> newMap, Map<Id, Account> oldMap){
    updateaccountHistoryTracking(newMap.values(),oldMap);
}
public static void afterinsert(List<Account> newvaluelist){
    list<HistoryTracking__c> hstTrackList = new list<HistoryTracking__c>();
    if(!newvaluelist.isEmpty()){
        for (Account acc: newvaluelist){
            HistoryTracking__c hstTrack = new HistoryTracking__c();
                    hstTrack.ObjectName__c = 'Account';
                    hstTrack.ObjectRecordId__c = acc.Id;
                    hstTrack.RichNewValue__c = acc.Narrative__c;
                    hstTrackList.add(hstTrack);

        }
    }
    if(!hstTrackList.isEmpty()){
        insert hstTrackList;
    }
}

private static void updateaccountHistoryTracking(list<Account> accounts, Map<Id, Account> oldaccMap){
    Account oldaccInfo = new Account();
        list<HistoryTracking__c> hstTrackList = new list<HistoryTracking__c>();
        for (Account acc: accounts){

            if (oldaccMap != null){

                oldaccInfo = oldaccMap.get(acc.Id);
                if(oldaccInfo.Narrative__c != acc.Narrative__c){

                    HistoryTracking__c hstTrack = new HistoryTracking__c();
                    hstTrack.ObjectName__c = 'Account';
                    hstTrack.ObjectRecordId__c = acc.Id;
                    hstTrack.RichOldValue__c = oldaccInfo.Narrative__c;
                    hstTrack.RichNewValue__c = acc.Narrative__c;
                    hstTrack.HistoryName__c = ProviderConstants.HISTORY_DESCRIPTION; 
                    hstTrackList.add(hstTrack);
                }
            }
        }
        if(hstTrackList.size() >0){
            insert hstTrackList;
        }
    }
}