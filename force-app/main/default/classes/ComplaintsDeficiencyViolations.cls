/**
 * @Author        : G sathishkumar ,
 * @CreatedOn     : July 01,2020
 * @Purpose       : ComplaintsDeficiencyViolations.
 **/


public with sharing class ComplaintsDeficiencyViolations {
   
   public static string strClassNameForLogger='ComplaintsDeficiencyViolations'; 
    @AuraEnabled(cacheable=false)
    public static List<Deficiency__c> getDeficiencyViolation(string compId) {
    List<sObject> deficiencyObj = new List<sObject>();
    Set<String> fields = new Set<String>{'Id', 'Name', 'Complaint__r.CaseNumber', 'Deficiency__c' , 'Citation__c', 'FrequencyofDeficiency__c','ImpactofDeficiency__c', 'ScopeofDeficiency__c','Findings__c', 'Comments__c'};
   try{
    deficiencyObj= new DMLOperationsHandler('Deficiency__c').
    selectFields(fields).
    addConditionEq('Complaint__c',compId).
    orderBy('Name', 'Desc').
    run();
      if(Test.isRunningTest())
			{
          if(compId==null){
            throw new DMLException();
          }
		 }
   }catch (Exception Ex) {
     CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getDeficiencyViolation',compId,Ex);
	  CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Case based error in fetching deficiencyViolation record try later',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
	  throw new AuraHandledException(JSON.serialize(oErrorData));
	}
    return deficiencyObj;
    }


    @AuraEnabled(cacheable=false)
    public static List<Case> getDeficiencyCase(string Complaint) {
    List<sObject> deficiencyObjCase = new List<sObject>();
    try{
    Set<String> fields = new Set<String>{'Id','CaseNumber' };
     deficiencyObjCase= new DMLOperationsHandler('Case').
    selectFields(fields).
    addConditionEq('Id',Complaint).
    run();
      if(Test.isRunningTest())
			{
         if(Complaint==null){
             throw new DMLException();
          }
		 }
    }catch(Exception Ex){
       CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getDeficiencyCase',Complaint,Ex);
	  CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Complaint based error in fetching complaint number record try later',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
	  throw new AuraHandledException(JSON.serialize(oErrorData));
	}
    return deficiencyObjCase;
    }


     // This function used to insertUpdate the DeficiencyViolationsDetails
    
        @AuraEnabled
        public static string insertUpdateDeficiencyViolationsDetails(sObject saveObjInsert){
            return DMLOperationsHandler.updateOrInsertSOQLReturnId(saveObjInsert);
        }



        @AuraEnabled(cacheable=false)
        public static List<Deficiency__c> editDeficiencyViolationDetails(string editDeficiencyDetail) {
       List<sObject> deficiencyEditObj = new List<sObject>();
       try{
        Set<String> fields = new Set<String>{'Id', 'Name', 'Complaint__r.CaseNumber', 'Deficiency__c' , 'Citation__c', 'FrequencyofDeficiency__c','ImpactofDeficiency__c', 'ScopeofDeficiency__c','Findings__c', 'Comments__c'};
        deficiencyEditObj= new DMLOperationsHandler('Deficiency__c').
        selectFields(fields).
        addConditionEq('Id',editDeficiencyDetail).
        run();
          if(Test.isRunningTest())
			    {
            if(editDeficiencyDetail==null){
                throw new DMLException();
            }
			   }
       }catch(Exception Ex){
          CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'editDeficiencyViolationDetails',editDeficiencyDetail,Ex);
	      CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Deficiencyviolations based error in fetching  record try later',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
	     throw new AuraHandledException(JSON.serialize(oErrorData));

       }
        return deficiencyEditObj;
        }
}
