/**
     * @Author        : Sindhu Venkateswarlu
     * @CreatedOn     : June 25,2020
     * @Purpose       :Test Methods to unit test PublicProvidersHouseholdMemberCheckList class in PublicProvidersHouseholdMemberCheckList.cls
     **/

    @isTest
    public with sharing class PublicProvidersHouseMemberCheckList_Test {
        @isTest static void testgetContactHouseHoldMembersDetails(){
            Contact con = new Contact(LastName = 'Name');
            insert con;
            Account acct = new Account(Name = 'Cardinality-CW-');
            insert acct;
            Address__c add = new Address__c(AddressLine1__c = '345 California St');
            insert add;
            ReferenceValue__c ref = new ReferenceValue__c();
            insert ref;
            Reconsideration__c recon = new Reconsideration__c();
            insert recon;
            ProviderRecordQuestion__c Prvrq = new ProviderRecordQuestion__c();
            insert Prvrq;
            ProviderRecordAnswer__c Prvra = new ProviderRecordAnswer__c();
            insert Prvra;
            Actor__c actr = new Actor__c();
            insert actr;

            string str1 = '[{"id":"'+Prvra.Id+'","Date__c":"2020-06-10","Comments__c":"'+Prvra.Comments__c+'","ProviderRecordQuestion__c":"'+Prvra.ProviderRecordQuestion__c+'","HouseholdStatus__c":"'+Prvra.HouseholdStatus__c+'", "Actor__c":"'+Prvra.Actor__c+'","Comar__c":"'+Prvra.Comar__c+'"}]';
            
            try {
                PublicProvidersHouseholdMemberCheckList.getContactHouseHoldMembersDetails(null);
            } catch (Exception Ex) {
                
            }
                PublicProvidersHouseholdMemberCheckList.getContactHouseHoldMembersDetails('acct.Id');
            try {
                PublicProvidersHouseholdMemberCheckList.getContactHouseHoldMemberAddress(null);
            } catch (Exception Ex) {
                
            }
                PublicProvidersHouseholdMemberCheckList.getContactHouseHoldMemberAddress('acct.Id');
            try {
                PublicProvidersHouseholdMemberCheckList.getQuestionsByType();

            } catch (Exception Ex) {
                
            }
            try {
                PublicProvidersHouseholdMemberCheckList.getUploadedDocuments(null);
            } catch (Exception Ex) {
                
            }
                PublicProvidersHouseholdMemberCheckList.getUploadedDocuments(con.Id);

                PublicProvidersHouseholdMemberCheckList.getRecordType('Household Member');
            try {
                PublicProvidersHouseholdMemberCheckList.getRecordQuestionId(null,null);
            } catch (Exception Ex) {
                
            }
                PublicProvidersHouseholdMemberCheckList.getRecordQuestionId('recon.Id','RecordTypeId');
            try {
                PublicProvidersHouseholdMemberCheckList.getProviderAnswer(null);
            } catch (Exception Ex) {
                
            }
                PublicProvidersHouseholdMemberCheckList.getProviderAnswer('Prvrq.Id');
            try {
                PublicProvidersHouseholdMemberCheckList.bulkAddRecordAns(null);
            } catch (Exception Ex) {
                
            }
                PublicProvidersHouseholdMemberCheckList.bulkAddRecordAns(str1);


    }
}
