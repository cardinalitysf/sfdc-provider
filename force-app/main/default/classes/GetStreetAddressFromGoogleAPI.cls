public with sharing class GetStreetAddressFromGoogleAPI {
    @AuraEnabled(cacheable=true)
	public static string getSuggestions(String input) {
        String url = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input='
            + EncodingUtil.urlEncode(input, 'UTF-8')
            + '&components=country:US' // country:US'
            + '&types=address' 
           // + '&location=40.291843,-86.795412' // Indiana co ordinates
             + '&location=34.052235,-118.243683' // California co ordinates
            + '&radius=500'
            + '&key=' + getKey();
        String response ='';
        try {
             response = getResponse(url);
             if(Test.isRunningTest())
             {
                 if(input==''){
                     throw new DMLException();
                 }
                 
             }
        } catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject('GetStreetAddressFromGoogleAPI', 'getSuggestions', 'Input Parameter input  is :: ' + input, ex);

        }
      
        return response;
	}
    @AuraEnabled(cacheable=true)
	public static string getPlaceDetails(String placeId) {
     	String url = 'https://maps.googleapis.com/maps/api/place/details/json?'
	            + 'placeid=' + EncodingUtil.urlEncode(placeId, 'UTF-8')
                + '&key=' + getKey();
	    String response = getResponse(url);
	    return response;
	}
	public static string getResponse(string strURL){
        String responseBody='';
        try {
            Http h = new Http();
		HttpRequest req = new HttpRequest();
		HttpResponse res = new HttpResponse();
		req.setMethod('GET');
		req.setEndpoint(strURL);
		req.setTimeout(120000);
		res = h.send(req); 
         responseBody = res.getBody(); 
        } catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject('GetStreetAddressFromGoogleAPI', 'getResponse', 'Input Parameter strURL  is :: ' + strURL, ex);

        }
		
		return responseBody;
	}
 
	public static string getKey(){
		string key = 'AIzaSyCIaY09lvZVSWi_-o3enJRdcTHQzc6ScSY';
		//string output = '&key=' + key;	 
		return key;
	}
}