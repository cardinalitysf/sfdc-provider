/**
 * @Author        : Sindhu Venkateswarlu
 * @CreatedOn     : MARCH 31 ,2020
 * @Purpose       :Narrative Trigger Hadler Class.
**/
public class applicationTriggerHandler
{
public static void afterupdate(Map<Id, Application__c> newMap, Map<Id, Application__c> oldMap){
    updateHistoryTracking(newMap,oldMap);
    updateApplicationForLicense(newMap,oldMap);
    //Below line is Added by Marimuthu om May-4 to update provider status ,if application status moved to approved
    updateProviderStatusInAccount(newMap,oldMap);

}
private static void updateHistoryTracking(Map<Id, Application__c> newMap, Map<Id, Application__c> oldMap){

    list<HistoryTracking__c> hstTrackList = new list<HistoryTracking__c>();
    for (Application__c contr: newMap.values()){

        Application__c newApplication = newMap.get(contr.ID);
        Application__c oldApplication = oldMap.get(contr.ID);
            if(oldApplication.Narrative__c != newApplication.Narrative__c){

                HistoryTracking__c hstTrack = new HistoryTracking__c();
                hstTrack.ObjectName__c = 'Application__c';
                hstTrack.ObjectRecordId__c = contr.Id;
                hstTrack.RichOldValue__c = oldApplication.Narrative__c;
                hstTrack.RichNewValue__c = newApplication.Narrative__c;
                hstTrack.HistoryName__c = ProviderConstants.HISTORY_DESCRIPTION; 
                hstTrackList.add(hstTrack);
            }
       
    }
    if(hstTrackList.size() >0){
        insert hstTrackList;
    }
}


    /**
	 * @Author        : V.S.Marimuthu
	 * @CreatedOn     : April 6 ,2020
	 * @Purpose       : Method to create License record when Appliction status moved to caseworked submitted
	 **/

    public static void updateApplicationForLicense(Map<Id, Application__c> newMap, Map<Id, Application__c> oldMap){
        try{
		    List<License__c> lstLicense= new List<License__c>();
            Map<String,Id> oMaptoGetAddessIdDependsOnApplicationType=new Map<String,Id>();
		    for(Application__c objApplication : [Select Id,Status__c,Program__c,ProgramType__c,LicenseStartdate__c,LicenseEnddate__c,Provider__c,RecordType.Name,(select id,AddressType__c from Address__r where AddressType__c In ('Site','Home Info')) from Application__c  where ID in : ((Map<Id,Application__c>)newMap).keyset()]){
		        //Create an old and new map so that we can compare values
		        Application__c oldApplication = oldMap.get(objApplication.ID);
		        Application__c newApplication = newMap.get(objApplication.ID);
		        //If the fields are different, then ID has changed
		        if(oldApplication.Status__c != newApplication.Status__c && newApplication.Status__c.toUppercase() == ProviderConstants.APPROVED){
                 License__c objLicense=new License__c();  
                 objLicense.ProgramName__c=objApplication.Program__c;
                 objLicense.ProgramType__c=objApplication.ProgramType__c;
                 objLicense.Application__c=objApplication.Id;
                 objLicense.StartDate__c =  objApplication.LicenseStartdate__c;
                 objLicense.EndDate__c = objApplication.LicenseEnddate__c;
                 objLicense.Provider__c  = objApplication.Provider__c;
                 for (Address__c   addType : objApplication.Address__r) {
                    oMaptoGetAddessIdDependsOnApplicationType.put(addType.AddressType__c ,addType.Id );
                 }
                 if(objApplication.RecordType.Name=='Private')
                 {
                     objLicense.Address__c=oMaptoGetAddessIdDependsOnApplicationType.get('Site');

                 }
                 else if(objApplication.RecordType.Name=='Public')
                 {
                      objLicense.Address__c=oMaptoGetAddessIdDependsOnApplicationType.get('Home Info');
                 }
                 Date dtToday = system.Today();
                 Date dtLicenseEndDate = objApplication.LicenseEnddate__c;   
                 if(dtLicenseEndDate <= dtToday)
                 {
                    objLicense.Status__c = ProviderConstants.LICENSEEXPIRED;
                 }
                 else {
                    objLicense.Status__c = ProviderConstants.LICENSEISSUED;
                 }
                 lstLicense.add(objLicense);
		        }
		    }
		    if(lstLicense.size()>0){
                if(
                    License__c.SObjectType.getDescribe().isAccessible() &&
                    Schema.SObjectType.License__c.fields.ProgramName__c.isAccessible() &&
                    Schema.SObjectType.License__c.fields.ProgramType__c.isAccessible() &&
                    Schema.SObjectType.License__c.fields.Application__c.isAccessible()
                ) {
                        insert lstLicense;
		   
		    }// if
        }
    }
		catch(Exception Ex)
		{
		}
    }

     /**
	 * @Author        : V.S.Marimuthu
	 * @CreatedOn     : May 4 ,2020
	 * @Purpose       : Method to Update Account when Appliction status moved to Approved
	 **/

    public static void updateProviderStatusInAccount(Map<Id, Application__c> newMap, Map<Id, Application__c> oldMap){
        try{
            Set<Account> lstPrivateAppToFetchProvider= new Set<Account>();
            Set<Account> lstPublicAppToFetchProvider= new Set<Account>();
		    for(Application__c objApplication : ((Map<Id,Application__c>)newMap).values()){
		        //Create an old and new map so that we can compare values
		        Application__c oldApplication = oldMap.get(objApplication.ID);
		        Application__c newApplication = newMap.get(objApplication.ID);
		        //If status is approved then form the private and public provider set
		        if(oldApplication.Status__c != newApplication.Status__c && newApplication.Status__c.toUppercase() == ProviderConstants.APPROVED){
                 Account objAccount=new Account();  
                 objAccount.Id=objApplication.Provider__c;    
                 //If Recordtype is private 
                    if(Schema.getGlobalDescribe().get('Application__c').getDescribe().getRecordTypeInfosById().get(objApplication.RecordTypeId).getName().toUppercase() =='PRIVATE')
                    {
                        lstPrivateAppToFetchProvider.add(objAccount); 
                    }
                //If Recordtype is Public 
                    else if(Schema.getGlobalDescribe().get('Application__c').getDescribe().getRecordTypeInfosById().get(objApplication.RecordTypeId).getName().toUppercase()=='PUBLIC')
                    {
                         lstPublicAppToFetchProvider.add(objAccount);  
                    }           
               
		        }
		    }
		    if(lstPrivateAppToFetchProvider.size()>0){
                //Update Account for Private provider
                UpdateAccountForPrivateProvider(lstPrivateAppToFetchProvider);
            }// Private provider if

            if(lstPublicAppToFetchProvider.size()>0){
                 //Update Account for Public provider
                UpdateAccountForPublicProvider(lstPublicAppToFetchProvider);
		    }// Public provider if
        }
  
		catch(Exception Ex)
		{
		}
    }
    private static void  UpdateAccountForPublicProvider(Set<Account> lstPublicAppToFetchProvider){
        try {
            List<Account> lstAcctToUpdate=new List<Account>();
            for (Account oAcc:lstPublicAppToFetchProvider)
            {
                Account objAccount=new Account();  
                objAccount.id=oAcc.Id;
                objAccount.Status__c=ProviderConstants.ACTIVE;
                lstAcctToUpdate.add(objAccount);
            }
            upsert lstAcctToUpdate;
        } catch (Exception Ex) {
            throw Ex;
            
        }
    }
    private static void UpdateAccountForPrivateProvider(Set<Account> lstPrivateAppToFetchProvider){
        try {
            List<Account> lstAccount=  new DMLOperationsHandler('Account').
                                        selectFields('Id,Status__c').  
                                        addConditionIn('Id',lstPrivateAppToFetchProvider). 
                                        run();

                if(lstAccount.size()>0)
                {
                     List<Account> lstAcctToUpdate=new List<Account>();
                        for(Account acc:lstAccount)
                        {
                            //If Status is empty then only update provider
                            if(String.isEmpty(acc.Status__c))
                            {
                                Account objAccount=new Account();  
                                objAccount.id=acc.Id;
                                objAccount.Status__c=ProviderConstants.INACTIVE;
                                lstAcctToUpdate.add(objAccount);
                            }
                        }
                    upsert lstAcctToUpdate;
                }
        } catch (Exception Ex) {
            throw Ex;
        }
    }
    
}