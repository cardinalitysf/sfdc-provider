/**
 * @Author        : Pratheeba V
 * @CreatedOn     : June 19, 2020
 * @Purpose       : Public Provider Training Tab 
 **/
public with sharing class publicProvidersTraining {
    public static string strClassNameForLogger='publicProvidersTraining';
    @AuraEnabled(cacheable=false)
    public static List<Actor__c > getHouseholdMembers(string providerid) {
        List<Actor__c > householdMemberObj = new List<Actor__c>();
        try {
            householdMemberObj = new DMLOperationsHandler('Actor__c').
                                    selectFields('Contact__r.Name,Contact__r.Id, Id, Role__c').
                                    addConditionEq('Provider__c ', providerid).
                                    run();
            if(Test.isRunningTest()) {
                if(providerid=='') {
                    throw new DMLException();
                }				
            }
        } catch (Exception Ex) { 
            String[] arguments = new String[] {providerid};
            string strInputRecord= String.format('Input parameters are :: providerid -- {0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getHouseholdMembers',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Members List',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return householdMemberObj;
    }
     
    @AuraEnabled(cacheable=false)
    public static List<Training__c> getTrainingDetails(string param) {
        List<Training__c> trainingDetailsObj = new List<Training__c>();
        Set<String> fields = new Set<String>{'Id','Name','Date__c','Description__c','StartTime__c','EndTime__c','SessionType__c','Duration__c'};
        try {
            trainingDetailsObj = new DMLOperationsHandler('Training__c').
                                    selectFields(fields).
                                    addConditionEq('Type__c', param).
                                    addConditionEq('RecordType.Name', 'Public').
                                    addConditionEq('Status__c', 'Scheduled').
                                    addConditionGe('Date__c', Date.today()).
                                    orderBy('Date__c', 'Asc').
                                    run();
            if(Test.isRunningTest()) {
                if(param=='') {
                    throw new DMLException();
                }				
            }
        } catch (Exception Ex) { 
            String[] arguments = new String[] {param};
            string strInputRecord= String.format('Input parameters are :: param -- {0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getTrainingDetails',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Training List',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return trainingDetailsObj;
    }

    @AuraEnabled(cacheable=false)
    public static List<SessionAttendee__c> getSessionTraining(string reconsiderationid, string providerid) {
        List<SessionAttendee__c> meetingDetailsObj = new List<SessionAttendee__c>();
        Set<String> fields = new Set<String>{'Id','Training__c','Training__r.Name','Training__r.Date__c','Training__r.SessionType__c','Actor__r.Contact__r.Name','Actor__r.Role__c','SessionStatus__c','IntenttoAttend__c','Training__r.Jurisdiction__c','Training__r.Duration__c'};
        try {
            meetingDetailsObj = new DMLOperationsHandler('SessionAttendee__c').
                                selectFields(fields).
                                addConditionEq('Reconsideration__c', reconsiderationid).
                                addConditionEq('Referral__c', providerid).
                                orderBy('CreatedDate', 'Desc').
                                run();
            if(Test.isRunningTest()) {
                if(reconsiderationid=='' || providerid=='') {
                    throw new DMLException();
                }				
            }
        } catch (Exception Ex) { 
            String[] arguments = new String[] {reconsiderationid,providerid};
            string strInputRecord= String.format('Input parameters are :: reconsiderationid -- {0},providerid -- {1}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getSessionTraining',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Session List',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return meetingDetailsObj;
    }

    @AuraEnabled(cacheable=false)
    public static List<SessionAttendee__c> viewSessionDetails(String sessionid) {
        List<SessionAttendee__c> sessionObj = new List<SessionAttendee__c>();
        Set<String> fields = new Set<String>{'Id','Training__r.Name','Training__r.Description__c','Training__r.Duration__c','Training__r.StartTime__c','Training__r.EndTime__c','Actor__r.Contact__r.Name','Training__r.Date__c','SessionStatus__c'};
        try {
            sessionObj = new DMLOperationsHandler('SessionAttendee__c').
            selectFields(fields).
            addConditionEq('Id', sessionid).
            run();
            if(Test.isRunningTest()) {
                if(sessionid=='') {
                    throw new DMLException();
                }				
            }
        } catch (Exception Ex) { 
            String[] arguments = new String[] {sessionid};
            string strInputRecord= String.format('Input parameters are :: sessionid -- {0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'viewSessionDetails',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('View Session',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return sessionObj;
    }

    @AuraEnabled(cacheable=true)
    public static List<Actor__c> selectActor(string applicationid, string providerid) {
        List<Actor__c> actorId = new List<Actor__c>();
        Set<String> fields = new Set<String>{'Id'};
        try {
            actorId = new DMLOperationsHandler('Actor__c').
                        selectFields(fields).
                        addConditionEq('Application__c', applicationid).
                        addConditionEq('Provider__c', providerid).
                        addConditionEq('Role__c', 'Applicant').
                        run();
            if(Test.isRunningTest()) {
                if(applicationid=='' || providerid=='') {
                    throw new DMLException();
                }				
            }
        } catch (Exception Ex) { 
            String[] arguments = new String[] {applicationid,providerid};
            string strInputRecord= String.format('Input parameters are :: applicationid -- {0},providerid--{1}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'selectActor',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Actor List',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return actorId;
    }

    @AuraEnabled
    public static void InsertUpdateSessionInfo(string strTrainingDetails){
        List<SessionAttendee__c> lstMeetingToParse = (List<SessionAttendee__c>)System.JSON.deserialize(strTrainingDetails, List<SessionAttendee__c>.class);
        List<SessionAttendee__c> lstMeetingToInsert = new List<SessionAttendee__c> ();
        for(SessionAttendee__c meeting : lstMeetingToParse) {
            SessionAttendee__c intMeetingInfo = new SessionAttendee__c();
            intMeetingInfo.Actor__c = meeting.Actor__c;
            intMeetingInfo.Training__c = meeting.Training__c;
            intMeetingInfo.SessionStatus__c = meeting.SessionStatus__c;
            intMeetingInfo.Referral__c = meeting.Referral__c;
            intMeetingInfo.TotalHoursAttended__c = meeting.TotalHoursAttended__c;
            intMeetingInfo.Reconsideration__c = meeting.Reconsideration__c;
            lstMeetingToInsert.add(intMeetingInfo);
        }
        DMLOperationsHandler.updateOrInsertSOQLForList(lstMeetingToInsert);
    }

    @AuraEnabled
    public static List<AggregateResult> getSessionAttendedHours(string reconsiderationid) {
        string role='Applicant';
        string status = 'Completed';
        List<AggregateResult> groupedResults =  new List<AggregateResult>();
        try {
            groupedResults =  new DMLOperationsHandler('SessionAttendee__c').
                                                    selectField('Actor__r.Contact__r.Name').   
                                                    sum('TotalHoursAttended__c', 'totalhours').
                                                    addConditionEq('Actor__r.Role__c',role).
                                                    addConditionEq('SessionStatus__c',status).
                                                    addConditionEq('Reconsideration__c',reconsiderationid).
                                                    groupBy('Actor__r.Contact__r.Name').
                                                    aggregate();
            if(Test.isRunningTest()) {
                if(reconsiderationid=='') {
                    throw new DMLException();
                }				
            }
        } catch (Exception Ex) { 
            String[] arguments = new String[] {reconsiderationid};
            string strInputRecord= String.format('Input parameters are :: reconsiderationid -- {0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getSessionAttendedHours',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Attended Hours',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }       
        return groupedResults;
    }
}
