/**
     * @Author        : Sindhu
     * @CreatedOn     : July 15,2020
     * @Purpose       :Test Methods to unit test for ComplaintsMonitoringActivities.cls
     **/
    
    @isTest
    public with sharing class ComplaintsMonitoringActivities_Test {
    @isTest static void ComplaintsMonitoringActivities_Test() {
        Task InsertobjNewTask=new  Task(ActivityName__c='XXX');
        insert InsertobjNewTask;
        ReferenceValue__c InsertobjNewRefer =new ReferenceValue__c(RefKey__c='MMMM');
        insert InsertobjNewRefer;
        Application__c InsertobjNewApplication=new Application__c(Status__c = 'Pending'); 
        insert InsertobjNewApplication;
        ApplicationMonitoring.getMonitoringDetails(InsertobjNewApplication.Id);
        Monitoring__c InsertobjNewMonitoring=new Monitoring__c(ApplicationLicenseId__c = InsertobjNewApplication.Id); 
        insert InsertobjNewMonitoring; 
        ComplaintsMonitoringActivities.UpdateTask(InsertobjNewTask);
        ComplaintsMonitoringActivities.deleteActivityDetails(InsertobjNewTask.Id);
        ComplaintsMonitoringActivities.editActivityDetails(InsertobjNewTask.Id);
        ComplaintsMonitoringActivities.getActivityListFromTask(InsertobjNewTask.Monitoring__c);
        ComplaintsMonitoringActivities.getActivityValueFromReference();
        string str = '[{"Activity__c":"testActivity","Monitoring__c":"'+InsertobjNewMonitoring.Id+'","Status__c":"Scheduled","Comments__c":"Sample","CompletionDate__c":"","WhatId":"'+InsertobjNewMonitoring.Id+'"}]';
        
           try
           {    
        ComplaintsMonitoringActivities.InsertUpdateActivityInfo(str,InsertobjNewMonitoring.Id,'');
           } catch (Exception Ex){
                   
               }
               
        ComplaintsMonitoringActivities.InsertUpdateActivityInfo(str,InsertobjNewMonitoring.Id,'Insert');
         
        try {
          
            List<DMLOperationsHandler.FetchValueWrapper> objgetPickListValues = ComplaintsMonitoringActivities.getActivitySubType(InsertobjNewTask,'');
        } 
        catch (Exception Ex)
        {
          
        }

    }
}