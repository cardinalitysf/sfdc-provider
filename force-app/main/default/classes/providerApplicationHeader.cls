public with sharing class providerApplicationHeader {
    @AuraEnabled(cacheable=true)
    public static List<SObject> getApplicationId(String Id) {
        String model = 'Application__c';
        String fields ='Id, Name'; 
        String cond = 'Id = \''+ Id +'\'';     
    return DMLOperationsHandler.selectSOQLWithConditionParameters(model,fields,cond);
    }

    @AuraEnabled(cacheable=true)
    public static List<SObject> getProviderId(String Id) {
        String model = 'Account';
        String fields ='Id, Name,Program__c'; 
        String cond = 'Id = \''+ Id +'\'';     
    return DMLOperationsHandler.selectSOQLWithConditionParameters(model,fields,cond);
    }
}