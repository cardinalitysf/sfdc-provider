/**
 * @Author        : K.Sundar
 * @CreatedOn     : APRIL 1 ,2020
 * @Purpose       : Responsible for fetch or upsert the Contact Details.
**/

public with sharing class applicationContactDetailsAddOrEdit {
    @AuraEnabled(cacheable = true)
    public static List<SObject> getAllContactDetails (String applicationId) {
        String model = 'ContactNotes__c';
        String fields = 'Id, ContactType__c, Purpose__c, Location__c, Date__c, Time__c, ContactName__c, ContactRole__c, Phone__c, Email__c, Narrative__c';
        String cond = 'Application__c = \''+ applicationId +'\''+' ORDER BY Id DESC';

        return DMLOperationsHandler.selectSOQLWithConditionParameters (model, fields, cond);
    }

    @AuraEnabled
    public static List<SObject> editContactDetails (String selectedContactId) {
        String model = 'ContactNotes__c';
        String fields = 'Id, ContactType__c, Purpose__c, Location__c, Date__c, Time__c, ContactName__c, ContactRole__c, Phone__c, Email__c, Narrative__c';
        String cond = 'Id = \''+ selectedContactId +'\'';

        return DMLOperationsHandler.selectSOQLWithConditionParameters (model, fields, cond);
    }

    @AuraEnabled(cacheable = true)
    public static List<SObject> getApplicationProviderStatus (String applicationId) {
        String model = 'Application__c';
        String fields = 'Id, Status__c';
        String cond ='Id = \''+ applicationId +'\'';

        return DMLOperationsHandler.selectSOQLWithConditionParameters (model, fields, cond);
    }
}