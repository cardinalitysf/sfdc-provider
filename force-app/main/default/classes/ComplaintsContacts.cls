/***
 * @Author: Janaswini S
 * @CreatedOn: July 02, 2020
 * @Purpose: Complaints Contact Details Add/View/Delete
 **/
public with sharing class ComplaintsContacts {
    public static string strClassNameForLogger='ComplaintsContacts';
    @AuraEnabled(cacheable = true)
        public static List<SObject> fetchContactDetails(String applicationId) {
            List<sObject> contactNote = new List<sObject>();
            try{
                contactNote = new DMLOperationsHandler('ContactNotes__c').
                selectFields('Id,Name, ContactType__c, Purpose__c, Location__c, Date__c, Time__c, ContactName__c, ContactRole__c, Phone__c, Email__c, Narrative__c,Case__c,Source__c').
                addConditionEq('Case__c', applicationId).
                orderBy('Id','DESC').
                run();
                if(Test.isRunningTest())
                {
                    if(applicationId==null){
                        throw new DMLException();
                    }
                    
                }
            }catch (Exception ex) {
                CustomAuraExceptionData.LogIntoObject('ComplaintsContacts','fetchContactDetails','Input parameters are :: applicationId' + applicationId,ex);
                CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetching contact details','Unable to get contact details, Please try again' , CustomAuraExceptionData.type.Error.name());
                throw new AuraHandledException(JSON.serialize(oErrorData));
            }
            
            return contactNote;
    }

    @AuraEnabled
    public static List<SObject> editContactDetails(String selectedContactId) {
        List<sObject> contactNote = new List<sObject>();
        try {
            contactNote = new DMLOperationsHandler('ContactNotes__c').
            selectFields('Id,Name, ContactType__c, Purpose__c, Location__c, Date__c, Time__c, ContactName__c ,ContactRole__c, Phone__c, Email__c, Narrative__c,Case__c,Source__c,LastModifiedDate,LastModifiedBy.Name').
            addConditionEq('Id', selectedContactId).
            run();
            if(Test.isRunningTest())
                {
                    if(selectedContactId==null){
                        throw new DMLException();
                    }
                    
                }
        } catch (Exception ex) {
            CustomAuraExceptionData.LogIntoObject('ComplaintsContacts','editContactDetails','Input parameters are :: selectedContactId' + selectedContactId,ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in editing contact details','Unable to save contact details, Please try again' , CustomAuraExceptionData.type.Error.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }             
        return contactNote;
 
     }

     @AuraEnabled(cacheable =true)
     public static Map<String, List<DMLOperationsHandler.FetchValueWrapper>> getMultiplePicklistValues(String objInfo, String picklistFieldApi) {
         return DMLOperationsHandler.fetchMultiplePickListValue(objInfo,picklistFieldApi);
    }

    @AuraEnabled(cacheable = true)
        public static List<SObject> getApplicationProviderStatus(String applicationId) {
            List<sObject> contactNote = new List<sObject>();
            try{
                contactNote = new DMLOperationsHandler('Case').
                selectFields('Id, Status').
                addConditionEq('Id', applicationId).
                run();
                if(Test.isRunningTest())
                {
                    if(applicationId==null){
                        throw new DMLException();
                    }
                    
                }
            }catch (Exception ex) {
                CustomAuraExceptionData.LogIntoObject('ComplaintsContacts','getApplicationProviderStatus','Input parameters are :: applicationId' + applicationId,ex);
                CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetching status','Unable to get status details, Please try again' , CustomAuraExceptionData.type.Error.name());
                throw new AuraHandledException(JSON.serialize(oErrorData));
            }
            
            return contactNote;
    }
}
