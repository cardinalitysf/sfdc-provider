/**
     * @Author        : Sindhu Venkateswarlu
     * @CreatedOn     : June 22,2020
     * @Purpose       :Test Methods to unit test PublicProvidersHomeStudy class in PublicProvidersHomeStudy.cls
     **/

    @isTest
    public with sharing class PublicProvidersHomeStudy_Test {
        @isTest static void testgetPersons (){
            Actor__c InsertobjNewActor=new Actor__c(Role__c='Applicant'); 
            insert InsertobjNewActor;

            Account InsertobjNewAccount=new Account(Name = 'test Account'); 
            insert InsertobjNewAccount;

            ActorDetails__c actdet = new ActorDetails__c();
            insert actdet;
            Reconsideration__c recon = new Reconsideration__c();
            insert recon;

            try
            {
                PublicProvidersHomeStudy.getPersons(null);
            }
            catch(Exception Ex)
            {
            }
            PublicProvidersHomeStudy.getPersons(InsertobjNewAccount.Id);
            string str = '[{"Actor__c":"'+InsertobjNewActor.Id+'","MeetingInfo__c":"'+recon.Id+'","SessionStatus__c":"Scheduled","Reconsideration__c":"'+recon.Id+'","TotalHoursAttended__c":"27"}]';
            
            PublicProvidersHomeStudy.InsertUpdateHomeStudyInfo(str);
             try
            {
                PublicProvidersHomeStudy.getHomeStudyDetails(null);
            }
            catch(Exception Ex)
            {
            }
            PublicProvidersHomeStudy.getHomeStudyDetails(recon.Id);
             try
            {
                PublicProvidersHomeStudy.editHomeStudyDetails(null);
            }
            catch(Exception Ex)
            {
            }
            PublicProvidersHomeStudy.editHomeStudyDetails('');
        }
}