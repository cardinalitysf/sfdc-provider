public with sharing class CommonUtils {
    public static string strClassNameForLogger='CommonUtils';
    @AuraEnabled(cacheable=true)
    public static List<DMLOperationsHandler.FetchValueWrapper> getPickListValues(sObject objInfo, string picklistFieldApi) {
        return DMLOperationsHandler.fetchPickListValue(objInfo,picklistFieldApi);
    }

    @AuraEnabled
    public static datetime getcurrentDateTime()
    {
        return System.now();
    }

    //Common Save Signature method
    public static void saveSign(string strSignElement, Id recId) {
        try {
            //Insert ContentVersion
            ContentVersion cVersion = new ContentVersion();
            cVersion.PathOnClient = 'Signature-'+System.now() +'.png';//File name with extention
            cVersion.Title = 'Signature - ' + System.now() + '.png';//Name of the file
            cVersion.VersionData = EncodingUtil.base64Decode(strSignElement);//File content
            cVersion.IsSignature__c = true;
            Insert cVersion;

            //After saved the Content Verison, get the ContentDocumentId
            Id conDocument = [SELECT ContentDocumentId  FROM ContentVersion WHERE Id =:cVersion.Id].ContentDocumentId;
    
            //Insert ContentDocumentLink
            ContentDocumentLink cDocLink = new ContentDocumentLink();
            cDocLink.ContentDocumentId = conDocument;//Add ContentDocumentId
            cDocLink.LinkedEntityId = recId;//Add attachment parentId
            cDocLink.ShareType = 'I';//V - Viewer permission. C - Collaborator permission. I - Inferred permission.
            cDocLink.Visibility = 'AllUsers';//AllUsers, InternalUsers, SharedUsers
            Insert cDocLink;

            if(Test.isRunningTest())
            {
        if(strSignElement=='' || recId==null){
            throw new DMLException();
        }
           }
        } catch (Exception ex) {
          
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'saveSign',  strSignElement + 'strSignElement' + recId + 'recId',Ex);
        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('saveSign Details fetch ',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        //return cDocLink;
    }

    //For Fetching State Details in Google Api Calls
    public static List<SObject> getStateDetails() {
        List<SObject> stateObj= new List<SObject>();
        String model = 'ReferenceValue__c';
        String fields = 'Id, RefValue__c';
        try{        
        stateObj = new DMLOperationsHandler(model).
                                    selectFields(fields).
                                    addConditionEq('Domain__c', 'State').
                                    run();
                                    
                            if(Test.isRunningTest()) {
                                 integer intTest =1/0;
                             }
                } catch (Exception ex) {
                                        //To Log into object
                  CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getStateDetails', null, ex);
                   //To return back to the UI
                 CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('State Details Fetch Issue,Please Try Again..!', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
                throw new AuraHandledException(JSON.serialize(oErrorData));
    } 
        return stateObj;
    }

    
    //Get the Signature of Current Login User
    public static List<contentversion>  getLoginUserSignature(String strDocumentID) {
        List<SObject> oContentVersionList= new List<SObject>();
        try {
            oContentVersionList= new DMLOperationsHandler('contentversion').
            selectFields('id,ContentDocumentId,Description,CWhealth__c,PathOnClient,DocumentSize__c,Title, Createddate,filetype,createdBy.Name').
            addConditionEq('IsSignature__c',true).
            addConditionEq('CreatedById',UserInfo.getUserId()).
            addConditionIn('ContentDocumentId ', getMapOfContentDocumentIDFromLinkedEntityID(strDocumentID).keyset()).
            orderBy('Createddate', 'Desc').
            run();   
         
   }catch(Exception Ex){
      CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getLoginUserSignature',strDocumentID,Ex);
      CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Signature Fetch Issues',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
     throw new AuraHandledException(JSON.serialize(oErrorData));

   }
        return oContentVersionList;
    }
     
    //Get the Uploaded documents except the sign and background check
    public static List<contentversion>  getUploadedDocuments(String strDocumentID) {
        List<contentversion> oContentVersionList = new List<contentversion>();        
        try {
            oContentVersionList= new DMLOperationsHandler('contentversion').
            selectFields('id,ContentDocumentId,Description,CWhealth__c,PathOnClient,DocumentSize__c,Title, Createddate,filetype,createdBy.Name').
            addConditionNotEq('IsSignature__c',true).
            addConditionNotEq('IsBackgroundCheck__c',true).
            addConditionIn('ContentDocumentId ', getMapOfContentDocumentIDFromLinkedEntityID(strDocumentID).keyset()).
            orderBy('Createddate', 'Desc').
            run();   

      } catch (Exception Ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getUploadedDocuments',strDocumentID,Ex);
        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Documents  Fetch Issue',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));              
        }
        return oContentVersionList;
    }
    public static Map<Id,contentversion[]>  getUploadedDocumentsByLinkedEntityID(Set<Id> Id) {
        Map<Id, ContentDocumentLink> contentDocumentIdsmap = new Map<Id, ContentDocumentLink>();
        Map<Id, contentversion[]> oAmpContDocLink = new Map<Id, contentversion[]>();
        try{
            List<ContentDocumentLink> oContentDocumetLink= new DMLOperationsHandler('ContentDocumentLink').
                                                            selectFields('Id, ContentDocument.Description, ContentDocumentId, ContentDocument.LatestPublishedVersionId,LinkedEntityId,LinkedEntity.Name').
                                                            addConditionIn('LinkedEntityId ', Id).
                                                            run();            
            for(ContentDocumentLink oContentDoc : oContentDocumetLink){
            contentDocumentIdsmap.put(oContentDoc.ContentDocumentId, oContentDoc);
            }      
        

          List<contentversion>  oContentVersionList= new DMLOperationsHandler('contentversion').
                                selectFields('id,ContentDocumentId,Description,CWhealth__c,PathOnClient,DocumentSize__c,Title, Createddate,filetype,createdBy.Name').
                                addConditionNotEq('IsSignature__c',true).
                                addConditionNotEq('IsBackgroundCheck__c',true).
                                addConditionIn('ContentDocumentId ', contentDocumentIdsmap.keyset()).
                                orderBy('Createddate', 'Desc').
                                run();   
         Map<ID,contentversion> oMapContentVersion= new Map<ID,contentversion>();
         for(contentversion oContVersion: oContentVersionList)
         {
            oMapContentVersion.put(oContVersion.ContentDocumentId,oContVersion);
         }

       
         for(Id key: oMapContentVersion.keySet())
         {
                id linkedId=contentDocumentIdsmap.get(key).LinkedEntityId;
             if(oAmpContDocLink.containsKey(linkedId)) {
  
                oAmpContDocLink.get(linkedId).add(oMapContentVersion.get(key));
            } else {
               
                oAmpContDocLink.put(linkedId, new List<contentversion> { oMapContentVersion.get(key) });
            }
         }
        }
        catch(Exception Ex)
        {}
        return oAmpContDocLink;
    }
 
    //This function used to get the content document Id
    private static Map<Id, ContentDocumentLink> getMapOfContentDocumentIDFromLinkedEntityID(String strDocumentID)
    {
        List<String> fieldList = strDocumentID.split(', *');
        Map<Id, ContentDocumentLink> contentDocumentIdsmap = new Map<Id, ContentDocumentLink>();
        try{
            List<ContentDocumentLink> oContentDocumetLink= new DMLOperationsHandler('ContentDocumentLink').
                                                            selectFields('Id, ContentDocument.Description, ContentDocumentId, ContentDocument.LatestPublishedVersionId,LinkedEntityId,LinkedEntity.Name').
                                                            addConditionIn('LinkedEntityId ', fieldList).
                                                            run();            
            for(ContentDocumentLink oContentDoc : oContentDocumetLink){
            contentDocumentIdsmap.put(oContentDoc.ContentDocumentId, oContentDoc);
            }      
         }
         catch(Exception Ex)
         {}
         return contentDocumentIdsmap;
    }
   //To get the Record ID by recordtype name
    public static Id getRecordTypeIdbyName(String objectName, String strRecordTypeName)
    {
        return  Schema.getGlobalDescribe().get(objectName).getDescribe().getRecordTypeInfosByName().get(strRecordTypeName).getRecordTypeId();
    }
    //To get the Record Name by RecordID
    public static String getRecordTypeNameById(String objectName, Id strRecordTypeId)
    {
        return Schema.getGlobalDescribe().get(objectName).getDescribe().getRecordTypeInfosById().get(strRecordTypeId).getName();
    }  
}
