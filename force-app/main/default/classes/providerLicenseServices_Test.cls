/**
     * @Author        : Sindhu Venkateswarlu
     * @CreatedOn     : May 11,2020
     * @Purpose       :Test Methods to unit test for providerLicenseServices.cls
     **/
@isTest
public class providerLicenseServices_Test {
                 @isTest static void testgetLicenseDetails(){
                 LicenseServices__c license = new LicenseServices__c();
                     insert license;
                 Contract__c contr = new Contract__c();
                     insert contr;
                     try{
                        providerLicenseServices.getLicenseDetails(null);
                      }catch(Exception Ex){
            
                        }
                     providerLicenseServices.getLicenseDetails('abc');
                     try{
                        providerLicenseServices.editLicenseDetails(null);
                      }catch(Exception Ex){
            
                        }
                     providerLicenseServices.editLicenseDetails(license.Id);
                     try{
                        providerLicenseServices.getContractLicenseStatus(null);
                      }catch(Exception Ex){
            
                        }
                     providerLicenseServices.getContractLicenseStatus(contr.Id);

                 }

}