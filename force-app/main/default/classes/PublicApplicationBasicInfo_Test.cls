/**
 * @Author        : Pratheeba.V
 * @CreatedOn     : June 8 ,2020
 * @Purpose       : Test Methods to uni test PublicApplicationBasicInfo class
**/

@IsTest
private class PublicApplicationBasicInfo_Test {
    @IsTest static void testPublicApplicationBasicInfoPositive(){
        Application__c InsertobjNewApplication=new Application__c(Status__c = 'Pending'); 
        insert InsertobjNewApplication;

        try {
            PublicApplicationBasicInfo.getPublicApplicationBasicInfo('');
        } catch(Exception e) {}
        PublicApplicationBasicInfo.getPublicApplicationBasicInfo(InsertobjNewApplication.Id);
        PublicApplicationBasicInfo.getReferenceValueData();
    }

    @IsTest static void testPublicApplicationBasicInfoException(){
        try
        {
        PublicApplicationBasicInfo.getPublicApplicationBasicInfo('');
             } catch(Exception e) {}
    }
}
