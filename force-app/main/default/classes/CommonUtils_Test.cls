/*
@Author        : sathishkumar G
@CreatedOn     : July 28,2020
@Purpose       : Test Methods to unit test CommonUtils_Test class
@Updated By    : 

*/
@isTest
public with sharing class CommonUtils_Test {

    @isTest
    public static void testGetCurrentTimeUsingDate() {
        CommonUtils.getcurrentDateTime();
    }
    @isTest static void testCommonUtils() {
           Application__c InsertobjNewApplication=new Application__c(Status__c = 'Pending'); 
           insert InsertobjNewApplication;

           List<ContentVersion> cvList = TestDataFactory.createTestContentVersionData();
           insert cvList;

        try{

           List<DMLOperationsHandler.FetchValueWrapper>  objgetPickListValues = CommonUtils.getPickListValues(InsertobjNewApplication,'XXX');
         }catch(Exception e){
                }   
                
       

                    try{
                    CommonUtils.getUploadedDocuments (null); 
                    }
                    catch(Exception Ex){

                    }
                        CommonUtils.getUploadedDocuments (InsertobjNewApplication.Id); 

                        try {
                        CommonUtils.getStateDetails ();
                        } catch (Exception Ex) {
                                    
                            }
                        

                            try {
                            CommonUtils.getLoginUserSignature (null);
                            } catch (Exception Ex) {
                                
                            }
                                CommonUtils.getLoginUserSignature (InsertobjNewApplication.Id);


                                try {
                                CommonUtils.saveSign ('',null);
                                } catch (Exception Ex) {
                                    
                                }
                                    CommonUtils.saveSign ('123',InsertobjNewApplication.Id);
                                }   
                                        
        @isTest
        public static void testGetRecordTypeIdbyName() {
            Case CountCase3= new Case(Status = 'Rejected');
            insert CountCase3;
            Id MyId2=Schema.Sobjecttype.Case.getRecordTypeInfosByName().get('Complaints').getRecordTypeId();
            CountCase3.RecordTypeId = MyId2;
            CommonUtils.getRecordTypeIdbyName ('Case', 'Complaints');
            CommonUtils.getRecordTypeNameById('Case', CountCase3.RecordTypeId);
        }
    
        @IsTest
        public static void TestLinkedEntity(){
             Application__c InsertobjNewLicenseInformation=new Application__c(ProgramType__c = 'XYZ');
           Database.UpsertResult Upsertresults = Database.upsert(InsertobjNewLicenseInformation);
           string strAppId= Upsertresults.getId();
             //Create content version
             ContentVersion contentVersion = new ContentVersion(
               Title = 'Cardinality',
               PathOnClient = 'Cardinality.jpg',
               VersionData = Blob.valueOf('ApplicationLicenseInformation_Test')
             );
             insert contentVersion;
             List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
             //create ContentDocumentLink  record
             ContentDocumentLink cdl = New ContentDocumentLink();
             cdl.LinkedEntityId = strAppId;
             cdl.ContentDocumentId = documents[0].Id;
             insert cdl;
            
            Set<id> oSet=new Set<id>{strAppId};
             Map<Id,contentversion[]> oMap= CommonUtils.getUploadedDocumentsByLinkedEntityID(oSet);
            List<contentversion> oList= CommonUtils.getUploadedDocuments(documents[0].Id);
            try
            {
               Map<Id,contentversion[]> oMap1= CommonUtils.getUploadedDocumentsByLinkedEntityID(null);
            }catch(Exception e){}
            
            try
            {
              CommonUtils.saveSign('',null);
            }catch(Exception e){}
            
        }
    }