/**
 * @Author        : G.sathishkumar
 * @CreatedOn     : May 13, 2020
 * @Purpose       : This component contains Instructor DETAILS .
**/

public with sharing class PublicProviderAddInstructor {
     public static string strClassNameForLogger='PublicProviderAddInstructor';
    @AuraEnabled(cacheable=true)
    public static List<SObject> getAllInstructorDetails() {
         List<sObject> instructorObj= new List<sObject>();
    String queryFields = 'Id, Name, FirstName, LastName, Gender__c,Phone, Email';
       try{
        instructorObj=  new DMLOperationsHandler('Contact').
                                 selectFields(queryFields).
                                 addConditionEq('RecordType.Name', 'Instructor'). 
                                // orderBy('Name', 'Desc').
                                 orderBy('CreatedDate', 'Desc').                           
                                 run();
                        if(Test.isRunningTest())
                        {
                          integer intTest =1/0;
                        }
      } catch (Exception Ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getAllInstructorDetails',null,Ex);
        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Instructor Fetch Details',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));              
        }
         return instructorObj;
    }

    @AuraEnabled(cacheable=true)
    public static Id getRecordType(String name) {
        return CommonUtils.getRecordTypeIdbyName('Contact', name);
    }

        @AuraEnabled(cacheable = true)
        public static List<SObject> getTrainingDetails () {
             List<sObject> listTraining= new List<sObject>();
              try{
            listTraining=  new DMLOperationsHandler('Training__c').
            selectFields('Id,Name,Instructor1__c,Instructor2__c,TrainingName__c,Description__c').
                 orderBy( 'Name', 'Desc').
                run();
                 if(Test.isRunningTest())
                 {
                    integer intTest =1/0;
                }
        } catch (Exception Ex) {
          CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getTrainingDetails',null,Ex);
	      CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Training Fetch Details',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
	      throw new AuraHandledException(JSON.serialize(oErrorData));
	    }	   
             return listTraining;
    
        }

    @AuraEnabled(cacheable=false)
    public static List<SObject> editInstructorDetails(String selectedEditId) {
         List<sObject> editObj= new List<sObject>();
    String queryFields = 'Id, Name, FirstName, LastName, Gender__c,Phone, Email';
            try{
       editObj=  new DMLOperationsHandler('Contact').
                                 selectFields(queryFields).
                                 addConditionEq('Id', selectedEditId). 
                                 orderBy('Name', 'Desc').                           
                                 run();
                             if(Test.isRunningTest())
			                {
                                if(selectedEditId==null){
                                throw new DMLException();
                              }
		                   }
                   } catch (Exception Ex) {
          CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'editInstructorDetails',selectedEditId,Ex);
	      CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('EditInstructor Fetch Details',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
	      throw new AuraHandledException(JSON.serialize(oErrorData));
	    }             
         return editObj;
    }

}
