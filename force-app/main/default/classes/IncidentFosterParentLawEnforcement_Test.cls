    /*
    Author         : Balamurugan B
    @CreatedOn     : Aug 18 ,2020
    @Purpose       : test class for IncidentFosterParentLawEnforcement Details .
    */
    @isTest
    public with sharing class IncidentFosterParentLawEnforcement_Test {
         @isTest static void IncidentFosterParentLawEnforcement_Test() {

            Actor__c InsertobjNewHHM=new Actor__c(Role__c = 'test Account');
            insert InsertobjNewHHM;

            Case InsertobjCase=new Case(Status = 'Pending'); 
            insert InsertobjCase;

            Address__c add = new Address__c();
            insert add;

            CPAHome__c CPAHomeDetails = new CPAHome__c(Status__c = 'Pending');
            insert CPAHomeDetails;

            ProviderRecordQuestion__c Prvrq = new ProviderRecordQuestion__c(Status__c = 'Pending',Actor__c=InsertobjNewHHM.Id);
        insert Prvrq;
        
        ProviderRecordAnswer__c prvans = new ProviderRecordAnswer__c();    
        insert prvans;

        string str = '[{"Comar__c":"","Comments__c":"Test Comments","ProviderRecordQuestion__c":"'+Prvrq.Id+'"}]';
                string strUpdate = '[{"Id":"'+prvans.Id+'","Comments__c":"Test Comments","ProviderRecordQuestion__c":"'+Prvrq.Id+'"}]';

        try {
            IncidentFosterParentLawEnforcement.incidentPickListValueFromReferenceValue('');
        } catch (Exception Ex) {
        }
        IncidentFosterParentLawEnforcement.incidentPickListValueFromReferenceValue('IncidentType');

        try {
            IncidentFosterParentLawEnforcement.getNarrativeQuestionsFromReference();
        } catch (Exception Ex) {

        }
    
      
        IncidentFosterParentLawEnforcement.getRecordTypeValue('Law Enforcement');
        IncidentFosterParentLawEnforcement.getIncidentRecordTypeValue('Incident');

        // try {
        //     IncidentFosterParentLawEnforcement.getIncidentRecordTypeValue('');
        // } catch (Exception Ex) {
        // }
        // IncidentFosterParentLawEnforcement.getIncidentRecordTypeValue(InsertobjCase.Id);

        try {
            IncidentFosterParentLawEnforcement.getReferralData('');
        } catch (Exception Ex) {
        }
        IncidentFosterParentLawEnforcement.getReferralData (InsertobjCase.Id);

        try {
            IncidentFosterParentLawEnforcement.insertAnswerToProviderAnswer(null);
        } catch (Exception Ex) {
        }
        IncidentFosterParentLawEnforcement.insertAnswerToProviderAnswer(str);

        

        try {
            IncidentFosterParentLawEnforcement.getFosterParentData(null);
        } catch (Exception Ex) {
        }
        IncidentFosterParentLawEnforcement.getFosterParentData(CPAHomeDetails.Id);

        

        try {
            IncidentFosterParentLawEnforcement.getAddressDetail('',null);
        } catch (Exception Ex) {
        }
        IncidentFosterParentLawEnforcement.getAddressDetail('Incident',InsertobjCase.Id);


        try {
            IncidentFosterParentLawEnforcement.fosterParentTableData(null);
        } catch (Exception Ex) {
        }
        IncidentFosterParentLawEnforcement.fosterParentTableData(InsertobjCase.Id);


        try {
            IncidentFosterParentLawEnforcement.getLawEnforcementDataTableData(null);
        } catch (Exception Ex) {
        }
        IncidentFosterParentLawEnforcement.getLawEnforcementDataTableData(InsertobjCase.Id);

        try {
            IncidentFosterParentLawEnforcement.getNarrativeAnswerForEdit(null);
        } catch (Exception Ex) {
        }
        IncidentFosterParentLawEnforcement.getNarrativeAnswerForEdit(InsertobjCase.Id);

        try
        {
            IncidentFosterParentLawEnforcement.bulkUpdateRecordAns(null);
            
        }catch(Exception Ex)
        {
        }
        IncidentFosterParentLawEnforcement.bulkUpdateRecordAns(strUpdate);

        }
        }
