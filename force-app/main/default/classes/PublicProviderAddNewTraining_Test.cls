/**
     * @Author        : Balamurugan.B
     * @CreatedOn     : June 07,2020
     * @Purpose       :Test Methods to unit test for PublicProviderAddNewTraining.cls
     **/


    @isTest
    public class PublicProviderAddNewTraining_Test {
            @isTest static void testAddNewTraining(){
                Account InsertobjNewAccount=new Account(Name = 'test Account');
        	insert InsertobjNewAccount;
          // TestDataFactory.createTestContacts(1,InsertobjNewAccount.id,true);
           ReferenceValue__c InsertobjNewRef=new ReferenceValue__c();
           insert InsertobjNewRef;
           Training__c InsertobjNewTrain=new Training__c();
           insert InsertobjNewTrain;
           MeetingInfo__c meetinginfo_Obj = new MeetingInfo__c(Duration__c = 'Durat218',Training__c=InsertobjNewTrain.id);
           Insert meetinginfo_Obj; 
           try{
         PublicProviderAddNewTraining.getInstructorDetails();
           }catch(Exception Ex){

          }
          try{
            PublicProviderAddNewTraining.getStateDetails();
          }catch(Exception Ex){

            }
            try{
              PublicProviderAddNewTraining.getRecordType(null);
            }catch(Exception Ex){
  
              }
        PublicProviderAddNewTraining.getRecordType('Private');

        try{
          PublicProviderAddNewTraining.editOrViewTrainingDetails(null);
        }catch(Exception Ex){

          }
    PublicProviderAddNewTraining.editOrViewTrainingDetails(InsertobjNewTrain.Id);

    try{
      PublicProviderAddNewTraining.getMeetingInfoDetails(null);
    }catch(Exception Ex){

      }
PublicProviderAddNewTraining.getMeetingInfoDetails(meetinginfo_Obj.Id);

        try{
            string strUpdate = '[{"Training__c":"InsertobjNewTrain.Id", "MeetingStatus__c":"Completed","Date__c":"2020-05-21","Duration__c":"12","SessionNumber__c":"Session 1","StartTime__c":"00:15:00.000Z","EndTime__c":"13:30:00.000Z"}]';
        
          PublicProviderAddNewTraining.insertOrUpdateMeetingInfoDetails(strUpdate,InsertobjNewTrain.Id);
        } catch(Exception Ex){

        }
            }}