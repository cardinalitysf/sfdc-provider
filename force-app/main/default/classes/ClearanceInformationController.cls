/**
 * Author : Pratheeba.V
 * Purpose : Clearance Information Tab
 */
public with sharing class ClearanceInformationController {
    public static string strClassNameForLogger='ClearanceInformationController';
    @AuraEnabled
    public static Id getIdAfterInsertSOQL(sObject objIdSobjecttoUpdateOrInsert) {
        return DMLOperationsHandler.updateOrInsertSOQLReturnId(objIdSobjecttoUpdateOrInsert);
    }

    @AuraEnabled
    public static List<Contact> getContactList(string accountId) {
        List<Contact> contactList = new List<Contact>();
        try {
            contactList = [SELECT Id, AccountId, CIRequestDate__c,CIResponseDate__c,CIRCC__c,CICPA__c,SCRequestDate__c,SCResponseDate__c,SCRCC__c,SCCPA__c,
                FCRequestDate__c,FCResponseDate__c,FCRCC__c,FCCPA__c,ProgramType__c FROM Contact WHERE Id = :accountId];
            if(Test.isRunningTest()) {
                if(accountId=='') {
                    throw new DMLException();
                }				
            }
        } catch (Exception Ex) { 
            String[] arguments = new String[] {accountId};
            string strInputRecord= String.format('Input parameters are :: accountId -- {0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getContactList',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Contact List',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return contactList;
    }
}