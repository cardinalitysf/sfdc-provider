/**
 * @Author        : G.Tamilarasan 
 * @CreatedOn     : March 03, 2020
 * @Purpose       : This component contains CASE console
 * @UpdatedBy     : Nandhini
 **/

public with sharing class referralProviderHeaderController {

    //This function used to get the case header details

    @AuraEnabled(cacheable=true)
    public static List<Case> getCaseHeaderDetails(string newreferral) {
        List<sObject> reviewObj = new List<sObject>();
        String queryFields = 'Id, Owner.Name, CaseNumber, Origin, CreatedDate, Agency__c, ReceivedDate__c, OwnerId, Status';
        try {
            reviewObj = new DMLOperationsHandler('Case').
            selectFields(queryFields).
            addConditionEq('Id', newreferral).
            run();
            if(Test.isRunningTest())
			{
                if(newreferral==null){
                    throw new DMLException();
                }
			}
        }
        catch (Exception ex) {
            String inputParametergetcaseheaderDetails='Inputs parameter get values of Case Header Details ::'+newreferral;
            CustomAuraExceptionData.LogIntoObject('referralProviderHeaderController','getCaseHeaderDetails',inputParametergetcaseheaderDetails,ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Case Header Details',ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return reviewObj;
    }

    //Function used to fetch communication picklist values from object

    @AuraEnabled(cacheable=true)
    public static List<DMLOperationsHandler.FetchValueWrapper> getPickListValues(sObject objInfo, string picklistFieldApi) {
        return DMLOperationsHandler.fetchPickListValue(objInfo,picklistFieldApi);
    }

    //Function used to insert the New Case

    @AuraEnabled
    public static Id getIdAfterInsertSOQL(sObject objIdSobjecttoUpdateOrInsert) {
        return DMLOperationsHandler.getIdAfterInsertSOQL(objIdSobjecttoUpdateOrInsert);
    }

}