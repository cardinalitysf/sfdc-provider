/**
 * Author   : Jayachandran s
 * Date     : 5-8-2020
 * @Purpose : Incident PersonKidChildInfo With Class methods(CaseOBJ)
 */
public with sharing class IncidentPersonKidChildInfo {

    public static string strClassNameForLogger='IncidentPersonKidChildInfo';

    @AuraEnabled
    public static string InsertUpdateIncident(SObject saveObjInsert){
        return DMLOperationsHandler.updateOrInsertSOQLReturnId(saveObjInsert);
    }

    @AuraEnabled(cacheable=false)
    public static List<Actor__c> getIncidentPersonChildDetails(string referID) {
    Set<String> fields = new Set<String>{'Id','Contact__r.Id','Address__r.Id','Contact__r.LastName', 'Contact__r.IdentifierNumber__c', 'Contact__r.AdmittingCharge__c' , 'RoleinIncident__c', 'DurationofPhysicalRestraint__c', 'Assign__c','StaffInvolved__c'};
    List<sObject> incidentdata = new List<sObject>();
    try{
    incidentdata= new DMLOperationsHandler('Actor__c').
     selectFields(fields).
     addConditionEq('referral__c', referID).
     addConditionEq('Role__c','Person/Kid').
     orderBy('Name', 'Desc').   
    run();
    if(Test.isRunningTest()) {
        if(referID==null){
            throw new DMLException();
        }
    } 
    }catch (Exception ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getincidentinfodetails',referID + 'Referalnid', ex);
        //To return back to the UI
        CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Sorry,Failed to load IncidentPersonChild Details ,Please Try Again..!', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
    }
    return incidentdata;
    }

@AuraEnabled(cacheable=true)
    public static Id getRecordType(String name) {
        return CommonUtils.getRecordTypeIdbyName('Contact', name);
    }

@AuraEnabled(cacheable=false)
public static List<Actor__c> EditIncidentPersonChildDetails(string ID) {
Set<String> fields = new Set<String>{'Id', 'Contact__r.FirstName__c','Contact__r.LastName__c', 'Contact__r.DOB__c', 'Contact__r.IdentifierNumber__c', 'Contact__r.AdmittingCharge__c', 'Contact__r.PlacingAgency__c', 'Address__r.AddressLine1__c', 'Address__r.AddressLine2__c', 'Address__r.State__c', 'Address__r.City__c', 'Address__r.County__c', 'Address__r.ZipCode__c', 'Assign__c', 'RoleinIncident__c', 'PhysicalRestrainedType__c', 'DurationofPhysicalRestraint__c', 'MechanicalRestrainedType__c', 'DurationofMechanicalRestraint__c', 'StaffInvolved__c', 'DeescalationEffortMade__c', 'InjurySustained__c', 'InjurySeverityLevel__c', 'InjuryResultingfromRestraint__c', 'SeenByMedical__c', 'Seclusion__c', 'DurationofSeclusion__c','PhysicalRestraintReason__c'};
List<sObject> incidentdata = new List<sObject>();
try{
incidentdata= new DMLOperationsHandler('Actor__c').
 selectFields(fields).
 addConditionEq('Id', ID).
 orderBy('Name', 'Desc').   
run();
if(Test.isRunningTest()) {
    if(ID==null){
        throw new DMLException();
    }
} 
}catch (Exception ex) {
    //To Log into object
    CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'EditIncidentPersonChildDetails',ID + 'Actorid', ex);
    //To return back to the UI
    CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Sorry,Failed to load IncidentPersonChild Details ,Please Try Again..!', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
    throw new AuraHandledException(JSON.serialize(oErrorData));
}
return incidentdata;
}

@AuraEnabled(cacheable=true)
    public static List<SObject> getStaffMembers(String applicationId) {
        List<sObject> staffMembers = new List<sObject>();
        try {
            String fields = 'Id, Staff__c, Staff__r.FirstName__c, Staff__r.LastName__c, Staff__r.LastName';
            staffMembers = new DMLOperationsHandler('Application_Contact__c').
                                selectFields(fields).
                                addConditionEq('Application__c', applicationId).               
                                run();
                                if(Test.isRunningTest()) {
                                    if(applicationId==null){
                                        throw new DMLException();
                                    }
                                } 
        } catch (Exception ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getStaffMembers','Input parameters are :: applicationId' + applicationId ,ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in Fecthing Staff details','Unable to get Staff details, Please try again' , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }           
        return staffMembers;
    } 

}