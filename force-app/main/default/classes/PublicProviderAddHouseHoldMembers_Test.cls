/**
 * @Author        : Preethi Bojarajan
 * @CreatedOn     : May 18, 2020
 * @Purpose       : Test Class for PublicProviderAddHouseHoldMembers
 * @updatedBy     : 
 * @updatedOn     : 
 **/
@IsTest
private class PublicProviderAddHouseHoldMembers_Test{
    @isTest
    private static void getRecordType_Test(){
        Test.startTest();
        try {
            PublicProviderAddHouseHoldMembers.getRecordType('Household Members');
        } catch(Exception e){}
        Test.stopTest();
    }

    @isTest
    private static void fetchHouseHoldMemberDetails_Test() {
        Test.startTest();
        try {
            RecordType rtype = [Select Name, Id From RecordType 
                  where sObjectType='Contact' and Name = 'Household Members' and isActive=true];
            List<Account> lstAcct= TestDataFactory.testAccountData();
            insert lstAcct;
        
            for(Account acct : lstAcct) {
                
            }
            string strCseID='';
            // Insert records into cases object  which are approved 
            List<Case> lstCase=TestDataFactory.TestCaseData(1,'Approved');
            List<Case> csList = new List<Case>();
            for(Case cse: lstCase) {
                cse.AccountId = lstAcct[0].Id;
                cse.status = 'Approved';
                csList.add(cse);
            }
            insert csList;
        
            List<Contact> conList = TestDataFactory.createTestContacts(5,lstAcct[0].Id,true);
            List<Contact> contList = new List<Contact>();
            for(Contact con: conList ) {
                con.RecordTypeId = rtype.Id;
                contList.add(con);
            }
            update contList;
            List<ContentVersion> cvList = TestDataFactory.createTestContentVersionData();
            insert cvList;

            List<SObject> sobjList = new List<SObject>();
            String str = 'Contact';
            //Casting String to sObject
            sObject obj = Schema.getGlobalDescribe().get(str).newSObject();
            obj.put('FirstName', 'test');
            obj.put('LastName', 'Caseworker Submitted');
            obj.put('Phone', '0123456789');
            obj.put('Id', contList[0].Id);
            obj.put('AccountId', contList[0].AccountId);
            obj.put('email','test@gmail.com');
            obj.put('MailingCountry','US');
            obj.put('MailingStreet','Street');
            obj.put('MailingCity','city');
            obj.put('MailingState','State');
            obj.put('MailingPostalCode','11235');
            obj.put('Comments__c','1');       
            sobjList.add(obj);
        
            ContentVersion cVersion = new ContentVersion();
            String strProfileImg = 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8z/C/HgAGgwJ/lK3Q6wAAAABJRU5ErkJggg==';
            cVersion.ContentLocation = 'S';
            cVersion.PathOnClient = 'ProfilePic-'+System.now() +'.png';
            cVersion.Origin = 'H';

            cVersion.Title = 'ProfilePic-'+System.now() +'.png';
            cVersion.VersionData = EncodingUtil.base64Decode(strProfileImg);
            cVersion.IsSignature__c = false;
            cVersion.IsMajorVersion = false;
            Insert cVersion;

            Id conDocument = [SELECT ContentDocumentId  FROM ContentVersion WHERE Id =:cVersion.Id].ContentDocumentId;
        
            ContentDocumentLink cDocLink = new ContentDocumentLink();
            cDocLink.ContentDocumentId = conDocument;
            cDocLink.LinkedEntityId = contList[0].Id;
            cDocLink.ShareType = 'I';
            cDocLink.Visibility = 'AllUsers';
            Insert cDocLink;
        
            PublicProviderAddHouseHoldMembers.fetchHouseHoldMemberDetails(contList[0].Id);
            PublicProviderAddHouseHoldMembers.fetchHouseHoldMemberDetails('');
            PublicProviderAddHouseHoldMembers.fetchHouseHoldMemberDetails(null);
        } catch(Exception e){}
        Test.stopTest();
    }

    @isTest
    private static void fetchHouseHoldMemberFrmActorDetails_Test(){
        Test.startTest();
        try {
            RecordType rtype = [Select Name, Id From RecordType 
                  where sObjectType='Contact' and Name = 'Household Members' and isActive=true];
            List<Account> lstAcct= TestDataFactory.testAccountData();
            insert lstAcct;
        
            for(Account acct : lstAcct) {
                
            }
            string strCseID='';
            // Insert records into cases object  which are approved 
            List<Case> lstCase=TestDataFactory.TestCaseData(1,'Approved');
            List<Case> csList = new List<Case>();
            for(Case cse: lstCase) {
                cse.AccountId = lstAcct[0].Id;
                cse.status = 'Approved';
                csList.add(cse);
            }
            insert csList;
        
            List<Contact> conList = TestDataFactory.createTestContacts(5,lstAcct[0].Id,true);
            List<Contact> contList = new List<Contact>();
            for(Contact con: conList ) {
                con.RecordTypeId = rtype.Id;
                contList.add(con);
            }
            update contList;
            List<ContentVersion> cvList = TestDataFactory.createTestContentVersionData();
            insert cvList;

            List<SObject> sobjList = new List<SObject>();
            String str = 'Contact';
            //Casting String to sObject
            sObject obj = Schema.getGlobalDescribe().get(str).newSObject();
            obj.put('FirstName', 'test');
            obj.put('LastName', 'Caseworker Submitted');
            obj.put('Phone', '0123456789');
            obj.put('Id', contList[0].Id);
            obj.put('AccountId', contList[0].AccountId);
            obj.put('email','test@gmail.com');
            obj.put('MailingCountry','US');
            obj.put('MailingStreet','Street');
            obj.put('MailingCity','city');
            obj.put('MailingState','State');
            obj.put('MailingPostalCode','11235');
            obj.put('Comments__c','1');       
            sobjList.add(obj);
        
            ContentVersion cVersion = new ContentVersion();
            String strProfileImg = 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8z/C/HgAGgwJ/lK3Q6wAAAABJRU5ErkJggg==';
            cVersion.ContentLocation = 'S';
            cVersion.PathOnClient = 'ProfilePic-'+System.now() +'.png';
            cVersion.Origin = 'H';

            cVersion.Title = 'ProfilePic-'+System.now() +'.png';
            cVersion.VersionData = EncodingUtil.base64Decode(strProfileImg);
            cVersion.IsSignature__c = false;
            cVersion.IsMajorVersion = false;
            Insert cVersion;

            Id conDocument = [SELECT ContentDocumentId  FROM ContentVersion WHERE Id =:cVersion.Id].ContentDocumentId;
        
            ContentDocumentLink cDocLink = new ContentDocumentLink();
            cDocLink.ContentDocumentId = conDocument;
            cDocLink.LinkedEntityId = contList[0].Id;
            cDocLink.ShareType = 'I';
            cDocLink.Visibility = 'AllUsers';
            Insert cDocLink;
        
            PublicProviderAddHouseHoldMembers.fetchHouseHoldMemberFrmActorDetails('xxx',contList[0].Id,'Referral__c');
            PublicProviderAddHouseHoldMembers.fetchHouseHoldMemberFrmActorDetails('','','Referral__c');
            PublicProviderAddHouseHoldMembers.fetchHouseHoldMemberFrmActorDetails(null,'','Referral__c');
        } catch(Exception e){}
        Test.stopTest();
    }
    
    @isTest
    private static void getMultiplePicklistValues_Test() {
        Test.startTest();
        PublicProviderAddHouseHoldMembers.getMultiplePicklistValues('Contact','PrimaryCitizenship__c');
        Test.stopTest();
    }

    @isTest
    private static void saveProfilePic_Test(){
        Test.startTest();  
        RecordType rtype = [Select Name, Id From RecordType 
              where sObjectType='Contact' and Name = 'Household Members' and isActive=true];
    List<Account> lstAcct= TestDataFactory.testAccountData();
    insert lstAcct;
    
    for(Account acct : lstAcct)
    {
       
    }
   
    string strCseID='';
    // Insert records into cases object  which are approved 
    List<Case> lstCase=TestDataFactory.TestCaseData(1,'Approved');
    List<Case> csList = new List<Case>();
    for(Case cse: lstCase)
    {
        cse.AccountId = lstAcct[0].Id;
        cse.status = 'Approved';
        csList.add(cse);
    }
    
    insert csList;
    
    List<Contact> conList = TestDataFactory.createTestContacts(5,lstAcct[0].Id,true);
    List<Contact> contList = new List<Contact>();
    for(Contact con: conList )
    {
        con.RecordTypeId = rtype.Id;
        contList.add(con);
    }
    
    update contList;
    
    List<ContentVersion> cvList = TestDataFactory.createTestContentVersionData();
    insert cvList;

    
    List<SObject> sobjList = new List<SObject>();
    
    String str = 'Contact';
    //Casting String to sObject
    sObject obj = Schema.getGlobalDescribe().get(str).newSObject();
    obj.put('FirstName', 'test');
    obj.put('LastName', 'Caseworker Submitted');
    obj.put('Phone', '0123456789');
    obj.put('Id', contList[0].Id);
    obj.put('AccountId', contList[0].AccountId);
    obj.put('email','test@gmail.com');
    obj.put('MailingCountry','US');
    obj.put('MailingStreet','Street');
    obj.put('MailingCity','city');
    obj.put('MailingState','State');
    obj.put('MailingPostalCode','11235');
    obj.put('Comments__c','1');       
    sobjList.add(obj);
    
    ContentVersion cVersion = new ContentVersion();
    String strProfileImg = 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8z/C/HgAGgwJ/lK3Q6wAAAABJRU5ErkJggg==';
    cVersion.ContentLocation = 'S';
    cVersion.PathOnClient = 'ProfilePic-'+System.now() +'.png';
    cVersion.Origin = 'H';

    cVersion.Title = 'ProfilePic-'+System.now() +'.png';
    cVersion.VersionData = EncodingUtil.base64Decode(strProfileImg);
    cVersion.IsSignature__c = false;
    cVersion.IsMajorVersion = false;
    Insert cVersion;

    Id conDocument = [SELECT ContentDocumentId  FROM ContentVersion WHERE Id =:cVersion.Id].ContentDocumentId;
    
    ContentDocumentLink cDocLink = new ContentDocumentLink();
    cDocLink.ContentDocumentId = conDocument;
    cDocLink.LinkedEntityId = contList[0].Id;
    cDocLink.ShareType = 'I';
    cDocLink.Visibility = 'AllUsers';
    Insert cDocLink;      
        PublicProviderAddHouseHoldMembers.saveProfilePic('test',contList[0].Id);
        PublicProviderAddHouseHoldMembers.updateProfilePic('test 1',cvList[0].Id);
        PublicProviderAddHouseHoldMembers.getRefContactExistDet(csList[0].Id);
        Test.stopTest();
    }
}