/**
 * @Author        : Preethi Bojarajan
 * @CreatedOn     : March 16,2020
 * @Purpose       : Provider's Applications List with Edit 
 * @updatedBy     : Preethi Bojarajan
 * @updatedOn     : Apr 27 ,2020
 **/
public with sharing class providerApplication {
    @AuraEnabled(cacheable=true)
    public static List<SObject> getDashboardDatas(String providerId, String type) {
        String model = '';
        String fields ='';
        String cond = '';
        if(type == 'capacityCount'){
            model = 'Application__c';
            fields ='SUM(Capacity__c)';
            String cond2 = 'Approved';
            cond = 'provider__C = \''+ providerId +'\'' +' and Status__c= \''+ cond2+'\'';
            /* List<SObject> provApp = new DMLOperationsHandler(model).
                                    selectFields(fields).
                                    addConditionEq('provider__C', providerId). 
                                    addConditionEq('Status__c', cond2).    
                                    run(); */
            return DMLOperationsHandler.selectSOQLWithConditionParameters(model,fields,cond);
        } else if(type == 'staffCount'){
            model = 'Contact';
            fields ='Id';    
            cond = 'AccountId = \''+ providerId +'\'';
            List<SObject> provApp = new DMLOperationsHandler(model).
                                    selectFields(fields).
                                    addConditionEq('AccountId', providerId).
                                    orderBy('Id', 'DESC'). 
                                    run();
            return provApp;
        } else {
            model = 'Application__c';
            fields ='Id, Name, Program__c, ProgramType__c, Status__c'; 
            cond = 'Provider__c = \''+ providerId +'\''+' ORDER BY Id DESC';
            List<SObject> provApp = new DMLOperationsHandler(model).
                                    selectFields(fields).
                                    addConditionEq('Provider__c', providerId). 
                                    orderBy('Id', 'DESC').
                                    run();
            return provApp;
        }        
    }

    @AuraEnabled(cacheable=true)
    public static List<SObject> getUserEmail(){
        String emailId = UserInfo.getUserEmail();
        String model = 'Account';
        String fields ='Id, Email__c'; 
        //String cond = 'Email__c  = \''+ emailId +'\'';
        List<SObject> userEmail = new DMLOperationsHandler(model).
                                selectFields(fields).
                                addConditionEq('Email__c', emailId). 
                                run();
        return userEmail;
    }

    @AuraEnabled(cacheable=true)
    public static List<WrapperTwoClass> getApplicationDet(String providerId){
        List<WrapperTwoClass> lstWrapper = new List<WrapperTwoClass>();
        String model = 'Application__c';
        String fields ='Id, Name, Program__c, ProgramType__c, Status__c'; 
        List<Application__c> applications = new DMLOperationsHandler(model).
                                selectFields(fields).
                                addConditionEq('Provider__c', providerId).
                                orderBy('Id', 'DESC').
                                run();
        
        String modelContract = 'Contract__c';
        String fieldsCont ='Id, License__r.Application__c, ContractStartDate__c, ContractEndDate__c, TotalContractBed__c'; 
        String type= 'Active';
        List<Contract__c> contracts = new DMLOperationsHandler(modelContract).
                                selectFields(fieldsCont).
                                addConditionEq('Provider__c', providerId).
                                addConditionEq('ContractStatus__c', type).
                                orderBy('Id', 'DESC').
                                run();
        lstWrapper.add(new WrapperTwoClass(applications, contracts)); 
        return lstWrapper;
    }

        public class WrapperTwoClass {
            @AuraEnabled public List<SObject> applications;
            @AuraEnabled public List<SObject> contracts;
        public WrapperTwoClass(List<SObject>applications, List<SObject>contracts) {
            this.applications = applications;
            this.contracts = contracts;
        }
    }

}