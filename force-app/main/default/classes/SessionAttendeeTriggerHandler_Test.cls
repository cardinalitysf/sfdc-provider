/*
@Author        : Naveen
@CreatedOn     : June 11 ,2020
@Purpose       : Test Methods to unit test SessionAttendeeTriggerHandler  class
@Updated By    : Sindhu on June 29

*/

@IsTest

public with sharing class SessionAttendeeTriggerHandler_Test {
  @IsTest
  static void setupTestSessionData(){
      
      test.startTest();
      
      Application__c app= new Application__c(Status__c='Draft'); 
      insert app;
     
      Case cse= new Case(Status = 'New',ProgramType__c='ABC,DEF,HIJ');
      insert cse;
      
      Training__c Tran=new Training__c(Type__c='Pre-Service Training');
      insert Tran;
     
      Training__c Trai =new Training__c(Type__c='Information Meeting');
      insert Trai;
     
    
      
    
    // If Taining type is Information Meeting
      
      List<SessionAttendee__c> sessionList = new List<SessionAttendee__c>();
      SessionAttendee__c InsSessionttendee= new SessionAttendee__c(Application__c=app.Id,Training__c=Tran.Id,Referral__c=cse.id);
      sessionList.add(InsSessionttendee);
      insert InsSessionttendee;    
     
      InsSessionttendee.SessionStatus__c=ProviderConstants.MEETINGINFO_DNA;
       update InsSessionttendee;
      InsSessionttendee.SessionStatus__c=ProviderConstants.MEETINGINFO_SCHEDULED;
        update InsSessionttendee;
      InsSessionttendee.SessionStatus__c=ProviderConstants.MEETINGINFO_CANCELLED;      
      	update InsSessionttendee;
     InsSessionttendee.SessionStatus__c=ProviderConstants.MEETINGINFO_COMPLETED;
       update InsSessionttendee;
      
// If Taining type is Pre-Service Training      
      List<SessionAttendee__c> sessionList1 = new List<SessionAttendee__c>();
      SessionAttendee__c InsSessionttendee1= new SessionAttendee__c(Application__c=app.Id,Training__c=Trai.Id);
      sessionList1.add(InsSessionttendee1);
      insert InsSessionttendee1;
      InsSessionttendee1.SessionStatus__c=ProviderConstants.MEETINGINFO_DNA; 
        update InsSessionttendee1;
      InsSessionttendee1.SessionStatus__c=ProviderConstants.MEETINGINFO_SCHEDULED;  
        update InsSessionttendee1;
      InsSessionttendee1.SessionStatus__c=ProviderConstants.MEETINGINFO_CANCELLED;
      update InsSessionttendee1;
      
      InsSessionttendee1.SessionStatus__c=ProviderConstants.MEETINGINFO_COMPLETED;     
      update InsSessionttendee1;
      
    //To test delete and Undelete  
      delete InsSessionttendee1;
      undelete InsSessionttendee1;
      test.stopTest();
  }
  
  
}