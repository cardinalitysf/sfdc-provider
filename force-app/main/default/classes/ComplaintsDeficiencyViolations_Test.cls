/**
 * @Author        : G sathishkumar ,
 * @CreatedOn     : July 15,2020
 * @Purpose       : ComplaintsDeficiencyViolations.
 **/
@isTest
public  class ComplaintsDeficiencyViolations_Test {
    @isTest static void testComplaintsDeficiencyViolations() {

        Case InsertobjCase=new Case(Status = 'Pending');
            insert InsertobjCase;
        Deficiency__c insertobjNewDeficiency = new Deficiency__c(Deficiency__c='');
            insert InsertobjNewDeficiency;
       
          try{
              ComplaintsDeficiencyViolations.getDeficiencyViolation(null);
           }catch(Exception Ex)
           {
               
           }
        ComplaintsDeficiencyViolations.getDeficiencyViolation(InsertobjCase.Id);
       
       try{
             ComplaintsDeficiencyViolations.getDeficiencyCase(null);
           }catch(Exception Ex)
           {
               
           }
      ComplaintsDeficiencyViolations.getDeficiencyCase(InsertobjCase.Id);
       
       try{
              ComplaintsDeficiencyViolations.insertUpdateDeficiencyViolationsDetails(null);
           }catch(Exception Ex)
           {
               
           }
      ComplaintsDeficiencyViolations.insertUpdateDeficiencyViolationsDetails(InsertobjNewDeficiency);
        
        try{
             ComplaintsDeficiencyViolations.editDeficiencyViolationDetails(null);
           }catch(Exception Ex)
           {
               
           }
      ComplaintsDeficiencyViolations.editDeficiencyViolationDetails(InsertobjNewDeficiency.Id);
    }
}
