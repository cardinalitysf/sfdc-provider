         /**
         * @Author        : Naveen S
         * @CreatedOn     : July 29 ,2020
         * @Purpose       : Responsible for all the actions in Case Service object.
         **/
    @isTest(SeeAllData=false)
    private class caseTrigger_Test{
    private static testMethod void testUpdateCaseMethod() {
        List<Case> caseList = new List<Case>();
        Case testCase;
        for(Integer i=0; i<5 ;i++){
        testCase = new Case(Status = 'New',ProgramType__c='ABC,DEF,HIJ');
        caseList.add(testCase);
        }
        insert caseList;
        for(Case cas : caseList){
        cas.Status = 'Approved';
        }
        List<Case> case_Obj  =  [SELECT Id,Outcomes__c,Summary__c,Description from Case];
        List<Case> case_Obj1  =  [SELECT Id,Outcomes__c,Summary__c,Description from Case];
        System.assertEquals(true,case_Obj.size()>0);

        Map<Id, Case> newcaseMap = new Map<Id, Case>();   
        Map<Id, Case> oldcaseMap = new Map<Id, Case>();   

        newcaseMap.putAll(case_Obj);
        oldcaseMap.putAll(case_Obj1);        
        for(id oid : newcaseMap.keySet()){
        oldcaseMap.get(oid).Outcomes__c='Changed OutCome';
        }    

        CaseTriggerHandler obj01 = new CaseTriggerHandler();
        obj01.AfterUpdate(newcaseMap,oldcaseMap);
        HistoryTracking__c historytracking_Obj = new HistoryTracking__c(CreatedDate__c = Date.today(), FieldName__c = '14', NewValue__c = '15', ObjectName__c = 'Case', ObjectRecordId__c = '17', RichOldValue__c = '18', RichNewValue__c = '19');
        Insert historytracking_Obj;
        Test.startTest();
        update caseList;
        Test.stopTest();
    }
    private static testMethod void testDeleteCaseMethod() {
        List<Case> caseList = new List<Case>();
        Case testCase;
        for(Integer i=0; i<5 ;i++){
        testCase = new Case(Status = 'New',ProgramType__c='ABC,DEF,HIJ');
        caseList.add(testCase);
        }
        insert caseList;  
        Test.startTest();
        delete caseList;
        Test.stopTest();
    }

    private static testMethod void testUnDeleteCaseMethod() {
        List<Case> caseList = new List<Case>();
        Case testCase;
        for(Integer i=0; i<5 ;i++){
        testCase = new Case(Status = 'New',ProgramType__c='ABC,DEF,HIJ');
        caseList.add(testCase);
        }
        insert caseList;  
        delete caseList;
        Test.startTest();
        undelete caseList;
        Test.stopTest();
    }

    static testMethod void test_BeforeInsert_UseCase1(){
        List<Case> meetinginfo_Obj  =  [SELECT Id,Outcomes__c,Summary__c,Description from Case];
        System.assertEquals(false,meetinginfo_Obj.size()>0);
        CaseTriggerHandler obj01 = new CaseTriggerHandler();
        obj01.BeforeInsert(new List<Sobject>());
    }
    static testMethod void test_BeforeUpdate_UseCase1(){
        List<Case> meetinginfo_Obj  =  [SELECT Id,Outcomes__c,Summary__c,Description from Case];
        System.assertEquals(false,meetinginfo_Obj.size()>0);
        CaseTriggerHandler obj01 = new CaseTriggerHandler();
        obj01.BeforeUpdate(new Map<id,sObject>(),new Map<id,sObject>());
    }
    static testMethod void test_BeforeDelete_UseCase1(){
        List<Case> meetinginfo_Obj  =  [SELECT Id,Outcomes__c,Summary__c,Description from Case];
        System.assertEquals(false,meetinginfo_Obj.size()>0);
        CaseTriggerHandler obj01 = new CaseTriggerHandler();
        obj01.BeforeDelete(new Map<id,sObject>());
    }
    static testMethod void test_AfterInsert_UseCase1(){
        List<Case> meetinginfo_Obj  =  [SELECT Id,Outcomes__c,Summary__c,Description from Case];
        System.assertEquals(false,meetinginfo_Obj.size()>0);
        CaseTriggerHandler obj01 = new CaseTriggerHandler();
        obj01.AfterInsert(new List<Sobject>());
    }

    static testMethod void test_AfterDelete_UseCase1(){
        List<Case> meetinginfo_Obj  =  [SELECT Id,Outcomes__c,Summary__c,Description from Case];
        System.assertEquals(false,meetinginfo_Obj.size()>0);
        CaseTriggerHandler obj01 = new CaseTriggerHandler();
        obj01.AfterDelete(new Map<id,sObject>());
    }
    static testMethod void test_AfterUndelete_UseCase1(){
        List<Case> meetinginfo_Obj  =  [SELECT Id,Outcomes__c,Summary__c,Description from Case];
        System.assertEquals(false,meetinginfo_Obj.size()>0);
        CaseTriggerHandler obj01 = new CaseTriggerHandler();
        obj01.AfterUndelete(new List<Sobject>());
    }

    private static testMethod void testUpdateCaseMethodSummary() {
        List<Case> caseList = new List<Case>();
        Case testCase;
        for(Integer i=0; i<5 ;i++){
        testCase = new Case(Status = 'New',ProgramType__c='ABC,DEF,HIJ');
        caseList.add(testCase);
        }
        insert caseList;

        for(Case cas : caseList){
        cas.Status = 'Approved';
        }
        List<Case> case_Obj  =  [SELECT Id,Outcomes__c,Summary__c,Description from Case];
        List<Case> case_Obj1  =  [SELECT Id,Outcomes__c,Summary__c,Description from Case];
        System.assertEquals(true,case_Obj.size()>0);
        Map<Id, Case> newcaseMap = new Map<Id, Case>();   
        Map<Id, Case> oldcaseMap = new Map<Id, Case>();   
        newcaseMap.putAll(case_Obj);
        oldcaseMap.putAll(case_Obj1);        
        for(id oid : newcaseMap.keySet()){
        oldcaseMap.get(oid).Summary__c='Changed Summary';
        }    
        CaseTriggerHandler obj02 = new CaseTriggerHandler();
        obj02.AfterUpdate(newcaseMap,oldcaseMap);
        HistoryTracking__c historytracking_Obj = new HistoryTracking__c(CreatedDate__c = Date.today(), FieldName__c = '14', NewValue__c = '15', ObjectName__c = 'Case', ObjectRecordId__c = '17', RichOldValue__c = '18', RichNewValue__c = '19');
        Insert historytracking_Obj;
        Test.startTest();
        update caseList;
        Test.stopTest();
    }

    private static testMethod void testUpdateCaseMethodDesc() {
        List<Case> caseList = new List<Case>();
        Case testCase;
        for(Integer i=0; i<5 ;i++){
        testCase = new Case(Status = 'New',ProgramType__c='ABC,DEF,HIJ');
        caseList.add(testCase);
        }
        insert caseList;
        for(Case cas : caseList){
        cas.Status = 'Approved';
        }
        List<Case> case_Obj  =  [SELECT Id,Outcomes__c,Summary__c,Description from Case];
        List<Case> case_Obj1  =  [SELECT Id,Outcomes__c,Summary__c,Description from Case];
        System.assertEquals(true,case_Obj.size()>0);
        Map<Id, Case> newcaseMap = new Map<Id, Case>();   
        Map<Id, Case> oldcaseMap = new Map<Id, Case>();   
        newcaseMap.putAll(case_Obj);
        oldcaseMap.putAll(case_Obj1);        
        for(id oid : newcaseMap.keySet()){
        oldcaseMap.get(oid).Description='Changed Description';
        }  
        CaseTriggerHandler obj03 = new CaseTriggerHandler();
        obj03.AfterUpdate(newcaseMap,oldcaseMap);
        HistoryTracking__c historytracking_Obj = new HistoryTracking__c(CreatedDate__c = Date.today(), FieldName__c = '14', NewValue__c = '15', ObjectName__c = 'Case', ObjectRecordId__c = '17', RichOldValue__c = '18', RichNewValue__c = '19');
        Insert historytracking_Obj;
        Test.startTest();
        update caseList;
        Test.stopTest();
    }
    }