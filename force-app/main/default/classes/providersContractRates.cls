public with sharing class providersContractRates {
    
    @AuraEnabled(cacheable=false)
    
        public static List<sObject> getLicenseRatesDetails(string getLicenseRatesDetails) {
    
            String queryFields;
            List<sObject> licenseRatesObj = new List<sObject>();
            try {
              queryFields = 'id,LicenseType__c,Provider__c,RateCode__c,StartDate__c,EndDate__c,PerDiemRate__c,Monthly__c,Annual__c';
              licenseRatesObj = new DMLOperationsHandler('IRCRates__c').
                                         selectFields(queryFields).
                                         addConditionEq('LicenseType__c', getLicenseRatesDetails).
                                          run();
            if(Test.isRunningTest())
			{
            if(getLicenseRatesDetails==''){
                    throw new DMLException();
            }
			}
                                          
             } 
            catch (Exception ex) {
             String inputParameterForLicenseRatesDetails='Inputs Parameter License Rates Details ::'+getLicenseRatesDetails;
             CustomAuraExceptionData.LogIntoObject('providersContractRates','getLicenseRatesDetails',inputParameterForLicenseRatesDetails,ex);
             CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('License Rates Details',ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
             throw new AuraHandledException(JSON.serialize(oErrorData));
               
             }
            return licenseRatesObj;
        }
    }