/**
	 * @Author        : S. Jayachandran
	 * @CreatedOn     : June 22 ,2020
	 * @Purpose       : Responsible for datas PublicProvidersHouseholdMemberList(Contact Sobj).
   * @UpdatedBy     :Jayachnadran s For Updation test class 
   * Date           :July 20, 2020 
**/
//Initial data load from contact Household
public with sharing class PublicProvidersHouseholdMemberList {
  public static string strClassNameForLogger='PublicProvidersHouseholdMemberList';

    @AuraEnabled(cacheable=false)
      public static List<Actor__c > getContactHouseHoldMembersDetails(string providerId) {
         List<sObject> Contacts = new List<sObject>();

         try {
          Contacts =
          new DMLOperationsHandler('Actor__c').
         selectFields('Id, Contact__r.Title__c,Contact__r.FirstName__c,BackgroungCheckPercentage__c,TrainingPercentage__c,Contact__r.LastName__c, Contact__r.ApproximateAge__c,Contact__r.Gender__c, Contact__r.DOB__c, Contact__r.SSN__c,Contact__r.ProfilePercentage__c,Contact__r.Id, Contact__r.ContactNumber__c, Role__c').
         addConditionEq('Provider__c ', providerId).
         addConditionEq('Contact__r.RecordType.Name','Household Members').
         run();
         if(Test.isRunningTest())
	    		{
                if(providerId==null){
                    throw new DMLException();
                }
         }
           
         } catch (Exception Ex) {
          //To Log into object
           CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getContactHouseHoldMembersDetails',providerId+ 'providerId',Ex);
           //To return back to the UI
           CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Sorry,Failed to load HouseHoldMembersDetails list details ,Please Try Again..!',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
           throw new AuraHandledException(JSON.serialize(oErrorData));
         }
         
         return Contacts;
      }

        //Get images from Content Document based on the Contacts
        @AuraEnabled(cacheable=false)
         public static List<ContentDocumentLink> getContactImgAsBas64Image(String ContactID) {
                  List<String> fieldList = ContactID.split(', *');
                  List<ContentDocumentLink> oContentDocumetLink= new List<ContentDocumentLink>();
                  try {
                    oContentDocumetLink= new DMLOperationsHandler('ContentDocumentLink').selectFields('Id, ContentDocumentId,ContentDocument.LatestPublishedVersionId,LinkedEntityId,LinkedEntity.Name').
                    addConditionIn('LinkedEntityId ', fieldList).
                    run();
                    if(Test.isRunningTest())
		               	{
                      if(ContactID==''){
                        throw new DMLException();
                        }
                     }
                  }catch (Exception Ex) {
                    //To Log into object
                    CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getContactImgAsBas64Image',ContactID+ 'ContactID',Ex);
                    //To return back to the UI
                    CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Sorry,Failed to load HouseHoldmembers Images details ,Please Try Again..!',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
                    throw new AuraHandledException(JSON.serialize(oErrorData));
                  }
                  return oContentDocumetLink;
  }

      
}