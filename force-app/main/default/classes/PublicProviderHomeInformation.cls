/**
 * @Author        : Janaswini S
 * @CreatedOn     : May 15, 2020
 * @Purpose       : Public Provider's Home Information
 **/

public with sharing class PublicProviderHomeInformation {
    public static string strClassNameForLogger='PublicProviderHomeInformation';
        @AuraEnabled(cacheable=true)
    public static List<SObject> fetchHomeInformation(String id) {
        String fields ='Id,Name,ChildrenResiding__c, Bedrooms__c,AgencyName__c, LicensedAgency__c, LicensedProvider__c, LicensedProviderDetail__c, PoolLocated__c, SwimmingPool__c, WaterfrontProperty__c, Referral__c, Provider__c';
        List<sObject> homeInformation = new List<sObject>();
        try{
        homeInformation = new DMLOperationsHandler('HomeInformation__c').
                                        selectFields(fields).
                                        addConditionEq('Provider__c', id).
                                        run();
                                        if(Test.isRunningTest())
                                        {
                                            if(id==null ){
                                                throw new DMLException();
                                            }
                                            
                                        }
                                } catch (Exception ex) {
                                    CustomAuraExceptionData.LogIntoObject('PublicApplicationHomeInfo','PublicProviderHomeInformation','Input parameters are :: id'+id ,ex);
                                    CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetch Home Information details','Unable to fetch Home Information details, Please try again' , CustomAuraExceptionData.type.Error.name());
                                    throw new AuraHandledException(JSON.serialize(oErrorData));
                                }    
        return homeInformation;
    }

    @AuraEnabled(cacheable =true)
     public static Map<String, List<DMLOperationsHandler.FetchValueWrapper>> getMultiplePicklistValues(String objInfo, String picklistFieldApi) {
         return DMLOperationsHandler.fetchMultiplePickListValue(objInfo,picklistFieldApi);
    }
}