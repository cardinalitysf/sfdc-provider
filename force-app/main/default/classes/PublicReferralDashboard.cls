/**
 * @Author        : Preethi Bojarajan
 * @CreatedOn     : Jun 29, 2020
 * @Purpose       : Referral Dashboard Private / Public with Pending, Approved and Rejected 
 * @updatedBy     : Preethi Bojarajan
 * @updatedOn     : July 07, 2020
 **/
public with sharing class PublicReferralDashboard {
    @AuraEnabled(cacheable=true)
    public static List<Case> getReferalPrivList(String searchKey, String type) {
        String model = 'Case';
        String key = '%' + searchKey + '%';
        String privateObj = 'Private';
        String recordType = 'Referral';
        String fields = 'Id, CaseNumber, Account.ProviderType__c, Program__c, Account.Name, Account.SON__c, Account.RFP__c, Status, Account.Id';
        List<String> lstStatus = type.split(',');
        if(type == 'total'){
            lstStatus.add('Pending');
            lstStatus.add('Approved');
            lstStatus.add('Rejected');
            lstStatus.add('Draft');
        } else if(type == 'draft'){
            lstStatus.add('Pending');
            lstStatus.add('Draft');
        } else lstStatus.add(type);
        List<SObject> referalList = new DMLOperationsHandler(model).
                                        selectFields(fields).
                                        addConditionEq('Account.ProviderType__c', privateObj).
                                        addConditionIn('Status', lstStatus).
                                        addConditionEq('RecordType.Name', recordType).
                                        addConditionLike('CaseNumber', key).
                                        orderBy('LastModifiedDate', 'DESC').
                                        run();
        return referalList;
    }

    @AuraEnabled(cacheable=true)
    public static List<WrapperCasecount> getReferalCount(String searchKey, String type) {
        List<WrapperCasecount> lstWrapper = new List<WrapperCasecount>();
        String key = '%' + searchKey + '%';
        String recordType = 'Referral';
        Integer countDraft = 0;
        Integer countPending = 0;
        Integer countApproved = 0;
        Integer countRejected = 0;
        Integer countAssignedFrTraining = 0;
        Integer countTrainUnAttached = 0;
        Integer countTrainCompleted = 0;
        List<AggregateResult> results =  new DMLOperationsHandler('Case'). 
                                 selectFields('Status').
                                 addConditionLike('CaseNumber', key).
                                 addConditionEq('Account.ProviderType__c', type).
                                 addConditionEq('RecordType.Name', recordType).
                                 count('Id').   
                                 groupBy('Status').
                                 run();
            for(AggregateResult result : results) {
                if(result.get('Status') == 'Draft')
                countDraft = (Integer) result.get('expr0');
                else if(result.get('Status') == 'Pending')
                countPending = (Integer) result.get('expr0');
                else if(result.get('Status') == 'Approved')
                countApproved = (Integer) result.get('expr0');
                else if(result.get('Status') == 'Rejected')
                countRejected = (Integer) result.get('expr0');
                else if(result.get('Status') == 'Assigned for Training')
                countAssignedFrTraining = (Integer) result.get('expr0');
                else if(result.get('Status') == 'Training Unattended')
                countTrainUnAttached = (Integer) result.get('expr0');
                else if(result.get('Status') == 'Training Completed')
                countTrainCompleted = (Integer) result.get('expr0');
            }
        lstWrapper.add(new Wrappercasecount(countDraft, countApproved, countRejected, countPending, countAssignedFrTraining, countTrainUnAttached, countTrainCompleted, type)); 
        return lstWrapper;
    }

    // Wrapper Class
    public class Wrappercasecount {
        @AuraEnabled public Integer draft;
        @AuraEnabled public Integer approved;
        @AuraEnabled public Integer rejected;
        @AuraEnabled public Integer pending;
        @AuraEnabled public Integer total;  
        public Wrappercasecount(Integer countDraft, Integer countApproved, Integer countRejected, Integer countPending, Integer countAssignedFrTraining, Integer countTrainUnAttached, Integer countTrainCompleted, String type) {
        this.approved = countApproved;
        this.rejected = countRejected;
        this.pending = countPending;
        if(type == 'Private') {
            this.draft = countDraft + countPending;
            this.total = countDraft + countApproved + countRejected + countPending;
        } else if(type == 'Public') {
            this.draft = countDraft + countPending + countAssignedFrTraining + countTrainUnAttached + countTrainCompleted;
            this.total = this.draft + countApproved + countRejected;
        }
        }
    }

    @AuraEnabled(cacheable=true)
    public static List<Account> getPubReferralList(String searchKey, String type) {
        String model = 'Case';
        String fields = 'Id, CaseNumber, Account.ProviderType__c, ProgramType__c, Program__c, Status';
        searchKey = '%' + searchKey + '%';
        String publicObj = 'Public';
        String recordType = 'Referral';
        List<String> lstStatus = type.split(',');
        if(type == 'total'){
            lstStatus.add('Pending');
            lstStatus.add('Approved');
            lstStatus.add('Rejected');
            lstStatus.add('Draft');
            lstStatus.add('Assigned for Training');
            lstStatus.add('Training Unattended');
            lstStatus.add('Training Completed');
        } else if(type == 'draft'){
            lstStatus.add('Pending');
            lstStatus.add('Draft');
            lstStatus.add('Assigned for Training');
            lstStatus.add('Training Unattended');
            lstStatus.add('Training Completed');
        } else lstStatus.add(type);
        String justRole = 'Applicant,Co-Applicant';
        List<String> role = justRole.split(',');
        List<SObject> proviList = new DMLOperationsHandler(model).
                                    selectFields(fields).
                                    addSubquery(
                                    DMLOperationsHandler.subquery('Actors__r').
                                    selectFields('Id, Role__c, Contact__r.LastName, Contact__r.SSN__c, Application__r.Program__c').
                                    addConditionIn('Role__c', role)).
                                    addConditionIn('Status', lstStatus).
                                    addConditionEq('Account.ProviderType__c', publicObj).
                                    addConditionEq('RecordType.Name', recordType).
                                    addConditionLike('CaseNumber', searchKey).
                                    orderBy('LastModifiedDate', 'DESC').
                                    run();
        return proviList;
    }
}