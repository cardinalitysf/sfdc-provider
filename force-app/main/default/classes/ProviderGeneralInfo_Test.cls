/**
* @Author        : Pratheeba.V
* @CreatedOn     : May 11 ,2020
* @Purpose       : Test Methods to unit test ProviderGeneralInfo class
@Updated By      : Naveen S
@Updated On      : July 24,2020
**/

@IsTest
private class ProviderGeneralInfo_Test {
    public ProviderGeneralInfo_Test() {}
    @IsTest static void testProviderGeneralInfoPositive(){
        Account InsertobjNewAccount=new Account(Name = 'Test Account'); 
        insert InsertobjNewAccount;

        try{
            providerGeneralInfo.getProviderDetails(null);
          }
          catch(Exception e){

          }
        providerGeneralInfo.getProviderDetails(InsertobjNewAccount.Id);
        try{
            providerGeneralInfo.fetchDataForAddress(null,null);
          }
          catch(Exception e){

          }
        providerGeneralInfo.fetchDataForAddress(InsertobjNewAccount.Id,'Site');
        try{
            providerGeneralInfo.getApplicationDetails(null);
          }
          catch(Exception e){

          }
        providerGeneralInfo.getApplicationDetails(InsertobjNewAccount.Id);
    }

    @IsTest static void testProviderGeneralInfoException(){
        providerGeneralInfo.getProviderDetails('');
        providerGeneralInfo.fetchDataForAddress('','');
        providerGeneralInfo.getApplicationDetails('');
    }
}