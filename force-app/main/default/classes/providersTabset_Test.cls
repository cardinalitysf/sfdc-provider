/**
 * @Author        : Pratheeba V
 * @CreatedOn     : June 8, 2020
 * @Purpose       : rovidersTabset class 
 * @Updated on    :June 20, 2020
 * @Updated by    :Janaswini Unit Test Updated methods with try catch
 **/
@IsTest
private class providersTabset_Test {
    @IsTest static void testPublicApplicationServiceTrainingPositive(){
        Account InsertobjNewAccount=new Account(Name = 'Test Account'); 
        insert InsertobjNewAccount;
        try
        {
            ProvidersTabset.fetchDataForTabset(null);
        }
        
        catch(Exception ex)
           {
               
           }
        ProvidersTabset.fetchDataForTabset(InsertobjNewAccount.Id);
    }

    @IsTest static void testPublicApplicationServiceTrainingException(){
        ProvidersTabset.fetchDataForTabset('');
    }
}
