public with sharing class TaskTriggerHandler implements ITriggerHandler{
	/**
	 * @Author        : V.S.Marimuthu
	 * @CreatedOn     : March 17 ,2020
	 * @Purpose       : Handler class which will do hand shake between trigger and task service
	 **/
	public Boolean isDisabled(){
		// string blnTriggerSwitch = 'false';
		// List<Bypass_Switch_For_Trigger__mdt> triggerSwitch = [select
		//                                                             Trigger_Switch__c
		//                                                       from Bypass_Switch_For_Trigger__mdt];
		// for (Bypass_Switch_For_Trigger__mdt value : triggerSwitch){
		//     blnTriggerSwitch = value.Trigger_Switch__c;
		// }
		// return blnTriggerSwitch == 'false' ? false : true;
		return false;
	}

	public void beforeInsert(List<Sobject> newItems){
		
	}

	public void beforeUpdate(Map<id, sObject> newItems, Map<id, sObject> oldItems){
	
	}

	public void beforeDelete(Map<id, sObject> oldItems){
	
	}

	public void afterInsert(List<Sobject> newMap){
		try{
				List<Monitoring__c> lstMonitorToInsert= new List<Monitoring__c>();
				// Iterate over the list of monitor list and extract the values
				// During first time insert be default we are setting "Draft as status"
				for(Task tsk:(List<Task>)newMap)
				{
					Monitoring__c mon= new Monitoring__c();	
					mon.id=tsk.Monitoring__c;
					mon.Status__c= ProviderConstants.APPLICATION_ACTIVITY_DRAFT_TODISPLAY;
					lstMonitorToInsert.add(mon);
				}
				// To remove duplicates
				Set<Monitoring__c> myset = new Set<Monitoring__c>();
				List<Monitoring__c> result = new List<Monitoring__c>();
				for (Monitoring__c s : lstMonitorToInsert) {
  				if (myset.add(s)) {
    				result.add(s);
  				}
			}
				// Insert into motoring object with status as draft
				new TaskService().updateMonitoring(result);
		}
		catch(Exception Ex)
		{

		}
	}

	public void afterUpdate(Map<Id, sObject> newMap, Map<Id, sObject> oldMap){
		
		try{			
			List<Monitoring__c> objMonitoring= new List<Monitoring__c>();
		    List<Task> lstTask= new List<Task>();
		    for(Task objTask : ((Map<Id,Task>)newMap).values()){
		        //Create an old and new map so that we can compare values
		        Task oldTask = (Task)oldMap.get(objTask.ID);
		        Task newTask = (Task)newMap.get(objTask.ID);
		        //If the fields are different, then ID has changed
		        if(oldTask.Status__c != newTask.Status__c){
					Monitoring__c mon= new Monitoring__c();
					mon.id=objTask.Monitoring__c;
		            objMonitoring.add(mon);
		        }
		    }//for			
				lstTask = [select Status__c,Monitoring__c from Task where  Monitoring__c in : objMonitoring ];
								Map<String, String> finalMap = new Map<String, String>();
				for(Task indTask : lstTask  ){            
					String tempKey = indTask.Monitoring__c ;
					//check key exists 
					if(finalMap.containsKey(tempKey))
					{
						//take previous value and append ';' before putting current value
						finalMap.put(tempKey,finalMap.get(tempKey) + ';' + indTask.Status__c.toUpperCase());
					}
					else
					{
						finalMap.put(tempKey,indTask.Status__c.toUpperCase());
					}
				} 
				List<Monitoring__c> lstMonitoringToUpdate=new List<Monitoring__c>();
				// To find out the status of each task and combine into a single row.
				for(String monitoringID : finalMap.keySet()){
					String strCmp=finalMap.get(monitoringID);
                    string setToCmp=ProviderConstants.APPLICATION_ACTIVITY_INCOMPLETE;
					List<String> lsttest=strCmp.split(';'); 				
					boolean blnStatusCheck=false;    
					if(lsttest.size()>0)
					{
					for(String s: lsttest){					
						if(setToCmp.equals(s.toUpperCase())){
						 blnStatusCheck=true;
							   break;
						  }
					}
				}
				else {					
					blnStatusCheck=true;
				}
					Monitoring__c montoupdate= new Monitoring__c();
					montoupdate.id=monitoringID;
					montoupdate.Status__c= blnStatusCheck ? ProviderConstants.APPLICATION_ACTIVITY_DRAFT_TODISPLAY:ProviderConstants.APPLICATION_ACTIVITY_SUCCESS_TODISPLAY;
					lstMonitoringToUpdate.add(montoupdate);					
				}					
		    new TaskService().updateMonitoring(lstMonitoringToUpdate);		   
		}
		catch(Exception Ex)
		{
		}
	}

	public void afterDelete(Map<id, sObject> oldMap){
		
	}

	public void afterUndelete(List<Sobject> newMap){
		
	}
}