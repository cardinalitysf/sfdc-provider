/**
     * @Author        : Naveen S
     * @CreatedOn     : July 17,2020
     * @Purpose       :Test Methods to unit test for Compliants MonitoringComarRecords.cls
     * @Updated on    :July 17,2020
     * @Updated by    :Naveen
     **/

    @isTest
public with sharing class ComplaintsMoniteringComarRecords_Test {
    @isTest static void ComplaintsMoniteringComarRecords_Test(){
        ReferenceValue__c InsertobjNewReference=new ReferenceValue__c(); 
        insert InsertobjNewReference;
        Account InsertobjNewAccount=new Account(Name = 'test Account'); 
        insert InsertobjNewAccount;
        Monitoring__c InsertobjNewMonitoring=new Monitoring__c(); 
        insert InsertobjNewMonitoring;
        Application__c InsertobjNewApplication=new Application__c(); 
        insert InsertobjNewApplication;
        ProviderRecordQuestion__c InsertobjNewProviderRecordQuestion=new ProviderRecordQuestion__c(Status__c= 'Draft'); 
        insert InsertobjNewProviderRecordQuestion;
        ProviderRecordAnswer__c InsertobjNewProviderRecordAnswer=new ProviderRecordAnswer__c(Comments__c	 = 'test comments'); 
        insert InsertobjNewProviderRecordAnswer;
        String str = JSON.serialize(InsertobjNewProviderRecordAnswer);    

        Address__c InsertobjNewAddress=new Address__c(AddressLine1__c = '345 California St'); 
        insert InsertobjNewAddress;

        
       try
       {
        complaintsMoniteringComarRecords.getQuestionsByType(null);
       }catch(Exception Ex)
       {
           
       }
       complaintsMoniteringComarRecords.getQuestionsByType('questions');
        try
        {
            complaintsMoniteringComarRecords.editRecordDet(null);
        }catch(Exception Ex)
        {
            
        }
        complaintsMoniteringComarRecords.editRecordDet('question');

       try
       {
        TestDataFactory.createTestContacts(null,null,null);
       }
       catch(Exception Ex)
       {
           
       }
       TestDataFactory.createTestContacts(1,InsertobjNewAccount.id,true);
       try
       {
        complaintsMoniteringComarRecords.getStaffFromApplicationId(null);
       }catch(Exception Ex)
       {
           
       }
       complaintsMoniteringComarRecords.getStaffFromApplicationId('abc');
       try
       {
        complaintsMoniteringComarRecords.getRecordDetails(null,null,null);
       }catch(Exception Ex)
       {
           
       }
       complaintsMoniteringComarRecords.getRecordDetails(InsertobjNewMonitoring.Id,InsertobjNewAddress.Id,'Id');

       
       complaintsMoniteringComarRecords.getRecordType('Board Interview');
       try
       {
        complaintsMoniteringComarRecords.getSiteId(null);
       }
       catch(Exception Ex)
       {
           
       }
       complaintsMoniteringComarRecords.getSiteId('address');
       try{
        complaintsMoniteringComarRecords.getProviderType(null);
       }
       catch(Exception Ex)
       {
           
       }
       complaintsMoniteringComarRecords.getProviderType(InsertobjNewAccount.Id);
       try{
        complaintsMoniteringComarRecords.getHouseholdMembers(null);
       }
       catch(Exception Ex)
       {
           
       }
       complaintsMoniteringComarRecords.getHouseholdMembers(InsertobjNewAccount.Id);
       
       ReferenceValue__c ref = new ReferenceValue__c();
       insert ref;
       string str1 = '[{"Comar__c":"'+InsertobjNewReference.Id+'","Compliance__c":"'+InsertobjNewProviderRecordAnswer.Id+'","Findings__c":"'+InsertobjNewProviderRecordAnswer.Id+'","ProviderRecordQuestion__c":"'+InsertobjNewProviderRecordQuestion.Id+'"}]';
       
       try
       {
       
       complaintsMoniteringComarRecords.bulkAddRecordAns(null);

       }
       catch(Exception Ex)
       {
           
       }
       complaintsMoniteringComarRecords.bulkAddRecordAns(str1);
       string strUpdate = '[{"Id":"'+InsertobjNewProviderRecordAnswer.Id+'","Compliance__c":"","Findings__c":"","ProviderRecordQuestion__c":"'+InsertobjNewProviderRecordQuestion.Id+'"}]';
           
       try
       {
        
        complaintsMoniteringComarRecords.bulkUpdateRecordAns(null);
       }catch(Exception Ex)
       {
           
       }
       complaintsMoniteringComarRecords.bulkUpdateRecordAns(strUpdate);
            
        }

}