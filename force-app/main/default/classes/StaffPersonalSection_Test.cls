/**
 * @Author        : Pratheeba V
 * @CreatedOn     : May 6,2020
 * @Purpose       : Test Method to unit test for StaffPersonalSection.cls
 * @Updated on    :June 20, 2020
 * @Updated by    :Janaswini Unit Test Updated methods with try catch
 **/

@isTest
private class StaffPersonalSection_Test {
    @isTest static void testStaffPersonalSectionPositive(){
        Contact InsertobjNewContact=new Contact(LastName = 'Test Contact'); 
        insert InsertobjNewContact;      
        try
        {
            StaffPersonalSelection.fetchDataForAddOrEdit(null);
        }
        
        catch(Exception ex)
           {
               
           }
        StaffPersonalSelection.fetchDataForAddOrEdit(InsertobjNewContact.Id);
        StaffPersonalSelection.updateOrInsertSOQLReturnId(InsertobjNewContact);

        try{
            StaffPersonalSelection.updateOrInsertSOQL(InsertobjNewContact); 
        } catch (Exception e) {
        }  
        try{
            List<DMLOperationsHandler.FetchValueWrapper>  objgetPickListValues = StaffPersonalSelection.getPickListValues(InsertobjNewContact,'XXX');
        } catch (Exception e) {
        }       
    }

    @isTest static void testStaffPersonalSectionException(){
        StaffPersonalSelection.fetchDataForAddOrEdit('');
    }
}