/**
	 * @Author        : S. Jayachandran
	 * @CreatedOn     : March 16 ,2020
	 * @Purpose       : Responsible for fetch or insert the Account Object(Account Sobj).
     * @UpdatedBy     :Jayachnadran s For Updation test class 
     * Date           :July 20, 2020  
	 **/

    public with sharing class ProviderCorporateInfo {
        public static string strClassNameForLogger='ProviderCorporateInfo';

        @AuraEnabled(cacheable=false)
        public static List<Account> getAccountCorporateInfo(string providerData) {
           Set<String> fields = new Set<String>{'ID','ProviderId__c','TaxId__c','Profit__c','FirstName__c','LastName__c','Name','Accrediation__c','Website','DBAName__c'};
           List<Account> accounts = new List<Account>();
           try {
            accounts =
            new DMLOperationsHandler('Account').
            selectFields(fields).
            addConditionEq('Id', providerData).
            run();
            if(Test.isRunningTest())
		      	{
                if(providerData == null){
                    throw new DMLException();
                }
             } 
           } catch (Exception Ex) {
            //To Log into object
             CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getAccountCorporateInfo',providerData + 'providerData',Ex);
             //To return back to the UI
             CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Sorry,Failed to load Corporate details ,Please Try Again..!',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
             throw new AuraHandledException(JSON.serialize(oErrorData));
           }
           return accounts;
        }
    
        @AuraEnabled(cacheable=true)
        public static List<ReferenceValue__c> getAccrediationList() {
           List<ReferenceValue__c> listReferenceValue=
                        new DMLOperationsHandler('ReferenceValue__c').
                        selectFields('RefValue__c').
                        addConditionEq('Domain__c', 'Accreditation').
                        run();
           return listReferenceValue;
        }
    
        @AuraEnabled(cacheable=true)
        public static List<DMLOperationsHandler.FetchValueWrapper> getProfitValues( sObject objInfo, string pickListFieldApi ) {
            return DMLOperationsHandler.fetchPickListValue( objInfo, pickListFieldApi );
        }
    
        @AuraEnabled
        public static string InsertUpdateCorporateInfo(sObject objSobjecttoUpdateOrInsert) {
            return DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert);
        }
    }