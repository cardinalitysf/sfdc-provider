@IsTest
public class ConferenceTriggerHandler_Test {
    
      static testMethod void testConference(){
           test.startTest();
        Account InsertobjNewAccount=new Account(Name = 'Test Account'); 
        insert InsertobjNewAccount;
        
        Application__c InsertobjNewApplication=new Application__c(Status__c = 'Pending',Provider__c = InsertobjNewAccount.Id);
        insert InsertobjNewApplication;
        
        List<Monitoring__c> monList = new List<Monitoring__c>();
        Monitoring__c Insmonitoring= new Monitoring__c(ApplicationLicenseId__c=InsertobjNewApplication.Id);
        monList.add(Insmonitoring);
        
        insert monList;
        
        List<Conference__c> conList = new List<Conference__c>();
        Conference__c conf = new Conference__c(Type__c = 'Entrance Conference',Monitoring__c=Insmonitoring.id);
        conList.add(conf);
        insert conList;
          
        List<Task> tskList = new List<Task>();
          Task tsk=new Task(Status__c='Completed',ActivityName__c ='Entrance Conference',Monitoring__c =Insmonitoring.Id);
          tskList.add(tsk);
          
          insert tskList; 
                  
          
           ConferenceTriggerHandler conTriHndlr = new ConferenceTriggerHandler();
        
           conTriHndlr.afterInsert(conList);
           test.stopTest();
      }
      
      
      static testMethod void test_BeforeInsert_UseCase1(){
        List<Conference__c> con_Obj  =  [SELECT Id,Type__c from Conference__c];
        System.assertEquals(false,con_Obj.size()>0);
        ConferenceTriggerHandler obj01 = new ConferenceTriggerHandler();
        obj01.BeforeInsert(new List<Conference__c>());
     }
    
     
  static testMethod void test_beforeUpdate_UseCase1(){
   List<Conference__c> con_Obj  =  [SELECT Id,Type__c from Conference__c];
    System.assertEquals(false,con_Obj.size()>0);
    ConferenceTriggerHandler obj01 = new ConferenceTriggerHandler();
    obj01.beforeUpdate(new Map<id,sObject>(),new Map<id,sObject>());
  }
  static testMethod void test_beforeDelete_UseCase1(){
    List<Conference__c> con_Obj  =  [SELECT Id,Type__c from Conference__c];
    System.assertEquals(false,con_Obj.size()>0);
    ConferenceTriggerHandler obj01 = new ConferenceTriggerHandler();
    obj01.beforeDelete(new Map<id,sObject>());
  }
  
  static testMethod void test_afterUpdate_UseCase1(){
   List<Conference__c> con_Obj  =  [SELECT Id,Type__c from Conference__c];
    System.assertEquals(false,con_Obj.size()>0);
    ConferenceTriggerHandler obj01 = new ConferenceTriggerHandler();
    obj01.afterUpdate(new Map<Id,sObject>(),new Map<Id,sObject>());
  }
  static testMethod void test_afterDelete_UseCase1(){
   List<Conference__c> con_Obj  =  [SELECT Id,Type__c from Conference__c];
    System.assertEquals(false,con_Obj.size()>0);
    ConferenceTriggerHandler obj01 = new ConferenceTriggerHandler();
    obj01.afterDelete(new Map<id,sObject>());
  }
  static testMethod void test_afterUndelete_UseCase1(){
    List<Conference__c> con_Obj  =  [SELECT Id,Type__c from Conference__c];
    System.assertEquals(false,con_Obj.size()>0);
    ConferenceTriggerHandler obj01 = new ConferenceTriggerHandler();
    obj01.afterUndelete(new List<Sobject>());
  }
}