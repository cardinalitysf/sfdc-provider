/**
 * @Author        : Marimuthu Subramanian
 * @CreatedOn     : July 09 ,2020
 * @Purpose       : Common Class to handle the expection happened in Apex.
**/

public class CustomAuraExceptionData {
	public String title;
    public String message;
    public string errorType;
    public Integer code;
    public ENUM type {Error, Warning,Success,Informational}   
   

    
    public CustomAuraExceptionData(String title, String message, String errorType) {
        this.title = title;
        this.message = message;
        this.code = StatusCode.INTERNAL_ERROR.ordinal();
        this.errorType=errorType;
    }
    public static void LogIntoObject(String strApexClassName,String strApexMethodName,String StrRecordData,Exception Ex) {
        new CommonDebugLogger().createLog(
            new CommonDebugLogger.Error(
                strApexClassName,
                strApexMethodName,
                StrRecordData,
                Ex
            )
        );

    }
}