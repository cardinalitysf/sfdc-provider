/**
 * Author   : Pratheeba.V
 * Date     : 17-8-2020
 * Purpose  : Test class for CPA Address Details class
 * UpdatedBy: Sindhu
 * UpdatedOn:20-08-2020
 */
@IsTest
private class CpaAddressDetails_Test {
    @IsTest static void testPositive() {
        Address__c InsertAddressObj = new Address__c();
        insert InsertAddressObj;
        CPAHome__c InsertCpaObj=new CPAHome__c();
        insert InsertCpaObj;
        CpaAddressDetails.updateOrInsertSOQL(InsertAddressObj);
        try {
            CpaAddressDetails.getStateDetails();
        } catch (Exception e) { }
        try {
            CpaAddressDetails.editAddressCpaDetails(InsertCpaObj.Id);
        } catch (Exception e) { }
    }

    @IsTest static void testException() {
        try {
            CpaAddressDetails.editAddressCpaDetails('');
        } catch (Exception e) { }
    }
}