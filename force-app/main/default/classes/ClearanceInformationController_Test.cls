/**
 * @Author        : Pratheeba V
 * @CreatedOn     : May 5,2020
 * @Purpose       : Test Method to unit test for ClearanceInformationController.cls
 **/

@isTest
public class ClearanceInformationController_Test {    
    @isTest static void testGetClearanceDetailsPositive(){
        Account InsertobjNewAccount=new Account(Name = 'test Account'); 
        insert InsertobjNewAccount;
        Contact Insertcontact = new Contact(LastName='Test Contact');
        insert Insertcontact;            
        ClearanceInformationController.getIdAfterInsertSOQL(InsertobjNewAccount);
        try {
            ClearanceInformationController.getContactList(Insertcontact.Id);
        } catch (Exception e) { }
    }

    @isTest static void testapplicationStaffException()
    {   
        try {
            ClearanceInformationController.getContactList('');
        } catch (Exception e) { }    
    }
}