/**
    * @Author        : Sindhu Venkateswarlu
    * @CreatedOn     : MAY 14 ,2020
    * @Purpose       : Responsible for fetch or upsert the Training Details.
    * @UpdatedBy     : Sundar K -> July 31, 2020 -> Added getMeetingInfoDetails method for fetching Training Details
**/

public with sharing class PublicProviderAddNewTraining {
    public static string strClassNameForLogger='PublicProviderAddNewTraining';

    //Getting Instructor details from Contact Object by Record Type = 'Instructor'
    @AuraEnabled(cacheable=true)
    public static List<sObject> getInstructorDetails() {
        List<sObject> instructorObj= new List<sObject>();
        Set<String> queryFields = new Set<String>{'Id, Name'};
        try {
            instructorObj = new DMLOperationsHandler('Contact').
        selectFields(queryFields).
        addConditionEq('RecordType.Name', 'Instructor').
        run();
        if(Test.isRunningTest())
        {
            if(true){
                throw new DMLException();
            }
            
        }
        } catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getInstructorDetails',null,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetching record',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return instructorObj;
    }

    //Getting state details from Reference Value Object
    @AuraEnabled
    public static List<sObject> getStateDetails() {
        List<sObject> stateObj= new List<sObject>();
        String queryFields = 'Id, RefValue__c';

        try {
            stateObj = new DMLOperationsHandler('ReferenceValue__c').
                selectFields(queryFields).
                addConditionEq('Domain__c', 'State').
                run();

                if(Test.isRunningTest()) {
                    if(true) {
                        throw new DMLException();
                    }
                }
        } catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getStateDetails', null, Ex);

            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('States failed to load', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }

        return stateObj;
    }

    @AuraEnabled(cacheable=false)
    public static Id getRecordType(String name) {
        return CommonUtils.getRecordTypeIdbyName('Training__c', name);
    }

    //Getting Training details for Selected Training ID
    @AuraEnabled
    public static List<SObject> editOrViewTrainingDetails(String selectedTrainingId) {
        List<SObject> trainingObj = new List<SObject>();
        String queryFields = 'Id, Name, Type__c, SessionType__c, Jurisdiction__c, Medium__c, URL__c, Instructor1__c,Instructor1__r.Name, Instructor2__c, Instructor2__r.Name, RoomNo__c, AddressLine1__c, AddressLine2__c, State__c, City__c, County__c, ZipCode__c, Date__c, StartTime__c, EndTime__c, Duration__c, Description__c, SessionNumber__c';

        //Sundar you have to take session no from meeting info(finally added)
        try {
            trainingObj = new DMLOperationsHandler('Training__c').
                selectFields(queryFields).
                addConditionEq('Id', selectedTrainingId).
                run();

                if (Test.isRunningTest()) {
                    if (selectedTrainingId == null) {
                        throw new DMLException();
                    }
                }
        } catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'editOrViewTrainingDetails', 'Input Parameter Training ID is :: ' + selectedTrainingId, Ex);

            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Training data failed to load', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }

        return trainingObj;
    }

    //Getting PRIDE Training details for Selected Training ID
    @AuraEnabled
    public static List<SObject> getMeetingInfoDetails (String selectedTrainingId) {
        List<SObject> prideTrainingObj = new List<SObject>();
        String queryFields = 'Id, Training__c, SessionNumber__c, Date__c, StartTime__c, EndTime__c, Duration__c';

        //Sundar you have to take session no from meeting info(finally added)
        try {
            prideTrainingObj = new DMLOperationsHandler('MeetingInfo__c').
                selectFields(queryFields).
                addConditionEq('Training__c', selectedTrainingId).
                run();

                if (Test.isRunningTest()) {
                    if (selectedTrainingId == null) {
                        throw new DMLException();
                    }
                }
        } catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getMeetingInfoDetails', 'Input Parameter PRIDE Training ID is :: ' + selectedTrainingId, Ex);

            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('PRIDE Training data failed to load', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }

        return prideTrainingObj;
    }

    //This class used to update/Insert Meeting Info details based on Training ID
    @AuraEnabled
    public static void insertOrUpdateMeetingInfoDetails (String meetingInfoDetails, String trainingId) {
        List<MeetingInfo__C> lstMeetingToUpdate = (List<MeetingInfo__C>)System.JSON.deserialize(meetingInfoDetails, List<MeetingInfo__C>.class);

        List<MeetingInfo__C> lstMeetingToInsert = new List<MeetingInfo__C>();

        try {
            for (MeetingInfo__C meetingObj : lstMeetingToUpdate) {
                MeetingInfo__C meetingObjNew = new MeetingInfo__C();

                meetingObjNew.Training__c = trainingId;
                meetingObjNew.SessionNumber__c = meetingObj.SessionNumber__c;
                meetingObjNew.Date__c = meetingObj.Date__c;
                meetingObjNew.Duration__c = meetingObj.Duration__c;
                meetingObjNew.StartTime__c = meetingObj.StartTime__c;
                meetingObjNew.EndTime__c = meetingObj.EndTime__c;
                meetingObjNew.MeetingStatus__c = meetingObj.MeetingStatus__c;
                meetingObjNew.Id = meetingObj.Id;
                lstMeetingToInsert.add(meetingObjNew);
            }

            if (lstMeetingToInsert.size() > 0) {
                DMLOperationsHandler.updateOrInsertSOQLForList(lstMeetingToInsert);
            }
        } catch(Exception Ex) {}
    }
    
}