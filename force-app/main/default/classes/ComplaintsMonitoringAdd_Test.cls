/**
 * @Author        : Sindhu
 * @CreatedOn     : July 15 ,2020
 * @Purpose       : Test Methods to unit test ComplaintsMonitoringAdd class
 **/
@IsTest
public with sharing class ComplaintsMonitoringAdd_Test {
    @IsTest static void testgetMonitoringDetails()
    {
        case InsertobjNewComplaint=new case(); 
        insert InsertobjNewComplaint;
        Monitoring__c InsertobjNewMonitoring=new Monitoring__c(); 
        insert InsertobjNewMonitoring; 
        ComplaintsMonitoringAdd.InsertUpdateMonitoring(InsertobjNewMonitoring);
        try
           {
            ComplaintsMonitoringAdd.getMonitoringDetails(null);
           }catch(Exception Ex)
           {
           }
        ComplaintsMonitoringAdd.getMonitoringDetails('InsertobjNewComplaint.Id');
        ComplaintsMonitoringAdd.getAddressDetails('InsertobjNewComplaint.Id');
        ComplaintsMonitoringAdd.getProgramDetails('InsertobjNewComplaint.Id');
        try
           {
            ComplaintsMonitoringAdd.editMonitoringDetails(null);
           }catch(Exception Ex)
           {
           }
        ComplaintsMonitoringAdd.editMonitoringDetails('InsertobjNewMonitoring.Id');
        try
        {
         ComplaintsMonitoringAdd.viewMonitoringDetails(null);
        }catch(Exception Ex)
        {
        }
        ComplaintsMonitoringAdd.viewMonitoringDetails('InsertobjNewMonitoring.Id');
        ComplaintsMonitoringAdd.getMonitoringStatus('InsertobjNewMonitoring.Id');     
    }   
}
