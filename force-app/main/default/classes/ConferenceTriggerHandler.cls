public with sharing class ConferenceTriggerHandler implements ITriggerHandler{
    /**
 * @Author        : V.S.Marimuthu
 * @CreatedOn     : April 21 ,2020
 * @Purpose       : To update Task Object when status is moved to completed in ProviderRecordQuestion
 **/
public Boolean isDisabled(){
    // string blnTriggerSwitch = 'false';
    // List<Bypass_Switch_For_Trigger__mdt> triggerSwitch = [select
    //                                                             Trigger_Switch__c
    //                                                       from Bypass_Switch_For_Trigger__mdt];
    // for (Bypass_Switch_For_Trigger__mdt value : triggerSwitch){
    //     blnTriggerSwitch = value.Trigger_Switch__c;
    // }
    // return blnTriggerSwitch == 'false' ? false : true;
    return false;
}

public void beforeInsert(List<Sobject> newItems){
    
}

public void beforeUpdate(Map<id, sObject> newItems, Map<id, sObject> oldItems){

}

public void beforeDelete(Map<id, sObject> oldItems){

}

public void afterInsert(List<Sobject> newMap){
    try{
     // After inserting into monitoring object we need to update the status in task table
     // Status as completed
        List<Monitoring__c> objMonitoring= new List<Monitoring__c>();     
        set<String> setActivityType= new set<String>();
        for(Conference__c providerQuestion:(List<Conference__c>)newMap)
            {
                Monitoring__c monitoring= new Monitoring__c();
                monitoring.Id=providerQuestion.Monitoring__c; 
                
                //Type will be Exit Conference or Entrance conference
                setActivityType.add(providerQuestion.Type__c);
                objMonitoring.add(monitoring);
            }
            if(objMonitoring.size()>0)
            {
                // Update Task with status as completed
                UpdateTaskStatus(objMonitoring,setActivityType);
            }
        
    }
    catch(Exception Ex)
    {

    }
}

private void UpdateTaskStatus(List<Monitoring__c> objMonitoring,set<String> setActivityType)
	{
	   List<Task> lstTask= new List<Task>();
		try{	
				   lstTask =[select Id,Status__c,Monitoring__c,ActivityName__c  from Task where Monitoring__c in :objMonitoring and ActivityName__c in :setActivityType ] ;		
				   List<Task> tasktoUpdate=new List<Task>();
					 for(Task indtask:lstTask)
				   {
					   Task tsk=new Task();
					   tsk.id=indtask.Id;
					   tsk.Status__c='Completed';
					   tsk.CompletionDate__c=System.today();
					   tasktoUpdate.add(tsk);
				   }
					  new TaskService().updateTaskStatus(tasktoUpdate);	
		   }
		   catch(Exception Ex)
		   {
   
		   }
	 
	}
public void afterUpdate(Map<Id, sObject> newMap, Map<Id, sObject> oldMap){
  
}

public void afterDelete(Map<id, sObject> oldMap){
    
}

public void afterUndelete(List<Sobject> newMap){
    
}
}