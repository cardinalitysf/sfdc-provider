/**
 * @Author        : G sathishkumar ,
 * @CreatedOn     : Aug 03,2020
 * @Purpose       : IncidentGangDetails.
 **/

public with sharing class IncidentGangDetails {
       public static string strClassNameForLogger='IncidentGangDetails'; 
       @AuraEnabled(cacheable=false)
       public static List<Case> getGangDetails(string gangId) {
       List<sObject> gangDetailsObj = new List<sObject>();
       Set<String> fields = new Set<String>{'Id','GangRelated__c','SupportingEvidence__c','EvidenceDetails__c'};
      try{
       gangDetailsObj= new DMLOperationsHandler('Case').
       selectFields(fields).
       addConditionEq('Id',gangId).
       addConditionEq('RecordType.Name','Incident').
       run();
      if(Test.isRunningTest()){
        if(gangId==null){
          throw new DMLException();
        }

      }
      
      }catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getGangDetails',gangId,Ex);
         CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Case based error in fetching GangDetails record try later',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
         throw new AuraHandledException(JSON.serialize(oErrorData));
       }
       return gangDetailsObj;
       }


    // This function used to insertUpdate the GangDetails
    
    @AuraEnabled
    public static string insertUpdateGangDetails(sObject saveObjInsert){
        return DMLOperationsHandler.updateOrInsertSOQLReturnId(saveObjInsert);
    }
}
