/**
 * @Author        : Sundar Karuppalagu
 * @CreatedOn     : JULY 05 ,2020
 * @Purpose       : Sanctions Suspension Screen
 * @Updated By    : Nandhini -> added Test class Method
 * @Updated By    : Sundar K -> July 23, 2020 -> added getComplaintLicenseDetails method for fetching license dates.
 **/

public with sharing class complaintsSanctionSuspension {
    public static string strClassNameForLogger = 'complaintsSanctionSuspension';

    @AuraEnabled
    public static List<sObject> getSelectedSanctionData(String sanctionId) {
        List<sObject> suspensionObj = new List<sObject>();
        String queryFields = 'Id, StartDate__c, EndDate__c, Document__c, Delivery__c';

        try {
            suspensionObj = new DMLOperationsHandler('Sanctions__c').
                            selectFields(queryFields).
                            addConditionEq('Id', sanctionId).
                            addConditionEq('RecordType.Name', 'Suspension').
                            run();
                            if(Test.isRunningTest())
                            {
                                if(sanctionId==null){
                                    throw new DMLException();
                                }
                            }
        } catch (Exception Ex) {
            //To Log into object
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getSelectedSanctionData', 'Input Parameter Suspension ID is :: ' + sanctionId, Ex);

            //To return back to the UI
            CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Suspension data failed to load', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }

        return suspensionObj;
    }

    @AuraEnabled(cacheable=false)
    public static Id getRecordType(String name) {
        return CommonUtils.getRecordTypeIdbyName('Sanctions__c', name);
    }

    //Added this method for fetching Complaint License Details
    @AuraEnabled
    public static List<sObject> getComplaintLicenseDetails(String complaintId) {
        List<sObject> complaintLicenseObj = new List<sObject>();
        String queryFields = 'Id, CaseNumber, License__c, License__r.StartDate__c, License__r.EndDate__c';

        try {
            complaintLicenseObj = new DMLOperationsHandler('Case').
                                    selectFields(queryFields).
                                    addConditionEq('Id', complaintId).
                                    addConditionEq('RecordType.Name', 'Complaints').
                                    run();
                                    if (Test.isRunningTest())
                                    {
                                        if (complaintId == null){
                                            throw new DMLException();
                                        }
                                    }
        } catch (Exception Ex) {
            //To Log into object
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getComplaintLicenseDetails', 'Input Parameter Complaint ID is :: ' + complaintId, Ex);

            //To return back to the UI
            CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Complaint License Details failed to load', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return complaintLicenseObj;
    }
}