/**
 * @Author        : Sindhu 
 * @CreatedOn     : August 3,2020
 * @Purpose       : Responsible for fetch or insert the Address  and CPA Details.
**/

public with sharing class CpaAddressDetails {
    public static string strClassNameForLogger='CpaAddressDetails';

    @AuraEnabled(cacheable=true)
    public static List<ReferenceValue__c> getStateDetails() {
        List<sObject> lstStateDetails = new List<sObject>();
        String queryFields = 'Id, RefValue__c';
        try {
            lstStateDetails = new DMLOperationsHandler('ReferenceValue__c').
            selectFields(queryFields).
            addConditionEq('Domain__c','State'). 
            run();
            if(Test.isRunningTest())
			{
                if(true==null){
                    throw new DMLException();
                }
			}
        } catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getStateDetails',null,Ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetching record',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
            }
            return lstStateDetails;
    }

    @AuraEnabled
    public static string updateOrInsertSOQL(sObject objSobjecttoUpdateOrInsert){
        return DMLOperationsHandler.updateOrInsertSOQLReturnId(objSobjecttoUpdateOrInsert);
    }


    @AuraEnabled(cacheable=false)
    public static List<CPAHome__c> editAddressCpaDetails (String selectedCpaId) { 
        List<CPAHome__c> lstEditCpaAddress = new List<CPAHome__c>();  
        Set<String> fields = new Set<String>{'Id', 'Name', 'StartDate__c', 'EndDate__c', 'CertificateType__c', 'HomeHealthInspection__c', 'FireInspection__c', 'IsInspectionDocumentRecord__c','Address__r.AddressType__c','Address__r.AddressLine1__c','Address__r.AddressLine2__c','Address__r.County__c','Address__r.City__c','Address__r.State__c','Address__r.ZipCode__c'};
        try {
            lstEditCpaAddress = new DMLOperationsHandler('CPAHome__c').
                                            selectFields(fields).
                                            addConditionEq('Id',selectedCpaId).              
                                            run();
            if(Test.isRunningTest()) {
                if(selectedCpaId=='') {
                    throw new DMLException();
                }				
            }
        } catch (Exception ex) {
            String[] arguments = new String[] {selectedCpaId};
            string strInputRecord= String.format('Input parameters are :: selectedCpaId -- {0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'editAddressCpaDetails',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Address List',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }        
        return lstEditCpaAddress;
    }

    
}