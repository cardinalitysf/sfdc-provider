/**
 * @Author        : Preethi Bojarajan
 * @CreatedOn     : Aug 04, 2020
 * @Purpose       : New Staff Details Capturing for The Incident Apex Methods
 * @updatedBy     : 
 * @updatedOn     : 
 **/
public with sharing class IncidentStaffDetails {
    public static string strClassNameForLogger='IncidentStaffDetails';

    @AuraEnabled(cacheable=true)
    public static List<SObject> getStaffMembers(String applicationId) {
        List<sObject> staffMembers = new List<sObject>();
        try {
            String fields = 'Id, Staff__c, Staff__r.FirstName__c, Staff__r.LastName__c, Staff__r.LastName, Staff__r.AffiliationType__c, Staff__r.JobTitle__c, Staff__r.EmployeeType__c, ProgramType__c, Application__c';

            staffMembers = new DMLOperationsHandler('Application_Contact__c').
                                selectFields(fields).
                                addConditionEq('Application__c', applicationId).               
                                run();
                                if(Test.isRunningTest()) {
                                    if(applicationId == null){
                                        throw new DMLException();
                                    }
                                } 
        } catch (Exception ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getStaffMembers','Input parameters are :: applicationId' + applicationId ,ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in Fecthing Staff details','Unable to get Staff details, Please try again' , CustomAuraExceptionData.type.Error.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }           
        return staffMembers;
    }

    @AuraEnabled(cacheable=true)
    public static List<SObject> getActorDetails(String caseId) {
        List<sObject> actorMembers = new List<sObject>();
        try {
            String fields = 'Id, Contact__c, Contact__r.FirstName__c, Contact__r.LastName__c, Contact__r.LastName, Contact__r.AffiliationType__c, Contact__r.JobTitle__c, Contact__r.EmployeeType__c, InjurySeverityLevel__c, PrimaryStaffMember__c, StaffAssaulted__c, InjurySustained__c';

            actorMembers = new DMLOperationsHandler('Actor__c').
                                selectFields(fields).
                                addConditionEq('Referral__c', caseId).
                                addConditionEq('Role__c', 'Staff').
                                run();
                                if(Test.isRunningTest()) {
                                    if(caseId == null){
                                        throw new DMLException();
                                    }
                                } 
        } catch (Exception ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getActorDetails','Input parameters are :: caseId' + caseId ,ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in Fetching Staff details','Unable to get Staff details, Please try again' , CustomAuraExceptionData.type.Error.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }           
        return actorMembers;
    }

    @AuraEnabled(cacheable=true)
    public static Map<String, List<DMLOperationsHandler.FetchValueWrapper>> getMultiplePicklistValues(String objInfo, String picklistFieldApi) {
        return DMLOperationsHandler.fetchMultiplePickListValue(objInfo,picklistFieldApi);
    }

}
