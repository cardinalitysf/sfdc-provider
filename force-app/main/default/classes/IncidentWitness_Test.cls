/**
 * @Author        : B Balamurugan.
 * @CreatedOn     : Aug 17,2020
 * @Purpose       : Test Class of Incident Witness.cls .
 **/

@isTest
public  class IncidentWitness_Test {
    @isTest static void testIncidentWitness(){
        Actor__c InsertobjNewHHM=new Actor__c(Role__c = 'test Account');
        insert InsertobjNewHHM;
        Case InsertobjCase=new Case(Status = 'Pending'); 
        insert InsertobjCase;
        List<Contact> contacts = new List<Contact>();
        Contact InsertobjContact = new Contact(LastName = 'Name');
        insert InsertobjContact;

         try{
            IncidentWitness.getPkcDetails(null);
           }catch(Exception Ex)
           {
           }
           IncidentWitness.getPkcDetails(InsertobjCase.Id);


           try{
            IncidentWitness.getSelectedPersonKidChildDropdownValueData(null);
           }catch(Exception Ex)
           {
           }
           IncidentWitness.getSelectedPersonKidChildDropdownValueData(InsertobjContact.id);

           try{
            IncidentWitness.getStaffDetails(null);
           }catch(Exception Ex)
           {
           }
           IncidentWitness.getStaffDetails(InsertobjCase.Id);

           try{
            IncidentWitness.getSelectedStaffDropdownValueData(null);
           }catch(Exception Ex)
           {
           }
           IncidentWitness.getSelectedStaffDropdownValueData(InsertobjContact.id);

           IncidentWitness.InsertUpdateContactActor(InsertobjNewHHM);

        
            IncidentWitness.getRecordType ('Witness');

           try{
            IncidentWitness.getWitnessDetails(null);
           }catch(Exception Ex)
           {
           }
           IncidentWitness.getWitnessDetails(InsertobjCase.Id);

           try{
            IncidentWitness.getActionDetails(null);
           }catch(Exception Ex)
           {
           }
           IncidentWitness.getActionDetails(InsertobjNewHHM.Id);

    }
 }
