/**
 * @Author        : Pratheeba V
 * @CreatedOn     : June 5, 2020
 * @Purpose       : Public Application Placement tab 
 **/
public with sharing class publicApplicationPlacement {
    public static string strClassNameForLogger='publicApplicationPlacement';
    @AuraEnabled(cacheable =true)
    public static List<DMLOperationsHandler.FetchValueWrapper> getPickListValues(sObject objInfo, string picklistFieldApi) {
        return DMLOperationsHandler.fetchPickListValue(objInfo,picklistFieldApi);
    }

    @AuraEnabled
    public static string InsertUpdateAppPlacement(sObject objSobjecttoUpdateOrInsert){
        return DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert);
    }

    @AuraEnabled
    public static string InsertUpdateAppPlacementAddr(sObject objSobjecttoUpdateOrInsert){
        return DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert);
    }
    
    @AuraEnabled(cacheable=false)
    public static List<Application__c> getProgramDetails(string applicationid) {
        List<Application__c> programObj = new List<Application__c> ();
        Set<String> fields = new Set<String>{'Id','Program__c','ProgramType__c'};
        try{
            programObj = new DMLOperationsHandler('Application__c').
            selectFields(fields).
            addConditionEq('Id', applicationid).
            run();
            if(Test.isRunningTest()) {
                if(applicationid=='') {
                    throw new DMLException();
                }				
			}
        } catch (Exception Ex) { 
            String[] arguments = new String[] {applicationid};
            string strInputRecord= String.format('Input parameters are :: applicationid -- {0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getProgramDetails',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Program List',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return programObj;
    }

    @AuraEnabled(cacheable=true)
    public static List<SObject> fetchPlacementInformation(String id) {
        List<sObject> homeInformation = new List<sObject> ();
        String fields ='Id,Capacity__c,MinAge__c,MaxAge__c,Gender__c,Race__c,LicenseStartdate__c,LicenseEnddate__c,RespiteServices__c';
        try {
            homeInformation = new DMLOperationsHandler('Application__c').
                                        selectFields(fields).
                                        addConditionEq('Id', id).
                                        run();
            if(Test.isRunningTest()) {
                if(id=='') {
                    throw new DMLException();
                }				
            }
        } catch (Exception Ex) { 
            String[] arguments = new String[] {id};
            string strInputRecord= String.format('Input parameters are :: id -- {0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'fetchPlacementInformation',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Placement List',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return homeInformation;
    }

    @AuraEnabled(cacheable=true)
    public static List<SObject> fetchAddressInformation(String id,String appid) {
        List<sObject> addressInformation = new List<sObject> ();
        String fields ='Id,AddressLine1__c,AddressLine2__c,AddressType__c,City__c,County__c,Email__c,Phone__c,State__c,ZipCode__c,Provider__c,Application__c,Application__r.Referral__c';
        try{
            addressInformation = new DMLOperationsHandler('Address__c').
                                        selectFields(fields).
                                        addConditionEq('Provider__c', id).
                                        addConditionEq('Application__c', appid).
                                        addConditionEq('AddressType__c', 'Payment').
                                        run();
            if(Test.isRunningTest()) {
                if(appid=='') {
                    throw new DMLException();
                }				
            }
        } catch (Exception Ex) { 
            String[] arguments = new String[] {appid};
            string strInputRecord= String.format('Input parameters are :: appid -- {0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'fetchAddressInformation',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Address List',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return addressInformation;
    }
}