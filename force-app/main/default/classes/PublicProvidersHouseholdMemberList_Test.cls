/**
     * @Author        : Sindhu Venkateswarlu
     * @CreatedOn     : June 25,2020
     * @Purpose       :Test Methods to unit test PublicProvidersHouseholdMemberList class in PublicProvidersHouseholdMemberList.cls
     **/

    @isTest
    public with sharing class PublicProvidersHouseholdMemberList_Test {
        @isTest static void testgetContactHouseHoldMembersDetails (){
            Actor__c InsertobjNewActor = new Actor__c(Role__c = 'test Account');
            insert InsertobjNewActor;
            Contact InsertobjContact = new Contact(LastName = 'Name');
                  insert InsertobjContact;
                  try {
                    PublicProvidersHouseholdMemberList.getContactHouseHoldMembersDetails(null);

                  } catch (Exception Ex) {
                      
                  }
              PublicProvidersHouseholdMemberList.getContactHouseHoldMembersDetails(InsertobjNewActor.id);
              try{
                PublicProvidersHouseholdMemberList.getContactImgAsBas64Image('');
               }
            catch(Exception Ex){
            
               }
               PublicProvidersHouseholdMemberList.getContactImgAsBas64Image(InsertobjContact.id);

    }
}
