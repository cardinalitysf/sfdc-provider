/**
 * @Author        : Sindhu 
 * @CreatedOn     : July28 ,2020
 * @Purpose       : Test coverage for CpaClearanceMedicalInfo  class

 **/
@isTest
public class CpaClearanceMedicalInfo_Test {
@isTest static void testgetCPAHomeHouseHoldMembersDetails() {
    CPAHome__c CPAHomes=new CPAHome__c();
    insert CPAHomes;
    Actor__c Actor = new Actor__c(CPAHomes__c=CPAHomes.Id);
         insert Actor;
         ProviderRecordQuestion__c ProviderRecordQuestion = new ProviderRecordQuestion__c(Actor__c=Actor.Id,CPAHomes__c=CPAHomes.Id);
         insert ProviderRecordQuestion;
         ReferenceValue__c ReferenceValue = new ReferenceValue__c(Type__c='Clearance');
         insert ReferenceValue;
         ProviderRecordAnswer__c ProviderRecordAnswer = new ProviderRecordAnswer__c(AsPerStateRegulations__c='Yes',IsResultsinRecord__c='Yes',ProviderRecordQuestion__c=ProviderRecordQuestion.Id,Actor__c=Actor.Id);
         insert ProviderRecordAnswer;
        //  List<ContentVersion> cvList =new List<ContentVersion>(ContentDocumentId=ReferenceValue.Id);
        //  insert cvList;

    

    try
        {
            CpaClearanceMedicalInfo.getCPAHomeHouseHoldMembersDetails(null);
        }
        catch(Exception Ex)
        {
        }               
        CpaClearanceMedicalInfo.getCPAHomeHouseHoldMembersDetails(Actor.Id);  
        
        try{
            CpaClearanceMedicalInfo.getCPARecordDetails(CPAHomes.Id,Actor.Id);
        }catch(Exception Ex)
        {
        } 
        
        string str = '[{"Id":"'+ProviderRecordAnswer.Id+'","ProviderRecordQuestion__c":"'+ProviderRecordAnswer.ProviderRecordQuestion__c+'","RequestDate__c":"2020-06-10","ResultDate__c":"2020-06-10","AsPerStateRegulations__c":"'+ProviderRecordAnswer.AsPerStateRegulations__c+'","IsResultsinRecord__c":"'+ProviderRecordAnswer.IsResultsinRecord__c+'"}]';

        

        try{
            CpaClearanceMedicalInfo.bulkUpdateRecordAns(null,null);

            }catch(Exception Ex){
            {
        }
            
            // CpaClearanceMedicalInfo.providerRecordQuestionAnswerWrapper appParse1= new CpaClearanceMedicalInfo.providerRecordQuestionAnswerWrapper('test');        


}
        
}
}