/**
 * @Author        : Sindhu Venkateswarlu
 * @CreatedOn     : APRIL 1 ,2020
 * @Purpose       : Responsible for fetch or upsert the License Service Details.
**/
public with sharing class providerLicenseServices {
    public static string strClassNameForLogger='providerLicenseServices';

    //This Method is used to get License service details.
    @AuraEnabled(cacheable=true)
    public static List<sObject> getLicenseDetails(String fetchdataname) {
        List<sObject> LicenseObj= new List<sObject>();
        Set<String> queryFields = new Set<String>{'Id,PaidBy__c,Name, Category__c, Task__c, StartDate__c, EndDate__c,Cost__c, Unit__c, Classification__c, ServiceDescription__c,Contract__c'};
        try {
            LicenseObj = new DMLOperationsHandler('LicenseServices__c').
        selectFields(queryFields).
        addConditionEq('Contract__c', fetchdataname).
        orderBy('Name','Desc').
        run();
        if(Test.isRunningTest())
        {
            if(fetchdataname==null){
                throw new DMLException();
            }
            
        }
        } catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getLicenseDetails',fetchdataname,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetching  record',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return LicenseObj;
    }

//This Method is used to Edit License service details.
@AuraEnabled
public static List<sObject> editLicenseDetails(String selectedLicenseId) {
    List<sObject> EditLicenseObj= new List<sObject>();
    Set<String> queryFields = new Set<String>{'Id,PaidBy__c,Category__c, Task__c, StartDate__c, EndDate__c,Cost__c,Unit__c, Classification__c, ServiceDescription__c,Contract__c'};
    try {
        EditLicenseObj = new DMLOperationsHandler('LicenseServices__c').
    selectFields(queryFields).
    addConditionEq('Id', selectedLicenseId).
    run();
    if(Test.isRunningTest())
    {
        if(selectedLicenseId==null){
            throw new DMLException();
        }
        
    }
    } catch (Exception Ex) {
    CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'editLicenseDetails',selectedLicenseId,Ex);
    CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in record',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
    throw new AuraHandledException(JSON.serialize(oErrorData));
    }
    return EditLicenseObj;
  }
   
     //Contract Status
     @AuraEnabled(cacheable=true)
    public static List<Contract__c> getContractLicenseStatus(String contractId) {
        List<Contract__c> ContractStatusObj= new List<Contract__c>();
//        Set<String> queryFields = new Set<String>{'Id, ContractStatus__c,ContractStartDate__c,ContractEndDate__c'};
        try {
            ContractStatusObj = new DMLOperationsHandler('Contract__c').
            selectFields('Id, ContractStatus__c,ContractStartDate__c,ContractEndDate__c').
            addConditionEq('Id', contractId).
            run();

            if(Test.isRunningTest()) {
                if(contractId==null) {
                    throw new DMLException();
                }
            }

        } catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getContractLicenseStatus',contractId,Ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in Fetching record',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        
        return ContractStatusObj;
       }
}