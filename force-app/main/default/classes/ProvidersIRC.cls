/**
 * @Author        : K.Sundar
 * @CreatedOn     : APRIL 19 ,2020
 * @Purpose       : Responsible for fetch or upsert the IRC Rate Details.
**/

public with sharing class ProvidersIRC {
    //Get All IRC Rate Details for Provider
    @AuraEnabled(cacheable = true)
    public static List<SObject> getAllIRCRateDetails (String providerId) {
        String queryFields = 'Id, Name, RateType__c, LicenseType__c, IRCBedCapacity__c, StartDate__c, EndDate__c, PerDiemRate__c, Monthly__c, Annual__c, RateCode__c, Status__c, LicenseType__r.Name, LicenseType__r.ProgramName__c, LicenseType__r.ProgramType__c';
        List<sObject> ircRateObj = new DMLOperationsHandler('IRCRates__c').
                                    selectFields(queryFields).
                                    addConditionEq('Provider__c', providerId).
                                    addConditionEq('Status__c', 'Active').
                                    orderBy('Id', 'Desc').
                                    run();
        return ircRateObj;
    }

    //Get Selected Row IRC Rate Details for Provider
    @AuraEnabled
    public static List<SObject> editIRCRateDetails (String selectedIRCRateId) {
        String queryFields = 'Id, Name, RateType__c, LicenseType__c, IRCBedCapacity__c, StartDate__c, EndDate__c, PerDiemRate__c, Monthly__c, Annual__c, RateCode__c, Status__c, LicenseType__r.Name, LicenseType__r.ProgramName__c, LicenseType__r.ProgramType__c';
        List<sObject> selectedIRCRateObj = new DMLOperationsHandler('IRCRates__c').
                                    selectFields(queryFields).
                                    addConditionEq('Id', selectedIRCRateId).
                                    run();
        return selectedIRCRateObj;
    }

    //Get All Active License Types for Provider
    @AuraEnabled(cacheable = true)
    public static List<SObject> getAvailableLicenseTypesForProvider (String providerId) {
        String queryFields = 'Id, Name, ProgramName__c, ProgramType__c, Status__c';
        List<sObject> licenseTypeObj = new DMLOperationsHandler('License__c').
                                    selectFields(queryFields).
                                    addConditionEq('Application__r.Provider__c', providerId).
                                    run();
        return licenseTypeObj;
    }

    //Get All Expired IRC Rates of Selected Row ID for Provider
    @AuraEnabled
    public static List<SObject> ircRateHistoryForId (String selectedIRCRateLicenseType) {
        String queryFields = 'Id, Name, LicenseType__c, IRCBedCapacity__c, StartDate__c, EndDate__c, PerDiemRate__c,  RateCode__c, Status__c, LicenseType__r.Name, LicenseType__r.ProgramType__c';
        List<sObject> ircRateHistoryObj = new DMLOperationsHandler('IRCRates__c').
                                    selectFields(queryFields).
                                    addConditionEq('LicenseType__c', selectedIRCRateLicenseType).
                                    addConditionEq('Status__c', 'Expired').
                                    orderBy('Name', 'Desc').
                                    run();
        return ircRateHistoryObj;
    }

    //Get last added IRC Code from IRC Rates Object
    @AuraEnabled 
    public static List<SObject> getIrcRateCode () {
        String queryFields = 'Id, Name, RateCode__c, Status__c';
        List<sObject> rateCodeObj = new DMLOperationsHandler('IRCRates__c').
                                    selectFields(queryFields).
                                    addConditionNotEq('Provider__c', NULL).
                                    orderBy('RateCode__c', 'Desc').
                                    setLimit(1).
                                    run();
        return rateCodeObj;
    }

    //Get Contract Status to Freeze the IRC Rate Screen
    @AuraEnabled(cacheable = true)
    public static List<SObject> getProviderLicenseContractStatus (String selectedIRCRateLicenseId) {
        String queryFields = 'Id, ContractStatus__c';
        List<sObject> contractStatusObj = new DMLOperationsHandler('Contract__c').
                                    selectFields(queryFields).
                                    addConditionEq('License__c', selectedIRCRateLicenseId).
                                    run();
        return contractStatusObj;
    }
}