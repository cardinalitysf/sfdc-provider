public with sharing class ChatBotHandler {
    @AuraEnabled(cacheable=false)
    public static string postChatText(string schatText){

        HttpResponse response;
        try
        {
      
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('http://13.233.73.238:3000/chat/');
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json;charset=UTF-8');
        request.setBody('{"chatText":"' + schatText + '"}');
        response = http.send(request);
     
       
        }
        catch (Exception Ex)
        {
        }
    // return '';
        return   parseResponse(response.getBody());
    }

    private static string parseResponse(string strResponseFromAPIAI)
    {
        string strResponseToUI;
        APIAIResponse oResponse = APIAIResponse.parse(strResponseFromAPIAI);
        if(oResponse.Status.code==200)
        {
                Switch on oResponse.result.action {
                        when 'CheckStatus' {                   
                           strResponseToUI= queryDBForTypes(oResponse.result.parameters.AppNumber);
                        } 
                        when 'SearchByProviderNames'{                         
                            strResponseToUI=queryDBForNames(oResponse.result.parameters.ProviderName);

                        }
                        when 'ReferralProgram'{
                            strResponseToUI=queryDBForPrograms(oResponse.result.parameters.programtype);

                        }
                        when 'input.unknown'{
                            strResponseToUI=  oResponse.result.fulfillment.speech;
                        }
                        when 'input.welcome'{
                            strResponseToUI=  oResponse.result.fulfillment.speech;
                        }
                        when else {
                            strResponseToUI= oResponse.result.fulfillment.speech;
                        }                     
        }
            
        }
        else
        {
        }

        return strResponseToUI;
    }
    private static string  queryDBForTypes(String appNumber)
    {
        
        string strResponseToUI;
      
                Case oCase=new Case();
                try
                {
                 oCase = (case) new DMLOperationsHandler('Case').
                                     selectFields('Id, Status').
                                     addConditionEq('CaseNumber', appNumber).
                                     fetch();
                  strResponseToUI = 'Status of referral # '+appNumber+' is ' + oCase.Status;
                }
               catch(Exception Ex)
                {
                 strResponseToUI = 'Sorry ,I am not able to find any status for referral # '+appNumber;
                }
               
        return strResponseToUI;
    }
    private static string  queryDBForNames(String providerName)
    {        
        string strResponseToUI;
        AggregateResult  result =  new DMLOperationsHandler('Account').
                                    addConditionLike('Name', '%'+providerName+'%'). 
                                    count('Name', 'countName').
                                    aggregate()[0];
    
       Integer countName = (Integer)result.get('countName');

       strResponseToUI='There are '+countName+' provider(s) with name '+providerName;
    
        return strResponseToUI;
    }
    private static string  queryDBForPrograms(String programName)
    {        
        string strResponseToUI;
        AggregateResult  result =  new DMLOperationsHandler('Case').
                                    addConditionLike('Program__c', '%'+programName+'%'). 
                                    count('Program__c', 'countName').
                                    aggregate()[0];
    
       Integer countName = (Integer)result.get('countName');

       strResponseToUI='There are '+countName+' referrals for '+programName;
    
        return strResponseToUI;
    }
}