public with sharing class publicProviderDashboard {
  public static string strApexDebLogClassName ='publicProviderDashboard';
    @AuraEnabled(cacheable=true)
    public static List<PublicProviderWrapper> getpublicProviderDashboard(String searchString) {
        String searchKey = '%' + searchString + '%';
        // List<Actor__c> getSanctionsTableData= new List<Actor__c>();

        List<Actor__c>  lstContact = new DMLOperationsHandler('Actor__c').
                                        selectFields('Actor__c.Contact__r.Name,Referral__c,Role__c').
                                        selectFields('Actor__c.Referral__r.Id,Actor__c.Referral__r.CaseNumber,Actor__c.Referral__r.ProgramType__c,Actor__c.Referral__r.Program__c,Actor__c.Referral__r.Status').
                                        selectFields('Actor__c.Referral__r.Account.ProviderType__c,Actor__c.Referral__r.Account.Id').
                                        addConditionEq('Actor__c.Referral__r.Account.ProviderType__c', 'Public').
                                        addConditionLike('Actor__c.Referral__r.CaseNumber',searchKey).  
                                        orderBy('Actor__c.Referral__r.CaseNumber', 'DESC').   
                                        run();
       
        Set<id> oset= new Set<id>();
        Map<Integer,Actor__c> mapContacts= new Map<Integer,Actor__c>();
        Integer intId=0;
        List<PublicProviderWrapper> objWrapper=new List<PublicProviderWrapper>();
        for(Actor__c oContact : lstContact)
        {  
                oset.add(oContact.Referral__c);
                mapContacts.put(intId,oContact);
                intId=intId+1;       
        }
        Map<Id,String> mapType= new Map<Id,String>();

        for(Integer key:mapContacts.keySet()){
        if(mapContacts.get(key).Role__c == ProviderConstants.COAPPLICANT)
          mapType.put(mapContacts.get(key).Referral__c,mapContacts.get(key).Contact__r.Name);  
        }
        for(Integer key:mapContacts.keySet()){
            if(mapContacts.get(key).Role__c == ProviderConstants.APPLICANT)
            {     
                PublicProviderWrapper oWrapper= new PublicProviderWrapper();
                oWrapper.providerId = mapContacts.get(key).Referral__r.Account.Id;
                oWrapper.ReferralID=mapContacts.get(key).Referral__c;
                oWrapper.ReferralNumber=mapContacts.get(key).Referral__r.CaseNumber;
                oWrapper.ReferralType =mapContacts.get(key).Referral__r.Account.ProviderType__c;
                oWrapper.Program =mapContacts.get(key).Referral__r.Program__c;
                oWrapper.ProgramType =mapContacts.get(key).Referral__r.ProgramType__c;
                oWrapper.Applicant =mapContacts.get(key).Contact__r.Name;
                oWrapper.CoApplicant =  mapType.get(mapContacts.get(key).Referral__c);
                oWrapper.Status= mapContacts.get(key).Referral__r.Status=='Draft'?'Pending': mapContacts.get(key).Referral__r.Status;       
                objWrapper.add(oWrapper);
              
            }
        }
        List<Case> lstCase=   [select Id,Casenumber,
        ProgramType__c,Program__c,Account.ProviderType__c,Status, Account.Id from 
        Case where Id not in :oset and  Account.ProviderType__c = 'Public' AND CaseNumber LIKE :searchKey ORDER BY Case.CaseNumber DESC ];
         for(Case oCase : lstCase)
        {
                PublicProviderWrapper oWrapper= new PublicProviderWrapper();
                oWrapper.ReferralID=oCase.Id;
                oWrapper.providerId = oCase.Account.Id;
                oWrapper.ReferralNumber=oCase.Casenumber;
                oWrapper.ReferralType =oCase.Account.ProviderType__c;
                oWrapper.Program = oCase.Program__c;
                oWrapper.ProgramType = oCase.ProgramType__c;
                oWrapper.Applicant = '-';
                oWrapper.CoApplicant =  '-';
                oWrapper.Status= oCase.Status=='Draft'?'Pending':oCase.Status;       
                objWrapper.add(oWrapper);              
        }
        return objWrapper;
    }

    public with sharing  class PublicProviderWrapper
      {
          @AuraEnabled public string ReferralID ;
          @AuraEnabled public String providerId;
          @AuraEnabled public string ReferralNumber ;
          @AuraEnabled public string ReferralType;
          @AuraEnabled public string Program ;
          @AuraEnabled public string ProgramType ;
          @AuraEnabled public string Applicant;
          @AuraEnabled public string CoApplicant;
          @AuraEnabled public string Status;
        
      }

    @AuraEnabled(cacheable=true)
    public static List<Training__c> getTrainingData(String searchString){
      String searchKey = '%' + searchString + '%';
      List<Training__c> TrainingData = new List<Training__c>();

        try {
          TrainingData = new DMLOperationsHandler('Training__c').
          selectFields('Id,Name,Duration__c,StartTime__c,EndTime__c,Type__c,Date__c,SessionType__c, Status__c, NoofTraineeAssigned__c').
          addSubquery(
          DMLOperationsHandler.subquery('Meeting_Info__r').
          selectFields('Id,Name,SessionNumber__c, Date__c,MeetingStatus__c, Duration__c, StartTime__c, EndTime__c')
            ).
          addConditionLike('Name', searchKey). 
          addConditionEq('RecordType.Name', 'Public').
          orderBy('Name', 'Desc').
          run();

          if(Test.isRunningTest()) {
              if(searchString == '') {
                  throw new DMLException();
              }
          }

        } catch (Exception e) {
          CustomAuraExceptionData.LogIntoObject(strApexDebLogClassName,'getTrainingData', 'searchString: ' + searchString, e);
          CustomAuraExceptionData errorData=new CustomAuraExceptionData('getTrainingData Data',e.getMessage() , CustomAuraExceptionData.type.Informational.name());
          throw new AuraHandledException(JSON.serialize(errorData));
        }
        return TrainingData;

      // List<Training__c> TrainingData = new DMLOperationsHandler('Training__c').
      //   selectFields('Id,Name,Duration__c,StartTime__c,EndTime__c,Type__c,Date__c,SessionType__c, Status__c, NoofTraineeAssigned__c').
      //   addSubquery(
      //       DMLOperationsHandler.subquery('Meeting_Info__r').
      //       selectFields('Id,Name,SessionNumber__c, Date__c,MeetingStatus__c, Duration__c, StartTime__c, EndTime__c')
      //         ).
      //       addConditionLike('Name', searchKey). 
      //       addConditionEq('RecordType.Name', 'Public').
      //       orderBy('Name', 'Desc').
      //   run();
      //         return TrainingData;
    }

    @AuraEnabled(cacheable=true)
    public static List<Contact> getAllInstructorDetails() {
      List<Contact> getAllInstructorDetailsData = new List<Contact>();

      try {
        getAllInstructorDetailsData = new DMLOperationsHandler('Contact').
        selectFields('Id, Name, FirstName, LastName').
        addConditionEq('RecordType.Name', 'Instructor'). 
        orderBy('Name', 'Desc').                           
        run();

        if(Test.isRunningTest()) {
          Integer intTest = 1/0;
      }

      } catch(Exception e) {
        CustomAuraExceptionData.LogIntoObject(strApexDebLogClassName,'getAllInstructorDetails', null, e);
        CustomAuraExceptionData errorData=new CustomAuraExceptionData('getAllInstructorDetails Data',e.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(errorData));
      }
      return getAllInstructorDetailsData;

          // List<SObject> instructorObj=  new DMLOperationsHandler('Contact').
          //                          selectFields(queryFields).
          //                          addConditionEq('RecordType.Name', 'Instructor'). 
          //                          orderBy('Name', 'Desc').                           
          //                          run();
          //  return instructorObj;
      }
}