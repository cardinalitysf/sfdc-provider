/**
     * @Author        : Vijayaraj M
     * @CreatedOn     : June 25,2020
     * @Purpose       :Test Methods to unit test for PublicProvidersHomeInformation.cls
     * @Updated on    :June 20, 2020
     * @Updated by    :Janaswini Unit Test Updated methods with try catch
     **/
    
    @isTest
    private with sharing class PublicProvidersHomeInformation_Test {
    @isTest static void testPublicProvidersHomeInformation_Test() {
        ReferenceValue__c InsertobjNewReference=new ReferenceValue__c(Description__c = 'XYZ'); 
        insert InsertobjNewReference;
        HomeInformation__c InsertobjNewHomeInformation=new HomeInformation__c(SwimmingPool__c = 'XYZ'); 
        insert InsertobjNewHomeInformation;
        Address__c InsertobjNewAddress=new Address__c(AddressLine1__c = 'XYZ'); 
        insert InsertobjNewAddress;   
        
        try {
            PublicProvidersHomeInformation.fetchHomeInformation(null);
        }
        
        catch(Exception ex) {
               
        }
        PublicProvidersHomeInformation.fetchHomeInformation(InsertobjNewHomeInformation.Id);
        try {
            PublicProvidersHomeInformation.fetchAddressInformation(null);
        }
        
        catch(Exception ex){
               
        }
        
        PublicProvidersHomeInformation.fetchAddressInformation(InsertobjNewAddress.Id);
        PublicProvidersHomeInformation.getStateDetails();           
        try{
            Map<String, List<DMLOperationsHandler.FetchValueWrapper>>  objgetProfitValues = PublicProvidersHomeInformation.getMultiplePicklistValues(InsertobjNewHomeInformation.Provider__c,'XYZ');
          }
         catch(Exception e){
        }              

    }
    
    @isTest static void testPublicProvidersHomeInformationException()
    {   
        PublicProvidersHomeInformation.fetchHomeInformation('');
        PublicProvidersHomeInformation.fetchAddressInformation('');       
    }
}
