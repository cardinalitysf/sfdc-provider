        /**
         * @Author        : Tamilarasan G
         * @CreatedOn     : June 29 ,2020
         * @Purpose       : Responsible for all the actions in Case object.
         * @Updated       : Naveen
         **/
        @istest
        public class CaseService_Test {

        static testmethod void testmethod1(){
            Account InsertobjNewAccount=new Account(Name = 'Test Account',AgeGroup__c='0 - 2',ProviderType__c='Private');
            insert InsertobjNewAccount;
            Application__c applicationListInsert= new Application__c(Status__c='Pending');
            insert applicationListInsert;
            Deficiency__c DeficiencyListInsert= new Deficiency__c(Status__c='Pending');
            insert DeficiencyListInsert;
            License__c LicenseList =new License__c();        
            LicenseList.Application__c=applicationListInsert.Id;
            insert LicenseList;

            List<Case> caseList = new List<Case>();
            Case caseListInsert= new Case(Status='Pending',ProgramType__c='RCC',AccountId=InsertobjNewAccount.Id);
            caseListInsert.CAPNumber__c=DeficiencyListInsert.Id;
            caseListInsert.License__c=LicenseList.Id;
            caseList.add(caseListInsert);
            insert caseList;

            Sanctions__c sanctionListInsert= new Sanctions__c(); 
            Id MyId2=Schema.Sobjecttype.Sanctions__c.getRecordTypeInfosByName().get('Suspension').getRecordTypeId();
            sanctionListInsert.RecordTypeId = MyId2;  
            sanctionListInsert.Complaint__c=caseList[0].Id;
            sanctionListInsert.Gender__c='Male';     
            insert sanctionListInsert;
            caseList[0].Sanctions__c=sanctionListInsert.Id;
            update caseList;

            CaseService cs = new CaseService();
            try {
            cs.createApplications(null); 
            } catch (Exception ex) {

            }
            cs.createApplications(caseList);
            try {
            cs.updateStatusDependingOnSanctions(null); 
            } catch (Exception ex) {

            }
            cs.updateStatusDependingOnSanctions(caseList);
            }
        }
