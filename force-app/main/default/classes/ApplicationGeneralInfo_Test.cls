/**
 * @Author        : Sindhu Venkateswarlu
 * @CreatedOn     : May 6,2020
 * @Purpose       :Test Methods to unit test for ApplicationGeneralInfo.cls
 * @Updated on    :June 20, 2020
 * @Updated by    :Janaswini Unit Test Updated methods with try catch
 **/

@isTest
private class ApplicationGeneralInfo_Test {
  @isTest
  static void testfetchDataForAddOrEdit() {
    Application__c InsertobjNewApplication = new Application__c(
      Status__c = 'Pending'
    );
    insert InsertobjNewApplication;
    Address__c acc = new Address__c();
    insert acc;
    try {
      ApplicationGeneralInfo.fetchDataForAddOrEdit(null);
    } catch (Exception ex) {
    }

    ApplicationGeneralInfo.fetchDataForAddOrEdit('abc');

    Account InsertobjNewAccount = new Account(Name = 'test Account');
    insert InsertobjNewAccount;
    Address__c InsertobjNewAddress = new Address__c(
      AddressLine1__c = 'InsertobjNewAccount'
    );
    insert InsertobjNewAddress;
    //TestDataFactory.createTestContacts(1,InsertobjNewAccount.id,true);
    try {
      ApplicationGeneralInfo.fetchDataForAddressSite(null, null);
    } catch (Exception ex) {
    }
    ApplicationGeneralInfo.fetchDataForAddressSite(
      InsertobjNewApplication.Id,
      InsertobjNewAddress.Id
    );
    try {
      ApplicationGeneralInfo.fetchDataForAddressMain(null, null);
    } catch (Exception ex) {
    }
    ApplicationGeneralInfo.fetchDataForAddressMain(
      InsertobjNewApplication.Id,
      InsertobjNewAddress.Id
    );
    try {
      ApplicationGeneralInfo.fetchDataForAddressPayment(null, null);
    } catch (Exception ex) {
    }
    ApplicationGeneralInfo.fetchDataForAddressPayment(
      InsertobjNewApplication.Id,
      InsertobjNewAddress.Id
    );
  }
}
