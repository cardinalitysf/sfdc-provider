/***
 * @Author: Sindhu Venkateswarlu
 * @CreatedOn: July 1, 2020
 * @Purpose: Complaints Monitoring Add/List/Edit
 **/

public with sharing class ComplaintsMonitoringAdd {
    public static string strClassNameForLogger='ComplaintsMonitoringAdd';

    //This Class is used for inserting Add Monitoring.
    @AuraEnabled
    public static string InsertUpdateMonitoring(sObject objSobjecttoUpdateOrInsert){
        return DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert);
    }
    
    //This class is used to get Monitoring details.
    @AuraEnabled(cacheable=false)
    public static List<Monitoring__c> getMonitoringDetails(string monitoringid) {
        List<sObject> monitoringDetailsObj= new List<sObject>();
        Set<String> queryFields = new Set<String>{'Id','Name','Complaint__r.CaseNumber','Complaint__r.License__r.Name','Complaint__r.License__r.ProgramName__c','Complaint__r.License__r.ProgramType__c','SelectPeriod__c','SiteAddress__r.Id','SiteAddress__r.AddressLine1__c','SiteAddress__r.City__c','VisitationStartDate__c','VisitationEndDate__c','AnnouncedUnannouced__c','TimetoVisit__c','JointlyInspectedWith__c','Status__c'};
        try {
        monitoringDetailsObj = new DMLOperationsHandler('Monitoring__c').
        selectFields(queryFields).
        addConditionEq('Complaint__c', monitoringid).
        orderBy('Name', 'Desc').
        run();
        if(Test.isRunningTest())
        {
            if(monitoringid==null){
                throw new DMLException();
            }
            
        }
        } catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getMonitoringDetails',monitoringid,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetching record',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return monitoringDetailsObj;
    }

    //This class is used to get Adddress details
    @AuraEnabled(cacheable=false)
    public static List<case> getAddressDetails(string applicationid) {
       
        Set<String> fields = new Set<String>{'Id','Address__r.AddressType__c','Address__r.AddressLine1__c','Address__r.City__c','Address__r.County__c'};
        List<case> addressObj = new DMLOperationsHandler('case').
        selectFields(fields).
        addConditionEq('Id', applicationid).
        run();
        return addressObj;
    }

    //This class is used to get Program Details
    @AuraEnabled(cacheable=false)
    public static List<case> getProgramDetails(string caseId) {
        //select id,Program__c,ProgramType__c from Complaint__c
        Set<String> fields = new Set<String>{'Id','License__r.ProgramName__c','License__r.ProgramType__c','CaseNumber','License__r.Name'};
        List<case> programObj = new DMLOperationsHandler('case').
        selectFields(fields).
        addConditionEq('Id', caseId).
        run();
        return programObj;
    }
    
    //This class is used for edit Monitoring
    @AuraEnabled(cacheable=false)
    public static List<Monitoring__c> editMonitoringDetails(string editMonitoringDetails) {
        List<sObject> monitoringEditObj= new List<sObject>();
        Set<String> queryFields = new Set<String>{'Id','Name','Complaint__r.CaseNumber','Complaint__r.License__r.Name','Complaint__r.License__r.ProgramName__c','Complaint__r.License__r.ProgramType__c','SelectPeriod__c','SiteAddress__r.Id','SiteAddress__r.AddressLine1__c','SiteAddress__r.City__c','VisitationStartDate__c','VisitationEndDate__c','AnnouncedUnannouced__c','TimetoVisit__c','JointlyInspectedWith__c','Status__c'};
         try {
        monitoringEditObj = new DMLOperationsHandler('Monitoring__c').
        selectFields(queryFields).
        addConditionEq('Id', editMonitoringDetails).
        run();
        if(Test.isRunningTest())
        {
            if(editMonitoringDetails==null){
                throw new DMLException();
            }
            
        }
        } catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'editMonitoringDetails',editMonitoringDetails,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetching record',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return monitoringEditObj;
    }

    //This class is used for View Monitoring
    @AuraEnabled(cacheable=false)
    public static List<Monitoring__c> viewMonitoringDetails(String viewMonitoringDetails) {       
        List<sObject> monitoringObj= new List<sObject>();
        Set<String> queryFields = new Set<String>{'Id','Name','Complaint__r.CaseNumber','Complaint__r.License__r.Name','Complaint__r.License__r.ProgramName__c','Complaint__r.License__r.ProgramType__c','SelectPeriod__c','SiteAddress__r.Id','SiteAddress__r.AddressLine1__c','SiteAddress__r.City__c','VisitationStartDate__c','VisitationEndDate__c','AnnouncedUnannouced__c','TimetoVisit__c','JointlyInspectedWith__c','Status__c'};
        try {
        monitoringObj = new DMLOperationsHandler('Monitoring__c').
        selectFields(queryFields).
        addConditionEq('Id', viewMonitoringDetails).
        run();
        if(Test.isRunningTest())
        {
            if(viewMonitoringDetails==null){
                throw new DMLException();
            }
            
        }
        
        } catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'viewMonitoringDetails',viewMonitoringDetails,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetching record',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return monitoringObj;
    }

    //This class is used to get Monitoring Status
    @AuraEnabled(cacheable=true)
    public static List<Monitoring__c> getMonitoringStatus(String monitoringid) {
        Set<String> fields = new Set<String>{'Id','Status__c'};
        List<Monitoring__c> monitoringStatusObj = new DMLOperationsHandler('Monitoring__c').
        selectFields(fields).
        addConditionEq('Id', monitoringid).
        run();
        return monitoringStatusObj;
    }

    
}