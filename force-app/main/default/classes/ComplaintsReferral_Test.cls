/**
 * @Author        : Preethi Bojarajan
 * @CreatedOn     : July 20, 2020
 * @Purpose       : Test class for ComplaintsReferral.cls
 **/
@isTest
private class ComplaintsReferral_Test{
    @isTest
    private static void getProvidersList_Test(){
        Test.startTest();
        try {
            ComplaintsReferral.getProvidersList('a',null,'a');
        } catch(Exception e){}
        try {
            ComplaintsReferral.getProvidersList('Public',null,'a');
        } catch(Exception e){}
        try {
            ComplaintsReferral.getProvidersList('',null,'a');
        } catch(Exception e){}
        try {
            ComplaintsReferral.getProvidersList('a','a','a');
        } catch(Exception e){}
        try {
            ComplaintsReferral.getProvidersList('Public','a','a');
        } catch(Exception e){}
        try {
            ComplaintsReferral.getProvidersList(null,'a','a');
        } catch(Exception e){}
        try {
            ComplaintsReferral.getProvidersList('_Public','a','a');
        } catch(Exception e){}
        try {
            ComplaintsReferral.getProvidersList('','a','a');
        } catch(Exception e){}
        Test.stopTest();
    }
    @isTest
    private static void getCaseHeaderDetails_Test(){
        Test.startTest();
        try {
            ComplaintsReferral.getCaseHeaderDetails('a','Case');
            ComplaintsReferral.getCaseHeaderDetails('a','a');
            ComplaintsReferral.getCaseHeaderDetails('a', '');
            ComplaintsReferral.getCaseHeaderDetails('', '');
            ComplaintsReferral.getCaseHeaderDetails(null, '');
        } catch(Exception e){}
        Test.stopTest();
    }
    @isTest
    private static void getMultiplePicklistValues_Test() {
        Test.startTest();
        ComplaintsReferral.getMultiplePicklistValues('Case','ComplaintSource__c');
        Test.stopTest();
    }
    
    @isTest
    private static void fetchUserId_Test(){
        Test.startTest();
        try {
            ComplaintsReferral.fetchUserId('123');
            ComplaintsReferral.fetchUserId(null);
        } catch(Exception e){}
        Test.stopTest();
    }

    @isTest
    private static void getRecordType_Test(){
        Test.startTest();
        try {
            ComplaintsReferral.getRecordType('Case','Complaints');
        } catch(Exception e){}
        Test.stopTest();
    }

    @isTest
    private static void getStateDetails_Test(){
        Test.startTest();
        try {
            ComplaintsReferral.getStateDetails();
        } catch(Exception e){}
        Test.stopTest();
    }

    @isTest
    private static void getIdAfterInsertSOQL_Test(){
        Test.startTest();
        ComplaintsReferral.getIdAfterInsertSOQL(new Account(Id='001x00000000000AAA'));
        ComplaintsReferral.getIdAfterInsertSOQL(null);
        Test.stopTest();
    }

    @isTest
    private static void ComplaintsReferral_Test(){
        ComplaintsReferral obj = new ComplaintsReferral();
    }
}