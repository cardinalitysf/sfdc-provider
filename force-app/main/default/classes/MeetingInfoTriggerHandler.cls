public with sharing class MeetingInfoTriggerHandler implements ITriggerHandler{
	/**
	 * @Author        : V.S.Marimuthu
	 * @CreatedOn     : May 27 ,2020
	 * @Purpose       : Handler class which will do hand shake between trigger and task service
	 **/
	public Boolean IsDisabled(){
		// string blnTriggerSwitch = 'false';
		// List<Bypass_Switch_For_Trigger__mdt> triggerSwitch = [select
		//                                                             Trigger_Switch__c
		//                                                       from Bypass_Switch_For_Trigger__mdt];
		// for (Bypass_Switch_For_Trigger__mdt value : triggerSwitch){
		//     blnTriggerSwitch = value.Trigger_Switch__c;
		// }
		// return blnTriggerSwitch == 'false' ? false : true;
		return false;
	}

	public void BeforeInsert(List<Sobject> newItems){
	
	}

	public void BeforeUpdate(Map<id, sObject> newItems, Map<id, sObject> oldItems){
	
	}

	public void BeforeDelete(Map<id, sObject> oldItems){
	
	}

	public void AfterInsert(List<Sobject> newMap){
		
	}

	
	private void changeReferralStatusDependsOnMeetingStatus(Map<Id, sObject> newMap, Map<Id, sObject> oldMap){
		try{
		    Set<Id> lstRefId= new Set<Id>();
		    for(MeetingInfo__c objMeetingInfo : ((Map<Id,MeetingInfo__c>)newMap).values()){
		        //Create an old and new map so that we can compare values
		        MeetingInfo__c oldMeetingInfo = (MeetingInfo__c)oldMap.get(objMeetingInfo.ID);
                MeetingInfo__c newMeetingInfo = (MeetingInfo__c)newMap.get(objMeetingInfo.ID);  
                if(oldMeetingInfo.MeetingStatus__c != newMeetingInfo.MeetingStatus__c){                  
                    lstRefId.add(oldMeetingInfo.Referral__c);
		        }
               
		        
            }        
            Map<Id,String> objCompleteMaptoUpdate=new Map<Id,String>();
            Map<Id,String> objDNAMaptoUpdate=new Map<Id,String>();
            Map<Id,String> objScheduleMaptoUpdate=new Map<Id,String>();
            Map<Id,String> objCancelledMaptoUpdate=new Map<Id,String>();
		    if(lstRefId.size()>0){
                List<MeetingInfo__c> lstMeetingStatus = new DMLOperationsHandler('MeetingInfo__c').
                                    selectFields('Id,MeetingStatus__c,Referral__c').
                                    addConditionIn('Referral__c', lstRefId).
                                    run();                     
            
              if(lstMeetingStatus.size()>0){
                    for(MeetingInfo__c objMeetinfo : lstMeetingStatus)
                    {     

                        if(objMeetinfo.MeetingStatus__c==ProviderConstants.MEETINGINFO_COMPLETED)
                        {
                            objCompleteMaptoUpdate.put(objMeetinfo.Referral__c,objMeetinfo.MeetingStatus__c);
                        }
                         if(objMeetinfo.MeetingStatus__c==ProviderConstants.MEETINGINFO_DNA)
                        {
                            objDNAMaptoUpdate.put(objMeetinfo.Referral__c,objMeetinfo.MeetingStatus__c);
                        }

                        if(objMeetinfo.MeetingStatus__c==ProviderConstants.MEETINGINFO_SCHEDULED)
                        {
                            objScheduleMaptoUpdate.put(objMeetinfo.Referral__c,objMeetinfo.MeetingStatus__c);
                        }

                        if(objMeetinfo.MeetingStatus__c==ProviderConstants.MEETINGINFO_CANCELLED)
                        {
                            objCancelledMaptoUpdate.put(objMeetinfo.Referral__c,objMeetinfo.MeetingStatus__c);
                        }
                        // switch on objMeetinfo.MeetingStatus__c {
                        //     when ProviderConstants.MEETINGINFO_COMPLETED {
                        //         objCompleteMaptoUpdate.put(objMeetinfo.Referral__c,objMeetinfo.MeetingStatus__c);
                        //     }
                        //     when ProviderConstants.MEETINGINFO_DNA {		
                        //         objDNAMaptoUpdate.put(objMeetinfo.Referral__c,objMeetinfo.MeetingStatus__c);
                        //     }
                        //     when ProviderConstants.MEETINGINFO_SCHEDULED {		
                        //         objScheduleMaptoUpdate.put(objMeetinfo.Referral__c,objMeetinfo.MeetingStatus__c);
                        //     }
                        //     when ProviderConstants.MEETINGINFO_CANCELLED {		
                        //         objCancelledMaptoUpdate.put(objMeetinfo.Referral__c,objMeetinfo.MeetingStatus__c);
                        //     }                            
                        // }
                    }
              }
            if(objScheduleMaptoUpdate.size()>0)
            {
                for( Id remId : objScheduleMaptoUpdate.keySet())
                {
                    objCompleteMaptoUpdate.remove(remId);
                }
            }

            if(objScheduleMaptoUpdate.size()>0)
            {
                for( Id remId : objScheduleMaptoUpdate.keySet())
                {
                    objCompleteMaptoUpdate.remove(remId);
                }
            }

            if(objDNAMaptoUpdate.size()>0)
            {
                for( Id remId : objDNAMaptoUpdate.keySet())
                {
                    objCompleteMaptoUpdate.remove(remId);
                }
            }       
            List<Case> objListC= new List<Case>();          
            if(objCompleteMaptoUpdate.size()>0)
            {
                for( Id refId : objCompleteMaptoUpdate.keySet())
                {
                case oCase= new Case();
                oCase.id=refId;
                oCase.Status=ProviderConstants.CASE_TRAINING_STATUS_COMPLETED;
                objListC.add(oCase);
                }
            }
            if(objDNAMaptoUpdate.size()>0)
            {
                for( Id refId : objDNAMaptoUpdate.keySet())
                {
                case oCase= new Case();
                oCase.id=refId;
                oCase.Status=ProviderConstants.CASE_TRAINING_STATUS_UNATTENTED;
                objListC.add(oCase);
                }
            }
            if(objListC.size()>0)
            {
             DMLOperationsHandler.updateOrInsertSOQLForList(objListC);
            }
        }

		}
		catch(Exception Ex)
		{
		}
	}

	

	//call the method to update the values
	public void AfterUpdate(Map<Id, sObject> newMap, Map<Id, sObject> oldMap){
		changeReferralStatusDependsOnMeetingStatus(newMap,oldMap);
	
	}

	public void AfterDelete(Map<id, sObject> oldMap){
		
	}

	public void AfterUndelete(List<Sobject> newMap){
		
	}
}