public with sharing class providerRecordFetching {
    @AuraEnabled(cacheable=true)
    public static List<SObject> getRecordDetails(String providerId, String type) {
        String model = '';
        String fields ='';
        String cond = '';
        if(type == 'staffTab'){
            model = 'Application__c';
            fields ='SUM(Capacity__c)';
            String cond2 = 'Approved';
            cond = 'provider__C = \''+ providerId +'\'' +' and Status__c= \''+ cond2+'\'';
        } else if(type == 'plantTab'){
            model = 'Contact';
            fields ='Id';    
            cond = 'AccountId = \''+ providerId +'\'';
        } else if(type == 'officeTab'){
            model = 'Application__c';
            fields ='Id, Name, Program__c, ProgramType__c, Status__c'; 
            cond = 'Provider__c = \''+ providerId +'\''+' ORDER BY Id DESC';
        }        
        return DMLOperationsHandler.selectSOQLWithConditionParameters(model,fields,cond);

    }
}