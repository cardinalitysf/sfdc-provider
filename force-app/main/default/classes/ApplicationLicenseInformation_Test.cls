/**
     * @Author        : Jayachandran s
     * @CreatedOn     : June 08,2020
     * @Purpose       :Test Methods to unit test for ApplicationLicenseInformation.cls
**/


    @isTest private class ApplicationLicenseInformation_Test
    {
       Static testmethod void applicationLicenseTest()
       {
           List<Account> accList =  TestDataFactory.testAccountData();
           insert accList;
           List<Application__c> applicationList= TestDataFactory.TestApplicationData(5,'pending');
           insert applicationList;

           Application__c InsertobjNewLicenseInformation=new Application__c(ProgramType__c = 'XYZ');
           Database.UpsertResult Upsertresults = Database.upsert(InsertobjNewLicenseInformation);
           string strAppId= Upsertresults.getId();
             //Create content version
             ContentVersion contentVersion = new ContentVersion(
               Title = 'Cardinality',
               PathOnClient = 'Cardinality.jpg',
               VersionData = Blob.valueOf('ApplicationLicenseInformation_Test')
             );
             insert contentVersion;
             List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
             //create ContentDocumentLink  record
             ContentDocumentLink cdl = New ContentDocumentLink();
             cdl.LinkedEntityId = strAppId;
             cdl.ContentDocumentId = documents[0].Id;
             insert cdl;
           
           Test.startTest();

           try{
           ApplicationLicenseInformation.getapplicationDetails(null);
           }catch(Exception Ex){

           }
           ApplicationLicenseInformation.getapplicationDetails(applicationList[0].Id);

            try{
           ApplicationLicenseInformation.getSignatureAsBas64Image(null);
           }catch(Exception Ex){

           }
           ApplicationLicenseInformation.getSignatureAsBas64Image(strAppId);

           string app =  ApplicationLicenseInformation.UpdateLicenseEndDate(applicationList[0]);
            try{
           ApplicationLicenseInformation.getLicensesId(null);
           }catch(Exception Ex){

           }
           ApplicationLicenseInformation.getLicensesId(applicationList[0].Id);
           Test.stopTest();
       } 
        

    }
