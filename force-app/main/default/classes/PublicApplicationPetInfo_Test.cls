/**
 * @Author        : Tamilarasan G
 * @CreatedOn     : June 8 ,2020
 * @Purpose       : Test Methods to unit test Public Application Pet Info class
 * @UpadtedBy     : JayaChandran Unit Test Updated methods with try catch.
 * @Date          : 17 july,2020
 **/
@IsTest
    public class PublicApplicationPetInfo_Test {

        @IsTest static void testPublicApplicationPetInfo()
        {
            Contact InsertobjNewContact=new Contact(LastName = 'Cat'); 
            insert InsertobjNewContact;
      
            PublicApplicationPetInfo.SavePetRecordDetails(InsertobjNewContact);

            PublicApplicationPetInfo.getRecordType('Pet');

            try{
                PublicApplicationPetInfo.getApplicationPetInfo(null);
            }catch(Exception Ex) {

            }
            PublicApplicationPetInfo.getApplicationPetInfo(InsertobjNewContact.Id);

            try{
                PublicApplicationPetInfo.editPetDetails(null);
            }catch(Exception Ex) {

            }
            PublicApplicationPetInfo.editPetDetails(InsertobjNewContact.Id); 


        }

      

    }

