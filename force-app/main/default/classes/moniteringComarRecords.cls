/**
 * @Author        : Preethi Bojarajan
 * @CreatedOn     : Apr 6,2020
 * @Purpose       : Monitering Records for Staff / Plant and Office Inspection
 * @updatedBy     : Preethi Bojarajan
 * @updatedOn     : Jul 30 ,2020
 **/
public with sharing class moniteringComarRecords {
    public static string strClassNameForLogger='moniteringComarRecords';
     
    @AuraEnabled(cacheable=true)
        public static List<SObject> getRecordDetails(String moniteringId, String siteId, String recordType) {
            List<SObject> questions= new List<SObject>();
           try {
          
            Set<String> fields = new Set<String>{'Id, Monitoring__c, Site__r.AddressLine1__c, AnyNc__c, AnyPending__c, Status__c, LastModifiedBy.Name, Date__c, Name__r.FirstName__c, RecordTypeId'};
             questions =new DMLOperationsHandler('ProviderRecordQuestion__c').
            selectFields(fields).
            addConditionEq('Monitoring__c', moniteringId).
            addConditionEq('Site__c', siteId). 
            addConditionEq('RecordTypeId', recordType).  
            run();
            if(Test.isRunningTest())
			{
                if(moniteringId==null || siteId== null || recordType== null){
                    throw new DMLException();
                }
				
            }
            
           } catch (Exception Ex) {
            String[] arguments = new String[] {moniteringId ,siteId, recordType};
            string strInputRecord= String.format('Input parameters are :: moniteringId -- {0} :: siteId -- {1} :: recordType -- {1}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getRecordDetails',strInputRecord,Ex);
             CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Record Type based Get  Details',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
             throw new AuraHandledException(JSON.serialize(oErrorData));
        }
              return questions;
           
        }

        @AuraEnabled(cacheable=true)
        public static List<SObject> getQuestionsByType(String type) {
            List<SObject> questions= new List<SObject>();
            try {
                
                Set<String> fields = new Set<String>{'Id, Question__c, Regulations__c, Type__c, RecordTypeId, QuestionOrder__c'};
                questions =new DMLOperationsHandler('ReferenceValue__c').
                selectFields(fields).
                addConditionEq('Type__c', type).    
                orderBy('QuestionOrder__c', 'asc').  
                run();
                if(Test.isRunningTest())
                {
                    if(type==null ){
                        throw new DMLException();
                    }
                    
                }

            } catch (Exception Ex) {
                CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getQuestionsByType',type,Ex);
                 CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Question based on Type',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
                 throw new AuraHandledException(JSON.serialize(oErrorData));
            }
               return  questions;
        }

        @AuraEnabled
        public static void bulkAddRecordAns(String datas){
            List<ProviderRecordAnswer__c> listInsert = new List<ProviderRecordAnswer__c>();
            try {
                List<ProviderRecordAnswer__c> dataToInsert = (List<ProviderRecordAnswer__c>)System.JSON.deserialize(datas, List<ProviderRecordAnswer__c>.class);
                 listInsert = new List<ProviderRecordAnswer__c>();
                for(ProviderRecordAnswer__c p: dataToInsert){
                    ProviderRecordAnswer__c objTask = new ProviderRecordAnswer__c();
                    objTask = new ProviderRecordAnswer__c(
                        Comar__c = p.Comar__c,
                        Compliance__c = p.Compliance__c,
                        Date__c = p.Date__c,
                        Findings__c = p.Findings__c,
                        FrequencyofDeficiency__c = p.FrequencyofDeficiency__c,
                        ImpactofDeficiency__c = p.ImpactofDeficiency__c,
                        Scope_of_Deficiency__c = p.Scope_of_Deficiency__c,
                        ProviderRecordQuestion__c = p.ProviderRecordQuestion__c);
                    listInsert.add(objTask);
                }
                insert listInsert;
            } catch (Exception Ex) {
                CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'bulkAddRecordAns',datas,Ex);
                 CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Insert Bulk Recored Answer based on Type',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
                 throw new AuraHandledException(JSON.serialize(oErrorData));
              }
        }

        @AuraEnabled
        public static void bulkUpdateRecordAns(String datas){
            List<ProviderRecordAnswer__c> listUpdate = new List<ProviderRecordAnswer__c>();
            try {

            List<ProviderRecordAnswer__c> dataToInsert = (List<ProviderRecordAnswer__c>)System.JSON.deserialize(datas, List<ProviderRecordAnswer__c>.class);
             listUpdate = new List<ProviderRecordAnswer__c>();
            for(ProviderRecordAnswer__c p: dataToInsert){
                ProviderRecordAnswer__c objTask = new ProviderRecordAnswer__c();
                objTask = new ProviderRecordAnswer__c(
                    Id = p.Id,
                    Comar__c = p.Comar__c,
                    Compliance__c = p.Compliance__c,
                    Date__c = p.Date__c,
                    Findings__c = p.Findings__c,
                    FrequencyofDeficiency__c = p.FrequencyofDeficiency__c,
                    ImpactofDeficiency__c = p.ImpactofDeficiency__c,
                    Scope_of_Deficiency__c = p.Scope_of_Deficiency__c,
                    ProviderRecordQuestion__c = p.ProviderRecordQuestion__c);
                    listUpdate.add(objTask);
            }
            update listUpdate;
        }
        catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'bulkUpdateRecordAns',datas,Ex);
             CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Update Bulk Recored Answer ',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
             throw new AuraHandledException(JSON.serialize(oErrorData));
           }
        }

        @AuraEnabled(cacheable=true)
        public static List<SObject> getSiteId(String siteId) {
            List<SObject> questions= new List<SObject>();
           try {
        Set<String> fields = new Set<String>{'Id, AddressLine1__c'};
        questions = new DMLOperationsHandler('Address__c').
        selectFields(fields).
        addConditionEq('Id', siteId). 
        run();
        if(Test.isRunningTest())
        {
            if(siteId==null ){
                throw new DMLException();
            }
            
        }
           } catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getSiteId',siteId,Ex);
             CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Site Address Details',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
             throw new AuraHandledException(JSON.serialize(oErrorData));
        }
           return questions;
            
        }

        @AuraEnabled(cacheable=true)
        public static Id getRecordType(string name) {
            return CommonUtils.getRecordTypeIdbyName('ProviderRecordQuestion__c', name);
        }
        
        @AuraEnabled(cacheable=true)
        public static List<SObject> editRecordDet(String questionId) {
            List<SObject> questions= new List<SObject>();
            try {
                if(questionId != null && questionId != ''){
                    Set<String> fields = new Set<String>{'Id, Comar__R.Question__c, Comar__R.Regulations__c, Compliance__c, Date__c, Findings__c, FrequencyofDeficiency__c, ImpactofDeficiency__c, Scope_of_Deficiency__c, ProviderRecordQuestion__c'};
                    questions =new DMLOperationsHandler('ProviderRecordAnswer__c').
                                    selectFields(fields).
                                    addConditionEq('ProviderRecordQuestion__c', questionId).  
                                    run();
                                    if(Test.isRunningTest()) {
                                        if(questionId==null ){
                                            throw new DMLException();
                                        }
                                    }
                }
            } catch (Exception Ex) {
                CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'editRecordDet',questionId,Ex);
                 CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Edit Record Details',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
                 throw new AuraHandledException(JSON.serialize(oErrorData));
            }
            return questions;
        }
        

        @AuraEnabled(cacheable=false)
        public static List<Contact> applicationStaff(string newStaff) {
            List<SObject> questions= new List<SObject>();
        try {
              Set<String> fields = new Set<String>{'Account.ProviderId__c, FirstName__c, LastName__c'};
              questions =new DMLOperationsHandler('Contact').
              selectFields(fields).
              addConditionEq('AccountId', newStaff).  
              run();
             if(Test.isRunningTest())
             {
                if(newStaff==null ){
                 throw new DMLException();
              }
         }
    } catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'applicationStaff',newStaff,Ex);
         CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Staff from Provider',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
         throw new AuraHandledException(JSON.serialize(oErrorData));
       }
         return  questions;
       
   }

        @AuraEnabled(cacheable=true)
        public static List<SObject> getStaffFromApplicationId(String contactfromapp) {
            List<SObject> questions= new List<SObject>();
            try{

        Set<String> fields = new Set<String>{'Id, Staff__c, Staff__r.FirstName__c, Application__c'};
         questions =new DMLOperationsHandler('Application_Contact__c').
        selectFields(fields).
        addConditionEq('Application__c', contactfromapp).  
        run();
        if(Test.isRunningTest())
        {
            if(contactfromapp==null ){
                throw new DMLException();
            }
            
        }
     }catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getStaffFromApplicationId',contactfromapp,Ex);
         CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Staff from Application',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
         throw new AuraHandledException(JSON.serialize(oErrorData));
       }
       return  questions;   
           
      }
}