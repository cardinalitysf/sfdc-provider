/**
 * @Author        : Nandhini 
 * @CreatedOn     : JULY 23 ,2020
 * @Purpose       : Sanctions Suspension Screen
 * @UpdatedBy     : Sindhu
 * @UpdatedOn     : July 28,2020
 **/
@isTest
private with sharing class ComplaintsSanctionLimitation_Test {
    @isTest static void testSanctionLimitation() {
        Sanctions__c insertsanction = new Sanctions__c();
        insert insertsanction;
        Case insertcase = new Case();
        insert insertcase;
        try
        {
            ComplaintsSanctionLimitation.getSelectedSanctionData(null);
        }
        catch(Exception Ex)
        {
        }               
        ComplaintsSanctionLimitation.getSelectedSanctionData(insertsanction.id);    
        try
        {
            ComplaintsSanctionLimitation.getRecordType('');
        }
        catch(Exception Ex)
        {
        } 
        ComplaintsSanctionLimitation.getRecordType('Limitation');
        try
        {
            ComplaintsSanctionLimitation.getComplaintLicenseDetails(null);
        }
        catch(Exception Ex)
        {
        } 
        ComplaintsSanctionLimitation.getComplaintLicenseDetails(insertcase.id);
    }
}