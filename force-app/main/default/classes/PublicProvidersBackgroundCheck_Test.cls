/**
 * Author   : pratheeba.V
 * Date     : 29-6-2020
 * Updated   : Naveen S
 */
@isTest
private class PublicProvidersBackgroundCheck_Test {
    @IsTest static void testPublicAppBackGrounChecks(){
        Account InsertobjNewAccount=new Account(Name = 'Test Account');
        insert InsertobjNewAccount;
        
        Reconsideration__c recon = new Reconsideration__c();
        insert recon;
        Actor__c Act=new Actor__c();
        insert Act;
        
        Id MyId2=Schema.Sobjecttype.ProviderRecordQuestion__c.getRecordTypeInfosByName().get('Criminal History').getRecordTypeId();
        Id MyId3=Schema.Sobjecttype.ProviderRecordQuestion__c.getRecordTypeInfosByName().get('Personal Security').getRecordTypeId();
        ProviderRecordQuestion__c   InsertobjInProviderRecordQues =new ProviderRecordQuestion__c(RecordTypeId=MyId2,Reconsideration__c=recon.Id);
        insert InsertobjInProviderRecordQues;
        ProviderRecordQuestion__c   InsertobjInProviderRecordQues1 =new ProviderRecordQuestion__c(RecordTypeId=MyId3,Reconsideration__c=recon.Id);
        insert InsertobjInProviderRecordQues1;
       
        ProviderRecordAnswer__c   InsertobjInProviderRecordAnswer =new ProviderRecordAnswer__c();
        insert InsertobjInProviderRecordAnswer;
        
      
        
        string bulkobject = '[{"id":"'+InsertobjInProviderRecordAnswer.Id+'","Date__c":"2020-06-29","Comments__c":"'+InsertobjInProviderRecordAnswer.Comments__c+'","ProviderRecordQuestion__c":"'+InsertobjInProviderRecordAnswer.ProviderRecordQuestion__c+'","HouseholdStatus__c":"'+InsertobjInProviderRecordAnswer.HouseholdStatus__c+'","Dependent__c":"'+InsertobjInProviderRecordAnswer.Dependent__c+'", "State__c":"'+InsertobjInProviderRecordAnswer.State__c+'","Comar__c":"'+InsertobjInProviderRecordAnswer.Comar__c+'"}]';
        string quesId = InsertobjInProviderRecordAnswer.ProviderRecordQuestion__c;

        try
       {
        PublicProvidersBackgroundCheck.getHouseholdMembers(null);        
       }
       catch(Exception Ex)
       {
           
       }
    PublicProvidersBackgroundCheck.getHouseholdMembers(InsertobjNewAccount.Id);

     try
       {
        PublicProvidersBackgroundCheck.getQuestionsByType(null);   
       }
       catch(Exception Ex)
       {
           
       }
       PublicProvidersBackgroundCheck.getQuestionsByType('Criminal History');

       try
       {
        PublicProvidersBackgroundCheck.bulkAddRecordAns(null, null);
       }
       catch(Exception Ex)
       {
           
       }
       PublicProvidersBackgroundCheck.bulkAddRecordAns(bulkobject, quesId);
       try
       {
        PublicProvidersBackgroundCheck.bulkUpdateRecordAns(null);
       }
       catch(Exception Ex)
       {
           
       }
       PublicProvidersBackgroundCheck.bulkUpdateRecordAns(bulkobject);
      
       PublicProvidersBackgroundCheck.getRecordType('Personal Security');
        
       try
       {
        PublicProvidersBackgroundCheck.editRecordDet(null);
       }
       catch(Exception Ex)
       {
           
       }
       PublicProvidersBackgroundCheck.editRecordDet(InsertobjInProviderRecordQues.Id);
        
     
       PublicProvidersBackgroundCheck.getUploadedDocuments(recon.Id);

       try
       {
        PublicProvidersBackgroundCheck.updateActorFieldInContentversion(null,null);
       }
       catch(Exception Ex)
       {
           
       }
       PublicProvidersBackgroundCheck.updateActorFieldInContentversion(recon.Id,Act.Id);
       

       PublicProvidersBackgroundCheck.getRecordDetails(recon.Id);
       
       try{
       List<DMLOperationsHandler.FetchValueWrapper> objgetPickListValues = PublicProvidersBackgroundCheck.getPickListValues(InsertobjNewAccount,'Pending');
       }
       catch(Exception Ex)
       {

       }
    }
}