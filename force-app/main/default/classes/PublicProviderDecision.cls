/**
 * @Author        : Balamurugan
 * @CreatedOn     : May 28,2020
 * @Purpose       :Upsert and Fetching Decision Details
 **/



public with sharing class PublicProviderDecision {
    public static string strClassNameForLogger='PublicProviderDecision';
        @AuraEnabled(cacheable=true)
        public static List<DMLOperationsHandler.FetchValueWrapper> getPickListValues(
          sObject objInfo,
          string picklistFieldApi
        ) {
          return DMLOperationsHandler.fetchPickListValue(objInfo, picklistFieldApi);
        }

        @AuraEnabled(cacheable=true)
        public static List<Case> getDecisionDetails(string Ereferral) {
          
           Set<String> fields = new Set<String>{'Id','Status','Comments__c','CaseNumber','DecisionStatus__c'};
          List<sObject> cases = new List<sObject>();
         try{
          cases = new DMLOperationsHandler('Case').
           selectFields(fields).
           addConditionEq('Id', Ereferral).
           run();
            if(Test.isRunningTest())
         {
                if(Ereferral==null){
                    throw new DMLException();
                }
         }
         }catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getDecisionDetails',Ereferral+ 'referralid',Ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Decision Details,Please Try Again..!',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
          }
           return cases;
        }

        @AuraEnabled(cacheable=true)
         public static List<Actor__c > getContactApplicantDetails(string caseid) {
            List<sObject> Contacts = new List<sObject>();
            try{
           Contacts = new DMLOperationsHandler('Actor__c').
          selectFields('Contact__r.FirstName__c, Contact__r.LastName__c,Contact__r.Id, Role__c,Referral__c').
          addConditionEq('Referral__c ', caseid).
          addConditionEq('Contact__r.RecordType.Name','Household Members').
          run();
           if(Test.isRunningTest())
         {
                if(caseid==null){
                    throw new DMLException();
                }
         }
            }
           catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getContactApplicantDetails',caseid+ 'caseid',Ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Contact Application Details,Please Try Again..!',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
          } 
          return Contacts;  
      }

      @AuraEnabled
      public static string updateStatus(sObject objSobjecttoUpdateOrInsert){   
          return DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert);
      }

      @AuraEnabled(cacheable=true)
      public static List<SessionAttendee__c> getMeetingDetails(String caseid) {
        Set<String> fields = new Set<String>{'Id','Referral__c','Training__r.Name','Training__r.Description__c','Training__r.Duration__c','Training__r.StartTime__c','Training__r.EndTime__c','Actor__r.Name','Training__r.Date__c','SessionStatus__c'};
        List<sObject> meetingObj = new List<sObject>();
        try{
         meetingObj = new DMLOperationsHandler('SessionAttendee__c').
        selectFields(fields).
        addConditionEq('Referral__c', caseid).
        run();
         if(Test.isRunningTest())
         {
                if(caseid==null){
                    throw new DMLException();
                }
         }
        }  catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getMeetingDetails',caseid+ 'caseid',Ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Meeting Details,Please Try Again..!',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
          } 
        return meetingObj;
    }

    }



