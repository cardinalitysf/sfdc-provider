/**
 * @Author        : Sindhu Venkateswarlu
 * @CreatedOn     : July 27,2020
 * @Purpose       :Test Methods to unit test for PublicApplicationDashboard
 **/


@isTest
Private class PublicApplicationDashboard_Test{
    @isTest static void testapplicationDashBoard (){
        Profile prf = [Select Id, Name from Profile where Name= 'Supervisor'];
        List<User> usrList = TestDataFactory.createTestUsers( 2, prf.Id,true );
        List<Account> lstAcct= TestDataFactory.testAccountData();
        insert lstAcct;
        for(Account acct : lstAcct)
        {
        }
        List<Application__c> appList = TestDataFactory.TestApplicationData(2,'Rejected');
        insert appList;
        List<Application__c> apList = new List<Application__c>();
        for(Application__c app: appList)
        {
           app.Program__c='rcc';
           app.ProgramType__c='others';
           app.Status__c='Pending';
           apList.add(app);
        }
            update apList;
        for(Account acct : lstAcct)
        {
        }
        List<Monitoring__c> monList = TestDataFactory.createTestMonitoringData(5,true);
        update monList;
        List<ContentVersion> cvList = TestDataFactory.createTestContentVersionData();
        insert cvList;
        PublicApplicationDashboard.getDashboardData(appList[0].Name, appList[0].Status__c, '');
        PublicApplicationDashboard.getDashboardData(appList[0].Name, appList[0].Status__c, 'Supervisor');
        PublicApplicationDashboard.getDashboardData(appList[0].Name, 'Rejected', 'Supervisor');
        PublicApplicationDashboard.getDashboardData(appList[0].Name, 'Rejected', '');
        PublicApplicationDashboard.getDashboardData(appList[0].Name, 'Supervisor Rejected', '');
        PublicApplicationDashboard.getDashboardData(appList[0].Name, 'total', 'Supervisor');
        PublicApplicationDashboard.getDashboardData(appList[0].Name, 'total', '');
        PublicApplicationDashboard.getDashboardData(appList[0].Name, 'Approved', '');
        PublicApplicationDashboard.getDashboardData(appList[0].Name, 'Revocation', '');
        PublicApplicationDashboard.getDashboardData(appList[0].Name, 'Suspension', '');
        PublicApplicationDashboard.getDashboardData(appList[0].Name, 'Limitation', '');

        PublicApplicationDashboard.getApplicationCount(apList[0].Name, false, 'Public');
        PublicApplicationDashboard.getApplicationCount(apList[0].Name, true, 'Public');
        PublicApplicationDashboard.getApplicationCount(apList[0].Name, true, 'Private');
        PublicApplicationDashboard.getApplicationCount(apList[0].Name, false, 'Private');
        PublicApplicationDashboard.getApplicationCount('', false, 'Pending');
        PublicApplicationDashboard.getApplicationCount('', false, 'For Review');


        PublicApplicationDashboard.getPrivateDashboardData(appList[0].Name, appList[0].Status__c, '');
        PublicApplicationDashboard.getPrivateDashboardData(appList[0].Name, 'total', 'Supervisor');
        PublicApplicationDashboard.getPrivateDashboardData(appList[0].Name, 'total', '');
        PublicApplicationDashboard.getPrivateDashboardData(appList[0].Name, 'Pending', '');
        PublicApplicationDashboard.getPrivateDashboardData(appList[0].Name, 'Pending', 'Supervisor');
        PublicApplicationDashboard.getPrivateDashboardData(appList[0].Name, 'Approved','');
        PublicApplicationDashboard.getPrivateDashboardData(appList[0].Name, 'Revocation','');
        PublicApplicationDashboard.getPrivateDashboardData(appList[0].Name, 'Suspension','');
        PublicApplicationDashboard.getPrivateDashboardData(appList[0].Name, 'Limitation','');

        


    }

   @isTest
    static void CountsTest() {
       
        Account Acc1 = new Account(ProviderType__c='Public' ,Name='Name');
        insert Acc1;
        Application__c CountApplication1= new Application__c(Status__c = 'Pending');
        Id MyId2=Schema.Sobjecttype.Application__c.getRecordTypeInfosByName().get('Public').getRecordTypeId();
        CountApplication1.RecordTypeId = MyId2;
        //CountApplication1.AccountId=Acc1.Id;
        insert CountApplication1;
        PublicApplicationDashboard.getApplicationCount('',true,'Public');
        CountApplication1.Status__c='Pending';
        update CountApplication1;

        PublicApplicationDashboard.getApplicationCount('',true,'Public');
        CountApplication1.Status__c='For Review';
        update CountApplication1;

        PublicApplicationDashboard.getApplicationCount('',true,'Public');
        CountApplication1.Status__c='Assigned for Training';
        update CountApplication1;

        PublicApplicationDashboard.getApplicationCount('',true,'Public');
        CountApplication1.Status__c='Training Completed';
        update CountApplication1;

        
        PublicApplicationDashboard.getApplicationCount('',true,'Public');
        CountApplication1.Status__c='Training Incompleted';
        update CountApplication1;

        PublicApplicationDashboard.getApplicationCount('',true,'Public');
        CountApplication1.Status__c='Caseworker Submitted';
        update CountApplication1;

        PublicApplicationDashboard.getApplicationCount('',true,'Public');
        CountApplication1.Status__c='Approved';
        update CountApplication1;

        PublicApplicationDashboard.getApplicationCount('',true,'Public');
        CountApplication1.Status__c='Rejected';
        update CountApplication1;

        PublicApplicationDashboard.getApplicationCount('',true,'Public');
        CountApplication1.Status__c='Supervisor Rejected';
        update CountApplication1;

        PublicApplicationDashboard.getApplicationCount('',true,'Public');
        CountApplication1.Status__c='Returned';
        update CountApplication1;

        PublicApplicationDashboard.getApplicationCount('',true,'Public');
        CountApplication1.Status__c='Revocation';
        update CountApplication1;

        PublicApplicationDashboard.getApplicationCount('',true,'Public');
        CountApplication1.Status__c='Limitation';
        update CountApplication1;
        
        PublicApplicationDashboard.getApplicationCount('',true,'Public');
        CountApplication1.Status__c='Suspension';
        update CountApplication1;

    }

}