/**
     * @Author        : Vijayaraj M
     * @CreatedOn     : May 6,2020
     * @Purpose       :Test Methods to unit test for ApplicationMonitoringActivity.cls
     * @update        : Jayachandran s
     * @CreatedOn     : July 28,2020
     * @Purpose       : try catch updated.
     **/
    
@isTest
private with sharing class ApplicationMonitoringActivity_Test {
    @isTest static void testApplicationMonitoringActivityTest() {
        
        Task InsertobjNewTask=new  Task(ActivityName__c='XXX');
        insert InsertobjNewTask;
        ReferenceValue__c InsertobjNewRefer =new ReferenceValue__c(RefKey__c='MMMM');
        insert InsertobjNewRefer;
        Application__c InsertobjNewApplication=new Application__c(Status__c = 'Pending'); 
        insert InsertobjNewApplication;
        ApplicationMonitoring.getMonitoringDetails(InsertobjNewApplication.Id);
        Monitoring__c InsertobjNewMonitoring=new Monitoring__c(ApplicationLicenseId__c = InsertobjNewApplication.Id); 
        insert InsertobjNewMonitoring; 
        

      ApplicationMonitoringActivity.UpdateTask(InsertobjNewTask);
      // ApplicationMonitoringActivity.deleteActivityDetails(InsertobjNewTask.Id);
      string str = '[{"Activity__c":"testActivity","Monitoring__c":"'+InsertobjNewMonitoring.Id+'","Status__c":"Scheduled","Comments__c":"Sample","CompletionDate__c":"","WhatId":"'+InsertobjNewMonitoring.Id+'"}]';


      try
      {    
        ApplicationMonitoringActivity.InsertUpdateActivityInfo(str,InsertobjNewMonitoring.Id,'');
      } catch (Exception Ex){
              
          }
          
          ApplicationMonitoringActivity.InsertUpdateActivityInfo(str,InsertobjNewMonitoring.Id,'Insert');

          try
          {    
            ApplicationMonitoringActivity.editActivityDetails(null);
          } catch (Exception Ex){
                  
              }
              
              ApplicationMonitoringActivity.editActivityDetails(InsertobjNewTask.Id);

           try
              {    
                ApplicationMonitoringActivity.getActivityListFromTask(null);
              } catch (Exception Ex){
                      
                  }
                  
                  ApplicationMonitoringActivity.getActivityListFromTask('iddata');

            try {
              ApplicationMonitoringActivity.getActivityValueFromReference();
            } catch (Exception Ex) {
              
            }

          try {
          
                List<DMLOperationsHandler.FetchValueWrapper>  objgetPickListValues = ApplicationMonitoringActivity.getActivitySubType(InsertobjNewTask,' ');
              } 
              catch (Exception e)
               {
                        
              }
              

}
}