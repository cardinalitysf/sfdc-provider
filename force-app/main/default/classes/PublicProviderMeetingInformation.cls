/**
 * @Author        : Pratheeba V
 * @CreatedOn     : May 14, 2020
 * @Purpose       : Public Provider Meeting Information tab 
 **/
public with sharing class PublicProviderMeetingInformation {
    public static string strClassNameForLogger='PublicProviderMeetingInformation';
    @AuraEnabled(cacheable=false)
    public static List<Actor__c> getHouseholdMembers(string providerid) {
        List<Actor__c> householdMemberObj = new List<Actor__c> ();
        try{
            householdMemberObj = new DMLOperationsHandler('Actor__c').
            selectFields('Contact__r.Name,Contact__r.Id, Id, Role__c').
            addConditionEq('Provider__c ', providerid).
            addConditionEq('Contact__r.RecordType.Name','Household Members').
            run();
            if(Test.isRunningTest()) {
                if(providerid=='') {
                    throw new DMLException();
                }				
			}
        } catch (Exception Ex) { 
            String[] arguments = new String[] {providerid};
            string strInputRecord= String.format('Input parameters are :: providerid -- {0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getHouseholdMembers',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Members List',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }        
        return householdMemberObj;
    }

    @AuraEnabled(cacheable=false)
    public static List<Training__c> getTrainingDetails(string param) {
        List<Training__c> trainingDetailsObj = new List<Training__c> ();
        Set<String> fields = new Set<String>{'Id','Name','Date__c','Description__c','StartTime__c','EndTime__c','SessionType__c','Duration__c'};
        try {    
            trainingDetailsObj = new DMLOperationsHandler('Training__c').
            selectFields(fields).
            addConditionEq('Type__c', param).
            addConditionEq('RecordType.Name', 'Public').
            addConditionGe('Date__c', Date.today()).
            orderBy('Date__c', 'Asc').
            run();
            if(Test.isRunningTest()) {
                if(param=='') {
                    throw new DMLException();
                }				
            }
        } catch (Exception Ex) { 
            String[] arguments = new String[] {param};
            string strInputRecord= String.format('Input parameters are :: param -- {0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getTrainingDetails',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Training List',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return trainingDetailsObj;
    }
     
    @AuraEnabled(cacheable=false)
    public static List<SessionAttendee__c> getMeetingDetails(string caseid) {
        List<SessionAttendee__c> meetingDetailsObj = new List<SessionAttendee__c> ();
        Set<String> fields = new Set<String>{'Id','Training__c','Training__r.Name','Training__r.Date__c','Training__r.SessionType__c','Actor__r.Contact__r.Name','Actor__r.Role__c','SessionStatus__c','IntenttoAttend__c','Training__r.Jurisdiction__c'};
        try{
            meetingDetailsObj = new DMLOperationsHandler('SessionAttendee__c').
            selectFields(fields).
            addConditionEq('Referral__c', caseid).
            orderBy('CreatedDate', 'Desc').
            run();
        if(Test.isRunningTest()) {
                if(caseid=='') {
                    throw new DMLException();
                }				
            }
        } catch (Exception Ex) { 
            String[] arguments = new String[] {caseid};
            string strInputRecord= String.format('Input parameters are :: caseid -- {0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getMeetingDetails',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Meeting List',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return meetingDetailsObj;
    }

    @AuraEnabled(cacheable=false)
    public static List<SessionAttendee__c> viewMeetingDetails(String meetingid) {
        List<SessionAttendee__c> meetingObj = new List<SessionAttendee__c> ();
        Set<String> fields = new Set<String>{'Id','Training__r.Name','Training__r.Description__c','Training__r.Duration__c','Training__r.StartTime__c','Training__r.EndTime__c','Actor__r.Contact__r.Name','Training__r.Date__c','SessionStatus__c'};
        try{
            meetingObj = new DMLOperationsHandler('SessionAttendee__c').
            selectFields(fields).
            addConditionEq('Id', meetingid).
            run();
            if(Test.isRunningTest()) {
                if(meetingid=='') {
                    throw new DMLException();
                }				
            }
        } catch (Exception Ex) { 
            String[] arguments = new String[] {meetingid};
            string strInputRecord= String.format('Input parameters are :: meetingid -- {0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'viewMeetingDetails',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Meeting List',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return meetingObj;
    }

    @AuraEnabled(cacheable=true)
    public static List<Case> getPublicDecisionStatus(string refid) {
        List<Case> cases = new List<Case> ();
        Set<String> fields = new Set<String>{'Id','Status','CaseNumber','DecisionStatus__c'};
        try {
            cases = new DMLOperationsHandler('Case').
            selectFields(fields).
            addConditionEq('Id', refid).
            run();
            if(Test.isRunningTest()) {
                if(refid=='') {
                    throw new DMLException();
                }				
            }
        } catch (Exception Ex) { 
            String[] arguments = new String[] {refid};
            string strInputRecord= String.format('Input parameters are :: refid -- {0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getPublicDecisionStatus',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Decision Status',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return cases;
    }

    @AuraEnabled
    public static void InsertUpdateMeetingInfo(string strTrainingDetails){
        List<SessionAttendee__c> lstMeetingToParse = (List<SessionAttendee__c>)System.JSON.deserialize(strTrainingDetails, List<SessionAttendee__c>.class);
        List<SessionAttendee__c> lstMeetingToInsert = new List<SessionAttendee__c> ();
        for(SessionAttendee__c meeting : lstMeetingToParse) {
            SessionAttendee__c intMeetingInfo = new SessionAttendee__c();
            intMeetingInfo.Actor__c = meeting.Actor__c;
            intMeetingInfo.Training__c = meeting.Training__c;
            //intMeetingInfo.Date__c = meeting.Date__c;
            //intMeetingInfo.SessionNumber__c = meeting.SessionNumber__c;
            //intMeetingInfo.StartTime__c = meeting.StartTime__c;
            //intMeetingInfo.EndTime__c = meeting.EndTime__c;
            intMeetingInfo.SessionStatus__c = meeting.SessionStatus__c;
            intMeetingInfo.Referral__c = meeting.Referral__c;
            intMeetingInfo.TotalHoursAttended__c = meeting.TotalHoursAttended__c;
            lstMeetingToInsert.add(intMeetingInfo);
        }
        DMLOperationsHandler.updateOrInsertSOQLForList(lstMeetingToInsert);
    }

    @AuraEnabled
    public static List<AggregateResult> getMeetingAttendedHours(string caseid) {
        string role='Applicant';
        string status = 'Completed';
        List<AggregateResult> groupedResults =  new List<AggregateResult> ();        
        try {
            groupedResults =  new DMLOperationsHandler('SessionAttendee__c').
                                    selectField('Actor__r.Contact__r.Name').   
                                    sum('TotalHoursAttended__c', 'totalhours').
                                    addConditionEq('Actor__r.Role__c',role).
                                    addConditionEq('sessionstatus__c',status).
                                    addConditionEq('Referral__c',caseid).
                                    groupBy('Actor__r.Contact__r.Name').
                                    aggregate(); 
            if(Test.isRunningTest()) {
                if(caseid=='') {
                    throw new DMLException();
                }				
            }
        } catch (Exception Ex) { 
            String[] arguments = new String[] {caseid};
            string strInputRecord= String.format('Input parameters are :: caseid -- {0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getMeetingAttendedHours',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Meeting Attended Hours',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }      
        return groupedResults;
    }
}