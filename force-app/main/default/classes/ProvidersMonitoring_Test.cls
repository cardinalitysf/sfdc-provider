/**
 * @Author        : Pratheeba.V
 * @CreatedOn     : May 11 ,2020
 * @Purpose       : Test Methods to unit test ProvidersMonitoring class
 * @Updated on    :June 20, 2020
 * @Updated by    :Janaswini Unit Test Updated methods with try catch
**/

@IsTest
private class ProvidersMonitoring_Test {
    public ProvidersMonitoring_Test() {}
    @IsTest static void testProvidersMonitoringPositive(){
        Application__c InsertobjNewApplication=new Application__c(Status__c = 'Pending'); 
        insert InsertobjNewApplication;
        Monitoring__c InsertobjNewMonitoring=new Monitoring__c(ApplicationLicenseId__c = InsertobjNewApplication.Id); 
        insert InsertobjNewMonitoring;
        Account InsertobjNewAccount=new Account(Name = 'Test Account'); 
        insert InsertobjNewAccount;

        
        ProvidersMonitoring.InsertUpdateMonitoring(InsertobjNewMonitoring);
        try
        {
            ProvidersMonitoring.getApplicationId(null);
        }
        
        catch(Exception ex)
           {
               
           }
        ProvidersMonitoring.getApplicationId(InsertobjNewAccount.Id);

        ProvidersMonitoring.getMonitoringDetails(InsertobjNewApplication.Id,'Supervisor');
        try
        {
            ProvidersMonitoring.getAddressDetails(null);
        }
        
        catch(Exception ex)
           {
               
           }
        ProvidersMonitoring.getAddressDetails(InsertobjNewApplication.Id);
        try
        {
            ProvidersMonitoring.getProgramDetails(null);
        }
        
        catch(Exception ex)
           {
               
           }
        ProvidersMonitoring.getProgramDetails(InsertobjNewApplication.Id);
        try
        {
            ProvidersMonitoring.getActivityStatus(null);
        }
        
        catch(Exception ex)
           {
               
           }
        ProvidersMonitoring.getActivityStatus(InsertobjNewMonitoring.Id);
        try
        {
            ProvidersMonitoring.editMonitoringDetails(null);
        }
        
        catch(Exception ex)
           {
               
           }
        ProvidersMonitoring.editMonitoringDetails(InsertobjNewMonitoring.Id);
        try
        {
            ProvidersMonitoring.viewMonitoringDetails(null);
        }
        
        catch(Exception ex)
           {
               
           }
        ProvidersMonitoring.viewMonitoringDetails(InsertobjNewMonitoring.Id);
        try
        {
            ProvidersMonitoring.getApplicationProviderStatus(null);
        }
        
        catch(Exception ex)
           {
               
           }
        ProvidersMonitoring.getApplicationProviderStatus(InsertobjNewApplication.Id);
        try
        {
            ProvidersMonitoring.getMonitoringStatus(null);
        }
        
        catch(Exception ex)
           {
               
           }
        ProvidersMonitoring.getMonitoringStatus(InsertobjNewMonitoring.Id);

        try
        {
            ProvidersMonitoring.getLicensesId(null);
        }
        
        catch(Exception ex)
           {
               
           }
        ProvidersMonitoring.getLicensesId(InsertobjNewApplication.Id);

        try
        {
            ProvidersMonitoring.getAppProviderid(null);
        }
        
        catch(Exception ex)
           {
               
           }
        ProvidersMonitoring.getAppProviderid(InsertobjNewApplication.Id);
        try
        {
            ProvidersMonitoring.getApplicationLicenseStatus(null);
        }
        
        catch(Exception ex)
           {
               
           }
        ProvidersMonitoring.getApplicationLicenseStatus(InsertobjNewApplication.Id);
    }

    @IsTest static void testProvidersMonitoringNegative(){
        Application__c InsertobjNewApplication=new Application__c(Status__c = 'Pending'); 
        insert InsertobjNewApplication;
        ProvidersMonitoring.getMonitoringDetails(InsertobjNewApplication.Id,'System Administrator');
    }

    @IsTest static void testProvidersMonitoringException(){
        ProvidersMonitoring.getApplicationId('');
        ProvidersMonitoring.getMonitoringDetails('','');
        ProvidersMonitoring.getAddressDetails('');
        ProvidersMonitoring.getProgramDetails('');
        ProvidersMonitoring.getActivityStatus('');
        ProvidersMonitoring.editMonitoringDetails('');
        ProvidersMonitoring.viewMonitoringDetails('');
        ProvidersMonitoring.getApplicationProviderStatus('');
        ProvidersMonitoring.getMonitoringStatus('');
        ProvidersMonitoring.getLicensesId('');
        ProvidersMonitoring.getAppProviderid('');
        ProvidersMonitoring.getApplicationLicenseStatus('');
    }
}