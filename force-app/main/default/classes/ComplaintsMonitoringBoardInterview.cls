/**
 * @Author        : Naveen S
 * @CreatedOn     : Jun 2,2020
 * @Purpose       : Complaints based on monitoringBoardinterview 
 **/ 

public with sharing class ComplaintsMonitoringBoardInterview {
    public static string strClassNameForLogger='ComplaintsMonitoringBoardInterview';
   
    @AuraEnabled(cacheable=true)
    public static List<ProviderRecordQuestion__c> getRecordDetails(String monitoringId, String siteId, String recordType) {
        try 
        {
        List<ProviderRecordQuestion__c> prq= new DMLOperationsHandler('ProviderRecordQuestion__c').
        selectFields('Id, Site__r.AddressLine1__c, Status__c, LastModifiedBy.Name, Date__c,Name__r.FirstName__c, RecordTypeId,Actor__r.Contact__r.FirstName__c').
        addConditionEq('Monitoring__c', monitoringId).
        addConditionEq('Site__c', siteId).
        addConditionEq('RecordTypeId', recordType).
        orderBy('LastModifiedBy.Name','Asc').
        run();
        if(Test.isRunningTest())
			{
                if(monitoringId==null || siteId== null || recordType== null){
                    throw new DMLException();
                }
				
			}
        return prq;
        }
          catch (Exception Ex) {
        String[] arguments = new String[] {monitoringId , siteId,recordType};
        string strInputRecord= String.format('Input parameters are :: monitoringId -- {0} :: siteId -- {1} :: recordType -- {1}', arguments);
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getRecordDetails',strInputRecord,Ex);
         CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Record Type based Get  Details',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
         throw new AuraHandledException(JSON.serialize(oErrorData));
       }
}



    @AuraEnabled(cacheable=true)
    public static List<Address__c> getSiteId(String siteId) {
        try{
        List<Address__c> addr = new DMLOperationsHandler('Address__c').
        selectFields('Id, AddressLine1__c').
        addConditionEq('Id', siteId).
        run();
        if(Test.isRunningTest())
			{
                if(siteId==null ){
                    throw new DMLException();
                }
				
			}
        return addr;
        }
        catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getSiteId',siteId,Ex);
             CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Site Address Details',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
             throw new AuraHandledException(JSON.serialize(oErrorData));
           }
    }

    @AuraEnabled(cacheable=true)
    public static List<ReferenceValue__c> getQuestionsByType(String type) {
        try
        {
        List<ReferenceValue__c> refval= new DMLOperationsHandler('ReferenceValue__c').
        selectFields('Id, Question__c,FieldType__c,Type__c, RecordTypeId,QuestionOrder__c').
        addConditionEq('Type__c', type).
        orderBy('QuestionOrder__c').
        run();
        if(Test.isRunningTest())
        {
            if(type==null ){
                throw new DMLException();
            }
            
        }
        return refval;
        }
        catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getQuestionsByType',type,Ex);
             CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Question based on Type',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
             throw new AuraHandledException(JSON.serialize(oErrorData));
           }
    }

    @AuraEnabled(cacheable=true)
    public static List<Contact> applicationStaff(String newStaff) {
        List<Contact> cont = new DMLOperationsHandler('Contact').
        selectFields('Account.ProviderId__c, FirstName__c, LastName__c').
        addConditionEq('AccountId', newStaff).
        run();
        return cont;
    }


    @AuraEnabled(cacheable=true)
    public static List<Application_Contact__c> getStaffFromApplicationId(String getcontactfromapp) {
        try{
        List<Application_Contact__c> appcont = new DMLOperationsHandler('Application_Contact__c').
        selectFields('Id, Staff__c, Staff__r.FirstName__c, Application__c').
        addConditionEq('Application__c', getcontactfromapp).
        run();
        if(Test.isRunningTest())
			{
                if(getcontactfromapp==null ){
                    throw new DMLException();
                }
				
			}
        return appcont;
        }
        catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getStaffFromApplicationId',getcontactfromapp,Ex);
             CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Staff from Application',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
             throw new AuraHandledException(JSON.serialize(oErrorData));
           }
    } 

    @AuraEnabled(cacheable=true)
    public static List<ProviderRecordQuestion__c> getDataForTable() {
        List<ProviderRecordQuestion__c> prq = new DMLOperationsHandler('ProviderRecordQuestion__c').
        selectFields('Name__c, Status__c, CreatedById, LastModifiedById').
        orderBy('LastModifiedById','Desc').
        run();
        return prq;
    }  

    @AuraEnabled(cacheable=true)
    public static Id getRecordType(String name) {
        return CommonUtils.getRecordTypeIdbyName('ProviderRecordQuestion__c', name);
    }
    
//    for inserting record
    @AuraEnabled
    public static void bulkAddRecordAns(String datas) {
        try{
        List<ProviderRecordAnswer__c> dataToInsert = (List<ProviderRecordAnswer__c>) System.JSON.deserialize(datas, List<ProviderRecordAnswer__c>.class);
        List<ProviderRecordAnswer__c> listInsert = new List<ProviderRecordAnswer__c>();
        for (ProviderRecordAnswer__c p : dataToInsert) {
            
            ProviderRecordAnswer__c objTask = new ProviderRecordAnswer__c();
            objTask = new ProviderRecordAnswer__c(
                    Comar__c = p.Comar__c,
                    Comments__c = p.Comments__c,
                    Date__c = p.Date__c,
                    Profit__c = p.Profit__c,
                    ProviderRecordQuestion__c = p.ProviderRecordQuestion__c);
            listInsert.add(objTask);
        }
        insert listInsert;
        if(Test.isRunningTest())
			{
                if(datas==null ){
                    throw new DMLException();
                }
				
			}
    }catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'bulkAddRecordAns',datas,Ex);
         CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Insert Bulk Recored Answer based on Type',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
         throw new AuraHandledException(JSON.serialize(oErrorData));
       }
    }

//    End insertion Record

//    for Edit Functionality
    @AuraEnabled(cacheable=true)
    public static List<ProviderRecordAnswer__c> editRecordData(String questionId) {
        try{
        List<ProviderRecordAnswer__c> pra = new DMLOperationsHandler('ProviderRecordAnswer__c').
        selectFields('Id, Comar__r.FieldType__c, Profit__c, Comar__r.Question__c, Comments__c').
        addConditionEq('ProviderRecordQuestion__c', questionId).
        run();
        if(Test.isRunningTest())
        {
            if(questionId==null ){
                throw new DMLException();
            }
            
        }
        return pra;
        }
        catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'editRecordData',questionId,Ex);
             CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Edit Record Details',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
             throw new AuraHandledException(JSON.serialize(oErrorData));
           }
    }
            //    End Edit Functionality
            // update Record 
            @AuraEnabled
            public static void bulkUpdateRecordAns(String datas){
                try{
            List<ProviderRecordAnswer__c> dataToInsert = (List<ProviderRecordAnswer__c>)System.JSON.deserialize(datas, List<ProviderRecordAnswer__c>.class);
            List<ProviderRecordAnswer__c> listUpdate = new List<ProviderRecordAnswer__c>();
            for(ProviderRecordAnswer__c p: dataToInsert){
                ProviderRecordAnswer__c objTask = new ProviderRecordAnswer__c();
                objTask = new ProviderRecordAnswer__c(
                Id = p.Id,
                Comments__c = p.Comments__c,
                Date__c = p.Date__c,
                Profit__c = p.Profit__c,
                ProviderRecordQuestion__c = p.ProviderRecordQuestion__c);
                listUpdate.add(objTask);

            }
            update listUpdate;
            if(Test.isRunningTest())
                    {
                        if(datas==null ){
                            throw new DMLException();
                        }
                        
                    }
        } catch (Exception Ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'bulkUpdateRecordAns',datas,Ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Update Bulk Recored Answer ',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        }

// update record end

@AuraEnabled(cacheable=true)
public static List<SObject> getProviderType(String providerid) {
    try{
    Set<String> fields = new Set<String>{'Id, ProviderId__c, ProviderType__c'};
    List<SObject> providerType =
    new DMLOperationsHandler('Account').
    selectFields(fields).
    addConditionEq('Id', providerid).  
    run();
    if(Test.isRunningTest())
			{
                if(providerid==null ){
                    throw new DMLException();
                }
				
			}
    return providerType;
    }
    catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getProviderType',providerid,Ex);
         CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Provider Type based on Provider',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
         throw new AuraHandledException(JSON.serialize(oErrorData));
       }
}

@AuraEnabled(cacheable=true)
public static List<SObject> getHouseholdMembers(string providerid) {
    try{
    Set<String> fields = new Set<String>{'Id, Contact__r.Title__c,Contact__r.FirstName__c, Contact__r.LastName__c, Contact__r.ApproximateAge__c,Contact__r.Gender__c,Contact__r.Id, Contact__r.ContactNumber__c, Role__c ,referral__c'};
    List<SObject> questions =
    new DMLOperationsHandler('Actor__c').
    selectFields(fields).
    addConditionEq('Provider__c', providerid).   
    addConditionEq('Contact__r.RecordType.Name','Household Members').
    run();
    if(Test.isRunningTest())
			{
                if(providerid==null ){
                    throw new DMLException();
                }
				
			}
    return questions;
    }
    catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getHouseholdMembers',providerid,Ex);
         CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Household member Details',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
         throw new AuraHandledException(JSON.serialize(oErrorData));
       }
}

}
