/**
 * @Author        : K.Sundar
 * @CreatedOn     : JUNE 02, 2020
 * @Purpose       : This method used to handle Decision for the Screen.
**/

public with sharing class PublicApplicationDecision {
	//This method used to get Decision Picklist Details for Public Application Screen
	@AuraEnabled
	public static List<SObject> getDecisionDetails (String applicationId) {
		String queryFields = 'Id, Status__c, CommonDecision__c, SupervisorComments__c, ApprovalComments__c';
		List<sObject> decisionObj = new DMLOperationsHandler('Application__c').
			selectFields(queryFields).
			addConditionEq('Id', applicationId).
			addConditionEq('RecordType.Name', 'Public').
			run();

        return decisionObj;
	}

	@AuraEnabled(cacheable=true)
	public static List<DMLOperationsHandler.FetchValueWrapper> getRecommendationPickListValues (SObject objInfo, String picklistFieldApi) {
		return DMLOperationsHandler.fetchPickListValue(objInfo, picklistFieldApi);
	}

	//This method used to get All Users List for Supervisor
    @AuraEnabled(cacheable=true)
    public static List<SObject> getAllSuperUsersList() {
        String queryFields = 'Id, Name, isActive, Profile.Name, Availability__c, CaseLoad__c, ProfileId, Unit__c';
        List<sObject> superuserObj = new DMLOperationsHandler('User').
			selectFields(queryFields).
			addConditionEq('Profile.Name', 'Supervisor').
			run();

        return superuserObj;
    }

    @AuraEnabled
    public static void saveSign(String strSignElement,Id recId){
        //Insert ContentVersion
        ContentVersion cVersion = new ContentVersion();
        cVersion.ContentLocation = 'S'; //S-Document is in Salesforce. E-Document is outside of Salesforce. L-Document is on a Social Netork.
        cVersion.PathOnClient = 'Signature-'+System.now() +'.png';//File name with extention
        cVersion.Origin = 'H';//C-Content Origin. H-Chatter Origin.
        //cVersion.OwnerId = attach.OwnerId;//Owner of the file
        cVersion.Title = 'Signature-'+System.now() +'.png';//Name of the file
        cVersion.VersionData = EncodingUtil.base64Decode(strSignElement);//File content
        cVersion.IsSignature__c=true;
        Insert cVersion;

        //After saved the Content Verison, get the ContentDocumentId
        Id conDocument = [SELECT ContentDocumentId  FROM ContentVersion WHERE Id =:cVersion.Id].ContentDocumentId;
        //Insert ContentDocumentLink
        ContentDocumentLink cDocLink = new ContentDocumentLink();
        cDocLink.ContentDocumentId = conDocument;//Add ContentDocumentId
        cDocLink.LinkedEntityId = recId;//Add attachment parentId
        cDocLink.ShareType = 'I';//V - Viewer permission. C - Collaborator permission. I - Inferred permission.
        cDocLink.Visibility = 'AllUsers';//AllUsers, InternalUsers, SharedUsers
        Insert cDocLink;      
    }

    //Preethi code reference
    @AuraEnabled
	public static string getAssignedSuperUserTableDetails(string applicationId) {

        String model = 'ProcessInstance';
		String fields = 'Id, (SELECT Id, ProcessInstanceId, ActorId, Actor.Name, Actor.Profile.Name, CreatedDate FROM Workitems), (SELECT Id, StepStatus, ActorId, Actor.Name, Actor.Profile.Name, Comments, CreatedDate FROM Steps ORDER BY ID DESC)';
		String cond = 'TargetObjectId =\'' + applicationId + '\'' + ' ORDER BY ID DESC';
        List<SObject> lstApp= DMLOperationsHandler.selectSOQLWithConditionParameters(model, fields, cond);

        Map<Id, ContentDocumentLink> contentDocumentIdsmap = new Map<Id, ContentDocumentLink>();

        for(ContentDocumentLink conDocu : [SELECT ContentDocumentId, LinkedEntityId,LinkedEntity.Name FROM ContentDocumentLink WHERE LinkedEntityId =:applicationId  ORDER BY SystemModstamp DESC]){
            contentDocumentIdsmap.put(conDocu.ContentDocumentId, conDocu);
        }

        
        list<ContentVersion> contelist = [SELECT Id, OwnerId, CreatedById FROM ContentVersion WHERE ContentDocumentId In: contentDocumentIdsmap.keyset() AND IsSignature__c=true ORDER BY SystemModstamp DESC];
        Set<Id> filteredContOwner = new Set <Id>();
        Set<ContentVersion> filteredContList = new Set <ContentVersion>();
        for(ContentVersion clist : contelist){
            if(filteredContOwner.add(clist.OwnerId) == true)
            filteredContList.add(clist);
        }
        if(lstApp.size() > 0) {
            List<processingWrapper> appWrap=new List<processingWrapper>();
            for(SObject app : lstApp) {
                processingWrapper prdWrapper = new processingWrapper(app, filteredContList);
                appWrap.add(prdWrapper);
            }
            return JSON.serialize(appWrap);
        } else {
            List<processingWrapperString> appWrap = new List<processingWrapperString>();
            String fieldsm = 'Id, SubmittedDate__c, Caseworker__c, Caseworker__r.Name, Supervisor__c, Supervisor__r.Name, CommonDecision__c, ApprovalComments__c, SupervisorComments__c, Caseworker__r.Unit__c';
			List<SObject> applicationApp = new DMLOperationsHandler('Application__c').
				selectFields(fieldsm).
				addConditionEq('Id', applicationId).     
				run();

            for(SObject app : applicationApp) {
                if(contelist.size() > 0) {
                    for (contentversion conversion: contelist) {
                        processingWrapperString prdWrapper = new processingWrapperString(app, conversion.id);
                        appWrap.add(prdWrapper);
                    }
                } else {
                    processingWrapperString prdWrapper = new processingWrapperString(app, '');
                    appWrap.add(prdWrapper);
                }
            }
            return JSON.serialize(appWrap);
        }
    }

    //Preethi code reference
    public class processingWrapper {
        public SObject applicationRecord {get;set;}
        public Set<ContentVersion> signatureUrl {get;set;}
        public processingWrapper(SObject app, Set<ContentVersion> url) {
            applicationRecord = app;
            this.signatureUrl = url;
        }
    }

    //Preethi code reference
    public class processingWrapperString {
        public SObject applicationRecord {get;set;}
        public String signatureUrl {get;set;}  
        public processingWrapperString(SObject app, String url) {
            applicationRecord = app;
            this.signatureUrl = url;
        }
    }

    //Preethi code reference
    @AuraEnabled
    public static void updateapplicationdetails (sObject resultObj, String supervisorId, String workItemId) {
        //String status = (String) resultObj.get('ReviewStatus__c');
        if(supervisorId != '') {
            String Id = (String) resultObj.get('Id');
            String comments = (String) resultObj.get('ApprovalComments__c');

            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments(comments);
            req1.setObjectId(Id);
            req1.setSubmitterId(UserInfo.getUserId());
            req1.setNextApproverIds(new Id[] {supervisorId});

            // Submit the approval request for the Opportunity
            Approval.ProcessResult result = Approval.process(req1);
        } else {
            String Id = (String) resultObj.get('Id');
            String caseworker = (String) resultObj.get('Caseworker__c');
            String comments = (String) resultObj.get('SupervisorComments__c');

            String status;
            if(resultObj.get('CommonDecision__c') == 'Returned') {
                status = 'Removed';  
            } else if (resultObj.get('CommonDecision__c') == 'Approved') {
                status = 'Approve';
            } else if (resultObj.get('CommonDecision__c') == 'Rejected') {
                status = 'Reject';
            } else {
                status = (String) resultObj.get('CommonDecision__c');
            }

            Approval.ProcessWorkitemRequest req1 = new Approval.ProcessWorkitemRequest();
            req1.setComments(comments);
            req1.setWorkitemId(workItemId);
            req1.setAction(status);
            req1.setNextApproverIds(new Id[] {caseworker});

            // Submit the approval request for the Opportunity
            Approval.ProcessResult result = Approval.process(req1);
        }
    }
}