/**
 * @Author        : Sindhu Venkateswarlu
 * @CreatedOn     : Juy 1, 2020
 * @Purpose       : Activity in monitoring for Complaints module.
 
 **/
public with sharing class ComplaintsMonitoringActivities {
  public static string strClassNameForLogger='ComplaintsMonitoringActivities';

    @AuraEnabled
  public static void InsertUpdateActivityInfo(string strMonitor,string strMonitoringId,string strAction) {
       
    List<ReferenceValue__c> tskToUpdate = (List<ReferenceValue__c>)System.JSON.deserialize(strMonitor, List<ReferenceValue__c>.class);
    List<Task> taskToInsert=new  List<Task>();
    try {
        for (ReferenceValue__c objVal : tskToUpdate) {
          Task objTask=new Task();
          objTask.ActivityName__c = objVal.Activity__c;
          objTask.Monitoring__c = strMonitoringId;
          objTask.Status__c = 'Incomplete';
          objTask.Comments__c = '-';
          objTask.CompletionDate__c = null;
          objTask.WhatId = strMonitoringId;
          if(strAction == 'Delete')
          {
            objTask.Id =  objVal.Id;
          }
          taskToInsert.add(objTask);
  
        }
          if(strAction == 'Insert') {
          insert taskToInsert;
          }
          else{
            delete taskToInsert;
          }
          
        } catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'InsertUpdateActivityInfo',strMonitor+ 'strMonitor' +strMonitoringId + 'strMonitoringId' + strAction + 'strAction',Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in record',Ex.getMessage() , CustomAuraExceptionData.type.Error.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        
    }
      
    

  
  @AuraEnabled
  public static string UpdateTask(sObject objSobjecttoUpdateOrInsert) {
  //This method is exposed to front end and reponsile for upsert operation
      //sObject is passed to updateOrInsertQuery where real data base insert or update will happen
  return DMLOperationsHandler.updateOrInsertSOQLReturnId(objSobjecttoUpdateOrInsert);
  }
  
  @AuraEnabled
  public static string deleteActivityDetails(Id deleteActivityDetails){
      return DMLOperationsHandler.deleteRecordById(deleteActivityDetails);
  }
  @AuraEnabled(cacheable=true)
  public static List<ReferenceValue__c> getActivityValueFromReference() {
    Set<String> fields = new Set<String>{'Id', 'RefKey__c', 'Activity__c'};
    List<ReferenceValue__c> OBJ =
    new DMLOperationsHandler('ReferenceValue__c').
    selectFields(fields).
    addConditionEq('RefKey__c','RCC').
    addConditionEq('RecordType.Name','Checklist').
    run();
    return OBJ;
  }

 @AuraEnabled(cacheable=true)
 public static List<Task> getActivityListFromTask(string ActivityData) {
     Set<String> fields = new Set<String>{ 'Id', 'ActivityName__c', 'Status__c', 'Monitoring__c','CompletionDate__c', 'Comments__c'};
     List<Task> contOBJ =
     new DMLOperationsHandler('Task').
     selectFields(fields).
     addConditionEq('Monitoring__c', ActivityData).
     run();
     return contOBJ;
 }

 @AuraEnabled(cacheable=false)
 public static List<Task> editActivityDetails(string editactivitydetails) {
  Set<String> fields = new Set<String>{'Id', 'ActivityName__c', 'Status__c', 'Monitoring__c', 'CompletionDate__c', 'Comments__c'};
  List<Task> OBJ =
  new DMLOperationsHandler('Task').
  selectFields(fields).
  addConditionEq('Id', editactivitydetails).
  run();
  return OBJ;
 
 }

 @AuraEnabled(cacheable=true)
  public static List<DMLOperationsHandler.FetchValueWrapper> getActivitySubType(sObject objInfo, string picklistFieldApi) {
      return DMLOperationsHandler.fetchPickListValue(objInfo,picklistFieldApi);
  }

}
