/**
	 * @Author        : S. Jayachandran
	 * @CreatedOn     : June 19 ,2020
	 * @Purpose       : Responsible for fetch or upsert data in Pet Object in Providers.
     * @UpadtedBy     : JayaChandran Unit Test Updated methods with try catch.
     * @Date          : 21 july,2020
**/

    public with sharing class PublicProvidersPetInformation {
        public static string strClassNameForLogger='PublicProvidersPetInformation';
		
		@AuraEnabled
		public static string SavePetRecordDetails(sObject objSobjecttoUpdateOrInsert) {
			return DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert);
		}

    @AuraEnabled(cacheable=true)
    public static Id getRecordType(String name) {
        return CommonUtils.getRecordTypeIdbyName('Contact', name);
    }

        @AuraEnabled(cacheable=false)
        public static List<Contact > getProvidersPetInfo(string Reconsid) {
        List<sObject> Contacts = new List<sObject>();
        try {
           Contacts =
           new DMLOperationsHandler('Contact').
           selectFields('LastName, ApproximateAge__c,PetBreed__c,PetType__c,RabiesCertificateExpiryDate__c,Reconsideration__c').
           addConditionEq('RecordType.Name','Pet').
           addConditionEq('Reconsideration__c', Reconsid).
           run();
           if(Test.isRunningTest())
		      	{
                if(Reconsid == null){
                    throw new DMLException();
                }
             }
        }catch (Exception Ex) {
            //To Log into object
             CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getProvidersPetInfo',Reconsid + 'ReconsiderationId',Ex);
             //To return back to the UI
             CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Sorry,Failed to load Application Pet Information list details ,Please Try Again..!',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
             throw new AuraHandledException(JSON.serialize(oErrorData));
           }
           
           return Contacts;
        }

        @AuraEnabled(cacheable=false)
        public static List<SObject> editPetDetails(String selectedEditId) {
        String queryFields = 'Id,LastName, ApproximateAge__c,PetBreed__c,PetType__c,RabiesCertificateExpiryDate__c';
        List<sObject> editObj = new List<sObject>();
        try {
            editObj=  new DMLOperationsHandler('Contact').
            selectFields(queryFields).
            addConditionEq('Id', selectedEditId). 
            orderBy('CreatedDate', 'Desc').                                                      
            run();
            if(Test.isRunningTest())
	              {
                   if(selectedEditId == null){
                     throw new DMLException();
                    }
                  }
        } catch (Exception Ex) {
            //To Log into object
             CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'editPetDetails',selectedEditId + 'selectedEditId',Ex);
             //To return back to the UI
             CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Sorry,Failed to load Application Pet Information list details ,Please Try Again..!',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
             throw new AuraHandledException(JSON.serialize(oErrorData));
           }
             return editObj;
        }

}
