/**
     * @Author        : Vijayaraj M
     * @CreatedOn     : June 12,2020
     * @Purpose       :Test Methods to unit test for ApplicationTabset.cls
     * @Updated on    :June 20, 2020
     * @Updated by    :Janaswini Unit Test Updated methods with try catch
     **/
    
@isTest
private with sharing class ApplicationTabset_Test {
    @isTest static void ApplicationTabset_Test() {
        Application__c InsertobjNewApplication=new Application__c(Status__c = 'XYZ'); 
        insert InsertobjNewApplication;
        try
        {
            ApplicationTabset.fetchDataForTabset(null);
        }
        
        catch(Exception ex)
           {
               
           }
        ApplicationTabset.fetchDataForTabset(InsertobjNewApplication.Id);

    }
}
