public class ProviderDetailController{
    @AuraEnabled
    public static string getReferenceCountry(){
        
        return JSON.serialize(DMLOperationsHandler.selectSOQLWithConditionParameters('ReferenceValue__c','Id,RefValue__c','RefKey__c = \'AK\' and Domain__c=\'RefKey__c\''));
    }
    @AuraEnabled
    public static string getReferenceCity(){
        return JSON.serialize(DMLOperationsHandler.selectSOQLWithConditionParameters('ReferenceValue__c','Id,RefValue__c','RefKey__c = \'AK\' and Domain__c=\'County\''));
    }
    @AuraEnabled
    public static string getReferenceState(){
        return JSON.serialize(DMLOperationsHandler.selectSOQLWithConditionParameters('ReferenceValue__c','Id,RefValue__c','RefKey__c = \'AK\' and Domain__c=\'State\''));
    }
}