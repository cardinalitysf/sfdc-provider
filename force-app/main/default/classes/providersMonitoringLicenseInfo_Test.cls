/**
 * @Author        : Pratheeba.V
 * @CreatedOn     : May 11 ,2020
 * @Purpose       : Test Methods to unit test ProvidersMonitoringLicenseInfo class
**/

@IsTest
private class providersMonitoringLicenseInfo_Test {
    public providersMonitoringLicenseInfo_Test() {}
    @IsTest static void testProviderMonitoringLicensePositive(){
        Application__c InsertobjNewApplication=new Application__c(Status__c = 'Pending'); 
        insert InsertobjNewApplication;
        Monitoring__c InsertobjNewMonitoring=new Monitoring__c(ApplicationLicenseId__c = InsertobjNewApplication.Id); 
        insert InsertobjNewMonitoring;
        Account InsertobjNewAccount=new Account(Name = 'Test Account'); 
        insert InsertobjNewAccount;
        License__c license = new License__c();
        insert license;
        try{
        providersMonitoringLicenseInfo.getLicenseInformation(null);
        }
        catch(Exception Ex){

        }
          providersMonitoringLicenseInfo.getLicenseInformation(InsertobjNewMonitoring.Id);

        try{
        providersMonitoringLicenseInfo.getProviderDetails(null);
        }
        catch(Exception Ex){

        }
        providersMonitoringLicenseInfo.getProviderDetails(InsertobjNewAccount.Id);
        
        try{
          providersMonitoringLicenseInfo.getaddressdetails(null);
        }
        catch(Exception Ex){

        }
         providersMonitoringLicenseInfo.getaddressdetails(InsertobjNewMonitoring.Id);
         try{
         providersMonitoringLicenseInfo.getIRCRatesCapacity(null);
        }
        catch(Exception Ex){

        }
        providersMonitoringLicenseInfo.getIRCRatesCapacity(license.id);
        try{
          providersMonitoringLicenseInfo.getContractCapacity(null);
        }
        catch(Exception Ex){

        }
       providersMonitoringLicenseInfo.getContractCapacity(license.id);
     
        providersMonitoringLicenseInfo.UpdateLicenseEndDate(license);
    }

}