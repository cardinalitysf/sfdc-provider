/**
     * @Author        : Sindhu Venkateswarlu
     * @CreatedOn     : May 11,2020
     * @Purpose       :Test Methods to unit test forProvidersContractAddLicenseInfo.cls
     * @UpdatedBy     :Jayachnadran s For Updation test class 
     * Date           :July 20, 2020  
     **/
@isTest
public class ProvidersContractAddLicenseInfo_Test {
                 @isTest static void testgetLicenseDetails (){
                  Application__c app = new Application__c();
                     insert app;
                     Account InsertobjNewAccount=new Account(Name = 'test Account'); 
                insert InsertobjNewAccount;
                TestDataFactory.createTestContacts(1,InsertobjNewAccount.id,true);
                Address__c acc = new Address__c();
                insert acc;
                     IRCRates__c irc = new IRCRates__c();
                     insert irc;
                     Contract__c contractObj = new Contract__c(ContractType__c='Grant');
                     insert contractObj;

                     try {
                         ProvidersContractAddLicenseInfo.ProgramList(null);
   
                     } catch (Exception Ex) {
                          
                     }
                     ProvidersContractAddLicenseInfo.ProgramList(InsertobjNewAccount.Id);
                     try {
                         ProvidersContractAddLicenseInfo.providerContract(null);
                     } catch (Exception Ex) {
                          
                     }
                     ProvidersContractAddLicenseInfo.providerContract(InsertobjNewAccount.Id);
                     try {
                         ProvidersContractAddLicenseInfo.getLicensesId('');
                     } catch (Exception Ex) {
                          
                     }
                     ProvidersContractAddLicenseInfo.getLicensesId('Id');
                     try{
                         ProvidersContractAddLicenseInfo.getIdAfterInsertSOQL(app);
                     }catch(Exception e){

                     }
                     try {
                         ProvidersContractAddLicenseInfo.getContractBedDetails(null);
                     } catch (Exception Ex) {
                          
                     }
                    ProvidersContractAddLicenseInfo.getContractBedDetails(irc.id);
                    try {
                         ProvidersContractAddLicenseInfo.getContractDetails(null);
                    } catch (Exception Ex) {
                         
                    }
                    ProvidersContractAddLicenseInfo.getContractDetails(contractObj.id);
                    try {
                         ProvidersContractAddLicenseInfo.InsertUpdateContract(null);
                    } catch (Exception Ex) {
                         
                    }
                    ProvidersContractAddLicenseInfo.InsertUpdateContract(contractObj);


                 }
}