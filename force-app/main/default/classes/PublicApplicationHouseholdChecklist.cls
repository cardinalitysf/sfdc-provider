// /**
//  * @Author        : G sathishkumar
//  * @CreatedOn     : june 1, 2020
//  * @Purpose       : PublicApplicationHouseholdChecklist
//  **/

public with sharing class PublicApplicationHouseholdChecklist {
    public static string strClassNameForLogger='PublicApplicationHouseholdChecklist';
    @AuraEnabled(cacheable=true)
    public static List<SObject> getQuestionsByType() {
     List<sObject> questions= new List<sObject>();
     try{
        Set<String> fields = new Set<String>{'Id, Question__c, Type__c, RecordTypeId,QuestionOrder__c'};
         questions =new DMLOperationsHandler('ReferenceValue__c').
        selectFields(fields).
        addConditionEq('Type__c', 'Household Checklist').    
        orderBy('QuestionOrder__c', 'asc').  
        run();
        if(Test.isRunningTest())
            {
                integer intTest =1/0;
            }

    } catch (Exception Ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getQuestionsByType',null,Ex);
        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Household Checklist Questions',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));              
        }
        return questions;
    }

    @AuraEnabled(cacheable=true)
    public static Id getRecordType(string name) {

    return CommonUtils.getRecordTypeIdbyName('ProviderRecordQuestion__c', name);
    }

    @AuraEnabled
          public static void bulkAddRecordAns(String datas){
            List<ProviderRecordAnswer__c> listInsert = new List<ProviderRecordAnswer__c>();
            try {
                List<ProviderRecordAnswer__c> dataToInsert = (List<ProviderRecordAnswer__c>)System.JSON.deserialize(datas, List<ProviderRecordAnswer__c>.class);
                listInsert = new List<ProviderRecordAnswer__c>();
                for(ProviderRecordAnswer__c p: dataToInsert){
                    ProviderRecordAnswer__c objTask = new ProviderRecordAnswer__c();
                    objTask = new ProviderRecordAnswer__c(
                        id = p.id,
                        Date__c = p.Date__c,
                        ProviderRecordQuestion__c = p.ProviderRecordQuestion__c,
                        Comments__c = p.Comments__c,
                        Comar__c = p.Comar__c,
                        HouseholdStatus__c = p.HouseholdStatus__c);
                
                    listInsert.add(objTask);
                }                
            } catch (Exception Ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'bulkAddRecordAns',datas,Ex);
        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Household Checklist Answer Update',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));                 
            }
            // insert listInsert;
           DMLOperationsHandler.updateOrInsertSOQLForListWithStatus(listInsert);
    }

    @AuraEnabled(cacheable=false)
   public static List<ProviderRecordQuestion__c > getRecordQuestionId(string appId, string recordTypeId) {
        List<ProviderRecordQuestion__c > QuestionId = new List<ProviderRecordQuestion__c >();
        try {
            QuestionId =new DMLOperationsHandler('ProviderRecordQuestion__c').
            selectFields('Id').
            addConditionEq('Application__c', appId).
            addConditionEq('RecordTypeId', recordTypeId).
            run();
                if(Test.isRunningTest())
                {
                    if(appId==null || recordTypeId==null){
                        throw new DMLException();
                    }
                }            
        } catch (Exception Ex) {
            String[] arguments = new String[] {appId , recordTypeId};
            string strInputRecord= String.format('Input parameters are :: Application Id -- {0} :: recordTypeId -- {1}', arguments);            
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getRecordQuestionId',strInputRecord,Ex);
        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Household  Checklist Question Id',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
        }
       return QuestionId;
    }
    @AuraEnabled(cacheable=false)
    public static List<ProviderRecordAnswer__c > getProviderAnswer(string getAnswers) {
        List<ProviderRecordAnswer__c > Answers = new List<ProviderRecordAnswer__c >();
        try {
            List<String> fieldList = getAnswers.split(', *');
            Answers =
            new DMLOperationsHandler('ProviderRecordAnswer__c').
            selectFields('Id, ProviderRecordQuestion__c,Comar__c,Date__c,Comments__c,HouseholdStatus__c').
            addConditionIn('ProviderRecordQuestion__c', fieldList).
            run();
                            
        } catch (Exception Ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getProviderAnswer',getAnswers,Ex);
        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Household  Checklist Answers',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));            
        }
       return Answers;
    }    
}
