/**
 * @Author        : G.Tamilarasan
 * @CreatedOn     : March 16, 2020
 * @Purpose       : This component contains Staff Certification Creation
 **/

public with sharing class ProviderStaffCertificationController {
    public static string strClassNameForLogger='ProviderStaffCertificationController';


    // This function used to get all certification details

    @AuraEnabled(cacheable=false)
    public static List<Certification__c> getCertificationDetails(string certificationdetails) {
        List<sObject> reviewObj = new List<sObject>();
        try {
            String queryFields = 'Id, CertificationType__c, DueDate__c, ApplicationMailedDate__c, TestedDate__c, CertificationEffectiveDate__c, CertificateNumber__c, RenewalDate__c';

            reviewObj = new DMLOperationsHandler('Certification__c').
            selectFields(queryFields).
            addConditionEq('Staff__c', certificationdetails).
            orderBy('Name', 'Desc').
            run();
            if(Test.isRunningTest())
			{
                if(certificationdetails==''){
                    throw new DMLException();
                }
				
			}              
        } catch (Exception Ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getCertificationDetails',certificationdetails,Ex);

        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Staff Certification Details',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
	    throw new AuraHandledException(JSON.serialize(oErrorData));               
        }
        
        return reviewObj;
    }

    // This function used to get current edit certification details

    @AuraEnabled(cacheable=false)
    public static List<Certification__c> editCertificationDetails(string editcertificationdetails) {

        List<sObject> reviewObj = new List<sObject>();
        try {
            String queryFields = 'Id, CertificationStatus__c, CertificationType__c, CertificationSubType__c, DueDate__c, ApplicationMailedDate__c, TestedDate__c, CertificationEffectiveDate__c, CertificateNumber__c, BehavioralInterventions__c, RenewalDate__c';

            reviewObj = new DMLOperationsHandler('Certification__c').
            selectFields(queryFields).
            addConditionEq('Id', editcertificationdetails).
            run();
            if(Test.isRunningTest())
			{
                if(editcertificationdetails==''){
                    throw new DMLException();
                }
				
			}                  
        } catch (Exception Ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'editCertificationDetails',editcertificationdetails,Ex);

        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Staff Certification Details',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
	    throw new AuraHandledException(JSON.serialize(oErrorData));               
        }
        
        return reviewObj;
    }

    // This function used to delete the certificate

    @AuraEnabled
    public static string deleteCertificationDetails(Id deletecertificationdetails){
        return DMLOperationsHandler.deleteRecordById(deletecertificationdetails);
    }

    // This function used to insert the certificate
    
    @AuraEnabled
    public static string InsertUpdateStaffCertificate(sObject objSobjecttoUpdateOrInsert){
        return DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert);
    }

}