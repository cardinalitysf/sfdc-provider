@isTest
private class PublicProviderDashboard_Test{
    @IsTest static void testProviderDashboard()
    {
          

        RecordType rtype = [Select Name, Id From RecordType
                  where sObjectType='Contact' and Name = 'Household Members' and isActive=true];
        List<Account> lstAcct= TestDataFactory.testAccountData();
        insert lstAcct;
        List<Account> lstAcc= new List<Account>();
       
        for(Account acct : lstAcct)
        {
           acct.ProviderType__c = 'Public';
           lstAcc.add(acct);
        }
        update lstAcc;
        string strCseID='';
        // Insert records into cases object  which are approved
        List<Case> lstCase=TestDataFactory.TestCaseData(1,'Approved');
        List<Case> csList = new List<Case>();
        for(Case cse: lstCase)
        {
            cse.AccountId = lstAcct[0].Id;
            cse.status = 'Approved';
            csList.add(cse);
        }
        insert csList;
        List<Case> cList = [Select Id, CaseNumber FROM Case where Id IN:csList];
        
        List<Contact> conList = TestDataFactory.createTestContacts(5,lstAcct[0].Id,true);
        List<Contact> contList = new List<Contact>();
        for(Contact con: conList )
        {
            con.RecordTypeId = rtype.Id;
            contList.add(con);
        }
        update contList;
        List<ContentVersion> cvList = TestDataFactory.createTestContentVersionData();
        insert cvList;
        List<Actor__c> actrList = TestDataFactory.createTestActorData(5,true);
        List<Actor__c> acrList = new List<Actor__c>();
        for(Actor__c actr : actrList)
        {
           actr.Referral__c = cList[0].Id;
           actr.Provider__c = lstAcct[0].Id;
           actr.Contact__c = contList[0].Id;
           acrList.add(actr);
        }


        update acrList;
        List<Training__c> trList= new List<Training__c>();
         Training__c trang = new Training__c();
            trang.TrainingName__c='Budget Preparation' ;
            trList.add( trang );
        insert trList;
                
        Account oAcc= new Account();
        oAcc.ProviderType__c = 'Public';
        oAcc.Name = 'Test 1';
        insert oAcc;

        case oCase=new case();
        oCase.AccountId=oAcc.Id;
        insert ocase;

        publicProviderDashboard.getpublicProviderDashboard(cList[0].CaseNumber);

        try {
             publicProviderDashboard.getTrainingData('');
        } catch (Exception e) {}
        publicProviderDashboard.getTrainingData(trList[0].Name);

        try {
             publicProviderDashboard.getAllInstructorDetails();
        } catch (Exception e) {}

        // publicProviderDashboard.getAllInstructorDetails();

        publicProviderDashboard.PublicProviderWrapper prvWrap = new publicProviderDashboard.PublicProviderWrapper();
        Actor__c actorObj = new Actor__c();
        publicProviderDashboard.getpublicProviderDashboard('');
        
        
    }
}