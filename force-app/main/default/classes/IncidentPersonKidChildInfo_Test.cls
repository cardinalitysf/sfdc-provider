/**
     * @Author        : Jayachandran s
     * @CreatedOn     : Augest 18,2020
     * @Purpose       : Test Methods to unit test for IncidentPersonKidChildInfo.cls
**/

@isTest
public with sharing class IncidentPersonKidChildInfo_Test {
    @isTest static void testIncidentPersonKidChildInfo (){
    
        Contact InsertobjNewContact=new Contact(LastName = 'Cat'); 
        insert InsertobjNewContact;
        Actor__c InsertobjNewActor=new Actor__c(); 
        insert InsertobjNewActor;
        Case InsertobjNewCase= new Case(Status = 'status');
        insert InsertobjNewCase;
        Application__c InsertobjNewApplication=new Application__c(Status__c = 'Pending'); 
        insert InsertobjNewApplication;

        IncidentPersonKidChildInfo.InsertUpdateIncident(InsertobjNewContact);

        try{
            IncidentPersonKidChildInfo.getIncidentPersonChildDetails(null);
        }catch(Exception Ex) {

        }
        IncidentPersonKidChildInfo.getIncidentPersonChildDetails(InsertobjNewCase.Id);

        IncidentPersonKidChildInfo.getRecordType('Person/Kid');

        try {
            IncidentPersonKidChildInfo.EditIncidentPersonChildDetails(null);
        } catch (Exception Ex) {
            
        }
        IncidentPersonKidChildInfo.EditIncidentPersonChildDetails(InsertobjNewActor.Id);

        try {
            IncidentPersonKidChildInfo.getStaffMembers(null);
        } catch (Exception Ex) {
            
        }
        IncidentPersonKidChildInfo.getStaffMembers(InsertobjNewApplication.Id);
}
}

