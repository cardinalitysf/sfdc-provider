/**
 * @Author        : Pratheeba V
 * @CreatedOn     : June 19, 2020
 * @Purpose       : Public Provider Placement tab === get data with last sprint and update with reconsideration id
 **/

public with sharing class publicProvidersPlacementSpec {
    public static string strClassNameForLogger='publicProvidersPlacementSpec';
    @AuraEnabled(cacheable =true)
    public static List<DMLOperationsHandler.FetchValueWrapper> getPickListValues(sObject objInfo, string picklistFieldApi) {
        return DMLOperationsHandler.fetchPickListValue(objInfo,picklistFieldApi);
    }

    @AuraEnabled
    public static string InsertUpdateAppPlacement(sObject objSobjecttoUpdateOrInsert){
        return DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert);
    }

    @AuraEnabled
    public static string InsertUpdateAppPlacementAddr(sObject objSobjecttoUpdateOrInsert){
        return DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert);
    }

    @AuraEnabled(cacheable=false)
    public static List<Application__c> getApplicationByProvider(string providerid) {
        List<Application__c> appObj = new List<Application__c>();
        Set<String> fields = new Set<String>{'Id'};
        try {
            appObj = new DMLOperationsHandler('Application__c').
                        selectFields(fields).
                        addConditionEq('Provider__c', providerid).
                        addConditionEq('RecordType.Name', 'Public').
                        run();
            if(Test.isRunningTest()) {
                if(providerid=='') {
                    throw new DMLException();
                }				
			}
        } catch (Exception Ex) { 
            String[] arguments = new String[] {providerid};
            string strInputRecord= String.format('Input parameters are :: providerid -- {0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getApplicationByProvider',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Application Provider List',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return appObj;
    }

    @AuraEnabled(cacheable=false)
    public static List<Reconsideration__c> getDataByReconsiderationId(string reconsiderationId) {
        List<Reconsideration__c> reconsiderationObj = new List<Reconsideration__c>();
        Set<String> fields = new Set<String>{'Gender__c'};
        try {
            reconsiderationObj = new DMLOperationsHandler('Reconsideration__c').
                                    selectFields(fields).
                                    addConditionEq('Id', reconsiderationId).
                                    run();
            if(Test.isRunningTest()) {
                if(reconsiderationId=='') {
                    throw new DMLException();
                }				
			}
        } catch (Exception Ex) { 
            String[] arguments = new String[] {reconsiderationId};
            string strInputRecord= String.format('Input parameters are :: reconsiderationId -- {0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getDataByReconsiderationId',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Reconsideration List',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return reconsiderationObj;
    }
    
    @AuraEnabled(cacheable=false)
    public static List<Application__c> getProgramDetails(string applicationid) {
        List<Application__c> programObj = new List<Application__c>();
        Set<String> fields = new Set<String>{'Id','Program__c','ProgramType__c','Capacity__c','MinAge__c','MaxAge__c','Gender__c','Race__c','LicenseStartdate__c','LicenseEnddate__c','RespiteServices__c'};
        try {
            programObj = new DMLOperationsHandler('Application__c').
                                selectFields(fields).
                                addSubquery(
                                    DMLOperationsHandler.subquery('Address__r').
                                    selectFields('Id, Name, AddressType__c, AddressLine1__c').
                                    addConditionEq('AddressType__c', 'Payment')
                                    ).
                                addConditionEq('Id', applicationid). 
                                run();
            if(Test.isRunningTest()) {
                if(applicationid=='') {
                    throw new DMLException();
                }				
			}
        } catch (Exception Ex) { 
            String[] arguments = new String[] {applicationid};
            string strInputRecord= String.format('Input parameters are :: applicationid -- {0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getProgramDetails',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Program List',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return programObj;
    }

    @AuraEnabled(cacheable=false)
    public static List<SObject> fetchPlacementInformation(String id) {
        List<SObject> homeInformation = new List<SObject>();
        String fields ='Id,Capacity__c,MinAge__c,MaxAge__c,Gender__c,Race__c,StartDate__c,EndDate__c,RespiteService__c,Program__c,ProgramType__c';
        try {
            homeInformation = new DMLOperationsHandler('Reconsideration__c').
                                        selectFields(fields).
                                        addConditionEq('Id', id).
                                        run();
            if(Test.isRunningTest()) {
                if(id=='') {
                    throw new DMLException();
                }				
            }
        } catch (Exception Ex) { 
            String[] arguments = new String[] {id};
            string strInputRecord= String.format('Input parameters are :: id -- {0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'fetchPlacementInformation',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Placement List',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return homeInformation;
    }

    @AuraEnabled(cacheable=false)
    public static List<SObject> fetchAddressInformation(String appid) {
        List<sObject> addressInformation = new List<sObject>();
        String fields ='Id,AddressLine1__c,AddressLine2__c,AddressType__c,City__c,County__c,Email__c,Phone__c,State__c,ZipCode__c,Provider__c,Application__c,Application__r.Referral__c';
        try {
            addressInformation = new DMLOperationsHandler('Address__c').
                                        selectFields(fields).
                                        addConditionEq('Reconsideration__c', appid).
                                        addConditionEq('AddressType__c', 'Payment').
                                        run();
            if(Test.isRunningTest()) {
                if(appid=='') {
                    throw new DMLException();
                }				
            }
        } catch (Exception Ex) { 
            String[] arguments = new String[] {appid};
            string strInputRecord= String.format('Input parameters are :: appid -- {0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'fetchAddressInformation',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Address List',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }   
        return addressInformation;
    }
}
