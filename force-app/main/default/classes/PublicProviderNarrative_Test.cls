/**
     * @Author        : Balamurugan B
     * @CreatedOn     : June 06,2020
     * @Purpose       :Test Methods to unit test for PublicProviderNarrative.cls
     **/

@isTest
public class PublicProviderNarrative_Test {
      @isTest static void testgetNarrativeDetails (){

        Case InsertobjNewCase = new Case(Status = 'Pending'); 
        insert InsertobjNewCase;

        HistoryTracking__c InsertobjNewHistoryTracking = new HistoryTracking__c(RichNewValue__c = ' ');
        insert InsertobjNewHistoryTracking;

        PublicProviderNarrative.updateNarrativeDetails(InsertobjNewCase);
        try
          {
          PublicProviderNarrative.getNarrativeDetails(null);
          }catch(Exception Ex)
          {
         
          }
          PublicProviderNarrative.getNarrativeDetails('Case');

          try
           {
            PublicProviderNarrative.getSelectedHistoryRec(null);
           }catch(Exception Ex)
           {
               
           }               
           PublicProviderNarrative.getSelectedHistoryRec('history');   

           try
           {
            PublicProviderNarrative.getHistoryOfNarrativeDetails(null);
           }catch(Exception Ex)
           {
               
           }               
           PublicProviderNarrative.getHistoryOfNarrativeDetails('text');   

           try
           {
            PublicProviderNarrative.getPublicProviderStatus(null);
           }catch(Exception Ex)
           {
               
           }               
           PublicProviderNarrative.getPublicProviderStatus('YYY');   
      
      }

}