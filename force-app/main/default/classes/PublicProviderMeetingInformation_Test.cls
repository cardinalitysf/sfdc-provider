/**
     * @Author        : Balamurugan.B
     * @CreatedOn     : June 07,2020
     * @Purpose       :Test Methods to unit test for PublicProviderMeetingInformation.cls
     **/


@isTest
public class PublicProviderMeetingInformation_Test {
    @isTest static void testgetMeetingInfo(){
        Actor__c InsertobjNewMeeting=new Actor__c(Role__c = 'test Account');
        insert InsertobjNewMeeting;
        Training__c trang = new Training__c(TrainingName__c = 'Yes');
        insert trang;
        MeetingInfo__c meetnginfo = new MeetingInfo__c(Training__c = trang.Id);
        insert meetnginfo;            
        try {
            PublicProviderMeetingInformation.getHouseholdMembers(InsertobjNewMeeting.id);
        } catch (Exception e) { }
        try {
            PublicProviderMeetingInformation.getTrainingDetails('RecordType.Name');
        } catch (Exception e) { }
        try {
            PublicProviderMeetingInformation.getMeetingDetails('Training__c');
        } catch (Exception e) { }
        try {
            PublicProviderMeetingInformation.viewMeetingDetails('');
        } catch (Exception e) { }
        try {
            PublicProviderMeetingInformation.getPublicDecisionStatus('');
        } catch (Exception e) { }
        try {
            PublicProviderMeetingInformation.getMeetingAttendedHours('');
        } catch (Exception e) { }
        string strUpdate = '[{"HouseholdMember__c": "'+meetnginfo.Id+'","Training__c": "'+trang.Id+'","MeetingStatus__c":"Completed","Date__c":"2020-05-21","SessionNumber__c":"Session 1","StartTime__c":"00:15:00.000Z","EndTime__c":"13:30:00.000Z","Referral__c":" ","TotalHoursattended__c":" "}]';
        try {
            PublicProviderMeetingInformation.InsertUpdateMeetingInfo(strUpdate);
        } catch (Exception e) { }
    }

    @isTest static void testgetMeetingInfoException(){
        try {
            PublicProviderMeetingInformation.getHouseholdMembers('');
        } catch (Exception e) { }
        try {
            PublicProviderMeetingInformation.getTrainingDetails('');
        } catch (Exception e) { }
        try {
            PublicProviderMeetingInformation.getMeetingDetails('');
        } catch (Exception e) { }
        try {
            PublicProviderMeetingInformation.getMeetingDetails('');
        } catch (Exception e) { }
    }
}