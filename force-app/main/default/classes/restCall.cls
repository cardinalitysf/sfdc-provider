@RestResource(urlMapping='/Dialogflow')
global class restCall {
    @HTTPPost
    global static JSON2Apex createRecords(){
        String request = RestContext.request.requestBody.toString();
         DialogFlowResponse r = DialogFlowResponse.parse(request);       
         string textToSpeech;
         string text ;
             string strFirstName='Jessica';
        	string strLastName= 'Adams';
            string strEmail=r.queryResult.parameters.Email;
        strEmail='jameswhites462@gmail.com';
         RestResponse res = RestContext.response;
    	
      
        if(r.queryResult.intent.displayName=='Create Account')
        {
            Account acc= new Account();
            acc.ProviderType__c='Private';
            acc.ParentCorporation__c='California';
            acc.Corporation__c='Texas';
            acc.TaxId__c=r.queryResult.parameters.TaxId;
            acc.Name=r.queryResult.parameters.Name;
            acc.BillingStreet='1405 Holloway Drive, Lafayette, IN, USA';
            acc.FirstName__c=strFirstName;
            acc.LastName__c=strLastName;
            acc.ShippingStreet='';
            acc.Email__c=strEmail;
            acc.BillingState='Indiana';
            acc.Phone='';
            acc.BillingCity='Lafayette';
            acc.CellNumber__c='';
            acc.BillingCountry='Tippecanoe County';
            acc.Fax='';
            acc.BillingPostalCode='47905';
            acc.Profit__c='';
            acc.SON__c='No';
            acc.RFP__c='Yes';
           Database.UpsertResult Upsertresults = Database.upsert(acc);
           string strAccId= Upsertresults.getId();
        
            Case oCase= new Case();
            oCase.Status='Draft';
            oCase.Priority='Medium';
            oCase.AccountId=strAccId;            
            oCase.Origin=r.queryResult.parameters.Communication;
            oCase.ReceivedDate__c=system.now();
            oCase.RecordTypeId= Schema.Sobjecttype.Case.getRecordTypeInfosByName().get('Referral').getRecordTypeId();           
            oCase.ProgramType__c=r.queryResult.parameters.ProgramType;
            oCase.Program__c=r.queryResult.parameters.Program;
            Database.UpsertResult caseUpsertresults = Database.upsert(oCase);
           Case ocase1 =[select Casenumber from case where ID=:caseUpsertresults.getId()]; 
            
            Integer len = 10;
			Blob blobKey = crypto.generateAesKey(128);
			String key = EncodingUtil.convertToHex(blobKey);
			String rdmNumber = key.substring(0,len);
            
              List<Profile> providerCommList = referralDecision.getProvidercommunity();
              User myuserobject = new User();
              myuserobject.ProfileId = providerCommList[0].Id;
              myuserobject.Username = strEmail + '.' + rdmNumber;
              myuserobject.Alias =  strFirstName;
              myuserobject.Email = strEmail;
              myuserobject.FirstName = strFirstName;
              myuserobject.LastName =strLastName;
              myuserobject.TimeZoneSidKey = 'GMT';
			  myuserobject.LanguageLocaleKey = 'en_US';
			  myuserobject.EmailEncodingKey = 'UTF-8';
			  myuserobject.LocaleSidKey = 'en_US';
			  myuserobject.IsActive = true;
            
            try {
			
			  Database.DMLOptions dmlOption = new Database.DMLOptions();
			  dmlOption.assignmentRuleHeader.useDefaultRule = true; // successfully assigns User
			
			  dmlOption.EmailHeader.triggerUserEmail = true;
			  dmlOption.EmailHeader.triggerAutoResponseEmail = true;
			 
			  myuserobject.setOptions(dmlOption);
			
			  insert myuserobject;
			  Database.update(myuserobject, dmlOption); // Assuming userObj is User Object instance
			
			} catch (exception ex) {
		
			}
            
           textToSpeech ='<speak><emphasis level=\'high\'>Hurray!!</emphasis>'+
               			'<break time=\'.5s\'/> Private Referral has been created succesfully and here is the referral number <break time=\'.8s\'/>'+
               			'<say-as interpret-as=\'characters\'>'+ocase1.Casenumber+'</say-as>'+
               			'<break time=\'.5s\'/>. Login details has been created and successfully sent to '+strEmail+'</speak>';
         
            
           
           
    	}
        else if(r.queryResult.intent.displayName=='ChangeReferralStatus')
        {
            
              string strCasenumber=r.queryResult.parameters.CaseNumber;
            
            System.debug('Casenumber'+strCasenumber);
              string strstatus= r.queryResult.parameters.Status;
              System.debug('status'+strstatus);
              Case ocase =[select id,Status  from case where Casenumber=:strCasenumber]; 
            
            case oCse=new Case();
            oCse.Id=ocase.Id;
            oCse.Status=strstatus;
            upsert oCse;
            
              textToSpeech ='<speak><emphasis level=\'high\'>Status has been succefully updated from '+ocase.status+' to '+strstatus+'</emphasis>'+
               			    '</speak>';
     
        }
        
         text = '{"payload": {"google": {"expectUserResponse": false,"richResponse": {"items": [{"simpleResponse": {"textToSpeech": "'+textToSpeech+'"}}] }}}}}';
              
         res.responseBody = Blob.valueOf(JSON.serialize(text));
            res.statusCode = 200;  
       return JSON2Apex.parse(text);      
    }
    
}