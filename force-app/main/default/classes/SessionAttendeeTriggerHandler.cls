public with sharing class SessionAttendeeTriggerHandler implements ITriggerHandler{
	/**
	 * @Author        : V.S.Marimuthu
	 * @CreatedOn     : May 27 ,2020
	 * @Purpose       : Handler class which will do hand shake between trigger and task service
	 **/
	public Boolean IsDisabled(){
		// string blnTriggerSwitch = 'false';
		// List<Bypass_Switch_For_Trigger__mdt> triggerSwitch = [select
		//                                                             Trigger_Switch__c
		//                                                       from Bypass_Switch_For_Trigger__mdt];
		// for (Bypass_Switch_For_Trigger__mdt value : triggerSwitch){
		//     blnTriggerSwitch = value.Trigger_Switch__c;
		// }
		// 
		// return blnTriggerSwitch == 'false' ? false : true;
		return false;
	}

	public void BeforeInsert(List<Sobject> newItems){
	
	}

	public void BeforeUpdate(Map<id, sObject> newItems, Map<id, sObject> oldItems){
	
	}

	public void BeforeDelete(Map<id, sObject> oldItems){
	
	}

	public void AfterInsert(List<Sobject> newMap){
		
	}

	
	private void changeApplicationStatusDependsOnSessionAttendeStatus(Map<Id, sObject> newMap, Map<Id, sObject> oldMap){
		try{
            Set<Id> lstApplicationId= new Set<Id>();
            Set<Id> lstReferralId= new Set<Id>();
		   // for(SessionAttendee__c objSessionAttendeeInfo : ((Map<Id,SessionAttendee__c>)newMap).values()){
		    for(SessionAttendee__c objSessionAttendeeInfo : [Select Id,Training__r.Type__c,SessionStatus__c from SessionAttendee__c where ID in : ((Map<Id,SessionAttendee__c>)newMap).keyset()]){
		        //Create an old and new map so that we can compare values
		        SessionAttendee__c oldSessionAttendeeInfo = (SessionAttendee__c)oldMap.get(objSessionAttendeeInfo.ID);
                SessionAttendee__c newSessionAttendeeInfo = (SessionAttendee__c)newMap.get(objSessionAttendeeInfo.ID);  
                if(oldSessionAttendeeInfo.SessionStatus__c != newSessionAttendeeInfo.SessionStatus__c){
                    if(objSessionAttendeeInfo.Training__r.Type__c == ProviderConstants.TRAINING_TYPE_INFORMATION_MEETING) 
                   {
                     lstReferralId.add(oldSessionAttendeeInfo.Referral__c);
                   }  
                   else if(objSessionAttendeeInfo.Training__r.Type__c == ProviderConstants.TRAINING_TYPE_PRE_SERVICE_TRAINING)  
                   {         
                    lstApplicationId.add(oldSessionAttendeeInfo.Application__c);
                   }
		        }    
            }  
           
            if(lstApplicationId.size()>0){
                UpdateApplicationForPrideTraining(lstApplicationId);
            } 
         
            if(lstReferralId.size()>0)
            {
                UpdateReferralForNonPrideTraining(lstReferralId);
            }
		}
		catch(Exception Ex)
		{
		}
	}
private void UpdateApplicationForPrideTraining(Set<ID> lstApplicationId)
{
    Map<Id,String> objCompleteMaptoUpdate=new Map<Id,String>();
    Map<Id,String> objDNAMaptoUpdate=new Map<Id,String>();
    Map<Id,String> objScheduleMaptoUpdate=new Map<Id,String>();
    Map<Id,String> objCancelledMaptoUpdate=new Map<Id,String>();
    if(lstApplicationId.size()>0){
        List<SessionAttendee__c> lstSessionAttendeeInfo = new DMLOperationsHandler('SessionAttendee__c').
                                                            selectFields('Id,SessionStatus__c,Application__c').
                                                            addConditionIn('Application__c', lstApplicationId).
                                                            run(); 
      if(lstSessionAttendeeInfo.size()>0){
            for(SessionAttendee__c objSessionAttendee : lstSessionAttendeeInfo)
            {  
                if(objSessionAttendee.SessionStatus__c==ProviderConstants.MEETINGINFO_COMPLETED)
                {
                    objCompleteMaptoUpdate.put(objSessionAttendee.Application__c,objSessionAttendee.SessionStatus__c);
                }
                 if(objSessionAttendee.SessionStatus__c==ProviderConstants.MEETINGINFO_DNA)
                {
                    objDNAMaptoUpdate.put(objSessionAttendee.Application__c,objSessionAttendee.SessionStatus__c);
                }
                if(objSessionAttendee.SessionStatus__c==ProviderConstants.MEETINGINFO_SCHEDULED)
                {
                    objScheduleMaptoUpdate.put(objSessionAttendee.Application__c,objSessionAttendee.SessionStatus__c);
                }
                if(objSessionAttendee.SessionStatus__c==ProviderConstants.MEETINGINFO_CANCELLED)
                {
                    objCancelledMaptoUpdate.put(objSessionAttendee.Application__c,objSessionAttendee.SessionStatus__c);
                }                       
            }
      }
    if(objScheduleMaptoUpdate.size()>0)
    {
        for( Id remId : objScheduleMaptoUpdate.keySet())
        {
            objCompleteMaptoUpdate.remove(remId);
        }
    }
    if(objScheduleMaptoUpdate.size()>0)
    {
        for( Id remId : objScheduleMaptoUpdate.keySet())
        {
            objCompleteMaptoUpdate.remove(remId);
        }
    }
    if(objDNAMaptoUpdate.size()>0)
    {
        for( Id remId : objDNAMaptoUpdate.keySet())
        {
            objCompleteMaptoUpdate.remove(remId);
        }
    }       
    List<Application__c> objAppToUpdate= new List<Application__c>();          
    if(objCompleteMaptoUpdate.size()>0)
    {
        for( Id refId : objCompleteMaptoUpdate.keySet())
        {
            Application__c oApp= new Application__c();
            oApp.id=refId;
            oApp.Status__c=ProviderConstants.CASE_TRAINING_STATUS_COMPLETED;
            objAppToUpdate.add(oApp);
        }
    }
    if(objDNAMaptoUpdate.size()>0)
    {
        for( Id refId : objDNAMaptoUpdate.keySet())
        {
        Application__c oApp= new Application__c();
        oApp.id=refId;
        oApp.Status__c=ProviderConstants.CASE_TRAINING_STATUS_UNATTENTED;
        objAppToUpdate.add(oApp);
        }
    }
    if(objAppToUpdate.size()>0)
    {
     DMLOperationsHandler.updateOrInsertSOQLForList(objAppToUpdate);
    }
}
}
private void UpdateReferralForNonPrideTraining(Set<ID> lstReferralId)
{
    Map<Id,String> objCompleteMaptoUpdate=new Map<Id,String>();
    Map<Id,String> objDNAMaptoUpdate=new Map<Id,String>();
    Map<Id,String> objScheduleMaptoUpdate=new Map<Id,String>();
    Map<Id,String> objCancelledMaptoUpdate=new Map<Id,String>();
    if(lstReferralId.size()>0){
        List<SessionAttendee__c> lstSessionAttendeeInfo = new DMLOperationsHandler('SessionAttendee__c').
                                                            selectFields('Id,SessionStatus__c,Referral__c').
                                                            addConditionIn('Referral__c', lstReferralId).
                                                           run(); 
      if(lstSessionAttendeeInfo.size()>0){
            for(SessionAttendee__c objSessionAttendee : lstSessionAttendeeInfo)
            {  
                if(objSessionAttendee.SessionStatus__c==ProviderConstants.MEETINGINFO_COMPLETED)
                {
                    objCompleteMaptoUpdate.put(objSessionAttendee.Referral__c,objSessionAttendee.SessionStatus__c);
                }
                 if(objSessionAttendee.SessionStatus__c==ProviderConstants.MEETINGINFO_DNA)
                {
                    objDNAMaptoUpdate.put(objSessionAttendee.Referral__c,objSessionAttendee.SessionStatus__c);
                }
                if(objSessionAttendee.SessionStatus__c==ProviderConstants.MEETINGINFO_SCHEDULED)
                {
                    objScheduleMaptoUpdate.put(objSessionAttendee.Referral__c,objSessionAttendee.SessionStatus__c);
                }
                if(objSessionAttendee.SessionStatus__c==ProviderConstants.MEETINGINFO_CANCELLED)
                {
                    objCancelledMaptoUpdate.put(objSessionAttendee.Referral__c,objSessionAttendee.SessionStatus__c);
                }                       
            }
      }
    List<Case> objAppToUpdate= new List<Case>();          
    if(objCompleteMaptoUpdate.size()>0)
    {

        for( Id refId : objCompleteMaptoUpdate.keySet())
        {
            Case oApp= new Case();
            oApp.id=refId;
            oApp.Status=ProviderConstants.CASE_TRAINING_STATUS_COMPLETED;
            objAppToUpdate.add(oApp);
        }
    }
    else
    {
            if(objDNAMaptoUpdate.size()>0)
            {
                for( Id refId : objDNAMaptoUpdate.keySet())
                {
                  Case oApp= new Case();
                oApp.id=refId;
                oApp.Status=ProviderConstants.CASE_TRAINING_STATUS_UNATTENTED;
                objAppToUpdate.add(oApp);
                }
            }
    }
    if(objAppToUpdate.size()>0)
    {
      DMLOperationsHandler.updateOrInsertSOQLForList(objAppToUpdate);
    }
}
}

	//call the method to update the values
	public void AfterUpdate(Map<Id, sObject> newMap, Map<Id, sObject> oldMap){
		changeApplicationStatusDependsOnSessionAttendeStatus(newMap,oldMap);
	
	}

	public void AfterDelete(Map<id, sObject> oldMap){
		
	}

	public void AfterUndelete(List<Sobject> newMap){
		
	}
}