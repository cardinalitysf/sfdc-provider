/**
 * @Author        : Balamurugan
 * @CreatedOn     : May 18,2020
 * @Purpose       :Attachments based on Provider Application
 **/


public with sharing class referralProviderAttachmentsController {
    @AuraEnabled(cacheable=true)
    public static List<Case> fetchCase(String intakeId) {
        String model = 'Case';
        String fields = 'Id,CaseNumber,ReceivedDate__c,CreatedDate,Owner.FirstName,Owner.Lastname,Origin';
        String cond = 'Id = \''+ intakeId +'\'';
        return DMLOperationsHandler.selectSOQLWithConditionParameters (model, fields, cond);
    }

    
    @AuraEnabled(cacheable=true)
    public static List<Application__c> fetchApplication(String intakeId) {
        String model = 'Application__c';
        String fields = 'id,CreatedDate,Provider__c,Name,Status__c';
        String cond = 'Id = \''+ intakeId +'\'';
        return DMLOperationsHandler.selectSOQLWithConditionParameters (model, fields, cond);
    }


    @AuraEnabled(cacheable=true)
    public static List<Monitoring__c> fetchMonitoring(String intakeId) {
        String model = 'Monitoring__c';
        String fields = 'Id, Name,ApplicationLicenseId__c, AnnouncedUnannouced__c, JointlyInspectedWith__c,SelectPeriod__c, Status__c, SiteAddress__c, VisitationEndDate__c, VisitationStartDate__c, TimetoVisit__c';
        String cond = 'Id = \''+ intakeId +'\'';
        return DMLOperationsHandler.selectSOQLWithConditionParameters (model, fields, cond);
    }

  
    @AuraEnabled(cacheable=true)
    public static List<Account> fetchProvider(String intakeId) {
        String model = 'Account';
        String fields = 'Id,Name,ProviderId__c,CreatedDate,Owner.FirstName,Owner.Lastname';
        String cond = 'Id = \''+ intakeId +'\'';
        return DMLOperationsHandler.selectSOQLWithConditionParameters (model, fields, cond);
    }


    @AuraEnabled(cacheable=true)
    public static List<User> fetchUserProfile(String UserId) {
        String model = 'User';
        String fields = 'Id,IsActive,ProfileId,Profile.Name';
        String cond = 'Id = \''+ UserId +'\'';
        return DMLOperationsHandler.selectSOQLWithConditionParameters (model, fields, cond);
    }

    @AuraEnabled
    public static list<contentversion>  SaveFile(Id parentId, String fileName, String base64Data, String contentType,string category,string subCategory,string documentTitle,String description) {         
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        ContentVersion conVer = new ContentVersion();
        conVer.Document__c    = category; 
        conVer.CWhealth__c = subCategory; 
        conVer.PathOnClient = fileName; 
        conVer.Description = description;
        conVer.Title = documentTitle; 
        conVer.VersionData = EncodingUtil.base64Decode(base64Data);
         
        insert conVer;
        
        Id conDoc = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id].ContentDocumentId;
        
        //Create ContentDocumentLink
        ContentDocumentLink cDe = new ContentDocumentLink();
        
        cDe.ContentDocumentId = conDoc;
        cDe.LinkedEntityId = parentId; // you can use objectId,GroupId etc
        cDe.Visibility = 'AllUsers';
        cDe.ShareType = 'I';
        insert cDe;        
        Map<Id, ContentDocumentLink> contentDocumentIdsmap = new Map<Id, ContentDocumentLink>();
        for(ContentDocumentLink conDocu : [SELECT ContentDocumentId, LinkedEntityId,LinkedEntity.Name FROM ContentDocumentLink WHERE LinkedEntityId =:parentId LIMIT 50000]){
             
            contentDocumentIdsmap.put(conDocu.ContentDocumentId, conDocu);
        }
        list<contentversion> contelist = [select id,ContentDocumentId,Description,DocumentSize__c,ContentDocument.ContentSize,CWhealth__c,PathOnClient,Document__c,Title, Createddate,filetype,createdBy.Name from contentversion where ContentDocumentId In: contentDocumentIdsmap.keyset()  limit 50000];
        return contelist;
    }
    
    @AuraEnabled
    public static list<contentversion>  EditFile(Id parentId, Id recordId,string category,string subCategory) {       
        contentversion conUpdate = [SELECT Id from contentversion where id =:recordId limit 1];        
        update conUpdate;        
        Map<Id, ContentDocumentLink> contentDocumentIdsmap = new Map<Id, ContentDocumentLink>();
        for(ContentDocumentLink conDocu : [SELECT ContentDocumentId, LinkedEntityId,LinkedEntity.Name FROM ContentDocumentLink WHERE LinkedEntityId =:parentId LIMIT 50000]){
            contentDocumentIdsmap.put(conDocu.ContentDocumentId, conDocu);
        }
        list<contentversion> contelist = [select id,ContentDocumentId,Description,DocumentSize__c,ContentDocument.ContentSize,CWhealth__c,PathOnClient,Document__c,Title, Createddate,filetype,createdBy.Name from contentversion where ContentDocumentId In: contentDocumentIdsmap.keyset()  limit 50000];
        return contelist;        
    }
    
    @AuraEnabled(cacheable=true)
    public static  list<contentversion>  fetchDocuments(String intakeId) {
        Map<Id, ContentDocumentLink> contentDocumentIdsmap = new Map<Id, ContentDocumentLink>();
        for(ContentDocumentLink conDocu : [SELECT ContentDocumentId, LinkedEntityId,LinkedEntity.Name FROM ContentDocumentLink WHERE LinkedEntityId =:intakeId LIMIT 50000]){
             
            contentDocumentIdsmap.put(conDocu.ContentDocumentId, conDocu);
        }        
        list<contentversion> contelist = [select id,ContentDocumentId,Description,DocumentSize__c,ContentDocument.ContentSize,CWhealth__c,PathOnClient,Document__c,Title, Createddate,filetype,createdBy.Name from contentversion where ContentDocumentId In: contentDocumentIdsmap.keyset() AND IsSignature__c=false limit 50000];
        
        return contelist;
    }
    
    @AuraEnabled   
    public static contentversion  getSelectedFileDetails(String recordId) {       
        contentversion contentver = [select id,ContentDocumentId,Description,DocumentSize__c,ContentDocument.ContentSize,CWhealth__c,PathOnClient,Document__c,Title, Createddate,filetype,createdBy.Name from contentversion where id =:recordId limit 1];
       
        return contentver;
    }    
    
    @AuraEnabled  
    public static list<contentversion> deleteFiles(Id parentId,string sdocumentId){ 
        delete [SELECT Id FROM ContentDocument WHERE id=:sdocumentId];       
        Map<Id, ContentDocumentLink> contentDocumentIdsmap = new Map<Id, ContentDocumentLink>();
        for(ContentDocumentLink conDocu : [SELECT ContentDocumentId, LinkedEntityId,LinkedEntity.Name FROM ContentDocumentLink WHERE LinkedEntityId =:parentId LIMIT 50000]){
            contentDocumentIdsmap.put(conDocu.ContentDocumentId, conDocu);
        }
        list<contentversion> contelist = [select id,ContentDocumentId,Description,DocumentSize__c,ContentDocument.ContentSize,CWhealth__c,PathOnClient,Document__c,Title, Createddate,filetype,createdBy.Name from contentversion where ContentDocumentId In: contentDocumentIdsmap.keyset()  AND IsSignature__c=false limit 50000];
        return contelist;
    }
    
    @AuraEnabled(cacheable=true)
    public static List<String> getPicklistvalues(String objectName, String field_apiname, Boolean nullRequired){
        List<String> optionlist = new List<String>();        
        Map<String,Schema.SObjectType> obj = Schema.getGlobalDescribe(); 
        Map<String, Schema.SObjectField> field_map = obj.get(objectName.toLowerCase()).getDescribe().fields.getMap();        
        List<Schema.PicklistEntry> picklistValues = field_map.get(field_apiname).getDescribe().getPickListValues();        
        if(nullRequired == true){
            optionlist.add('--None--');
        }        
        for (Schema.PicklistEntry pv : picklistValues) {
            optionlist.add(pv.getValue());
        }
      
        return optionlist;
    }
    
    @AuraEnabled
    public static datetime getcurrentDateTime()
    {
        return System.now();
    }

    @AuraEnabled(cacheable=true)
    public static List<Case> getReferralProviderStatus(String caseId) {
        String model = 'Case';
        String fields = 'Id, Status';
        String cond = 'Id = \''+ caseId +'\'';
        return DMLOperationsHandler.selectSOQLWithConditionParameters (model, fields, cond);
    }

    @AuraEnabled(cacheable = true)
    public static List<Application__c> getApplicationProviderStatus (String applicationId) {
        String model = 'Application__c';
        String fields = 'Id, Status__c';
        String cond ='Id = \''+ applicationId +'\'';

        return DMLOperationsHandler.selectSOQLWithConditionParameters (model, fields, cond);
    }
}