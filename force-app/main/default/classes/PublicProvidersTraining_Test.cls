/**
 * @Author        : Pratheeba.V
 * @CreatedOn     : July 24,2020
 * @Purpose       : Test class for publicProvidersTraining class
 **/
@isTest
private class PublicProvidersTraining_Test {
    @isTest static void testPublicProvidersTraining_Positive() {
        Actor__c InsertobjNewActor=new Actor__c(Role__c = 'XYZ'); 
        insert InsertobjNewActor;
        Training__c InsertobjNewTraining=new Training__c(TrainingName__c = 'XYZ'); 
        insert InsertobjNewTraining;
        SessionAttendee__c InsertobjNewSessionAttendee=new SessionAttendee__c(Training__c = InsertobjNewTraining.Id); 
        insert InsertobjNewSessionAttendee;
        Account InsertobjNewSessionAccount=new Account(Name ='HGJ'); 
        insert InsertobjNewSessionAccount;
        Reconsideration__c InsertobjNewSessionReconsideration=new Reconsideration__c (Gender__c ='HGJ'); 
        insert InsertobjNewSessionReconsideration;
        try {
            publicProvidersTraining.getHouseholdMembers(InsertobjNewActor.Provider__c);
        } catch (Exception e) { }  
        try {
            publicProvidersTraining.getTrainingDetails(InsertobjNewTraining.Type__c);
        } catch (Exception e) { } 
        try {
            publicProvidersTraining.getSessionTraining(InsertobjNewSessionAttendee.Reconsideration__c,'HGDF');
        } catch (Exception e) { } 
        try {
            publicProvidersTraining.viewSessionDetails(InsertobjNewSessionAttendee.Id);
        } catch (Exception e) { } 
        try {
            publicProvidersTraining.selectActor(InsertobjNewActor.Application__c,'VVV');
        } catch (Exception e) { } 
        string str = '[{"Actor__c":"'+InsertobjNewActor.Id+'","Training__c":"'+InsertobjNewTraining.Id+'","SessionStatus__c":"Scheduled","Referral__c":"'+InsertobjNewSessionAccount.Id+'","TotalHoursAttended__c":"","Reconsideration__c":"'+InsertobjNewSessionReconsideration+'"}]';
        try {
            publicProvidersTraining.InsertUpdateSessionInfo(str);
        } catch (Exception e) { } 
        try {
            publicProvidersTraining.getSessionAttendedHours(InsertobjNewSessionAttendee.Reconsideration__c);
        } catch (Exception e) { } 
    }
    @isTest static void testPublicProvidersTraining_Exception() {
        try {
            publicProvidersTraining.getHouseholdMembers('');
        } catch (Exception e) { }  
        try {
            publicProvidersTraining.getTrainingDetails('');
        } catch (Exception e) { } 
        try {
            publicProvidersTraining.getSessionTraining('','');
        } catch (Exception e) { }
        try {
            publicProvidersTraining.viewSessionDetails('');
        } catch (Exception e) { } 
        try {
            publicProvidersTraining.selectActor('','');
        } catch (Exception e) { } 
        try {
            publicProvidersTraining.getSessionAttendedHours('');
        } catch (Exception e) { }         
    }
}