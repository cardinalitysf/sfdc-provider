/**
 * @Author        : V.S.Marimuthu
 * @CreatedOn     : March 18 ,2020
 * @Purpose       : Test Methods to unit test GetStreetAddressFromGoogleAPI class in GetStreetAddressFromGoogleAPI.cls
 **/
@isTest
private class GetStreetAddressFromGoogleAPI_Test {

    private class RestMock implements HttpCalloutMock {

        public HTTPResponse respond(HTTPRequest req) {
            String fullJson = 'your Json Response';

            HTTPResponse res = new HTTPResponse();
            res.setHeader('Content-Type', 'text/json');
            res.setBody(fullJson);
            res.setStatusCode(200);
            return res;
        }
    }
    //Positive flow
   @isTest static void testGetSuggestions()
   {

    Test.setMock(HttpCalloutMock.class, new RestMock());
    Test.startTest();
    try{
        string strStreetValue=GetStreetAddressFromGoogleAPI.GetSuggestions('');
    }catch(Exception Ex){

    }
    string strStreetValue=GetStreetAddressFromGoogleAPI.GetSuggestions('1');
    strStreetValue=GetStreetAddressFromGoogleAPI.getPlaceDetails('1');
    Test.stopTest();   
       

   }
}