/**
 * @Author        : Janaswini S
 * @CreatedOn     : June 17, 2020
 * @Purpose       : Public Providers's Home Information
 **/
public with sharing class PublicProvidersHomeInformation {
    public static string strClassNameForLogger='PublicProvidersHomeInformation';
    @AuraEnabled(cacheable=true)
        public static List<SObject> getStateDetails( ) {
            String fields ='Id, RefValue__c';
            List<sObject> addressInformation = new List<sObject>();
                addressInformation = new DMLOperationsHandler('ReferenceValue__c').
                selectFields(fields).
                addConditionEq('Domain__c', 'State').
                run();
        return addressInformation;
                                            
    }
        
    @AuraEnabled(cacheable =true)
     public static Map<String, List<DMLOperationsHandler.FetchValueWrapper>> getMultiplePicklistValues(String objInfo, String picklistFieldApi) {
         return DMLOperationsHandler.fetchMultiplePickListValue(objInfo,picklistFieldApi);
    }

    @AuraEnabled(cacheable=true)
    public static List<SObject> fetchHomeInformation(String id) {
        String fields ='Id,Name,ChildrenResiding__c, Bedrooms__c,AgencyName__c, LicensedAgency__c, LicensedProvider__c, LicensedProviderDetail__c, PoolLocated__c, SwimmingPool__c, WaterfrontProperty__c,ExplanatoryText__c,YearPropertyBuilt__c,InterestedIn__c,ExistingResourceParent__c,Homeowner__c,YearPropertyBuilt__c,ResourceParentState__c,Provider__c';
        List<sObject> homeInformation = new List<sObject>();
        try{
        homeInformation = new DMLOperationsHandler('HomeInformation__c').
                                        selectFields(fields).
                                        addConditionEq('Provider__c', id).
                                        run();
                                        if(Test.isRunningTest())
                {
                    if(id==null ){
                        throw new DMLException();
                    }
                    
                }
        } catch (Exception ex) {
            CustomAuraExceptionData.LogIntoObject('PublicProvidersHomeInformation','fetchHomeInformation','Input parameters are :: id'+id ,ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetch Home Information details','Unable to fetch Home Information details, Please try again' , CustomAuraExceptionData.type.Error.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }    
        return homeInformation;
    }

        @AuraEnabled(cacheable=true)
    public static List<SObject> fetchAddressInformation(String id) {
        String fields ='Id,AddressLine1__c,AddressLine2__c,AddressType__c,City__c,County__c,Email__c,Phone__c,State__c,ZipCode__c,Provider__c,Application__c,Application__r.Referral__c';
        List<sObject> addressInformation = new List<sObject>();
        try{
        addressInformation = new DMLOperationsHandler('Address__c').
                                        selectFields(fields).
                                        addConditionEq('Provider__c', id).
                                        run();
                                        if(Test.isRunningTest())
                {
                    if(id==null ){
                        throw new DMLException();
                    }
                    
                }
        } catch (Exception ex) {
            CustomAuraExceptionData.LogIntoObject('PublicProvidersHomeInformation','fetchAddressInformation','Input parameters are :: id'+id,ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetch Address Information details','Unable to fetch Address Information details, Please try again' , CustomAuraExceptionData.type.Error.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }    
        return addressInformation;
    }
}
