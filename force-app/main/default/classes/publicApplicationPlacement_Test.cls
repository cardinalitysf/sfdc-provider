/**
 * @Author        : Pratheeba.V
 * @CreatedOn     : June 8 ,2020
 * @Purpose       : Test Methods to unit test publicApplicationPlacement class
**/
@IsTest
private class publicApplicationPlacement_Test {
    @IsTest static void testpublicApplicationPlacementPositive(){
        Application__c InsertobjNewApplication=new Application__c(Status__c = 'Pending'); 
        insert InsertobjNewApplication;
        Account InsertobjNewAccount=new Account(Name = 'Test Account'); 
        insert InsertobjNewAccount;
        Address__c InsertobjNewAddress=new Address__c(); 
        insert InsertobjNewAddress;
        try{
            PublicApplicationPlacement.InsertUpdateAppPlacement(InsertobjNewApplication);
        } catch (Exception e) {}
        try{
            PublicApplicationPlacement.InsertUpdateAppPlacementAddr(InsertobjNewAddress);
        } catch (Exception e) {}
        try{
            PublicApplicationPlacement.getProgramDetails(InsertobjNewApplication.Id);
        } catch (Exception e) {}
        try{
            PublicApplicationPlacement.fetchPlacementInformation(InsertobjNewApplication.Id);
        } catch (Exception e) {}
        try{
            PublicApplicationPlacement.fetchAddressInformation(InsertobjNewApplication.Id,InsertobjNewAccount.Id);
        } catch (Exception e) {}
        try{
            List<DMLOperationsHandler.FetchValueWrapper> objApplicationPlacementPickList = PublicApplicationPlacement.getPickListValues(InsertobjNewApplication,'Type__c');
        } catch (Exception e) {}
    }

    @IsTest static void testpublicApplicationPlacementException(){
        try{
            PublicApplicationPlacement.getProgramDetails('');
        } catch (Exception e) {}
        try{
            PublicApplicationPlacement.fetchPlacementInformation('');
        } catch (Exception e) {}
        try{
            PublicApplicationPlacement.fetchAddressInformation('','');
        } catch (Exception e) {}
    }
}
