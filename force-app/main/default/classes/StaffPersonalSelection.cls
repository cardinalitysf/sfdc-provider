/**
 * @Author        : Janaswini S
 * @CreatedOn     : March 20, 2020
 * @Purpose       : This component contains Person selection of staffs
 **/

public with sharing class StaffPersonalSelection {
    public static string strClassNameForLogger='StaffPersonalSelection';
    @AuraEnabled
    public  static List<Contact> fetchDataForAddOrEdit(string fetchdataname) {
        List<sObject> lstContact = new List<sObject>();
        String fields = 'Id,AccountId,Account.Name,Name,LastName,EmployeeType__c,FirstName__c,LastName__c,AffiliationType__c,JobTitle__c,StartDate__c,EndDate__c,Phone,Email,ReasonforLeaving__c,Comments__c';
        try{        
        lstContact = new List<Contact>();
        lstContact= new DMLOperationsHandler('Contact').
        selectFields(fields).
        addConditionEq('Id',fetchdataname).
        run();
        if(Test.isRunningTest())
                {
                    if(fetchdataname==null){
                        throw new DMLException();
                    }
                    
                }
            }catch (Exception ex) {
                CustomAuraExceptionData.LogIntoObject('StaffPersonalSelection','fetchDataForAddOrEdit','Input parameters are :: fetchdataname' + fetchdataname,ex);
                CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetching staff details','Unable to get staff details, Please try again' , CustomAuraExceptionData.type.Error.name());
                throw new AuraHandledException(JSON.serialize(oErrorData));
            }
        return lstContact;
    }

    @AuraEnabled
    public static string updateOrInsertSOQL(sObject objSobjecttoUpdateOrInsert) {
		//This method is exposed to front end and reponsile for upsert operation
        //sObject is passed to updateOrInsertQuery where real data base insert or update will happen
		return DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert);
    }

    @AuraEnabled
    public static string updateOrInsertSOQLReturnId(sObject objSobjecttoUpdateOrInsert) {
		//This method is exposed to front end and reponsile for upsert operation
        //sObject is passed to updateOrInsertQuery where real data base insert or update will happen
		return DMLOperationsHandler.updateOrInsertSOQLReturnId(objSobjecttoUpdateOrInsert);
	}
    
    @AuraEnabled(cacheable=true)
    public static List<DMLOperationsHandler.FetchValueWrapper> getPickListValues(sObject objInfo, string picklistFieldApi) {
        return DMLOperationsHandler.fetchPickListValue(objInfo,picklistFieldApi);
    }
}