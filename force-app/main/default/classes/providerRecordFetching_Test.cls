/*
     Author         : G.sathishkumar
     @CreatedOn     : june 08 ,2020
     @Purpose       : test class for providerRecordFetching Details .
*/
@isTest
public class providerRecordFetching_Test {
    @isTest static void testProviderRecordFetching() {
       
        Account InsertobjNewAccount= new Account(Name = 'test Account'); 
        insert InsertobjNewAccount;

        Application__c InsertobjNewApplication=new Application__c(Status__c = 'Pending'); 
        insert InsertobjNewApplication;

        Contact InsertobjContact= new Contact(LastName = 'Name'); 
        insert InsertobjContact;


        providerRecordFetching.getRecordDetails( InsertobjNewAccount.Id,'staffTab');
        providerRecordFetching.getRecordDetails( InsertobjNewAccount.Id,'officeTab');
        providerRecordFetching.getRecordDetails(InsertobjContact.Id,'plantTab');
    }
}

