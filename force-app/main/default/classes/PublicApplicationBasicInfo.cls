public with sharing class PublicApplicationBasicInfo {
public static string strApexDebLogClassName = 'PublicApplicationBasicInfo';

    @AuraEnabled(cacheable=true)
    public static List<Application__c> getPublicApplicationBasicInfo(String applicationId) {
        List<Application__c> getPublicApplicationBasicInfoData = new List<Application__c>();

        try {
            getPublicApplicationBasicInfoData = new DMLOperationsHandler('Application__c').
            selectFields('Id, Program__c, ProgramType__c, AgeGroup__c, ReferralSource__c, PayeeLastName__c, PayeeFirstName__c, Name, MedicaidProvider__c, X1099Indicator__c, PlacementStructure__c').
            addConditionEq('Id', applicationId).
            run();

            if(Test.isRunningTest()) {
                    if(applicationId == '') {
                        throw new DMLException();
                    }
                }

        } catch(Exception e) {
            CustomAuraExceptionData.LogIntoObject(strApexDebLogClassName,'getPublicApplicationBasicInfo', 'applicationId: ' + applicationId, e);
            CustomAuraExceptionData errorData=new CustomAuraExceptionData('getPublicApplicationBasicInfo Data',e.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(errorData));
        }
        return getPublicApplicationBasicInfoData;

//       List<Application__c> applicationBasicInfoData = new  DMLOperationsHandler('Application__c').
//            selectFields('Id, Program__c, ProgramType__c, AgeGroup__c, ReferralSource__c, PayeeLastName__c, PayeeFirstName__c, Name, MedicaidProvider__c, X1099Indicator__c, PlacementStructure__c').
//            addConditionEq('Id',applicationId).
//            run();
//            
//            return applicationBasicInfoData;

    }

    @AuraEnabled(cacheable=true)
    public static List<ReferenceValue__c> getReferenceValueData() {

        List<ReferenceValue__c> referenceValueData = new DMLOperationsHandler('ReferenceValue__c').
            selectFields('Id, RefValue__c').
            addConditionEq('RefKey__c', 'Placement Structure').
            addConditionEq('RecordType.Name', 'Picklist').
            run();
            
            return referenceValueData;
    }
}