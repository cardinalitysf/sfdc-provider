/**
 * @Author        : Preethi Bojarajan
 * @CreatedOn     : May 05, 2020
 * @Purpose       : Test class for PublicApplicationReferenceChecks.cls
 **/

 public with sharing class PublicApplicationReferenceChecks {
    public static string strClassNameForLogger='PublicApplicationReferenceChecks';
    @AuraEnabled(cacheable=true)
        public static Id getRecordType(String name) {
            try {
                return CommonUtils.getRecordTypeIdbyName('ActorDetails__c', name);
            } catch (Exception ex) {
                //To Log into object
                CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getRecordType', 'Record Type Name' + name, ex);
                //To return back to the UI
                CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Record Type Fetch Issue,Please Try Again..!', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
                throw new AuraHandledException(JSON.serialize(oErrorData));
            }
    }
    
    @AuraEnabled(cacheable =true)
     public static Map<String, List<DMLOperationsHandler.FetchValueWrapper>> getMultiplePicklistValues(String objInfo, String picklistFieldApi) {
         return DMLOperationsHandler.fetchMultiplePickListValue(objInfo,picklistFieldApi);
    }

    @AuraEnabled(cacheable=true)
        public static List<SObject> getHouseHoldMembers(String provId) {
            List<sObject> compContObj = new List<sObject>();
            String fields ='Id, Contact__r.LastName';
           try {
                compContObj = new DMLOperationsHandler('Actor__c').
                                    selectFields(fields).
                                    addConditionEq('Provider__c', provId).
                                    run();
                                    if(Test.isRunningTest()) {
                                        if(provId == null){
                                            throw new DMLException();
                                        }
                                    }
                } catch (Exception Ex) {
                    CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getHouseHoldMembers',provId+ 'providerId' ,Ex);
                    CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Sorry,Failed to load Referral list details ,Please Try Again..!',Ex.getMessage() , CustomAuraExceptionData.type.error.name());
                    throw new AuraHandledException(JSON.serialize(oErrorData));
                } 
                return compContObj;
            }

    @AuraEnabled(cacheable=true)
        public static List<SObject> fetchRefCheckedMemDetails(String provId, String recId) {
            String fields ='Id, Actor__r.Contact__r.FirstName__c, Actor__r.Contact__r.LastName__c, Comments__c, Date__c, Relation__c, FirstName__c, LastName__c, ReferenceRecommends__c, TypeofContact__c, SchoolReferenceCheck__c, RelationshiptoApplicant__c';
            List<sObject> compRefCheckedObj = new List<sObject>();
            try{
                compRefCheckedObj = new DMLOperationsHandler('ActorDetails__c').
                                                selectFields(fields).
                                                addConditionEq('RecordTypeId', recId).
                                                addConditionEq('Provider__c', provId).
                                                run();
                                                if(Test.isRunningTest()) {
                                                    if(provId == null || recId == null){
                                                        throw new DMLException();
                                                    }
                                                }
            } catch (Exception Ex) {
                CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'fetchRefCheckedMemDetails',provId + 'providerId' + recId + 'recordTypeId', Ex);
                CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Sorry,Failed to load Referral list details ,Please Try Again..!',Ex.getMessage() , CustomAuraExceptionData.type.error.name());
                throw new AuraHandledException(JSON.serialize(oErrorData));
            }
            return compRefCheckedObj;
        }

    @AuraEnabled(cacheable=false)
        public static List<SObject> fetchAppRefCheckDetails(String actorDetId, String model) {
            List<sObject> fetchAppRefCheckDet = new List<sObject>();
            String fields ='Id, Actor__c, Comments__c, Date__c, Relation__c, FirstName__c, LastName__c, ReferenceRecommends__c, TypeofContact__c, SchoolReferenceCheck__c, RelationshiptoApplicant__c, Relation__c';
            String cond = 'Id';
            if(model == 'Address__c') {
                cond = 'ActorDetails__c';
                fields = 'Id, AddressLine1__c, AddressLine2__c, City__c, County__c, Phone__c, State__c, ZipCode__c, ActorDetails__r.Comments__c, ActorDetails__r.Date__c, ActorDetails__r.FirstName__c, ActorDetails__r.LastName__c, ActorDetails__r.ReferenceRecommends__c, ActorDetails__r.TypeofContact__c, ActorDetails__r.SchoolReferenceCheck__c, ActorDetails__r.RelationshiptoApplicant__c, ActorDetails__r.Relation__c';
            }
            try {
                fetchAppRefCheckDet = new DMLOperationsHandler(model).
                                            selectFields(fields).
                                            addConditionEq(cond, actorDetId).
                                            run();
                                            if(Test.isRunningTest()) {
                                                if(actorDetId == null || model == null){
                                                    throw new DMLException();
                                                }
                                            } 
            } catch (Exception Ex) {
                CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'fetchAppRefCheckDetails',actorDetId+ 'actorId' + model + 'object', Ex);
                CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Sorry,Failed to load Referral list details ,Please Try Again..!',Ex.getMessage() , CustomAuraExceptionData.type.error.name());
                throw new AuraHandledException(JSON.serialize(oErrorData));
            }
            return fetchAppRefCheckDet;
        }

    @AuraEnabled(cacheable=true)
    public static List<SObject> getStateDetails() {
        List<SObject> getStateDetails = new List<SObject>();
        try {
            getStateDetails = CommonUtils.getStateDetails();
              if(Test.isRunningTest()) {
                  integer intTest =1/0;
                }
        } catch (Exception ex) {
            //To Log into object
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getStateDetails', 'State List Types', ex);
            //To return back to the UI
            CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('State Details Fetch Issue,Please Try Again..!', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        } 
        return getStateDetails;        
    }
}