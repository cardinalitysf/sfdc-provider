/**
 * @Author        : Sindhu Venkateswarlu
 * @CreatedOn     : May 12,2020
 * @Purpose       : Test Methods for ProviderDashboard class
 **/

@isTest
private class providersDashboard_Test {
 
    @isTest static void testProvidersDashboardPositive(){
        Account InsertobjNewAccount=new Account(Name='Test Account',Status__c = 'Active'); 
        insert InsertobjNewAccount;
        Monitoring__c InsertobjNewMonitoring = new Monitoring__c(MonitoringStatus__c='Submitted');
        insert InsertobjNewMonitoring;
        Contract__c InsertobjNewContract = new Contract__c(ContractStatus__c='Submitted');
        insert InsertobjNewContract;
        providersDashboard.getProvidersCount(InsertobjNewAccount.Id,'ProviderId__c');
        providersDashboard.getProvidersSupCount('searchKey','ProviderId__c','Monitoring__c');
        providersDashboard.getProvidersSupCount('searchKey','Name__c','Monitoring__c');
        providersDashboard.getProvidersSupCount('searchKey','BillingCountry','Monitoring__c');
        providersDashboard.getProvidersSupCount('searchKey','ProviderId__c','Contract__c');
        providersDashboard.getProvidersSupCount('searchKey','Name__c','Contract__c');
        providersDashboard.getProvidersSupCount('searchKey','BillingCountry','Contract__c');
        providersDashboard.getProvidersList('searchKey','Status__c','Name__c');
        providersDashboard.getMonitoringList('searchKey','Completed','Name__c');
        providersDashboard.getContractList('searchKey','Completed','Name');
    }
}