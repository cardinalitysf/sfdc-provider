  /**
	 * @Author        : Balamurugan B
	 * @CreatedOn     : June 29 ,2020
	 * @Purpose       : Test Class for ReconsiderationTriggerHandler.cls
**/
  
@isTest
private class reconsiderationTriggerHandler_Test{
  @testSetup
  static void setupTestData(){
    test.startTest();
    //Reconsideration__c reconsider_Obj = new Reconsideration__c(Narrative__c='Narrative', Status__c = 'Active', SubmittedDate__c = Date.today());
    
      Account account_Obj = new Account(Name = 'Test',Status__c='Active');
      //insert account_Obj;
      Database.UpsertResult Upsertresults1 = Database.upsert(account_Obj);
        id strProviderId= Upsertresults1.getId();
      
     List<Reconsideration__c> reconsider_Obj = new List<Reconsideration__c>{
    new Reconsideration__c(Provider__c=strProviderId,Narrative__c='Narrative', Status__c = 'Active', SubmittedDate__c = Date.today()),
    new Reconsideration__c(Provider__c=strProviderId,Narrative__c='Narrative', Status__c = 'Active', SubmittedDate__c = Date.today()),
    new Reconsideration__c(Provider__c=strProviderId, Narrative__c='Narrative', Status__c = 'Active', SubmittedDate__c = Date.today())
      };  
   
          
     Insert reconsider_Obj;
    HistoryTracking__c historytracking_Obj = new HistoryTracking__c(CreatedDate__c = Date.today(), FieldName__c = '14', NewValue__c = '15', ObjectName__c = 'Reconsideration__c', ObjectRecordId__c = '17', RichOldValue__c = '18', RichNewValue__c = '19');
    Insert historytracking_Obj;
    test.stopTest();
  }
    
    

  static testMethod void test_afterupdate_UseCase1(){
 List<Reconsideration__c> reconsider_Obj  =  [SELECT Id,Narrative__c,Status__c from Reconsideration__c];
 List<Reconsideration__c> reconsider_Obj1  =  [SELECT Id,Narrative__c,Status__c from Reconsideration__c];
 System.assertEquals(true,reconsider_Obj.size()>0);

 Map<Id, Reconsideration__c> newReconsiderMap = new Map<Id, Reconsideration__c>();   
 Map<Id, Reconsideration__c> oldReconsiderMap = new Map<Id, Reconsideration__c>();   
 
 newReconsiderMap.putAll(reconsider_Obj);
 oldReconsiderMap.putAll(reconsider_Obj1);
 for(id oid : newReconsiderMap.keySet()){
          oldReconsiderMap.get(oid).Narrative__c='Changed Narrative';
      }     
    reconsiderationTriggerHandler obj01 = new reconsiderationTriggerHandler();
    reconsiderationTriggerHandler.afterupdate(newReconsiderMap,oldReconsiderMap);
     

  }
  static testMethod void test_afterinsert_UseCase1(){
    List<Reconsideration__c> reconsider_Obj  =  [SELECT Id,Narrative__c,Status__c from Reconsideration__c];
    System.assertEquals(true,reconsider_Obj.size()>0);
    reconsiderationTriggerHandler obj01 = new reconsiderationTriggerHandler();
    reconsiderationTriggerHandler.afterinsert(reconsider_Obj);
     
     List<HistoryTracking__c> historytracking_Obj  =  [SELECT Id,CreatedDate__c,FieldName__c,NewValue__c,ObjectName__c,ObjectRecordId__c,RichOldValue__c,RichNewValue__c from HistoryTracking__c];
    System.assertEquals(true,historytracking_Obj.size()>0);
    reconsiderationTriggerHandler obj02 = new reconsiderationTriggerHandler();
    reconsiderationTriggerHandler.afterinsert(new List<Reconsideration__c>());
  }
}