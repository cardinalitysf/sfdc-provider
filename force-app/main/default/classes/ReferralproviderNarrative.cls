/**
 * @Author        : Sindhu Venkateswarlu
 * @CreatedOn     : Feb 27,2020
 * @Purpose       :Upsert and Fetching Narrative Details
 **/


public with sharing class ReferralproviderNarrative {
    public static string strClassNameForLogger='ReferralproviderNarrative';


    //This Method is used to get Narrative details.
    @AuraEnabled
    public static List<sObject> fetchDataForNarrative(String fetchdatanamenew) {
        List<sObject> CaseObj= new List<sObject>();
        Set<String> queryFields = new Set<String>{'Id, Description'};
        try {
            CaseObj = new DMLOperationsHandler('Case').
        selectFields(queryFields).
        addConditionEq('Id', 'fetchdatanamenew').
        run();
        if(Test.isRunningTest())
        {
            if(fetchdatanamenew==null){
                throw new DMLException();
            }
            
        }
        } catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'fetchDataForNarrative',fetchdatanamenew,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetching  record',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return CaseObj;
    }
   
      //This Method is used to Insert/Update.
    @AuraEnabled
    public static string updatenarrativedetails(sObject objSobjecttoUpdateOrInsert){
        return JSON.serialize(DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert));
    }


    
    //This Method is used to get Status details.
    @AuraEnabled(cacheable=true)
    public static List<sObject> getReferralProviderStatus(String caseId) {
        List<sObject> CaseStatusObj= new List<sObject>();
        Set<String> queryFields = new Set<String>{'Id, Status'};
        try {
            CaseStatusObj = new DMLOperationsHandler('Case').
        selectFields(queryFields).
        addConditionEq('Id', 'caseId').
        run();
        if(Test.isRunningTest())
        {
            if(caseId==null){
                throw new DMLException();
            }
            
        }
        } catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getReferralProviderStatus',caseId,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in fetching  record',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return CaseStatusObj;
    }
   
}