/**
     * @Author        : Vijayaraj M
     * @CreatedOn     : May 11,2020
     * @Purpose       :Test Methods to unit test for ProviderBasicContractInfo.cls
     **/
    @isTest
private with sharing class ProviderBasicContractInfo_Test {
    @isTest static void testProviderBasicContractInfoTest() {
        Contract__c InsertobjNewContract=new Contract__c(Status__c = 'XYZ'); 
        insert InsertobjNewContract;
        IRCRates__c InsertobjNewIRCRates=new IRCRates__c(RateType__c = 'PQR'); 
        insert InsertobjNewIRCRates;
        License__c InsertobjNewLicense=new License__c(ProgramName__c  = 'PQR'); 
        insert InsertobjNewLicense;
        ReferenceValue__c InsertobjNewReferenceValue=new ReferenceValue__c(Description__c  = 'PQR'); 
        insert InsertobjNewReferenceValue;
        Address__c InsertobjNewAddress=new Address__c(AddressLine1__c  = 'PQR'); 
        insert InsertobjNewAddress;

        try {
             ProviderBasicContractInfo.getProviderContractInfoData('');
        } catch (Exception e) {

        }
        ProviderBasicContractInfo.getProviderContractInfoData(InsertobjNewContract.Id);

        try {
            ProviderBasicContractInfo.getIrcBedCapacity('');
        } catch(Exception e) {}
        ProviderBasicContractInfo.getIrcBedCapacity(InsertobjNewIRCRates.LicenseType__c);

        try {
            ProviderBasicContractInfo.getApplicationCapacity('');
        } catch(Exception e) {}
        ProviderBasicContractInfo.getApplicationCapacity(InsertobjNewLicense.Id);

        try {
            ProviderBasicContractInfo.getChildCharacteristicsData('');
        } catch(Exception e) {}
        ProviderBasicContractInfo.getChildCharacteristicsData(InsertobjNewReferenceValue.RefKey__c);

         try {
            ProviderBasicContractInfo.getAddressInfoData('');
        } catch(Exception e) {}
        ProviderBasicContractInfo.getAddressInfoData(InsertobjNewAddress.Application__c);

        ProviderBasicContractInfo.getChildCharacteristicsFromContract(InsertobjNewContract.Id);
        ProviderBasicContractInfo.insertOrUpdateChildCharacteristicsData('KKK',InsertobjNewContract.Id);
        ProviderBasicContractInfo.deleteChildCharacteristics('KKK',InsertobjNewContract.Id);

        

    }
}