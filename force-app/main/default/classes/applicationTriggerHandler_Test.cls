/**
 * @Author        : Sindhu V
 * @CreatedOn     : June 10 ,2020
 * @Purpose       : Test Methods to unit test applicationTriggerHandler  class
 **/
@IsTest
public class applicationTriggerHandler_Test {
@IsTest
  static void setupTestData(){
      Account acct = new Account(Name = 'Cardinality-CW-');
      insert acct;
      Id MyId2=Schema.Sobjecttype.Application__c.getRecordTypeInfosByName().get('Public').getRecordTypeId();
      Application__c app = new Application__c(ApprovalComments__c = 'test Comments',Narrative__c='text',Status__c='Pending',Provider__c=acct.Id,RecordTypeId=MyId2);
      insert app;
      app.Narrative__c='narrative';
      app.Status__c='APPROVED';
      update app;   
    }  
    @IsTest
    static void setupTestData1(){
        Account acct = new Account(Name = 'Cardinality-CW-');
        insert acct;
        Id MyId2=Schema.Sobjecttype.Application__c.getRecordTypeInfosByName().get('Private').getRecordTypeId();
        Application__c app = new Application__c(ApprovalComments__c = 'test Comments',Narrative__c='text',Status__c='Pending',Provider__c=acct.Id,RecordTypeId=MyId2);
        insert app;
        app.Narrative__c='narrative';
        app.Status__c='APPROVED';
        update app;   
      }  
      @IsTest
    static void setupTestData2(){
        Account acct = new Account(Name = 'Cardinality-CW-');
        insert acct;
        Id MyId2=Schema.Sobjecttype.Application__c.getRecordTypeInfosByName().get('Private').getRecordTypeId();
        Application__c app = new Application__c(LicenseEnddate__c=Date.today().addDays(-1),ApprovalComments__c = 'test Comments',Narrative__c='text',Status__c='Pending',Provider__c=acct.Id,RecordTypeId=MyId2);
        insert app;
        app.Narrative__c='narrative';
        app.Status__c='APPROVED';
        update app;   
      }
}