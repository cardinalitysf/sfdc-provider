/*
     Author         : Sindhu
     @CreatedOn     : june 26 ,2020
     @Purpose       : test class for PublicProvidersReconsiderationDecision.cls .
*/

@isTest
public with sharing class PublicProvidersReconsiDecision_Test {
    @isTest static void  testgetDecisionDetails() {
        Reconsideration__c recon = new Reconsideration__c();
        insert recon;
         Profile prf = [Select Id, Name from Profile where Name= 'Supervisor'];
        List<User> usrList = TestDataFactory.createTestUsers( 2, prf.Id,true );
        List<Account> lstAcct= TestDataFactory.testAccountData();
        List<Account> accList= new List<Account>();
        for(Account acct : lstAcct)
        {
           acct.Narrative__c = 'New Comments';
           accList.add(acct);
        }
        insert accList;
        update accList;
        List<Application__c> appList = TestDataFactory.TestApplicationData(2,'Rejected');
        insert appList;
        update appList;
        List<Monitoring__c> monList = TestDataFactory.createTestMonitoringData(5,true);
        update monList;
        List<ContentVersion> cvList = TestDataFactory.createTestContentVersionData();
        insert cvList;
        Set<ContentVersion> cvrset = new Set<ContentVersion>();
        for(ContentVersion cv:cvList)
        {
            cvrset.add(cv);
        }
        PublicProvidersReconsiderationDecision.getDecisionDetails('reconsiderationId');
        PublicProvidersReconsiderationDecision.getAllSuperUsersList();
        PublicProvidersReconsiderationDecision.saveSign('test',recon.Id);
        PublicProvidersReconsiderationDecision.getAssignedSuperUserTableDetails(recon.Id);

        try{
                 // Testing getPickListValues method getPickListValues
                 List<DMLOperationsHandler.FetchValueWrapper>  objgetPickListValues = PublicProvidersReconsiderationDecision.getRecommendationPickListValues(recon,'XXX');
               }
            	catch(Exception e){
                				} 
        PublicProvidersReconsiderationDecision.processingWrapper pWrap = new PublicProvidersReconsiderationDecision.processingWrapper(recon, cvrset);
        PublicProvidersReconsiderationDecision.processingWrapperString pWrapStr = new PublicProvidersReconsiderationDecision.processingWrapperString(recon, '');

    }
   @IsTest static void testupdateapprovaldetails ()
    {
        Profile prf = [Select Id, Name from Profile where Name= 'Supervisor'];
        List<User> userList =  TestDataFactory.createTestUsers(1, prf.Id, true);
       

        Reconsideration__c InsertobjPublicApplicationDecisionPeriod=new Reconsideration__c(ReconsiderationDecision__c='Approve',Caseworker__c=userList[0].id,Status__c='Submitted');
        insert InsertobjPublicApplicationDecisionPeriod;
        PublicProvidersReconsiderationDecision.getAssignedSuperUserTableDetails(InsertobjPublicApplicationDecisionPeriod.Id);

        PublicProvidersReconsiderationDecision.updateapprovaldetails(InsertobjPublicApplicationDecisionPeriod,userList[0].id,InsertobjPublicApplicationDecisionPeriod.Id);

    
    }

  
     @IsTest static void testupdateapprovaldetails1 ()
    {
        Profile prf = [Select Id, Name from Profile where Name= 'Supervisor'];
        List<User> userList =  TestDataFactory.createTestUsers(1, prf.Id, true);

        Reconsideration__c InsertobjPublicApplicationDecisionPeriod=new Reconsideration__c(ReconsiderationDecision__c='Approve',Caseworker__c=userList[0].id,Status__c='Submitted');
        insert InsertobjPublicApplicationDecisionPeriod;

        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments('Submitting request for approval.');
        req.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        req.setObjectId(InsertobjPublicApplicationDecisionPeriod.Id);
        Approval.ProcessResult resu = Approval.process(req);
        System.assert(resu.isSuccess());
        System.assertEquals('Pending', resu.getInstanceStatus(),'Instance Status'+resu.getInstanceStatus());
        List<Id> newWorkItemIds = resu.getNewWorkitemIds();
        PublicProvidersReconsiderationDecision.updateapprovaldetails(InsertobjPublicApplicationDecisionPeriod,'',newWorkItemIds.get(0));
         PublicProvidersReconsiderationDecision.getAssignedSuperUserTableDetails(InsertobjPublicApplicationDecisionPeriod.Id);

    }
     @IsTest static void testupdateapprovaldetails2 ()
    {
        Profile prf = [Select Id, Name from Profile where Name= 'Supervisor'];
        List<User> userList =  TestDataFactory.createTestUsers(1, prf.Id, true);

        Reconsideration__c InsertobjPublicApplicationDecisionPeriod=new Reconsideration__c(ReconsiderationDecision__c='Returned',Caseworker__c=userList[0].id,Status__c='Submitted');
        insert InsertobjPublicApplicationDecisionPeriod;

        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments('Submitting request for approval.');
        req.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        req.setObjectId(InsertobjPublicApplicationDecisionPeriod.Id);
        Approval.ProcessResult resu = Approval.process(req);
        System.assert(resu.isSuccess());
        System.assertEquals('Pending', resu.getInstanceStatus(),'Instance Status'+resu.getInstanceStatus());
        List<Id> newWorkItemIds = resu.getNewWorkitemIds();
        PublicProvidersReconsiderationDecision.updateapprovaldetails(InsertobjPublicApplicationDecisionPeriod,'',newWorkItemIds.get(0));
         PublicProvidersReconsiderationDecision.getAssignedSuperUserTableDetails(InsertobjPublicApplicationDecisionPeriod.Id);

    }
    @IsTest static void testupdateapprovaldetails3 ()
    {
        Profile prf = [Select Id, Name from Profile where Name= 'Supervisor'];
        List<User> userList =  TestDataFactory.createTestUsers(1, prf.Id, true);

        Reconsideration__c InsertobjPublicApplicationDecisionPeriod=new Reconsideration__c(ReconsiderationDecision__c='Approved',Caseworker__c=userList[0].id,Status__c='Submitted');
        insert InsertobjPublicApplicationDecisionPeriod;

        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments('Submitting request for approval.');
        req.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        req.setObjectId(InsertobjPublicApplicationDecisionPeriod.Id);
        Approval.ProcessResult resu = Approval.process(req);
        System.assert(resu.isSuccess());
        System.assertEquals('Pending', resu.getInstanceStatus(),'Instance Status'+resu.getInstanceStatus());
        List<Id> newWorkItemIds = resu.getNewWorkitemIds();
        PublicProvidersReconsiderationDecision.updateapprovaldetails(InsertobjPublicApplicationDecisionPeriod,'',newWorkItemIds.get(0));
         PublicProvidersReconsiderationDecision.getAssignedSuperUserTableDetails(InsertobjPublicApplicationDecisionPeriod.Id);

    }

   
   
}