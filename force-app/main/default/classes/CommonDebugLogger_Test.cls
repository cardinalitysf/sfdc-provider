@isTest
public class CommonDebugLogger_Test {
    @isTest static void testCommonDebugLogger(){


        String strApexClassName = 'testClass';
        String strApexMethodName = 'testMethod';
        String StrRecordData = 'testRecord';
        String Msg = 'Error';

        final String originalMessage = 'Hello World!';
        DmlException Ex = new DmlException();
        Ex.setMessage(originalMessage);

        Test.StartTest();
        new CommonDebugLogger().createLog(
            new CommonDebugLogger.Information(
                strApexClassName,
                strApexMethodName,
                StrRecordData,
                Msg
            )
        );
        new CommonDebugLogger().createLog(
            new CommonDebugLogger.Error(
                strApexClassName,
                strApexMethodName,
                StrRecordData,
                Ex
            )
        );
        Test.StopTest();
        
    }
}        

