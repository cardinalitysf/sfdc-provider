public class ProviderBasicContractInfo {
    public static string strApexDebLogClassName = 'ProviderBasicContractInfo';

    @AuraEnabled(cacheable=true)
    public static List<Contract__c> getProviderContractInfoData(String contractId) {
        List<Contract__c> getProviderContractInfo = new List<Contract__c>();

        try {
            getProviderContractInfo = new DMLOperationsHandler('Contract__c').
            selectFields('FiscalDate__c, Status__c, License__r.EndDate__c, License__r.Name, License__r.StartDate__c, License__r.ProgramType__c, ContractEndDate__c, ContractStartDate__c, ContractTermType__c, TotalContractBed__c, ContractType__c, ContractStatus__c, License__c, Name, Id').
            addConditionEq('Id', contractId).
            run();

            if(Test.isRunningTest()) {
                if(contractId == ''){
                    throw new DMLException();
                }
            }

        } catch(Exception e) {
            CustomAuraExceptionData.LogIntoObject(strApexDebLogClassName,'getProviderContractInfoData', 'contractId: ' + contractId, e);
            CustomAuraExceptionData errorData=new CustomAuraExceptionData('Sanctions Data',e.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(errorData));
        }
        
        return getProviderContractInfo;

//        String model = 'Contract__c';
//        String fields = 'FiscalDate__c, Status__c, License__r.EndDate__c, License__r.Name, License__r.StartDate__c, License__r.ProgramType__c, ContractEndDate__c, ContractStartDate__c, ContractTermType__c, TotalContractBed__c, ContractType__c, ContractStatus__c, License__c, Name, Id';
//        String cond = 'Id = \'' + contractId + '\'';
//
//        return DMLOperationsHandler.selectSOQLWithConditionParameters(model, fields, cond);
    }

    @AuraEnabled(cacheable=true)
    public static List<IRCRates__c> getIrcBedCapacity(String licenseId) {
        List<IRCRates__c> getIrcBedCapacityData = new List<IRCRates__c>();

        try {
            getIrcBedCapacityData = new DMLOperationsHandler('IRCRates__c').
            selectFields('Id, IRCBedCapacity__c').
            addConditionEq('LicenseType__c', licenseId).
            addConditionEq('Status__c', 'Active').
            run();

            if(Test.isRunningTest()) {
                if(licenseId == '') {
                    throw new DMLException();
                }
            }

        } catch(Exception e) {
            CustomAuraExceptionData.LogIntoObject(strApexDebLogClassName,'getIrcBedCapacity', 'licenseId: ' + licenseId, e);
            CustomAuraExceptionData errorData=new CustomAuraExceptionData('Sanctions Data',e.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(errorData));
        }

        return getIrcBedCapacityData;

//        String model = 'IRCRates__c';
//        String fields = 'Id, IRCBedCapacity__c';
//        String cond = 'LicenseType__c =\''+ licenseId +'\' And Status__c=\'Active\'';
//        return DMLOperationsHandler.selectSOQLWithConditionParameters(model,fields,cond);

    }


    @AuraEnabled(cacheable=true)
    public static List<License__c> getApplicationCapacity(String licenseId) {
            List<License__c> getApplicationCapacityData = new List<License__c>();

            try {
                getApplicationCapacityData = new DMLOperationsHandler('License__c').
                selectFields('Id, Application__r.Capacity__c, Application__c').
                addConditionEq('Id', licenseId).
                run();

                if(Test.isRunningTest()) {
                    if(licenseId == '') {
                        throw new DMLException();
                    }
                }

        } catch(Exception e) {
            CustomAuraExceptionData.LogIntoObject(strApexDebLogClassName,'getApplicationCapacity', 'licenseId: ' + licenseId, e);
            CustomAuraExceptionData errorData=new CustomAuraExceptionData('getApplicationCapacity Data',e.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(errorData));
        }

        return getApplicationCapacityData;


//        String model = 'License__c';
//        String fields = 'Id, Application__r.Capacity__c, Application__c';
//        String cond = 'Id = \'' + licenseId + '\'';
//
//        return DMLOperationsHandler.selectSOQLWithConditionParameters(model, fields, cond);

    }

    @AuraEnabled(cacheable=true)
    public static List<ReferenceValue__c> getChildCharacteristicsData(String contractId) {
           List<ReferenceValue__c> getChildCharacteristics = new List<ReferenceValue__c>();

           try {
                getChildCharacteristics = new DMLOperationsHandler('ReferenceValue__c').
                selectFields('Id,RefKey__c, Activity__c').
                addConditionEq('RefKey__c', 'Child Characteristics').
                run();

                if(Test.isRunningTest()) {
                    if(contractId == '') {
                        throw new DMLException();
                    }
                }

           } catch(Exception e) {
                CustomAuraExceptionData.LogIntoObject(strApexDebLogClassName,'getChildCharacteristicsData', 'contractId: ' + contractId, e);
                CustomAuraExceptionData errorData=new CustomAuraExceptionData('getChildCharacteristics Data',e.getMessage() , CustomAuraExceptionData.type.Informational.name());
                throw new AuraHandledException(JSON.serialize(errorData));
           }

           return getChildCharacteristics;

//        String model = 'ReferenceValue__c';
//        String fields = 'Id,RefKey__c, Activity__c';
//        String cond = 'RefKey__c = \'' + 'Child Characteristics' + '\'';
//
//        return DMLOperationsHandler.selectSOQLWithConditionParameters(model, fields, cond);


    }

    @AuraEnabled(cacheable=true)
    public static List<Address__c> getAddressInfoData(String applicationId) {
        List<Address__c> getAddressInfo = new List<Address__c>();

           try {
                getAddressInfo = new DMLOperationsHandler('Address__c').
                selectFields('Name, AddressLine1__c, Email__c, Phone__c, Id').
                addConditionEq('Application__c', applicationId).
                run();

                if(Test.isRunningTest()) {
                    if(applicationId == '') {
                        throw new DMLException();
                    }
                }

           } catch(Exception e) {
                CustomAuraExceptionData.LogIntoObject(strApexDebLogClassName,'getAddressInfoData', 'applicationId: ' + applicationId, e);
                CustomAuraExceptionData errorData=new CustomAuraExceptionData('getAddressInfoData-Data',e.getMessage() , CustomAuraExceptionData.type.Informational.name());
                throw new AuraHandledException(JSON.serialize(errorData));
           }

           return getAddressInfo;

//        String model = 'Address__c';
//        String fields =
//                'Name, AddressLine1__c, Email__c, Phone__c, Id';
//        String cond = 'Application__c  = \'' + applicationId + '\'';
//
//        return DMLOperationsHandler.selectSOQLWithConditionParameters(model, fields, cond);

    }

    @AuraEnabled(cacheable=true)
    public static List<Contract__c> getChildCharacteristicsFromContract(string contractId) {
        String model = 'Contract__c';
        String fields = 'Id, ChildCharacteristics__c';
        String cond = 'Id = \'' + contractId + '\'';
        return DMLOperationsHandler.selectSOQLWithConditionParameters(model, fields, cond);

    }

    @AuraEnabled
    public static void insertOrUpdateChildCharacteristicsData(string selectedChildCharac, string contractId) {
        List<Contract__c> taskToInsert = new List<Contract__c>();
        Contract__c objTask = new Contract__c();
        objTask.ChildCharacteristics__c = selectedChildCharac;
        objTask.Id = contractId;
        taskToInsert.add(objTask);
        upsert taskToInsert;

    }

    @AuraEnabled
    public static List<String> deleteChildCharacteristics(String childCharacter, String contractId) {

        List<Contract__c> objs = new List<Contract__c>();
        string contId = '%' + contractId + '%';
        objs = [SELECT ChildCharacteristics__c FROM Contract__c WHERE Id =: contractId ];
        List<String> newChildCharacteristics;
        for (Contract__c obj : objs) {
            List<String> splitStrings = obj.ChildCharacteristics__c.split(',');
            Integer i = 0;

            if (splitStrings.contains(',')) {
                
            }
            if(splitStrings.size() > 1) {
                for (i = 0; i < splitStrings.size(); i++) {
                    if(splitStrings.get(i) == childCharacter) {
                        splitStrings.remove(i);
                    }
                }
                newChildCharacteristics = splitStrings;
                return newChildCharacteristics;
            }
        }
        return newChildCharacteristics;
    }
}