/**
 * @Author        : Vijayaraj M
 * @CreatedOn     : June 09 , 2020
 * @Purpose       : This component contains providerStaffCertificationContr test class Creation
 **/

 @isTest
private with sharing class ProviderStaffCertificationContr_Test {
    @isTest static void testProviderStaffCertificationContrTest() {
        Certification__c InsertobjNewCertification=new Certification__c(CertificationType__c='General'); 
        insert InsertobjNewCertification;
        
        try {
            ProviderStaffCertificationController.getCertificationDetails('');
        } catch (Exception Ex) {
            
        }
        ProviderStaffCertificationController.getCertificationDetails(InsertobjNewCertification.Staff__c);
        try {
            ProviderStaffCertificationController.editCertificationDetails('');
        } catch (Exception Ex) {
            
        }
        ProviderStaffCertificationController.editCertificationDetails(InsertobjNewCertification.Id);
        ProviderStaffCertificationController.deleteCertificationDetails(InsertobjNewCertification.Id);
        try{
            ProviderStaffCertificationController.InsertUpdateStaffCertificate(InsertobjNewCertification);
        }catch(Exception e)
        {

        }
        

    }
}
