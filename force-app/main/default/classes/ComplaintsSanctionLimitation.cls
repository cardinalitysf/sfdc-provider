/**
 * @Author        : Sundar Karuppalagu
 * @CreatedOn     : JULY 23 ,2020
 * @Purpose       : Sanctions Limitations Screen
 * @Updated By    : Nandhini
 * @Updated By    : Sundar K -> July 24, 2020 -> added getComplaintLicenseDetails method for fetching license dates.
 **/

public with sharing class ComplaintsSanctionLimitation {
    public static string strClassNameForLogger = 'ComplaintsSanctionLimitation';

    @AuraEnabled
    public static List<sObject> getSelectedSanctionData(String sanctionId) {
        List<sObject> limitationObj = new List<sObject>();
        String queryFields = 'Id, Numberofyouthserved__c, YouthEffectiveDate__c, YouthEndDate__c, YouthLimitationReason__c, MinAge__c, MaxAge__c, Gender__c, PopulationLimitationReason__c, PopulationEffectiveDate__c, PopulationEndDate__c, ProviderAssignedService__c, ProgramEffectiveDate__c, ProgramEndDate__c, ProgramLimitationReason__c, Document__c, Delivery__c';

        try {
            limitationObj = new DMLOperationsHandler('Sanctions__c').
                            selectFields(queryFields).
                            addConditionEq('Id', sanctionId).
                            addConditionEq('RecordType.Name', 'Limitation').
                            run();
                            if(Test.isRunningTest())
                            {
                                if(sanctionId==null){
                                    throw new DMLException();
                                }
                            }
        } catch (Exception Ex) {
            //To Log into object
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getSelectedSanctionData', 'Input Parameter Limitation ID is :: ' + sanctionId, Ex);

            //To return back to the UI
            CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Limitation data failed to load', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }

        return limitationObj;
    }

    @AuraEnabled(cacheable=false)
    public static Id getRecordType(String name) {
        return CommonUtils.getRecordTypeIdbyName('Sanctions__c', name);
    }

    //Added this method for fetching Complaint License Details
    @AuraEnabled
    public static List<sObject> getComplaintLicenseDetails(String complaintId) {
        List<sObject> complaintLicenseObj = new List<sObject>();
        String queryFields = 'Id, CaseNumber, License__c, License__r.StartDate__c, License__r.EndDate__c';

        try {
            complaintLicenseObj = new DMLOperationsHandler('Case').
                                    selectFields(queryFields).
                                    addConditionEq('Id', complaintId).
                                    addConditionEq('RecordType.Name', 'Complaints').
                                    run();
                                    if (Test.isRunningTest())
                                    {
                                        if (complaintId == null){
                                            throw new DMLException();
                                        }
                                    }
        } catch (Exception Ex) {
            //To Log into object
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getComplaintLicenseDetails', 'Input Parameter Complaint ID is :: ' + complaintId, Ex);

            //To return back to the UI
            CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Complaint License Details failed to load', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return complaintLicenseObj;
    }
}
