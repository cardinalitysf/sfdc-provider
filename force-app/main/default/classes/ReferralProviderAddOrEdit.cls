/**
 * @Author        : Janaswini S
 * @CreatedOn     :
 * @Purpose       : This component contains ReferralProvider's Add Or Edit details
 * @Updated on    :June 20, 2020
 * @Updated by    :Janaswini Unit Test Updated methods with try catch
 **/
public with sharing class ReferralProviderAddOrEdit {
  public static string strClassNameForLogger = 'ReferralProviderAddOrEdit';
  @AuraEnabled(cacheable=true)
  public static List<SObject> getDataByCaseOrProvider(
    string caseorProviderID,
    string prgtype
  ) {
    List<sObject> lstMonitoring = new List<sObject>();
    String fields = 'Id, AccountId,Account.Name,Account.ProviderType__c,Account.ParentCorporation__c,Account.Corporation__c,Account.TaxId__c,Account.RFP__c,Account.FEINTaxID__c,Account.FirstName__c,Account.SON__c,Account.LastName__c,Account.BillingStreet,Account.Email__c,Account.ShippingStreet,Account.Phone,Account.BillingState,Account.CellNumber__c,Account.BillingCity,Account.Fax,Account.BillingCountry,Account.Accrediation__c,Account.BillingPostalCode,Account.Profit__c,Origin,Status,Priority,Program__c,ProgramType__c';
    try {
      if (prgtype == 'case') {
        lstMonitoring = new DMLOperationsHandler('Case')
          .selectFields(fields)
          .addConditionEq('Id', caseorProviderID)
          .run();
      } else {
        lstMonitoring = new DMLOperationsHandler('Case')
          .selectFields(fields)
          .addConditionEq('AccountId', caseorProviderID)
          .run();
      }
      if (Test.isRunningTest()) {
        if (caseorProviderID == null || prgtype == null) {
          throw new DMLException();
        }
      }
    } catch (Exception ex) {
      CustomAuraExceptionData.LogIntoObject(
        strClassNameForLogger,
        'getDataByCaseOrProvider',
        'Input parameters are :: caseorProviderID' +
        caseorProviderID +
        'prgtype' +
        prgtype,
        ex
      );
      CustomAuraExceptionData oErrorData = new CustomAuraExceptionData(
        'Error in fetching status',
        'Unable to get status details, Please try again',
        CustomAuraExceptionData.type.Error.name()
      );
      throw new AuraHandledException(JSON.serialize(oErrorData));
    }

    return lstMonitoring;
  }

  @AuraEnabled(cacheable=true)
  public static List<DMLOperationsHandler.FetchValueWrapper> getPickListValues(
    sObject objInfo,
    string picklistFieldApi
  ) {
    return DMLOperationsHandler.fetchPickListValue(objInfo, picklistFieldApi);
  }

  @AuraEnabled
  public static string updateOrInsertSOQL(sObject objSobjecttoUpdateOrInsert) {
    //This method is exposed to front end and reponsile for upsert operation
    //sObject is passed to updateOrInsertQuery where real data base insert or update will happen
    return DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert);
  }

  @AuraEnabled
  public static string updateOrInsertSOQLReturnId(
    sObject objSobjecttoUpdateOrInsert
  ) {
    //This method is exposed to front end and reponsile for upsert operation
    //sObject is passed to updateOrInsertQuery where real data base insert or update will happen
    return DMLOperationsHandler.updateOrInsertSOQLReturnId(
      objSobjecttoUpdateOrInsert
    );
  }

  //Modified by Sundar K
  @AuraEnabled(cacheable=true)
  public static List<SObject> getStateDetails() {
    String model = 'ReferenceValue__c';
    String fields = 'Id, RefValue__c';
    String cond = 'Domain__c = \'State\'';

    return DMLOperationsHandler.selectSOQLWithConditionParameters(
      model,
      fields,
      cond
    );
  }

  @AuraEnabled(cacheable=true)
  public static List<SObject> getAccreditationValues() {
    Set<String> fields = new Set<String>{ 'RefKey__c, RefValue__c' };
    List<ReferenceValue__c> OBJ = new List<ReferenceValue__c>();
    OBJ = new DMLOperationsHandler('ReferenceValue__c')
      .selectFields(fields)
      .addConditionEq('Domain__c', 'Accreditation')
      .run();
    return OBJ;
  }

  @AuraEnabled(cacheable=true)
  public static List<SObject> getProgramTypeOptions() {
    Set<String> fields = new Set<String>{ 'RefKey__c, RefValue__c' };
    List<ReferenceValue__c> OBJ = new List<ReferenceValue__c>();
    OBJ = new DMLOperationsHandler('ReferenceValue__c')
      .selectFields(fields)
      .addConditionEq('Domain__c', 'Program Type')
      .run();
    
    return OBJ;
  }

  @AuraEnabled(cacheable=true)
  public static List<SObject> getProgramOptions() {
    String model = 'ReferenceValue__c';
    String fields = 'RefValue__c,RefKey__c';
    String cond = 'Domain__c = \'Program\'';

    return DMLOperationsHandler.selectSOQLWithConditionParameters(
      model,
      fields,
      cond
    );
  }

  @AuraEnabled(cacheable=true)
  public static List<SObject> getReferralProviderStatus(String caseId) {
    List<sObject> contactNote = new List<sObject>();
    try {
      contactNote = new DMLOperationsHandler('Case')
        .selectFields('Id, Status')
        .addConditionEq('Id', caseId)
        .run();
      if (Test.isRunningTest()) {
        if (caseId == null) {
          throw new DMLException();
        }
      }
    } catch (Exception ex) {
      CustomAuraExceptionData.LogIntoObject(
        strClassNameForLogger,
        'getApplicationProviderStatus',
        'Input parameters are :: caseId' + caseId,
        ex
      );
      CustomAuraExceptionData oErrorData = new CustomAuraExceptionData(
        'Error in fetching status',
        'Unable to get status details, Please try again',
        CustomAuraExceptionData.type.Error.name()
      );
      throw new AuraHandledException(JSON.serialize(oErrorData));
    }

    return contactNote;
    
  }
}
