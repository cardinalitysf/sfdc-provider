/**
     * @Author        : Jayachandran s
     * @CreatedOn     : August 08,2020
     * @Purpose       : Apex Methods to Use for IncidentDecision.cls
**/
public with sharing class IncidentDecision {
    public static string strClassNameForLogger='IncidentDecision';

    @AuraEnabled(cacheable=true)
    public static List<DMLOperationsHandler.FetchValueWrapper> fetchPickListValue( sObject objInfo, string pickListFieldApi ) {
        return DMLOperationsHandler.fetchPickListValue( objInfo, pickListFieldApi );
    }

    @AuraEnabled
	public static string updateIncidentdetailsDraft(sObject objSobjecttoUpdateOrInsert){
        return DMLOperationsHandler.updateOrInsertSOQLReturnId(objSobjecttoUpdateOrInsert);
    }

    @AuraEnabled
    public static void saveSign(String strSignElement, Id recId) {
        try {
            CommonUtils.saveSign(strSignElement, recId);
        } catch (Exception ex) {
            //To Log into object
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'saveSign', 'Input Parameters are :: {0}' + strSignElement + ' :: {1} ' + recId, ex);
            //To return back to the UI
            CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Save Signature failed to load', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
    }

    @AuraEnabled
    public static void updatecasedetails (sObject objSobjecttoUpdateOrInsert, String nextApproverId, String workItemId,String status){
        try {
            String comments = '';
        if(workItemId == ''){
                String Id = DMLOperationsHandler.updateOrInsertSOQLReturnId(objSobjecttoUpdateOrInsert);
                comments = (String) objSobjecttoUpdateOrInsert.get('IntakeComments__c');
                Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                req1.setComments(comments);
                req1.setObjectId(Id);
                req1.setSubmitterId(UserInfo.getUserId());
                req1.setNextApproverIds(new Id[] {nextApproverId});
                Approval.ProcessResult result = Approval.process(req1);
            }else { 
                String Id = DMLOperationsHandler.updateOrInsertSOQLReturnId(objSobjecttoUpdateOrInsert);
                    comments = (String) objSobjecttoUpdateOrInsert.get('CaseworkerComments__c');
                Approval.ProcessWorkitemRequest req1 = new Approval.ProcessWorkitemRequest();
                req1.setComments(comments);
                req1.setWorkitemId(workItemId);
                req1.setAction(status);
                if(nextApproverId!='')
                    req1.setNextApproverIds(new Id[] {nextApproverId});
                Approval.ProcessResult result = Approval.process(req1);
            }
                
            }catch (Exception Ex) {
                String[] arguments = new String[] {nextApproverId,workItemId};
                string strInputRecord= String.format('Input parameters are :: objSobjecttoUpdateOrInsert--{0} :: nextApproverId--{1} :: workItemId--{2}', arguments);
                CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'updatecasedetails',strInputRecord,Ex);            
                CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Case Update',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
                throw new AuraHandledException(JSON.serialize(oErrorData));
            }  
        }

        @AuraEnabled
        public static string getDatatableDetails(string caseid) {
            try {
                String model = 'ProcessInstance';
                String fields = 'Id, (SELECT Id, StepStatus, ActorId, Actor.Name, Actor.Profile.Name, Comments, CreatedDate,ProcessNodeId, ProcessNode.Name FROM StepsAndWorkitems ORDER BY Id DESC),(SELECT Id, ProcessInstanceId, ActorId, Actor.Name, Actor.Profile.Name, CreatedDate FROM Workitems),(SELECT Id, StepStatus, ActorId, Actor.Name, Actor.Profile.Name, Comments, CreatedDate FROM Steps ORDER BY CreatedDate DESC)';
                String cond = 'TargetObjectId =\'' + caseid + '\' ORDER BY CreatedDate DESC';
                List<SObject> lstApp= DMLOperationsHandler.selectSOQLWithConditionParameters(model, fields, cond);
                Map<Id, ContentDocumentLink> contentDocumentIdsmap = new Map<Id, ContentDocumentLink>();
                for(ContentDocumentLink conDocu : [SELECT ContentDocumentId, LinkedEntityId,LinkedEntity.Name FROM ContentDocumentLink WHERE LinkedEntityId =:caseid Order By SystemModstamp DESC]){
                    contentDocumentIdsmap.put(conDocu.ContentDocumentId, conDocu);
                }
                List<ContentVersion> contelist = [SELECT Id, OwnerId, CreatedById FROM ContentVersion WHERE ContentDocumentId In: contentDocumentIdsmap.keyset() And IsSignature__c=true];
                Set<Id> filteredContOwner = new Set <Id>();
                Set<ContentVersion> filteredContList = new Set <ContentVersion>();
                for(ContentVersion clist : contelist){
                    if(filteredContOwner.add(clist.OwnerId) == true)
                    filteredContList.add(clist);
                }
                if(lstApp.size() > 0){
                    List<processingWrapper> appWrap = new List<processingWrapper>();
                    for(SObject app : lstApp) {
                        processingWrapper prdWrapper = new processingWrapper(app, filteredContList);
                        appWrap.add(prdWrapper);
                    }
                    return JSON.serialize(appWrap);
                } else {
                    List<processingWrapperString> appWrap = new List<processingWrapperString>();
                    String modelm = 'Case';
                    String fieldsm = 'Id,SubmittedDate__c,Caseworker__r.Name,IntakeComments__c,IntakeDecisionStatus__c,Owner.Name,CaseworkerDecisionStatus__c,CaseworkerComments__c'; //add supervisor status and comments
                    String condm = 'Id =\'' + caseid + '\'';
                    List<SObject> monApp= DMLOperationsHandler.selectSOQLWithConditionParameters(modelm, fieldsm, condm);
                    for(SObject app : monApp) {
                        if(contelist.size() > 0) {
                            for (contentversion conversion: contelist) {
                                processingWrapperString prdWrapper=new processingWrapperString(app, conversion.id);
                                appWrap.add(prdWrapper);
                            }
                        } else {
                            processingWrapperString prdWrapper=new processingWrapperString(app, '');
                            appWrap.add(prdWrapper);
                        }
                    }
                    return JSON.serialize(appWrap);
                }
            } catch (Exception Ex) {  
                String[] arguments = new String[] {caseid};
                string strInputRecord= String.format('Input parameters are :: caseid -- {0}', arguments);
                CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getDatatableDetails',strInputRecord,Ex); 
                CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('History List',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
                throw new AuraHandledException(JSON.serialize(oErrorData));
            }
        }

        public class processingWrapper {
            public SObject cases {get;set;}
            public Set<ContentVersion> signatureUrl {get;set;}
            public processingWrapper(SObject app, Set<ContentVersion> url) {
                cases = app;
                this.signatureUrl = url;
            }
        }
    
        public class processingWrapperString {
            public SObject cases {get;set;}
            public String signatureUrl {get;set;}  
            public processingWrapperString(SObject app, String url) {
                cases = app;
                this.signatureUrl = url;
            }
        }

// validation data Fetch

        @AuraEnabled(cacheable=false)
        public static List<Actor__c> getIncidentPersonChildDetails(string referID) {
        Set<String> fields = new Set<String>{'Id'};
        List<sObject> incidentdata = new List<sObject>();
        try{
        incidentdata= new DMLOperationsHandler('Actor__c').
         selectFields(fields).
         addConditionEq('referral__c', referID).
         addConditionEq('Role__c','Person/Kid').
        run();
        if(Test.isRunningTest()) {
            if(referID==null){
                throw new DMLException();
            }
        } 
        }catch (Exception ex) {
            //To Log into object
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getincidentinfodetails',referID + 'Referalnid', ex);
            //To return back to the UI
            CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Sorry,Failed to load IncidentPersonChild Details ,Please Try Again..!', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return incidentdata;
        }

        @AuraEnabled(cacheable=false)
        public static List<Actor__c> getWitnessDetails(string IncidentId) {
        List<sObject> actorObj = new List<sObject>();
        Set<String> fields = new Set<String>{'Id,Contact__r.FirstName__c'};
        try {
            actorObj= new DMLOperationsHandler('Actor__c').
            selectFields(fields).
            addConditionEq('Referral__c',IncidentId).
            addConditionEq('RoleName__c', true).
            run();
        if(Test.isRunningTest()) {
            if(IncidentId==null){
                throw new DMLException();
            }
        } 
        } catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getWitnessDetails','IncidentId'+IncidentId,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('DataTable based error in fetching from Actor. try later',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
          }
          return actorObj;
        }

        @AuraEnabled(cacheable=false)
        public static List<SObject> getActorDetails(String caseId) {
        List<sObject> staffMembers = new List<sObject>();
           try {
            String fields = 'Id';
            staffMembers = new DMLOperationsHandler('Actor__c').
                                selectFields(fields).
                                addConditionEq('Referral__c', caseId).
                                addConditionEq('Role__c', 'Staff').               
                                run();
                                if(Test.isRunningTest()) {
                                    if(caseId == null) {
                                        throw new DMLException();
                                    }
                                }
           } catch (Exception ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getActorDetails','Input parameters are :: caseId' + caseId ,ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in Fetching Staff details','Unable to get Staff details, Please try again' , CustomAuraExceptionData.type.Error.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
           }           
           return staffMembers;
        }

        @AuraEnabled(cacheable=false)
       public static List<Case> getReferralData(String referralId) {
               List<Case> getReferralDataForDataTable = new List<Case>();
               try {
                    getReferralDataForDataTable = new DMLOperationsHandler('Case').
                    selectFields('Id, IncidentType__c').
                    addConditionEq('Id', referralId).
                    run();
                    if(Test.isRunningTest()) {
                        if(referralId == null) {
                            throw new DMLException();
                        }
                    }
               } catch(Exception e) {
                    CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getReferralData', 'referralId' + referralId, e);
                    CustomAuraExceptionData errorData=new CustomAuraExceptionData('getReferralData Data',e.getMessage() , CustomAuraExceptionData.type.Informational.name());
                    throw new AuraHandledException(JSON.serialize(errorData));
               }
               return getReferralDataForDataTable;
       }

       @AuraEnabled(cacheable=false)
       public static List<Case> getReviewDetails(string caseid){
           List<Case> caseObj = new List<Case>();
           String fields = 'Id,SubmittedDate__c,Caseworker__r.Name,IntakeComments__c,IntakeDecisionStatus__c,Owner.Name,CaseworkerComments__c,CaseworkerDecisionStatus__c';
           try {
               caseObj = new DMLOperationsHandler('Case').
               selectFields(fields).
               addConditionEq('Id', caseid).
               run();
               if(Test.isRunningTest()) {
                   if(caseid==null) {
                       throw new DMLException();
                   }				
               }
           } catch (Exception Ex) {  
               String[] arguments = new String[] {caseid};
               string strInputRecord= String.format('Input parameters are :: case--{0}', arguments);
               CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getReviewDetails',strInputRecord,Ex);            
               CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Review Details',Ex.getMessage() , CustomAuraExceptionData.type.Warning.name());
               throw new AuraHandledException(JSON.serialize(oErrorData));
           }
           return caseObj;
       }

        
}
