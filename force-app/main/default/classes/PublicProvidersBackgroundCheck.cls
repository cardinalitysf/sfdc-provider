public with sharing class PublicProvidersBackgroundCheck {
public static string strClassNameForLogger='PublicProvidersBackgroundCheck';
@AuraEnabled(cacheable=true)
public static List<SObject> getHouseholdMembers(string providerid) {
try 
{
        Set<String> fields = new Set<String>{'Id, Contact__r.Title__c,Contact__r.FirstName__c, Contact__r.LastName__c, Contact__r.ApproximateAge__c,Contact__r.Gender__c,Contact__r.Id, Contact__r.ContactNumber__c, Role__c ,referral__c'};
        List<SObject> questions =
        new DMLOperationsHandler('Actor__c').
        selectFields(fields).
        addConditionEq('Provider__c', providerid).   
        addConditionEq('Contact__r.RecordType.Name','Household Members').
        run();
        if(Test.isRunningTest())
        {
        if(providerid==null ){
        throw new DMLException();
        }

        }
        return questions;
}
catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getHouseholdMembers',providerid,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Household Members details based on Provider',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
}

}

@AuraEnabled(cacheable=true)
public static List<SObject> getQuestionsByType(string type) {
try{
        Set<String> fields = new Set<String>{'Id, Question__c,Fieldtype__c, Regulations__c, Type__c, RecordTypeId,RefKey__c,QuestionOrder__c'};
        List<SObject> questions =
        new DMLOperationsHandler('ReferenceValue__c').
        selectFields(fields).
        addConditionEq('Type__c', type).    
        orderBy('QuestionOrder__c', 'asc').  
        run();
        if(Test.isRunningTest())
        {
        if(type==null ){
        throw new DMLException();
        }

        }
        return questions;
}
catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getQuestionsByType',type,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Question based on Type',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
}
}


@AuraEnabled(cacheable=true)
public static string getRecordDetails(string reconsiderationId) {

        List<ProviderRecordQuestion__c > oProviderQuestion =  new DMLOperationsHandler('ProviderRecordQuestion__c').
        selectFields('Id, Reconsideration__c, actor__c,RecordTypeId,RecordType.name,status__c,Actor__r.Contact__r.FirstName__c,actor__r.Role__c,Actor__r.Contact__r.lastname__c,Actor__r.Contact__r.Gender__c,Actor__r.Contact__r.ApproximateAge__c, Actor__r.Contact__r.ContactNumber__c').
        addConditionEq('Reconsideration__c', reconsiderationId). 
        run();

        Map<id,ProviderRecordQuestion__c> oMapCriminalHistory=new Map<id,ProviderRecordQuestion__c>();
        Map<id,ProviderRecordQuestion__c> oPersonalSecurity=new Map<id,ProviderRecordQuestion__c>();
        for (ProviderRecordQuestion__c provQues: oProviderQuestion)
        {
        if(Schema.getGlobalDescribe().get('ProviderRecordQuestion__c').getDescribe().getRecordTypeInfosById().get(provQues.RecordTypeId).getName()=='Criminal History')
        {
        oMapCriminalHistory.put(provQues.actor__c,provQues) ;  
        }
        else if(Schema.getGlobalDescribe().get('ProviderRecordQuestion__c').getDescribe().getRecordTypeInfosById().get(provQues.RecordTypeId).getName()=='Personal Security')
        {
        oPersonalSecurity.put(provQues.actor__c,provQues) ;  
        }
        }
        List<ProviderRecordQuestionWrapper> oProviderRecordQuestionWrapper=new List<ProviderRecordQuestionWrapper>();
        for(id key : oMapCriminalHistory.keyset() )
        {
        ProviderRecordQuestionWrapper oWrapper=new ProviderRecordQuestionWrapper();
        oWrapper.actorId=key;
        oWrapper.criminalBackGroundType=oMapCriminalHistory.get(key).Status__c;
        oWrapper.personalHistoryBackGroundType=oPersonalSecurity.get(key).Status__c;

        oWrapper.actorRole=oMapCriminalHistory.get(key).Actor__r.Role__c;
        oWrapper.actorFirstName=oMapCriminalHistory.get(key).Actor__r.Contact__r.FirstName__c;
        oWrapper.actorLastName=oMapCriminalHistory.get(key).Actor__r.Contact__r.Lastname__c;
        oWrapper.actorGender=oMapCriminalHistory.get(key).Actor__r.Contact__r.Gender__c;
        oWrapper.actorAge=oMapCriminalHistory.get(key).Actor__r.Contact__r.ApproximateAge__c;
        oWrapper.actorPID=oMapCriminalHistory.get(key).Actor__r.Contact__r.ContactNumber__c;

        oWrapper.criminalBackGroundTypeId=oMapCriminalHistory.get(key).id;
        oWrapper.personalHistoryBackGroundTypeId=oPersonalSecurity.get(key).Id;
        oProviderRecordQuestionWrapper.add(oWrapper);
        }
        return JSON.serialize(oProviderRecordQuestionWrapper);

}

@AuraEnabled
public static void bulkAddRecordAns(String datas,string questionId){
try{
        List<ProviderRecordAnswer__c> dataToInsert = (List<ProviderRecordAnswer__c>)System.JSON.deserialize(datas, List<ProviderRecordAnswer__c>.class);
        List<ProviderRecordAnswer__c> listInsert = new List<ProviderRecordAnswer__c>();
        for(ProviderRecordAnswer__c p: dataToInsert){
        ProviderRecordAnswer__c objTask = new ProviderRecordAnswer__c();
        objTask = new ProviderRecordAnswer__c(              
        HouseholdStatus__c = p.HouseholdStatus__c,
        Date__c = p.Date__c, 
        Comments__c  = p.Comments__c,  
        Comar__c   = p.Comar__c, 
        Dependent__c = p.Dependent__c, 
        State__c = p.State__c,          
        ProviderRecordQuestion__c = questionId);
        listInsert.add(objTask);
        }
        DMLOperationsHandler.updateOrInsertSOQLForListWithStatus(listInsert);
}
catch (Exception Ex) {
        String[] arguments = new String[] {datas , questionId};
        string strInputRecord= String.format('Input parameters are :: datas -- {0} :: questionId -- {1} ', arguments);
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'bulk insert Background Checks',strInputRecord,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('bulk insert Background Checks',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
}

}


@AuraEnabled
public static void bulkUpdateRecordAns(String datas){
try{
        List<ProviderRecordAnswer__c> dataToInsert = (List<ProviderRecordAnswer__c>)System.JSON.deserialize(datas, List<ProviderRecordAnswer__c>.class);
        List<ProviderRecordAnswer__c> listUpdate = new List<ProviderRecordAnswer__c>();
        for(ProviderRecordAnswer__c p: dataToInsert){
        ProviderRecordAnswer__c objTask = new ProviderRecordAnswer__c();
        objTask = new ProviderRecordAnswer__c(
        Id = p.Id,
        HouseholdStatus__c = p.HouseholdStatus__c,
        Date__c = p.Date__c, 
        Comments__c  = p.Comments__c, 
        Dependent__c = p.Dependent__c,  
        State__c = p.State__c,         
        ProviderRecordQuestion__c = p.ProviderRecordQuestion__c);
        listUpdate.add(objTask);
        }
        DMLOperationsHandler.updateOrInsertSOQLForListWithStatus(listUpdate);
}
catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'bulkUpdateRecordAns',datas,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Update Background Checks based on Reconsider',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
}

}

@AuraEnabled(cacheable=true)
public static Id getRecordType(String name) {
    return CommonUtils.getRecordTypeIdbyName('ProviderRecordQuestion__c', name);
}

public class ProviderRecordQuestionWrapper{
        @AuraEnabled public string actorId;
        @AuraEnabled public string criminalBackGroundType;
        @AuraEnabled public string personalHistoryBackGroundType;
        @AuraEnabled public string criminalBackGroundTypeId;
        @AuraEnabled public string personalHistoryBackGroundTypeId;    
        @AuraEnabled public string actorFirstName;    
        @AuraEnabled public string actorRole;  
        @AuraEnabled public string actorLastName;  
        @AuraEnabled public string actorGender; 
        @AuraEnabled public decimal actorAge;   
        @AuraEnabled public string actorPID;  
}




@AuraEnabled(cacheable=true)
public static List<SObject> editRecordDet(string questionId) {
try{
        Set<String> fields = new Set<String>{'Id, Comar__R.Question__c,Comar__r.Type__c,Comar__R.RefKey__c,Dependent__c, State__c,Date__c,ProviderRecordQuestion__c,HouseholdStatus__c,Comments__c,Actor__c'};
        List<SObject> questions =
        new DMLOperationsHandler('ProviderRecordAnswer__c').
        selectFields(fields).
        addConditionEq('ProviderRecordQuestion__c', questionId).  

        run();
        if(Test.isRunningTest())
        {
        if(questionId==null ){
        throw new DMLException();
        }

        }
        return questions;
}
catch (Exception Ex) {
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'editRecordDet',questionId,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Edit Record Delte',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
}
}

@AuraEnabled(cacheable=true)
public static List<DMLOperationsHandler.FetchValueWrapper> getPickListValues(sObject objInfo, string picklistFieldApi) {
        return DMLOperationsHandler.fetchPickListValue(objInfo,picklistFieldApi);
}


@AuraEnabled
public static string updateActorFieldInContentversion(string strDocumentIds,string strActor){
string strResultStatus;
try {         
        Map<Id, ContentDocumentLink> contentDocumentIdsmap = new Map<Id, ContentDocumentLink>();
        contentDocumentIdsmap=CommonDocuments.getMapOfContentDocumentIDFromDocumentId(strDocumentIds);
        List<ContentVersion> oVersions = new List<ContentVersion>();        
        for(Id cdl: contentDocumentIdsmap.keyset()) {
        ContentVersion oVersion = new ContentVersion();
        oVersion.id = contentDocumentIdsmap.get(cdl).ContentDocument.LatestPublishedVersionId;
        oVersion.Actor__c = strActor;                   
        oVersions.add(oVersion);
        } 
        DMLOperationsHandler.updateOrInsertSOQLForList(oVersions);    
        strResultStatus=ProviderConstants.SUCCESS; 
} catch (Exception Ex) {
        String[] arguments = new String[] {strDocumentIds , strActor};
        string strInputRecord= String.format('Input parameters are :: strDocumentIds -- {0} :: strActor -- {1} ', arguments);
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'updateActorFieldInContentversion',strInputRecord,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Fileload Save',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));         
}          
return strResultStatus;
}


@AuraEnabled(cacheable=true)
public static List<contentversion>  getUploadedDocuments(String strDocumentID) {  

        List<contentversion> oContentVersionList= new DMLOperationsHandler('contentversion').
        selectFields('id,Actor__c,ContentDocumentId,Description,CWhealth__c,PathOnClient,DocumentSize__c,Title, Createddate,filetype,createdBy.Name').                                               
        addConditionIn('ContentDocumentId ', getMapOfContentDocumentIDFromLinkedEntityID(strDocumentID).keyset()).
        orderBy('Createddate', 'Desc').
        run();   

        return oContentVersionList;


}

private static Map<Id, ContentDocumentLink> getMapOfContentDocumentIDFromLinkedEntityID(String strDocumentID)
{

        List<String> fieldList = strDocumentID.split(', *');
        Map<Id, ContentDocumentLink> contentDocumentIdsmap = new Map<Id, ContentDocumentLink>();

        List<ContentDocumentLink> oContentDocumetLink= new DMLOperationsHandler('ContentDocumentLink').
        selectFields('Id, ContentDocument.Description, ContentDocumentId, ContentDocument.LatestPublishedVersionId,LinkedEntityId,LinkedEntity.Name').
        addConditionIn('LinkedEntityId ', fieldList).
        run();    

        for(ContentDocumentLink oContentDoc : oContentDocumetLink){
        contentDocumentIdsmap.put(oContentDoc.ContentDocumentId, oContentDoc);
        }      

        return contentDocumentIdsmap;

}
}
