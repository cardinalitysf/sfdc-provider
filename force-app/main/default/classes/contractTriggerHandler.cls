/**
 * @Author        : Sindhu Venkateswarlu
 * @CreatedOn     : APRIL 23 ,2020
 * @Purpose       :Narrative Trigger Handler Class on Contract Object.
**/
public class contractTriggerHandler
{
public static void afterupdate(Map<Id, Contract__c> newMap, Map<Id, Contract__c> oldMap){
    updatecontractHistoryTracking(newMap,oldMap);
    //Added by Marimuthu on May 4 2020
    //Inorder to move Provider status to Active If any of the contract belongs to him becomes submitted
    updateProviderStatusInAccount(newMap,oldMap);
}
public static void afterinsert(List<Contract__c> newvaluelist){
    list<HistoryTracking__c> hstTrackList = new list<HistoryTracking__c>();
    if(!newvaluelist.isEmpty()){
        for (Contract__c contract: newvaluelist){
            HistoryTracking__c hstTrack = new HistoryTracking__c();
                    hstTrack.ObjectName__c = 'Contract__c';
                    hstTrack.ObjectRecordId__c = contract.Id;
                    hstTrack.RichNewValue__c = contract.Narrative__c;
                    hstTrackList.add(hstTrack);

        }
    }
    if(!hstTrackList.isEmpty()){
        insert hstTrackList;
    }
}

private static void updatecontractHistoryTracking(Map<Id, Contract__c> newMap, Map<Id, Contract__c> oldMap){

        list<HistoryTracking__c> hstTrackList = new list<HistoryTracking__c>();
        for (Contract__c contr: newMap.values()){

                Contract__c newContract = newMap.get(contr.ID);
                Contract__c oldContract = oldMap.get(contr.ID);
                if(oldContract.Narrative__c != newContract.Narrative__c){

                    HistoryTracking__c hstTrack = new HistoryTracking__c();
                    hstTrack.ObjectName__c = 'Contract__c';
                    hstTrack.ObjectRecordId__c = contr.Id;
                    hstTrack.RichOldValue__c = oldContract.Narrative__c;
                    hstTrack.RichNewValue__c = newContract.Narrative__c;
                    hstTrack.HistoryName__c = ProviderConstants.HISTORY_DESCRIPTION;
                    hstTrackList.add(hstTrack);
                }
           
        }
        if(hstTrackList.size() >0){
            insert hstTrackList;
        }
    }

     /**
	 * @Author        : V.S.Marimuthu
	 * @CreatedOn     : May 4 ,2020
	 * @Purpose       : Method to Update Account when Appliction status moved to Approved
	 **/

    public static void updateProviderStatusInAccount(Map<Id, Contract__c> newMap, Map<Id, Contract__c> oldMap){
        try{
		    List<Account> lstAppToFetchProvider= new List<Account>();
		    for(Contract__c objContract : ((Map<Id,Contract__c>)newMap).values()){
		        //Create an old and new map so that we can compare values
		        Contract__c oldContract = oldMap.get(objContract.ID);
		        Contract__c newContract = newMap.get(objContract.ID);
		        //If the fields are different, then ID has changed
		        if(oldContract.ContractStatus__c != newContract.ContractStatus__c && newContract.ContractStatus__c.toUppercase() == ProviderConstants.ACTIVE){
                 Account objAccount=new Account();  
                 objAccount.Id=objContract.Provider__c;       
                 objAccount.Status__c=ProviderConstants.ACTIVE;   
                 lstAppToFetchProvider.add(objAccount);
		        }
		    }
		    if(lstAppToFetchProvider.size()>0){
              upsert lstAppToFetchProvider;
		    }// if
        }
  
		catch(Exception Ex)
		{
		}
    }
}