/**
   * @Author        : Balamurugan
   * @CreatedOn     : March 17,2020
   * @Purpose       :Upsert and Fetching Decision Details
   **/

public with sharing class referralDecision {
  public static string strClassNameForLogger='referralDecision';
	@AuraEnabled(cacheable=true)
 public static List<User> getUserDetails() {
  Id profileId = userinfo.getProfileId();
           Set<String> fields = new Set<String>{'Id','Name', 'Profile.Name', 'UserRole.Name'};
           List<User> users = new List<User>();
		   try{
           users =  new DMLOperationsHandler('User').
           selectFields(fields).
           addConditionEq('Id', 'profileId').
           run();
		   if(Test.isRunningTest())
            {
            if(true){
                    throw new DMLException();
            }
			}
		   }catch(Exception ex){
			//To Log into object
             CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getUserDetails',null,Ex);
             //To return back to the UI
             CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get user details ,Please Try Again..!',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
             throw new AuraHandledException(JSON.serialize(oErrorData));   
		   }
           
           return users;
        }

  
	@AuraEnabled(cacheable=true)
	public static List<DMLOperationsHandler.FetchValueWrapper> getPickListValues(sObject objInfo,string picklistFieldApi) {
	  return DMLOperationsHandler.fetchPickListValue(objInfo, picklistFieldApi);
	}
  
	@AuraEnabled
	public static string updateOrInsertSOQL(sObject objSobjecttoUpdateOrInsert,String newprovider,String rdmNumber) {
	  String strSuccess = DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert);
  
	  Case csObj = (Case) objSobjecttoUpdateOrInsert;
  
	  //csObj = objSobjecttoUpdateOrInsert;
  
	  if (strSuccess == 'SUCCESSFULLY UPDATED') {
		if (csObj.Status == 'Approved') {
		  List<Account> accList = getProviderDetails(newprovider);
		  
		  List<Profile> providerCommList = getProvidercommunity();
  
		  if (!accList.isEmpty()) {
			string strAlias = accList[0].FirstName__c + accList[0].LastName__c;
			string strLastName= accList[0].LastName__c;
			strLastName = strLastName == '' ? 'Cardinality' : strLastName;
			strAlias= strAlias == '' ? 'New':strAlias;
			strAlias = strAlias.length() > 8 ? strAlias.substring(0, 7) : strAlias;
			User myuserobject = new User();
			myuserobject.ProfileId = providerCommList[0].Id;
			myuserobject.Username = accList[0].Email__c + '.' + rdmNumber;
			myuserobject.Alias = strAlias;
			myuserobject.Email = accList[0].Email__c;
			myuserobject.FirstName = accList[0].FirstName__c;
			myuserobject.LastName =strLastName;
			myuserobject.TimeZoneSidKey = 'GMT';
			myuserobject.LanguageLocaleKey = 'en_US';
			myuserobject.EmailEncodingKey = 'UTF-8';
			myuserobject.LocaleSidKey = 'en_US';
			myuserobject.IsActive = true;
  
			try {
			  
			  Database.DMLOptions dmlOption = new Database.DMLOptions();
			  dmlOption.assignmentRuleHeader.useDefaultRule = true; // successfully assigns User
			  // This will send Notification Email to User
			  dmlOption.EmailHeader.triggerUserEmail = true;
			  dmlOption.EmailHeader.triggerAutoResponseEmail = true;
			  
			  myuserobject.setOptions(dmlOption);
			  
			  insert myuserobject;
			  Database.update(myuserobject, dmlOption); // Assuming userObj is User Object instance
			  return ProviderConstants.UPDATESUCCESS;
			} catch (Exception ex) {
			 // return ProviderConstants.ERROR;

            String[] arguments = new String[] {newprovider,rdmNumber};
        string strInputRecord= String.format('Input parameters are :: newprovider -- {0} :: rdmNumber -- {1}', arguments);
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'updateOrInsertSOQL',strInputRecord ,ex);
       //CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('User Insertion',ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        //throw new AuraHandledException(JSON.serialize(oErrorData));
			}
		  }
		}
		return ProviderConstants.UPDATESUCCESS;
	  }
	  return null;
	}
  
	@AuraEnabled(cacheable=true)
	public static List<Account> getProviderDetails(string newprovider) {
	 // String model = 'Account';
	 // String fields = 'Id,Email__c,FirstName__c,LastName__c';
	 // String cond = 'Id =\'' + newprovider + '\'';
	 // return DMLOperationsHandler.selectSOQLWithConditionParameters(model,fields,cond);

         String queryFields;
       List<Account> providerObj = new List<Account>();
       try {
         queryFields = 'Id,Email__c,FirstName__c,LastName__c';
        providerObj = new DMLOperationsHandler('Account').
                                    selectFields(queryFields).
                                    addConditionEq('Id', newprovider).
                                     run();
            if(Test.isRunningTest())
            {
            if(newprovider==null){
                    throw new DMLException();
            }
            }
        } 
       catch (Exception ex) {
        String inputParameterForProviderDetails='Inputs parameter new provider ::'+newprovider;
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getProviderDetails',inputParameterForProviderDetails,ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Provider Details',ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
        }
       return providerObj;
	}
	 @AuraEnabled(cacheable=true)
        public static List<Profile> getProvidercommunity() {
           Set<String> fields = new Set<String>{'Id'};
           List<Profile> profiles = new List<Profile>();	
           	 
           profiles = new DMLOperationsHandler('Profile').
                       selectFields(fields).
                       addConditionEq('Name', 'Provider Community').
                       run();   
           return profiles;
        }
  
	

  @AuraEnabled(cacheable=false)
        public static List<Case> getDecisionDetails(string Ereferral) {
           Set<String> fields = new Set<String>{'Id','Status','Comments__c','CaseNumber','DecisionStatus__c'};
           List<Case> cases = new  List<Case>();
		   try{
          cases = new DMLOperationsHandler('Case').
           selectFields(fields).
           addConditionEq('Id', Ereferral).
           run();
		     if(Test.isRunningTest())
            {
            if(Ereferral==null){
                    throw new DMLException();
            }
            }
		   }catch(Exception ex){
			   
			//To Log into object
             CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getDecisionDetails',Ereferral + 'Ereferral',Ex);
             //To return back to the UI
             CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Decision details ,Please Try Again..!',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
             throw new AuraHandledException(JSON.serialize(oErrorData));   
		   }
          
           return cases;
        }

  }