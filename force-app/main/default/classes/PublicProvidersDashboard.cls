
/**
 * @Author        : Preethi Bojarajan
 * @CreatedOn     : Apr 16,2020
 * @Purpose       : Providers Dashboard for Acitve / Inactive
 * @updatedBy     : May 08, 2020
 * @updatedOn     : Preethi Bojarajan
 **/

 //Dashboard Card Counting Classes
 public with sharing class PublicProvidersDashboard {
    public static string strClassNameForLogger='PublicProvidersDashboard';
    @AuraEnabled(cacheable=true)
    public static List<WrapperAccountcount> getProvidersCount(String searchKey, String searchType, String providerType) {
        List<WrapperAccountcount> lstWrapper = new List<WrapperAccountcount>();
        String key = '%' + searchKey + '%';
        Integer countActive = 0;
        Integer countInactive = 0;
        List<AggregateResult> results = new List<AggregateResult>();
        try {
            results =  new DMLOperationsHandler('Account').
                            selectFields('Status__c').
                            addConditionEq('ProviderType__c', providerType).
                            addConditionLike(searchType, key).
                            count('Id').   
                            groupBy('Status__c').
                            aggregate();
                            if(Test.isRunningTest()) {
                                if(searchKey == null){
                                    throw new DMLException();
                                }
                            } 
        } catch (Exception ex) {
            //To Log into object
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getProvidersCount', searchKey +'Search Key' + searchType + 'Search By' + providerType + 'ProviderType' , ex);
            //To return back to the UI
            CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Providers Count Fetch Issue,Please Try Again..!', ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        for(AggregateResult result : results) {
            if(result.get('Status__c') == 'Active')
                countActive = (Integer) result.get('expr0');
            else if(result.get('Status__c') == 'Inactive')
                countInactive = (Integer) result.get('expr0');
        }
        lstWrapper.add(new WrapperAccountcount(countActive, countInactive));
        return lstWrapper;
    }

    @AuraEnabled(cacheable=true)
    public static List<WrapperAccountSupcount> getProvidersSupCount(String searchKey, String searchType, String modelName) {
        List<WrapperAccountSupcount> lstWrapper = new List<WrapperAccountSupcount>();
        String key = '%' + searchKey + '%';
        String fields = '';
        String likeFields = '';
        String secField = '';
        String thrField = '';
        if(modelName == 'Monitoring__c') {
            fields = 'MonitoringStatus__c';
            likeFields = 'ApplicationLicenseId__r.Provider__r.' ;
            secField = 'Approved';
            thrField = 'Rejected';       
        } else if(modelName == 'Contract__c') {
            fields='ContractStatus__c';
            likeFields = 'Provider__r.';  
            secField = 'Active';
            thrField = 'Inactive';        
        }
        
        Integer countSubmitted = 0;
        Integer countApproved = 0;
        Integer countRejected = 0;
        if (searchType == 'ProviderId__c')
            likeFields = likeFields + 'ProviderId__c';
        else if (searchType == 'Name__c')
            likeFields = likeFields + 'Name__c';
        else 
            likeFields = likeFields + 'BillingCountry';
        List<AggregateResult> results = new List<AggregateResult>();
        try {
            results =  new DMLOperationsHandler(modelName).  
                            selectFields(fields).
                            addConditionLike(likeFields, key).
                            count('Id').   
                            groupBy(fields).
                            aggregate();
                            if(Test.isRunningTest()) {
                                if(searchKey == null){
                                    throw new DMLException();
                                }
                            } 
        } catch (Exception ex) {
            //To Log into object
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getProvidersCount', searchKey +'Search Key' + searchType + 'Search By' + modelName + 'Model Name' , ex);
            //To return back to the UI
            CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Providers Count Fetch Issue,Please Try Again..!', ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        for(AggregateResult result : results) {
            if(result.get(fields) == 'Submitted')
            countSubmitted = (Integer) result.get('expr0');
            else if(result.get(fields) == secField)
            countApproved = (Integer) result.get('expr0');
            else if(result.get(fields) == thrField)
            countRejected = (Integer) result.get('expr0');
        }
        lstWrapper.add(new WrapperAccountSupcount(countSubmitted, countApproved, countRejected));
        return lstWrapper;
    }

    // Wrapper Class
    public class WrapperAccountcount {
        @AuraEnabled public Integer active;
        @AuraEnabled public Integer inactive;
        @AuraEnabled public Integer total;
        public WrapperAccountcount(Integer countActive, Integer countInactive) {
            this.active = countActive;
            this.inactive = countInactive;
            this.total = countActive + countInactive;
        }
    }

    public class WrapperAccountSupcount {
        @AuraEnabled public Integer Submitted;
        @AuraEnabled public Integer Approved;
        @AuraEnabled public Integer Rejected;
        public WrapperAccountSupcount(Integer countSubmitted, Integer countApproved, Integer countRejected) {
            this.Submitted = countSubmitted;
            this.Approved = countApproved;
            this.Rejected = countRejected;
        }
    }

    //Data Table Data Fetching Classes
    @AuraEnabled(cacheable=true)
    public static List<Account> getProvidersList(String searchKey, String type, String searchType, String providerType) {
        String model = 'Account';
        String fields = 'Id, ProviderId__c, ProviderType__c, Name, Name__c, BillingCountry, TaxId__c, Phone, Status__c';
        searchKey = '%' + searchKey + '%';
        List<String> lstStatus = type.split(',');
        if(type == 'total'){
            lstStatus.add('Inactive');
            lstStatus.add('Active');
        } else if(type == 'active'){
            lstStatus.add('Active');
        } else lstStatus.add(type);
        String justRole = 'Applicant,Co-Applicant';
        List<String> role = justRole.split(',');
        List<SObject> proviList = new List<SObject>();
        try {
            proviList = new DMLOperationsHandler(model).
                            selectFields(fields).
                            addSubquery(
                            DMLOperationsHandler.subquery('Actors__r').
                            selectFields('Id, Role__c, Contact__r.LastName, Contact__r.SSN__c, Application__r.Program__c').
                            addConditionIn('Role__c', role)).
                            addConditionIn('Status__c', lstStatus).
                            addConditionEq('ProviderType__c', providerType).
                            addConditionLike(searchType, searchKey).
                            orderBy('LastModifiedDate', 'DESC').
                            run();
                            if(Test.isRunningTest()) {
                                if(searchKey == null){
                                    throw new DMLException();
                                }
                            } 
        } catch (Exception ex) {
            //To Log into object
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getProvidersCount', searchKey +'Search Key' + searchType + 'Search By' + providerType + 'ProviderType' , ex);
            //To return back to the UI
            CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Providers Count Fetch Issue,Please Try Again..!', ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return proviList;
    }

    @AuraEnabled
    public static List<Monitoring__c> getMonitoringList(String searchKey, String type, String searchType) {
        String model = 'Monitoring__c';
        searchType = 'ApplicationLicenseId__r.Provider__r.' + searchType;
        searchKey = '%' + searchKey + '%';
        String fields = 'Id, Name, ApplicationLicenseId__r.Id, ApplicationLicenseId__r.Provider__r.ProviderId__c, ApplicationLicenseId__r.Provider__r.ProviderType__c, ApplicationLicenseId__r.Provider__r.Name, ApplicationLicenseId__r.Provider__r.BillingCountry, ApplicationLicenseId__r.Provider__r.TaxId__c, ApplicationLicenseId__r.Provider__r.Status__c, ApplicationLicenseId__r.Provider__r.Phone, MonitoringStatus__c' ;
        
        List<SObject> moniList = new List<SObject>();
        try {
            moniList = new DMLOperationsHandler(model).
                        selectFields(fields).
                        addConditionEq('MonitoringStatus__c', type).
                        addConditionNotEq('ApplicationLicenseId__r.Id', null).
                        addConditionLike(searchType, searchKey).
                        orderBy('ApplicationLicenseId__r.Provider__r.ProviderId__c', 'DESC').
                        run();
                        if(Test.isRunningTest()) {
                            if(searchKey == null){
                                throw new DMLException();
                            }
                        }
            } catch (Exception Ex) {
                CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getContractList',searchKey+ 'searchKey'+ type+'contractStatus'+searchType+'searchType', Ex);
                CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Sorry,Failed to load Referral list details ,Please Try Again..!',Ex.getMessage() , CustomAuraExceptionData.type.error.name());
                throw new AuraHandledException(JSON.serialize(oErrorData));
            }
        return moniList;
    }

    @AuraEnabled(cacheable=true)
    public static List<Contract__c> getContractList(String searchKey, String type, String searchType) {
        String model = 'Contract__c';
        searchType = 'Provider__r.' + searchType;
        searchKey = '%' + searchKey + '%';
        String fields = 'Id, Name, Provider__r.ProviderId__c, Provider__r.ProviderType__c, Provider__r.Name, Provider__r.BillingCountry, Provider__r.TaxId__c, Provider__r.Status__c, Provider__r.Phone, ContractStatus__c' ;
        List<SObject> contractList = new List<SObject>();
        try {
            contractList = new DMLOperationsHandler(model).
                                    selectFields(fields).
                                    addConditionEq('ContractStatus__c', type).
                                    addConditionNotEq('Provider__r.ProviderId__c', null).
                                    addConditionLike(searchType, searchKey).
                                    orderBy('Provider__r.ProviderId__c', 'DESC').
                                    run();
                                    if(Test.isRunningTest()) {
                                        if(searchKey == null || type == null || searchType == null) {
                                            throw new DMLException();
                                        }
                                    }
        } catch (Exception Ex) {
                CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getContractList',searchKey+ 'searchKey'+ type+'contractStatus'+searchType+'searchType', Ex);
                CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Sorry,Failed to load Referral list details ,Please Try Again..!',Ex.getMessage() , CustomAuraExceptionData.type.error.name());
                throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return contractList;
    }

    @AuraEnabled(cacheable=true)
    public static List<Contract__c> fetchApplicationName(String providerId) {
        String model = 'Application__c';
        String fields = 'Id, Name, Status__c' ;
        List<SObject> contractList = new List<SObject>();
        try {
            contractList = new DMLOperationsHandler(model).
                                    selectFields(fields).
                                    addConditionEq('Provider__c', providerId).
                                    run();
                                    if(Test.isRunningTest()) {
                                        if(providerId == null) {
                                            throw new DMLException();
                                        }
                                    }
        } catch (Exception Ex) {
                CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'fetchApplicationName',providerId+ 'providerId', Ex);
                CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Sorry,Failed to load Referral list details ,Please Try Again..!',Ex.getMessage() , CustomAuraExceptionData.type.error.name());
                throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return contractList;
    }
}
