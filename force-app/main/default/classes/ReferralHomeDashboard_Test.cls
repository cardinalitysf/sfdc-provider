/**
 * @Author        : V.S.Marimuthu
 * @CreatedOn     : March 18 ,2020
 * @Purpose       : Test Methods to unit test ReferralHomeDashboard class in ReferralHomeDashboard.cls
 **/

@isTest
public with sharing class ReferralHomeDashboard_Test {
    // Testing Positive flow
    @isTest static void testgetreferralhomeCasesPositiveFlow(){
       //Inserting records into case object where status=Approved
        List<Case> lstinsertCase=TestDataFactory.TestCaseData(5,'Approved');
        INSERT lstinsertCase;

        //Inserting records into case object where status=Draft
        lstinsertCase=TestDataFactory.TestCaseData(6,'Draft');
        INSERT lstinsertCase;

        //Inserting records into case object where status=Rejected
        lstinsertCase=TestDataFactory.TestCaseData(10,'Rejected');
        INSERT lstinsertCase;

       
        Test.startTest();
        List<ReferralHomeDashboard.Wrappercasecount> lstAllCOunt=ReferralHomeDashboard.getreferralhomeCases();
        Integer strRejectedcasescount=0,strpendingcasescount=0,strapprovedcasescount=0,strtotalcount=0;
        for(ReferralHomeDashboard.Wrappercasecount individualCount:lstAllCOunt)
        {
            System.assertEquals(true, individualCount.Rejectedcasescount == 10 , 'Rejected casecount size quals the inserted record');
            System.assertEquals(true, individualCount.pendingcasescount == 6 , 'Pending casecount size quals the inserted record');
            System.assertEquals(true, individualCount.approvedcasescount == 5 , 'Approved casecount size quals the inserted record');
            System.assertEquals(true, individualCount.totalcount == 21 , 'Toal casecount size quals the inserted record');
        }         
        
        Test.stopTest();
    }

    // Testing Negative flow
    @isTest static void testgetreferralhomeCasesNegativeFlow(){
         //Inserting records into case object where status=Approved
         List<Case> lstinsertCase=TestDataFactory.TestCaseData(1,'Approved');
         INSERT lstinsertCase;
 
         //Inserting records into case object where status=Draft
         lstinsertCase=TestDataFactory.TestCaseData(1,'Draft');
         INSERT lstinsertCase;
 
         //Inserting records into case object where status=Rejected
         lstinsertCase=TestDataFactory.TestCaseData(1,'Rejected');
         INSERT lstinsertCase;
 
        //Start Test
         Test.startTest();
         List<ReferralHomeDashboard.Wrappercasecount> lstAllCOunt=ReferralHomeDashboard.getreferralhomeCases();
         Integer strRejectedcasescount=0,strpendingcasescount=0,strapprovedcasescount=0,strtotalcount=0;
         for(ReferralHomeDashboard.Wrappercasecount individualCount:lstAllCOunt)
         {
             System.assertEquals(false, individualCount.Rejectedcasescount == 10 , 'Rejected casecount size quals the inserted record');
             System.assertEquals(false, individualCount.pendingcasescount == 6 , 'Pending casecount size quals the inserted record');
             System.assertEquals(false, individualCount.approvedcasescount == 5 , 'Approved casecount size quals the inserted record');
             System.assertEquals(false, individualCount.totalcount == 21 , 'Toal casecount size quals the inserted record');
         }         
         //End Test
         Test.stopTest();
     }
}