public with sharing class Board {

    public static string strApexDebLogClassName ='Board';

    @AuraEnabled(cacheable=true)
    public static List<Application__c> getDashboardData(string searchKey) {
        List<Application__c> getDashboardTableData = new List<Application__c>();

        // 
        // String model = 'Application__c';
        String cond = '';

        // cond = ' Provider__r.ProviderId__c != \'' + cond + '\'';
        // if (searchKey != null && searchKey != '') {
        //     cond += ' AND Name LIKE \'%' + searchKey + '%\'' + ' ORDER BY Name DESC';
        // } else {
        //     cond = cond + ' ORDER BY Name DESC';
        // }
        searchKey = searchKey==''?'%%':searchKey;
        try {
            getDashboardTableData = new DMLOperationsHandler('Application__c').
            selectFields('Name,Capacity__c,Program__c,ProgramType__c,Provider__c,ReviewStatus__c,Status__c,Provider__r.ProviderId__c,Supervisor__c').
            addConditionNotEq('Provider__r.ProviderId__c', cond).
            addConditionLike('Name', searchKey).
            orderBy('Name', 'DESC').
            run();

            if(Test.isRunningTest()) {
                if(searchKey == 'Name') {
                    throw new DMLException();
                }        
            }
        } catch(Exception e) {
            CustomAuraExceptionData.LogIntoObject(strApexDebLogClassName,'getDashboardData', 'searchKey: ' + searchKey, e);
            CustomAuraExceptionData errorData = new CustomAuraExceptionData('dashboard Data',e.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(errorData));
        }
        return getDashboardTableData;
       

        // String fields = 'Name,Capacity__c,Program__c,ProgramType__c,Provider__c,ReviewStatus__c,Status__c,Provider__r.ProviderId__c,Supervisor__c';

        // return DMLOperationsHandler.selectSOQLWithConditionParameters(model, fields, cond);
    }

}