/**
 * @Author        : M.Vijayaraj
 * @CreatedOn     : April 17, 2020
 * @Purpose       : Application License Pdf generation.
 * @updatedBy     : 
 * @updatedOn     : 
 **/

 public with sharing class applicationLicenseInfo {
    @AuraEnabled(cacheable=false)
    public static List<SObject> getlicenseDetails(string licensedetails) {

        String model = 'License__c';
        String fields = 'Id, Name,ProgramName__c,ProgramType__c,StartDate__c,EndDate__c';
        String cond = 'Application__c =\''+ licensedetails+'\'';
        return DMLOperationsHandler.selectSOQLWithConditionParameters(model,fields,cond);
}
    
@AuraEnabled(cacheable=false)
        public static List<SObject> editlicenseDetails(string editlicensedetails) {

            List<SObject> pages = [select id,Provider__r.Name,Provider__r.BillingStreet,Provider__r.FirstName__c,Provider__r.LastName__c,Caseworker__r.Name,SubmittedDate__c,(select Id,Application__r.MinAge__c,Application__r.MaxAge__c,Application__r.Gender__c,Application__r.Capacity__c from Licences__r) ,
            (select  Id, Name, AddressType__c, AddressLine1__c from Address__r where AddressType__c='Site')
            from Application__c where Id=:editlicensedetails ]; 
            return pages;
        }
        @AuraEnabled
        public static string getSignatureAsBas64Image(string appID) {

            Map<Id, ContentDocumentLink> contentDocumentIdsmap = new Map<Id, ContentDocumentLink>();
            for(ContentDocumentLink conDocu : [SELECT ContentDocumentId, LinkedEntityId,LinkedEntity.Name FROM ContentDocumentLink WHERE LinkedEntityId =:appID  order by SystemModstamp desc LIMIT 1]){
                contentDocumentIdsmap.put(conDocu.ContentDocumentId, conDocu);
            }
          ContentVersion cv = [select id,ContentDocumentId,versiondata from Contentversion where ContentDocumentId In: contentDocumentIdsmap.keyset()  LIMIT 1];
          Blob base64Image =cv.VersionData;
          String base64ImageAsString=EncodingUtil.Base64Encode(base64Image);
          return base64ImageAsString;
        }
        
}