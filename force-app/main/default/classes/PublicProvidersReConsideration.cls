/**
 * @Author        : Saranraj V
 * @CreatedOn     : 
 * @Purpose       : Public Provider Reconsideration 
 **/
public with sharing class PublicProvidersReConsideration {
    public static string strApexDebLogClassName = 'PublicProvidersReConsideration';

    @AuraEnabled(cacheable=true)
    public static List<Reconsideration__c> getPublicProvidersReConsideration(String Id) {
        List<Reconsideration__c> reconsiderationData = new List<Reconsideration__c>();
        try {
            reconsiderationData = new DMLOperationsHandler('Reconsideration__c').
            selectFields('Id,Name,Status__c,ReconsiderationDate__c,ReconsiderationType__c, Provider__c').
            addConditionEq('Provider__c', Id).
            orderBy('ReconsiderationDate__c', 'Desc').
            run();
            if(Test.isRunningTest()) {
                if(Id == '') {
                    throw new DMLException();
                }  
            }
        } catch (Exception e) {
            CustomAuraExceptionData.LogIntoObject(strApexDebLogClassName,'getPublicProvidersReConsideration', 'Id: ' + Id, e);
            CustomAuraExceptionData errorData=new CustomAuraExceptionData('getPublicProvidersReConsideration Data',e.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(errorData));
        }

        // List<Reconsideration__c> reconsiderationData= new DMLOperationsHandler('Reconsideration__c').
        // selectFields('Id,Name,Status__c,ReconsiderationDate__c,ReconsiderationType__c, Provider__c').
        // addConditionEq('Provider__c', Id).
        // orderBy('ReconsiderationDate__c', 'Desc').
        // run();
        return reconsiderationData;
    }
}
