/**
     * @Author        : Sindhu
     * @CreatedOn     : July 15,2020
     * @Purpose       :Test Methods to unit test for ComplaintsMonitoringActivities.cls
     **/
    
    @isTest
    public with sharing class ComplaintsMonitoringConference_Test {
        @isTest static void ComplaintsMonitoringConference_Test() {
            
        Application__c InsertobjNewApplication=new Application__c(Status__c = 'Pending'); 
        insert InsertobjNewApplication;
        Account InsertobjNewAccount=new Account(Name = 'Test Account'); 
        insert InsertobjNewAccount;
        Contact InsertobjNewContact=new Contact(LastName = 'Account'); 
        insert InsertobjNewContact;
        Monitoring__c InsertobjNewMonitoring=new Monitoring__c(ApplicationLicenseId__c = InsertobjNewApplication.Id); 
        insert InsertobjNewMonitoring; 
        Conference__c InsertobjNewConference=new Conference__c(); 
        insert InsertobjNewConference;
        Actor__c InsertobjNewActor=new Actor__c(); 
        insert InsertobjNewActor; 
        try
        {
            ComplaintsMonitoringConference.assingStaffMember(null);
        }catch(Exception Ex)
        {
        }
        ComplaintsMonitoringConference.assingStaffMember(InsertobjNewAccount.Id);
        ComplaintsMonitoringConference.assingStaffMember(InsertobjNewAccount.Id);
        ComplaintsMonitoringConference.getStaffFromApplicationID(InsertobjNewApplication.Id);        
        ComplaintsMonitoringConference.getAllUsersList();
        ComplaintsMonitoringConference.getConferenceDetails(InsertobjNewMonitoring.Id);
        try{
            List<DMLOperationsHandler.FetchValueWrapper> objApplicationConferencePickList = ComplaintsMonitoringConference.getConferenceType(InsertobjNewConference,'Type__c');
        } catch (Exception e) {
        }
        ComplaintsMonitoringConference.getConferenceExitDetails(InsertobjNewConference.Id);
        ComplaintsMonitoringConference.InsertUpdateConference(InsertobjNewConference);
        ComplaintsMonitoringConference.getassignActors(InsertobjNewAccount.Id);
    }

    @isTest static void testgetMonitoringDetailsException()
    {
        ComplaintsMonitoringConference.getConferenceExitDetails('');
        ComplaintsMonitoringConference.assingStaffMember('');
        ComplaintsMonitoringConference.getStaffFromApplicationID(''); 
        ComplaintsMonitoringConference.getConferenceDetails('');
        ComplaintsMonitoringConference.getassignActors('');
    }

}

