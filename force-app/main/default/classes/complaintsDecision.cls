/**
 * Author   : Pratheeba.V
 * Date     : 3-7-2020
 */
public with sharing class complaintsDecision {
    public static string strClassNameForLogger='complaintsDecision';
    @AuraEnabled(cacheable =true)
    public static List<DMLOperationsHandler.FetchValueWrapper> getPickListValues(sObject objInfo, string picklistFieldApi) {
        return DMLOperationsHandler.fetchPickListValue(objInfo,picklistFieldApi);
    }

    @AuraEnabled(cacheable=true)
    public static List<User> getAllUsersList(String profileName) {
        List<User> tireApproverObj = new List<sObject>();
        Set<String> fields = new Set<String>{'Id','Name','isActive','Profile.Name','Availability__c','CaseLoad__c','ProfileId','Unit__c'};
        try {
            tireApproverObj = new DMLOperationsHandler('User').
            selectFields(fields).
            addConditionEq('isActive', true).
            addConditionEq('Profile.Name', profileName).
            run();
            if(Test.isRunningTest()) {
                if(profileName=='') {
                    throw new DMLException();
                }				
			}
        } catch (Exception Ex) { 
            String[] arguments = new String[] {profileName};
            string strInputRecord= String.format('Input parameters are :: profileNmae -- {0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getAllUsersList',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('All Users List',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return tireApproverObj;
    }

    @AuraEnabled
	public static string updatecasedetailsDraft(sObject objSobjecttoUpdateOrInsert){
        return DMLOperationsHandler.updateOrInsertSOQLReturnId(objSobjecttoUpdateOrInsert);
    }

    @AuraEnabled
    public static void saveSign(String strSignElement,Id recId){
        try {
            CommonUtils.saveSign(strSignElement,recId);
            if(Test.isRunningTest()) {
                if(strSignElement=='' || recId==null) {
                    throw new DMLException();
                }				
			}
        } catch (Exception Ex) {  
            String[] arguments = new String[] {strSignElement,recId};
            string strInputRecord= String.format('Input parameters are :: strSignElement -- {0} :: recId -- {1}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'saveSign',strInputRecord,Ex);          
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Signature Save',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }  
    }

    @AuraEnabled
	public static string getDatatableDetails(string caseid) {
        try {
            String model = 'ProcessInstance';
            String fields = 'Id, (SELECT Id, StepStatus, ActorId, Actor.Name, Actor.Profile.Name, Comments, CreatedDate,ProcessNodeId, ProcessNode.Name FROM StepsAndWorkitems ORDER BY Id DESC),(SELECT Id, ProcessInstanceId, ActorId, Actor.Name, Actor.Profile.Name, CreatedDate FROM Workitems),(SELECT Id, StepStatus, ActorId, Actor.Name, Actor.Profile.Name, Comments, CreatedDate FROM Steps ORDER BY CreatedDate DESC)';
            String cond = 'TargetObjectId =\'' + caseid + '\' ORDER BY CreatedDate DESC';
            List<SObject> lstApp= DMLOperationsHandler.selectSOQLWithConditionParameters(model, fields, cond);
            Map<Id, ContentDocumentLink> contentDocumentIdsmap = new Map<Id, ContentDocumentLink>();
            for(ContentDocumentLink conDocu : [SELECT ContentDocumentId, LinkedEntityId,LinkedEntity.Name FROM ContentDocumentLink WHERE LinkedEntityId =:caseid Order By SystemModstamp DESC]){
                contentDocumentIdsmap.put(conDocu.ContentDocumentId, conDocu);
            }
            List<ContentVersion> contelist = [SELECT Id, OwnerId, CreatedById FROM ContentVersion WHERE ContentDocumentId In: contentDocumentIdsmap.keyset() And IsSignature__c=true];
            Set<Id> filteredContOwner = new Set <Id>();
            Set<ContentVersion> filteredContList = new Set <ContentVersion>();
            for(ContentVersion clist : contelist){
                if(filteredContOwner.add(clist.OwnerId) == true)
                filteredContList.add(clist);
            }
            if(lstApp.size() > 0){
                List<processingWrapper> appWrap = new List<processingWrapper>();
                for(SObject app : lstApp) {
                    processingWrapper prdWrapper = new processingWrapper(app, filteredContList);
                    appWrap.add(prdWrapper);
                }
                return JSON.serialize(appWrap);
            } else {
                List<processingWrapperString> appWrap = new List<processingWrapperString>();
                String modelm = 'Case';
                String fieldsm = 'Id,SubmittedDate__c,Caseworker__r.Name,IntakeComments__c,IntakeDecisionStatus__c,Owner.Name,Supervisor__c,CaseworkerDecisionStatus__c,CaseworkerComments__c'; //add supervisor status and comments
                String condm = 'Id =\'' + caseid + '\'';
                List<SObject> monApp= DMLOperationsHandler.selectSOQLWithConditionParameters(modelm, fieldsm, condm);
                for(SObject app : monApp) {
                    if(contelist.size() > 0) {
                        for (contentversion conversion: contelist) {
                            processingWrapperString prdWrapper=new processingWrapperString(app, conversion.id);
                            appWrap.add(prdWrapper);
                        }
                    } else {
                        processingWrapperString prdWrapper=new processingWrapperString(app, '');
                        appWrap.add(prdWrapper);
                    }
                }
                return JSON.serialize(appWrap);
            }
        } catch (Exception Ex) {  
            String[] arguments = new String[] {caseid};
            string strInputRecord= String.format('Input parameters are :: caseid -- {0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getDatatableDetails',strInputRecord,Ex); 
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('History List',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
    }

    public class processingWrapper {
        public SObject cases {get;set;}
        public Set<ContentVersion> signatureUrl {get;set;}
        public processingWrapper(SObject app, Set<ContentVersion> url) {
            cases = app;
            this.signatureUrl = url;
        }
    }

    public class processingWrapperString {
        public SObject cases {get;set;}
        public String signatureUrl {get;set;}  
        public processingWrapperString(SObject app, String url) {
            cases = app;
            this.signatureUrl = url;
        }
    }
    
    @AuraEnabled
    public static void updatecasedetails (sObject objSobjecttoUpdateOrInsert, String nextApproverId, String workItemId, String profileName, String status){
        try {
            String comments = '';
            if(profileName == ProviderConstants.USER_PROFILE_SA) { 
                String Id = DMLOperationsHandler.updateOrInsertSOQLReturnId(objSobjecttoUpdateOrInsert);
                comments = (String) objSobjecttoUpdateOrInsert.get('IntakeComments__c');            
                Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                req1.setComments(comments);
                req1.setObjectId(Id);
                req1.setSubmitterId(UserInfo.getUserId());
                req1.setNextApproverIds(new Id[] {nextApproverId});
                Approval.ProcessResult result = Approval.process(req1);
            } else { 
                String Id = DMLOperationsHandler.updateOrInsertSOQLReturnId(objSobjecttoUpdateOrInsert);
                if(profileName==ProviderConstants.USER_PROFILE_SP) {
                    comments = (String) objSobjecttoUpdateOrInsert.get('SupervisorComments__c');
                } 
                if(profileName==ProviderConstants.USER_PROFILE_CW) {
                    comments = (String) objSobjecttoUpdateOrInsert.get('CaseworkerComments__c');
                }
                Approval.ProcessWorkitemRequest req1 = new Approval.ProcessWorkitemRequest();
                req1.setComments(comments);
                req1.setWorkitemId(workItemId);
                req1.setAction(status);
                if(nextApproverId!='')
                    req1.setNextApproverIds(new Id[] {nextApproverId});
                Approval.ProcessResult result = Approval.process(req1);
            }
        } catch (Exception Ex) {
            String[] arguments = new String[] {nextApproverId,workItemId,profileName,status};
            string strInputRecord= String.format('Input parameters are :: objSobjecttoUpdateOrInsert--{0} :: nextApproverId--{1} :: workItemId--{2} :: profileName--{3} :: status--{4}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'updatecasedetails',strInputRecord,Ex);            
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Case Update',Ex.getMessage(),CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        } 
    }

    @AuraEnabled(cacheable=false)
	public static List<Case> getReviewDetails(string caseid){
        List<Case> caseObj = new List<Case>();
        String fields = 'Id,SubmittedDate__c,Caseworker__r.Name,IntakeComments__c,IntakeDecisionStatus__c,Owner.Name,CaseworkerComments__c,CaseworkerDecisionStatus__c';
        try {
            caseObj = new DMLOperationsHandler('Case').
            selectFields(fields).
            addConditionEq('Id', caseid).
            run();
            if(Test.isRunningTest()) {
                if(caseid=='') {
                    throw new DMLException();
                }				
			}
        } catch (Exception Ex) {  
            String[] arguments = new String[] {caseid};
            string strInputRecord= String.format('Input parameters are :: case--{0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getReviewDetails',strInputRecord,Ex);            
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Review Details',Ex.getMessage() , CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return caseObj;
    }

    @AuraEnabled(cacheable=false)
	public static List<Deficiency__c> getDeficiencyDetails(string caseid){
        List<Deficiency__c> defObj = new List<Deficiency__c>();
        String fields = 'Id,Status__c,ProviderDecision__c';
        try {
            defObj = new DMLOperationsHandler('Deficiency__c').
            selectFields(fields).
            addConditionEq('Complaint__c', caseid).
            run();
            if(Test.isRunningTest()) {
                if(caseid=='') {
                    throw new DMLException();
                }				
			}
        } catch (Exception Ex) {  
            String[] arguments = new String[] {caseid};
            string strInputRecord= String.format('Input parameters are :: case--{0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getDeficiencyDetails',strInputRecord,Ex);            
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Deficiency Details',Ex.getMessage() , CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return defObj;
    }
    
    @AuraEnabled(cacheable=false)
	public static List<Case> getCaseworker(string caseid){
        List<Case> cwObj = new List<Case>();
        String fields = 'Id,Caseworker__c,AccountId';
        try {
            cwObj = new DMLOperationsHandler('Case').
            selectFields(fields).
            addConditionEq('Id', caseid).
            run();
            if(Test.isRunningTest()) {
                if(caseid=='') {
                    throw new DMLException();
                }				
			}
        } catch (Exception Ex) {  
            String[] arguments = new String[] {caseid};
            string strInputRecord= String.format('Input parameters are :: case--{0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getCaseworker',strInputRecord,Ex);            
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Caseworker Details',Ex.getMessage() , CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return cwObj;
    }

    @AuraEnabled(cacheable=false)
	public static List<Sanctions__c> getSanctionDetails(string caseid){
        List<Sanctions__c> cwObj = new List<Sanctions__c>();
        String fields = 'Id';
        try {
            cwObj = new DMLOperationsHandler('Sanctions__c').
            selectFields(fields).
            addConditionEq('Complaint__c', caseid).
            run();
            if(Test.isRunningTest()) {
                if(caseid=='') {
                    throw new DMLException();
                }				
			}
        } catch (Exception Ex) {  
            String[] arguments = new String[] {caseid};
            string strInputRecord= String.format('Input parameters are :: case--{0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getSanctionDetails',strInputRecord,Ex);            
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Sanctions Details',Ex.getMessage() , CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return cwObj;
    }

    @AuraEnabled(cacheable=false)
	public static List<Monitoring__c> getMonitoringDetails(string caseid){
        List<Monitoring__c> monitorObj = new List<Monitoring__c>();
        String fields = 'Id,Status__c';
        try {
            monitorObj = new DMLOperationsHandler('Monitoring__c').
            selectFields(fields).
            addConditionEq('Complaint__c', caseid).
            //addConditionEq('Status__c', 'Completed').
            run();
            if(Test.isRunningTest()) {
                if(caseid=='') {
                    throw new DMLException();
                }				
			}
        } catch (Exception Ex) {  
            String[] arguments = new String[] {caseid};
            string strInputRecord= String.format('Input parameters are :: case--{0}', arguments);
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getMonitoringDetails',strInputRecord,Ex);            
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Monitoring Details',Ex.getMessage() , CustomAuraExceptionData.type.Warning.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return monitorObj;
    }
}