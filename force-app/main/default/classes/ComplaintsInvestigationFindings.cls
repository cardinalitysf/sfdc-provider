/**
 * @Author        : Tamilarasan G
 * @CreatedOn     : July 3, 2020
 * @Purpose       : Complaints Investigation / Findings Dashboard AND Detail Page
**/
public with sharing class ComplaintsInvestigationFindings {
    
    public static string strClassNameForLogger='ComplaintsInvestigationFindings';

    //This function used to get all the investigation/finding details

   @AuraEnabled(cacheable=false)
   public static List<Deficiency__c> getInvestigationDetails(string investigationdetails) {

    List<sObject> reviewObj = new List<sObject>();
    try {
        String queryFields = 'Id, Name,Deficiency__c, FrequencyofDeficiency__c, ImpactofDeficiency__c, ScopeofDeficiency__c, Status__c, Citation__c, Findings__c, Comments__c,ProviderDecision__c,ProviderComments__c, Complaint__c, Complaint__r.CaseNumber, Complaint__r.CreatedDate, Complaint__r.Caseworker__r.Name, Complaint__r.Caseworker__r.Email, Complaint__r.Caseworker__r.Phone, Provider__r.Name,Provider__r.FirstName__c,Provider__r.LastName__c,Complaint__r.Address__r.AddressLine1__c,Complaint__r.Address__r.Email__c,Complaint__r.Address__r.Phone__c,Complaint__r.License__r.Name,Complaint__r.Supervisor__c';

        reviewObj = new DMLOperationsHandler('Deficiency__c').
        selectFields(queryFields).
        addConditionEq('Provider__c', investigationdetails).
        addConditionNotEq('Status__c', 'New').        
        orderBy('Name', 'Desc').
        run();
        if(Test.isRunningTest())
			{
                if(investigationdetails==null){
                    throw new DMLException();
                }
				
			}        
         
    } catch (Exception Ex) {

        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getInvestigationDetails',investigationdetails,Ex);

        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Provider Investigation Details',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
	    throw new AuraHandledException(JSON.serialize(oErrorData));
    }
       return reviewObj;
   }

       //This function used to get Sanction details

       @AuraEnabled(cacheable=false)
       public static List<Sanctions__c> getSupervisorResponse(string complaintId) {
    
        List<sObject> reviewObj = new List<sObject>();
        try {
            String queryFields = 'Id, RecordType.Name';
    
            reviewObj = new DMLOperationsHandler('Sanctions__c').
            selectFields(queryFields).
            addConditionEq('Complaint__c', complaintId).
            run();
            if(Test.isRunningTest())
			{
                if(complaintId==null){
                    throw new DMLException();
                }
				
			}              
             
        } catch (Exception Ex) {
    
            //To Log into object
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getSupervisorResponse',complaintId,Ex);
    
            //To return back to the UI
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Supervisor Response Details',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
           return reviewObj;
       }

   @AuraEnabled
   public static string InsertUpdateProviderDecision(sObject objSobjecttoUpdateOrInsert){
       return DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert);
   }

   @AuraEnabled(cacheable=true)
   public static List<ProcessInstance> getWorkItemId(string complaintId) {
    List<ProcessInstance> workItemId = new List<ProcessInstance>(); 
    try {
        workItemId= new DMLOperationsHandler('ProcessInstance').
        selectFields('Id').
        addSubquery(
            DMLOperationsHandler.subquery('Workitems').
            selectFields('Id, ProcessInstanceId, ActorId, Actor.Name, Actor.Profile.Name')
              ).
        addConditionEq('TargetObjectId', complaintId).
        run();
        if(Test.isRunningTest())
        {
            if(complaintId==null){
                throw new DMLException();
            }
            
        }           
            
       } catch (Exception Ex) {
            //To Log into object
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getWorkItemId',complaintId,Ex);
    
            //To return back to the UI
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Approval Process Work Item Id Missing',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
       }
       return workItemId;
   }

   @AuraEnabled
   public static void updateProviderApprovalProcess (String nextApproverId, String workItemId, String status,String comments){

    try {
        Approval.ProcessWorkitemRequest req1 = new Approval.ProcessWorkitemRequest();
        req1.setComments(comments);
        req1.setWorkitemId(workItemId);
        req1.setAction(status);
        req1.setNextApproverIds(new Id[] {nextApproverId});
        Approval.ProcessResult result = Approval.process(req1);


    } catch (Exception Ex) {
            String[] arguments = new String[] {nextApproverId , workItemId, status, comments};
            string strInputRecord= String.format('Input parameters are :: nextApproverId -- {0} :: workItemId -- {1}  :: status -- {2}  :: comments -- {3}', arguments);
    
            //To Log into object
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'updateProviderApprovalProcess',strInputRecord,Ex);
    
            //To return back to the UI
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Approval Process Failed',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        
    }
}

@AuraEnabled
    public static void saveSign(String strSignElement, Id recId) {
        try {
            CommonUtils.saveSign(strSignElement, recId);
        } catch (Exception ex) {
            //To Log into object
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'saveSign', 'Input Parameters are :: {0}' + strSignElement + ' :: {1} ' + recId, ex);
            //To return back to the UI
            CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Save Signature failed to load', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
    }

    @AuraEnabled(cacheable=true)
    public static List<contentversion>  getUploadedDocuments(String strDocumentID) {

        List<contentversion> getDocuments = new List<contentversion>();
        try {
            getDocuments =  CommonUtils.getUploadedDocuments(strDocumentID);
        } catch (Exception ex) {
            //To Log into object
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getUploadedDocuments', 'Document Id' + strDocumentID, ex);
            //To return back to the UI
            CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Documents Fetch Issues', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return getDocuments;
    }

    @AuraEnabled(cacheable=true)
    public static List<contentversion>  getLoginUserSignature(String strDocumentID) {

        List<contentversion> getSignature = new List<contentversion>();
        try {
            getSignature =  CommonUtils.getLoginUserSignature(strDocumentID);
        } catch (Exception ex) {
            //To Log into object
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getLoginUserSignature', 'Document Id' + strDocumentID, ex);
            //To return back to the UI
            CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Signature Fetch Issues', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        return getSignature;
    }    

}
