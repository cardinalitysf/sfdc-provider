global class JSON2Apex {

	global class Google {
		public Boolean expectUserResponse;
		public RichResponse richResponse;
	}

	public Payload payload;

	global class RichResponse {
		public List<Items> items;
	}

	global class SimpleResponse {
		public String textToSpeech;
	}

	global class Items {
		public SimpleResponse simpleResponse;
	}

	global class Payload {
		public Google google;
	}

	
	public static JSON2Apex parse(String json) {
		return (JSON2Apex) System.JSON.deserialize(json, JSON2Apex.class);
	}
}