/**
 * @Author        : Naveen S
 * @CreatedOn     : July 17 ,2020
 * @Purpose       : Test Methods to unit test complaintsMonitoringBoardInterview class
 **/
@IsTest
public with sharing class ComplaintsMonitoringBoardInterview_Test {
    @IsTest static void ComplaintsMonitoringBoardInterview_Test() {
                Application__c InsertobjNewApplication=new Application__c(Status__c = 'Pending'); 
                insert InsertobjNewApplication;
                ApplicationMonitoring.getMonitoringDetails(InsertobjNewApplication.Id);
                Monitoring__c InsertobjNewMonitoring=new Monitoring__c(ApplicationLicenseId__c = InsertobjNewApplication.Id); 
                insert InsertobjNewMonitoring;
                Address__c InsertobjNewAddress=new Address__c(); 
                insert InsertobjNewAddress; 
                Account InsertobjNewAccount=new Account(Name = 'Test Account'); 
                insert InsertobjNewAccount;
                ProviderRecordQuestion__c Prvrq = new ProviderRecordQuestion__c(Status__c = 'Pending',Monitoring__c=InsertobjNewMonitoring.Id);
                insert Prvrq;        
                ProviderRecordAnswer__c prvans = new ProviderRecordAnswer__c();    
                insert prvans;

                try
                {
                    complaintsMonitoringBoardInterview.getRecordDetails(null,null,null);
                }catch(Exception Ex)
                {
                    
                }
                complaintsMonitoringBoardInterview.getRecordDetails(InsertobjNewMonitoring.Id,InsertobjNewAddress.Id,'Id');
                try
                {
                    complaintsMonitoringBoardInterview.getSiteId(null);
                }catch(Exception Ex)
                {
                    
                }
                complaintsMonitoringBoardInterview.getSiteId(InsertobjNewAddress.Id);

                try
                {
                    complaintsMonitoringBoardInterview.getQuestionsByType(null);   
                }catch(Exception Ex)
                {
                    
                }
                complaintsMonitoringBoardInterview.getQuestionsByType('Web');  

                try
                {
                    complaintsMonitoringBoardInterview.applicationStaff(null);
                }catch(Exception Ex)
                {
                    
                }
                complaintsMonitoringBoardInterview.applicationStaff(InsertobjNewAccount.Id);

                try
                {
                    complaintsMonitoringBoardInterview.getStaffFromApplicationId(null);
                    
                }catch(Exception Ex)
                {
                    
                }
                complaintsMonitoringBoardInterview.getStaffFromApplicationId(InsertobjNewApplication.Id);
                
                complaintsMonitoringBoardInterview.getDataForTable();                    
                
               
                complaintsMonitoringBoardInterview.getRecordType('Board Interview');
                try
                {
                    complaintsMonitoringBoardInterview.editRecordData(null);
                    
                }catch(Exception Ex)
                {
                    
                }
                complaintsMonitoringBoardInterview.editRecordData('Id');        

                string str = '[{"Comar__c":"","Comments__c":"Test Comments","Profit__c":"","ProviderRecordQuestion__c":"'+Prvrq.Id+'"}]';
                string strUpdate = '[{"Id":"'+prvans.Id+'","Comments__c":"Test Comments","Profit__c":"","ProviderRecordQuestion__c":"'+Prvrq.Id+'"}]';
                       
                try
                {
                    complaintsMonitoringBoardInterview.bulkAddRecordAns(null);
                    
                }catch(Exception Ex)
                {
                    
                }
                complaintsMonitoringBoardInterview.bulkAddRecordAns(str);
                try
                {
                    complaintsMonitoringBoardInterview.editRecordData(null);
                    
                }catch(Exception Ex)
                {
                   
                }
                complaintsMonitoringBoardInterview.editRecordData('Id');
                try
                {
                    complaintsMonitoringBoardInterview.bulkUpdateRecordAns(null);
                    
                }catch(Exception Ex)
                {
                }
                complaintsMonitoringBoardInterview.bulkUpdateRecordAns(strUpdate);
                try
                {
                complaintsMonitoringBoardInterview.getProviderType(null);
                }
                catch(Exception Ex)
                {
                    
                }
                complaintsMonitoringBoardInterview.getProviderType(InsertobjNewAccount.Id);
                try
                {
                complaintsMonitoringBoardInterview.getHouseholdMembers(null);
                }
                catch(Exception Ex)
                {
                    
                }
                complaintsMonitoringBoardInterview.getHouseholdMembers(InsertobjNewAccount.Id);
    }
}