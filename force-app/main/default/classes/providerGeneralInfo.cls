/**
 * @Author        : Naveen S
 * @CreatedOn     : April 21 ,2020
 * @Purpose       : General Information of Provider
 * @updatedBy     :
 * @updatedOn     :
 **/

public with sharing class providerGeneralInfo {
    public static string strClassNameForLogger='providerGeneralInfo';
    
        @AuraEnabled(cacheable=false)
        public static List<Account> getProviderDetails(string providerID) {
            try{
            Set<String> fields = new Set<String>{'Name,FirstName__c, LastName__c,RFP__c, TaxId__c, SON__c, Corporation__c, ParentCorporation__c,Email__c,CellNumber__c,Phone,Fax'};
            List<Account> accounts =
            new DMLOperationsHandler('Account').
            selectFields(fields).
            addConditionEq('Id', providerID).
            run();
            if(Test.isRunningTest())
                {
                if(providerID==null ){
                throw new DMLException();
                }
                }
            return accounts;
            }catch (Exception Ex) {
                CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getProviderDetails',providerID,Ex);
                CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Provider Details',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
                throw new AuraHandledException(JSON.serialize(oErrorData));
               }
        }  

        @AuraEnabled(cacheable=false)
        public static List<Address__c> fetchDataForAddress(string fetchdataname,string fetchdatatype) {
            try{
        Set<String> fields = new Set<String>{'Id, AddressType__c, AddressLine1__c, AddressLine2__c, City__c, County__c, Email__c, Phone__c, State__c, ZipCode__c,Application__c'};
        List<Address__c> addresses =
        new DMLOperationsHandler('Address__c').
        selectFields(fields).
        addConditionEq('Provider__c', fetchdataname).
        addConditionEq('AddressType__c', fetchdatatype).
        run();
        if(Test.isRunningTest())
        {
        if(fetchdataname==null || fetchdatatype== null ){
        throw new DMLException();
        }            
        }
        return addresses;
    } catch (Exception Ex) {
        String[] arguments = new String[] {fetchdataname , fetchdatatype};
        string strInputRecord= String.format('Input parameters are :: fetchdataname -- {0} :: fetchdatatype -- {1} ', arguments);
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'fetchDataForAddress',strInputRecord,Ex);
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Address Based on Address Type',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
}
        }   
 
        @AuraEnabled(cacheable=false)
        public static List<Application__c> getApplicationDetails(string providerId) {
            try{
            Set<String> fields = new Set<String>{'Id,Name,Program__c,ProgramType__c'};
            List<Application__c> applications =
            new DMLOperationsHandler('Application__c').
            selectFields(fields).
            addConditionEq('Provider__c', providerId).
            run();
            if(Test.isRunningTest())
                {
                if(providerId==null ){
                throw new DMLException();
                }
                }
            return applications;
            }catch (Exception Ex) {
                CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getApplicationDetails',providerId,Ex);
                CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Get Application details  from Provider ID',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
                throw new AuraHandledException(JSON.serialize(oErrorData));
               }
        }
    

}