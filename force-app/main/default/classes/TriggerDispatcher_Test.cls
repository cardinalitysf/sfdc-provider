/**
 * @Author        : Pratheeba V
 * @CreatedOn     : June 11 ,2020
 * @Purpose       : Test class for TriggerDispatcher
 **/
@isTest
private class TriggerDispatcher_Test{
  static testMethod void test_Run_UseCase1(){
    Test.startTest();
    Training__c InsertTrainingObj = new Training__c();
    Insert InsertTrainingObj;
    SessionAttendee__c sessionattendee_Obj = new SessionAttendee__c( TotalHoursAttended__c = 21,Training__c=InsertTrainingObj.Id);
    Insert sessionattendee_Obj;
    System.assertEquals(21,sessionattendee_Obj.TotalHoursAttended__c);
    SessionAttendee__c sessionattendee_Obj1=[select Id,TotalHoursAttended__c from SessionAttendee__c where Id=:sessionattendee_Obj.Id];
    sessionattendee_Obj1.TotalHoursAttended__c=27;
    sessionattendee_Obj1.id=sessionattendee_Obj.Id;
    update sessionattendee_Obj1;
    System.assertEquals(27,sessionattendee_Obj1.TotalHoursAttended__c);
    delete sessionattendee_Obj;
    System.assertEquals(21,sessionattendee_Obj.TotalHoursAttended__c);
    undelete sessionattendee_Obj;
    System.assertEquals(21,sessionattendee_Obj.TotalHoursAttended__c);
    Test.stopTest();
  }

  static testMethod void test_isDisabled_UseCase1(){
    List<SessionAttendee__c> task_Obj  =  [select Id,TotalHoursAttended__c from SessionAttendee__c];
    System.assertEquals(false,task_Obj.size()>0);
    SessionAttendeeTriggerHandler obj01 = new SessionAttendeeTriggerHandler();
    obj01.isDisabled();
  }
}