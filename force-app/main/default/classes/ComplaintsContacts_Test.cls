/**
     * @Author: Janaswini S
     * @CreatedOn: July 16, 2020
     * @Purpose: Test Methods to unit test for Complaints Contact Details Add/View/Delete Component
**/

@isTest
public with sharing class ComplaintsContacts_Test {

    @isTest static void testContactNote(){
        ContactNotes__c InsertobjNewCN = new ContactNotes__c(ContactType__c = 'test');
        insert InsertobjNewCN;
        Case InsertobjNewCase= new Case(Status = 'pending');
        insert InsertobjNewCase;
        
        try
        {
        ComplaintsContacts.fetchContactDetails(null);
        }
        
        catch(Exception ex)
           {
               
           }
           ComplaintsContacts.fetchContactDetails(InsertobjNewCase.Id);
           try{
            ComplaintsContacts.editContactDetails(null);
           }
        
        catch(Exception ex)
        {
            
        }
        ComplaintsContacts.editContactDetails(InsertobjNewCN.Id);
        try{
            ComplaintsContacts.getApplicationProviderStatus(null);
           }
        
        catch(Exception ex)
        {
            
        }
        ComplaintsContacts.getApplicationProviderStatus(InsertobjNewCase.Id);
        try{
            Map<String, List<DMLOperationsHandler.FetchValueWrapper>>  objgetProfitValues = ComplaintsContacts.getMultiplePicklistValues(InsertobjNewCase.Id,'XXX');
          }
         catch(Exception e){
                   }
      
        
        }

        @isTest static void testContactNoteException()
        {   
            ComplaintsContacts.fetchContactDetails('');
            ComplaintsContacts.editContactDetails(''); 
            ComplaintsContacts.getApplicationProviderStatus('');       
        }
}
