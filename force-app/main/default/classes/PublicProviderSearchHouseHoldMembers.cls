/**
 * @Author        : G.Tamilarasan
 * @CreatedOn     : May 13, 2020
 * @Purpose       : This component contains Search Household Members
 **/
public with sharing class PublicProviderSearchHouseHoldMembers {
    public static string strClassNameForLogger='PublicProviderSearchHouseHoldMembers';

//This function used to search the household members
    @AuraEnabled(cacheable=false)
    public static List<Contact> getSearchHoldMembers(string searchdetails) {

        String cond;
        String model;
        String fields;
        try {
            
            cond = '';
            if (searchdetails != null && searchdetails != '') {
                cond ='RecordType.Name =\'Household Members\''+ searchdetails;
            }
            
    
           model = 'Contact';
           fields = 'LastName__c, FirstName__c, Id, Gender__c, DOB__c, SSN__c, ContactNumber__c,CHESSIEID__c, MDMID__c, History__c';
           if(Test.isRunningTest())
           {
               if(searchdetails==''){
                   throw new DMLException();
               }
               
           } 
        } catch (Exception Ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getSearchHoldMembers',searchdetails,Ex);

        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Household Member Search Details',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
	    throw new AuraHandledException(JSON.serialize(oErrorData));              
        }

       return DMLOperationsHandler.selectSOQLWithConditionParameters (model, fields, cond);
    }


}
