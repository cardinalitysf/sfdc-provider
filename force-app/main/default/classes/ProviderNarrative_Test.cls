/*
     Author         : G.sathishkumar
     @CreatedOn     : may 11 ,2020
     @Purpose       : test class for Provider & Contract Narrative Details .
     @Updated By    : Sindhu
     @UpdatedOn     : 20 july,2020
     
*/

@isTest
private  class ProviderNarrative_Test {
    @isTest static void testProviderNarrativeTest() {

        Account InsertobjNewAccount=new Account(Name = 'XYZ'); 
        insert InsertobjNewAccount;

        Contract__c InsertobjNewContract=new Contract__c(Narrative__c = 'ABC'); 
        insert InsertobjNewContract;

        HistoryTracking__c InsertobjNewHistory=new HistoryTracking__c(RichOldValue__c = 'ABCD'); 
        insert InsertobjNewHistory;

        try{
            providerNarrative.getAccountNarrativeDetails(null);
          }catch(Exception Ex){

            }
        providerNarrative.getAccountNarrativeDetails(InsertobjNewAccount.Id);
        try{
            providerNarrative.getContractNarrativeDetails(null);
          }catch(Exception Ex){

            }
        providerNarrative.getContractNarrativeDetails(InsertobjNewContract.Id);
         string applicationId= providerNarrative.updateNarrativeDetails(InsertobjNewContract);
         try{
            providerNarrative.getHistoryOfNarrativeDetails(null);
          }catch(Exception Ex){

            }
         providerNarrative.getHistoryOfNarrativeDetails(InsertobjNewHistory.Id);
         try{
            providerNarrative.getSelectedHistoryRec(null);
          }catch(Exception Ex){

            }
         providerNarrative.getSelectedHistoryRec(InsertobjNewHistory.Id);
         try{
            providerNarrative.getContractStatus(null);
          }catch(Exception Ex){

            }
         
         providerNarrative.getContractStatus(InsertobjNewContract.Id);
    
        
    }
}