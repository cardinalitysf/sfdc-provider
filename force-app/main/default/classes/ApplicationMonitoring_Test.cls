/**
 * @Author        : Pratheeba.V
 * @CreatedOn     : May 7 ,2020
 * @Purpose       : Test Methods to unit test ApplicationMonitoring class
 **/
@IsTest
private class ApplicationMonitoring_Test {    
    @IsTest static void testgetMonitoringDetailsPositive()
    {
        Application__c InsertobjNewApplication=new Application__c(Status__c = 'Pending'); 
        insert InsertobjNewApplication;
        Monitoring__c InsertobjNewMonitoring=new Monitoring__c(ApplicationLicenseId__c = InsertobjNewApplication.Id); 
        insert InsertobjNewMonitoring;  
        try {
            ApplicationMonitoring.getMonitoringDetails(InsertobjNewApplication.Id);
        } catch (Exception e) { }
        try {
            ApplicationMonitoring.InsertUpdateMonitoring(InsertobjNewMonitoring); 
        } catch (Exception e) { }
        try {
            ApplicationMonitoring.getAddressDetails(InsertobjNewApplication.Id);
        } catch (Exception e) { }
        try {
            ApplicationMonitoring.getProgramDetails(InsertobjNewApplication.Id);
        } catch (Exception e) { }
        try {
            ApplicationMonitoring.editMonitoringDetails(InsertobjNewMonitoring.Id);
        } catch (Exception e) { }
        try {
            ApplicationMonitoring.viewMonitoringDetails(InsertobjNewMonitoring.Id);
        } catch (Exception e) { }
        try {
            ApplicationMonitoring.getApplicationProviderStatus(InsertobjNewApplication.Id);
        } catch (Exception e) { }
        try {
            ApplicationMonitoring.getMonitoringStatus(InsertobjNewMonitoring.Id);
        } catch (Exception e) { }
        try {
            ApplicationMonitoring.getApplicationLicenseStatus(InsertobjNewApplication.Id);
        } catch (Exception e) { }            
    }

    @isTest static void testgetMonitoringDetailsException()
    { 
        try {
            ApplicationMonitoring.getMonitoringDetails('');
        } catch (Exception e) { }   
        try {
            ApplicationMonitoring.getAddressDetails('');
        } catch (Exception e) { }       
        try {
            ApplicationMonitoring.getProgramDetails('');
        } catch (Exception e) { } 
        try {
            ApplicationMonitoring.editMonitoringDetails('');
        } catch (Exception e) { }     
        try {
            ApplicationMonitoring.viewMonitoringDetails('');
        } catch (Exception e) { }     
        try {
            ApplicationMonitoring.getApplicationProviderStatus('');
        } catch (Exception e) { }             
        try {
            ApplicationMonitoring.getMonitoringStatus(''); 
        } catch (Exception e) { } 
        try {
            ApplicationMonitoring.getApplicationLicenseStatus('');  
        } catch (Exception e) { }         
    }
}