@isTest
private class contractTriggerHandler_Test{
  @testSetup
  static void setupTestData(){
    test.startTest();
    //Contract__c contract_Obj = new Contract__c(IsSignature__c = false, ReviewStatus__c = 'None',Narrative__c='Narrative', Status__c = 'Active', SubmittedDate__c = Date.today());
    
      Account account_Obj = new Account(Name = 'Test',Status__c='Active');
      //insert account_Obj;
      Database.UpsertResult Upsertresults1 = Database.upsert(account_Obj);
        id strProviderId= Upsertresults1.getId();
      
     List<Contract__c> contract_Obj = new List<Contract__c>{
    new Contract__c(IsSignature__c = false,Provider__c=strProviderId, ReviewStatus__c = 'None',ContractStatus__c='Inactive',Narrative__c='Narrative', Status__c = 'Active', SubmittedDate__c = Date.today()),
    new Contract__c(IsSignature__c = false,Provider__c=strProviderId, ReviewStatus__c = 'None',ContractStatus__c='Inactive',Narrative__c='Narrative', Status__c = 'Active', SubmittedDate__c = Date.today()),
    new Contract__c(IsSignature__c = false,Provider__c=strProviderId, ReviewStatus__c = 'None',ContractStatus__c='Inactive',Narrative__c='Narrative', Status__c = 'Active', SubmittedDate__c = Date.today())
      };  
   
          
     Insert contract_Obj;
    HistoryTracking__c historytracking_Obj = new HistoryTracking__c(CreatedDate__c = Date.today(), FieldName__c = '14', NewValue__c = '15', ObjectName__c = 'Contract__c', ObjectRecordId__c = '17', RichOldValue__c = '18', RichNewValue__c = '19');
    Insert historytracking_Obj;
    test.stopTest();
  }
    
    

  static testMethod void test_afterupdate_UseCase1(){
 List<Contract__c> Contract_Obj  =  [SELECT Id,IsSignature__c,Narrative__c,ReviewStatus__c,Status__c,SubmittedDate__c from Contract__c];
 List<Contract__c> Contract_Obj1  =  [SELECT Id,IsSignature__c,Narrative__c,ReviewStatus__c,Status__c,SubmittedDate__c from Contract__c];
 System.assertEquals(true,Contract_Obj.size()>0);

 Map<Id, Contract__c> newContractMap = new Map<Id, Contract__c>();   
 Map<Id, Contract__c> oldContractMap = new Map<Id, Contract__c>();   
 
 newContractMap.putAll(Contract_Obj);
 oldContractMap.putAll(Contract_Obj1);
 for(id oid : oldContractMap.keySet()){
          oldContractMap.get(oid).Narrative__c='Changed Narrative';
      }     
    contractTriggerHandler obj01 = new contractTriggerHandler();
    contractTriggerHandler.afterupdate(newcontractMap,oldContractMap);
     

  }
  static testMethod void test_afterinsert_UseCase1(){
    List<Contract__c> contract_Obj  =  [SELECT Id,IsSignature__c,Narrative__c,ReviewStatus__c,Status__c,SubmittedDate__c from Contract__c];
    System.assertEquals(true,contract_Obj.size()>0);
    contractTriggerHandler obj01 = new contractTriggerHandler();
    contractTriggerHandler.afterinsert(contract_Obj);
     
     List<HistoryTracking__c> historytracking_Obj  =  [SELECT Id,CreatedDate__c,FieldName__c,NewValue__c,ObjectName__c,ObjectRecordId__c,RichOldValue__c,RichNewValue__c from HistoryTracking__c];
    System.assertEquals(true,historytracking_Obj.size()>0);
    contractTriggerHandler obj02 = new contractTriggerHandler();
    contractTriggerHandler.afterinsert(new List<Contract__c>());
  }
  static testMethod void test_updateProviderStatusInAccount_UseCase1(){    
   List<Contract__c> Contract_Obj  =  [SELECT Id,Provider__c,IsSignature__c,ContractStatus__c,Narrative__c,ReviewStatus__c,Status__c,SubmittedDate__c from Contract__c];
  List<Contract__c> Contract_Obj1  =  [SELECT Id,Provider__c,IsSignature__c,ContractStatus__c,Narrative__c,ReviewStatus__c,Status__c,SubmittedDate__c from Contract__c];
  System.assertEquals(true,Contract_Obj.size()>0);

  Map<Id, Contract__c> newContractMap = new Map<Id, Contract__c>();   
  Map<Id, Contract__c> oldContractMap = new Map<Id, Contract__c>();   
 
 newContractMap.putAll(Contract_Obj);
 oldContractMap.putAll(Contract_Obj1);
 for(id oid : newContractMap.keySet()){
          newContractMap.get(oid).ContractStatus__c='ACTIVE';
      }   

    //  contractTriggerHandler obj01 = new contractTriggerHandler();
    contractTriggerHandler.updateProviderStatusInAccount(newcontractMap,oldContractMap);
}
}