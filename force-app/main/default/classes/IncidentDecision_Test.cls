/**
     * @Author        : Jayachandran s
     * @CreatedOn     : Augest 18,2020
     * @Purpose       : Test Methods to unit test for IncidentDecision.cls
**/

@isTest
public with sharing class IncidentDecision_Test {
    @isTest static void testIncidentDecision (){
        Case InsertobjNewCase= new Case(Status = 'Pending');
        insert InsertobjNewCase;
        Profile prf = [Select Id, Name from Profile where Name= 'Caseworker'];
        List<User> usrList = TestDataFactory.createTestUsers(2, prf.Id,true);
        List<Account> lstAcct= TestDataFactory.testAccountData();
        insert lstAcct;
        List<Case> csList = TestDataFactory.TestCaseData(5,'Approved');
        List<Case> cList =new List<Case>();
        for(Case cs: csList)
        {
            cs.AccountId = lstAcct[0].Id;
            cList.add(cs);
        }       
        insert cList;
        List<ContentVersion> cvList = TestDataFactory.createTestContentVersionData();
        insert cvList;
        SObject sobj = (SObject)Type.forName('ContentVersion').newInstance();
        IncidentDecision.processingWrapperString appParse1= new IncidentDecision.processingWrapperString(sobj,'test');
        String str = 'Case';
        sObject obj = Schema.getGlobalDescribe().get(str).newSObject();
        obj.put('Origin', 'Email');
        obj.put('Description', 'Test Comments');
        obj.put('Status', 'Submitted to Caseworker');
        obj.put('Id', cList[0].Id);
        obj.put('AccountId', lstAcct[0].Id);
        obj.put('ProgramType__c','');
        obj.put('Program__c','');

        IncidentDecision.updateIncidentdetailsDraft(InsertobjNewCase);

        try{
            List<DMLOperationsHandler.FetchValueWrapper>  objgetPickListValues = IncidentDecision.fetchPickListValue(InsertobjNewCase,'Origin');
           }
           catch (Exception ex) { }

        try{
            IncidentDecision.saveSign(null,null);
            }catch(Exception Ex){ }                
        IncidentDecision.saveSign('test',InsertobjNewCase.Id);

        try {
            IncidentDecision.updatecasedetails(obj,usrList[0].Id,'','Approve');
        } catch(Exception Ex) { }

        try {
            IncidentDecision.getDatatableDetails(obj.Id);    
        } catch(Exception Ex) { }

        try{
            IncidentDecision.getReviewDetails(null);
        }catch(Exception Ex) { }
        IncidentDecision.getReviewDetails(InsertobjNewCase.Id);

        try{
            IncidentDecision.getReferralData(null);
        }catch(Exception Ex) { }
        IncidentDecision.getReferralData(InsertobjNewCase.Id);

        try{
            IncidentDecision.getActorDetails(null);
        }catch(Exception Ex) { }
        IncidentDecision.getActorDetails(InsertobjNewCase.Id);

        try{
            IncidentDecision.getWitnessDetails(null);
        }catch(Exception Ex) { }
        IncidentDecision.getWitnessDetails(InsertobjNewCase.Id);

        try{
            IncidentDecision.getIncidentPersonChildDetails(null);
        }catch(Exception Ex) { }
        IncidentDecision.getIncidentPersonChildDetails(InsertobjNewCase.Id);
    }

    @isTest static void testIncidentDecisionException(){
        Case InsertobjCase=new Case(Status = 'Pending'); 
        insert InsertobjCase;
        Profile prf = [Select Id, Name from Profile where Name= 'Caseworker'];
        List<User> usrList = TestDataFactory.createTestUsers(2, prf.Id,true);
        List<Account> lstAcct= TestDataFactory.testAccountData();
        insert lstAcct;
        List<Case> csList = TestDataFactory.TestCaseData(5,'Approved');
        List<Case> cList =new List<Case>();
        for(Case cs: csList)
        {
            cs.AccountId = lstAcct[0].Id;
            cList.add(cs);
        }       
        insert cList;
        List<ContentVersion> cvList = TestDataFactory.createTestContentVersionData();
        insert cvList;
        SObject sobj = (SObject)Type.forName('ContentVersion').newInstance();
        IncidentDecision.processingWrapperString appParse1= new IncidentDecision.processingWrapperString(sobj,'test');
        String str = 'Case';
        sObject obj = Schema.getGlobalDescribe().get(str).newSObject();
        obj.put('Origin', 'Email');
        obj.put('Description', 'Test Comments');
        obj.put('Status', 'Approved');
        obj.put('Id', cList[0].Id);
        obj.put('AccountId', lstAcct[0].Id);
        obj.put('ProgramType__c','');
        obj.put('Program__c','');
        
        try {
            IncidentDecision.getDatatableDetails('');    
        } catch(DMLException Ex){
         }
    }

    @IsTest static void testcaseNegative3()
    {
        Profile prf = [Select Id, Name from Profile where Name= 'Caseworker'];
        List<User> userList =  TestDataFactory.createTestUsers(1, prf.Id, true);
        Account acc = new Account(Name='Test');
        insert acc;
        Reconsideration__c recon = new Reconsideration__c();
        insert recon;
        Id MyId2=Schema.Sobjecttype.Case.getRecordTypeInfosByName().get('Incident').getRecordTypeId();
        Case InsertobjCase=new Case(RecordTypeId = MyId2,AccountId = acc.Id); 
        insert InsertobjCase;
      
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments('Submitting request for approval.');
        req.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        req.setObjectId(InsertobjCase.Id);
        Approval.ProcessResult resu = Approval.process(req);
        System.assert(resu.isSuccess());
        System.assertEquals('Pending', resu.getInstanceStatus(),'Instance Status'+resu.getInstanceStatus());
        List<Id> newWorkItemIds = resu.getNewWorkitemIds();
        IncidentDecision.updatecasedetails(InsertobjCase,userList[0].Id,newWorkItemIds.get(0),'Approve');
        try {
            IncidentDecision.getDatatableDetails(InsertobjCase.Id);    
        } catch(Exception Ex) { } 
    }

    @IsTest static void testcaseNegative2(){
        Case InsertobjNewCase= new Case(Status = 'Pending');
        insert InsertobjNewCase;

        try {
            IncidentDecision.getDatatableDetails(InsertobjNewCase.CaseNumber);    
        } catch(Exception Ex) { } 
    }

}
