/**
	 * @Author        : S. Jayachandran and Tamil
	 * @CreatedOn     : April 24 ,2020
	 * @Purpose       : Responsible for fetch or insert the Task Object(Activity Sobj).
     * @UpdatedBy     :Jayachnadran s For Updation test class 
     * Date           :July 20, 2020  
	 **/
public with sharing class ProvidersContractAddLicenseInfo {
    public static string strClassNameForLogger='ProvidersContractAddLicenseInfo';

    @AuraEnabled(cacheable=true)
    public static List<Application__c> ProgramList(String ProviderId) {
    Set<String> fields = new Set<String>{'Program__c','id','Name'};
    List<Application__c> OBJ =new List<Application__c>();
    try {
        OBJ =
        new DMLOperationsHandler('Application__c').
        selectFields(fields).
        addConditionEq('Provider__c', ProviderId).
        addConditionEq('Status__c', 'Approved').
        run();
        if(Test.isRunningTest())
            {
                if(ProviderId==null){
                    throw new DMLException();
              }
             }
    } catch (Exception ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'ProgramList', 'ProviderId' + ProviderId, ex);
        //To return back to the UI
        CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Record Type Fetch Issue,Please Try Again..!', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
    }
       return OBJ;
    }
    
    @AuraEnabled(cacheable=false)
    public static List<Contract__c> providerContract(string contract) {
        Set<String> fields = new Set<String>{'Id','Name','contractStatus__c','License__c'};
        List<Contract__c> contOBJ = new List<Contract__c>();
        try {
            contOBJ =
            new DMLOperationsHandler('Contract__c').
            selectFields(fields).
            addConditionEq('Provider__c', contract).
            addConditionNotEq('contractStatus__c', 'New').
            run();  
            if(Test.isRunningTest())
            {
                if(contract==null){
                    throw new DMLException();
              }
             }
        } catch (Exception ex) {
            //To Log into object
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'providerContract', 'contract' + contract, ex);
            //To return back to the UI
            CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Record Type Fetch Issue,Please Try Again..!', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
        
        return contOBJ;
    }

    @AuraEnabled(cacheable=false)
    public static List<Application__c> getLicensesId(string applicationID) {    
    List<String> lstapplicationID = applicationID.split(','); 
    List<Application__c> listApplication= new List<Application__c>();
        try {
             listApplication= new DMLOperationsHandler('Application__c').
                           selectFields('Name').
                           selectFields('Id').
                           selectFields('Provider__r.Name').
                           addSubquery(
                             DMLOperationsHandler.subquery('Licences__r').
                             selectFields('Id, Name, ProgramName__c,ProgramType__c,StartDate__c,EndDate__c')).
                           addSubquery(DMLOperationsHandler.subquery('Address__r').
                                selectFields('Id,Name,AddressType__c')).
                                addConditionIn('Id',lstapplicationID).
                                run(); 
                                if(Test.isRunningTest())
                                  {
                                   if(applicationID== ''){
                                    throw new DMLException();
                                      }
                                  }  
        
        }  catch (Exception ex) {
            //To Log into object
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getLicensesId', 'applicationID' + applicationID, ex);
            //To return back to the UI
            CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Record Type Fetch Issue,Please Try Again..!', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }
         
           return listApplication;
      }

    @AuraEnabled
    public static Id getIdAfterInsertSOQL(sObject objIdSobjecttoUpdateOrInsert) {
        return DMLOperationsHandler.getIdAfterInsertSOQL(objIdSobjecttoUpdateOrInsert);
    }

    @AuraEnabled(cacheable=false)
    public static List<IRCRates__c> getContractBedDetails(string licenseID) {
        List<IRCRates__c> listIRC= new List<IRCRates__c>();
    try {
    listIRC= new DMLOperationsHandler('IRCRates__c').
    selectFields('Id').
    selectFields('IRCBedCapacity__c').
    addConditionEq('LicenseType__c', licenseID).
    addConditionEq('Status__c', 'Active').
    run();
    if(Test.isRunningTest())
            {
                if(licenseID==null){
                    throw new DMLException();
              }
             }
    } catch (Exception ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getContractBedDetails', 'licenseID' + licenseID, ex);
        //To return back to the UI
        CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Record Type Fetch Issue,Please Try Again..!', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
    }
    
    return listIRC;
    }

    @AuraEnabled(cacheable=false)
    public static List<Contract__c> getContractDetails(string contractdetails) {
    Set<String> fields = new Set<String>{'Id', 'CreatedDate','Name','ReceivedDate__c','LicensingAgency__c','ContractingAgency__c','SolicitationType__c','ContractStatus__c','ContractType__c','TotalContractBed__c','ContractTermType__c','ContractStartDate__c','ContractEndDate__c','TotalContractAmount__c','BudgetCodeType__c','BudgetCode__c','Notes__c','FiscalAmount__c'};
    List<Contract__c> listBedCount= new List<Contract__c>();
    try {
        listBedCount= new DMLOperationsHandler('Contract__c').
        selectFields(fields).
        addConditionEq('Id', contractdetails).
        run();
        if(Test.isRunningTest())
            {
                if(contractdetails==null){
                    throw new DMLException();
              }
             }
        
    } catch (Exception ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger, 'getContractDetails', 'contractdetails' + contractdetails, ex);
        //To return back to the UI
        CustomAuraExceptionData oErrorData = new CustomAuraExceptionData('Record Type Fetch Issue,Please Try Again..!', Ex.getMessage(), CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
    }
   
    return listBedCount;
    }

    @AuraEnabled
    public static string InsertUpdateContract(sObject objSobjecttoUpdateOrInsert){
        return DMLOperationsHandler.updateOrInsertSOQL(objSobjecttoUpdateOrInsert);
    }
}