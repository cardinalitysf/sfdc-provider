/*
     Author         : G.sathishkumar
     @CreatedOn     : june 09 ,2020
     @Purpose       : test class for MeetingInfoTriggerHandler_Test Details .
*/

@isTest
private class MeetingInfoTriggerHandler_Test{
  @testSetup
  static void setupTestData(){
    test.startTest();
      Case ocase= new Case();
        oCase.Agency__c='';
        Database.UpsertResult Upsertresults1 = Database.upsert(ocase);
        id strReferaalId= Upsertresults1.getId();
     
       Case ocase2= new Case();
        oCase2.Agency__c='';
        Database.UpsertResult Upsertresults2 = Database.upsert(ocase2);
        id strReferaalId2= Upsertresults2.getId();
     
        Training__c TrainingInfo_Obj3 = new Training__c(TrainingName__c = 'Training2', TrainingHours__c =27);
        Database.UpsertResult Upsertresults3 = Database.upsert(TrainingInfo_Obj3);
        id strTrainingId3= Upsertresults3.getId();
     
        Training__c TrainingInfo_Obj = new Training__c(TrainingName__c = 'Training', TrainingHours__c =27);
        Database.UpsertResult Upsertresults = Database.upsert(TrainingInfo_Obj);
        id strTrainingId= Upsertresults.getId();
     
     
      List<MeetingInfo__c> meetinginfo_Obj = new List<MeetingInfo__c>{
    new MeetingInfo__c(MeetingStatus__c = ProviderConstants.MEETINGINFO_COMPLETED, Training__c =strTrainingId,Referral__c=strReferaalId),
    new MeetingInfo__c(MeetingStatus__c = ProviderConstants.MEETINGINFO_DNA, Training__c = strTrainingId,Referral__c=strReferaalId),
    new MeetingInfo__c(MeetingStatus__c = ProviderConstants.MEETINGINFO_SCHEDULED, Training__c =strTrainingId,Referral__c=strReferaalId),
    new MeetingInfo__c(MeetingStatus__c = ProviderConstants.MEETINGINFO_CANCELLED, Training__c =strTrainingId,Referral__c=strReferaalId),
    new MeetingInfo__c(MeetingStatus__c = ProviderConstants.MEETINGINFO_COMPLETED, Training__c =strTrainingId3,Referral__c=strReferaalId2),
    new MeetingInfo__c(MeetingStatus__c = ProviderConstants.MEETINGINFO_COMPLETED, Training__c = strTrainingId3,Referral__c=strReferaalId2)
 
        };  
     
    Insert meetinginfo_Obj;
    test.stopTest();
  }
  static testMethod void test_IsDisabled_UseCase1(){
    List<MeetingInfo__c> meetinginfo_Obj  =  [SELECT Id,Name,MeetingStatus__c,Training__c from MeetingInfo__c];
    System.assertEquals(true,meetinginfo_Obj.size()>0);
    MeetingInfoTriggerHandler obj01 = new MeetingInfoTriggerHandler();
    obj01.IsDisabled();
  }
  static testMethod void test_BeforeInsert_UseCase1(){
    List<MeetingInfo__c> meetinginfo_Obj  =  [SELECT Id,Name,MeetingStatus__c,Training__c from MeetingInfo__c];
    System.assertEquals(true,meetinginfo_Obj.size()>0);
    MeetingInfoTriggerHandler obj01 = new MeetingInfoTriggerHandler();
    obj01.BeforeInsert(new List<Sobject>());
  }
  static testMethod void test_BeforeUpdate_UseCase1(){
    List<MeetingInfo__c> meetinginfo_Obj  =  [SELECT Id,Name,MeetingStatus__c,Training__c from MeetingInfo__c];
    System.assertEquals(true,meetinginfo_Obj.size()>0);
    MeetingInfoTriggerHandler obj01 = new MeetingInfoTriggerHandler();
    obj01.BeforeUpdate(new Map<id,sObject>(),new Map<id,sObject>());
  }
  static testMethod void test_BeforeDelete_UseCase1(){
    List<MeetingInfo__c> meetinginfo_Obj  =  [SELECT Id,Name,MeetingStatus__c,Training__c from MeetingInfo__c];
    System.assertEquals(true,meetinginfo_Obj.size()>0);
    MeetingInfoTriggerHandler obj01 = new MeetingInfoTriggerHandler();
    obj01.BeforeDelete(new Map<id,sObject>());
  }
  static testMethod void test_AfterInsert_UseCase1(){
    List<MeetingInfo__c> meetinginfo_Obj  =  [SELECT Id,Name,MeetingStatus__c,Training__c from MeetingInfo__c];
    System.assertEquals(true,meetinginfo_Obj.size()>0);
    MeetingInfoTriggerHandler obj01 = new MeetingInfoTriggerHandler();
    obj01.AfterInsert(new List<Sobject>());
  }
  static testMethod void test_AfterUpdate_UseCase1(){
 
   
     
   List<MeetingInfo__c> meetinginfo_Obj  =  [SELECT Id,Name,MeetingStatus__c,Training__c,Referral__c from MeetingInfo__c];

 List<MeetingInfo__c> meetinginfo_Obj1  =  [SELECT Id,Name,MeetingStatus__c,Training__c,Referral__c from MeetingInfo__c];
 System.assertEquals(true,meetinginfo_Obj.size()>0);

 Map<Id, MeetingInfo__c> newMeetingMap = new Map<Id, MeetingInfo__c>();
   
 Map<Id, MeetingInfo__c> oldMeetingMap = new Map<Id, MeetingInfo__c>();
   
 
 newMeetingMap.putAll(meetinginfo_Obj);
 oldMeetingMap.putAll(meetinginfo_Obj1);
 for(id oid : oldMeetingMap.keySet()){
          oldMeetingMap.get(oid).MeetingStatus__c='Pending';
      }
     MeetingInfoTriggerHandler obj01 = new MeetingInfoTriggerHandler();
    obj01.AfterUpdate(newMeetingMap,oldMeetingMap);
  }
  static testMethod void test_AfterDelete_UseCase1(){
    List<MeetingInfo__c> meetinginfo_Obj  =  [SELECT Id,Name,MeetingStatus__c,Training__c from MeetingInfo__c];
    System.assertEquals(true,meetinginfo_Obj.size()>0);
    MeetingInfoTriggerHandler obj01 = new MeetingInfoTriggerHandler();
    obj01.AfterDelete(new Map<id,sObject>());
  }
  static testMethod void test_AfterUndelete_UseCase1(){
    List<MeetingInfo__c> meetinginfo_Obj  =  [SELECT Id,Name,MeetingStatus__c,Training__c from MeetingInfo__c];
    System.assertEquals(true,meetinginfo_Obj.size()>0);
    MeetingInfoTriggerHandler obj01 = new MeetingInfoTriggerHandler();
    obj01.AfterUndelete(new List<Sobject>());
  }
}