/**
     * @Author        : Sindhu Venkateswarlu
     * @CreatedOn     : March 28,2020
     * @Purpose       :Dasboard Details
     **/
    public with sharing class ReferralHomeDashboard {
    
    @AuraEnabled(cacheable=true)
        public static List<Wrappercasecount> getreferralhomeCases() {
            List<Wrappercasecount> lstWrapper = new List<Wrappercasecount>();
            Integer approvedRec=0;
            Integer PendingRec=0;
            Integer RejectedRec=0;
            for(case childwelfarcase : [select Id,Casenumber,Status,Type from Case where (Status='Approved' OR Status='Draft' OR Status='Rejected' ) Order By CaseNumber DESC]){
                if(childwelfarcase.Status=='Approved'){
                    approvedRec++;
                }
                if(childwelfarcase.Status=='Draft'){
                    PendingRec++;
                }
				if(childwelfarcase.Status=='Rejected'){
                    RejectedRec++;
                }
            }
            
          lstWrapper.add(new Wrappercasecount(approvedRec,PendingRec,RejectedRec)); 
          return   lstWrapper; 
        }

        // Wrapper Class
 public class Wrappercasecount {
    @AuraEnabled public Integer approvedcasescount;
    @AuraEnabled public Integer pendingcasescount;
	@AuraEnabled public Integer Rejectedcasescount;
    @AuraEnabled public Integer totalcount;
    public Wrappercasecount(Integer approvercount, Integer pendingcount,Integer Rejectingcount) {
        this.approvedcasescount = approvercount;
        this.pendingcasescount = pendingcount;
		this.Rejectedcasescount= Rejectingcount;
        this.totalcount=approvercount+pendingcount+Rejectingcount;
    }
} 
}