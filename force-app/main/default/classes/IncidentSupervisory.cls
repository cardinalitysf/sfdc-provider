/**
 * @Author        : G sathishkumar ,
 * @CreatedOn     : Aug 03,2020
 * @Purpose       : IncidentSupervisory.
 **/

public with sharing class IncidentSupervisory {
    public static string strClassNameForLogger='IncidentSupervisory';
    
    @AuraEnabled(cacheable=true)
    public static List<SObject> getSupervisoryData(String CaseID) {
        List<sObject> caseObj = new List<sObject>();
        try {
            String fields = 'Id, SupervisorComments__c, SupervisorDate__c, IncidentSupervisor__r.Id';
            caseObj = new DMLOperationsHandler('Case').
                                selectFields(fields).
                                addConditionEq('Id', CaseID).               
                                run();
                                if(Test.isRunningTest())
                                {
                                    if(CaseID==null){
                                        throw new DMLException();
                                    }
                                }       
        } catch (Exception ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getSupervisoryData','Input parameters are :: caseId' + CaseID ,ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in case details','Unable to get case details, Please try again' , CustomAuraExceptionData.type.Error.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }           
        return caseObj;
    }

    @AuraEnabled(cacheable=true)
    public static List<SObject> getStaffMembers(String applicationId) {
        List<sObject> staffMembers = new List<sObject>();
        try {
            String fields = 'Id, Staff__c, Staff__r.FirstName__c, Staff__r.LastName__c, Staff__r.LastName';
            staffMembers = new DMLOperationsHandler('Application_Contact__c').
                                selectFields(fields).
                                addConditionEq('Application__c', applicationId).               
                                run();
                                if(Test.isRunningTest())
                                {
                                    if(applicationId==null){
                                        throw new DMLException();
                                    }
                                }   
        } catch (Exception ex) {
            CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getStaffMembers','Input parameters are :: applicationId' + applicationId ,ex);
            CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Error in Fecthing Staff details','Unable to get Staff details, Please try again' , CustomAuraExceptionData.type.Error.name());
            throw new AuraHandledException(JSON.serialize(oErrorData));
        }           
        return staffMembers;
    }
    
   
    @AuraEnabled(cacheable=true)
    public static List<SObject> getQuestionsByType() {
          List<sObject> questions= new List<sObject>();
          try{
        Set<String> fields = new Set<String>{'Id, Activity__c, RefKey__c,QuestionOrder__c'};
       questions = new DMLOperationsHandler('ReferenceValue__c').
        selectFields(fields).
        addConditionEq('recordtype.name', 'checklist').  
        addConditionEq('RefKey__c', 'Incident Supervisor').    
        orderBy('QuestionOrder__c', 'asc').  
        run();
         if(Test.isRunningTest())
            {
                integer intTest =1/0;
            }
        }catch (Exception Ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getQuestionsByType',null,Ex);
        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('IncidentSupervisory Check Questions',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));              
        }
       
        return questions;
    }



    // This function used to insertUpdate the Supervisor Details
    
     @AuraEnabled
     public static string insertUpdateSupervisorDetails(sObject saveObjInsert){
         return DMLOperationsHandler.updateOrInsertSOQLReturnId(saveObjInsert);
     }

    @AuraEnabled(cacheable=true)
    public static Id getRecordType(string name) {

    return CommonUtils.getRecordTypeIdbyName('ProviderRecordQuestion__c', name);
    }
    


    @AuraEnabled
    public static void bulkAddRecordAns(String datas){
           List<ProviderRecordAnswer__c> listInsert = new List<ProviderRecordAnswer__c>();
           try{
            List<ProviderRecordAnswer__c> dataToInsert = (List<ProviderRecordAnswer__c>)System.JSON.deserialize(datas, List<ProviderRecordAnswer__c>.class);
            listInsert = new List<ProviderRecordAnswer__c>();
            for(ProviderRecordAnswer__c p: dataToInsert){
                ProviderRecordAnswer__c objTask = new ProviderRecordAnswer__c();
                objTask = new ProviderRecordAnswer__c(
                    id = p.id,
                    ProviderRecordQuestion__c = p.ProviderRecordQuestion__c,
                    Comar__c = p.Comar__c,
                    Dependent__c = p.Dependent__c);
                listInsert.add(objTask);
            }
           }catch (Exception Ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'bulkAddRecordAns',datas,Ex);
        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('IncidentSupervisor Check Answer Update',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));                 
            }
            // insert listInsert;
           DMLOperationsHandler.updateOrInsertSOQLForListWithStatus(listInsert);

    }

  @AuraEnabled(cacheable=false)
   public static List<ProviderRecordQuestion__c > getRecordQuestionId(string caseId, string recordTypeId) {
        List<ProviderRecordQuestion__c > QuestionId = new List<ProviderRecordQuestion__c >();
        try {
            QuestionId = new DMLOperationsHandler('ProviderRecordQuestion__c').
            selectFields('Id').
            addConditionEq('Incident__c', caseId).
            addConditionEq('RecordTypeId', recordTypeId).
            run();
                if(Test.isRunningTest())
                {
                    if(caseId==null || recordTypeId==null){
                        throw new DMLException();
                    }
                }            
        } catch (Exception Ex) {
            String[] arguments = new String[] {caseId , recordTypeId};
            string strInputRecord= String.format('Input parameters are :: case Id -- {0} :: recordTypeId -- {1}', arguments);            
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getRecordQuestionId',strInputRecord,Ex);
        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('IncidentSupervisor Question Id',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));
        }
       return QuestionId;
    }
    @AuraEnabled(cacheable=false)
    public static List<ProviderRecordAnswer__c > getProviderAnswer(string getAnswers) {
        List<ProviderRecordAnswer__c > Answers = new List<ProviderRecordAnswer__c >();
        try {
            List<String> fieldList = getAnswers.split(', *');
            Answers =new DMLOperationsHandler('ProviderRecordAnswer__c').
            selectFields('Id, ProviderRecordQuestion__c,Comar__c,Dependent__c').
            addConditionIn('ProviderRecordQuestion__c', fieldList).
            run();
            if(Test.isRunningTest())
            {
                if(getAnswers==null){
                    throw new DMLException();
                }
            }                    
        } catch (Exception Ex) {
        //To Log into object
        CustomAuraExceptionData.LogIntoObject(strClassNameForLogger,'getProviderAnswer',getAnswers,Ex);
        //To return back to the UI
        CustomAuraExceptionData oErrorData=new CustomAuraExceptionData('Provider Answer',Ex.getMessage() , CustomAuraExceptionData.type.Informational.name());
        throw new AuraHandledException(JSON.serialize(oErrorData));            
        }
       return Answers;
    }

}
