/**
 * @Author        : Janaswini S
 * @CreatedOn     : April 15 ,2020
 * @Purpose       : Providers Tabsets
 **/

public with sharing class ProvidersTabset {
  public static string strClassNameForLogger = 'ProvidersTabset';
  @AuraEnabled(cacheable=true)
  public static List<SObject> fetchDataForTabset(string fetchdataname) {
    String fields = 'Id,Name,ProviderId__c';
    List<sObject> lstAccount = new List<sObject>();
    try {
      lstAccount = new DMLOperationsHandler('Account')
        .selectFields(fields)
        .addConditionEq('Id', fetchdataname)
        .run();
      if (Test.isRunningTest()) {
        if (fetchdataname == null) {
          throw new DMLException();
        }
      }
    } catch (Exception ex) {
      CustomAuraExceptionData.LogIntoObject(
        'ProvidersTabset',
        'fetchDataForTabset',
        'Input parameters are :: fetchdataname' + fetchdataname,
        ex
      );
      CustomAuraExceptionData oErrorData = new CustomAuraExceptionData(
        'Error in fetching provider details',
        'Unable to get provider details, Please try again',
        CustomAuraExceptionData.type.Error.name()
      );
      throw new AuraHandledException(JSON.serialize(oErrorData));
    }

    return lstAccount;
  }
}
